//
//  CKLeaderboard.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 19.06.13.
//
//

#include "CKLeaderboard.h"
#include "CKLoaderGUI.h"
#include "CKTableScore.h"
#include "CKAudioMgr.h"
#include "CKParseHelper.h"
#include "CKTwitterHelper.h"
#include "CKFacebookHelper.h"

CCScene* CKLeaderboard::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    // add layer as a child to scene
    CCLayer* layer = new CKLeaderboard();
   
    if(layer->init())
    {
        scene->addChild(layer);
        layer->setTouchEnabled(true);
        layer->release();
    };
    return scene;
};

CKLeaderboard::~CKLeaderboard()
{
    CC_SAFE_DELETE(m_gui_lead);
    CC_SAFE_DELETE(m_gui_score);
    CC_SAFE_DELETE(m_lead);
    CC_SAFE_DELETE(m_score);
    CC_SAFE_DELETE(m_dialog_wait);
    CKTableFacebook::Instance().saveScoreRecords("fb_score.plist",1);
    CKTableTwitter::Instance().saveScoreRecords("tw_score.plist",2);
};

CKLeaderboard::CKLeaderboard()
{
    m_parent = NULL;
    m_dialog_wait = NULL;
    CCLayer::init();
 
    win_size = CCDirector::sharedDirector()->getWinSize();
    m_bg_mask = CCSprite::create("gray_mask.png");
    addChild(m_bg_mask,-1);
    m_bg_mask->setScaleX(win_size.width/m_bg_mask->getContentSize().width);
    m_bg_mask->setScaleY(win_size.height/m_bg_mask->getContentSize().height);
    m_bg_mask->setPosition(ccp(win_size.width/2,win_size.height/2));
    
    initLeaderBoard();
    initScoreBoard();
    
    showMainTabel(false);
    current_tap = CK::Action_Facebook;

    CKTableFacebook::Instance().setCallbackFunc(NULL,NULL);
    initDialogWait();
    scheduleUpdate();
};

void CKLeaderboard::initDialogWait()
{
    m_dialog_wait = new CKGUI;
    m_dialog_wait->create(CCNode::create());
    m_dialog_wait->load(CK::loadFile("c_ModalWindowWait.plist").c_str());
    m_dialog_wait->setTarget(this);
    m_dialog_wait->get()->setVisible(false);
    m_dialog_wait->get()->setTag(0);
    addChild(m_dialog_wait->get(),99);
    m_dialog_wait->get()->setPosition(ccp(win_size.width/2,win_size.height/2));

    m_dialog_wait->getChildByTag(4)->getNode()->runAction(CCRepeatForever::create(CCRotateBy::create(1.0, 360)));
};
void CKLeaderboard::showDialogWait()
{
    unschedule(schedule_selector(CKLeaderboard::closeWait));
    schedule(schedule_selector(CKLeaderboard::closeWait),TIMEOUT_WAIT,0,TIMEOUT_WAIT);
    m_dialog_wait->get()->setVisible(true);
    m_dialog_wait->get()->setTag(m_dialog_wait->get()->getTag() + 1);
    this->setTouchEnabled(false);
    CCLOG("CKLeaderboard::showDialogWait %d",m_dialog_wait->get()->getTag());
}
void CKLeaderboard::hideDialogWait()
{
   // return;
    m_dialog_wait->get()->setTag(m_dialog_wait->get()->getTag() - 1);
    if(m_dialog_wait->get()->getTag() <= 0)
    {
        unschedule(schedule_selector(CKLeaderboard::closeWait));
        m_dialog_wait->get()->setVisible(false);
        this->setTouchEnabled(true);
    };
    CCLOG(" CKLeaderboard::hideDialogWait %d",m_dialog_wait->get()->getTag());
};

void CKLeaderboard::closeWait()
{
    unschedule(schedule_selector(CKLeaderboard::closeWait));
    m_dialog_wait->get()->setVisible(false);
    this->setTouchEnabled(true);
    m_dialog_wait->get()->setTag(0);
    showTap();
};

void CKLeaderboard::initLeaderBoard()
{
    m_gui_lead = new CKGUI;
    m_gui_lead->create(CCNode::create());
    m_gui_lead->load(CK::loadFile("c_Leaderboard.plist").c_str());
    m_gui_lead->addFuncToMap("menuCall", callfuncND_selector(CKLeaderboard::menuCall));
    m_gui_lead->addFuncToMap("loginCall", callfuncND_selector(CKLeaderboard::loginCall));
    m_gui_lead->setTarget(this);
    m_gui_lead->setTouchEnabled(true);
    m_lead = new CKSScroll;
    m_lead->m_scroll = m_gui_lead->getChildByTag(12);
    
    m_lead->frame = m_lead->m_scroll->getChildByTag(1)->getNode()->getContentSize().height;
    m_lead->top_pos = m_lead->frame*0.9;
    m_lead->bottom_pos = m_lead->frame*6.2;
    
    m_lead->m_scroll->setSlideSensivity(ccp(0,1.0));
    m_lead->m_scroll->setVisible(false);
    mask_lead = m_gui_lead->getChildByTag(-1);
    
    me_object = m_gui_lead->getChildByTag(97);
    me_object->setVisible(false);
    
    addChild(m_gui_lead->get());
    m_view_lead = CCRenderTexture::create(mask_lead->getNode()->getContentSize().width, mask_lead->getNode()->getContentSize().height);
    m_view_lead->setPosition(mask_lead->getPos());
    m_view_lead->setVisible(false);
    
    m_lead->posX = m_lead->m_scroll->getPos().x;
    m_gui_lead->get()->addChild(m_view_lead,3);
    if(win_size.width == 568)
    {
        m_gui_lead->get()->setPositionX(44);
    }
    m_gui_lead->getChildByTag(100 + CK::Action_Tweet)->setVisible(false);
    m_gui_lead->getChildByTag(100 + CK::Action_Facebook)->setVisible(false);
};

void CKLeaderboard::initScoreBoard()
{
    m_gui_score = new CKGUI;
    m_gui_score->create(CCNode::create());
    m_gui_score->load(CK::loadFile("c_LeaderboardTable.plist").c_str());
    m_gui_score->addFuncToMap("menuCall", callfuncND_selector(CKLeaderboard::menuCall));
    m_gui_score->addFuncToMap("loginCall", callfuncND_selector(CKLeaderboard::loginCall));
    m_gui_score->setTarget(this);
    m_gui_score->setTouchEnabled(true);
    m_score = new CKSScroll;
    m_score->m_scroll = m_gui_score->getChildByTag(12);
    
    m_score->frame = m_score->m_scroll->getChildByTag(1001)->getNode()->getContentSize().height;
    m_score->top_pos = 0;
    m_score->bottom_pos = m_score->frame*6.0;
    
    m_score->m_scroll->setSlideSensivity(ccp(0,1.0));
    mask_score = m_gui_score->getChildByTag(-1);
    
    addChild(m_gui_score->get());
    m_view_score = CCRenderTexture::create(mask_score->getNode()->getContentSize().width, mask_score->getNode()->getContentSize().height);
    m_view_score->setPosition(mask_score->getPos());
    
    m_score->m_scroll->setPosition(m_lead->posX, m_view_score->getContentSize().height*2);
    m_score->posX = m_score->m_scroll->getPos().x;
    
    m_gui_lead->get()->addChild(m_view_score,3);
    
    if(win_size.width == 568)
    {
        m_gui_score->get()->setPositionX(44);
    }
    
    score_objects.clear();
    first_position = m_score->m_scroll->getChildByTag(1001)->getPos();
    for (int i = 0; i < 50; ++i) {
        score_objects.push_back( m_score->m_scroll->getChildByTag(1001 + i));
    };
};

void CKLeaderboard::setValueToScrollChild(CKObject *_obj,const char *_name,int _score,int _num,int _score_format)
{
    static_cast<CCLabelTTF*>(_obj->getChildByTag(1)->getNode())->setString(_name);
    char str_score[255];
    if(_score_format == 9)
        sprintf(str_score,"%.2f",float(_score)/1000.0);
    else
        sprintf(str_score,"%d",_score);
    static_cast<CCLabelBMFont*>(_obj->getChildByTag(2)->getNode())->setString(str_score);
    char str_num[255];
    sprintf(str_num, "%d.",_num);
    static_cast<CCLabelBMFont*>(_obj->getChildByTag(3)->getNode())->setString(str_num);
};

void CKLeaderboard::fillScoreTabel(int _field)
{
    if(_field < 0)
        _field = 0;
    if(_field > 11)
        _field = 11;
    
    int k = 0;
    std::list<CKScoreRecords *> *list = NULL;
    
    if(current_tap == CK::Action_Facebook)
        list =  CKTableFacebook::Instance().getField(_field);
    if(current_tap == CK::Action_Tweet)
        list =  CKTableTwitter::Instance().getField(_field);
    if(!list)
        return;
    me_tab_position = -1;
    std::list<CKScoreRecords *>::iterator it = list->begin();
    it_object = score_objects.begin();
    while (it_object != score_objects.end())
    {
        if (it != list->end()) {
            (*it_object)->setVisible(true);
            (*it_object)->setPosition(first_position.x, first_position.y - m_score->frame*k);
            setValueToScrollChild((*it_object),(*it)->name,(*it)->fields[_field],k+1,_field);
            
            if((current_tap == CK::Action_Tweet && (strcmp((*it)->m_id, CKHelper::Instance().TwitterID.c_str()) == 0))||(current_tap == CK::Action_Facebook && (strcmp((*it)->m_id, CKHelper::Instance().FacebookID.c_str()) == 0)))
            {
                    me_tab_position = k;
                    setValueToScrollChild(me_object,(*it)->name,(*it)->fields[_field],k+1,_field);
            };
            k++;
          //  CCLOG("fillScoreTabel %s %s",(*it)->m_id,(*it)->name);
            it++;
        }
        else
        {
            (*it_object)->setVisible(false);
        }
        it_object++;
    };
    if (k < 6) {
        m_score->bottom_pos = m_score->top_pos;
        if(k == 0)
        {
            it_object = score_objects.begin();
            
            k = 1;
            if(current_tap == CK::Action_Facebook)
                setValueToScrollChild((*it_object),CKHelper::Instance().FACEBOOK_USERNAME.c_str(),0,1,_field);
            if(current_tap == CK::Action_Tweet)
                setValueToScrollChild((*it_object),CKHelper::Instance().TWITTER_USERNAME.c_str(),0,1,_field);
            //setValueToScrollChild((*it_object),(*it)->name,0,k,_field);
            //setValueToScrollChild(me_object,(*it)->name,0,k,_field);
            (*it_object)->setVisible(true);
           // me_object->setVisible(false);
        }
    }
    else
    {
        m_score->bottom_pos = m_score->top_pos - m_score->frame*(k - 6);
    };
    
    if(me_tab_position == -1)//not my score in leaderboard
    {
        if(current_tap == CK::Action_Facebook)
            setValueToScrollChild(me_object,CKHelper::Instance().FACEBOOK_USERNAME.c_str(),0,1,_field);
        if(current_tap == CK::Action_Tweet)
            setValueToScrollChild(me_object,CKHelper::Instance().TWITTER_USERNAME.c_str(),0,1,_field);
    };
    count_visible_rows = k;
};

void CKLeaderboard::update(float _dt)
{
    if(m_view_lead->isVisible())
    {
        m_view_lead->clear(0.0, 0.0, 0.0, 0.0);
        m_view_lead->begin();
        m_lead->m_scroll->setVisible(true);
        m_lead->m_scroll->getNode()->visit();
        m_lead->m_scroll->setVisible(false);
        m_view_lead->end();
    }
    if(m_view_score->isVisible())
    {
        
        m_view_score->clear(0.0, 0.0, 0.0, 0.0);
        m_view_score->begin();
        m_score->m_scroll->setVisible(true);
        m_score->m_scroll->getNode()->visit();
        m_score->m_scroll->setVisible(false);
        m_view_score->end();
    }
};

#pragma mark - TOUCH
void CKLeaderboard::ccTouchesBegan(cocos2d::CCSet* pTouches, cocos2d::CCEvent* event)
{
    CCSetIterator it;
    CCTouch* touch;
    
    for( it = pTouches->begin(); it != pTouches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        last_pos = location;
        touch_scroll = false;
        b_touch_move = false;
        if(currnet_leaderboard == TABEL_MAIN)
        {
            touch_scroll = true;//mask_lead->isPointInObject(location);
        }
        if(currnet_leaderboard == TABEL_SCORE)
        {
            if(mask_score->isPointInObject(location))
            {
                touch_scroll = true;
            }
            if(m_gui_score)
                m_gui_score->setTouchBegan(location);
        }
        if(m_gui_lead)
            m_gui_lead->setTouchBegan(location);
    };
};

void CKLeaderboard::ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent)
{
    CCSetIterator it;
    CCTouch* touch;
    
    for( it = pTouches->begin(); it != pTouches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        b_touch_move = true;
        if(currnet_leaderboard == TABEL_MAIN)
        {
            if(touch_scroll)
            {
                float moved = location.y - last_pos.y;
                m_lead->start_anim = false;
                moveScroll(m_lead,moved);
                   // CCLOG("m_lead pos %f %f",location.y,m_lead->m_scroll->getPos().y);
            };
        }
        if(currnet_leaderboard == TABEL_SCORE)
        {
            if(touch_scroll)
            {
               // CCLOG("pos %f %f %d",location.y,m_score->m_scroll->getPos().y,int((m_score->m_scroll->getPos().y + m_score->frame)/m_score->frame));

                float moved = location.y - last_pos.y;
                m_score->start_anim = false;
                moveScroll(m_score,moved);
            };
            
            if(m_gui_score)
                m_gui_score->setTouchMove(location);
        }
        if(m_gui_lead)
            m_gui_lead->setTouchMove(location);
    }
};


void CKLeaderboard::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
{
    CCSetIterator it;
    CCTouch* touch;

    for( it = pTouches->begin(); it != pTouches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        if(currnet_leaderboard == TABEL_MAIN)
        {
            if(touch_scroll)
            {
                touch_scroll = false;
                returnScroll(m_lead);
                if(!b_touch_move && m_view_lead->isVisible())
                {
                    m_lead->m_scroll->setVisible(true);
                    m_lead->m_scroll->setTouch(true);
                    CCPoint diff = ccp(m_view_lead->getPositionX()/2,m_view_lead->getPositionY()/2);
                    if(win_size.width != 1024)
                    {
                        diff.y -= m_lead->frame/2;
                    };
                    CCPoint point = ccpSub(location,diff);
                    CKObject* tmp =  m_gui_lead->getTouchObject(point);
                    if(tmp)
                    {
                        CCLOG("tmp getTag %d",tmp->getTag());
                        if(tmp->getType() == CK::GuiPicture)
                        {
                            CKAudioMng::Instance().playEffect("run_scroll");
                            loadScoreTabel(tmp->getTag() - 1);
                        }
                    }
                    m_lead->m_scroll->setVisible(false);
                    m_lead->m_scroll->setTouch(false);

                };
            };
        };
//        if(b_touch_move)
//        {
//             CKAudioMng::Instance().playEffect("run_scroll");
//        }
        b_touch_move = false;
        if(currnet_leaderboard == TABEL_SCORE)
        {
            if(touch_scroll)
            {
                touch_scroll = false;
                returnScroll(m_score);
            };
            if(m_gui_score)
                m_gui_score->setTouchEnd(location);
        };
        if(m_gui_lead)
            m_gui_lead->setTouchEnd(location);
    }
};
void CKLeaderboard::moveScroll(CKSScroll* _scroll,float _value)
{
    if((_scroll->m_scroll->getPos().y - _scroll->top_pos) < 0 && _value < 0)
        _value *= fabs( 1.0 - MIN(1.0,fabs(_scroll->m_scroll->getPos().y - _scroll->top_pos)/(_scroll->frame*15)));
    
    if((_scroll->m_scroll->getPos().y - _scroll->bottom_pos) > 0 && _value > 0)
        _value *= fabs( 1.0 - MIN(1.0,fabs(_scroll->m_scroll->getPos().y - _scroll->bottom_pos)/(_scroll->frame*15)));
    _scroll->m_scroll->moveY(_value);
};
void CKLeaderboard::returnScroll(CKSScroll* _lead)
{
    if((_lead->m_scroll->getPos().y - _lead->top_pos) < 0)
    {
        _lead->m_scroll->getNode()->runAction(CCMoveTo::create(0.1,ccp(_lead->posX,_lead->top_pos) ));
        _lead->start_anim = false;
        _lead->m_scroll->savePosition(ccp(_lead->posX,_lead->top_pos));
    }
    else
        if((_lead->m_scroll->getPos().y - _lead->bottom_pos) > 0)
        {
            _lead->start_anim = false;
            _lead->m_scroll->savePosition(ccp(_lead->posX,_lead->bottom_pos));
            _lead->m_scroll->getNode()->runAction(CCMoveTo::create(0.1,ccp(_lead->posX,_lead->bottom_pos) ));
        }
        else
        {
            _lead->m_scroll->savePosition(_lead->m_scroll->getNode()->getPosition());
        }
};

void CKLeaderboard::refresh()
{
    CKHelper::Instance().leaderboard_login = false;
    //hideDialogWait();
    if(current_tap == CK::Action_Tweet)
    {
        showDialogWait();
        CKTwitterHelper::Instance().TweetLoadFriendList(this,callfuncND_selector(CKLeaderboard::compliteLoadFriend));
    };
    if(current_tap == CK::Action_Facebook)
    {
        showDialogWait();
        CKFacebookHelper::Instance().FB_LoadFriendsList();
    }
};

void CKLeaderboard::loginCall(CCNode *_sender,void *_value)
{
    CKAudioMng::Instance().playEffect("button_pressed");
    
    switch (*static_cast<int *>(_value)) {
        case CK::Action_Facebook:
            CKTableFacebook::Instance().setCallbackFunc(this, callfunc_selector(CKLeaderboard::showTap));
            CKHelper::Instance().leaderboard_login = true;
            showDialogWait();
            if(!CKFacebookHelper::Instance().FB_isOpenSession())
            {
                CKFacebookHelper::Instance().setCallBackFunc(this, callfuncND_selector(CKLeaderboard::compliteLoadFriend));
                CKFacebookHelper::Instance().setNextCallFuncName("FB_LoadFriendsList");
                CKFacebookHelper::Instance().FB_Login();
            }
            else{
                if(!CKTableFacebook::Instance().friendEmpty())
                {
                    CKTableFacebook::Instance().clearScoreRecords();
                    //ObjCCalls::setCallBackCompliteLoad(this, callfuncND_selector(CKLeaderboard::compliteLoadScore));
                    CKParseHelper::Instance().parseLoadScore(CKTableFacebook::Instance().getFriends(), 1,this, callfuncND_selector(CKLeaderboard::compliteLoadScore));
                }
                else
                {
                    CKFacebookHelper::Instance().setCallBackFunc(this, callfuncND_selector(CKLeaderboard::compliteLoadFriend));
                    CKFacebookHelper::Instance().FB_LoadFriendsList();
                }
            }

            break;
        case CK::Action_Tweet:
            showDialogWait();
            CKTableFacebook::Instance().setCallbackFunc(this, callfunc_selector(CKLeaderboard::showTap));
            CKHelper::Instance().leaderboard_login = true;
            if(!CKTwitterHelper::Instance().TweetCanSend())
            {
                if(ObjCCalls::getDeviceVersion() < 6.0)
                    CKTwitterHelper::Instance().TweetLogin();
                else
                {
                    CKTwitterHelper::Instance().TweetgetInfo(true,this, callfuncND_selector(CKLeaderboard::compliteLoadFriend));
                }
            }
            else
            {
                CKTwitterHelper::Instance().TweetLoadFriendList(this,callfuncND_selector(CKLeaderboard::compliteLoadFriend));
            }
            break;
        default:
            break;
    }
};

void CKLeaderboard::menuCall(CCNode *_sender,void *_value)
{
    
    switch (*static_cast<int *>(_value)) {
        case CK::Action_Back:
            CKAudioMng::Instance().playEffect("run_scroll");
            showMainTabel(true);
            break;
        case CK::Action_Close:
            CKAudioMng::Instance().playEffect("button_pressed");
            deactived();
            break;
        case CK::Action_GameCenter:
            CKAudioMng::Instance().playEffect("button_pressed");
            ObjCCalls::showLeaderboard();
            break;
        case CK::Action_Facebook:
            CKAudioMng::Instance().playEffect("button_pressed");
            current_tap = CK::Action_Facebook;
            showTap();
            break;
        case CK::Action_Tweet:
            CKAudioMng::Instance().playEffect("button_pressed");
            current_tap = CK::Action_Tweet;
            showTap();
            break;
        default:
            break;
    }
};

void CKLeaderboard::fadeTabel(int _num)
{
    
};

void CKLeaderboard::showTap()
{
   bool b_fblogin = !CKTableFacebook::Instance().scoreEmpty();
   bool b_tlogin = !CKTableTwitter::Instance().scoreEmpty();
    
    if(current_tap == CK::Action_Facebook)
    {
        m_gui_lead->getChildByTag(CK::Action_Facebook)->setStateImage(1);
        m_gui_lead->getChildByTag(CK::Action_Tweet)->setStateImage(0);
        
        m_gui_lead->getChildByTag(100 + CK::Action_Tweet)->setVisible(false);
        m_gui_lead->getChildByTag(100 + CK::Action_Facebook)->setVisible(!b_fblogin);
        if(currnet_leaderboard == TABEL_MAIN)
        {
            m_view_lead->setVisible(b_fblogin);
        }else if(currnet_leaderboard == TABEL_SCORE)
        {
            fillScoreTabel(current_field);
            m_view_score->setVisible(b_fblogin);
            me_object->setVisible(b_fblogin);
        }
    }
    else if(current_tap == CK::Action_Tweet)
    {
        if(currnet_leaderboard == TABEL_MAIN)
        {
            m_view_lead->setVisible(b_tlogin);
        }
        
        m_gui_lead->getChildByTag(CK::Action_Facebook)->setStateImage(0);
        m_gui_lead->getChildByTag(CK::Action_Tweet)->setStateImage(1);
        
        m_gui_lead->getChildByTag(100 + CK::Action_Facebook)->setVisible(false);
        m_gui_lead->getChildByTag(100 + CK::Action_Tweet)->setVisible(!b_tlogin);
        
        if(currnet_leaderboard == TABEL_SCORE)
        {
            fillScoreTabel(current_field);
            me_object->setVisible(b_tlogin);
            m_view_score->setVisible(b_tlogin);
        }
    }
};

void CKLeaderboard::loadScoreTabel(const int &_field)
{
    CCLOG("loadScoreTabel %d",_field);
   current_field = _field;
    if(current_field < 0)
        current_field = 0;
    if(current_field > 11)
        current_field = 11;
    
        m_gui_score->getChildByTag(14)->setText(m_gui_lead->getChildByTag(12)->getChildByTag(current_field + 1)->getChildByTag(2)->getText());
        showScoreTabel();
};

void CKLeaderboard::showMainTabel(bool _animate)
{
    m_gui_score->get()->setVisible(false);
    me_object->setVisible(false);
    m_gui_lead->get()->setVisible(true);
    m_gui_lead->getChildByTag(201)->setVisible(true);
    m_gui_lead->getChildByTag(202)->setVisible(true);
    if(_animate)
    {
        m_lead->m_scroll->setPosition(m_lead->m_scroll->getPos().x, m_lead->top_pos);
        m_lead->m_scroll->getNode()->runAction(CCMoveTo::create(TIME_EFFECT, ccp(m_lead->posX, m_lead->top_pos)));
        m_lead->m_scroll->savePosition(ccp(m_lead->posX, m_lead->top_pos));
        
        m_score->m_scroll->getNode()->runAction(CCMoveBy::create(TIME_EFFECT, ccp(win_size.width*0.7,0)));
        m_score->m_scroll->savePosition(ccp(m_score->posX + win_size.width*0.7, m_score->top_pos));
    }
    else
    {
          m_lead->m_scroll->setPosition(m_lead->posX, m_lead->top_pos);
         m_score->m_scroll->setPosition(m_score->posX + win_size.width*0.7, m_score->top_pos);
    }
    
    currnet_leaderboard = TABEL_MAIN;
    showTap();
};

void CKLeaderboard::showScoreTabel()
{
    currnet_leaderboard = TABEL_SCORE;
    m_gui_score->get()->setVisible(true);
    me_object->setVisible(true);
    m_view_score->setVisible(true);
    
    m_gui_lead->getChildByTag(201)->setVisible(false);
    m_gui_lead->getChildByTag(202)->setVisible(false);
    
    
    m_score->m_scroll->setPosition(m_score->m_scroll->getPos().x, m_score->top_pos);
    
    m_score->m_scroll->getNode()->runAction(CCMoveTo::create(TIME_EFFECT, ccp(m_score->posX, m_score->top_pos)));
    m_score->m_scroll->savePosition(ccp(m_score->posX, m_score->top_pos));
    
    
    m_lead->m_scroll->getNode()->runAction(CCMoveBy::create(TIME_EFFECT, ccp(-win_size.width*0.7,0)));
    m_lead->m_scroll->savePosition(ccp(m_lead->posX - win_size.width*0.7, m_lead->top_pos));
    showTap();
};
void CKLeaderboard::compliteLoadFriend(CCNode *_sender,void *_value)
{
    CCLOG("CKLeaderboard::compliteLoadFriend:: %d",*static_cast<int *>(_value));
    if(*static_cast<int *>(_value) == CK::Callback_State_TW_Complite)
    {
        if(!CKTableTwitter::Instance().friendEmpty() || CKHelper::Instance().TwitterID.length() > 0)
        {
            CKTableTwitter::Instance().clearScoreRecords();
            //ObjCCalls::setCallBackCompliteLoad(this, callfuncND_selector(CKLeaderboard::compliteLoadScore));
            CKParseHelper::Instance().parseLoadScore(CKTableTwitter::Instance().getFriends(), 2,this, callfuncND_selector(CKLeaderboard::compliteLoadScore));
        }
        else
        {
           hideDialogWait(); 
        }
    }
    else if (*static_cast<int *>(_value) == CK::Callback_State_TW_NoGranted)
    {
        showTap();
        hideDialogWait();
    };
    if(*static_cast<int *>(_value) == 1)
    {
        if(!CKTableFacebook::Instance().friendEmpty())
        {
            CKTableFacebook::Instance().clearScoreRecords();
            //ObjCCalls::setCallBackCompliteLoad(this, callfuncND_selector(CKLeaderboard::compliteLoadScore));
            CKParseHelper::Instance().parseLoadScore(CKTableFacebook::Instance().getFriends(), 1,this, callfuncND_selector(CKLeaderboard::compliteLoadScore));
        }else
        {
            hideDialogWait();
            showTap();
        }
    }
    else if (*static_cast<int *>(_value) == 3)
    {
        if(CKFacebookHelper::Instance().FB_isOpenSession())
        {
            CKFacebookHelper::Instance().setCallBackFunc(this, callfuncND_selector(CKLeaderboard::compliteLoadScore));
            CKFacebookHelper::Instance().FB_LoadFriendsListOld();
        }
        else
        {
            showTap();
            hideDialogWait();
        }
    }else if (*static_cast<int *>(_value) == 5)
    {
        showTap();
        hideDialogWait();
    }else if (*static_cast<int *>(_value) == CK::Callback_State_Error)
    {
        hideDialogWait();
    };
};

void CKLeaderboard::compliteLoadScore(CCNode *_sender,void *_value)
{
    CCLOG("CKLeaderboard::compliteLoadScore:: %d",*static_cast<int *>(_value));
    showTap();
    hideDialogWait();
};

void CKLeaderboard::loadScore()
{
    if(!ObjCCalls::isActiveConnection())
    {
        CKTableFacebook::Instance().loadScoreRecords("fb_score.plist",1);
        CKTableTwitter::Instance().loadScoreRecords("tw_score.plist",2);
        showTap();
        return;
    };
    if(!CKTableFacebook::Instance().friendEmpty())
    {
        showDialogWait();
        CKTableFacebook::Instance().clearScoreRecords();
       // ObjCCalls::setCallBackCompliteLoad(this, callfuncND_selector(CKLeaderboard::compliteLoadScore));
        CKParseHelper::Instance().parseLoadScore(CKTableFacebook::Instance().getFriends(), 1,this, callfuncND_selector(CKLeaderboard::compliteLoadScore));
    }
    else
    {
        showDialogWait();
        CKFacebookHelper::Instance().setCallBackFunc(this, callfuncND_selector(CKLeaderboard::compliteLoadFriend));
        CKFacebookHelper::Instance().FB_LoadFriendsList();
    };
    
    if(!CKTableTwitter::Instance().friendEmpty())
    {
        showDialogWait();
        CKTableTwitter::Instance().clearScoreRecords();
     //   ObjCCalls::setCallBackCompliteLoad(this, callfuncND_selector(CKLeaderboard::compliteLoadScore));
        CKParseHelper::Instance().parseLoadScore(CKTableTwitter::Instance().getFriends(), 2,this, callfuncND_selector(CKLeaderboard::compliteLoadScore));
    }
    else
    {
        showDialogWait();
        CKFacebookHelper::Instance().setCallBackFunc(this, callfuncND_selector(CKLeaderboard::compliteLoadFriend));
        CKTwitterHelper::Instance().TweetLoadFriendList(this,callfuncND_selector(CKLeaderboard::compliteLoadFriend));
    }
};

void CKLeaderboard::activeted(cocos2d::CCLayer *_node)
{
    CK::StatisticLog::Instance().setNewPage(CK::Page_LeadBoard);
    loadScore();
    
    cocos2d::CCLayer *m_layer = (cocos2d::CCLayer *)this;
    if(m_parent && m_parent->getParent())
        m_parent->getParent()->removeChild(m_layer, false);
    {
        m_parent = _node;
        CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(m_parent);
        
        m_layer->setTouchEnabled(true);
        m_layer->setVisible(true);
        
        m_parent->getParent()->addChild(m_layer,100);
        
        m_bg_mask->setVisible(true);
        
//        rtt->clear(0, 0, 0, 0);
//        rtt->begin();
//        m_bg_mask->setVisible(true);
//        CCDirector::sharedDirector()->getRunningScene()->visit();
//        
//        rtt->end();
//        
//        m_bg_mask->setVisible(false);
//        m_parent->pauseSchedulerAndActions();
//        m_parent->setVisible(false);
//        rtt->setVisible(true);
    };
    
};
void CKLeaderboard::deactived()
{
    CK::StatisticLog::Instance().setNewPage(CK::Page_MainMenu);
    cocos2d::CCLayer *m_layer = (cocos2d::CCLayer *)this;
    if(m_parent)
    {
        m_parent->setTouchEnabled(true);
        m_parent->getParent()->removeChild(m_layer, false);
        m_parent->resumeSchedulerAndActions();
        m_parent->setVisible(true);
        CCDirector::sharedDirector()->getTouchDispatcher()->addStandardDelegate(m_parent, 0);
    };
    m_parent = NULL;
    //rtt->setVisible(false);

    CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(this);
    
};
