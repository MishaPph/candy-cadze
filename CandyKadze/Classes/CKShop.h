//
//  CKShop.h
//  CandyKadze
//
//  Created by PR1.Stigol on 25.12.12.
//
//

#ifndef __CandyKadze__CKShop__
#define __CandyKadze__CKShop__

#include <iostream>
#include <vector.h>
#include <map.h>
#include "cocos2d.h"
#include "CKButton.h"
#include "CKHelper.h"
#include "CKBonusMgr.h"
#include "CKFileOptions.h"
#include "CKFileInfo.h"
#include "CK.h"
#include "ShopBomb.h"

class CKObject;
class CKGUI;
const int COUNT_BUY_BUTTON = 4;

struct CKSScroll
{
    CKObject* m_scroll;
    float frame;
    float posX;
    float height,top_pos,bottom_pos;
    int min_pos,max_pos;
    bool start_anim;
};

class CKShop : public cocos2d::CCLayer {
public:
    ~CKShop();
    CKShop();
    
    void reload();
    void functAsunc(CKShop * _sender);
    void activeted(cocos2d::CCLayer *_node);
    static CCScene* scene();
    void setLikes();
    void tweetCallBack(CCNode *_node,int *_value);
    void callTopTap(CCNode* _sender,void *_value);
    void sort(unsigned char _type);
    void showObjectType(const int &_type);
private:
    const int COUNT_OBJECT_IN_SCROLL  = 52;
    const float TIME_ANIMATION_BUY = 0.05;
    const int TOTAL_COST_ID = 8;
    const int ICON_COINS_ID = 4;
    const int COUNT_BUY_ID = 10;
    const int BUTTON_BUY_ID = 9;
    const int BEST_PRICE_ID = 12;
    const int LABEL_PRICE_ID = 8;
    const int LABEL_PRICE_VALUE = 7;
    const int LABEL_FREE_ID = 9;
    const int LABEL_ININVENTORY_VALUE = 5;
    const int MIN_SCROLL_FRAME = -48;
    const int DISCOUNT_ID = 13;
    const int LABEL_BUY_ID = 6;
    const int BUTTON_HINT_ID = 22;
    const int LABEL_TOTAL_SCORE = 51;
    
    const float TIMEOUT_WAIT = 10.0;
    const int COUNT_LEFT_TAP = 5;
    const int COUNT_COINS_TAP = 3;
    
    std::map<int,ShopBomb*>::iterator bomb_it;
    
  //  CK::ShopBomb* m_bomb;
    CCLayer* m_parent;
    
    int click_buyIapp;
    float show_diff;
    CKObject *top_tap[2];
    CKObject *left_tap[5];
    CKObject *coins_tap[3];
    float version;
    float offset_bombs_tap;
    int curent_load;
    int mas_count[COUNT_BUY_BUTTON];
    CCPoint location,prev_location;
    int best_price_mas[COUNT_BUY_BUTTON];
    float discount_mas[COUNT_BUY_BUTTON];
    int prev_num;
    bool b_touchmove;
    CKObject *first_object,*last_object,*next_show;
    float size_combo,max_size_combo;
    float anim_size;
    int *best_price;
    float *free_info;
    float height_buy_coins;
    bool enable_animation;
    CCSize win_size;
    
    std::vector<CKObject*> shop_object_list;
    std::vector<CKObject*> active_object_list;
    std::vector<CKObject*> price_list;
    std::vector<CKObject*>::iterator it;
    CKObject* curent_object,*show_object,*buy_object;
    CKSScroll* m_bomb,*m_coins;
    
    CCNode *m_slider;
    CCRenderTexture* rt;
    CCSprite* sprite_scroll;
    CCDictionary *dict_shop;
    
    bool touch_scroll;
    bool show_button_km_inventory;
    int count_shop_elements;
    int sum_bouth_bomb;
    unsigned char curent_sort_type;
    CCPoint bomb_tap_pos;
    
    CCPoint last_point;
    CKGUI *m_gui,*m_dialog,*m_dialog_wait;
    
    void initScroll();
    void loadWithDict();
    void initKMDialogGui();
    void initDialogWait();
    void showDialogWait();
    void hideDialogWait();
   // void saveDict();
    void load();
    
   // void initBonus(int s1,int s2,int s3);
    ShopBomb* getBomb(const unsigned int &_type);
    void updateValueScroll(CKObject *_obj);
    void updateVisibleActiveList(const int &_num);
    void updateTotalScoreLabel();
    bool alignForLastObject();
    bool updatePositionDownObject(float _value);
    void updateComboBomb(CKObject *_obj);
    void menuCall(CCNode * _sender,void *_value);
    
    void updateMultiCoins(bool _start = true);
    void updateAdsRemove();
    void calcScrollCoinsSize();
    void callBuyIAP(CCNode * _sender,void *_value);
    void callbackIAP(CCNode * _sender,void *_value);
    void showBuyObjectInfo(CKObject *_object,bool _anim = true);
    void hideBuyObjectInfo(CKObject *_object);
    void hideNode(CCNode *_obj);
   
    std::vector<CKObject*>::iterator& findObjectInShop(CKObject *_object);
    void callFilter(CCNode* _sender,void *_value);
    
    void callHint(CCNode* _sender,void *_value);
    void callFilterCoins(CCNode* _sender,void *_value);
    void callDialog(CCNode *_sender,void *_value);
    
    void showDialogBuy();
    void returnScrollPosEndTouch();
    void scrollCombo(CCPoint &_p);
    void update(const float &dt);
    void timeDisableKM();
    void schedureNextShow(const float &dt);
    void initScrollBomb();
    void initScrollCoins();
    void setCoinsBuyValue(CKObject *_obj,int _count,float _price);
    void setPlaneSkinValue(CKObject *_obj,const char * _name,float _price);
    void setMultipliersValue(CKObject *_obj,int count,float _price);
    void setRemoveAdsPrice(CKObject *_obj,float _price);
    void setKidsModePrice(CKObject *_obj,float _price);
    
    void setComboBoxValue(CKObject *_element,const char *_text,const int &value,bool _free);
    void disableKidsMode();
    void enableKidsMode();
    void initGUI();
    void buyBomb(void *_d);
    void touchEndBombsScroll();
    void callShowCoint(void *_d);
    void callCoins(CCNode *_node,void *_data);
    void initDiscount(const unsigned char _type);
    void fillPriceList(unsigned char _type);
    
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesCancelled(CCSet *pTouches, CCEvent *pEvent){ccTouchesEnded(pTouches, pEvent);};

};
#endif /* defined(__CandyKadze__CKShop__) */
