//
//  CKGameSceneBase.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 05.07.13.
//
//

#include "CKGameSceneBase.h"
#include "CKHelper.h"
#include "CKFileOptions.h"
#include "CKAudioMgr.h"

#pragma mark - CK_AUDIO_OBJECT
void CKAudioObject::initAudio(int _previour_scene)
{
    if(_previour_scene != CK::SCENE_GAME)
    {
        CKAudioMng::Instance().removeAllEffect();
        
        CCLOG("CKAudioObject::initAudio load world sound");
        CKAudioMng::Instance().loadWorldSound(CKHelper::Instance().getWorld().c_str());
        CKAudioMng::Instance().preloadWorldSound();        
//        if(CKFileOptions::Instance().isPlayMusic())
//        {
//            CKAudioMng::Instance().playBackground("bg",0.0);
//            CKAudioMng::Instance().fadeBgToVolume(1.0);
//        }
    }
    else
    {
        //Issue #171
        if(CKFileOptions::Instance().isPlayMusic())
        {
            CKAudioMng::Instance().playBackground("bg",0.0);
            CKAudioMng::Instance().fadeBgToVolume(1.0);
        }
    }
}
void CKAudioObject::pauseAudio(bool _anim)
{
    if(_anim)
    {
        CKAudioMng::Instance().pauseAllEffect();
        
        if(CKFileOptions::Instance().isPlayMusic())
        {
            CKAudioMng::Instance().fadeBgToVolume(CK::SOUND_FADE_VALUE);
        };
    }
    else
    {
        CKAudioMng::Instance().pauseAllEffect();
        CKAudioMng::Instance().pauseBackground();
        CKAudioMng::Instance().setVolumeBgd(CK::SOUND_FADE_VALUE);
    };
}
void CKAudioObject::resumeAudio()
{
    if(CKFileOptions::Instance().isPlayEffect())
    {
        CKAudioMng::Instance().resumeAllEffect();
        CKAudioMng::Instance().stopEffect("plane_fly");
        CKAudioMng::Instance().playEffect("plane_fly");
    };
    if(CKFileOptions::Instance().isPlayMusic())
    {
        CKAudioMng::Instance().fadeBgToVolume(1.0);
    };
}
void CKAudioObject::stopMusic()
{
    
};

void CKAudioObject::playMusic()
{
    
};

void CKAudioObject::changeMusic()
{
    bool sound = !CKFileOptions::Instance().isPlayMusic();
    
    CKFileOptions::Instance().setPlayMusic(sound);
    if(sound)
    {
        if(!CKAudioMng::Instance().isPlayBackground())
            CKAudioMng::Instance().playBackground("bg");
        CKAudioMng::Instance().fadeBgToVolume(CK::SOUND_FADE_VALUE);
    }
    else
    {
        CKAudioMng::Instance().pauseBackground();
    }
};


#pragma mark - GAMESCENE
using namespace CK;
CKGameSceneBase::CKGameSceneBase()
{
    CCLOG("CKGameSceneBase::CKGameSceneBase");
    count_accuvate_shots = 0;
    BOX_SIZE = CKHelper::Instance().BOX_SIZE;
}
CKGameSceneBase::~CKGameSceneBase()
{
    CCLOG("CKGameSceneBase::~CKGameSceneBase");
};

void CKGameSceneBase::incLevelWorld()
{
    if(CKFileOptions::Instance().isKidsModeEnabled())
    {
        if(CKHelper::Instance().lvl_number < COUNT_LEVEL_IN_WORLD)
        {
            if(CKFileInfo::Instance().getCurentWorld()->open_lvl > CKHelper::Instance().lvl_number)
            {
                CKHelper::Instance().lvl_number++;
            }
        }
    }
    else
    {
        if(CKHelper::Instance().lvl_number < COUNT_LEVEL_IN_WORLD)
        {
            CKHelper::Instance().lvl_number++;
        }
    }
    
    if(CKHelper::Instance().lvl_number == COUNT_LEVEL_IN_WORLD)
    {
        CKHelper::Instance().lvl_number = 0;
        CKHelper::Instance().lvl_speed++;
        if(CKHelper::Instance().lvl_speed >= COUNT_DIFFICULT)
            CKHelper::Instance().lvl_speed = 0;
    };
    
    CKFileInfo::Instance().setCurentOpenLevel(CKHelper::Instance().lvl_number);
};
void CKGameSceneBase::createLabelScorePool(CCNode *_node)
{
    for(int i = 0; i < 10; i++ )
    {
        CCLabelTTF *tmp = CCLabelTTF::create("0", font_name.c_str(), BOX_SIZE/2);
        label_pool.push_back(tmp);
        tmp->setVisible(false);
        tmp->setColor((ccColor3B){220,255,20});
        _node->addChild(tmp,zOrder_LabelInfo);
    };
};

void CKGameSceneBase::hideNode(CCNode *_sender)
{
    _sender->setVisible(false);
};

void CKGameSceneBase::showLabelScore(const CCPoint& _pos,int _score,SEL_CallFuncN _selector)
{
    if(CKFileOptions::Instance().isKidsModeEnabled())
        return;
    label_pool_it = label_pool.begin();
    while (label_pool_it != label_pool.end()) {
        if(!(*label_pool_it)->isVisible())
        {
            (*label_pool_it)->setPosition(_pos);
            (*label_pool_it)->setVisible(true);
            (*label_pool_it)->setScale(0.0);
            (*label_pool_it)->setOpacity(255);
            CCFiniteTimeAction *move = CCMoveTo::create(0.7/float(CKHelper::Instance().lvl_speed+1), ccp((*label_pool_it)->getPositionX(),(*label_pool_it)->getPositionY() + BOX_SIZE));
            CCFiniteTimeAction *scale = CCScaleTo::create(0.9/float(CKHelper::Instance().lvl_speed+1), 1.0);
            CCFiniteTimeAction *swap = CCSpawn::create(move,scale, NULL);
            CCFiniteTimeAction *fade = CCFadeOut::create(0.4/float(CKHelper::Instance().lvl_speed+1));
            CCFiniteTimeAction *func = CCCallFuncN::create((*label_pool_it), callfuncN_selector(CKGameSceneBase::hideNode));
            CCAction *seq = CCSequence::create(swap,fade,func,NULL);
            (*label_pool_it)->runAction(seq);
            
            char str[NAME_MAX];
            sprintf(str,"+%d",_score);
            (*label_pool_it)->setString(str);
            switch (MIN(6,0/*m_bombs->getAccuracy()*/)) {
                case 2:
                    (*label_pool_it)->setColor((ccColor3B){0,133,0});
                    break;
                case 3:
                    (*label_pool_it)->setColor((ccColor3B){236,252,0});
                    break;
                case 4:
                    (*label_pool_it)->setColor((ccColor3B){255,188,0});
                    break;
                case 5:
                    (*label_pool_it)->setColor((ccColor3B){255,116,0});
                    break;
                case 6:
                    count_accuvate_shots++;
                    (*label_pool_it)->setColor((ccColor3B){255,0,0});
                    break;
                default:
                    (*label_pool_it)->setColor((ccColor3B){0,133,0});
                    break;
            };
            
            return;
        }
        label_pool_it++;
    };
};