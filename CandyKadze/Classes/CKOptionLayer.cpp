//
//  CKOptionLayer.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 13.06.13.
//
//

#include "CKOptionLayer.h"
#include "CKLoaderGUI.h"
#include "CKAudioMgr.h"
#include "CKMainMenuScene.h"
#include "CKLanguage.h"
#include "StatisticLog.h"
#include "CKTwitterHelper.h"
#include "ICloudHelper.h"

#pragma mark - OPTION LAYER
CKOptionLayer::~CKOptionLayer()
{
    CCLOG("~CKOptionLayer");
    CC_SAFE_DELETE(m_gui);
    CC_SAFE_DELETE(m_dialog);
    CC_SAFE_DELETE(m_dialogkm_on);
    CC_SAFE_DELETE(m_dialog_wait);
    CC_SAFE_DELETE(m_info);
    
    
    //m_info_rtt->release();
    //group_label_info->release();
    
    removeAllChildrenWithCleanup(true);
    ObjCCalls::setCallBack(NULL,NULL);
};

CKOptionLayer::CKOptionLayer()
{
    CK::StatisticLog::Instance().setNewPage(CK::Page_Options);
    can_resetgame = false;
    m_gui = NULL;
    m_dialogkm_on = NULL;
    m_dialog_wait = NULL;
    m_info = NULL;
   // ObjCCalls::isActiveConnection();
    win_size = CCDirector::sharedDirector()->getWinSize();
    
    initGui();
    
    setTouchEnabled(true);
    
    num_language = 0;
    for(int i = 0;i < CK::COUNT_LANGUAGE;i++)
    {
        CCLOG("CKFileOptions::Instance().getLanguage() %s %s",CKFileOptions::Instance().getLanguage(),CK::language_str[i]);
        
        std::string str_lang = CKFileOptions::Instance().getLanguage();
        if(strcmp(CKFileOptions::Instance().getLanguage(), "00") == 0)
        {
            str_lang = ObjCCalls::getDeviceLang();
        };
        
        str_lang = CKLanguage::Instance().langEqual(str_lang.c_str());
        
        if(strcmp(CK::language_str[i], str_lang.c_str()) == 0)
        {
            num_language = i;
            break;
        };
    }
    
    CKObject* _node = m_gui->getChildByTag(CK::Action_Icloud);
    if(_node)
    {
        _node->getChildByTag(3)->setVisible(!CKFileOptions::Instance().isEnableIcloud());
    }
    
    _node = m_gui->getChildByTag(CK::Action_Music);
    if(_node)
    {
        if(_node->getType() == CK::GuiRadioBtn)
        {
            _node->setStateChild(int(!CKFileOptions::Instance().isPlayMusic()));
        };
    }
    
    _node = m_gui->getChildByTag(CK::Action_Effect);
    if(_node)
    {
        if(_node->getType() == CK::GuiRadioBtn)
        {
            _node->setStateChild(int(!CKFileOptions::Instance().isPlayEffect()));
        };
    };
    _node = m_gui->getChildByTag(CK::Action_RemoveAds);
    if(_node)
    {
        _node->getChildByTag(3)->setVisible(!CKFileOptions::Instance().isBannerShow());
    }
    
    _node = m_gui->getChildByTag(CK::Action_Language);
    if(_node)
    {
        _node->getChildByTag(16)->setStateImage(num_language);
    };
    
    _node = m_gui->getChildByTag(CK::Action_EveryPlay_Show);
    if(_node)
    {
        _node->getChildByTag(3)->setVisible(ObjCCalls::isLowCPU()&&!CKFileOptions::Instance().isEveryPlayShow());
    };
    
   // if(ObjCCalls::IAPHelper_isbuy("com.cc.kids_mode"))
    {
        CKObject* _node = m_gui->getChildByTag(CK::Action_KidsMode);
        if(_node)
        {
            _node->getChildByTag(3)->setVisible(!CKFileOptions::Instance().isKidsModeEnabled());
        };
    }
    
    m_dialog = new CKGUI();
    m_dialog->create(CCNode::create());
    m_dialog->load(CK::loadFile("c_ConfirmationReset.plist").c_str());
    m_dialog->setTouchEnabled(true);
    m_dialog->setTarget(this);
    m_dialog->addFuncToMap("callDialog", callfuncND_selector(CKOptionLayer::callDialog));
    m_dialog->get()->setVisible(false);
    addChild(m_dialog->get(),99);
    
    initKMDialog();
    initDialogWait();
    initGuiInfo();
    
    runStartAnimation();
    
    scheduleUpdate();
};

void CKOptionLayer::initKMDialog()
{
    m_dialogkm_on = new CKGUI();
    m_dialogkm_on->create(CCNode::create());
    m_dialogkm_on->load(CK::loadFile("c_ModalWindowTurnONKM.plist").c_str());
    m_dialogkm_on->setTouchEnabled(true);
    m_dialogkm_on->setTarget(this);
    m_dialogkm_on->addFuncToMap("callDialog", callfuncND_selector(CKOptionLayer::callKM));
    m_dialogkm_on->get()->setVisible(false);
    addChild(m_dialogkm_on->get(),99);
};

void CKOptionLayer::initGuiInfo()
{
    m_info = new CKGUI;
    m_info->create(CCNode::create());
    m_info->load(CK::loadFile("c_Info.plist").c_str());
    m_info->setTarget(this);
    m_info->setTouchEnabled(true);
    m_info->get()->setVisible(false);
    addChild(m_info->get(),99);
    m_info->addFuncToMap("callInfo", callfuncND_selector(CKOptionLayer::closeInfo));
   // m_info->get()->setPosition(ccp(win_size.width/2,win_size.height/2));
    CCNode *mask = m_info->getChildByTag(1)->getNode();
    mask->setVisible(false);
    
    group_label_info = m_info->getChildByTag(10)->getNode();
    group_label_info->setVisible(false);

    
    m_info_rtt = CCRenderTexture::create(mask->getContentSize().width*mask->getScaleX(), mask->getContentSize().height*mask->getScaleY());
    
    m_info_rtt->setPosition(mask->getPosition());
    m_info->get()->addChild(m_info_rtt);
    m_info->get()->setPositionY(-win_size.height);
    
    if(win_size.width == 568)
    {
        m_info->get()->setPositionX(44);
    };
    if(win_size.width == 1024)
    {
        info_top_posY = -84;
        info_bootom_posY = 2115;
    }else
    {
        info_top_posY = -32;
        info_bootom_posY = 880;
    };
    back_info = CCSprite::createWithSpriteFrameName("grey_mask.png");
    back_info->setScaleX(win_size.width/back_info->getContentSize().width);
    back_info->setScaleY(win_size.height/back_info->getContentSize().height);
    back_info->setPosition(ccp(win_size.width/2,win_size.height/2));
    back_info->setVisible(false);
    addChild(back_info,98);
    
};

void CKOptionLayer::initDialogWait()
{
    m_dialog_wait = new CKGUI;
    m_dialog_wait->create(CCNode::create());
    m_dialog_wait->load(CK::loadFile("c_ModalWindowWait.plist").c_str());
    m_dialog_wait->setTarget(this);
    m_dialog_wait->get()->setVisible(false);
    m_dialog_wait->get()->setTag(0);
    m_dialog_wait->getChildByTag(3)->setText(LCZ("Connection..."));
    addChild(m_dialog_wait->get(),99);
    m_dialog_wait->get()->setPosition(ccp(win_size.width/2,win_size.height/2));
    if(win_size.width == 568)
    {
        m_dialog_wait->get()->setPositionX(win_size.width/2 + 44);
    };
    m_dialog_wait->getChildByTag(4)->getNode()->runAction(CCRepeatForever::create(CCRotateBy::create(1.0, 360)));
}
void CKOptionLayer::showDialogWait(bool _loading)
{
    //unschedule(schedule_selector(CKOptionLayer::hideDialogWait));
    //schedule(schedule_selector(CKOptionLayer::hideDialogWait),300.0,false,300.0);
    if(_loading)
    {
        m_dialog_wait->getChildByTag(3)->setText(LCZ("Loading..."));
    }
    else
    {
        m_dialog_wait->getChildByTag(3)->setText(LCZ("Connection..."));
    }
        
    m_dialog_wait->get()->setVisible(true);
    //m_dialog_wait->get()->setTag(m_dialog_wait->get()->getTag() + 1);
    this->setTouchEnabled(false);
    CCLOG("CKOptionLayer::showDialogWait %d",m_dialog_wait->get()->getTag());
}
void CKOptionLayer::hideDialogWait()
{
    // return;
    //m_dialog_wait->get()->setTag(m_dialog_wait->get()->getTag() - 1);
    //if(m_dialog_wait->get()->getTag() == 0)
    {
        unschedule(schedule_selector(CKOptionLayer::hideDialogWait));
        m_dialog_wait->get()->setVisible(false);
        this->setTouchEnabled(true);
    };
    CCLOG(" CKOptionLayer::hideDialogWait %d",m_dialog_wait->get()->getTag());
}
CCScene* CKOptionLayer::scene()
{
    CCScene *scene = CCScene::create();
    CCLayer* layer = new CKOptionLayer();
    
    if(layer->init())
    {
        scene->addChild(layer);
        layer->setTag(CK::LAYER_OPTION);
        layer->setTouchEnabled(true);
        layer->release();
    };
    CKHelper::Instance().compliteLoadScene(scene);
    return scene;
};

void CKOptionLayer::initGui()
{
    m_gui = new CKGUI();
    m_gui->create(CCNode::create());
    m_gui->load(CK::loadFile("c_Options.plist",true).c_str());
    m_gui->setTouchEnabled(true);
    m_gui->addFuncToMap("menuCall", callfuncND_selector(CKOptionLayer::menuCall));
    m_gui->setTarget(this);
    addChild(m_gui->get(),1);
};

#pragma mark - OPERATION
void CKOptionLayer::resetGame()
{
    if(can_resetgame)
    {
        CKAudioMng::Instance().playEffect("complite_reset_game");
        CCLOG("CKOptionLayer::resetGame");
        CKFileOptions::Instance().clearFile();
        CKObject* _node = m_gui->getChildByTag(CK::Action_Icloud);
        if(_node)
        {
            CKFileOptions::Instance().enableIcloud(false);
            _node->getChildByTag(3)->setVisible(true);
        };
        ICloudHelper::Instance().deleteIcloudDocument();
    };
};
void CKOptionLayer::loadIcloud(CCNode *_sender)
{
    CKAudioMng::Instance().playEffect("button_pressed");
    if(!_sender)
        return;
    CKObject* _node = m_gui->getChildByTag(CK::Action_Icloud);
    if(_node)
    {
        CKFileOptions::Instance().enableIcloud(true);
        ICloudHelper::Instance().loadWithIcloud();
        _node->getChildByTag(3)->setVisible(false);
    }
};

void CKOptionLayer::enableEveryplay(CCNode *_sender)
{
    CK::StatisticLog::Instance().setNewPage(CK::Page_Options);
    CKAudioMng::Instance().playEffect("button_pressed");
    if(!_sender)
        return;
    CKObject* _node = m_gui->getChildByTag(CK::Action_EveryPlay_Show);
    if(_node)
    {
        CKFileOptions::Instance().setEveryPlayShow(true);
        _node->getChildByTag(3)->setVisible(false);
    }
};
void CKOptionLayer::callKM(CCNode *_sender,void *_value)
{
    CKAudioMng::Instance().playEffect("button_pressed");
    switch(*static_cast<int*>(_value))
    {
        case CK::Action_Yes:
        {
            CKObject* _node = m_gui->getChildByTag(CK::Action_KidsMode);
            if(_node)
            {
                _node->getChildByTag(3)->setVisible(!_node->getChildByTag(3)->isVisible());
                CKFileOptions::Instance().setKidsMode(!_node->getChildByTag(3)->isVisible());
                CKFileOptions::Instance().save();
                CCDirector::sharedDirector()->replaceScene(CKMainMenuScene::scene());
            }
        };
        case CK::Action_No:
        case CK::Action_Close:
            m_dialogkm_on->get()->setVisible(false);
            break;
    };
    CK::StatisticLog::Instance().setNewPage(CK::Page_Options);
};

void CKOptionLayer::callDialog(CCNode *_sender,void *_value)
{
    switch(*static_cast<int*>(_value))
    {
        case CK::Action_Yes:
            resetGame();
        case CK::Action_No:
        case CK::Action_Close:
            m_dialog->get()->setVisible(false);
            CKAudioMng::Instance().playEffect("button_pressed");
            break;
    };
};

void CKOptionLayer::runStartAnimation()
{
    float time_scale = 0.1;
    float time_delay = 0.0;
    float time_koef = 0.09;
    
    CKAudioMng::Instance().playEffect("show_option_button");
    
    CCNode * tmp = m_gui->getChildByTag(CK::Action_Facebook)->getNode();
    tmp->setScale(0.0);
    CCAction *action = CCSequence::create(CCDelayTime::create(time_delay),CCScaleTo::create(time_scale, 1.0),NULL);
    time_delay += time_koef;
    tmp->runAction(action);
    
    tmp = m_gui->getChildByTag(CK::Action_Tweet)->getNode();
    tmp->setScale(0.0);
    action = CCSequence::create(CCDelayTime::create(time_delay),CCScaleTo::create(time_scale, 1.0),NULL);
    time_delay += time_koef;
    tmp->runAction(action);
    
    tmp = m_gui->getChildByTag(CK::Action_EveryPlay_Show)->getNode();
    tmp->setScale(0.0);
    action = CCSequence::create(CCDelayTime::create(time_delay),CCScaleTo::create(time_scale, 1.0),NULL);
    time_delay += time_koef;
    tmp->runAction(action);
    
    tmp = m_gui->getChildByTag(CK::Action_GameCenter)->getNode();
    tmp->setScale(0.0);
    action = CCSequence::create(CCDelayTime::create(time_delay),CCScaleTo::create(time_scale, 1.0),NULL);
    time_delay += time_koef;
    tmp->runAction(action);
    
    tmp = m_gui->getChildByTag(CK::Action_Hint)->getNode();
    tmp->setScale(0.0);
    action = CCSequence::create(CCDelayTime::create(time_delay),CCScaleTo::create(time_scale, 1.0),NULL);
    time_delay += time_koef;
    tmp->runAction(action);
    
    tmp = m_gui->getChildByTag(CK::Action_Chillibites)->getNode();
    tmp->setScale(0.0);
    action = CCSequence::create(CCDelayTime::create(time_delay),CCScaleTo::create(time_scale, 1.0),NULL);
    time_delay += time_koef;
    tmp->runAction(action);
    
    tmp = m_gui->getChildByTag(CK::Action_Info)->getNode();
    tmp->setScale(0.0);
    action = CCSequence::create(CCDelayTime::create(time_delay),CCScaleTo::create(time_scale, 1.0),NULL);
    time_delay += time_koef;
    tmp->runAction(action);
    
    tmp = m_gui->getChildByTag(CK::Action_Facebook)->getNode();
    tmp->setScale(0.0);
    action = CCSequence::create(CCDelayTime::create(time_delay),CCScaleTo::create(time_scale, 1.0),NULL);
    time_delay += time_koef;
    tmp->runAction(action);
};

void CKOptionLayer::callbackIAP(CCNode * _sender,void *_value)
{
    char *str = (char *)_value;
    CCLOG("CKOptionLayer::callbackIAP %s,%s",(char *)_value,strstr(str, "com.cc.coins.buy"));
    hideDialogWait();
    
    if(click_buyIapp == CK::Action_Buy_Restore)
        CK::StatisticLog::Instance().setBuyinApp("restore", CKFileInfo::Instance().getTotalScore());
    else
        CK::StatisticLog::Instance().setBuyinApp(str, CKFileInfo::Instance().getTotalScore());
    
    
    if(strcmp(str, "com.cc.ads_removing") == 0)
    {
        CKObject* _node = m_gui->getChildByTag(CK::Action_RemoveAds);
        if(_node)
        {
            _node->getChildByTag(3)->setVisible(true);
        }
        CKFileOptions::Instance().setBannerShow(false);

    }else if(strcmp(str, "com.cc.kids_mode") == 0)
    {
        
        CKObject* _node = m_gui->getChildByTag(CK::Action_KidsMode);
        if(_node)
        {
            _node->getChildByTag(3)->setVisible(!_node->getChildByTag(3)->isVisible());
            CKFileOptions::Instance().setKidsMode(!_node->getChildByTag(3)->isVisible());
           // CKFileOptions::Instance().save();
        }
    };
};
void CKOptionLayer::closeInfo(CCNode *_sender,void *_value)
{
    CK::StatisticLog::Instance().setNewPage(CK::Page_Options);
    
    m_info->get()->stopAllActions();
    CCAction *action = CCSequence::create(CCMoveBy::create(0.5, ccp(0,-win_size.height)),CCCallFuncN::create(m_info->get(), callfuncN_selector(CKOptionLayer::hideNode)),NULL);
    m_info->get()->runAction(action);
    CCAction *action2 = CCSequence::create(CCFadeOut::create(0.5),CCCallFuncN::create(back_info, callfuncN_selector(CKOptionLayer::hideNode)),NULL);
    back_info->runAction(action2);
    
};

void CKOptionLayer::hideNode(CCNode *_node)
{
    if(_node)
        _node->setVisible(false);
};

void CKOptionLayer::callbackLanguage(CCNode *_sender,void *data)
{

};

void CKOptionLayer::menuCall(CCNode* _sender,void *_value)
{
    CCLOG("_value %d node_tag %d" ,*static_cast<int*>(_value),_sender->getTag());
    
    click_buyIapp = *static_cast<int*>(_value);
    switch(*static_cast<int*>(_value))
    {
        case CK::Action_Back:
            CKAudioMng::Instance().playEffect("button_pressed");
            CKFileOptions::Instance().save();
            CKFileInfo::Instance().save();
            //CCDirector::sharedDirector()->replaceScene(CKMainMenuScene::scene());
            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(CK::SCENE_MAIN);
            break;
        case CK::Action_Info:
            CKAudioMng::Instance().playEffect("button_pressed");
            if(m_info)
            {
                CK::StatisticLog::Instance().setNewPage(CK::Page_Credits);
                group_label_info->stopAllActions();
                group_label_info->setPositionY(info_top_posY);
                float time = 1.0 - (group_label_info->getPositionY()-info_top_posY)/(info_bootom_posY - info_top_posY);
                CCLOG("time %f",time);
                group_label_info->runAction(CCSequence::create(CCDelayTime::create(2.5),CCMoveTo::create(time*20.0, ccp(group_label_info->getPositionX(),info_bootom_posY)),NULL));
                
                m_info->get()->setVisible(true);
                
                m_info->get()->runAction(CCEaseElasticOut::create(CCMoveBy::create(1.0, ccp(0,win_size.height))));
                
                back_info->setOpacity(0);
                back_info->setVisible(true);
                back_info->runAction(CCFadeIn::create(0.5));
                
            }
            break;
        case CK::Action_Chillibites:
            CKAudioMng::Instance().playEffect("button_pressed");
            CK::StatisticLog::Instance().setLink(CK::Link_ChilibiteMoreGames);
            if (ObjCCalls::isActiveConnection()) {
               ObjCCalls::showChartboost(""); 
            }
            else
            {
                ObjCCalls::goToURL(CHILLIBITES_SITE_URL);
            }
            break;
        case CK::Action_Language:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
           // break;
            showDialogWait(true);
            CCDirector::sharedDirector()->drawScene();
            CCDirector::sharedDirector()->pause();
            CKObject* _node = m_gui->getChildByTag(CK::Action_Language);
            char str[255];
            sprintf(str, "Cookies_%s.png",CK::language_str[num_language]);
            
            
            num_language++;
            if(num_language >= CK::COUNT_LANGUAGE)
            {
                num_language = 0;
            };
            if(_node)
            {
                _node->getChildByTag(16)->setStateImage(num_language);
            }
            CKFileOptions::Instance().setLanguage(CK::language_str[num_language]);
            m_gui->reloadText();
            m_dialog_wait->reloadText();
            CCTexture2D *texture = CCTextureCache::sharedTextureCache()->textureForKey(str);
            CCLOG("texture %s %d",str,(texture)?texture->retainCount():0);
//            CCTextureCache::sharedTextureCache()->removeTextureForKey(str);
            CCDirector::sharedDirector()->resume();
            hideDialogWait();
        }
            break;
        case CK::Action_KidsMode:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            if(m_dialogkm_on)
            {
                CK::StatisticLog::Instance().setNewPage(CK::Page_KM_ON);
                m_dialogkm_on->get()->setVisible(true);
            };
            break;
//            //if(ObjCCalls::IAPHelper_isbuy("com.cc.kids_mode"))
//            {
//                CKObject* _node = m_gui->getChildByTag(CK::Action_KidsMode);
//                if(_node)
//                {
//                    _node->getChildByTag(3)->setVisible(!_node->getChildByTag(3)->isVisible());
//                    CKFileOptions::Instance().setKidsMode(!_node->getChildByTag(3)->isVisible());
//                    CKFileOptions::Instance().save();
//                    CCDirector::sharedDirector()->replaceScene(CKMainMenuScene::scene());
//                }
//            }
        }
            break;
        case CK::Action_RemoveAds:
        {
            
            CKAudioMng::Instance().playEffect("button_pressed");
            ObjCCalls::setCallBack(this,callfuncND_selector(CKOptionLayer::callbackIAP));
            if(ObjCCalls::IAPHelper_isbuy("com.cc.ads_removing"))
            {
                CKObject* _node = m_gui->getChildByTag(CK::Action_RemoveAds);
                if(_node)
                {
                    _node->getChildByTag(3)->setVisible(true);
                }
                CKFileOptions::Instance().setBannerShow(false);
            }
            else
            {
                showDialogWait();
                
                ObjCCalls::IAPHelper_buy("com.cc.ads_removing");
            }
            
        }
            break;
        case CK::Action_Icloud:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            if(!CKFileOptions::Instance().isEnableIcloud())
            {
                CKHelper::Instance().getDialog()->setTarget(this);
                CKHelper::Instance().getDialog()->setConfirmText(LCZ("Do you want to enable syncronization?"));
                CKHelper::Instance().getDialog()->show(callfuncN_selector(CKOptionLayer::loadIcloud));
            }
            else
            {
               CKObject* _node = m_gui->getChildByTag(CK::Action_Icloud);
                if(_node)
                {
                    CKFileOptions::Instance().enableIcloud(false);
                    _node->getChildByTag(3)->setVisible(true);
                }
            };
        };
            break;
        case CK::Action_RestorePurchases:
            showDialogWait();
            ObjCCalls::setCallBack(this,callfuncND_selector(CKOptionLayer::callbackIAP));
            CKAudioMng::Instance().playEffect("button_pressed");
            ObjCCalls::IAPHelper_restore();
            break;
        case CK::Action_ResetGame:
            CKAudioMng::Instance().playEffect("reset_game");
            can_resetgame = false;
            m_dialog->get()->setVisible(true);
            break;
        case CK::Action_Facebook:
            CKAudioMng::Instance().playEffect("button_pressed");
            CK::StatisticLog::Instance().setLink(CK::Link_GamepageFacebook);
            ObjCCalls::goToFasebook();
            break;
        case CK::Action_Tweet:
            CKAudioMng::Instance().playEffect("button_pressed");
            CK::StatisticLog::Instance().setLink(CK::Link_GamepageTwitter);
            CKTwitterHelper::Instance().goToTwitter(TWEET_FOLLOW);
            break;
        case CK::Action_EveryPlay_Show:
            CKAudioMng::Instance().playEffect("button_pressed");
            if(ObjCCalls::isLowCPU())
            {
                if(!CKFileOptions::Instance().isEveryPlayShow())
                {
                    CK::StatisticLog::Instance().setNewPage(CK::Page_EveryplayOn);
                    CKHelper::Instance().getDialog()->setTarget(this);
                    CKHelper::Instance().getDialog()->setConfirmText(LCZ("Enable Everyplay ?"),LCZ("May cause lower performance on weak device"));
                    CKHelper::Instance().getDialog()->show(callfuncN_selector(CKOptionLayer::enableEveryplay));
                }
                else
                {
                    CK::StatisticLog::Instance().setLink(CK::Link_EveryplayVideo);
                    CKObject* _node = m_gui->getChildByTag(CK::Action_EveryPlay_Show);
                    if(_node)
                    {
                        CKFileOptions::Instance().setEveryPlayShow(false);
                        _node->getChildByTag(3)->setVisible(true);
                    }
                     
                }
               
            }
            else
            {
                ObjCCalls::everyplayShow();
            }
            break;
        case CK::Action_GameCenter:
            CKAudioMng::Instance().playEffect("button_pressed");
            ObjCCalls::showLeaderboard();
            break;
        case CK::Action_Music:
        {
            bool sound = !CKFileOptions::Instance().isPlayMusic();
            CKAudioMng::Instance().playEffect("button_pressed");
            CKFileOptions::Instance().setPlayMusic(sound);
            
            if(sound)
            {
                if(!CKAudioMng::Instance().isPlayBackground())
                {
                    CKAudioMng::Instance().playBackground("bg");
                };
                CKAudioMng::Instance().setVolumeBgd(0.0);
                CKAudioMng::Instance().fadeBgToVolume(1.0);
            }
            else
            {
                CKAudioMng::Instance().pauseBackground();
            }
            
            CKObject* _node = m_gui->getChildByTag(CK::Action_Music);
            if(_node)
            {
                if(_node->getType() == CK::GuiRadioBtn)
                {
                    _node->setStateChild(!sound);
                };
            }
        };
            break;
            
        case CK::Action_Effect:
        {
            bool sound = !CKFileOptions::Instance().isPlayEffect();
            CKFileOptions::Instance().setPlayEffect(sound);
            CKAudioMng::Instance().playEffect("button_pressed");
            
            CKObject* _node = m_gui->getChildByTag(CK::Action_Effect);
            if(_node)
            {
                if(_node->getType() == CK::GuiRadioBtn)
                {
                    _node->setStateChild(!sound);
                };
            }
        }
            break;
        case CK::Action_Hint:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            if(DISABLE_HINTS)
                break;
            
            CKHelper::Instance().getTutorial()->activeted(this,CK::SCENE_OPTION);
            CKHelper::Instance().getTutorial()->start(1,-1); //default tap
        };
            break;
        default:
            break;
            //CCAssert(0, "Uknow tag");
    };
    
};

void CKOptionLayer::update(float dt)
{
    if(m_info_rtt && m_info->get()->isVisible())
    {
      //  CCLOG("group_label_info %f",group_label_info->getPositionY());
        m_info_rtt->clear(0, 0, 0, 0);
        m_info_rtt->begin();
        group_label_info->setVisible(true);
        group_label_info->visit();
        group_label_info->setVisible(false);
        m_info_rtt->end();
    }
};

void CKOptionLayer::timeResetGame()
{
    can_resetgame = true;
    m_dialog->get()->setVisible(false);
    resetGame();
};

#pragma mark - UPDATE
void CKOptionLayer::ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{

    CCSetIterator it;
    CCTouch* touch;
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        
        if(m_dialogkm_on && m_dialogkm_on->get()->isVisible())
        {
            m_dialogkm_on->setTouchEnd(location);
            return;
        };
        
        if(m_info && m_info->get()->isVisible())
        {
            m_info->setTouchEnd(location);
            if(touch_info)
            {
                if(group_label_info->getPositionY() < info_top_posY)
                {
                    group_label_info->runAction(CCMoveTo::create(0.1, ccp(group_label_info->getPositionX(),info_top_posY)));
                }
                else if(group_label_info->getPositionY() > info_bootom_posY)
                {
                    group_label_info->runAction(CCMoveTo::create(0.1, ccp(group_label_info->getPositionX(),info_bootom_posY)));
                }
                else
                {
//                    float time = 1.0 - (group_label_info->getPositionY() - info_top_posY)/(info_bootom_posY - info_top_posY);
//                    CCLOG("time %f",time);
//                    group_label_info->runAction(CCMoveTo::create(time*10.0, ccp(group_label_info->getPositionX(),info_bootom_posY)));
                }
                touch_info = false;
            }
            return;
        };
        
        if(m_dialog && m_dialog->get()->isVisible())
        {
            unschedule(schedule_selector(CKOptionLayer::timeResetGame));
            m_dialog->setTouchEnd(location);
            return;
        };
        
        if(m_gui)
            m_gui->setTouchEnd(location);
    }
};

void CKOptionLayer::ccTouchesMoved(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{

    CCSetIterator it;
    CCTouch* touch;
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        
        if(m_info && m_info->get()->isVisible())
        {
            m_info->setTouchEnd(location);
            
            if(touch_info)
                group_label_info->setPositionY(group_label_info->getPositionY() - (last_pos.y - location.y));
            CCLOG("group_label_info %f",group_label_info->getPositionY());
            last_pos = location;
            return;
        };
        
        if(m_gui)
            m_gui->setTouchMove(location);

    }
};

void CKOptionLayer::ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{

    CCSetIterator it;
    CCTouch* touch;
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        
        if(m_dialogkm_on && m_dialogkm_on->get()->isVisible())
        {
            m_dialogkm_on->setTouchBegan(location);
            return;
        };
        
        touch_info = false;
        if(m_info && m_info->get()->isVisible())
        {
            last_pos = location;
            m_info->setTouchBegan(location);
            if(m_info->getChildByTag(1)->isPointInObject(location))
            {
                group_label_info->stopAllActions();
                touch_info = true;
            }
            return;
        };
        
        if(m_dialog && m_dialog->get()->isVisible())
        {
            m_dialog->setTouchBegan(location);
            if (m_dialog->getCurrnet() && m_dialog->getCurrnet()->getTag() == CK::Action_Yes) {
                schedule(schedule_selector(CKOptionLayer::timeResetGame),TIME_RESET_GAME,0, TIME_RESET_GAME);
            };
            return;
        };
        if(m_gui)
            m_gui->setTouchBegan(location);
        
    }
};


