//
//  CKGameScene90.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 01.11.12.
//
//

#include "CKGameScene90.h"
#include "CKStaticInfo.h"
#include "CKAudioMgr.h"
#include "CKAdBanner.h"
#include "CKLanguage.h"
#include "CKParseHelper.h"
#include "CKTwitterHelper.h"
#include "CKFacebookHelper.h"

#pragma  mark - WORLD90
#pragma mark  - BASE
CKGameScene90::~CKGameScene90()
{
    CCLOG("CKGameScene90::~CKGameScene90");
    ObjCCalls::everyplayStopRecording();
    CC_SAFE_DELETE(m_airplane);
    CC_SAFE_DELETE(m_bombs);
    CC_SAFE_DELETE(m_cube_array);
    CC_SAFE_DELETE(m_gui_pause);
    CC_SAFE_DELETE(m_gui_win);
    CC_SAFE_DELETE(m_gui_over);
    CC_SAFE_DELETE(m_share);
    ObjCCalls::hideBanner(true);
    CC_SAFE_DELETE(stars_spectrum);
    stars_spectrum = NULL;
    if(banner)
    {
        banner->removeFromParentAndCleanup(true);
        menu_banner->removeFromParentAndCleanup(true);
    }
    if(texture_banner)
    {
        CCTextureCache::sharedTextureCache()->removeTexture(texture_banner);
    };
};

CCScene* CKGameScene90::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    // add layer as a child to scene
    CCLayer* layer = new CKGameScene90();
    if(layer->init())
    {
        layer->setTouchEnabled(true);
        scene->addChild(layer);
        layer->setTag(20);
        layer->release();
        
    };
    CKHelper::Instance().compliteLoadScene(scene);
    return scene;
};

CKGameScene90::CKGameScene90()
{
    CK::StatisticLog::Instance().setNewPage(CK::Page_GameScene90);
    
    if(CKHelper::Instance().menu_number == CK::SCENE_GAME)
    {
        CKAudioMng::Instance().stopBackground();
    };
    
    timeval t;
    gettimeofday(&t,NULL);
    time_start = t.tv_sec;
    pause_time = 0;
    count_active_touch = 0;
    banner = NULL;
    m_cube_array = NULL;
    m_gui_pause = NULL;
    m_gui_win = NULL;
    m_gui_over = NULL;
    is_hiscore = false;
    stars_spectrum = NULL;
    texture_banner = NULL;
    
    m_airplane = NULL;
    m_bombs = NULL;
    m_cube_array = NULL;
    m_share = NULL;
    
    count_accuvate_shots = 0;
    
    is_game_win = false;
    
    is_posted_in_facebook = false;
    
    have_everyplay_record = CKFileOptions::Instance().isEveryPlayShow();
    add_score_koef = 0.2;

    BOX_SIZE = CKHelper::Instance().BOX_SIZE;
    font_name = "Agent Orange";
    is_pause = false;
    curent_score = 0;
    score_active = false;
    last_score = 0;

    win_size = CCDirector::sharedDirector()->getWinSize();
    
    panel_left = 0;
    if(win_size.width == 568)
    {
        panel_left = 44;
    };
    
    CKFileInfo::Instance().getLvlOpenWithWorld(CKHelper::Instance().getWorld(CKHelper::Instance().getWorldNumber()));//world90
    CKFileInfo::Instance().setLevel(CKHelper::Instance().lvl_number);
    
    load();
    
    CKHelper::Instance().menu_number = CK::SCENE_GAME;
   // ObjCCalls::FB_getUserInfoSL();
   // CKTwitterHelper::Instance().TweetgetInfo();
};

void CKGameScene90::load()
{

    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(CK::loadImage("a_World90.plist").c_str());
    
    initGUI();
    initGUIShare();
    
    initPauseMenu();
    
    label_score = CCLabelTTF::create("0", font_name.c_str(), BOX_SIZE/4);
    addChild(label_score, zOrder_LabelInfo);
    label_score->setPosition(ccp( win_size.width*0.1, win_size.height*0.8));
    if(CKFileOptions::Instance().isKidsModeEnabled())
        label_score->setVisible(false);
    
    char str_level[NAME_MAX];
    sprintf(str_level, "Level %d",CKHelper::Instance().lvl_number + 1);
    CCLabelTTF* label = CCLabelTTF::create(str_level, font_name.c_str(), BOX_SIZE/4);
    label->setPosition(ccp( win_size.width*0.1, win_size.height*0.85));
    //label->setColor(ccc3(255,240,20));
    addChild(label,zOrder_LabelInfo);
    

    m_bombs = new CKBomb90;
    m_cube_array = new CKCubeArrayBase;
    
    m_cube_array->setLayer(this);
    m_cube_array->createBatch(CK::loadImage("a_World90.png").c_str());
    m_cube_array->setPanelSize(panel_left);

    
    m_cube_array->setGroundHeight(ground_height);
    m_cube_array->parsingLevel("");
    
    //lvl cube =
    curent_world_id = CKHelper::Instance().getWorldNumber();
    current_level_id = CKHelper::Instance().lvl_number;
    int diff = int(CKHelper::Instance().lvl_number/4);
    int number = (CKHelper::Instance().lvl_number%4)*4;
    CCLOG("CKHelper::Instance().lvl_number %d %d",diff,number);
    m_cube_array->createLevel(diff,number);
   // m_cube_array->setPanelSize(panel_left);
    initAirpalne();
    //create bombs object
    
    m_bombs->setLayer(this);
    m_bombs->setSpeed(BOX_SIZE*5);
    m_bombs->setBombName(bomb_name);
    m_bombs->setHeight(ground_height);
    m_bombs->init();
    ((CKBombs *)m_bombs)->setCubeArray((CKCubeArray*)m_cube_array);

    initBGStartAndCloud();
    
    bg_mask90 = NULL;
    bg_mask90 = CCSprite::create("gray_mask.png");
    bg_mask90->setPosition(ccp(win_size.width/2,win_size.height/2));
    bg_mask90->setScaleX(win_size.width/bg_mask90->getContentSize().width);
    bg_mask90->setScaleY(win_size.height/bg_mask90->getContentSize().height);
    
    bg_mask90->setVisible(false);
    bg_mask90->setOpacity(0.0);
    addChild(bg_mask90,zOrder_Menu-1);
    
    initAudio(CKHelper::Instance().menu_number);

    if(m_airplane->getAirplaneHeight() <= SHOW_BANNER_AFTER_HEIGHT && CKFileOptions::Instance().isBannerShow() && !CKFileOptions::Instance().isKidsModeEnabled())
    {
        float k = float(rand()%100)/100.0f; //random persent
        CCLOG("Iad,Admob banner Showed %f %f activeConection %d",CKAdBanner::Instance().koef_showed,k,ObjCCalls::isActiveConnection());
        if(ObjCCalls::isActiveConnection())
        {
            if(CKAdBanner::Instance().koef_showed < k) //iad or adMob
            {
                CKAdBanner::Instance().setDisable(true);
                ObjCCalls::showBanner();
            }
            else // our banner
            {
                
                CKAdBanner::Instance().setDisable(false);
            }
        };
        
        if(!CKAdBanner::Instance().isDisable())
        {
            CKAdBanner::Instance().generated();
        }
        
        createBanner();
        
        pauseItem->setPosition(ccp(pauseItem->getPositionX(),pauseItem->getPositionY() - ((win_size.width == 1024)?90:32)));
    };
    
//    CKStaticFile::Instance().setStartInfo(ObjCCalls::getUserName(),0, CKHelper::Instance().lvl_number + 1, CKHelper::Instance().lvl_speed + 1,m_airplane->getAirplaneHeight());
//    CKStaticFile::Instance().setStartTime(time(0));
    

    unschedule( schedule_selector(CKGameScene90::updateLoading));
    this->setTouchEnabled(true);
    
    //send to Flurry static
    ObjCCalls::logPageView();
    
    ObjCCalls::sendEventFlurry("lvl_start",false,"num_airplane_speed=%d,num_playing_world=%d,num_playing_lvl=%d",int(m_airplane->getSpeed()*100),(CKHelper::Instance().getWorldNumber() + 1),(CKHelper::Instance().lvl_number + 1)*(CKHelper::Instance().getWorldNumber()+1));
    
    //GA
    ObjCCalls::logPageView(true,"/StartGame90x");
    ObjCCalls::sendCustomGA(5,"num_playing_speed",(CKHelper::Instance().lvl_speed + 1)*100);
    ObjCCalls::sendCustomGA(6,"num_playing_world",CKHelper::Instance().getWorldNumber() + 1);
    ObjCCalls::sendCustomGA(7,"num_playing_lvl",(CKHelper::Instance().lvl_number + 1)*(CKHelper::Instance().getWorldNumber()+1));
    
    scheduleUpdate();
#if ENABLE_EVERYPLAY
    if(CKFileOptions::Instance().isEveryPlayShow())
        ObjCCalls::everyplayStartCapture();
#endif
    CKAudioMng::Instance().playEffect("plane_fly");
};

void CKGameScene90::initBGStartAndCloud()
{
    stars_spectrum = new CKStart90;
    stars_spectrum->setLayer(this);
    stars_spectrum->addStartName("staryellow_90.png");
    stars_spectrum->addStartName("starw_90.png");
    stars_spectrum->addCloudName("cloud_90_1.png");
    stars_spectrum->addCloudName("cloud_90.png");
    stars_spectrum->setMinHeight(win_size.height*0.65);
    stars_spectrum->createStarts(5, 0.6);
    stars_spectrum->createCloud(3, 0.4, 0.0);
};

void CKGameScene90::updateLoading(const float &_dt)
{
    m_loading.getChildByTag(1)->getNode()->setRotation(m_loading.getChildByTag(1)->getNode()->getRotation() + 1);
};

void CKGameScene90::functAsunc(CKGameScene90 * _sender)
{
//    curen_load--;
//    CCLOG("functAsunc:: curen_load %d",curen_load);
//    if(curen_load == 0)
//    {
//        // _sender->setTouchEnabled(true);
//        
//        //   load();
//    };
};

void CKGameScene90::draw()
{
    //    CCLayer::draw();
};

#pragma mark  - INIT
void CKGameScene90::initAirpalne()
{
    //create new airlpane object
    m_airplane = new CKAirplane90;
    m_airplane->setLayer(this);
    m_airplane->setBombs(m_bombs);
    
    m_airplane->create(airplane_name.c_str());
    m_airplane->setCubeArray(m_cube_array);
    m_airplane->setGroundHeight(ground_height);
    
    if(CKHelper::Instance().lvl_speed == 0 && CKHelper::Instance().lvl_number < 4)
    {
        if(CKHelper::Instance().lvl_number < 2)//lvl 1 and lvl 2
        {
            m_airplane->setSlowFall(-3,3,2,true);
        }
        else //lvl 3 and lvl 4
            m_airplane->setSlowFall(-1,2,2,true);
    }
    else
        m_airplane->setSlowFall(0,0,0); // disable options slow fall
    
    m_airplane->setBaseSpeed(m_airplane->getSpeed());
    
    char str[NAME_MAX];
    sprintf(str, "L#%d D#%d S#%.1f",CKHelper::Instance().lvl_number + 1,CKHelper::Instance().lvl_speed + 1,m_airplane->getSpeed()/BOX_SIZE);
    label_info = CCLabelTTF::create(str, "Arial", BOX_SIZE/3);
    addChild(label_info, 15);
    label_info->setPosition(ccp( win_size.width*0.9, win_size.height*0.8));
    label_info->setColor(ccc3(255,255,255));
    label_info->setVisible(false);
};

void CKGameScene90::initGuiWin()
{
    m_gui_win = new CKGUI();
    m_gui_win->create(CCNode::create());
    if(CKFileOptions::Instance().isKidsModeEnabled())
    {
        m_gui_win->load(loadFile("c_LevelComplete90KM.plist").c_str());
    }
    else
    {
        m_gui_win->load(loadFile("c_LevelComplete90.plist").c_str());
        m_gui_win->getChildByTag(53)->setVisible(false);
    }
    m_gui_win->setTarget(this);
    m_gui_win->addFuncToMap("menuCall", callfuncND_selector(CKGameScene90::menuCall));
    m_gui_win->setTouchEnabled(true);
    m_gui_win->get()->setVisible(false);
    m_gui_win->get()->setPosition(ccp(panel_left,0));
    addChild(m_gui_win->get(),zOrder_Menu);
};

void CKGameScene90::initGuiOver()
{
    m_gui_over = new CKGUI();
    m_gui_over->create(CCNode::create());
    m_gui_over->load(loadFile("c_LevelFailed90.plist").c_str());
    m_gui_over->setTarget(this);
    m_gui_over->addFuncToMap("menuCall", callfuncND_selector(CKGameScene90::menuCall));
    m_gui_over->setTouchEnabled(true);
    m_gui_over->get()->setVisible(false);
    m_gui_over->get()->setPosition(ccp(panel_left,0));
    addChild(m_gui_over->get(),zOrder_Menu);
    
    
    m_gui_over->getChildByTag(CK::Action_EveryPlay_Play)->getChildByTag(3)->setVisible(!CKFileOptions::Instance().isEveryPlayShow());
};

void CKGameScene90::initPauseMenu()
{
    //
    m_gui_pause = new CKGUI();
    m_gui_pause->create(CCNode::create());
    m_gui_pause->load(loadFile("c_Pause90.plist").c_str());
    
    m_gui_pause->setTarget(this);
    m_gui_pause->addFuncToMap("menuCall", callfuncND_selector(CKGameScene90::menuCall));
    m_gui_pause->setTouchEnabled(true);
    
    CKObject* _node = m_gui_pause->getChildByTag(Action_Music);
    if(_node)
    {
        if(_node->getType() == CK::GuiRadioBtn)
        {
            _node->setStateChild(!CKFileOptions::Instance().isPlayMusic());
        };
    }
    _node = m_gui_pause->getChildByTag(Action_Effect);
    if(_node)
    {
        if(_node->getType() == CK::GuiRadioBtn)
        {
            _node->setStateChild(int(!CKFileOptions::Instance().isPlayEffect()));
        };
    }
    m_gui_pause->get()->setPosition(ccp(-win_size.width*1.0 + panel_left,win_size.height*0.0));
    m_gui_pause->get()->setVisible(false);
   // m_gui_pause->getChildByTag(1)->setTouch(true);
    addChild(m_gui_pause->get(),zOrder_Menu);

    initGuiWin();    
    initGuiOver();
};
void CKGameScene90::initGUIShare()
{
    m_share = new CKGUI;
    m_share->create(CCNode::create());
    m_share->load(loadFile("c_LevelComplete90Share.plist",false).c_str());
    m_share->get()->setPosition(ccp(win_size.width/2,win_size.height/2));
    m_share->setTarget(this);
    // m_share->addFuncToMap("menuCall", callfuncND_selector(CKMainMenuScene::menuCall));
    m_share->addFuncToMap("shareCall", callfuncND_selector(CKGameScene90::shareCall));
    m_share->setTouchEnabled(false);
    m_share->get()->setVisible(false);
    CCTextureCache::sharedTextureCache()->textureForKey(loadImage("a_World90.png").c_str())->setAliasTexParameters();
    addChild(m_share->get(),30);
    
    m_share->getChildByTag(CK::Action_EveryPlay_Play)->getChildByTag(3)->setVisible(!CKFileOptions::Instance().isEveryPlayShow());
};

void CKGameScene90::initGUI()
{
    CCLOG("start with plist world %s",CKHelper::Instance().getWorld().c_str());
    CCDictionary *dict = CCDictionary::createWithContentsOfFile("world90.plist");
    
    CCDictionary * dict_meta = (CCDictionary *)dict->objectForKey("meta");
    
    airplane_name = dictStr(dict_meta, "airplane");
    bomb_name = dictStr(dict_meta, "bomb");
   
    
    CCSprite* bg = CCSprite::createWithSpriteFrameName("fon_90.png");// (loadImage(dictStr(dict_meta, "background")).c_str());
    
    if(bg)
    {
        bg->setPosition(ccp(win_size.width/2, win_size.height/2));
        addChild(bg,0);
    };
    CCSprite* ground;
    if(win_size.width != 1024)
       ground = CCSprite::createWithSpriteFrameName("ground_90_568h.png");
    else
        ground = CCSprite::createWithSpriteFrameName("ground_90.png");
    
    ground->setPosition(ccp(win_size.width/2,ground->getContentSize().height/2));
    //  ground->setVisible(false);
    addChild(ground,zOrder_Plane);
    ground_height = ground->getContentSize().height;
    font_name = "Pixel Arial 11";
    
    CCMenu *itemMenu = CCMenu::create(NULL);
    dict->release();
    
    CCSprite* button_pause = CCSprite::createWithSpriteFrameName("BtnPause90.png");
    CCSprite* button_pause_press = CCSprite::createWithSpriteFrameName("BtnPause90Pressed.png");
    
    pauseItem = CCMenuItemSprite::create(button_pause,button_pause_press,this,menu_selector(CKGameScene90::menuCallback));
    pauseItem->setTag(Action_Pause);
    pauseItem->setPosition(ccp(win_size.width*0.45,win_size.height*0.4));
    itemMenu->addChild(pauseItem,5);
    
    addChild(itemMenu,10);
    
    createLabelScorePool(this);
};

void CKGameScene90::incLevelWorld()
{
    if(CKFileOptions::Instance().isKidsModeEnabled())
    {
        if(CKHelper::Instance().lvl_number < COUNT_LEVEL_IN_WORLD)
        {
            if(CKFileInfo::Instance().getCurentWorld()->open_lvl > CKHelper::Instance().lvl_number)
            {
                CKHelper::Instance().lvl_number++;
            }
        }
    }
    else
    {
        if(CKHelper::Instance().lvl_number < COUNT_LEVEL_IN_WORLD)
        {
            CKHelper::Instance().lvl_number++;
        }
    }
    
    if(CKHelper::Instance().lvl_number == COUNT_LEVEL_IN_WORLD)
    {
        CKHelper::Instance().lvl_number = 0;
        CKHelper::Instance().lvl_speed++;
        if(CKHelper::Instance().lvl_speed >= COUNT_DIFFICULT)
            CKHelper::Instance().lvl_speed = 0;
    };
    
    CKFileInfo::Instance().setCurentOpenLevel(CKHelper::Instance().lvl_number);
};

void CKGameScene90::createLabelScorePool(CCNode *_node)
{
    for(int i = 0; i < 10; i++ )
    {
        CCLabelTTF *tmp = CCLabelTTF::create("0", font_name.c_str(), BOX_SIZE/2);
        label_pool.push_back(tmp);
        tmp->setVisible(false);
        tmp->setColor((ccColor3B){220,255,20});
        _node->addChild(tmp,zOrder_LabelInfo);
    };
};

void CKGameScene90::hideNode(CCNode *_sender)
{
    _sender->setVisible(false);
};

void CKGameScene90::showLabelScore(const CCPoint& _pos,int _score)
{
    if(CKFileOptions::Instance().isKidsModeEnabled())
        return;
    label_pool_it = label_pool.begin();
    while (label_pool_it != label_pool.end()) {
        if(!(*label_pool_it)->isVisible())
        {
            (*label_pool_it)->setPosition(_pos);
            (*label_pool_it)->setVisible(true);
            (*label_pool_it)->setScale(0.0);
            (*label_pool_it)->setOpacity(255);
            CCFiniteTimeAction *move = CCMoveTo::create(0.7/float(CKHelper::Instance().lvl_speed+1), ccp((*label_pool_it)->getPositionX(),(*label_pool_it)->getPositionY() + BOX_SIZE));
            CCFiniteTimeAction *scale = CCScaleTo::create(0.9/float(CKHelper::Instance().lvl_speed+1), 1.0);
            CCFiniteTimeAction *swap = CCSpawn::create(move,scale, NULL);
            CCFiniteTimeAction *fade = CCFadeOut::create(0.4/float(CKHelper::Instance().lvl_speed+1));
            CCFiniteTimeAction *func = CCCallFuncN::create((*label_pool_it), callfuncN_selector(CKGameScene90::hideNode));
            CCAction *seq = CCSequence::create(swap,fade,func,NULL);
            (*label_pool_it)->runAction(seq);
            
            char str[NAME_MAX];
            sprintf(str,"+%d",_score);
            (*label_pool_it)->setString("");
            (*label_pool_it)->setString(str);
            switch (MIN(6,m_bombs->getAccuracy())) {
                case 2:
                    (*label_pool_it)->setColor((ccColor3B){0,133,0});
                    break;
                case 3:
                    (*label_pool_it)->setColor((ccColor3B){236,252,0});
                    break;
                case 4:
                    (*label_pool_it)->setColor((ccColor3B){255,188,0});
                    break;
                case 5:
                    (*label_pool_it)->setColor((ccColor3B){255,116,0});
                    break;
                case 6:
                    count_accuvate_shots++;
                    (*label_pool_it)->setColor((ccColor3B){255,0,0});
                    break;
                default:
                    (*label_pool_it)->setColor((ccColor3B){0,133,0});
                    break;
            };
            
            return;
        }
        label_pool_it++;
    };
};
void CKGameScene90::gameWin()
{
    int t_score;
    unscheduleUpdate();
    if(CKHelper::Instance().lvl_speed == 4)
        t_score = m_airplane->getBonusFinish()*(CKHelper::Instance().lvl_speed + 7)*4;
    else
        t_score = m_airplane->getBonusFinish()*(CKHelper::Instance().lvl_speed + 6)*4;
    
    curent_score += t_score;
    if(!CKFileOptions::Instance().isKidsModeEnabled())
        CKFileInfo::Instance().addScore(curent_score);
    
    if(CKFileInfo::Instance().getLevel()->score < curent_score && CKFileInfo::Instance().getLevel()->score > 0)//show hiscore
    {
        is_hiscore = true;
    };
    
    incLevelWorld();
    
    CCLOG("CKHelper::Instance().lvl_number %d",CKHelper::Instance().lvl_number);
    if(!CKFileOptions::Instance().isKidsModeEnabled()) //show label score on plane height
    {
        char score_str[NAME_MAX];
        sprintf(score_str, "+%d",t_score);
        CCLabelTTF* label = CCLabelTTF::create(score_str, font_name.c_str(), BOX_SIZE/2);
        addChild(label,zOrder_LabelInfo);
        
        label->setColor(ccc3(255,255,0));
        label->setPosition(ccp(win_size.width/2,win_size.height/2));
        
        
        CCAction* action = CCMoveTo::create(1.1, ccp(label->getPositionX(),label->getPositionY() + BOX_SIZE*2));
        CCAction* action2 = CCScaleTo::create(1.0, 1.6);
        CCAction* action3 = CCFadeOut::create(1.0);
        CCAction* action4 = CCSpawn::create((CCFiniteTimeAction*)action,(CCFiniteTimeAction*)action2,(CCFiniteTimeAction*)action3,NULL);
        label->runAction(action4);
    }
    CCLOG("score %d ",m_airplane->getBonusFinish());
    is_pause = true;
    
};

void CKGameScene90::saveProgress()
{
    if(CKFileOptions::Instance().isKidsModeEnabled())
        return;
    int count_org_bomb = m_bombs->getCountCreateBomb();
    int count_create_spec_bomb = 0;

    int count_loop_plane = MAX(0,m_airplane->getAirplaneHeight() -  m_airplane->getBonusFinish());
    int count_destroy_cube = (m_cube_array->getCountCube()*4 - m_cube_array->getCountActiveCube())/4;
    
    CKFileInfo::Instance().getScore()->addAccuvateShots(count_accuvate_shots);
    CKFileInfo::Instance().getScore()->addCountOrginaryBomb(count_org_bomb);
    CKFileInfo::Instance().getScore()->addCountSpecialBomb(count_create_spec_bomb);
    CKFileInfo::Instance().getScore()->addCountDestroyCube(count_destroy_cube);
    CKFileInfo::Instance().getScore()->addCountLoopPlane(count_loop_plane);
    CKFileInfo::Instance().getScore()->addCountHitBomb(m_bombs->getCountHitBomb());
    CKFileInfo::Instance().save();
    
    CCLOG("m_bombs->getCountHitBomb() %d",m_bombs->getCountHitBomb());
    
    CKHelper::Instance().createSocialAttach("attach.jpg", current_level_id + 1, WORLD_NAMES[curent_world_id], curent_score, m_bombs->getCountCreateBomb(), count_destroy_cube, count_loop_plane, CKFileInfo::Instance().getScore()->balance_score);
    
    int acc = 0;
    if(CKFileInfo::Instance().getScore()->getCountHitBomb() > 0)
        acc = float(CKFileInfo::Instance().getScore()->getAccuvateShots())*100000.0f/float(CKFileInfo::Instance().getScore()->getCountHitBomb());
    if(!CKFileInfo::Instance().isBlackList())
    {
        CCLOG("leaderboardSendAllScore %d %d %d",CKFileInfo::Instance().getScore()->getCountHitBomb(),CKFileInfo::Instance().getScore()->getAccuvateShots(),acc);
        
        ObjCCalls::leaderboardSendAllScore(CKFileInfo::Instance().getScore()->score_total, CKFileInfo::Instance().getScoreAllWorld(), CKHelper::Instance().getWorldNumber(), CKFileInfo::Instance().getScoreCurrentWorld(), CKFileInfo::Instance().getScore()->getCountOrginaryBomb(), CKFileInfo::Instance().getScore()->getCountSpecialBomb(), acc, CKFileInfo::Instance().getScore()->getCountDestroyCube(), CKFileInfo::Instance().getScore()->getCountLoopPlane());
        if(is_game_win)
        {
            CKParseHelper::Instance().parseSendOpenWorldLvl(CKFileInfo::Instance().getLvlOpenWithWorld(CKHelper::Instance().getWorld(0)),CKFileInfo::Instance().getLvlOpenWithWorld(CKHelper::Instance().getWorld(1)),CKFileInfo::Instance().getLvlOpenWithWorld(CKHelper::Instance().getWorld(2)),CKFileInfo::Instance().getLvlOpenWithWorld(CKHelper::Instance().getWorld(3)),CKFileInfo::Instance().getLvlOpenWithWorld(CKHelper::Instance().getWorld(4)));
        }
    };
    
};

#pragma mark - UPDATE
void CKGameScene90::updateBomb(const float &dt)
{
    //update bomb
    if (m_bombs->update(dt))
    {
        if(m_bombs->getCurentBomb())
        {
            if(m_cube_array->checkCollision(m_bombs->getCurentBomb()) != 0)
            {
                m_bombs->deleteBomb(0);
            }
            else
                if((m_bombs->getCurentBomb()->getPositionY() < (ground_height + m_bombs->getCurentBomb()->getContentSize().height)))
                {
                    m_bombs->deleteBomb(1);
                };
        }
    }
};

void CKGameScene90::createBanner()
{
    texture_banner = CCTextureCache::sharedTextureCache()->addImage(CKAdBanner::Instance().show());
    banner = CCSprite::createWithTexture(texture_banner);
    
    if(banner)
    {
        CCMenuItemSprite *item =  CCMenuItemSprite::create(banner, banner, this, menu_selector(CKGameScene90::menuCallback));
        item->setScale(CCDirector::sharedDirector()->getContentScaleFactor());
        item->setTag(99);
        menu_banner = CCMenu::create(item,NULL);
        menu_banner->setPosition(ccp(win_size.width/2,win_size.height - item->getContentSize().height/2*item->getScaleY()));
        addChild(menu_banner,99);
    };
};

void CKGameScene90::showBanner(bool _anim)
{
    if(!CKFileOptions::Instance().isBannerShow())
        return;
    if(!banner)
    {
        banner = CCSprite::create(loadImage("stigolBanner.jpg").c_str());
        banner->setPosition(ccp(win_size.width/2,win_size.height + banner->getContentSize().height/2));
        banner->setOpacity(127);
        addChild(banner,99);
    };
    if(banner->getPositionY() < win_size.height)
        return;
    
    if(_anim)
    {
        CCAction *action0 = CCMoveTo::create(1.0, ccp(pauseItem->getPositionX(),pauseItem->getPositionY() - banner->getContentSize().height/2));
        pauseItem->runAction(action0);
        CCAction *action = CCMoveTo::create(1.0, ccp(win_size.width/2,win_size.height - banner->getContentSize().height/2));
        banner->runAction(action);
    }
    else
    {
        pauseItem->setPosition(ccp(pauseItem->getPositionX(),pauseItem->getPositionY() - banner->getContentSize().height/2));
        banner->setPosition(ccp(win_size.width/2,win_size.height - banner->getContentSize().height/2));
    }
    
    
};

void CKGameScene90::update(float dt)
{
    if(is_pause)    //don't update if pause
        return;

    if(count_active_touch == 1 && !m_airplane->isDoubleSpeed())
    {
        touchAirplane(touch_position);
    };
    
    switch (m_airplane->update(dt)) {
        case rAirplane_Banner: //Show banner
        {
            //if(CKFileOptions::Instance().isBannerShow())
            //     showBanner();
            break;
        }
        case rAirplane_Win: //You win
        {
            CK::StatisticLog::Instance().setNewPage(CK::Page_LevelComplete);
            is_game_win = true;
            ObjCCalls::sendCustomGA(3,"num_lvl_completed",(CKHelper::Instance().lvl_number+1)*(CKHelper::Instance().getWorldNumber() + 1));
            ObjCCalls::sendCustomGA(4,"unused_bombs_lvl_completed",0);
            gameWin();
            if(!CKFileOptions::Instance().isKidsModeEnabled())
                saveProgress();
            showEndMenu(true);
            break;
        }
        case rAirplane_Over: //Game Over
        {
            CK::StatisticLog::Instance().setNewPage(CK::Page_LevelFail);
            ObjCCalls::sendCustomGA(1,"num_lvl_failed",(CKHelper::Instance().lvl_number+1)*(CKHelper::Instance().getWorldNumber() + 1));
            ObjCCalls::sendCustomGA(2,"unused_bombs_lvl_failed",0);
            is_pause = true;
            //CKFileInfo::Instance().addScore(curent_score);
            if(!CKFileOptions::Instance().isKidsModeEnabled())
                saveProgress();
            showEndMenu(false);
            break;
        }
        case rAirplane_Splash: //Airplane touch cube
        {
            CKAudioMng::Instance().playEffect("plane_slice");
            CCParticleSystem* m_emitter = CCParticleSystemQuad::create("splash90.plist");
            m_emitter->setPosition(ccp(m_airplane->getPosition().x + m_airplane->getSize().width/3,m_airplane->getPosition().y - m_airplane->getSize().height/4));
            addChild(m_emitter ,zOrderParticle);
            m_emitter->setScale(0.5);
        }
            break;
        case 7: //send score to airplane
        {
            //   CKHelper::Instance().score = score;
            //   CCLOG("send score %d",score);
        }
            break;
        default:
            break;
    };
    
    updateBomb(dt);
    
    int tmp_score = m_cube_array->getScore();
    
    if(tmp_score > 0)
    {
        int score_value = tmp_score * MIN(6,m_bombs->getAccuracy());
        CCLOG("add score %d %d speed %d",tmp_score,score_value,CKHelper::Instance().lvl_speed);
        addScore(score_value);
        showLabelScore(m_bombs->getPosition(),score_value);
    };
    
    if(stars_spectrum)
        stars_spectrum->update(dt);
};

void CKGameScene90::animationLabelMult()
{
    label_score->setScale(0.0);
    CCFiniteTimeAction * action = CCSpawn::create(CCScaleTo::create(0.5, 1.0),CCFadeIn::create(0.5),NULL);
    label_score->runAction(action);
    
    char tmp[NAME_MAX];
    sprintf(tmp, "%d",last_score*CKFileInfo::Instance().getMultiBuyKoef());
    if(label_score)
        label_score->setString(tmp);
};

void CKGameScene90::updateScore(float _dt)
{
    if(last_score >= curent_score)
    {
        unschedule(schedule_selector(CKGameScene90::updateScore));
        score_active = false;
        CKAudioMng::Instance().stopEffect("add_score");
        if(is_game_win)
        {
            if(CKFileInfo::Instance().getMultiBuyKoef() > 0)
            {
                char str[255];
                sprintf(str, "x%d",CKFileInfo::Instance().getMultiBuyKoef());
                CCLabelTTF * labl = static_cast<CCLabelTTF *>(m_gui_win->getChildByTag(53)->getNode());
                labl->setVisible(true);
                labl->setString(str);
                labl->setOpacity(0);
                CCFiniteTimeAction * action = CCSequence::create(CCFadeIn::create(0.5),CCSpawn::create(CCScaleTo::create(0.5, 1.3),CCFadeOut::create(0.5),NULL),NULL);
                CCFiniteTimeAction * action2 = CCSequence::create(CCDelayTime::create(0.5),CCSpawn::create(CCScaleTo::create(0.5, 1.3),CCFadeOut::create(0.5),NULL),CCCallFunc::create(this, callfunc_selector(CKGameScene90::animationLabelMult)),NULL);
                
                label_score->runAction(action2);
                labl->runAction(action);
            };
        }
    }
    else
    {
        int t = (curent_score - last_score);
        if(t > (1.0/add_score_koef))
        {
            if(int(t*add_score_koef) > 2.0/add_score_koef)
                last_score += 2.0/add_score_koef;
            else
                last_score += t*add_score_koef;
        }
        else
            ++last_score;
        
        // CCLOG("updateScore::last_score %d score %d",last_score,curent_score);
        CCAssert(curent_score < 100000, "Error:: incorect value score");
        if(label_score->isVisible())
        {
            char tmp[NAME_MAX];
            sprintf(tmp, "%d",last_score);
            label_score->setString(tmp);
        }
    }
};

void CKGameScene90::ccTouchesEnded(CCSet* touches, CCEvent* event)
{
    if(touchShareEnded(touches))
        return;
    //Add a new body/atlas sprite at the touched location
    CCSetIterator it;
    CCTouch* touch;
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        count_active_touch--;
    };
        
    CCLOG("ccTouchesEnded %d",count_active_touch);
    if(count_active_touch < 2 && !m_airplane->isWaitCreateBomb())
    {
        m_airplane->setDoubleSpeed(false);
    };
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        
        if(count_active_touch > 1)
            break;
        
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        if(m_gui_pause)
            m_gui_pause->setTouchEnd(location);
        
        if(m_gui_win)
            m_gui_win->setTouchEnd(location);
        
        if(m_gui_over)
            m_gui_over->setTouchEnd(location);
        
        if(is_pause)
            return;
    }
};
bool CKGameScene90::touchShareEnded(cocos2d::CCSet* touches)
{
    if(m_share && m_share->isTouchEnabled()&& m_share->get()->isVisible())
    {
        CCSetIterator it;
        CCTouch* touch;
        for( it = touches->begin(); it != touches->end(); it++)
        {
            touch = (CCTouch*)(*it);
            CCPoint location = touch->getLocationInView();
            location = CCDirector::sharedDirector()->convertToGL(location);
            m_share->setTouchEnd(location);
            return true;
        };
    };
    return false;
};

bool CKGameScene90::touchShareBegan(cocos2d::CCSet* touches)
{
    if(m_share && m_share->isTouchEnabled()&& m_share->get()->isVisible())
    {
        CCSetIterator it;
        CCTouch* touch;
        for( it = touches->begin(); it != touches->end(); it++)
        {
            touch = (CCTouch*)(*it);
            CCPoint location = touch->getLocationInView();
            location = CCDirector::sharedDirector()->convertToGL(location);
            
            m_share->setTouchBegan(location);
            
            return true;
        }
    }
    return false;
};

void CKGameScene90::ccTouchesBegan(CCSet* touches, CCEvent* event)
{

    if(touchShareBegan(touches))
        return;
    //Add a new body/atlas sprite at the touched location
    CCSetIterator it;
    CCTouch* touch;
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        count_active_touch++;
    };

    
    if(count_active_touch > 1 && !is_pause)
    {
        m_airplane->setDoubleSpeed(true);
    };
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        touch_position = location;
        
        if(m_gui_pause && m_gui_pause->setTouchBegan(location))
            return;
        if(m_gui_win && m_gui_win->setTouchBegan(location))
            return;
        if(m_gui_over && m_gui_over->setTouchBegan(location))
            return;
        
        if(is_pause)
            return;
        
        if(!m_airplane->isDoubleSpeed())
        {
            if(m_bombs->canCreateBomb())
            {
                touchAirplane(location);
            }
            else
            {
                CKAudioMng::Instance().playEffect("cant_create_bomb");
            }
        }
    };
    return;
};

void CKGameScene90::touchAirplane(const cocos2d::CCPoint &_point)
{
    //don't create bomb in panel
    if(!is_pause)
    {
        m_airplane->touch(_point);
    }
};

#pragma mark - OPERATION
void CKGameScene90::addScore(int _score)
{
    CCLog("addScore:: %d",_score);
    curent_score += _score;
    if(last_score <= curent_score)
    {
        if(!score_active)
        {
            unschedule(schedule_selector(CKGameScene90::updateScore));
            schedule(schedule_selector(CKGameScene90::updateScore),0.03);
            score_active = true;
        }
    };
};

void CKGameScene90::shareCall(CCNode* _sender,void *_value)
{
    CKAudioMng::Instance().playEffect("button_pressed");
    switch(*static_cast<int*>(_value))
    {
        case Action_EveryPlay_Play:
            if(!CKFileOptions::Instance().isEveryPlayShow())
            {
                CKHelper::Instance().getDialog()->setTarget(this);
                CKHelper::Instance().getDialog()->setConfirmText(LCZ("Enable Everyplay ?"),LCZ("May cause lower performance on weak device"));
                CKHelper::Instance().getDialog()->show(callfuncN_selector(CKGameScene90::enableEveryplay));
            }
            else
            {
                if(have_everyplay_record)
                {
                    ObjCCalls::everyplayPlayLastVideo(curent_score, current_level_id + 1,curent_world_id);
                }
                else
                {
                    if(ObjCCalls::isLittleMemory())
                    {
                        ObjCCalls::goToURL(EVERYPLAY_URL);
                    }
                    else
                    {
                        ObjCCalls::everyplayShow();
                    }
                }
            }
            break;
        case Action_FB_Challenge:
        {
            CK::StatisticLog::Instance().setLink(CK::Link_ChallengeScore);
            CKFacebookHelper::Instance().FB_Challenge(curent_score, current_level_id + 1,curent_world_id);
        }
            break;
        case Action_FB_Brag:
        {
            if(is_posted_in_facebook && ObjCCalls::getDeviceVersion() < 6.0)
                break;
            CK::StatisticLog::Instance().setLink(CK::Link_PostScore);
            is_posted_in_facebook = true;
            std::string fullpath = CCFileUtils::sharedFileUtils()->getWriteablePath() + "attach.jpg";
            char str[255];
            sprintf(str, "%s\n%s",LCZ("Checkout my score at CandyKaze game"),POST_APP_URL);
            CKFacebookHelper::Instance().FB_PostImage(str, fullpath.c_str());
            //ObjCCalls::FB_SendBrag(curent_score,current_level_id + 1,curent_world_id);
        }
            break;
        case Action_Tweet_Post:
        {
            CK::StatisticLog::Instance().setLink(CK::Link_TweetScore);
            char str[NAME_MAX];
            sprintf(str, LCZ("%s  game. My score in lvl  %d of %s is %d\n"),GAME_NAME,current_level_id + 1,WORLD_NAMES[curent_world_id],curent_score);
            std::string fullpath = CCFileUtils::sharedFileUtils()->getWriteablePath() + "attach.jpg";
            CKTwitterHelper::Instance().TweetPost(str,fullpath.c_str());
        }
            break;
        case Action_Mail:
        {
            CK::StatisticLog::Instance().setLink(CK::Link_EmailScore);
            std::string fullpath = CCFileUtils::sharedFileUtils()->getWriteablePath() + "attach.jpg";
            ObjCCalls::sendMail(curent_score,current_level_id + 1,curent_world_id,fullpath.c_str());
        }
            break;
        case Action_IMessage:
            CK::StatisticLog::Instance().setLink(CK::Link_ShareImassage);
            ObjCCalls::sendMessage(current_level_id + 1,curent_world_id,curent_score);
            break;
        case Action_Close:
        {
            m_share->setTouchEnabled(false);
            m_share->get()->setVisible(false);
            CK::StatisticLog::Instance().setNewPage(Page_GameScene90);
        };
            break;
    };
    if(*static_cast<int*>(_value) != Action_Close)
    {
        CK::StatisticLog::Instance().setNewPage(CK::Page_ShareWindow);
    };
    
};
void CKGameScene90::restart()
{
    CKHelper::Instance().lvl_number = current_level_id;
    CKHelper::Instance().setWorldNumber(curent_world_id);
    
    CCScene *pScene = CKGameScene90::scene();
    CCDirector::sharedDirector()->replaceScene(pScene);
};

void CKGameScene90::menuCall(CCNode* _sender,void *_value)
{
    CCLOG("menuCall %d",*static_cast<int *>(_value));
    switch(*static_cast<int *>(_value))
    {
        case CK::Action_Menu:
        case CK::Action_Back:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            //GA Фіксуємо кількість переходів на наступний левел
            if(m_gui_pause->get()->isVisible())
            {
                ObjCCalls::sendEventGA("mnu_pause","menu_after_pause","mnu_pause",1);//GA Фіксуємо кількість виходів в меню після натиснення паузи
            }
            else
            {
                if(is_pause)
                {
                    ObjCCalls::sendEventGA("lvl_complete","menu_after_lvl","lvl_complete",1);//GA Фіксуємо кількість повернення в меню після успішного завершення левел
                }
                else
                {
                    ObjCCalls::sendEventGA("lvl_crash","menu_after_crash","lvl_crash",1);//GA Фіксуємо кількість виходів в меню після крешу левела
                    
                };
            };
            
//            ObjCCalls::sendToAmazon();
//            time_t tim = time(0);
//            
//            CKStaticFile::Instance().newFileName(&tim);
            
            CKAudioMng::Instance().fadeBgToVolume(0.0,0.1);
//            CKAudioMng::Instance().removeBackground();
            this->setTouchEnabled(false);
            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(CK::SCENE_WORLD);
        };
            break;
        case CK::Action_Options:
            break;
        case CK::Action_Share:
            CK::StatisticLog::Instance().setNewPage(Page_ShareWindow);
            CKAudioMng::Instance().playEffect("show_modal");
            m_share->get()->setVisible(true);
            m_share->setTouchEnabled(true);
            break;
        case CK::Action_Next:
        {
            CKAudioMng::Instance().stopAllEffect();
            CKAudioMng::Instance().fadeBgToVolume(0.0,0.1);
            CKAudioMng::Instance().playEffect("button_pressed");
            CCDirector::sharedDirector()->getTouchDispatcher()->removeAllDelegates();
            setTouchEnabled(false);
            CCScene *pScene = CKGameScene90::scene();
            CCDirector::sharedDirector()->replaceScene(pScene);
        }
            break;
        case CK::Action_Facebook:
//            break;
//            if(m_gui_pause->get()->getTag() == 1)//is pause
//            {
//                if(ObjCCalls::isOpenSession())
//                {
//                    char str[NAME_MAX];
//                    sprintf(str,"Level:%d\t Score:%d \t Hi-Score:%d", CKHelper::Instance().lvl_number,curent_score,CKFileInfo::Instance().getLevel()->score);
//                    
//                    m_gui_pause->setVisible(false);
//                    bg_mask90->setVisible(false);
//                    
//                    this->visit();
//                    this->draw();
//                    CCDirector::sharedDirector()->drawScene();
//                    
//                    sprintf(str,"   ");
//                    ObjCCalls::sendScreen(str);
//                    
//                    m_gui_pause->setVisible(true);
//                    bg_mask90->setVisible(true);
//                }
//                else
//                    ObjCCalls::changeSession();
//            }
            
            break;
        case CK::Action_FBLike:
            break;
        case Action_EveryPlay_Play:
            CKAudioMng::Instance().playEffect("button_pressed");
            if(!CKFileOptions::Instance().isEveryPlayShow())
            {
                CK::StatisticLog::Instance().setNewPage(CK::Page_EveryplayOn);
                CKHelper::Instance().getDialog()->setTarget(this);
                CKHelper::Instance().getDialog()->setConfirmText(LCZ("Enable Everyplay ?"),LCZ("May cause lower performance on weak device"));
                CKHelper::Instance().getDialog()->show(callfuncN_selector(CKGameScene90::enableEveryplay));
            }
            else
            {
                ObjCCalls::everyplayPlayLastVideo(0, current_level_id + 1,curent_world_id);
            }
            
            break;
        case Action_Feedback:

            break;
        case CK::Action_Tweet:
        {
        }
            break;
        case CK::Action_Restart:
        {
            CKAudioMng::Instance().stopAllEffect();
            CKAudioMng::Instance().playEffect("button_pressed");
            CKAudioMng::Instance().fadeBgToVolume(0.0,0.1);
            
            setTouchEnabled(false);
            setAccelerometerEnabled(false);
            this->runAction(CCSequence::create(CCDelayTime::create(0.2),CCCallFunc::create(this, callfunc_selector(CKGameScene90::restart)),NULL));
            

        }
            break;
        case CK::Action_Pause:
        {
            
            CCLOG("Action_Pause");
            is_pause = !is_pause;
            showOrHidePauseMenu(is_pause);
            CKAudioMng::Instance().playEffect("show_modal");
        }
            break;
        case Action_Effect:
        {
            
            CKFileOptions::Instance().setPlayEffect(!CKFileOptions::Instance().isPlayEffect());
            CKAudioMng::Instance().playEffect("button_pressed");
            CKObject* _node = m_gui_pause->getChildByTag(Action_Effect);
            if(_node)
            {
                if(_node->getType() == CK::GuiRadioBtn)
                {
                    _node->setStateChild(int(!CKFileOptions::Instance().isPlayEffect()));
                };
            };
        }
            break;
        case Action_Music:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            changeMusic();

            CKObject* _node = m_gui_pause->getChildByTag(Action_Music);
            if(_node)
            {
                if(_node->getType() == CK::GuiRadioBtn)
                {
                    _node->setStateChild(!CKFileOptions::Instance().isPlayMusic());
                };
            };
        };
            break;
        case CK::Action_Shop:
#ifndef DISABLE_SHOP
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            CKAudioMng::Instance().fadeBgToVolume(0.0,0.1);
            CKHelper::Instance().show_layer = CK::Action_Shop;

            CKAudioMng::Instance().removeAllEffect();
            this->setTouchEnabled(false);
            this->setAccelerometerEnabled(false);
            
            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(CK::SCENE_SHOP);
            
        }
#endif
            break;
        case CK::Action_Inventory:
#ifndef DISABLE_INVENTORY
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            CKAudioMng::Instance().fadeBgToVolume(0.0,0.1);
            CKHelper::Instance().show_layer = CK::Action_Inventory;

            CKAudioMng::Instance().removeAllEffect();
            
            setTouchEnabled(false);
            setAccelerometerEnabled(false);
            
            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(CK::SCENE_INVENTORY);
            
        }
#endif
            break;
        default:
            CCAssert(0, "Uknow tag");
    }
};
void CKGameScene90::onEnter()
{
    CCLayer::onEnter();
    if(CKFileOptions::Instance().isPlayEffect())
    {
        CKAudioMng::Instance().resumeAllEffect();
        
        if(!CKAudioMng::Instance().isPlayEffect("plane_fly"))
        {
            CKAudioMng::Instance().playEffect("plane_fly");
        };
    };
    if(CKFileOptions::Instance().isPlayMusic())
    {
        CKAudioMng::Instance().playBackground("bg",0.0);
        CKAudioMng::Instance().fadeBgToVolume(1.0);
    }
};

void CKGameScene90::enableEveryplay(CCNode *_sender)
{
    //Issue #172
    if(m_share && m_share->get()->isVisible())
    {
        CK::StatisticLog::Instance().setNewPage(CK::Page_ShareWindow);
    }
    else
    {
        CK::StatisticLog::Instance().setNewPage(CK::Page_GameScene90);
    }
    //----------------------
    CKAudioMng::Instance().playEffect("button_pressed");
    if(!_sender)
        return;
    CKObject* _node = NULL;
    if(m_share && m_share->get()->isVisible())
    {
        _node = m_share->getChildByTag(CK::Action_EveryPlay_Play);
    }
    else if(m_gui_over)
    {
        _node = m_gui_over->getChildByTag(CK::Action_EveryPlay_Play);
    }
    if(_node)
    {
        CKFileOptions::Instance().setEveryPlayShow(true);
        _node->getChildByTag(3)->setVisible(false);
    }
};
void CKGameScene90::menuCallback(CCNode* _sender)
{
    if(_sender->getTag() == 99)
    {
        ObjCCalls::goToURL(CKAdBanner::Instance().getURL());
    }
    else
    {
        int d = _sender->getTag();
        menuCall(NULL, &d);
    };
}

void CKGameScene90::showEndMenu(bool _win)
{
    CKAudioMng::Instance().playEffect("show_modal");
    label_score->runAction(CCMoveBy::create(0.2, ccp(0,win_size.height*0.4)));
    pauseItem->runAction(CCMoveBy::create(0.2, ccp(0,win_size.height*0.4)));
    
    char str1[NAME_MAX];

    CCFiniteTimeAction* m_action1 =  CCSequence::create(CCEaseElasticOut::create(CCMoveTo::create(1.5,ccp(panel_left,0))),NULL); //show


    if(_win)
    {
        m_gui_win->get()->setVisible(true);
        m_gui_win->get()->setPosition(ccp(panel_left,-win_size.height));
    }
    if(!CKFileOptions::Instance().isKidsModeEnabled())
    {
        char total_str[NAME_MAX];
        char score_str[NAME_MAX];
        sprintf(total_str, "%d",CKFileInfo::Instance().getTotalScore());
        sprintf(score_str, "%d",last_score);
        
        last_score = curent_score;
        CCAction *sequer;
        if(_win)
        {
            if(CKHelper::Instance().lvl_speed == 4)
                last_score -= m_airplane->getBonusFinish()*(CKHelper::Instance().lvl_speed + 7)*4;
            else
                last_score -= m_airplane->getBonusFinish()*(CKHelper::Instance().lvl_speed + 6)*4;
            m_gui_win->getChildByTag(51)->setText(total_str);
            m_gui_win->getChildByTag(52)->setText(score_str);
            label_score = static_cast<CCLabelTTF*>(m_gui_win->getChildByTag(52)->getNode());
            m_gui_win->getChildByTag(61)->setVisible(is_hiscore);
            m_gui_win->getChildByTag(62)->setVisible(is_hiscore);
            sequer = CCSequence::create(CCDelayTime::create(0.2),m_action1,CCCallFunc::create(this, callfunc_selector(CKGameScene90::startAddScoreFn)), NULL);
            m_gui_win->get()->runAction(sequer);
        }
        else
        {
            sprintf(str1,"%d",CKFileInfo::Instance().getTotalScore());
            m_gui_over->get()->setVisible(true);
            m_gui_over->get()->setPosition(ccp(panel_left,-win_size.height));
            
            m_gui_over->getChildByTag(51)->setText(total_str);
            sequer = CCSequence::create(CCDelayTime::create(0.2),m_action1,CCDelayTime::create(0.1), NULL);
            m_gui_over->get()->runAction(sequer);
        }
        sprintf(str1,"%d",last_score);
        label_score->setString(str1);
    }
    else
    {
        m_gui_win->get()->runAction(m_action1);
    }

    
    if(bg_mask90)
    {
        CCAction* fin = CCFadeIn::create(TIME_SHOW_FRAME_ANIMATION);
        bg_mask90->runAction(fin);
    };
    
    //save static file
    timeval t;
    gettimeofday(&t,NULL);
    time_start = t.tv_sec - pause_time - time_start;
    
//    CKStaticFile::Instance().setEndInfo(time_start,curent_score,_win,m_cube_array->getCountCube(),m_cube_array->getCountActiveCube(),m_airplane->getBonusFinish());
//    CKStaticFile::Instance().setBombInfo(0, int(m_bombs->getCountCreateBomb() - m_bombs->getCountFailBomb()),  m_bombs->getCountFailBomb(), m_bombs->getCountCreateBomb(),0);
    int state = is_game_win;

    int count_destroy_cube = m_cube_array->getCountCube()*4 - m_cube_array->getCountActiveCube();
    
    if(!CKFileOptions::Instance().isKidsModeEnabled())
    {
       // CKStaticFile::Instance().save();
    }
    else
    {
        state = 2;
        ObjCCalls::sendEventFlurry("kids_level_complete", false, "world_num=%d,lvl_num=%d,difficulty_num=%d,lvl_state=%d,blocks_big_all=%d,bombs_available=%d,level_time=%d",curent_world_id,current_level_id,CKHelper::Instance().lvl_speed + 1,int(_win),m_cube_array->getCountCube(),0,int(time_start));
    }
    int plane_loop = MAX(0,m_airplane->getAirplaneHeight() -  m_airplane->getBonusFinish());
    StatisticLog::Instance().setLP(curent_world_id, current_level_id + 1,m_airplane->getStartSpeed()*10, state, m_cube_array->getCountCube(), 0, time_start, m_airplane->getStartHeight(), plane_loop, int(m_bombs->getCountCreateBomb() - m_bombs->getCountFailBomb()), m_bombs->getCountFailBomb(), count_destroy_cube,  m_bombs->getCountCreateBomb(), 0, curent_score);
    
#if ENABLE_EVERYPLAY
    ObjCCalls::everyplayStopRecording();
#endif
};

void CKGameScene90::startAddScoreFn()
{
    if(last_score <= curent_score)
    {
        CKAudioMng::Instance().playEffect("add_score");
        if(!score_active)
        {
            unschedule(schedule_selector(CKGameScene90::updateScore));
            schedule(schedule_selector(CKGameScene90::updateScore),0.03);
            score_active = true;
        }
    };
};

void CKGameScene90::setPause()
{
    if(is_pause)
        return;
    
    CK::StatisticLog::Instance().setNewPage(CK::Page_Pause);
#if ENABLE_EVERYPLAY
    ObjCCalls::everyplayPauseRecording();
#endif
    count_active_touch = 0;
    
    is_pause = true;
    timeval t;
    gettimeofday(&t,NULL);
    pause_time = t.tv_sec - pause_time;
    
    m_gui_pause->get()->setVisible(true);
    m_gui_pause->get()->setPosition(ccp(-win_size.width*1.0 + panel_left,win_size.height*0.0));
    CCAction* action = CCMoveTo::create(TIME_SHOW_FRAME_ANIMATION, ccp(win_size.width*0.0 + panel_left,win_size.height*0.0));
    m_gui_pause->get()->runAction(action);
    m_gui_pause->get()->setTag(1);
    pauseItem->setVisible(false);
    

    pauseAudio(false);
    if(bg_mask90)
    {
        bg_mask90->setVisible(true);
    };
    
};

void CKGameScene90::endShowPause()
{
    scheduleUpdate();
    //calc time pause
    timeval t;
    gettimeofday(&t,NULL);
    pause_time = t.tv_sec - pause_time;
    CCLOG("pause_time resume = %d",pause_time);
#if ENABLE_EVERYPLAY
    ObjCCalls::everyplayResumeRecording();
#endif
    
};

//show or hide pause gui
//if _show = true then show animation pause and stop music
void CKGameScene90::showOrHidePauseMenu(bool _show)
{
    is_pause = _show;
    if(!_show)
    {
        CK::StatisticLog::Instance().setNewPage(CK::Page_GameScene90);
        unscheduleUpdate();
        schedule(schedule_selector(CKGameScene90::endShowPause), TIME_SHOW_FRAME_ANIMATION, 0, TIME_SHOW_FRAME_ANIMATION);
        resumeAudio();
        CCAction* action = CCMoveTo::create(TIME_SHOW_FRAME_ANIMATION, ccp(win_size.width*1.0 + panel_left,win_size.height*0.0));
        m_gui_pause->get()->runAction(action);
        m_gui_pause->get()->setTag(0);
        pauseItem->setVisible(true);
        if(bg_mask90)
        {
            bg_mask90->setVisible(true);
            CCAction* fin = CCFadeOut::create(TIME_SHOW_FRAME_ANIMATION);
            bg_mask90->runAction(fin);
        }
    }
    else
    {
        CK::StatisticLog::Instance().setNewPage(CK::Page_Pause);
        
#if ENABLE_EVERYPLAY
        ObjCCalls::everyplayPauseRecording();
#endif
        //calc time pause
        timeval t;
        gettimeofday(&t,NULL);
        pause_time = t.tv_sec - pause_time;
        CCLOG("pause_time pause= %d",pause_time);
        m_gui_pause->get()->setVisible(true);
        m_gui_pause->get()->setPosition(ccp(-win_size.width*1.0 + panel_left,win_size.height*0.0));
        CCAction* action = CCMoveTo::create(TIME_SHOW_FRAME_ANIMATION, ccp(win_size.width*0.0 + panel_left,win_size.height*0.0));
        m_gui_pause->get()->runAction(action);
        m_gui_pause->get()->setTag(1);
        pauseItem->setVisible(false);
        if(bg_mask90)
        {
            bg_mask90->setVisible(true);
            CCAction* fout = CCFadeIn::create(TIME_SHOW_FRAME_ANIMATION);
            bg_mask90->runAction(fout);
        }
        
        pauseAudio(true);
    };
    
};
