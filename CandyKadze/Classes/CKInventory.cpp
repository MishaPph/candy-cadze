//
//  CKInventory.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 09.10.12.
//
//

#include "CKInventory.h"
#include "CKLoaderGUI.h"
#include "CKAudioMgr.h"
#include "CKStaticInfo.h"
#include "CKWordScene.h"
#include "CKHelper.h"
#include "CKMainMenuScene.h"
#include "CKShop.h"
#include "CKLanguage.h"
#include "StatisticLog.h"

#define LCZ(varType) CKLanguage::Instance().getLocal(varType)

#define COUNT_ROW 9
#define COUNT_COLL 5
//#define SIZE_ICON 140
#define NEED_ACTIVE_FOR_START_MOVE_SCROLL 8
const char *base_bomb_name = "inv_bomb33.png";

CKInventory::~CKInventory()
{
    CCLOG("~CKInventory");
   // m_gui->removeAtlasPlist();
   // m_gui->get()->removeFromParentAndCleanup(true);
    CC_SAFE_DELETE(m_gui);
    CC_SAFE_DELETE(m_upgrade);
    CKHelper::Instance().show_inventory_km_button = false;
    removeAllChildrenWithCleanup(true);
};

CCScene* CKInventory::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    // add layer as a child to scene
    CCLayer* layer = new CKInventory();
    
    CCLOG("scene:: scene %d layer %d ",scene,layer);
    if(layer->init())
    {
        scene->addChild(layer);
        layer->setTouchEnabled(true);
        layer->setTag(CK::LAYERID_MAIN);
        layer->release();
    };
    CKHelper::Instance().compliteLoadScene(scene);
    return scene;
};

CKInventory::CKInventory()
{
    m_message = NULL;
    m_gui = NULL;
    m_parent = NULL;
    CK::StatisticLog::Instance().setNewPage(CK::Page_Inventory);
    
    previor_bomb_in_inventory = -1;
    this->setTouchEnabled(false);
    if(CKFileInfo::Instance().is_load)
        CCLOG("CKInventory:: CKFileInfo.isLoad");
    
    win_size = CCDirector::sharedDirector()->getWinSize();
    curent_load = 0;
    min_pos_scroll = -10;
    load();
};


void CKInventory::load()
{
    if(CKHelper::Instance().menu_number == CK::SCENE_GAME)
    {
        CKAudioMng::Instance().playMenuSound();
        CKHelper::Instance().menu_number = CK::SCENE_WORLD;
    };
    
    this->setTouchEnabled(true);
    
    m_gui = new CKGUI();
    m_gui->create(CCNode::create());
    m_gui->load(CK::loadFile("c_Inventory.plist",true).c_str());
    m_gui->setTouchEnabled(true);
    m_gui->setTarget(this);
    m_gui->addFuncToMap("menuCall", callfuncND_selector(CKInventory::menuCall));
    m_gui->addFuncToMap("showModal", callfuncND_selector(CKInventory::showModal));
    m_gui->setMovedScroll(false);
    
    if(CKHelper::Instance().show_inventory_km_button && CKHelper::Instance().menu_number != CK::SCENE_GAME)
    {
        m_gui->getChildByTag(CK::Action_KidsMode)->setVisible(true);
    }
    else
    {
        m_gui->getChildByTag(CK::Action_KidsMode)->setVisible(false);
    };
    
    m_gui->setActiveScrollFrame(-10,false);
    scroll_object = m_gui->getChildByType(CK::GuiScrollView);
    //float pos = MAX(0.0,MIN(fabs(scroll_object->getPos().y/scroll_object->getNode()->getContentSize().height),1.0));
   // m_gui->getChildByType(CK::GuiSlider)->setProgress(1.0 - pos);
    char str_total[NAME_MAX];
    sprintf(str_total,"%d",CKFileInfo::Instance().getTotalScore());
    m_gui->getChildByTag(51)->setText(str_total);
    addChild(m_gui->get(),1);
    
    m_upgrade = new CKGUI();
    m_upgrade->create(CCNode::create());
    m_upgrade->load(CK::loadFile("c_Upgrade.plist",true).c_str());
    m_upgrade->setTouchEnabled(true);
    m_upgrade->setTarget(this);
    m_upgrade->addFuncToMap("menuCall", callfuncND_selector(CKInventory::menuCall));
    m_upgrade->addFuncToMap("callUpgrade", callfuncND_selector(CKInventory::callUpgrade));
    m_upgrade->addFuncToMap("resizeSlotCall", callfuncND_selector(CKInventory::resizeSlotCall));
    m_upgrade->addFuncToMap("callHint", callfuncND_selector(CKInventory::callHint));
    
    m_upgrade->get()->setVisible(false);

    addChild(m_upgrade->get(),2);
    CCSprite *m_sprite = CCSprite::create("gray_mask.png");
    m_upgrade->get()->addChild(m_sprite,-1);
    m_sprite->setScaleX(win_size.width/m_sprite->getContentSize().width);
    m_sprite->setScaleY(win_size.height/m_sprite->getContentSize().height);
    m_sprite->setPosition(ccp(win_size.width/2,win_size.height/2));

    
    inventory_list.clear();
    for(int i = 1; i <= 52;++i)
    {
        inventory_list.push_back(scroll_object->getChildByTag(1000 + i));
        scroll_object->getChildByTag(1000 + i)->setVisible(false);
        backSlot_list.push_back(scroll_object->getChildByTag(3000 + i));
    }

    createTmpBomb();
    reload();
    
    ObjCCalls::logPageView();//Flurry
    ObjCCalls::logPageView(true,"/Inventory");//GA
};
void CKInventory::removeWithInventory(const unsigned char &_type)
{
    it2 = active_inventory_list.begin();
    while (it2 != active_inventory_list.end()) {
        
        if (getTypeWithObject(*it2) == _type)
        {
            (*it2)->setVisible(false);
            break;
        }
            it2++;
    };
    if(it2 != active_inventory_list.end())
        active_inventory_list.erase(it2);
    
    max_pos_scroll = min_pos_scroll + MAX(0,int((active_inventory_list.size() - 1)/4) - 1);
}

void CKInventory::sortVisible()
{
    float diff_height = (win_size.width == 1024)?147:83;
    float stepY = scroll_object->getNode()->getContentSize().height/13;
    float stepX = scroll_object->getNode()->getContentSize().width/4;
    float offsetX = (win_size.width == 1024)?107:52;
    it2 = active_inventory_list.begin();
    int i = 0;
    while (it2 != active_inventory_list.end()) {
            (*it2)->setPosition((i%4)*stepX + offsetX,scroll_object->getNode()->getContentSize().height - int(i/4)*stepY - diff_height);
            i++;
        it2++;
    };
};

void CKInventory::sort()
{
    float diff_height = (win_size.width == 1024)?147:83;
    float stepY = scroll_object->getNode()->getContentSize().height/13;
    float stepX = scroll_object->getNode()->getContentSize().width/4;
    sortVisible();
    float offsetX = (win_size.width == 1024)?107:52;
    it2 = backSlot_list.begin();
    int j = 0;
    while (it2 != backSlot_list.end()) {
        if((*it2)->isVisible())
        {
            (*it2)->setPosition((j%4)*stepX + offsetX,scroll_object->getNode()->getContentSize().height - int(j/4)*stepY - diff_height);
            j++;
        }
        it2++;
    };

};

CKObject* CKInventory::findInInventory(const unsigned char &_type)
{
    it2 = inventory_list.begin();
    while (it2 != inventory_list.end()) {
        if(*(*it2)->getValue() == _type)
        {
            return (*it2);
        }
        it2++;
    }
    return NULL;
};

CKObject* CKInventory::findInSlot(const unsigned char &_type)
{
    for(int i = 500; i < 500 + CK::SIZE_INVENTORY_SELECT;++i)
    {
        if(getTypeWithObject(m_gui->getChildByTag(i)->getChildByTag(1)) == _type)
            return m_gui->getChildByTag(i)->getChildByTag(1);
    };
    return NULL;
};

void CKInventory::returnToInventory(const unsigned char &_type,const unsigned int &_count,bool _replase)
{
    if(_count == 0)
        return;
    
    it2 = active_inventory_list.begin();
    while (it2 != active_inventory_list.end()) { //find this type bomb in invetory if find add count
        if(getTypeWithObject(*it2) == _type)
        {
            if(_replase)
            {
                 setBomb(*it2,_type,_count);
            }
            else
            {
                setBomb(*it2,_type,atoi((*it2)->getChildByTag(1)->getText()) + _count); 
            }
           
            return;
        }
        it2++;
    };
    //push back to invetory bomb
    it2 = inventory_list.begin();
    while (it2 != inventory_list.end()) {
        if(!(*it2)->isVisible())
        {
            setBomb(*it2,_type,_count);
            active_inventory_list.push_back((*it2));
            return;
        };
        it2++;
    }
    max_pos_scroll = min_pos_scroll + MAX(0,int((active_inventory_list.size() - 1)/4) - 1);
};

void CKInventory::setBomb(CKObject* _tobj,const unsigned char &_type,const unsigned int &_count)
{
    if(!_tobj)
        return;
    
    _tobj->setVisible(((_type*_count) != 0));
    updateSlotButton();

    CCLOG("setBomb:: type %d count %d",_type,_count);

    CCSprite* tmp = static_cast<CCSprite*>(_tobj->getNode());
    tmp->setOpacity(255);
    const char* tex_name = CKBonusMgr::Instance().getInventoryNameById(_type).c_str();
    CCSpriteFrame* s_frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(tex_name);
//    if(!s_frame)
//    {
//        CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(CK::loadImage("a_Bomb.plist").c_str());
//    };
    if(s_frame)
    {
        tmp->setTexture(s_frame->getTexture());
        tmp->setTextureRect(s_frame->getRect(), s_frame->isRotated(), tmp->getContentSize());
    }
    else
    {
        CCLOG("setBomb:: Error not file in CCSpriteFrameCache %s",tex_name);
    }
    
    tmp->setVisible(((_type*_count) != 0));
    _tobj->setVisible(((_type*_count) != 0));
    
    _tobj->setValue(_type);

    char str_count[NAME_MAX];
    sprintf(str_count,"%d",_count);
    _tobj->getChildByTag(1)->setText(str_count);
    _tobj->getChildByTag(1)->setVisible(true);
    //sprintf(str_count,"%d",_type%10);
    std::string type_lvl = "|";
    switch (_type%10) {
        case 2:
            type_lvl = "||";
            break;
        case 3:
            type_lvl = "|||";
            break;
        default:
            break;
    }
    _tobj->getChildByTag(2)->setText(type_lvl.c_str());
};

void CKInventory::createTmpBomb()
{
    //init object need to move bomb
    //curent_object.bomb = CCSprite::create(CK::loadImage(base_bomb_name).c_str());
    curent_object.bomb = CCSprite::createWithSpriteFrameName(base_bomb_name);
    curent_object.is_active = false;
    curent_object.bomb->setPosition(ccp(win_size.width/2,win_size.height/2));
    curent_object.count = 0;
    curent_object.type = 0;
    curent_object.bomb->setVisible(false);
    curent_object.bomb->setScale(1.5);
    addChild(curent_object.bomb,CK::zOrderParticle+1);
};

void CKInventory::updateSlotButton()
{
    for(int i = 0; i < CK::SIZE_INVENTORY_SELECT;++i)
    {
        if(!m_gui->getChildByTag(500 + i)->getChildByTag(1)->isVisible())
            m_gui->getChildByTag(300+i)->setEnabled(false);
        else
            m_gui->getChildByTag(300+i)->setEnabled(true);
    }
};

void CKInventory::showUpgradeButton()
{
   // return;
    CKAudioMng::Instance().playEffect("button_pressed");
    CKObject * upg_1 = m_upgrade->getChildByTag(71);
    CKObject * upg_2 = m_upgrade->getChildByTag(72);
    char str[NAME_MAX];
    sprintf(str,"%d", CKBonusMgr::Instance().getInfoById(int(curent_object.type/10)*10 + 1)->cost);
    upg_1->getChildByTag(1)->setText(str);
    sprintf(str,"%d", CKBonusMgr::Instance().getInfoById(int(curent_object.type/10)*10 + 2)->cost);
    upg_2->getChildByTag(1)->setText(str);
    
   // if(!CKFileOptions::Instance().isDebugMode())
    {
        upg_1->setTouch(false);
        upg_1->setEnabled(false);
        upg_2->setTouch(false);
        upg_2->setEnabled(false);
    }
    
 //   if(curent_object.type%10 < 3 )
    {
        int n = CKFileInfo::Instance().isNeedUpgradeBomb(curent_object.type);
        CCLOG("showUpgradeButton:: %d",n);
        if(n == 1 && curent_object.type%10 == 1)
        {
             upg_1->setTouch(true);
             upg_1->setEnabled(true);
        };
        
        if(n == 1 && curent_object.type%10 == 2)
        {
            upg_2->setTouch(true);
            upg_2->setEnabled(true);
        };
        
        if(n == 2)
        {
            if(curent_object.type%10 == 1)
            {
                upg_1->setTouch(true);
                upg_1->setEnabled(true);
                upg_2->setTouch(true);
                upg_2->setEnabled(true);
            };
        }
    };
};

void CKInventory::resizeSlotCall(CCNode* _sender,void *_value)
{
    
    if(int(value_add) == 1)
    {
        value_add = 1.0;
    }
    else
    {
        return;
    }
    
    if(*static_cast<int*>(_value))
    {
        if(count_bomb_in_inventory - int(value_add) >= 0)
        {
            count_bomb_in_slot += int(value_add);
            count_bomb_in_inventory-= int(value_add);
        }
        else
        {
            count_bomb_in_slot += count_bomb_in_inventory;
            count_bomb_in_inventory = 0;
        }
    }
    else
        if(count_bomb_in_slot - int(value_add) >= 0)
        {
            count_bomb_in_slot -= int(value_add);
            count_bomb_in_inventory += int(value_add);
        }
        else
        {
            count_bomb_in_inventory += count_bomb_in_slot;
            count_bomb_in_slot = 0;
        };

    if(count_bomb_in_slot*count_bomb_in_inventory == 0 && previor_bomb_in_inventory == count_bomb_in_inventory)
    {
        CKAudioMng::Instance().playEffect("reset_game");
    }
    else
    {
        CKAudioMng::Instance().playEffect("inventory_slide");
    };
    updateSlider();

};

void CKInventory::showModal(cocos2d::CCNode *_sender, void *_value)
{
    if(!m_gui->getChildByTag(*static_cast<int*>(_value))->getChildByTag(1)->isVisible())
    {
        return;
    };
    
    CK::StatisticLog::Instance().setNewPage(CK::Page_BombUpgrade);
    setCurent(m_gui->getChildByTag(*static_cast<int*>(_value))->getChildByTag(1));
    
    m_upgrade->get()->setVisible(true);
    default_bomb_in_slot = count_bomb_in_slot = curent_object.count;
    curent_object.bomb->setVisible(false);
    
    setBomb(m_upgrade->getChildByTag(10)->getChildByTag(10),curent_object.type,curent_object.count);
    default_bomb_in_inventory =  count_bomb_in_inventory = findCountBombInInventory(curent_object.type);

    m_upgrade->getChildByTag(32)->setValue(curent_object.type);
    CKObject* tmp = findInInventory(curent_object.type);
    if(tmp)
        tmp->getChildByTag(1)->setText("0");
    
    showUpgradeButton();
    
    updateSlider();
};

int CKInventory::findCountBombInInventory(const unsigned char &_type)
{
    it2 = inventory_list.begin();
    while (it2 != inventory_list.end()) {
        if(*(*it2)->getValue() == _type)
        {
            return atoi((*it2)->getChildByTag(1)->getText());
        }
        it2++;
    };
    return 0;
};

void CKInventory::upgradeBomb(void *_d)
{
    if(_d)
    {
        CKObject * upg_1 = m_upgrade->getChildByTag(69 + update_value);
        if(CKFileInfo::Instance().getTotalScore() > atoi(upg_1->getChildByTag(1)->getText()))
        {
            CCLOG("CKInventory::upgradeBomb %d %d",count_bomb_in_inventory,count_bomb_in_slot);
            returnToInventory(curent_object.type,count_bomb_in_inventory,true);
            curent_object.type = char(curent_object.type/10)*10 + update_value;
            setBomb(m_upgrade->getChildByTag(10)->getChildByTag(10),curent_object.type,count_bomb_in_slot);
            count_bomb_in_inventory = findCountBombInInventory(curent_object.type);
            showUpgradeButton();
            updateSlider();
            
            CCLOG("CKInventory::upgradeBomb %d %d",count_bomb_in_inventory,count_bomb_in_slot);
            
            CKAudioMng::Instance().playEffect("upgrade_bomb");
        }
        else
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            int n = CK::Action_Shop;
            menuCall(NULL,&n);
        };
    }else{
        CKAudioMng::Instance().playEffect("button_pressed");
    }
};

void CKInventory::callUpgrade(cocos2d::CCNode *_sender, void *_value)
{
    CKAudioMng::Instance().playEffect("button_pressed");
    
    update_value = MIN(3,MAX(2,*static_cast<int*>(_value)));
    CKObject * upg_1 = m_upgrade->getChildByTag(69 + update_value);
    
    CKHelper::Instance().getDialog()->setTarget(this);
    if(CKFileInfo::Instance().getTotalScore() > atoi(upg_1->getChildByTag(1)->getText()))
    {
        CKHelper::Instance().getDialog()->setConfirmText(LCZ("You have upgrade this bomb?"));
    }
    else
    {
        CKHelper::Instance().getDialog()->setConfirmText(LCZ("You don't have enough coins. Buy coins?"));
    };
    CKHelper::Instance().getDialog()->show(callfuncN_selector(CKInventory::upgradeBomb));
};

void CKInventory::doublTouch(const float &_dt)
{
    is_double_touch = false;
    last_touch_object = NULL;
    unschedule(schedule_selector(CKInventory::doublTouch));
};

void CKInventory::updateSliderTick(const float &_dt)
{
    if(add_bomb_to_slot)
    {
        if(count_bomb_in_inventory - int(value_add) >= 0)
        {
            count_bomb_in_slot += int(value_add);
            count_bomb_in_inventory-= int(value_add);
        }
        else
        {
            count_bomb_in_slot += count_bomb_in_inventory;
            count_bomb_in_inventory = 0;
        }
    }
    else
        if(count_bomb_in_slot - int(value_add) >= 0)
        {
            count_bomb_in_slot -= int(value_add);
            count_bomb_in_inventory += int(value_add);
        }
        else
        {
            count_bomb_in_inventory += count_bomb_in_slot;
            count_bomb_in_slot = 0;
        };
    updateSlider();

       value_add *= 1.1;
};

void CKInventory::callHint(CCNode* _sender,void *_value)
{
    CKAudioMng::Instance().playEffect("button_pressed");
    if(DISABLE_HINTS)
        return;
    CKHelper::Instance().getTutorial()->activeted(this,CK::SCENE_INVENTORY);
    int type_bomb = *static_cast<int*>(_value);
    CKHelper::Instance().getTutorial()->start(3,int(type_bomb));
};

void CKInventory::save()
{
    int _count = 0;
    sum_bomb_in_slot = 0;
    sum_bomb_in_inventory = 0;
    for(int i = 0; i < CK::SIZE_INVENTORY_SELECT;++i)
    {
        CKFileInfo::Instance().setInventorySelect(i,0,0);
        CKObject *tobj = m_gui->getChildByTag(500 + i)->getChildByTag(1);
        
        
        if(tobj->isVisible() && (*tobj->getValue() != 0))
        {
            _count = atoi(tobj->getChildByTag(1)->getText());
            sum_bomb_in_slot += _count;
            
            CKFileInfo::Instance().setInventorySelect(i,*tobj->getValue(),_count);
        }
        else
        {
            CKFileInfo::Instance().setInventorySelect(i,0,0);
        };
    };
   
    CKFileInfo::Instance().clearInventory();
    it2 = inventory_list.begin();
    while (it2 != inventory_list.end()) {
        
        if((*it2)->isVisible())
        {
            _count = atoi((*it2)->getChildByTag(1)->getText());
            sum_bomb_in_inventory += _count;
            CKFileInfo::Instance().addToInventory(*(*it2)->getValue(),_count);
        }
        it2++;
    };
    //Flurry
    ObjCCalls::sendEventFlurry("inventory_change",false, "bombs_in_slots=%d,bombs_non_selected=%d",sum_bomb_in_slot,sum_bomb_in_inventory);
    
    //GA
    ObjCCalls::sendCustomGA(8,"num_bombs_unselected",sum_bomb_in_inventory);
    ObjCCalls::sendCustomGA(9,"num_bombs_selected",sum_bomb_in_slot);
    
    CKFileInfo::Instance().save();
};

void CKInventory::menuCall(CCNode* _sender,void *_value)
{
    switch (*static_cast<int*>(_value)) {
        case CK::Action_KidsMode:
            CKHelper::Instance().show_inventory_km_button = false;
            CKFileOptions::Instance().setKidsMode(true);
            CKFileOptions::Instance().save();
        case CK::Action_Back:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            save();
            this->setVisible(false);
            this->setTouchEnabled(false);
            
            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(CKHelper::Instance().menu_number);
            
        };
            break;
        case CK::Action_Ok:
        {
            CK::StatisticLog::Instance().setNewPage(CK::Page_Inventory);
            CKAudioMng::Instance().playEffect("button_pressed");
            m_upgrade->get()->setVisible(false);
            
            CKObject *tmp = findInSlot(curent_object.type);
            if(tmp && tmp != curent_object.link)// any slot have this type bombs,combine bomb
            {
                setBomb(curent_object.link, curent_object.type, count_bomb_in_slot + getCountWithObject(tmp));
                char str_count[NAME_MAX];
                tmp->setVisible(false);
                sprintf(str_count,"%d",0);
                tmp->getChildByTag(1)->setText(str_count);
            }
            else
            {
                setBomb(curent_object.link, curent_object.type, count_bomb_in_slot);
                if(count_bomb_in_slot == 0)
                {
                    curent_object.link->setVisible(false);
                }
                if(count_bomb_in_inventory == 0)
                {
                    removeWithInventory(curent_object.type);
                }
            }

            returnToInventory(curent_object.type,count_bomb_in_inventory,true);

            sort();
            
            curent_object.link = NULL;
            curent_object.type = 0;
            curent_object.count = 0;
            curent_object.is_active = false;
        };
            break;
        case CK::Action_No:
        {
            CK::StatisticLog::Instance().setNewPage(CK::Page_Inventory);
            CKAudioMng::Instance().playEffect("button_pressed");
            m_upgrade->get()->setVisible(false);
            setBomb(curent_object.link, curent_object.type, default_bomb_in_slot);
            returnToInventory(curent_object.type,default_bomb_in_inventory);
            curent_object.link = NULL;
            curent_object.type = 0;
            curent_object.count = 0;
            curent_object.is_active = false;
        };
            break;

        case CK::Action_Shop:
            CKAudioMng::Instance().playEffect("button_pressed");
            this->setVisible(false);
            this->setTouchEnabled(false);
            save();
            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(CK::SCENE_SHOP);
        default:
            break;
    }
};

void CKInventory::setCurent(CKObject*_tobj)
{
    if(*_tobj->getValue() == 0)
        return;
    
    static_cast<CCSprite *>(_tobj->getNode())->setOpacity(128);
    
    const char* tex_name = CKBonusMgr::Instance().getInventoryNameById(*_tobj->getValue()).c_str();
    
    CCSpriteFrame* s_frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(tex_name);
    curent_object.bomb->setTexture(s_frame->getTexture());
    curent_object.bomb->setTextureRect(s_frame->getRect(), s_frame->isRotated(), curent_object.bomb->getContentSize());
    
    curent_object.bomb->setVisible(true);
    curent_object.link = _tobj;
    curent_object.type =  *_tobj->getValue();
    curent_object.count = atoi(_tobj->getChildByTag(1)->getText());
    curent_object.is_active = true;
    
};

void CKInventory::touchMoveBomb()
{
    CKObject* tmp = touch_move_object;
    //curent_object.is_active = false;
    curent_object.is_active = true;
    if(tmp)
    {
        CKAudioMng::Instance().playEffect("select_bomb");
        setCurent(tmp);
        curent_object.bomb->setPosition(last_point);
        touch_scroll = false;
    };
    unschedule(schedule_selector(CKInventory::touchMoveBomb));
};

CKObject* CKInventory::getFreeSlot(const unsigned char &_type)
{
    for(int i = 0; i < CK::SIZE_INVENTORY_SELECT;++i)
    {
        CKObject * tmp = m_gui->getChildByTag(500 + i)->getChildByTag(1);
        
        if(tmp && tmp->isVisible() && getTypeWithObject(tmp) == _type)
        {
            return tmp;
        };
    }
    for(int i = 0; i < CK::SIZE_INVENTORY_SELECT;++i)
    {
        CKObject * tmp = m_gui->getChildByTag(500 + i)->getChildByTag(1);
        
        if(tmp && !tmp->isVisible())
        {
            return tmp;
        };
    }
    return NULL;
};

void CKInventory::ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    if(!this->isVisible())
        return;
    
    CCTouch* touch =(CCTouch*)*touches->begin();
    if(touch->getID() != 0)//first touch
        return;
    
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    last_pointY = location.y;
    last_point = location;
    touch_scroll = false;
    touch_move = false;
    if(m_upgrade->get()->isVisible())
    {
        // hide modal dialog
        if(!m_upgrade->setTouchBegan(location) && !m_upgrade->getChildByTag(1)->isPointInObject(location)) 
        {
            if(location.x > win_size.width*0.05 && location.x < win_size.width*0.95)
                if(location.y > win_size.height*0.05 && location.y < win_size.height*0.95)
                {
                    int n = CK::Action_No;
                    menuCall(NULL,&n);
                    return;
                }
        };
        
        if(m_upgrade->getChildByTag(100)->isPointInObject(location)) // slider label '+' buuto
        {
            add_bomb_to_slot = true;
            value_add = 1.0;
            schedule(schedule_selector(CKInventory::updateSliderTick), 0.1);
        };
        if(m_upgrade->getChildByTag(101)->isPointInObject(location)) //slider label '-' button
        {
            add_bomb_to_slot = false;
            value_add = 1.0;
            schedule(schedule_selector(CKInventory::updateSliderTick), 0.1);
        };
        CKObject* p_obj= m_upgrade->getTouchObject(location);
        slide_obj = NULL;
        if(p_obj && p_obj->getTag() == 3)
        {
            slide_obj = p_obj;
            slide_obj->setStateImage(1);
            last_progress = p_obj->getParent()->getProgress();
        };
        return;
    };
    is_double_touch = true;
    schedule(schedule_selector(CKInventory::doublTouch),TIME_DOUBLE_TOUCH);
    if(!scroll_object)
        scroll_object = m_gui->getChildByType(CK::GuiScrollView);
    scroll_object->setTouch(false);
    if(m_gui->getChildByTag(-1)->isPointInObject(location)) //touch on scroll
    {
        if(CKHelper::Instance().show_inventory_km_button)
        {
           CKObject* tmp = m_gui->getTouchObject(location);
            if(tmp && tmp->getTag() == CK::Action_KidsMode)
            {
                tmp->setStateImage(1);
                return;
            }
        };
        touch_scroll = true;
        scroll_object->setTouch(false);
                
        CKObject* tmp = scroll_object->getTouchObject(location);

        if(tmp && tmp != scroll_object && tmp->isVisible())
        {
            if(last_touch_object == tmp) // double touch
            {
                CKObject* ptmp = getFreeSlot(getTypeWithObject(tmp));
                if( ptmp != NULL)
                {
                    CKAudioMng::Instance().playEffect("select_bomb");
                    setBomb(ptmp, getTypeWithObject(tmp), getCountWithObject(tmp) + getCountWithObject(ptmp));
                    char str_count[NAME_MAX];
                    tmp->setVisible(false);
                    sprintf(str_count,"%d",0);
                    tmp->getChildByTag(1)->setText(str_count);
                    removeWithInventory(getTypeWithObject(tmp));
                    sort();
                   // m_gui->setActiveScrollFrame(max_pos_scroll); //move scroll to last visible slot
                };
                last_touch_object = NULL;
                return;
            }
            else
            {
                last_touch_object = tmp;
                touch_move_object = NULL;
                touch_move_object =  scroll_object->getTouchObject(last_point);
                schedule(schedule_selector(CKInventory::touchMoveBomb),TIME_TOUCH_MOVE_BOMB);
                curent_object.is_slot = false;
            };
        };
        
        if(touch_scroll)
        {
            min_pos_scroll = -10;
            it2 = inventory_list.begin();
            
            max_pos_scroll = min_pos_scroll + MAX(0,int((active_inventory_list.size() - 1)/4) - 1);

        }
    }
    else // touch slots
    {
        CKObject* tobj = getSlotInPoint(location);
        if(tobj && tobj->isVisible())
        {
            if(last_touch_object == tobj)
            {
                CKAudioMng::Instance().playEffect("cancel_sel_bomb");
                last_touch_object = NULL;
                returnToInventory(getTypeWithObject(tobj),getCountWithObject(tobj));
                char str_count[NAME_MAX];
                tobj->setVisible(false);
                sprintf(str_count,"%d",0);
                tobj->getChildByTag(1)->setText(str_count);
                updateSlotButton();
                sortVisible();//sort();
                max_pos_scroll = min_pos_scroll + MAX(0,int((active_inventory_list.size() - 1)/4) - 1);
                m_gui->setActiveScrollFrame(max_pos_scroll); //move scroll to last visible slot
                
                return;
            }
            else
            {
                last_touch_object = tobj;
            };
            //if(!is_double_touch)
            {
                touch_move_object =  tobj;
                
                schedule(schedule_selector(CKInventory::touchMoveBomb),TIME_TOUCH_MOVE_BOMB);
                curent_object.is_slot = true;
            }
        };
        if(m_gui->setTouchBegan(location))
        {
            unschedule(schedule_selector(CKInventory::touchMoveBomb));
        };
        scroll_object->setTouch(true);
        touch_scroll = false;
        CCLOG("ccTouchesBegan::point not in scroll");
    };
    return;
};

void CKInventory::ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent)
{
    CCTouch* touch =(CCTouch*)*pTouches->begin();
    if(touch->getID() != 0)//first touch
        return;
    
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    
    if(m_upgrade->get()->isVisible())
    {
        if(slide_obj)
        {
            float persent = last_progress + (location.x - last_point.x)/m_upgrade->getChildByType(CK::GuiSlider)->getLong();
            
         //   CCLog("ccTouchesMoved:: size width %f",);

            if(persent > 1.0)
            {
                persent = 1.0;
            }
            else
                if(persent < 0.0)
                {
                    persent = 0.0;
                }
            
            
            int sum = (count_bomb_in_inventory + count_bomb_in_slot);
            count_bomb_in_slot = persent * sum;
            count_bomb_in_inventory = sum - count_bomb_in_slot;
            
            
            if(previor_bomb_in_inventory != count_bomb_in_inventory)
            {
                CKAudioMng::Instance().playEffect("inventory_slide");
            };
            updateSlider();
        };
        return;
    }
    
    if(touch_scroll)
    {
        touch_move = true;
        unschedule(schedule_selector(CKInventory::touchMoveBomb));
        float sizeFrameY = scroll_object->getNode()->getContentSize().height/13;
        float moved = (location.y - last_pointY);

        if(active_inventory_list.size() > NEED_ACTIVE_FOR_START_MOVE_SCROLL)
        {
            if( (scroll_object->getPos().y + moved + sizeFrameY*2) < min_pos_scroll*sizeFrameY)
            {
                float x2 = fabs(scroll_object->getPos().y + moved + sizeFrameY*2);
                float x1 = fabs(min_pos_scroll*sizeFrameY);
                scroll_object->moveY(moved*(1.0 - MIN(1.0,(x2 - x1)/(sizeFrameY*3))));
            }
            else
            {
                    if((scroll_object->getPos().y + moved  - sizeFrameY) > max_pos_scroll*sizeFrameY)
                    {
                        
                        float x2 = fabs(scroll_object->getPos().y + moved  - sizeFrameY);
                        float x1 = fabs(max_pos_scroll*sizeFrameY);
                        scroll_object->moveY(moved*(1.0-MIN(1.0,(x1 - x2)/(sizeFrameY*3))));
                    }
                    else
                    {
                        scroll_object->moveY(moved);    
                    }
            }
        }
            
        float pos = scroll_object->getPos().y/sizeFrameY;
        
        if(pos > max_pos_scroll)
            pos = max_pos_scroll;
        if(pos < min_pos_scroll)
            pos = min_pos_scroll;
        
        pos -= min_pos_scroll;
        pos /=(max_pos_scroll - min_pos_scroll);
  
      //  m_gui->getChildByType(CK::GuiSlider)->setProgress(1.0 - pos);
    }
    else
    {
        curent_object.bomb->setPosition(location);
    }
    
    return;
};

void CKInventory::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
{
    CCTouch* touch =(CCTouch*)*pTouches->begin();
    if(touch->getID() != 0)//first touch
        return;
    
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    
    if(m_upgrade->get()->isVisible()) //is modal frame upgrade
    {
        if(slide_obj)
        {
             if(count_bomb_in_inventory*count_bomb_in_slot == 0 )
                CKAudioMng::Instance().playEffect("reset_game");
            slide_obj->setStateImage(0);
        }
        slide_obj = NULL;
        value_add = 1.0;
        unschedule(schedule_selector(CKInventory::updateSliderTick));
        m_upgrade->setTouchEnd(location);
        return;
    }
    unschedule(schedule_selector(CKInventory::touchMoveBomb));
    
    if(CKHelper::Instance().show_inventory_km_button)
    {
        CKObject* tmp = m_gui->getTouchObject(location);
        if(tmp && tmp->getTag() == CK::Action_KidsMode)
        {
            tmp->setStateImage(0);
            int d = CK::Action_KidsMode;
            menuCall(NULL, &d);
            return;
        }
    };
    
    if(curent_object.is_active)
    {
        if(curent_object.is_slot)//is object drag with slot
        {
            CKObject* tobj = getSlotInPoint(location);
            if(tobj == curent_object.link) // return bomb to self slot
            {
                curent_object.is_active = false;
                curent_object.bomb->setVisible(false);
                tobj->setVisible(true);
                static_cast<CCSprite *>(tobj->getNode())->setOpacity(255);
            }
            else
            {
                if(tobj && tobj->isVisible())// swap bomb in slot
                {
                    int count = getCountWithObject(tobj);
                    int type = getTypeWithObject(tobj);
                    setBomb(tobj, curent_object.type, curent_object.count);
                    if(curent_object.count == 0)
                    {
                        curent_object.bomb->setVisible(false);
                    };
                    curent_object.is_active = false;
                    setBomb(curent_object.link, type, count);
                    curent_object.bomb->setVisible(false);
                }
                else//return bomb with slot to inventory
                {
                    CKAudioMng::Instance().playEffect("cancel_sel_bomb");
                    if(tobj && !tobj->isVisible())//move bomb to slot
                    {
                        setBomb(tobj, curent_object.type, curent_object.count);
                        curent_object.is_active = false;
                        setBomb(curent_object.link, curent_object.type, 0);
                        curent_object.bomb->setVisible(false);
                        //curent_object.link->setVisible(false);
                        static_cast<CCSprite *>(curent_object.link->getNode())->setOpacity(255);
                     //   m_gui->setActiveScrollFrame(max_pos_scroll); //move scroll to last visible slot
                    }
                    else
                    {
                        
                        returnToInventory(curent_object.type,curent_object.count);
                        curent_object.is_active = false;
                        curent_object.bomb->setVisible(false);
                        char str_count[NAME_MAX];
                        curent_object.link->setVisible(false);
                        sprintf(str_count,"%d",0);
                        curent_object.link->getChildByTag(1)->setText(str_count);
                        max_pos_scroll = min_pos_scroll + MAX(0,int((active_inventory_list.size() - 1)/4) - 1);
                        m_gui->setActiveScrollFrame(max_pos_scroll); //move scroll to last visible slot
                    }
                }
                updateSlotButton();
                sort();
            }
        }
        else //
        {
            CKObject* tobj = getSlotInPoint(location);
            if(tobj)
            {
                CKAudioMng::Instance().playEffect("select_bomb");
                if(tobj->isVisible()) 
                {
                    if(getTypeWithObject(tobj) == curent_object.type)//add bomb to slot
                    {
                        setBomb(tobj, curent_object.type, curent_object.count + getCountWithObject(tobj));
                        
                    }
                    else //swap bomb
                    {
                        int t_type = getTypeWithObject(tobj);
                        int t_count = getCountWithObject(tobj);
                        setBomb(tobj, curent_object.type, curent_object.count);
                        setBomb(curent_object.link, t_type, t_count);
                        sortVisible();
                        curent_object.is_active = false;
                        curent_object.bomb->setVisible(false);
                        return;
                    }
                    
                }
                else //insert bomb into slot
                {
                    tobj->setVisible(true);
                    setBomb(tobj, curent_object.type, curent_object.count);
                };
                curent_object.is_active = false;
                curent_object.bomb->setVisible(false);
                setBomb(curent_object.link, curent_object.type, 0);//clear in inventory bomb
                removeWithInventory(curent_object.type);
                
                sortVisible();

                return;
            };
            
            if(curent_object.is_active)
            {
                curent_object.is_active = false;
                CCAction* action = CCSequence::create(CCMoveTo::create(MIN(CK::getPointLength(curent_object.bomb->getPosition(),last_point)/500.f,0.4), last_point),CCCallFuncN::create(curent_object.bomb, callfuncN_selector(CKInventory::hideNode)),NULL);
                static_cast<CCSprite *>(curent_object.link->getNode())->setOpacity(255);
                curent_object.bomb->runAction(action);
            }
        }
    }
    else
    {
       //curent_object.bomb->setVisible(false);
    }
    
    if(touch_scroll) // align scroll
    {
//        if (touch_move) {
//            CKAudioMng::Instance().playEffect("run_scroll");
//        }
        touch_move = false;
        
        float sizeFrameY = scroll_object->getNode()->getContentSize().height/13;
        int cur = int(scroll_object->getPos().y/sizeFrameY - 0.1);
       // bool p = false;
        if(cur > max_pos_scroll)
        {
            cur = max_pos_scroll;
        }
        if(cur < min_pos_scroll)
        {
            cur = min_pos_scroll;
        }
        touch_scroll = false;

        m_gui->setActiveScrollFrame(cur);
    }
    else
    {
        scroll_object->setTouch(false);
        m_gui->setTouchEnd(location);
        scroll_object->setTouch(true);
    }
    
    return;
};

inline const unsigned char CKInventory::getTypeWithObject(CKObject* _obj)
{
    return *_obj->getValue();
};

inline const unsigned int CKInventory::getCountWithObject(CKObject* _obj)
{
    return atoi(_obj->getChildByTag(1)->getText());
};

void CKInventory::updateSlider()
{
    if(previor_bomb_in_inventory == count_bomb_in_inventory)
    {
        return;
    }
    previor_bomb_in_inventory = count_bomb_in_inventory;
    CKAudioMng::Instance().playEffect("inventory_slide");
    
    char str_count[NAME_MAX];
    sprintf(str_count,"%d",count_bomb_in_inventory);
    m_upgrade->getChildByTag(50)->setText(str_count);
    sprintf(str_count,"%d",count_bomb_in_slot);
    m_upgrade->getChildByTag(10)->getChildByTag(10)->getChildByTag(1)->setText(str_count);
    m_upgrade->getChildByType(CK::GuiSlider)->setProgress(float(count_bomb_in_slot)/float(count_bomb_in_inventory + count_bomb_in_slot));
    
    CKObject * upg_1 = m_upgrade->getChildByTag(71);
    CKObject * upg_2 = m_upgrade->getChildByTag(72);
    char str[NAME_MAX];
    int cost = CKBonusMgr::Instance().getInfoById(int(curent_object.type/10)*10 + 2)->cost - CKBonusMgr::Instance().getInfoById(int(curent_object.type/10)*10 + 1)->cost;
    sprintf(str,"%d", cost*count_bomb_in_slot);
    upg_1->getChildByTag(1)->setText(str);
    cost = CKBonusMgr::Instance().getInfoById(int(curent_object.type/10)*10 + 3)->cost - CKBonusMgr::Instance().getInfoById(int(curent_object.type/10)*10+1)->cost;
    sprintf(str,"%d",cost*count_bomb_in_slot);
    upg_2->getChildByTag(1)->setText(str);
};

CKObject* CKInventory::getSlotInPoint(const CCPoint &_point)
{
  //  CKObject* m_object = m_gui->getTouchObject(_point);
    for(int i = 500; i < 500 + CK::SIZE_INVENTORY_SELECT;++i)
    {
        if(m_gui->getChildByTag(i)->isPointInObject(_point))
        {
            
            return m_gui->getChildByTag(i)->getChildByTag(1);
            break;
        };
    }
    return NULL;
};

void CKInventory::reload()
{
    if(!m_gui)
        return;
    
    char str_total[NAME_MAX];
    sprintf(str_total,"%d",CKFileInfo::Instance().getTotalScore());
    m_gui->getChildByTag(51)->setText(str_total);
    
    
    //init and create inventory with file
    std::vector<CK::Inventory> m_inv =  CKFileInfo::Instance().getInventory();
      CCLOG("reload::INVENTORY_SIZE %d",m_inv.size());
    std::vector<CK::Inventory>::iterator it = m_inv.begin();
    it = m_inv.begin();
    it2 = inventory_list.begin();
    while ( it2 != inventory_list.end()) {
        (*it2)->setVisible(false);
        it2++;
    };
    it2 = inventory_list.begin();
    while (it !=  m_inv.end() && it2 != inventory_list.end()) {
        unsigned char type_ = (*it).type;
        if(CKFileOptions::Instance().isDebugMode())
        {
            type_ = int(type_/10)*10 + CKFileOptions::Instance().getBombLvl();
        }
        setBomb(*it2,type_,(*it).count);
        active_inventory_list.push_back(*it2);
        it++;
        it2++;
    };
    
    sort();
    
    //init slot
   for(int i = 0; i < CK::SIZE_INVENTORY_SELECT;++i)
    {
        CKObject *tobj = m_gui->getChildByTag(500 + i)->getChildByTag(1);
        tobj->setVisible(false);
      //  CCLOG("INVENTORY_SIZE %d",i);
        tobj->getChildByTag(1)->setText("0");
        
            if(CKFileInfo::Instance().getInventorySelect(i).type != 0 && CKFileInfo::Instance().getInventorySelect(i).count != 0)
            {
                setBomb(tobj,CKFileInfo::Instance().getInventorySelect(i).type,CKFileInfo::Instance().getInventorySelect(i).count);
            };
    };
    m_gui->reloadAtlas();
    updateSlotButton();
};

void CKInventory::hideNode(CCNode *_sender)
{
    _sender->setVisible(false);
};

void CKInventory::activeted(cocos2d::CCLayer *_node)
{
        cocos2d::CCLayer *m_layer = (cocos2d::CCLayer *)this;
        reload();
        m_parent = _node;
        m_parent->setVisible(false);
        m_parent->setTouchEnabled(false);
        m_layer->setTouchEnabled(true);
        m_layer->setVisible(true);
}
