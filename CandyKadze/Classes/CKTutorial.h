//
//  CKTutorial.h
//  CandyKadze
//
//  Created by PR1.Stigol on 17.01.13.
//
//

#ifndef __CandyKadze__CKTutorial__
#define __CandyKadze__CKTutorial__

#include <iostream>
#include "cocos2d.h"
#include <list.h>
#include <vector.h>

class CKGUI;
class CKObject;
using namespace cocos2d;

class CKModalLayer
{
protected:
    CCLayer* m_parent;
    virtual void funcActive() = 0;
    virtual void funcDeactive() = 0;
public:
    virtual void deactived() = 0;
    virtual void activeted(CCLayer* _node) = 0;
};

class CKTutorialScriptScene;
class CKScriptLayerBase;
class CKCubeArray;
class CKTutorial: public cocos2d::CCLayer
{
    const int GUI_TAP_ID = 8;
    const int COUNT_FRAME_BOMB = 13;
    const int COUNT_FRAME_PLANE = 8;
    const int COUNT_FRAME_GENERAL = 3;
    const int UNDERLAER_ID = 41;
    CKGUI *m_gui;
    CCLayer* m_parent;
    CCSize win_size;
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesCancelled(CCSet *pTouches, CCEvent *pEvent){ccTouchesEnded(pTouches, pEvent);};
    
    CKTutorialScriptScene* script_bomb;
    CKTutorialScriptScene* script_plane;
    CKTutorialScriptScene* script_rtt;
    CCDictionary *m_dict;
    
    CCRenderTexture* m_rtt;
    std::map<int, int> map_tutorial_bomb;
   // std::map<int, std::string> map_tutorial_plane;
    std::map<int, int>::iterator map_it;
    std::vector<int> list_bomb_name;
    std::vector<int> list_plane_name;
    
    std::vector<CKObject*>  list_bomb;
    std::vector<CKObject*>  list_general;
    std::vector<CKObject*>  list_plane;
    std::vector<CKObject*>::iterator it;
    CKObject* last_tap;
    int curent_tap;
    float panel_left;
    CCRenderTexture *rtt;
    CCRenderTexture *frame_rtt[3];
    CKScriptLayerBase *frame_base[3];
    
    CKCubeArray *m_cube[3];
    
    CCSprite *m_bg_mask;
    
    void menuCall(CCNode* _sender,void *_value);
    void tapCall(CCNode* _sender,void *_value);
    void showHintBomb(CCNode* _sender,void *_value);
    void showHintPlane(CCNode* _sender,void *_value);
    void initGui();
    void showFrameBomb();
    void showFramePlane();
    void showFrameGeneral();
    void initBombList();
    void initPlaneList();
    void initGeneralList();
    void setVisibleList(const std::vector<CKObject*> &_list,bool _visible);
    void initList();
    void initFramesScriptBomb();
    void initFramesScriptPlane();
    void releaseRTT();
    void createRTT();
    
   
public:
    CKTutorial();
    ~CKTutorial();
    void start(int _tap,int _frame = -1);
    void activeted(CCLayer* _node,int scene_num);
    void deactived();
  
};

#endif /* defined(__CandyKadze__CKTutorial__) */
