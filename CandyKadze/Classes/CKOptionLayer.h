//
//  CKOptionLayer.h
//  CandyKadze
//
//  Created by PR1.Stigol on 13.06.13.
//
//

#ifndef __CandyKadze__CKOptionLayer__
#define __CandyKadze__CKOptionLayer__

#include <iostream>
#include "cocos2d.h"
#include <list.h>
#include <vector.h>

class CKGUI;
class CKObject;
using namespace cocos2d;

class CKOptionLayer: public cocos2d::CCLayer
{
    CKGUI *m_gui,*m_dialog,*m_dialogkm_on,*m_dialog_wait,*m_info;
    CCRenderTexture *m_info_rtt;
    CCNode *group_label_info;
    CCSprite *back_info;
    static const int TIME_RESET_GAME = 5;
    
    CCPoint  last_pos;
    CCSize win_size;
    bool can_resetgame,touch_info;
    float info_top_posY;
    float info_bootom_posY;
    int click_buyIapp;
    
    void initGui();
    void initKMDialog();
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesCancelled(CCSet *pTouches, CCEvent *pEvent){ccTouchesEnded(pTouches, pEvent);};
    void menuCall(CCNode* _sender,void *_value);
    void closeInfo(CCNode *_sender,void *_value);
    int num_language;

    void resetGame();
    void callDialog(CCNode *_sender,void *_value);
    void callKM(CCNode *_sender,void *_value);
    void loadIcloud(CCNode *_sender);
    void enableEveryplay(CCNode *_sender);
    void timeResetGame();
    void runStartAnimation();
    
    void initGuiInfo();
    void initDialogWait();
    void showDialogWait(bool _loading = false);
    void callbackLanguage(CCNode *_sender,void *data);
    void hideDialogWait();
    void hideNode(CCNode *_node);
public:
    void callbackIAP(CCNode * _sender,void *_value);
    CKOptionLayer();
    virtual ~CKOptionLayer();
    void update(float dt);
    static CCScene* scene();
};
#endif /* defined(__CandyKadze__CKOptionLayer__) */
