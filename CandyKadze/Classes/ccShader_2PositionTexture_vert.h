"                                                        \n\
attribute vec4 a_position;                                \n\
attribute vec2 a_texCoord;                                \n\
attribute vec2 a_maskCoord;                                \n\
uniform    mat4 u_MVPMatrix;                                \n\
\n\
#ifdef GL_ES                                            \n\
varying mediump vec2 v_texCoord;                        \n\
varying mediump vec2 v_maskCoord;                        \n\
#else                                                    \n\
varying vec2 v_texCoord;                                \n\
varying vec2 v_maskCoord;                                \n\
#endif                                                    \n\
\n\
void main()                                                \n\
{                                                        \n\
gl_Position = u_MVPMatrix * a_position;                \n\
v_texCoord = a_texCoord;                            \n\
v_maskCoord = a_maskCoord;                              \n\
}                                                        \n\
";
