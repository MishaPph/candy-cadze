//
//  CKScriptScene.h
//  CandyKadze
//
//  Created by PR1.Stigol on 08.04.13.
//
//

#ifndef __CandyKadze__CKScriptScene__
#define __CandyKadze__CKScriptScene__

#include <iostream>
#include "cocos2d.h"
#include <list>
#include <vector>

using namespace cocos2d;

class CKScriptLayerBase;
class CKBombScriptLayer;
class CKPlaneScriptLayer;
class CKScript;

class FrameRTT
{
public:
    CCRenderTexture * rtt;
    CKScriptLayerBase* layer;
   // CKBombScriptLayer* s_bomb;
   // CKPlaneScriptLayer* s_plane;
   // CKScript *script;
    int frame;
    FrameRTT();
    void render();
};
class CKCubeArray;
class CKLayerScript;
class CKTutorialScriptScene: public cocos2d::CCLayer
{
    
    class ScripRT{
    public:
        CCPoint _pos;
        int type;
        int index;
    };
    static const int FRAME_POOL_SIZE = 3;
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesCancelled(CCSet *pTouches, CCEvent *pEvent){ccTouchesEnded(pTouches, pEvent);};
    
    CCPoint last_posotion,touch_began_position;
    CCRenderTexture* m_rtt;
    int COUNT_RTT_SCRIPT;
    ScripRT* sc_rt[13];
    int curent_k;
    FrameRTT frame[FRAME_POOL_SIZE];
    //CKScriptLayerBase * m_base[FRAME_POOL_SIZE];
    CCDictionary *m_dict;
    bool active_rtt_create;
    
    //ScripRT* cur_sc;
    CCNode * m_node;
    CCSize size_frame,win_size;
    
    int curent_frame;
    
    bool start_scroll;
    float left_scroll_pos,right_scroll_pos,size_frame_x;
    float pause_time;
    int getFreeRTTFrame(int _index);
    void initLayer();
    bool touch_in_scroll;
    int previour_lvl_bomb;
    CKLayerScript *script[3];
public:
    void createAllFrame(CCDictionary *_dict);
    virtual void draw();
    void setRTT(CCRenderTexture* _rtt,CCRenderTexture **_rtt_frame);
    void update(float dt);
    void setCurentFrameNumb(int _frame,bool _anim = true,int type = 0);
    void setEnable(bool _en);
    void setPreviourLvl(const int _lvl);
    int getCurentFrameNumb()
    {
        return curent_frame;
    };
    void reload();
    bool isActiveRTTFrame(int _index);
    void setActiveRTTFrame(int _index);
    void initPlaneFrame(const CCSize &_sizeframe,const CCPoint &_pos);
    void initBombFrame(const CCSize &_sizeframe,const CCPoint &_pos);
    CKTutorialScriptScene();
    ~CKTutorialScriptScene();
};
#endif /* defined(__CandyKadze__CKScriptScene__) */
