//
//  CKInventory.h
//  CandyKadze
//
//  Created by PR1.Stigol on 09.10.12.
//
//

#ifndef __CandyKadze__CKInventory__
#define __CandyKadze__CKInventory__

#include <iostream>
#include "cocos2d.h"
#include "CKButton.h"
#include "CKHelper.h"
#include "CKBonusMgr.h"
#include "CKFileOptions.h"
#include "CKFileInfo.h"


class CKGUI;
class CKObject;
using namespace cocos2d;

struct InventoryObject
{
    CCSprite* bomb;
    CCLabelTTF *label;
    
    
    unsigned int count;
    unsigned char type;
    
    bool is_active;
    //InventoryObject* link;
    CKObject* link;
    bool is_slot;
};


class CKInventory : public cocos2d::CCLayer {
public:
    ~CKInventory();
    CKInventory();
    
    void reload();
    void activeted(cocos2d::CCLayer *_node);
    void deactive();
    
    static CCScene* scene();
    // returns a Scene that contains the HelloWorld as the only child
   // static cocos2d::CCScene* scene();
    //    virtual bool init();
private:
    CCLayer* m_parent;
    const float TIME_TOUCH_MOVE_BOMB = 0.15;
    const float TIME_DOUBLE_TOUCH = 0.2;
    int count_bomb_in_slot,count_bomb_in_inventory,default_bomb_in_slot,default_bomb_in_inventory;
    int previor_bomb_in_inventory;
    int min_pos_scroll,max_pos_scroll,update_value;
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesCancelled(CCSet *pTouches, CCEvent *pEvent){ccTouchesEnded(pTouches, pEvent);};
    
    std::vector<CKObject*> inventory_list;
    std::vector<CKObject*> active_inventory_list;
    std::vector<CKObject*> backSlot_list;
    std::vector<CKObject*>::iterator it2,it3;
    void sortVisible();
    int count_visible;
    
    int sum_bomb_in_slot,sum_bomb_in_inventory;//only after method save, need to "send event" Flurry
    
   // InventoryObject cell_inv[4];
    bool touch_scroll;
    bool touch_move;
    bool add_bomb_to_slot;
    bool is_double_touch;
    float value_add;
    CKGUI* m_gui;
    CKGUI* m_upgrade;
    CKObject* touch_move_object;
    //CCPoint last_pos;
    CCNode* m_slider;
    CKObject* scroll_object;
    CKObject* slide_obj;
    CCSprite* m_message;

    CCSize win_size;

    CCPoint last_point;
    InventoryObject curent_object;
    int index_bomb_hint;
    float last_pointY,last_progress;
    CCLabelTTF *label_info;
    
    int curent_load;
    void load();
   // void functAsunc(CKInventory * _sender);
    
    void updateInventorySelect();
    void updateSlider();
    void createSelectPanel();
    void upgradeBomb(void *_d);
    void menuCall(CCNode* _sender,void *_value);
    void callHint(CCNode* _sender,void *_value);
    void callUpgrade(CCNode* _sender,void *_value);
    void showModal(CCNode* _sender,void *_value);
    void updateSliderTick(const float &_dt);
    void doublTouch(const float &_dt);
    void showUpgradeButton();
    void resizeSlotCall(CCNode* _sender,void *_value);
    CKObject* getFreeSlot(const unsigned char &_type);
    CKObject* last_touch_object;
    int findCountBombInInventory(const unsigned char &_type);
    void save();
    void createTmpBomb();
    void updateSlotButton();
    void setBomb(CKObject*_tobj, const unsigned char &_type,const unsigned int &_count);
    void returnToInventory(const unsigned char &_type,const unsigned int &_count,bool _replace = false);
    void removeWithInventory(const unsigned char &_type);
    void hideNode(CCNode *_sender);
    CKObject* getSlotInPoint(const CCPoint &_point);
    void setCurent(CKObject*_tobj);
    void touchMoveBomb();
    void sort();
    CKObject* findInInventory(const unsigned char &_type);
    CKObject* findInSlot(const unsigned char &_type);
    const unsigned char getTypeWithObject(CKObject* _obj);
    const unsigned int getCountWithObject(CKObject* _obj);
};

#endif /* defined(__CandyKadze__CKInventory__) */
