//
//  CKTutorialScriptLayer.h
//  CandyKadze
//
//  Created by PR1.Stigol on 03.04.13.
//
//

#ifndef __CandyKadze__CKTutorialScriptLayer__
#define __CandyKadze__CKTutorialScriptLayer__

#include <iostream>
#include "cocos2d.h"
#include <list>
#include <vector>

using namespace cocos2d;
class CKAirplane;
class CKBombs;
class CKCubeArray;
class CKGUI;
class CKObject;

//const int COUNT_RTT_SCRIPT = 13;
class CKTutorialScriptLayer;
class CKScriptLayerBase;

class CKScriptLayerBase;
class CKLayerScript : public cocos2d::CCLayer
{
    public:
    CKCubeArray* m_cube_array;
    CKGUI * m_gui;
    CKAirplane* m_airplane;
    CKBombs* m_bombs;
    CKBombs* m_bomb_ghost;
    CKObject* curent_node;
    CCSprite *sprite_hand;
    CCSprite *sprite_accelerate;
    CKScriptLayerBase *m_base;
public:
    void initObject();
    void initBomb(int ground_height,int BOX_SIZE);
    void initCube(int ground_height,CCArray *_array);
    float initPlane(int ground_height);
    void update(float _dt);
    void startActionAcc();
    void stopActionAcc();
    void schedureAction();
    void showType(CCNode *_node,void *_d);
    void menuCall(CCNode* _sender,void *_data);
    CKLayerScript();
    ~CKLayerScript();
};

class CKScriptLayerBase: public CCObject
{
protected:
    bool is_load;
    CKLayerScript *m_script;
    CCArray * m_array;
public:
    const int LABEL_BUY_ID = 33;
    const int BUTTON_BUY_ID = 32;
    CCSize win_size;
    int action_type;
    int plane_height;
    int type;
    bool start_schedure_action;
    
    float BOX_SIZE;
    float ground_height;
    float start_pos_plane;
    float action_start;
    float frame_size_x;
    float speed_plane;
    float previor_pos_plane_x;
    float create_new_bomb;
    float panel_left;
    bool need_reload;
    bool is_end;
    bool is_mono;
    bool can_run_bonus;
    bool active_action;
    bool have_action;
    bool active_action_hand;
    bool  is_reload_loop;
    CCDictionary *m_dict;

    void newLoop();
    void initGui();

    virtual void updateSlotState(){};
    virtual void initWithDict(CCDictionary  *_dict){};

public:
    CKLayerScript *getLayer()
    {
        return m_script;
    };
    CKScriptLayerBase();
    virtual ~CKScriptLayerBase();
    CKGUI* getGui();
    void setMono(bool _enable);
    void reload();
    
   // void refObject(CKScriptLayerBase *_base);
    const int& getType() const
    {
        return type;
    };
    void setTouchBegan(const CCPoint &_pos);
    void setTouchEnd(const CCPoint &_pos);
    void copy(const CKScriptLayerBase &_base);
    void setScript(CKLayerScript *_layer)
    {
        
    };
    virtual void pause();
    virtual void start();
    virtual void initWithType(CCDictionary *_dict,const int &_type,const float &frame_size_x);
    virtual void update(float dt);
    virtual void showType(CCNode* _sender,void *_data){};
    virtual void menuCall(CCNode* _sender,void *_data);
    virtual void reloadForType(int _newtype){};
   // virtual void reParent();
  //  virtual CCObject *copyWithZone(CCZone *pZone);
};

class CKBombScriptLayer: public CKScriptLayerBase
{
    virtual void updateSlotState();
    virtual void initWithDict(CCDictionary  *_dict);
    void saveCurentBombType();
    void schedureAction();
    void actionHand();
    void actionAccelat();
public:
  //  CKBombScriptLayer(const  CKScriptLayerBase &_base): CKScriptLayerBase(_base){};
    virtual void reloadForType(int _newtype);
    virtual void initWithType(CCDictionary *_dict,const int &_type,const float &frame_size_x);
    virtual void update(float dt);
    virtual void showType(CCNode* _sender,void *_data);

    CKBombScriptLayer(CKLayerScript *_layer);
    ~CKBombScriptLayer();
};

class CKPlaneScriptLayer: public CKScriptLayerBase
{
    virtual void updateSlotState();
    void createBomb();
    virtual void initWithDict(CCDictionary  *_dict);
    void setPlaneSetting();
    void actionHand();
public:
    virtual void reloadForType(int _newtype);
    virtual void initWithType(CCDictionary *_dict,const int &_type,const float &frame_size_x);
    virtual void update(float dt);
    virtual void showType(CCNode* _sender,void *_data);
    CKPlaneScriptLayer(CKLayerScript *_layer);
    ~CKPlaneScriptLayer();
};


#endif /* defined(__CandyKadze__CKTutorialScriptLayer__) */
