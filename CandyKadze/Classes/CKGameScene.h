//
//  CKGameScene.h
//  CandyKadze
//
//  Created by PR1.Stigol on 18.09.12.
//
//

#ifndef __CandyKadze__CKGameScene__
#define __CandyKadze__CKGameScene__

#include <iostream>
#include <cocos2d.h>
#include <list.h>
#include <map.h>
#include "CK.h"
#include "CKHelper.h"
#include "CKAirplane.h"
#include "CKBomb.h"
#include "CKCubeArray.h"
#include "CKButton.h"
#include "CK.h"
#include "CKLoaderParallax.h"
#include "CKBonusMgr.h"

#include "CKStaticInfo.h"
#include "CKSystemInfo.h"
#include "CKGameSceneBase.h"
#include "TBSpriteMask.h"

using namespace CK;


class CKTutorialScriptOneScene;

struct PanelObject
{
    CCSprite* bomb;
    CCLabelTTF *label;
    CKObject* slot;
    unsigned int count;
    unsigned char type;
    
    int number;
    bool is_active;
    InventoryObject* link;
    //int current_link;
};
//class CKGameSceneNew;
struct CKAccelerometerDelegat;


class CKGameSceneNew :public cocos2d::CCLayer{
public:
   // friend void* proxy_function(void*);
    virtual ~CKGameSceneNew();
    CKGameSceneNew();
    // returns a Scene that contains the HelloWorld as the only child
    static cocos2d::CCScene* scene();
    
    virtual void draw();
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesCancelled(CCSet *pTouches, CCEvent *pEvent){ccTouchesEnded(pTouches, pEvent);};
    
    bool touchShareEnded(cocos2d::CCSet* touches);
    bool touchShareBegan(cocos2d::CCSet* touches);
    
//    virtual void onExit();
    virtual void onEnter();
    virtual bool initGame();
    virtual void didAccelerate(CCAcceleration* pAccelerationValue);
    
    void update(float _dt);
    void updateInfo(const float &_dt);
    void showOrHidePauseMenu(bool _show = true);
    void setPause();
    void menuCall(CCNode* _sender,void *_value);
    void shareCall(CCNode* _sender,void *_value);
    void messageCall(CCNode* _sender,void *_value);
    void shopCall(CCNode* _sender,void *_value);
    void showBanner(bool _animation = true);
    void createBanner();
    void setLikes();
    void enableEveryplay(CCNode *_sender);
private:
    int BOX_SIZE;
    int count_accuvate_shots;
    std::string font_name;
    
    std::list<cocos2d::CCLabelTTF*> label_pool;
    std::list<cocos2d::CCLabelTTF*>::iterator label_pool_it;
    
    void incLevelWorld();
    void createLabelScorePool(cocos2d::CCNode *_node);
    void showLabelScore(const cocos2d::CCPoint& _pos,int _score);
    void hideNode(cocos2d::CCNode *_sender);
    
    
    const int BONUS_PROGRESS_ID = 8;
    const float TIME_TO_SHOW_BONUS_PALNE = 0.3;
    const float TIME_TO_START_SHOW_TUTORIAL = 3.0;
    const float TIME_SHOW_FRAME_ANIMATION = 0.3;
    
    const float TIME_TO_BONUS_ANIM = 0.3;
    const float ADD_SCORE_PERSENT = 0.0;
    const int LABEL_TOTAL_SCORE_ID = 51;
    const int MAX_BOMB_ID = 1000;
    static const unsigned char COUNT_PANEL_BOMB_CELL = 5;
    static const unsigned char COUNT_PANEL_PLANE_CELL = 2;
    
    enum
    {
        t_UnlockWorld = 1,
        t_WorldComplite = 2,
        t_UpgradeBombs = 3,
        t_NoBombsAnyWhere = 4,
        t_OutOfBombs = 5,
        t_NoBombsInSlots = 6,
        t_LikeOnFacebook = 7
    };
    enum
    {
        GAME_RUN = 0,
        GAME_PAUSE = 1,
        GAME_WIN = 2,
        GAME_OVER = 3
    };
    typedef std::pair<unsigned char, unsigned int> m_pair;

    std::vector<m_pair > tmp_bonus_list;
    std::vector<m_pair>::iterator _it;
    
    PanelObject bonus_bomb[COUNT_PANEL_BOMB_CELL];
    PanelObject bonus_airplane[COUNT_PANEL_PLANE_CELL];
    cocos2d::CCSize win_size;
    float limitet_top_zone_touch;
    
    CCPoint touch_position;
    CKAudioObject m_audio;
    
    
    int game_state;
    bool score_active;
    //bool is_pause;
    bool is_hiscore;
    bool baner_showed;
    bool bonus_drag_up;
    bool is_ipad;
    bool end_lvl;
    bool is_been_clear_lvl;
    bool  have_everyplay_record;
    float banner_height;
    int panel_left;
    int ground_height;
    int curent_koef_cube_destroy;
    bool is_posted_in_facebook;
    
    unsigned int score,last_score,score_destroy_one_cube;
    unsigned int count_special_bomb;
    
    int curent_level_id;
    int curent_world_id;
    int count_create_default_bomb;
    
    int count_active_touch;
    long time_start;
    long pause_time;
    
    std::string airplane_name;
    std::string bomb_name;
    std::string file_bach;
    
    CCSprite* banner,*bg_mask;
    CCMenu* menu_banner;
    PanelObject *curent_bomb;
    CCRenderTexture* rt;
    //CCSprite * sprite_cloud;
    std::list<CCParticleSystem* > emitter_pool;
    void newLoopCloud(CCNode *_sender,void *_d);
    void newBubleAnim(CCNode *_sender,void *_d);
    void actionCloudChocko(CCNode *_sender,void *_d);
    void createEmitterChocko(CCNode *_sender,void *_d);
    void circleChocko(CCNode *_sender,void *_d);
    void shineIce(CCNode *_sender,void *_d);
    void skewCloud(CCNode *_sender,void *_d);
    void animationWinScore();
    void animationLabelMult();
    
    void loadPositionPool(const char *_file_name);
    std::vector<CCPoint > position_pool;
    std::vector<CCNode *> action_node;
    std::list<CCSprite* > shine_star_pool;
    void createShineStar(CCNode *_sender,CCSprite* _sprite);
    
    CCParticleSystem* m_emiter_snow;
    CCNode* message_sprite;
    CCLabelTTF* label_info;
    CKObject* label_score;
    CCLabelTTF* label_sysinfo;
    CKTutorialScriptOneScene *script_scene;
    
    CCNode* pause_menu_sprite;
    PanelObject* progress_sprite[2];
    
    CKBombs* m_bombs;
    CKBombs* m_bomb_ghost;
    CKParalaxNode* m_backnode;
    
    CKGUI* m_gui_game;
    CKGUI* m_gui_pause;
   // CKGUI* m_info_msg;
    
    CKGUI* m_gui_complite;
    CKGUI* m_gui_failed;
    CKGUI* m_share;
    CKBonus *m_bonus;
    CKAirplane* m_airplane;
    CKCubeArray* m_cube_array;
    TBSpriteMask* cub_mask;
    
    CKAccelerometerDelegat* m_accelerometer;
    void initGuiShare();

    void touchAirplane(const CCPoint& _pos);

    
    void menuCallback(CCNode* _sender,void *_data);

    void hideSlot(CKObject *_object);
    void showSlotBomb(PanelObject &_slot,int _count,int _type);
    void clearSlotBomb(PanelObject &_slot);
    void clearSlotPlane(PanelObject &_slot);
    
    void load();
    void functAsunc(CKGameSceneNew* _sender);
    void startAddScoreFn();
    void initParallax();
    void initAnimationParallax();
    void initGUI();
    void initGamePanelGui();
    void initLevelFailedGui();
    void initLevelCompliteGui();
    void initPauseGui();
    void initPanelBomb();
    void initPanelAirplane();
    void initAirpalne();
    void initBanner(float _plane_height);
    //void initAudio(int _previour_scene);
    void initLabelSysInfo();
    void showEndMenu(bool _win = false);
    void showDialog();
    void runBonusAnimation(CCNode *_node);
    bool addBonusToBombPanel(CCNode *_node);
    bool addBonusToPlanePanel(CCNode *_node);
    
    bool showTutorial(int _tag = 0);
   
    void runBonusPlaneSlot(const int &_num,const int &_ind);
    void slotCall(CCNode* _sender,void *_value);
    void enabledGui(CCNode *_sender,void *_gui);
    void clearScene(bool _full = true);
    void gameWin();
    bool showTutorialScript();
    void gameOver();
    void addScore(int _score);
    
    void saveUserProgress();
    
    void updateBomb(const float &_dt);
    void updateScore(float _dt);
    void updateParticle(const float &_dt);

    void updateBonusProgress(const float &_dt);
    void updatePlanePanelProgress(const int &_num);
    
    void restart();

    void endShowPause();
    void callbackAirplane(CCNode *_sender,void *data);
};

struct  CKAccelerometerDelegat: public CCAccelerometerDelegate
{
    CKGameSceneNew *m_scene;
    virtual void didAccelerate(CCAcceleration* pAccelerationValue)
    {
        m_scene->didAccelerate(pAccelerationValue);
    };
    virtual ~CKAccelerometerDelegat()
    {
        m_scene = NULL;
    };
};

#endif /* defined(__CandyKadze__CKGameScene__) */
