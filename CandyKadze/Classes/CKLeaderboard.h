//
//  CKLeaderboard.h
//  CandyKadze
//
//  Created by PR1.Stigol on 19.06.13.
//
//

#ifndef __CandyKadze__CKLeaderboard__
#define __CandyKadze__CKLeaderboard__

#include <iostream>
#include <cocos2d.h>
#include <list.h>

#include "CK.h"

class CKGUI;
class CKObject;
using namespace cocos2d;

class CKLeaderboard:public CCLayer
{
    struct CKSScroll
    {
        CKObject* m_scroll;
        float frame;
        float posX;
        float height,top_pos,bottom_pos;
        int min_pos,max_pos;
        bool start_anim;
    };
    static const int TABEL_MAIN = 1;
    static const int TABEL_SCORE = 2;
    const float TIME_EFFECT = 0.3;
    const float TIMEOUT_WAIT = 10.0;
    int currnet_leaderboard,current_tap;
    int current_field;
    float offset_leadboard;
    //const char* table_name[12]= {"Total scores","Best High Score","Caramel High Scores","Choco High Scores","Caramel High Scores"};
    CKSScroll* m_lead,*m_score;
    CCRenderTexture *m_view_lead,*m_view_score;
    CKObject *mask_lead,*mask_score,*me_object;
    CKGUI* m_gui_lead,*m_gui_score,*m_dialog_wait;
    std::list<CKObject *> score_objects;
    std::list<CKObject *>::iterator it_object;
    CCPoint last_pos,first_position;
    bool b_touch_move;
    bool touch_scroll;
    int me_tab_position;
    void initLeaderBoard();
    void initScoreBoard();
    void initDialogWait();
    void showDialogWait();
    void hideDialogWait();
    void closeWait();
    void loadScore();
    void returnScroll(CKSScroll* _lead);
    void moveScroll(CKSScroll* _scroll,float _value);
    void setValueToScrollChild(CKObject *_obj,const char *_name,int _score,int _num,int _format);
    void fillScoreTabel(int _field_count = 20);
    
    virtual void ccTouchesBegan(cocos2d::CCSet* pTouches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesCancelled(CCSet *pTouches, CCEvent *pEvent){ccTouchesEnded(pTouches, pEvent);};
    
    void showScoreTabel();
    
    void showTap();
    void loadScoreTabel(const int &_field);
    void fadeTabel(int _num = 1);
    CCLayer *m_parent;
    CCSprite *m_bg_mask;
    CCSize win_size;
    CCRenderTexture *rtt_back;
    int count_visible_rows;
public:
    void showMainTabel(bool _animate);
    void deactived();
    void activeted(cocos2d::CCLayer *_node);
    void menuCall(CCNode *_sender,void *_value);
    void loginCall(CCNode *_sender,void *_value);
    void compliteLoadFriend(CCNode *_sender,void *_value);
    void compliteLoadScore(CCNode *_sender,void *_value);
    void update(float _dt);
    
    CKLeaderboard();
    virtual ~CKLeaderboard();
    void refresh();
    static cocos2d::CCScene* scene();
};
#endif /* defined(__CandyKadze__CKLeaderboard__) */
