//
//  CKTutorialScriptOneScene.h
//  CandyKadze
//
//  Created by PR1.Stigol on 08.04.13.
//
//

#ifndef __CandyKadze__CKTutorialScriptOneScene__
#define __CandyKadze__CKTutorialScriptOneScene__

#include <iostream>
#include "cocos2d.h"
#include <list>
#include <vector>

using namespace cocos2d;
class CKGUI;
class CKScriptLayerBase;
class ScripRT;
class CKLayerScript;

class CKTutorialScriptOneScene: public CCLayer
{
    class ScripRT{
    public:
        CKScriptLayerBase* layer;
        CCRenderTexture *rt;
        int type;
    };
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesCancelled(CCSet *pTouches, CCEvent *pEvent){ccTouchesEnded(pTouches, pEvent);};
    
    CCRenderTexture* m_rtt;
    ScripRT* cur_sc;
    CCNode * m_node;
    CCSize size_frame,win_size;
    float size_frame_x;
    
    float pause_time;
    CKGUI *m_gui;
    CCDictionary *m_dict;
    CKLayerScript *script;
    void initGUI();
public:
    ~CKTutorialScriptOneScene();
    CKTutorialScriptOneScene();
    static CKTutorialScriptOneScene *create();
    void menuCall(CCNode *_sender,void *_data);
//    virtual void draw();
    void update(float dt);
    void setEnable(bool _en);
    void setPositionFrame(const CCPoint &_pos);
    void initFrame(int _type,const CCSize &_sizeframe,const CCPoint &_pos);
};
#endif /* defined(__CandyKadze__CKTutorialScriptOneScene__) */
