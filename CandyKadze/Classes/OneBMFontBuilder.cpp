//
//  OneBMFontBuilder.cpp
//  fgf
//
//  Created by PR1.Stigol on 16.04.13.
//
//

#include "OneBMFontBuilder.h"
#include <fstream>
#include <stdio.h>
#include "cocos2d.h"
//#include "ccUTF8.h"

//#include "cocos2dx/label_nodes/CCLabelBMFont.cpp"

using namespace cocos2d;
using namespace SL;

#pragma mark - NODE_GRAPH

int cc_wcslen(const unsigned short* str)
{
    int i=0;
    while(*str++) i++;
    return i;
}

/* Code from GLIB gutf8.c starts here. */

#define UTF8_COMPUTE(Char, Mask, Len)        \
if (Char < 128)                \
{                        \
Len = 1;                    \
Mask = 0x7f;                \
}                        \
else if ((Char & 0xe0) == 0xc0)        \
{                        \
Len = 2;                    \
Mask = 0x1f;                \
}                        \
else if ((Char & 0xf0) == 0xe0)        \
{                        \
Len = 3;                    \
Mask = 0x0f;                \
}                        \
else if ((Char & 0xf8) == 0xf0)        \
{                        \
Len = 4;                    \
Mask = 0x07;                \
}                        \
else if ((Char & 0xfc) == 0xf8)        \
{                        \
Len = 5;                    \
Mask = 0x03;                \
}                        \
else if ((Char & 0xfe) == 0xfc)        \
{                        \
Len = 6;                    \
Mask = 0x01;                \
}                        \
else                        \
Len = -1;

#define UTF8_LENGTH(Char)            \
((Char) < 0x80 ? 1 :                \
((Char) < 0x800 ? 2 :            \
((Char) < 0x10000 ? 3 :            \
((Char) < 0x200000 ? 4 :            \
((Char) < 0x4000000 ? 5 : 6)))))


#define UTF8_GET(Result, Chars, Count, Mask, Len)    \
(Result) = (Chars)[0] & (Mask);            \
for ((Count) = 1; (Count) < (Len); ++(Count))        \
{                            \
if (((Chars)[(Count)] & 0xc0) != 0x80)        \
{                        \
(Result) = -1;                \
break;                    \
}                        \
(Result) <<= 6;                    \
(Result) |= ((Chars)[(Count)] & 0x3f);        \
}

#define UNICODE_VALID(Char)            \
((Char) < 0x110000 &&                \
(((Char) & 0xFFFFF800) != 0xD800) &&        \
((Char) < 0xFDD0 || (Char) > 0xFDEF) &&    \
((Char) & 0xFFFE) != 0xFFFE)


static const char utf8_skip_data[256] = {
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
    1, 1, 1, 1, 1, 1, 1,
    2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
    2, 2, 2, 2, 2, 2, 2,
    3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 5,
    5, 5, 5, 6, 6, 1, 1
};

static const char *const g_utf8_skip = utf8_skip_data;

#define cc_utf8_next_char(p) (char *)((p) + g_utf8_skip[*(unsigned char *)(p)])

/*
 * @str:    the string to search through.
 * @c:        the character to not look for.
 *
 * Return value: the index of the last character that is not c.
 * */
unsigned int cc_utf8_find_last_not_char(std::vector<unsigned short> str, unsigned short c)
{
    int len = str.size();
    
    int i = len - 1;
    for (; i >= 0; --i)
        if (str[i] != c) return i;
    
    return i;
}

/*
 * @str:    the string to trim
 * @index:    the index to start trimming from.
 *
 * Trims str st str=[0, index) after the operation.
 *
 * Return value: the trimmed string.
 * */
static void cc_utf8_trim_from(std::vector<unsigned short>* str, int index)
{
    int size = str->size();
    if (index >= size || index < 0)
        return;
    
    str->erase(str->begin() + index, str->begin() + size);
}

/*
 * @ch is the unicode character whitespace?
 *
 * Reference: http://en.wikipedia.org/wiki/Whitespace_character#Unicode
 *
 * Return value: weather the character is a whitespace character.
 * */
bool isspace_unicode(unsigned short ch)
{
    return  (ch >= 0x0009 && ch <= 0x000D) || ch == 0x0020 || ch == 0x0085 || ch == 0x00A0 || ch == 0x1680
    || (ch >= 0x2000 && ch <= 0x200A) || ch == 0x2028 || ch == 0x2029 || ch == 0x202F
    ||  ch == 0x205F || ch == 0x3000;
}

void cc_utf8_trim_ws(std::vector<unsigned short>* str)
{
    int len = str->size();
    
    if ( len <= 0 )
        return;
    
    int last_index = len - 1;
    
    // Only start trimming if the last character is whitespace..
    if (isspace_unicode((*str)[last_index]))
    {
        for (int i = last_index - 1; i >= 0; --i)
        {
            if (isspace_unicode((*str)[i]))
                last_index = i;
            else
                break;
        }
        
        cc_utf8_trim_from(str, last_index);
    }
}

/*
 * cc_utf8_strlen:
 * @p: pointer to the start of a UTF-8 encoded string.
 * @max: the maximum number of bytes to examine. If @max
 *       is less than 0, then the string is assumed to be
 *       null-terminated. If @max is 0, @p will not be examined and
 *       may be %NULL.
 *
 * Returns the length of the string in characters.
 *
 * Return value: the length of the string in characters
 **/
long
cc_utf8_strlen (const char * p, int max)
{
    long len = 0;
    const char *start = p;
    
    if (!(p != NULL || max == 0))
    {
        return 0;
    }
    
    if (max < 0)
    {
        while (*p)
        {
            p = cc_utf8_next_char (p);
            ++len;
        }
    }
    else
    {
        if (max == 0 || !*p)
            return 0;
        
        p = cc_utf8_next_char (p);
        
        while (p - start < max && *p)
        {
            ++len;
            p = cc_utf8_next_char (p);
        }
        
        /* only do the last len increment if we got a complete
         * char (don't count partial chars)
         */
        if (p - start == max)
            ++len;
    }
    
    return len;
}

/*
 * g_utf8_get_char:
 * @p: a pointer to Unicode character encoded as UTF-8
 *
 * Converts a sequence of bytes encoded as UTF-8 to a Unicode character.
 * If @p does not point to a valid UTF-8 encoded character, results are
 * undefined. If you are not sure that the bytes are complete
 * valid Unicode characters, you should use g_utf8_get_char_validated()
 * instead.
 *
 * Return value: the resulting character
 **/
static unsigned int
cc_utf8_get_char (const char * p)
{
    int i, mask = 0, len;
    unsigned int result;
    unsigned char c = (unsigned char) *p;
    
    UTF8_COMPUTE (c, mask, len);
    if (len == -1)
        return (unsigned int) - 1;
    UTF8_GET (result, p, i, mask, len);
    
    return result;
}


unsigned short* cc_utf8_to_utf16(const char* str_old)
{
    int len = cc_utf8_strlen(str_old, -1);
    
    unsigned short* str_new = new unsigned short[len + 1];
    str_new[len] = 0;
    
    for (int i = 0; i < len; ++i)
    {
        str_new[i] = cc_utf8_get_char(str_old);
        str_old = cc_utf8_next_char(str_old);
    }
    
    return str_new;
}

std::vector<unsigned short> cc_utf16_vec_from_utf16_str(const unsigned short* str)
{
    int len = cc_wcslen(str);
    std::vector<unsigned short> str_new;
    
    for (int i = 0; i < len; ++i)
    {
        str_new.push_back(str[i]);
    }
    return str_new;
}

/**
 * cc_unichar_to_utf8:
 * @c: a ISO10646 character code
 * @outbuf: output buffer, must have at least 6 bytes of space.
 *       If %NULL, the length will be computed and returned
 *       and nothing will be written to @outbuf.
 *
 * Converts a single character to UTF-8.
 *
 * Return value: number of bytes written
 **/
int
cc_unichar_to_utf8 (unsigned short c,
                    char   *outbuf)
{
    unsigned int len = 0;
    int first;
    int i;
    
    if (c < 0x80)
    {
        first = 0;
        len = 1;
    }
    else if (c < 0x800)
    {
        first = 0xc0;
        len = 2;
    }
    else if (c < 0x10000)
    {
        first = 0xe0;
        len = 3;
    }
    else if (c < 0x200000)
    {
        first = 0xf0;
        len = 4;
    }
    else if (c < 0x4000000)
    {
        first = 0xf8;
        len = 5;
    }
    else
    {
        first = 0xfc;
        len = 6;
    }
    
    if (outbuf)
    {
        for (i = len - 1; i > 0; --i)
        {
            outbuf[i] = (c & 0x3f) | 0x80;
            c >>= 6;
        }
        outbuf[0] = c | first;
    }
    
    return len;
}

#define SURROGATE_VALUE(h,l) (((h) - 0xd800) * 0x400 + (l) - 0xdc00 + 0x10000)

/**
 * cc_utf16_to_utf8:
 * @str: a UTF-16 encoded string
 * @len: the maximum length of @str to use. If @len < 0, then
 *       the string is terminated with a 0 character.
 * @items_read: location to store number of words read, or %NULL.
 *              If %NULL, then %G_CONVERT_ERROR_PARTIAL_INPUT will be
 *              returned in case @str contains a trailing partial
 *              character. If an error occurs then the index of the
 *              invalid input is stored here.
 * @items_written: location to store number of bytes written, or %NULL.
 *                 The value stored here does not include the trailing
 *                 0 byte.
 * @error: location to store the error occuring, or %NULL to ignore
 *         errors. Any of the errors in #GConvertError other than
 *         %G_CONVERT_ERROR_NO_CONVERSION may occur.
 *
 * Convert a string from UTF-16 to UTF-8. The result will be
 * terminated with a 0 byte.
 *
 * Return value: a pointer to a newly allocated UTF-8 string.
 *               This value must be freed with free(). If an
 *               error occurs, %NULL will be returned and
 *               @error set.
 **/
char *
cc_utf16_to_utf8 (const unsigned short  *str,
                  long             len,
                  long            *items_read,
                  long            *items_written)
{
    /* This function and g_utf16_to_ucs4 are almost exactly identical - The lines that differ
     * are marked.
     */
    const unsigned short *in;
    char *out;
    char *result = NULL;
    int n_bytes;
    unsigned short high_surrogate;
    
    if (str == 0) return NULL;
    
    n_bytes = 0;
    in = str;
    high_surrogate = 0;
    while ((len < 0 || in - str < len) && *in)
    {
        unsigned short c = *in;
        unsigned short wc;
        
        if (c >= 0xdc00 && c < 0xe000) /* low surrogate */
        {
            if (high_surrogate)
            {
                wc = SURROGATE_VALUE (high_surrogate, c);
                high_surrogate = 0;
            }
            else
            {
                CCLOGERROR("Invalid sequence in conversion input");
                goto err_out;
            }
        }
        else
        {
            if (high_surrogate)
            {
                CCLOGERROR("Invalid sequence in conversion input");
                goto err_out;
            }
            
            if (c >= 0xd800 && c < 0xdc00) /* high surrogate */
            {
                high_surrogate = c;
                goto next1;
            }
            else
                wc = c;
        }
        
        /********** DIFFERENT for UTF8/UCS4 **********/
        n_bytes += UTF8_LENGTH (wc);
        
    next1:
        in++;
    }
    
    if (high_surrogate && !items_read)
    {
        CCLOGERROR("Partial character sequence at end of input");
        goto err_out;
    }
    
    /* At this point, everything is valid, and we just need to convert
     */
    /********** DIFFERENT for UTF8/UCS4 **********/
    result = new char[n_bytes + 1];
    
    high_surrogate = 0;
    out = result;
    in = str;
    while (out < result + n_bytes)
    {
        unsigned short c = *in;
        unsigned short wc;
        
        if (c >= 0xdc00 && c < 0xe000) /* low surrogate */
        {
            wc = SURROGATE_VALUE (high_surrogate, c);
            high_surrogate = 0;
        }
        else if (c >= 0xd800 && c < 0xdc00) /* high surrogate */
        {
            high_surrogate = c;
            goto next2;
        }
        else
            wc = c;
        
        /********** DIFFERENT for UTF8/UCS4 **********/
        out += cc_unichar_to_utf8 (wc, out);
        
    next2:
        in++;
    }
    
    /********** DIFFERENT for UTF8/UCS4 **********/
    *out = '\0';
    
    if (items_written)
    /********** DIFFERENT for UTF8/UCS4 **********/
        *items_written = out - result;
    
err_out:
    if (items_read)
        *items_read = in - str;
    
    return result;
}

#pragma mark - NODE_GRAPH


int NodeGraph::count = 0;
NodeGraph::~NodeGraph()
{
    CC_SAFE_DELETE(child[0]);
    CC_SAFE_DELETE(child[1]);
};

NodeGraph::NodeGraph(const cocos2d::CCRect &_rc)
{
    //   CCLOG("Node:: pos %f %f size %f %f",_rc.origin.x,_rc.origin.y ,_rc.size.width,_rc.size.height);
    rc = _rc;
    child[1] = NULL;
    child[0] = NULL;
    is_full = false;
    count++;
};

bool NodeGraph::Insert(TextureSymbol *img)
{
    if(is_full)
    {
        //        CCLOG("imageID");
        return false;
    };
    
    if(child[0] == NULL)
    {
        int dw = rc.size.width - img->getSize().width;
        int dh = rc.size.height - img->getSize().height;
        if(dw < 0|| dh < 0)
        {
            // CCLOG("not size");
            return false;
        }
        
        if (dw == 0 && dh == 0) {
            //   CCLOG("add size %f %f",rc.size.width,rc.size.height);
            img->setPosition(rc.origin.x + img->getSize().width/2, rc.origin.y + img->getSize().height/2);
            is_full = true;
            return true;
        };
        
        if(dw > dh)
        {
            child[0] = new NodeGraph(CCRect(rc.origin.x, rc.origin.y, img->getSize().width,rc.size.height));
            child[1] = new NodeGraph(CCRect(rc.origin.x + img->getSize().width, rc.origin.y, dw,rc.size.height));
        }
        else
        {
            child[0] = new NodeGraph(CCRect(rc.origin.x, rc.origin.y, rc.size.width,img->getSize().height));
            child[1] = new NodeGraph(CCRect(rc.origin.x, rc.origin.y + img->getSize().height,rc.size.width,dh));
        };
    }
    
    bool tnode = child[0]->Insert(img);
    if(!tnode)
    {
        tnode = child[1]->Insert(img);
        if(!tnode)
            return false;
        else
            return tnode;
    }
    else
        return tnode;
    
    return NULL;
};

//

TextureSymbol::~TextureSymbol()
{
  //  CC_SAFE_RELEASE_NULL(m_rtt);
    CC_SAFE_RELEASE_NULL(sprite);
  //  sprite->autorelease();
};
TextureSymbol::TextureSymbol()
{
    sprite = NULL;
}
bool TextureSymbol::operator< (const TextureSymbol &_tex) const
{
    return (getSize().height*getSize().width< _tex.getSize().height*_tex.getSize().width);
};

bool TextureSymbol::build(const unsigned short _sh, const char *fontName, float fontSize,render_func &_func)
{
    char *str = cc_utf16_to_utf8(&_sh,1,0,0);
    m_id = _sh;
    
    //CCLOG("%s",str);
    CCLabelTTF* _label = CCLabelTTF::create(str, fontName, fontSize);
    
	CCSize size(MAX(6,_label->getContentSize().width),_label->getContentSize().height);
    if(size.width*size.height == 0)
    {
       // m_rtt = NULL;
        _label->release();
        return false;
    }
    CCRenderTexture *m_rtt  = CCRenderTexture::create(size.width,size.height,kCCTexture2DPixelFormat_RGBA8888,GL_DEPTH24_STENCIL8_OES);
    
    CCPoint bottomLeft = ccp(size.width * _label->getAnchorPoint().x, size.height * _label->getAnchorPoint().y);
    _label->setPosition(bottomLeft);
    _label->setVisible(true);
    
    if(_func)
    {
        _func(_label,m_rtt,fontSize);
    }
    else
    {
        m_rtt->begin();
        _label->visit();
        m_rtt->end();
    };
    
    sprite = m_rtt->getSprite();
    sprite->retain();
    sprite->removeFromParentAndCleanup(false);
    //m_rtt->release();
    CC_SAFE_RELEASE_NULL(m_rtt);
    CC_SAFE_RELEASE_NULL(_label);
    //_label->release();
    return true;
};

#pragma mark - ONE_BMFONT_BUILDER
OneBMFontBuilder::OneBMFontBuilder()
{
    m_render_func = NULL;
    font_name = "";
	square = 0.0f;
    atlas_created = false;
    atlas = NULL;
	scale = CCDirector::sharedDirector()->getContentScaleFactor();
};

OneBMFontBuilder::~OneBMFontBuilder()
{
    CC_SAFE_RELEASE_NULL(atlas);
    m_it = map_font_size.begin();
    while (m_it != map_font_size.end()) {
        CC_SAFE_DELETE(m_it->second);
        m_it++;
    };
    map_font_size.clear();
    
    it = list_all_char.begin();
    while (it != list_all_char.end()) {
        CC_SAFE_DELETE(*it);
        it++;
    };
    list_all_char.clear();
};

OneBMFontBuilder* OneBMFontBuilder::create(const char *_fontName,render_func _func)
{
    OneBMFontBuilder *p_Ref = new OneBMFontBuilder();
    if (p_Ref)
    {
        p_Ref->m_render_func = *_func;
        p_Ref->font_name = _fontName;
        return p_Ref;
    };
    CC_SAFE_DELETE(p_Ref);
    return NULL;
};

void OneBMFontBuilder::saveFont(int _font_size)
{
    if(map_font_size[_font_size]->list.empty())
        return;
    CCSize s = atlas->getSprite()->getContentSize();
	std::string path = cocos2d::CCFileUtils::sharedFileUtils()->getWriteablePath();
    path.append(font_name);
	
    char size_fnt[4];
    sprintf(size_fnt,"%d",_font_size);
    path.append(size_fnt);
    path.append(".fnt");
    
    std::ofstream out(path.c_str(), std::ios::out | std::ios::binary);
    if (out.is_open())
    {
        it = map_font_size[_font_size]->list.begin();
        
        out << "info face=\"ArialMT\" size="<<_font_size<<" bold=0 italic=0 charset=\"\" unicode=0 stretchH=0 smooth=0 aa=1 padding=0,0,0,0 spacing=1,1 \n";
		out<<"common lineHeight="<<int((*it)->getSize().height)*scale<<" base=26 scaleW="<<int(s.width)*scale<<" scaleH="<<int(s.height)*scale<<" pages=1 packed=0 \npage id=0 file=\""<< atlas_name.c_str() <<"\" \n";
        out << "chars count=" << map_font_size[_font_size]->list.size()<< "\n";
        
        while (it != map_font_size[_font_size]->list.end()) {
            const CCSize &size = (*it)->getSize();
            out << "char id="<< (*it)->getID()<<"   x="<< (*it)->getX()*scale<<"     y="<< int(s.height - (*it)->getY())*scale<<"     width="<<int(size.width)*scale - 2<<"     height="<<int(size.height)*scale<<"     xoffset=0     yoffset=0    xadvance="<<int(size.width)*scale -3 <<"     page=0  chnl=0\n";
            it++;
        };
        out.close();
    };
};

void OneBMFontBuilder::save(const char *_name)
{
    // CCLOG("OneBMFontBuilder::save %s",_name);
    build();
    //CCSize size(2048,64);
    //  CCLOG("%f %f",square,_size.width*_size.height);
    if(strlen(_name) > 3)
    {
        font_name = _name;
    };
    if(!atlas_created)
        createAtlas(CCSize(0,0),true);
    saveAllFonts();
};

void OneBMFontBuilder::build()
{
    m_it = map_font_size.begin();
    while (m_it != map_font_size.end()) {
        std::set<unsigned short>::iterator t_it = m_it->second->char_set.begin();
        while (t_it != m_it->second->char_set.end()) {
            TextureSymbol* m_rt = new TextureSymbol;
            // CCLOG("%u %u",t_it->first,t_it->second);
            if(m_rt->build(*t_it, font_name.c_str(), m_it->first,m_render_func))
            {
                square += m_rt->getSize().height*m_rt->getSize().width;
                map_font_size[m_it->first]->list.push_back(m_rt);
                list_all_char.push_back(m_rt);
            }
            else
            {
                CC_SAFE_DELETE(m_rt);
            }
            t_it++;
        };
        m_it++;
    };
};

void OneBMFontBuilder::addString(const char *_string,  int _fontSize)
{
    if(!map_font_size[_fontSize])
    {
        map_font_size[_fontSize] = new CHAR_DICT;
    };
    //Removes duplicate chars in string
    //and insert to char set
    unsigned short* utf16String = cc_utf8_to_utf16(_string);
    std::vector<unsigned short> str_whole = cc_utf16_vec_from_utf16_str(utf16String);
    std::vector<unsigned short>::iterator t_it = str_whole.begin();
    while (t_it != str_whole.end()) {
        map_font_size[_fontSize]->char_set.insert(*t_it);
        t_it++;
    };
    //  removeDublicate(_fontSize);
};

//_______________[Removes duplicate chars in string] ______________
//algorithm using temporary string
// and the method of exhaustive search all characters
//_________________________________________________________________
//void OneBMFontBuilder::removeDublicate(int _index)
//{
//    std::string str = "";
//    for (int i = 0; i < map_font_size[_index]->dict.size(); ++i) {
//        bool p = false;
//        for (int j = 0; j < str.size(); ++j) {
//            if(str.at(j) == map_font_size[_index]->dict.at(i))
//            {
//                p = true;
//                break;
//            };
//        };
//        if(!p)
//        {
//            str.append(map_font_size[_index]->dict.substr(i, 1).c_str());
//        };
//    };
//    CCLOG("removeDublicate %s",str.c_str());
//    map_font_size[_index]->dict = str;
//};

cocos2d::CCSize OneBMFontBuilder::calcAtlasSize()
{
    int h = 32;
    int w = 32;
    bool p = false;
    float step = 1.2;
    while (h*w < square*1.08) { //1.18 -if step = 2
        if (p) {
            h *= step;
        }
        else
        {
            w *= step;
        }
        p = !p;
    };

     return CCSize(w,h);
};

void OneBMFontBuilder::createAtlas(const CCSize &_size,bool _auto_size)
{
    CCSize size = (_auto_size)?calcAtlasSize():_size;
    
    r_it = list_all_char.rbegin();
    NodeGraph *m_node = new NodeGraph(CCRect(0,0,size.width,size.height));
    atlas = CCRenderTexture::create(size.width,size.height,kCCTexture2DPixelFormat_RGBA8888,GL_DEPTH24_STENCIL8_OES);
    
    atlas->begin();
    while (r_it != list_all_char.rend()) {
		
        bool p_node = m_node->Insert((*r_it));
        CCAssert(p_node, "small atlas, need a larger atlas");
        (*r_it)->visit();
        r_it++;
    };
    CCLOG("count create NodeGraph %d",NodeGraph::count);
    CC_SAFE_DELETE(m_node);
    
    atlas->end();
    atlas_name = font_name;
    atlas_name.append(".png");
   // atlas->getSprite()->getTexture()->setAntiAliasTexParameters();
    atlas->saveToFile(atlas_name.c_str(),kCCImageFormatPNG);
	atlas_created = true;
};

void OneBMFontBuilder::saveAllFonts()
{
    m_it = map_font_size.begin();
    while (m_it != map_font_size.end()) {
        saveFont(m_it->first);
        m_it++;
    }
};

void OneBMFontBuilder::func2(cocos2d::CCLabelTTF *_label,cocos2d::CCRenderTexture *&_rtt,const int &_font_size)
{
	GLchar *frag = "																		\n\
    #ifdef GL_ES \n\
    precision mediump float; \n\
    #endif					\n\
    \n\
    varying vec2 v_texCoord; \n\
    uniform sampler2D u_texture;\n\
    \n\
    void main()\n\
    {\n\
    // 2\n\
    vec2 onePixel = vec2(1.0 / 200.0, 1.0 / 200.0);\n\
    \n\
    // 3\n\
    vec2 texCoord = v_texCoord;\n\
    \n\
    // 4\n\
    vec4 color;\n\
    color.rgb = vec3(0.5);\n\
    color -= texture2D(u_texture, texCoord - onePixel) * 1.0;\n\
    color += texture2D(u_texture, texCoord + onePixel) * 1.0;\n\
    // 5\n\
    color.rgb = vec3((color.r + color.g + color.b) / 1.0);\n\
    gl_FragColor = vec4(color.rgb, color.a);\n\
    }";
    
	GLchar *vert =	"														\n\
    attribute vec4 a_position;								\n\
    attribute vec2 a_texCoord;								\n\
    \n\
    #ifdef GL_ES											\n\
    varying mediump vec2 v_texCoord;						\n\
    #else													\n\
    varying vec2 v_texCoord;								\n\
    #endif													\n\
    \n\
    void main()												\n\
    {														\n\
    gl_Position = CC_MVPMatrix * a_position;			\n\
	v_texCoord = a_texCoord;							\n\
    }														\n\
    ";
	CCGLProgram *pBWShaderProgram = new CCGLProgram();
    pBWShaderProgram->autorelease();
	pBWShaderProgram->initWithVertexShaderByteArray(vert,frag);
    pBWShaderProgram->addAttribute(kCCAttributeNamePosition, kCCVertexAttrib_Position);
    pBWShaderProgram->addAttribute(kCCAttributeNameTexCoord, kCCVertexAttrib_TexCoords);
    pBWShaderProgram->link();
    pBWShaderProgram->updateUniforms();
    CCShaderCache::sharedShaderCache()->addProgram(pBWShaderProgram, "kBWShaderProgram");
    
    _label->setShaderProgram(CCShaderCache::sharedShaderCache()->programForKey("kBWShaderProgram"));
    _label->getShaderProgram()->use();
	ccColor3B c = {255,124,0};
	_label->setColor(c);
	_rtt->begin();
	_label->visit();
	_rtt->end();
};

void OneBMFontBuilder::funcGradBorder(CCLabelTTF* label,cocos2d::CCRenderTexture *&rtt,const int &_font_size)
{
    rtt->release();
    rtt = NULL;
    int _size = 1;
    int border = (CCDirector::sharedDirector()->getWinSize().width == 1024)?2:1;
    CCSize size(label->getTexture()->getContentSize().width + _size*2 + border,label->getTexture()->getContentSize().height + _size*2);
	rtt = CCRenderTexture::create(size.width,size.height);
    
    CCRenderTexture* rt2 = CCRenderTexture::create(size.width,size.height);
    
	//CCPoint originalPos = label->getPosition();
    
    label->setColor((ccColor3B){255,255,255});
    label->setVisible(true);
    
	CCPoint bottomLeft = ccp(size.width * label->getAnchorPoint().x,size.height* label->getAnchorPoint().y);
    label->setPosition(bottomLeft);
    // CCPoint bottomLeft = label->getPosition();
    
    CCSprite *m_sprite = CCSprite::create("GradientFont.png");
    
    m_sprite->setScaleY(label->getContentSize().height/m_sprite->getContentSize().height);
    m_sprite->setScaleX(label->getContentSize().width/m_sprite->getContentSize().width);
    m_sprite->setPosition(bottomLeft);
    
    
    rt2->begin();
    m_sprite->visit();
    label->setBlendFunc((ccBlendFunc) { GL_DST_COLOR, GL_NONE});
    label->visit();
	rt2->end();
    rt2->setPosition(bottomLeft);
    
    label->setBlendFunc((ccBlendFunc) {  GL_SRC_ALPHA, GL_ONE});
    label->setColor((ccColor3B){59,15,5});
    rtt->begin();
    for (int i=0; i < 360; i+=60) // you should optimize that for your needs
	{
        label->setPosition(ccp(bottomLeft.x + sin(CC_DEGREES_TO_RADIANS(i))*_size, bottomLeft.y + cos(CC_DEGREES_TO_RADIANS(i))*_size));
		label->visit();
	};
    rt2->visit();
    rtt->end();
    m_sprite->release();
    rt2->release();
};