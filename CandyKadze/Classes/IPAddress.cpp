
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/sockio.h>
#include <net/if.h>
#include <errno.h>
#include <net/if_dl.h>
 #include <net/ethernet.h>

#include "IPAddress.h"

#define	min(a,b)	((a) < (b) ? (a) : (b))
#define	max(a,b)	((a) > (b) ? (a) : (b))

#define BUFFERSIZE	4000

static char macAddress[255];

void IPAddresses::InitAddresses()
{
	int i;
	for (i=0; i<MAXADDRS; ++i)
	{
		if_names[i] = ip_names[i] = hw_addrs[i] = NULL;
		ip_addrs[i] = 0;
	}
}

void IPAddresses::FreeAddresses()
{
	int i;
	for (i = 0; i < MAXADDRS; ++i)
	{
		if (if_names[i] != 0) free(if_names[i]);
		if (ip_names[i] != 0) free(ip_names[i]);
		if (hw_addrs[i] != 0) free(hw_addrs[i]);
		ip_addrs[i] = 0;
	}
	InitAddresses();
}

void IPAddresses::GetHWAddresses()
{
    struct ifconf ifc;
    struct ifreq *ifr;
    int i, sockfd;
    char buffer[BUFFERSIZE], *cp, *cplim;
    
    for (i=0; i < MAXADDRS; ++i)
    {
        hw_addrs[i] = NULL;
    }
    
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd < 0)
    {
        perror("socket failed");
        return;
    }
    
    ifc.ifc_len = BUFFERSIZE;
    ifc.ifc_buf = buffer;
    
    if (ioctl(sockfd, SIOCGIFCONF, (char *)&ifc) < 0)
    {
        perror("ioctl error");
        close(sockfd);
        return;
    }
    
    ifr = ifc.ifc_req;
    
    cplim = buffer + ifc.ifc_len;
    
    for (cp = buffer; cp < cplim; )
    {
        ifr = (struct ifreq *)cp;
       // printf("ifr_name %s\n",ifr->ifr_name);
        if (ifr->ifr_addr.sa_family == AF_LINK && strcmp(ifr->ifr_name, "en0") == 0 )
        {

            struct sockaddr_dl * dlAddr = (struct sockaddr_dl *)&ifr->ifr_addr;
            const unsigned char* base = (const unsigned char*) &dlAddr->sdl_data[dlAddr->sdl_nlen];
            strcpy(macAddress, "");
            for (i = 0; i < dlAddr->sdl_alen; i++) {
                if (i != 0) {
                    strcat(macAddress, "");
                }
                char partialAddr[3];
                sprintf(partialAddr, "%02X", base[i]);
                strcat(macAddress, partialAddr);
            }
            if(strlen(macAddress) > 4)
            {
                printf("macAddress %s\n",macAddress);
                close(sockfd);
                return;
            }
        }
       // memset(macAddress, 0, 255);
        cp += sizeof(ifr->ifr_name) + max(sizeof(ifr->ifr_addr), ifr->ifr_addr.sa_len);
    }
    
    close(sockfd);
};
std::string IPAddresses::getWH()
{
    //InitAddresses();
    // GetIPAddresses();
    if(strlen(macAddress) > 4) //
    {
        return macAddress;
    }
    else
    {
        IPAddresses *m_id = new IPAddresses;
        m_id->GetHWAddresses();
        delete m_id;
        return macAddress;
    };
}
