//
//  CKAudioMgr.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 08.10.12.
//
//

#include "CKAudioMgr.h"

static CKAudioMng *g_CKAudioMng = NULL;

void* func_fade(void *_data)
{
    CKFadeValue *bg_fade = static_cast<CKFadeValue*>(_data);
    bool end_fade = false;
    while (!end_fade) {
        if(bg_fade->step > 0.0)
        {
            end_fade = (CocosDenshion::SimpleAudioEngine::sharedEngine()->getBackgroundMusicVolume() >= bg_fade->end);
        }
        else
        {
            end_fade = (CocosDenshion::SimpleAudioEngine::sharedEngine()->getBackgroundMusicVolume() <= bg_fade->end);
        }
        if(end_fade)
        {
            CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(bg_fade->end);
            break;
        }
        else
        {
            CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(CocosDenshion::SimpleAudioEngine::sharedEngine()->getBackgroundMusicVolume() + bg_fade->step);
        }
        usleep(20000);
    };
    pthread_exit(0);
    //    return NULL;
};

CKAudioMng& CKAudioMng::Instance()
{
    if (!g_CKAudioMng)
    {
        g_CKAudioMng = new CKAudioMng();
    }
    return *g_CKAudioMng;
};

void CKAudioMng::loadWorldSound(const std::string &_world)
{
    CCDictionary *dict = CCDictionary::createWithContentsOfFile("sound.plist");
    CCDictionary* worldDic = (CCDictionary*)dict->objectForKey("world");
    
    is_background_pause = false;
    if(!worldDic)
        return;
    CCLOG("CKAudioMng::loadWorldSound %s",_world.c_str());
    worldDic = (CCDictionary*)worldDic->objectForKey(_world);
    CCDictElement* pElement = NULL;
    world_sound.clear();
    
    CCDICT_FOREACH(worldDic, pElement)
    {
        //  CCLOG("pElement->getStrKey() %s",pElement->getStrKey());
        CCDictionary* tdict = (CCDictionary*)pElement->getObject();
        if(strcmp(pElement->getStrKey(),"bg") == 0 )
        {
            bg_sound["bg"] = (CKSound){0,dictStr(tdict,"name"),dictFloat(tdict,"fade_in"),dictFloat(tdict,"fade_out"),1.0,0,dictInt(tdict,"loop"),0,0,false,false};
            if(tdict->objectForKey("volume"))
            {
                bg_sound["bg"].volume = dictFloat(tdict,"volume");
            };
        }
        else
        {
            world_sound[pElement->getStrKey()] = (CKSound){0,dictStr(tdict,"name"),dictFloat(tdict,"fade_in"),dictFloat(tdict,"fade_out"),1.0,0,dictInt(tdict,"loop"),0,0,0,false,false};
            world_sound[pElement->getStrKey()].volume = 1.0;
            if(tdict->objectForKey("volume"))
            {
                world_sound[pElement->getStrKey()].volume = dictFloat(tdict,"volume");
            };
            if( world_sound[pElement->getStrKey()].fade_in > 0.0)
            {
                world_sound[pElement->getStrKey()].fade = true;
            };
        }
    };
};

void CKAudioMng::removeAllEffect()
{
    it = world_sound.begin();
    while (it != world_sound.end()) {
        CocosDenshion::SimpleAudioEngine::sharedEngine()->unloadEffect(it->second.file_name.c_str());
        it++;
    };
    world_sound.clear();
};

void CKAudioMng::removeBackground()
{
    CocosDenshion::SimpleAudioEngine::sharedEngine()->releaseBackgroundMusic();
};


void CKAudioMng::fadeBgToVolume(const float  &_end,const float &_time)
{
    //return;
        bg_fade.step = (_end - CocosDenshion::SimpleAudioEngine::sharedEngine()->getBackgroundMusicVolume())/(60.0f*_time);
        bg_fade.end = _end;
        CCLOG("CKAudioMng::fadeBgToVolume bg_fade.step %f end %f",bg_fade.step,_end);
    
    
   // load_next_sound = false;
  //  CCDirector::sharedDirector()->getScheduler()->unscheduleSelector(schedule_selector(CKAudioMng::fadeBgSchedule), this);
  //  CCDirector::sharedDirector()->getScheduler()->scheduleSelector(schedule_selector(CKAudioMng::fadeBgSchedule), this, 1.0/60.0, false);
    
     pthread_cancel(m_bg_thread);
     pthread_create(&m_bg_thread, NULL, func_fade, &bg_fade);
}

void CKAudioMng::fadeBgSchedule(const float &_dt)
{
   // CCLOG("fadeBgSchedule %f",CocosDenshion::SimpleAudioEngine::sharedEngine()->getBackgroundMusicVolume());
    bool end_fade = false;
    if(bg_fade.step > 0.0)
    {
        end_fade = (CocosDenshion::SimpleAudioEngine::sharedEngine()->getBackgroundMusicVolume() >= bg_fade.end);
    }
    else
    {
        end_fade = (CocosDenshion::SimpleAudioEngine::sharedEngine()->getBackgroundMusicVolume() <= bg_fade.end);
    }
    if(end_fade)
    {
        CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(bg_fade.end);
        CCDirector::sharedDirector()->getScheduler()->unscheduleSelector(schedule_selector(CKAudioMng::fadeBgSchedule),this);
    }
    else
    {
        CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(CocosDenshion::SimpleAudioEngine::sharedEngine()->getBackgroundMusicVolume() + bg_fade.step);
    }
};

void CKAudioMng::fadeInEffect(const std::string &_name)
{
    if(world_sound[_name].sound_id != 0)
    {
        CCDirector::sharedDirector()->getScheduler()->unscheduleSelector(schedule_selector(CKAudioMng::fadeInEffectSchedule), this);
        CCDirector::sharedDirector()->getScheduler()->unscheduleSelector(schedule_selector(CKAudioMng::fadeOutEffectSchedule), this);
        CCDirector::sharedDirector()->getScheduler()->scheduleSelector(schedule_selector(CKAudioMng::fadeInEffectSchedule), this, 0, false);
        effect_fade_volume = 0.00;
        fade_effect_id = world_sound[_name].sound_id;
        //CocosDenshion::SimpleAudioEngine::sharedEngine()->setEffectsVolume(effect_fade_volume);
        alSourcef(fade_effect_id, AL_GAIN, effect_fade_volume);
    };
};

void CKAudioMng::fadeInEffectSchedule(const float &_dt)
{
    if(effect_fade_volume >= 1.0)
    {
        alSourcef(fade_effect_id, AL_GAIN, 1.0);
        CCDirector::sharedDirector()->getScheduler()->unscheduleSelector(schedule_selector(CKAudioMng::fadeInEffectSchedule),this);
    }
    else
    {
        effect_fade_volume += 0.04;
        alSourcef(fade_effect_id, AL_GAIN, effect_fade_volume);
    };
    
};
void CKAudioMng::fadeOutEffect(const std::string &_name)
{
   // return;
    
    if(world_sound[_name].sound_id != 0)
    {
        fade_effect_id  = world_sound[_name].sound_id;
        CCDirector::sharedDirector()->getScheduler()->unscheduleSelector(schedule_selector(CKAudioMng::fadeInEffectSchedule), this);
        CCDirector::sharedDirector()->getScheduler()->unscheduleSelector(schedule_selector(CKAudioMng::fadeOutEffectSchedule), this);
        CCDirector::sharedDirector()->getScheduler()->scheduleSelector(schedule_selector(CKAudioMng::fadeOutEffectSchedule), this, 0, false);
        effect_fade_volume = 1.00;
        
        alSourcef(fade_effect_id, AL_GAIN, 1.0);
        
        // CocosDenshion::SimpleAudioEngine::sharedEngine()->setEffectsVolume(effect_fade_volume);
    }
}
void CKAudioMng::fadeOutEffectSchedule(const float &_dt)
{
    if(effect_fade_volume <= 0.0)
    {
        //CocosDenshion::SimpleAudioEngine::sharedEngine()->setEffectsVolume(0.0);
        alSourcef(fade_effect_id, AL_GAIN, 0.0);
        CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(fade_effect_id);
        fade_effect_id = 0;
        CCDirector::sharedDirector()->getScheduler()->unscheduleSelector(schedule_selector(CKAudioMng::fadeOutEffectSchedule),this);
    }
    else
    {
        effect_fade_volume -= 0.04;
        alSourcef(fade_effect_id, AL_GAIN, effect_fade_volume);
    };
};

//void CKAudioMng::update(const float &_dt)
//{
//    if(!play_effect && !play_music)
//        return;
//
//    if(!update_sound.empty())
//    {
//        update_it = update_sound.begin();
//        while (update_it != update_sound.end())
//        {
//            (*update_it)->curent_volume += (*update_it)->step;
//
//            if((*update_it)->curent_volume >= (*update_it)->volume)
//            {
//                alSourcef((*update_it)->sound_id, AL_GAIN, (*update_it)->volume);
//                update_sound.erase(update_it);
//                update_it++;
//                if(update_sound.empty() || update_it != update_sound.end())
//                    break;
//                continue;
//            };
//            update_it++;
//            alSourcef((*update_it)->sound_id, AL_GAIN, (*update_it)->curent_volume);
//        }
//    }
//
//    if(!fadeout_sound.empty())
//    {
//        update_it = fadeout_sound.begin();
//        while (update_it != fadeout_sound.end()) {
//            if((*update_it)->start_fadeout)
//            {
//                (*update_it)->curent_volume -= (*update_it)->step;
//                alSourcef((*update_it)->sound_id, AL_GAIN, (*update_it)->volume);
//                if((*update_it)->curent_volume <= 0.0)
//                {
//                    fadeout_sound.erase(update_it);
//
//                    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect((*update_it)->sound_id);
//                    if(fadeout_sound.empty() || update_it != fadeout_sound.end())
//                        break;
//                    continue;
//                };
//            }
//            else
//            {
//                float position;
//                alGetSourcef((*update_it)->sound_id, AL_SEC_OFFSET, &position);
//                if(position >= (*update_it)->curent_time)
//                {
//                    (*update_it)->start_fadeout = true;
//                };
//            }
//            update_it++;
//        }
//    }
//
//    if(!update_loop.empty())
//    {
//        update_loop_it = update_loop.begin();
//        while (update_loop_it != update_loop.end()) {
//            float position;
//            alGetSourcef((*update_loop_it).sound_id, AL_SEC_OFFSET, &position);
//
//            if((*update_loop_it).last_position > position)
//            {
//                (*update_loop_it).curent_loop++;
//                if((*update_loop_it).curent_loop == (*update_loop_it).count_loop)
//                {
//                    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect((*update_loop_it).sound_id);
//                    if(update_loop.empty() || update_loop_it != update_loop.end())
//                        break;
//                    continue;
//                };
//            };
//            (*update_loop_it).last_position = position;
//            update_loop_it++;
//        }
//    }
//
//};

void CKAudioMng::preloadWorldSound(int _world)
{
    //preload effect
    removeAllEffect();
    is_background_pause = false;
    it = world_sound.begin();
    while (it != world_sound.end()) {
        it->second.id = CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffectId(it->second.file_name.c_str());
        // CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadEffectId(it->second.file_name.c_str());
        CCLOG("CKAudioMng::preloadWorldSound it->second.id %d %s",it->second.id,it->second.file_name.c_str());
        it++;
    };
};


void CKAudioMng::playEffect(const std::string &_sound)
{
    if(!CKFileOptions::Instance().isPlayEffect())
    {
        CCLOG("disable Effect");
        return;
    };
    if(world_sound[_sound].sound_id == 0)
    {
        CCLOG("playEffect::name %s volume %f id %d",world_sound[_sound].file_name.c_str(),world_sound[_sound].volume,world_sound[_sound].sound_id);
        world_sound[_sound].sound_id = CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect(world_sound[_sound].file_name.c_str(),world_sound[_sound].loop);
        
    }
    else
    {
        alSourcePlay(world_sound[_sound].sound_id);
        alSourcef(world_sound[_sound].sound_id, AL_PITCH, 1.0);
        alSourcef(world_sound[_sound].sound_id, AL_GAIN, 1.0);
    }
    
    //  alSourcef(world_sound[_sound].sound_id, AL_GAIN, world_sound[_sound].volume);
};

void CKAudioMng::setEffectSpeed(const std::string &_sound,float _speed)
{
    if(!CKFileOptions::Instance().isPlayEffect())
    {
        //  CCLOG("disable Effect");
        return;
    };
    if(world_sound[_sound].sound_id != 0)
        alSourcef(world_sound[_sound].sound_id, AL_PITCH, _speed);
};

void CKAudioMng::stopEffect(const std::string &_sound)
{
    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopEffect(world_sound[_sound].sound_id);
};

bool CKAudioMng::isPlayEffect(const std::string &_sound)
{
    ALint state;
    alGetSourcei(world_sound[_sound].sound_id, AL_SOURCE_STATE, &state);
    return (state == AL_PLAYING);
};

void CKAudioMng::pauseAllEffect()
{
    CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseAllEffects();
};

void CKAudioMng::pauseBackground()
{
    if(isPlayBackground())
    {
        is_background_pause = true;
        CocosDenshion::SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
    }
}

void CKAudioMng::resumeAllEffect()
{
    CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeAllEffects();
};

void CKAudioMng::resumeBackground()
{
    if(is_background_pause)
        CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
};

bool CKAudioMng::isPlayBackground()
{
    return CocosDenshion::SimpleAudioEngine::sharedEngine()->isBackgroundMusicPlaying();
};


void CKAudioMng::playBackground(const std::string &_sound,const float &_volume)
{
    if(is_background_pause)
    {
        CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
        is_background_pause = false;
        return;
    }
    is_background_pause = false;
    
    if (!CocosDenshion::SimpleAudioEngine::sharedEngine()->isBackgroundMusicPlaying()) {
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(bg_sound[_sound].file_name.c_str(),true);
    }

    CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(_volume);

    CCLOG("bg_sound[_sound].file_name.c_str() %s %s",_sound.c_str(),bg_sound[_sound].file_name.c_str());
    curent_bg = &bg_sound[_sound];
};

void CKAudioMng::playMenuSound()
{
    CKAudioMng::Instance().stopBackground();
    CKAudioMng::Instance().removeBackground();
    CKAudioMng::Instance().loadWorldSound("menu");
    CKAudioMng::Instance().removeAllEffect();
    CKAudioMng::Instance().preloadWorldSound();
    
    if(CKFileOptions::Instance().isPlayMusic())
    {
        //CKAudioMng::Instance().setVolumeBgd(0.0);
        CKAudioMng::Instance().playBackground("bg",0.0);
        CKAudioMng::Instance().fadeBgToVolume(1.0);
    };
};

void CKAudioMng::stopAllEffect()
{
    CocosDenshion::SimpleAudioEngine::sharedEngine()->stopAllEffects();
};
void CKAudioMng::preloadBgd(const std::string &_name)
{
    CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic(bg_sound[_name].file_name.c_str());
}
void CKAudioMng::setVolumeBgd(float _volume)
{
    CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(_volume);
};

void CKAudioMng::stopBackground()
{
    if(isPlayBackground())
        CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
   // b_next_sound = false;
    //    if(CocosDenshion::SimpleAudioEngine::sharedEngine()->isBackgroundMusicPlaying())
    //    {
    //        if(curent_bg->fade_out > 0.0)
    //        {
    //            fadeBackground(curent_bg->volume,0.0,curent_bg->fade_out);
    //        }
    //        else
    //        {
    //            CocosDenshion::SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(0.0);
    //        }
    //    }
};
CocosDenshion::SimpleAudioEngine* CKAudioMng::getSAE()
{
    return CocosDenshion::SimpleAudioEngine::sharedEngine();
}