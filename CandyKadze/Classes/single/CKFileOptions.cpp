//
//  CKFileOptions.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 28.11.12.
//
//

#include "CKFileOptions.h"
#include "CKFileInfo.h"
#include "CKLanguage.h"
#include <stdio.h>
#include <dirent.h>


void removeFileWithDirectory()
{
    struct dirent *next_file;
    DIR *theFolder;
    
    char filepath[256];
    std::string path = CCFileUtils::sharedFileUtils()->getWriteablePath().c_str();
    
    if((theFolder = opendir (path.c_str())) != NULL)
    {
        while ((next_file = readdir (theFolder)) != NULL)
            {
                // build the full path for each file in the folder
                sprintf(filepath, "%s/%s", path.c_str(), next_file->d_name);
                remove(filepath);
            }
    }
};

void removeFileWithDocumentsDirectory()
{
    struct dirent *next_file;
    DIR *theFolder;
    
    char filepath[256];
    std::string path = CCFileUtils::sharedFileUtils()->getDocumentPath().c_str();
    
    if((theFolder = opendir (path.c_str())) != NULL)
    {
        while ((next_file = readdir (theFolder)) != NULL)
        {
            // build the full path for each file in the folder
            sprintf(filepath, "%s/%s", path.c_str(), next_file->d_name);
            remove(filepath);
        }
    }
};
static CKFileOptions* newCKFileInfo = NULL;
CKFileOptions& CKFileOptions::Instance()
{
    if(!newCKFileInfo)
    {
        newCKFileInfo = new CKFileOptions;
    }
    return *newCKFileInfo;
};
CKFileOptions::~CKFileOptions()
{
    CC_SAFE_DELETE(newCKFileInfo);
}
void CKFileOptions::setDebugMode(bool _debug_mode)
{
#ifndef DEBUG_MODE
    debug_mode = _debug_mode;
#endif
   // CK::DEBUG_MODE = debug_mode;
};

bool CKFileOptions::isDebugMode()
{
    //CCLOG("DEBUG MODE %d",debug_mode);
#ifndef DEBUG_MODE
    return debug_mode;
#else
    return DEBUG_MODE;
#endif
};
CKFileOptions::CKFileOptions()
{
    is_first_launch = false;
}
void CKFileOptions::setKidsMode(bool _enable)
{
    kids_mode_enabled = _enable;
}

bool CKFileOptions::isKidsModeEnabled()
{
    return kids_mode_enabled;
};

bool CKFileOptions::isFirstLaunch()
{
    return is_first_launch;
};

void CKFileOptions::setFirstLaunch(bool _enab)
{
    CCAssert(!_enab, "we have only enabled lauch");
    is_first_launch = _enab;
};

bool CKFileOptions::isEveryPlayShow()
{
    return show_everyplay;
}
void CKFileOptions::setEveryPlayShow(bool _show)
{
    show_everyplay = _show;
}
bool CKFileOptions::isBannerShow()
{
    CCLOG("SHOW BANNER %d",debug_mode);
    return show_banner;
};

void CKFileOptions::setBannerShow(bool _show )
{
    show_banner = _show;
};

void CKFileOptions::enableIcloud(bool _enable)
{
    enable_icloud = _enable;
};

bool CKFileOptions::isEnableIcloud()
{
    return enable_icloud;
};

unsigned int CKFileOptions::getBombLvl()
{
    CCLOG("getBombLvl %d",bomb_lvl);
    return bomb_lvl;
};

bool CKFileOptions::isPlayMusic()
{
    return play_music;
};

bool CKFileOptions::isPlayEffect()
{
    return play_effect;
};

void CKFileOptions::setPlayMusic(bool _play)
{
    play_music = _play;
};

void CKFileOptions::setPlayEffect(bool _play)
{
    play_effect = _play;
}

void CKFileOptions::setBombLvl(unsigned char _d)
{
    bomb_lvl = _d;
    if(bomb_lvl < 1)
        bomb_lvl += 3;
    if(bomb_lvl > 3)
    {
        bomb_lvl -= 3;
    }
};

void CKFileOptions::load()
{
    //CK::DEBUG_MODE = false;
    debug_mode = false;
    
    show_banner = true;
    bomb_lvl = 1;
    play_effect = true;
    ObjCCalls::recognizingModel();
    show_everyplay = !ObjCCalls::isLowCPU();
    play_music = true;
    enable_icloud = false;
    kids_mode_enabled = false;
    is_first_launch = true;
    
    std::string st = ObjCCalls::getDeviceLang();
    st = "00";
    sprintf(language, "%s",st.c_str());
    
    std::string path = cocos2d::CCFileUtils::sharedFileUtils()->getDocumentPath();
    path.append(CK::file_setting);
    CCLOG("Load setting to %s",CK::file_setting);
    std::ifstream myfile (path.c_str());
    std::string line = "";
    bool file_exist = false;
    if (myfile.is_open())
    {
        file_exist = true;
        //is_first_launch = false;
        while (!myfile.eof() )
        {
            getline (myfile,line);
        };
        int idebug_mode,ishow_banner,ibomb_lvl,iplay_effect,iplay_sound,icloud,ieveryplay,ikids_mode,ifirst_launch;
        float ver = 0.0;
        CCLOG("line %s",line.c_str());
        sscanf(line.c_str(), "%d;%d;%d;%d;%d;%d;%d;%d;%d;%f;%f;%s", &idebug_mode,&ishow_banner,&ibomb_lvl,&iplay_effect,&iplay_sound,&icloud,&ieveryplay,&ikids_mode,&ifirst_launch,&version_language,&ver,language);

        if(ver == CK::CURRENT_VERSION_SETTING)
        {
#ifdef DEBUG_MODE
            debug_mode = DEBUG_MODE;
#else
            debug_mode = idebug_mode;
#endif
            show_everyplay = ieveryplay;
            show_banner = ishow_banner;
            bomb_lvl = ibomb_lvl;
            enable_icloud = icloud;
            kids_mode_enabled = ikids_mode;
            is_first_launch = ifirst_launch;
            play_music = iplay_sound;
            play_effect = iplay_effect;
        }
        else
        {
            //is_first_launch = true;
        }
       // myfile.close();
    };
    if(CK::CURRENT_VERSION_LANGUAGE  > version_language || !myfile)
    {
        removeFileWithDirectory();
    };

    
  //  std::string path = CCFileUtils::sharedFileUtils()->getWriteablePath().append(CK::file_setting);
   // std::ifstream ifile(path.c_str());
    
    bool replace = false;
    if(CK::CURRENT_VERSION_LANGUAGE  > version_language || !myfile)
    {
        CCLOG("version_language %f %f",!myfile);
        replace = true;
        version_language = CK::CURRENT_VERSION_LANGUAGE;
        save();
    };
    if(strcmp(language, "00") == 0)
    {
        CKLanguage::Instance().load(ObjCCalls::getDeviceLang());
    }
    else
    {
        CKLanguage::Instance().load(language);
    };
    
    CCLOG("language %s",language);
    CKLanguage::Instance().createFontAtlas(replace);
    myfile.close();
};
void CKFileOptions::setLanguage(const char* _lang,CCNode *_node,SEL_CallFuncND _func)
{
    sprintf(language,"%s",_lang);
    CCLOG("CKFileOptions::setLanguage %s",language);
    if(strcmp(language, "00") == 0)
    {
        CKLanguage::Instance().load(ObjCCalls::getDeviceLang());
    }
    else
    {
        CKLanguage::Instance().load(language);
    };
    //CKLanguage::Instance().load(language);
    CKLanguage::Instance().createFontAtlas(false);
};

const char* CKFileOptions::getLanguage()
{
    return language;
};

void CKFileOptions::loadLanguage()
{

};

void CKFileOptions::clearFile()
{
    removeFileWithDocumentsDirectory();
};

void CKFileOptions::save()
{
    CCLOG("save file setting %s",CK::file_setting);
    // std::cout<<<<std::endl;
    std::string path = cocos2d::CCFileUtils::sharedFileUtils()->getDocumentPath();
    path.append(CK::file_setting);
    std::ofstream out(path.c_str(), std::ios::out | std::ios::binary);
    if (out.is_open())
    {
        char str[NAME_MAX];
        sprintf(str, "%d;%d;%d;%d;%d;%d;%d;%d;%d;%.1f;%.1f;",debug_mode,show_banner,bomb_lvl,play_effect,play_music,enable_icloud,show_everyplay,kids_mode_enabled,is_first_launch,CK::CURRENT_VERSION_LANGUAGE,CK::CURRENT_VERSION_SETTING);
        out << str << language;
        out.close();
    };
};