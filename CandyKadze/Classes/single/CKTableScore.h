//
//  CKTableScore.h
//  CandyKadze
//
//  Created by PR1.Stigol on 19.06.13.
//
//

#ifndef __CandyKadze__CKTableScore__
#define __CandyKadze__CKTableScore__

#include <iostream>
#include <list>
#include "cocos2d.h"

struct CKScoreRecords
{
    char m_id[255];
    char name[255];
    int fields[12];
    int sort_field;
};

struct CKFriendLvlRecords {
    char name[255];
    int world[5];
};

class CKTableScoreBase
{
protected:
    std::list<CKScoreRecords*> sorted;
    std::list<CKScoreRecords*> score_list;
//    std::list<CKScoreRecords*> sorted_twitter;
//    std::list<CKScoreRecords*> twitter;
    std::list<CKScoreRecords*>::iterator it,t_it;
    
    std::list<CKFriendLvlRecords*> world_lvl;
    std::list<CKFriendLvlRecords*> sorted_world;
    std::list<CKFriendLvlRecords*>::iterator world_it;
    std::list<std::string> friends;
    
    int count_thread;
    cocos2d::SEL_CallFunc m_func;
    cocos2d::CCObject *callback_object;
    CKTableScoreBase();
public:
    virtual ~CKTableScoreBase();
    void saveScoreRecords(const char *_file_name,int _type);
    void loadScoreRecords(const char *_file_name,int _type);
    void saveFriendProgress(const char *_file_name);
    void loadFriendProgress(const char *_file_name);
    
    void setCallbackFunc(cocos2d::CCObject *_obj,cocos2d::SEL_CallFunc _func);
    void addRecords(const char *_id,const char *_name,int _total,int _best_hiscore,int _ordinary_using,int _special_using,int _accuracy_rating,int _block_destroy,int _plane_flights,int _world_pixel,int _world_caramel,int _world_choco,int _world_ice,int _world_waffel);
    void addLvlRecorld(std::string _name,int _world_pixel,int _world_caramel,int _world_choco,int _world_ice,int _world_waffel);
    
    void addFriend(std::string _id);
    //void addTwitterFriend(std::string _id);
    
    void clearFriends();
   // void clearTwitterFriends();
    void clearLvlRecord();
    void clearScoreRecords();
    //void clearTwitterRecords();
    void showScoreField(int _num_field);
        
    std::list<CKScoreRecords*> showField(int _num_field);
    
    void printList(std::list<CKScoreRecords*> *_list,int _num_filed);
    std::list<CKScoreRecords*>* getField(int _num_field);
    
    std::list<CKFriendLvlRecords*>* getFriendsOpenLvlinWorld(int _world);
    std::list<std::string> *getFriends();
    
    void incThread();
    void decThread();
    bool scoreEmpty();
    bool friendEmpty();
    //bool listTwitterEmpty();
    const int &getCountTh();
    void addWaitThread(int _count);
    
    static CKTableScoreBase& Instance();
};
class CKTableFacebook: public CKTableScoreBase
{
    CKTableFacebook();
public:
    virtual ~CKTableFacebook();
    static CKTableFacebook& Instance();
};
class CKTableTwitter: public CKTableScoreBase
{
    CKTableTwitter();
public:
    virtual ~CKTableTwitter();
    static CKTableTwitter& Instance();
};
#endif /* defined(__CandyKadze__CKTableScore__) */
