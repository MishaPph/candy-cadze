//
//  StatisticLog.h
//  CandyKadze
//
//  Created by PR1.Stigol on 25.07.13.
//
//

#ifndef __CandyKadze__StatisticLog__
#define __CandyKadze__StatisticLog__

#include <iostream>
#include "cocos2d.h"
namespace CK
{
    class StatisticLog: public cocos2d::CCObject
    {
        struct pageLog
        {
            int id;
            int number;
            pageLog()
            {
                number = 0;
                id = -1;
            }
        };
        pageLog m_page;
        void *data;
        StatisticLog();
        int current_page_number;
        int time_in_page;
        int current_page_id;
        std::string session_name;
        void updateTimer();
        typedef const int& cint;
    public:
        void createSession();
        void setNewPage(const int &_id,int _status = 0);
        void enterBackground();
        void enterForeground();
        void setLP(cint _world,cint _lvl,cint _speed,cint _state, cint _count_big_cube,cint _spec_bomb_allow,cint _time_game,cint _plane_start,cint _count_loop_plane,cint _bomb_hit,cint _bomb_fail,cint _destroy_cube,cint _bomb_default,cint _bomb_spec,cint _score);
        void setLink(cint _linkid);
        void setBuyinApp(const char *_id,cint _total);
        void setBuyBomb(cint _id,cint _count,cint _cost,cint _discount);
        void setSpecBomb(cint _world,cint _lvl,cint _count);
        void setTryBuy(const char *_id,cint _count);
        void save();
       // void sendOld();
        void showLog();
        ~StatisticLog();
        static StatisticLog &Instance();
    };
}
#endif /* defined(__CandyKadze__StatisticLog__) */
