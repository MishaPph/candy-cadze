//
//  CKAudioMgr.h
//  CandyKadze
//
//  Created by PR1.Stigol on 08.10.12.
//
//

#ifndef __CandyKadze__CKAudioMgr__
#define __CandyKadze__CKAudioMgr__

#include <iostream>
#include <map.h>
#include <list.h>
#include "SimpleAudioEngine.h"
#include "CK.h"
#include "CKFileOptions.h"
#include <OpenAL/al.h>
#include "cocos2d.h"
#include <pthread.h>

using namespace cocos2d;


struct CKSoundLooping
{
    unsigned int sound_id;
    signed int count_loop;
    signed int curent_loop;
    float last_position;
};

struct CKSound
{
    int id;
    std::string file_name;
    float fade_in;
    float fade_out;
    float volume;
    unsigned int sound_id;
    signed int loop;
    float curent_time;
    float step;
    float curent_volume;
    bool fade;
    bool start_fadeout;
};

struct CKFadeValue
{
    float end;
    float step;
};

class CKAudioMng: public CCObject
{
    std::map<std::string,CKSound> world_sound;
    std::map<std::string,CKSound>::iterator it;
    std::map<std::string,CKSound> bg_sound;
    std::list<CKSound *> update_sound;
    std::list<CKSoundLooping> update_loop;
    std::list<CKSoundLooping>::iterator update_loop_it;
    std::list<CKSound *> fadeout_sound;
    std::list<CKSound *>::iterator update_it;
    CKSound *curent_bg;
    bool tmp;
    //bool play_music;
    //bool play_effect;
    float bg_volume;    
    
    float step;
    int start_frame;
    int end_frame;
    bool start_fade_bg;
    bool is_background_pause;
    CCObject obj_schedule;
    float getStepVolume();
    std::string next_sound;
   // bool b_next_sound;
   // bool load_next_sound;
    std::string sound_name;
    
    int fade_effect_id;
    float effect_fade_volume;

    CKFadeValue bg_fade;
    pthread_t m_bg_thread;
public:
    
    float step_volume;
    void preloadWorldSound(int _world = 0);

    void playNextSound(const std::string &_sound);
    void removeAllEffect();
    void removeBackground();
    
    //void update(const float &_dt);//need fade sound effect
    void playMenuSound();
    
    void fadeBgToVolume(const float  &_end,const float &_time = CK::TIME_TO_FADE_BEACKGROUND_SOUND);
    void fadeBgAndLoadSound(const std::string &_name);
    //void fadeSchedule(const float &_dt);
    void fadeBgSchedule(const float &_dt);
    
    void fadeInEffect(const std::string &_name);
    void fadeInEffectSchedule(const float &_dt);
    void fadeOutEffect(const std::string &_name);
    void fadeOutEffectSchedule(const float &_dt);
    //bool getFade();
    void pauseAllBackGround();
    bool isPlayBackground();
    bool isPlayEffect(const std::string &_sound);
    //void changePlayMusic();
    //void changePlayEffect();
    void playBackground(const std::string &_sound,const float &_volume = -1.0);
    void loadWorldSound(const std::string &_world);
    void preloadBgd(const std::string &_name);
    void pauseBackground();
    void pauseAllEffect();
    void resumeAllEffect();
    void resumeBackground();
    //void stopFade();
    void stopBackground();
    void setVolumeBgd(float _volume);
    void stopAllEffect();
    void playEffect(const std::string &_sound);
    void playBG();
    void stopMusic(const std::string &_sound);
    void stopEffect(const std::string &_sound);
    void setEffectSpeed(const std::string &_sound,float _speed);
    CocosDenshion::SimpleAudioEngine* getSAE();
    
    static CKAudioMng& Instance();
};

#endif /* defined(__CandyKadze__CKAudioMgr__) */
