//
//  CKSystemInfo.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 28.11.12.
//
//

#include "CKSystemInfo.h"

void CKSystemInfo::init()
{

    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_port = mach_host_self();
    host_page_size(host_port, &pagesize);
    
    //CPU
    task_info_count = TASK_INFO_MAX;
    kr = task_info(mach_task_self(), TASK_BASIC_INFO, (task_info_t)tinfo, &task_info_count);
    
    
    if (kr != KERN_SUCCESS) {
        return;
    }
}

unsigned int CKSystemInfo::getFreeMemory()
{
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &(host_size)) != KERN_SUCCESS) {
        return 0;
    }
    /* Stats in bytes */
    return vm_stat.free_count * pagesize;
};

float CKSystemInfo::getCPU()
{
    if (kr != KERN_SUCCESS) {
        return -1;
    }
    basic_info = (task_basic_info_t)tinfo;
    // get threads in the task
    kr = task_threads(mach_task_self(), &thread_list, &thread_count);
    if (kr != KERN_SUCCESS) {
        return -1;
    }
    if (thread_count > 0)
        stat_thread += thread_count;
  //  printf(" thread_count %d",thread_count);
    long tot_sec = 0;
    long tot_usec = 0;
    float tot_cpu = 0;
    int j;
    
    for (j = 0; j < thread_count; j++)
    {
        thread_info_count = THREAD_INFO_MAX;
        kr = thread_info(thread_list[j], THREAD_BASIC_INFO,
                         (thread_info_t)thinfo, &thread_info_count);
        if (kr != KERN_SUCCESS) {
            return -1;
        }
        
        basic_info_th = (thread_basic_info_t)thinfo;
        
        if (!(basic_info_th->flags & TH_FLAGS_IDLE)) {
            tot_sec = tot_sec + basic_info_th->user_time.seconds + basic_info_th->system_time.seconds;
            tot_usec = tot_usec + basic_info_th->system_time.microseconds + basic_info_th->system_time.microseconds;
            tot_cpu = tot_cpu + basic_info_th->cpu_usage / (float)TH_USAGE_SCALE * 100.0;
        }
    } // for each thread
    
    kr = vm_deallocate(mach_task_self(), (vm_offset_t)thread_list, thread_count * sizeof(thread_t));
 //   assert(kr == KERN_SUCCESS);
    
    return tot_cpu;
};