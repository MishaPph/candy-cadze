//
//  CKFileInfo.h
//  CandyKadze
//
//  Created by PR1.Stigol on 26.09.12.
//
//

#ifndef __CandyKadze__CKFileInfo__
#define __CandyKadze__CKFileInfo__

#include <iostream>
#include <list>
#include <cocos2d.h>
#include "CK.h"
#include "CKHelper.h"
#include "ShopBomb.h"

using namespace cocos2d;

struct Level
{
    unsigned int score;
    unsigned int time;
};

struct World
{
    std::string name;
    unsigned char open_lvl;
    Level m_level[CK::COUNT_LEVEL_IN_WORLD];
  //  unsigned char last_speed;
private:
};

struct Data{
   // unsigned char last_difficult;
    std::list<World> m_world;
    std::list<std::string> open_world;
    std::vector<CK::Inventory> m_inventory;
    //std::vector<CK::Inventory> m_inventory_sel;
    CK::Inventory m_inventory_sel[4];
    std::list<unsigned char> open_difficult;
    void clear()
    {
        m_inventory.clear();
        open_difficult.clear();
        m_world.clear();
        open_world.clear();
    };
};

class CKScore
{

    
    unsigned int count_loop_plane;
    unsigned int count_destroy_cube;
    unsigned int accuvate_shots;
    unsigned int count_orginary_bomb;
    unsigned int count_special_bomb;
    unsigned int count_hit_bomb;
public:
    unsigned long balance_score;//is value label score in shop,inventory,world
    unsigned long icloud_score;//is value use to icloud sync
    unsigned long score_total; //is value send to game center and parse
//    unsigned long getBalanceScore();
//    unsigned long getTotalScore();
    
    unsigned int getCountDestroyCube();
    unsigned int getCountLoopPlane();
    unsigned int getAccuvateShots();
    unsigned int getCountOrginaryBomb();
    unsigned int getCountSpecialBomb();
    unsigned int getCountHitBomb();
    
    void addCountDestroyCube(int _value);
    void addCountLoopPlane(int _value);
    void addAccuvateShots(int _value);
    void addCountOrginaryBomb(int _value);
    void addCountSpecialBomb(int _value);
    void addCountHitBomb(int _value);
    //void addAbsoluteScore(int _value);
    //void addAbsoluteHiScore(int _value);
    
    void clear();
    void load(CCDictionary *_dict);
//    void save();
};
struct  CKSocial
{
    std::string twID,twName,fbID,fbName;
};
class CKFileInfo
{
    CKFileInfo();
    World *curent_world;
    Level *curent_lvl;
    CKScore *m_score;
    
    CKSocial m_social;
    float version;
    std::string filename;
    bool likes_our,likes_any,follow_our,follow_any,tell_friend,gift_disable,app_rate;//true then operation complite
    Data m_data;
    int black_list;
    int current_world;

    
    int data_last_shop_gen;
    std::list<int> show_tutor;
    
    unsigned int hash_data;
    //std::string language;
    void initBombDiscount(int r1 ,int r2,int r3);
    const std::string getCurentData();
    long multi_data;
    int mult_koef;
public:
    ~CKFileInfo();
    std::map<int, ShopBomb*> bomb_info_map;
    std::vector<int> open_bonus;
    std::vector<int> open_bonus_airplane;
    bool is_load;
    CCDictionary *dict;
    
    bool isOpenBonus(int _t);
    int isNeedUpgradeBomb(const int &_id);
    void genWorld(const std::string &world_name);
    void genShopBombWithCurentData();
    
    CK::Inventory* getInvSelect(int i);
    bool isEmptyInvSelect();
    bool isEmptyInv();
    void clearInventory();
    bool addToInventorySelect(unsigned char _type,unsigned int _count = 1);
    void removeInventorySelectBomb(unsigned char _type,unsigned int _count = 1);
    void addToInventory(unsigned char _type,unsigned int _count = 1);
    int getCountInInventory(unsigned char _type);
    int getCountInInventorySelect(unsigned char _type);
    void clearInvetorySelect();
    World * getCurentWorld();
    CKScore* getScore()
    {
        return m_score;
    };
    void genDiscountBombAllPeriodAndSave();
    
    bool isNeedShowTutorial(int _num);
    void deleteShowTutorial(int _num);
    
    void setLevel(int _lvl);
    bool isOpenLvl(int _lvl);
    void addWorld(const std::string &world_name);
    void addDifficult(int _number);

    bool isOpenWorld(const char* _name);
    
    void setMultiBuyData(long _time);
    long getMultiBuyData()const;
    void setMultiBuyKoef(const int &_value);
    int getMultiBuyKoef() const;
    
    bool isLikesOur();
    void setLikesOur(bool _likes);
    bool isLikesAny();
    void setLikesAny(bool _likes);
    
    bool isFollowOur();
    void setFollowOur(bool _follow);
    bool isFollowAny();
    void setFollowAny(bool _follow);
    
    bool isTellFriend();
    void setTellFriend(bool _en);
    bool isGift();
    void setGift(bool _en);
    bool isAppRate();
    void setAppRate(bool _value);
    
    bool isBlackList();

    int getCountOpenLvlInAllWorld();
    bool isOpenDifficult(int _num);
    void addOpenBonus(unsigned int _id);
    Level* getLevel();
    Level* getLevel(int _number);
    int getLevelScore(int _number);
    int getCurentOpenLevel();
    void setCurentOpenLevel(int _lvl);
    
    void loadShopInfo(CCDictionary *_dict);
    void load(bool _local = false);
    void loadWithDict(CCDictionary *_dict ,bool documents, bool _local = false);
    void reload();
    void loadBin(const char *_filename);
    void save();
    void saveBin(const char *_filename);
    void dump();
    
    void showInventory();
    std::vector<CK::Inventory> getInventory();
    const CK::Inventory &getInventorySelect(const int &_num)const;
    void setInventorySelect(int _num_cell,unsigned char _type,int _count);
    bool addScore(int _score);
    void addBigScore(const int _score,bool _buy_coins);
    unsigned int getTotalScore();
    unsigned int getScoreCurrentWorld();
    unsigned int getScoreAllWorld();
    void setFileName(const char* _filename);
    void saveTmp(const char * _message);
    std::string loadTmp();
    
    int getLvlOpenWithWorld(const std::string &_name);
    std::string calcHeshData();
    static CKFileInfo& Instance();
};

#endif /* defined(__CandyKadze__CKFileInfo__) */
