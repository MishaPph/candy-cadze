//
//  CKSystemInfo.h
//  CandyKadze
//
//  Created by PR1.Stigol on 28.11.12.
//
//

#ifndef __CandyKadze__CKSystemInfo__
#define __CandyKadze__CKSystemInfo__

#include <iostream>
#include <string>
#include <stdio.h>
#include <time.h>
#include <ctime>
#include <fstream>
#import <mach/mach.h>
#import <mach/mach_host.h>

class CKSystemInfo
{
private:
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    vm_statistics_data_t vm_stat;
    
    //CPU
    kern_return_t kr;
    task_info_data_t tinfo;
    mach_msg_type_number_t task_info_count;
    
    task_basic_info_t      basic_info;
    thread_array_t         thread_list;
    mach_msg_type_number_t thread_count;
    
    thread_info_data_t     thinfo;
    mach_msg_type_number_t thread_info_count;
    
    thread_basic_info_t basic_info_th;
    uint32_t stat_thread = 0; // Mach threads
public:
    void init();
    unsigned int getFreeMemory();
    float getCPU();
    static CKSystemInfo& Instance()
    {
        static CKSystemInfo newCKSystemInfo;
        return newCKSystemInfo;
    };
};

#endif /* defined(__CandyKadze__CKSystemInfo__) */
