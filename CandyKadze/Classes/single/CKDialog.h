//
//  CKDialog.h
//  CandyKadze
//
//  Created by PR1.Stigol on 27.12.12.
//
//

#ifndef __CandyKadze__CKDialog__
#define __CandyKadze__CKDialog__

#include <iostream>
#include <cocos2d.h>
#include <list.h>

#include "CK.h"

class CKGUI;

using namespace cocos2d;

class CKDialog:public CCLayer
{
    SEL_CallFuncN m_selector;
    SEL_CallFuncND m_info_selector;
    
    CKGUI* m_dialog,*m_info_msg;
    CCLayer *target;
    
    CCSize win_size;
    const int MODAL_DIALOG_ID = 10;
    const int LABEL_MSG_ID = 1;
    const int LABEL_SMALL_MSG_ID = 2;
    void hide();
    virtual void ccTouchesBegan(cocos2d::CCSet* pTouches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);
public:
    CKDialog();
    ~CKDialog();
    void callDialog(CCNode* _sender,void *_value);
    void callMessage(CCNode* _sender,void *_value);
    
    void setSelector(SEL_CallFuncN _selector);
    void setInfoSelector(SEL_CallFuncND _selector);
    void setTarget(CCLayer *_layer);
    void setConfirmText(const std::string &_str,const char* small_text = NULL);
    CKGUI *getInfoGui();
    void setInfoText(int _id,const char *_top,const char* _bottom);
    
    void show(SEL_CallFuncN _selector = NULL);
    void showMessage(const int _id,SEL_CallFuncND _selector = NULL);
    void setDialogText(const std::string &_str);
};
#endif /* defined(__CandyKadze__CKDialog__) */
