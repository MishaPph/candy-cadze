//
//  CKAdBanner.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 22.04.13.
//
//

#include "CKAdBanner.h"
#include <dirent.h>
#include "cocos2d.h"
#include <iostream>
#include <fstream>
#include "CK.h"

static CKAdBanner *newCKAdBanner = NULL;
float CKAdBanner::koef_showed = 0.0f;
#define DEFAULT_URL "http://www.stigol.com/"
#define OFFLINE_BANNER CK::loadImage("baner_offline.png").c_str()

using namespace cocos2d;
CKAdBanner::CKAdBanner()
{
    is_load = false;
    current = NULL;
};

CKAdBanner::~CKAdBanner()
{
    it = m_ad.begin();
    while (it != m_ad.end()) {
        (*it)->dict->release();
        delete *it;
        *it = NULL;
        it++;
    };
    delete newCKAdBanner;
    newCKAdBanner = NULL;
};

CKAdBanner& CKAdBanner::Instance()
{
    if(!newCKAdBanner)
    {
        newCKAdBanner = new CKAdBanner;
    };
    return *newCKAdBanner;
};

void CKAdBanner::load()
{
    // if(is_load)
    //     return;
    
    it =  m_ad.begin();
    while (it != m_ad.end()) {
        delete *it;
        it++;
    }
    m_ad.clear();
    
    DIR *dir;
    struct dirent *ent;
    sum_percent = 0;
    koef_showed = 0;
    std::string path = CCFileUtils::sharedFileUtils()->getWriteablePath().c_str();
    //path.append("ck.banner/");
    if ((dir = opendir (path.c_str())) != NULL) {
        /* print all the files and directories within directory */
        while ((ent = readdir (dir)) != NULL) {
            CCLOG("load file in directory %s",ent->d_name);
            std::string tmp_str = ent->d_name;
            int nPosRight  = tmp_str.find('.');
            
            std::string _sufix = tmp_str.substr(nPosRight + 1,tmp_str.length());
            std::string _name = tmp_str.substr(0,nPosRight);
            //  char tmp[255];
            
            //sprintf(char *, const char *, ...)
            if(strcmp(_sufix.c_str(), "plist") == 0 )
            {
                std::string file_name = path;
                
                std::string image = _name;
                if(CCDirector::sharedDirector()->getWinSize().width == 1024)
                    image.append("_p.png");
                else
                    image.append(".png");
                file_name.append(image);
                std::ifstream my_file(file_name.c_str());
                
                if(my_file.good())
                {
                    std::string file_name = path;
                    file_name.append(ent->d_name);
                    CCDictionary* dict = CCDictionary::createWithContentsOfFile(file_name.c_str());
                    CCDictionary* setting = (CCDictionary*)dict->objectForKey("Settings");
                    dict->retain();
                    
                    int y,m,d;
                    sscanf(dictStr(setting, "Start_date"),"%d.%d.%d",&d,&m,&y);
                    
                    time_t tim = time(0);
                    struct tm  tstruct;
                    tstruct = * std::localtime(&tim);
                    
                    // printf ("Start_date %.0m %.0d s\n",(tstruct.tm_mon - (m - 1))*30 + (tstruct.tm_mday - d));
                    
                    int time_delay = (tstruct.tm_mon - (m - 1))*30 + (tstruct.tm_mday - d);
                    
                    if(time_delay > (dictInt(setting, "Normal_period_duration") + dictInt(setting, "Active_period_duration")))
                    {
                        dict->release();
                        continue;
                    };
                    
                    
                    Banner *ban = new Banner;
                    if(time_delay <= dictInt(setting, "Active_period_duration"))
                    {
                        ban->value = dictInt(setting, "Active_period_probability");
                    }
                    else
                    {
                        ban->value = dictInt(setting, "Normal_period_probability");
                    };
                    koef_showed += ban->value;
                    ban->dict = dict;
                    ban->name = image;
                    m_ad.push_back(ban);
                    
                    sum_percent += ban->value;
                    
                    CC_SAFE_RELEASE_NULL(dict);
                    my_file.close();
                    printf ("S load:: %s %d %d %d %d\n", ent->d_name,d,m,y,ban->value);
                }
                else
                {
                    printf ("load error:: %s %s\n", ent->d_name,file_name.c_str());
                }
            };
        }
        closedir (dir);
    } else {
        /* could not open directory */
        perror ("");
    };
    CCLOG("CKAdBanner::load %d",m_ad.size());
    if(m_ad.empty())
    {
        koef_showed = 0.0;
    }
    else
    {
        koef_showed /= m_ad.size();
        koef_showed /= 100.0f;
    }
    is_load = true;
};

void CKAdBanner::setDisable(bool _disable)
{
    disbaled = _disable;
};
bool CKAdBanner::isDisable()
{
    return disbaled;
};
void CKAdBanner::generated()
{
    if (disbaled) {
        current = NULL;
        return;
    };
    
    // current = NULL;
    Banner * tmp = current;
    //  CCLOG("generated %f %f",rand_value,curr_value);
    srand(time(0));
    
    do {
        //  CCLOG("generated %d %d",tmp,current);
        float rand_value = float(rand()%100)/100.0;
        float curr_value = 0.0f;
        
        for (int i = 0; i < m_ad.size(); ++i) {
            Banner *ban = m_ad[i];
            CCLOG("current %s %f %f",m_ad[i]->name.c_str(),curr_value,rand_value);
            current = m_ad[i];
            if(curr_value > rand_value)
            {
                if(i > 0)
                    current = m_ad[i-1];
                break;
            };
            curr_value += float(ban->value)/float(sum_percent);
            it++;
        }
        
    } while (tmp == current && current != NULL && (m_ad.size() > 2));
    // CCLOG("generated %d %d",tmp,current);
};

const char * CKAdBanner::getURL()
{
    if(current)
    {
        if(current->dict && (CCDictionary*)current->dict->objectForKey("Settings"))
        {
            CK::StatisticLog::Instance().setLink(CK::Link_StigolBanner);
            CCDictionary  *sdict = (CCDictionary*)current->dict->objectForKey("Settings");
            return dictStr(sdict, "Target");
        }
        else
        {
            CK::StatisticLog::Instance().setLink(CK::Link_OfflineBanner);
            return DEFAULT_URL;
        }
    }
    else
    {
        CK::StatisticLog::Instance().setLink(CK::Link_OfflineBanner);
        return DEFAULT_URL;
    }
};

const char * CKAdBanner::show()
{
    if(current && !disbaled)
    {
        CCLOG("CKAdBanner::show:: %s",current->name.c_str());
        return current->name.c_str();
    } 
    else
    {
        return OFFLINE_BANNER;
    }
};

