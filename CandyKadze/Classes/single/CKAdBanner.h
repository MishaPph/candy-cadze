//
//  CKAdBanner.h
//  CandyKadze
//
//  Created by PR1.Stigol on 22.04.13.
//
//

#ifndef __CandyKadze__CKAdBanner__
#define __CandyKadze__CKAdBanner__

#include <iostream>
#include "cocos2d.h"
class CKAdBanner
{
    struct Banner
    {
        cocos2d::CCDictionary *dict;
        std::string name;
        int value;
    };
    int sum_percent;
    Banner * current;
    CKAdBanner();
    std::vector<Banner *> m_ad;
    std::vector<Banner *>::iterator it;
    bool is_load;
    bool disbaled;
public:
    static float koef_showed;
    ~CKAdBanner();
    void load();
    void setDisable(bool _disable = true);
    bool isDisable();
    void generated();
    const char * show();
    const char * getURL();
    static CKAdBanner &Instance();
};

#endif /* defined(__CandyKadze__CKAdBanner__) */
