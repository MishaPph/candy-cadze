//
//  CKDialog.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 27.12.12.
//
//
#include "CKLoaderGUI.h"
#include "CKDialog.h"

CKDialog::CKDialog()
{
    CCLOG("CKDialog::CKDialog");
    this->setTouchEnabled(false);
    m_dialog = new CKGUI();
    m_dialog->create(CCNode::create());
    m_dialog->load(CK::loadFile("c_Confirmation.plist").c_str());
    m_dialog->setTouchEnabled(true);
    m_dialog->setTarget(this);
    m_dialog->addFuncToMap("callDialog", callfuncND_selector(CKDialog::callDialog));
    m_dialog->getChildByTag(MODAL_DIALOG_ID)->getChildByTag(LABEL_SMALL_MSG_ID)->setVisible(false);                      
    target = NULL;
    win_size = CKHelper::Instance().getWinSize();
    
//    CCSprite *m_sprite = CCSprite::create("gray_mask.png");
//    addChild(m_sprite,-1);
//    m_sprite->setScaleX(win_size.width/m_sprite->getContentSize().width);
//    m_sprite->setScaleY(win_size.height/m_sprite->getContentSize().height);
//    m_sprite->setPosition(ccp(win_size.width/2,win_size.height/2));
//    m_sprite->setVisible(false);
    addChild(m_dialog->get(),99);

    retain();

    m_info_msg = new CKGUI;
    m_info_msg->create(CCNode::create());
    m_info_msg->load(CK::loadFile("c_InfoMessage.plist").c_str());
    m_info_msg->setTarget(this);
    m_info_msg->addFuncToMap("menuCall", callfuncND_selector(CKDialog::callMessage));
    m_info_msg->addFuncToMap("menuCallback", callfuncND_selector(CKDialog::callMessage));
    m_info_msg->setTouchEnabled(true);
    m_info_msg->get()->setVisible(false);

    addChild(m_info_msg->get(),99);
    
    if (CCDirector::sharedDirector()->getWinSize().width == 568) {
        m_info_msg->get()->setPosition(ccp(44,0));
        m_dialog->get()->setPosition(ccp(44,0));
    }
};

CKGUI *CKDialog::getInfoGui()
{
    return m_info_msg;
};

CKDialog::~CKDialog()
{
    CC_SAFE_DELETE(m_dialog);
    CC_SAFE_DELETE(m_info_msg);
    release();
};

void CKDialog::setConfirmText(const std::string &_str,const char* small_text)
{
    CCLOG("setConfirmText::%s",_str.c_str());
    m_dialog->getChildByTag(MODAL_DIALOG_ID)->getChildByTag(LABEL_MSG_ID)->setText(_str);
    if(small_text)
    {
        m_dialog->getChildByTag(MODAL_DIALOG_ID)->getChildByTag(LABEL_SMALL_MSG_ID)->setVisible(true);
        m_dialog->getChildByTag(MODAL_DIALOG_ID)->getChildByTag(LABEL_SMALL_MSG_ID)->setText(small_text);
    }
    else{
        m_dialog->getChildByTag(MODAL_DIALOG_ID)->getChildByTag(LABEL_SMALL_MSG_ID)->setVisible(false);
    }
};

void CKDialog::setInfoText(int _id,const char *_top,const char* _bottom)
{
    CCLOG("setInfoText::%d %s %s",_id,_top,_bottom);
    m_info_msg->getChildByTag(_id)->getChildByTag(1)->setText(_top);
    m_info_msg->getChildByTag(_id)->getChildByTag(2)->setText(_bottom);
};

void CKDialog::setInfoSelector(SEL_CallFuncND _selector)
{
    m_info_selector = _selector;
};

void CKDialog::setSelector(SEL_CallFuncN _selector)
{
    m_selector = _selector;
};

void CKDialog::callMessage(CCNode* _sender,void *_value)
{
    hide();
    if(*static_cast<int *>(_value) != CK::Action_Close)
    {
        (target->*m_info_selector)(_sender,_value);
    };
};

void CKDialog::callDialog(CCNode* _sender,void *_value)
{
    if(*static_cast<int*>(_value) == 1)
    {
        (target->*m_selector)(target);
    }
    else
    {
        (target->*m_selector)(NULL);
    };
    hide();
};

void CKDialog::ccTouchesBegan(cocos2d::CCSet* pTouches, cocos2d::CCEvent* event)
{
    CCTouch* touch =(CCTouch*)*pTouches->begin();
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    if(m_dialog->get()->isVisible() && !m_dialog->setTouchBegan(location))
    {
        if(location.x > win_size.width*0.1 && location.x < win_size.width*0.9)
            if(location.y > win_size.height*0.1 && location.y < win_size.height*0.9)
                hide();
    };
    
    if(m_info_msg->get()->isVisible()&& !m_info_msg->setTouchBegan(location))
    {
        if(location.x > win_size.width*0.1 && location.x < win_size.width*0.9)
            if(location.y > win_size.height*0.1 && location.y < win_size.height*0.9)
                hide();
    };
};

void CKDialog::ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent)
{
    CCTouch* touch =(CCTouch*)*pTouches->begin();
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    m_dialog->setTouchMove(location);
    m_info_msg->setTouchMove(location);
}

void CKDialog::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
{
    CCTouch* touch =(CCTouch*)*pTouches->begin();
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    m_dialog->setTouchEnd(location);
    m_info_msg->setTouchEnd(location);
}

void CKDialog::setTarget(CCLayer *_layer)
{
    if(target)
    {
        if(_layer != target)
        {
         //   target->removeFromParentAndCleanup(false);
        }
        target = _layer;
    }
    else
    {
        target = _layer;
    }
};

void CKDialog::showMessage(const int _id,SEL_CallFuncND _selector)
{
    if(_selector)
    {
        m_info_selector = _selector;
    };
    if(this->getParent() == NULL)
        target->getParent()->addChild(this,99);
   
    for (int i = 0; i < 8; i++) {
        m_info_msg->getChildByTag(i + 1)->setVisible(false);
    };
    
    m_dialog->get()->setVisible(false);
    m_info_msg->get()->setVisible(true);

    m_info_msg->getChildByTag(_id)->setVisible(true);
    
    m_info_msg->setTouchEnabled(true);
    CCLOG("show:: %d",target->getParent());
    this->setTouchEnabled(true);
    target->setTouchEnabled(false);
};

void CKDialog::show(SEL_CallFuncN _selector)
{
    if(_selector)
    {
        m_selector = _selector;
    };
    if(this->getParent() == NULL)
        target->getParent()->addChild(this,99);
    
    CCLOG("show:: %d",target->getParent());
    m_dialog->get()->setVisible(true);
    m_info_msg->get()->setVisible(false);
    this->setTouchEnabled(true);
    target->setTouchEnabled(false);
};

void CKDialog::hide()
{
    m_info_msg->get()->setVisible(false);
    m_dialog->get()->setVisible(false);
    this->setTouchEnabled(false);
    //target->getParent()->removeChild(this,true);
    this->removeFromParentAndCleanup(false);
    target->setTouchEnabled(true);
};

void CKDialog::setDialogText(const std::string &_str)
{
    
};
