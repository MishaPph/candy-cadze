////
////  CKStaticInfo.cpp
////  CandyKadze
////
////  Created by PR1.Stigol on 16.11.12.
////
////
//
//#include "CKStaticInfo.h"
//#include "CK.h"
//#include "IPAddress.h"
//
//
//static CKStaticFile *newCKStaticFile = NULL;
//CKStaticFile& CKStaticFile::Instance()
//{
//    if(!newCKStaticFile)
//    {
//        newCKStaticFile = new CKStaticFile;
//    }
//    return *newCKStaticFile;
//};
//void CKStaticFile::clear()
//{
//    player_name = "";
//    time_start = "";
//    world = 255;
//    lvl_number = 0;
//    diff_number = 0;
//    lvl_state = false;
//    cube_count = 0;
//    bomb_special = 0;
//    time = 0;
//    airplane_loop = 0;
//    bomb_complite = 0;
//    bomb_failed = 0;
//    cube_destroy = 0;
//    bomb_base_count = 0;
//    bomb_count = 0;
//    lvl_score = 0;
//};
//
//void CKStaticFile::newFileName(const time_t * tim)
//{
//    struct tm  tstruct;
//    char       buf[80],str[NAME_MAX];
//    tstruct = * std::localtime(tim);
//    strftime(buf, sizeof(buf), "D%y%m%dT%H%M%S", &tstruct);
//    
//    IPAddresses *m_ip = new IPAddresses;
//    sprintf(str, "%s%s",m_ip->getWH().c_str(),buf);
//    
//    CCLOG("newFileName:: str %s",str);
//    file_name = str;
//    
//    CC_SAFE_DELETE(m_ip);
//};
//
//void CKStaticFile::setFileName(const std::string &_file)
//{
//    file_name = _file;
//};
//
//const std::string& CKStaticFile::getFileName()
//{
//    return file_name;
//};
//
//void CKStaticFile::setStartInfo(const std::string &_name,const int _world,int _lvl,int _diff,const int _base_height)
//{
//    CCLOG("player name %s world %d lvl %d",_name.c_str(),_world,_lvl);
//    player_name = _name; world = _world; lvl_number = _lvl; diff_number = _diff; base_airplane_height = _base_height;
//};
//
//void CKStaticFile::setBombInfo(const int _special,const int _count_hit,const int _count_fail,const int _count_default_bomb,const int _count_spec)
//{
//    bomb_special = _special;
//    bomb_complite = _count_hit;
//    bomb_failed = _count_fail;
//    bomb_base_count = _count_default_bomb;
//    bomb_count = _count_spec;
//};
//
//void CKStaticFile::setEndInfo(const int _time,const int _score,const bool _state,const int _count_cube,const int _count_active_cube,const int _air_height,const unsigned int _unused_spec)
//{
//    time = _time; lvl_score = _score; lvl_state = _state;
//    cube_count = _count_cube;
//    cube_destroy = cube_count*4 - _count_active_cube;
//    airplane_loop = _air_height;
//    unused_spec_bomb = _unused_spec;
//    CCLOG("_count_cube %d cube_destroy %d",_count_cube,cube_destroy);
//};
//
//const std::string CKStaticFile::setStartTime(const time_t &now)
//{    
//    struct tm  tstruct;
//    char       buf[80];
//    
//    tstruct = *localtime(&now);
//
//    strftime(buf, sizeof(buf), "%x,%X", &tstruct);
//    time_start = buf;
//    return buf;
//}
//
//int CKStaticFile::getFileSize()
//{
//    std::string path = cocos2d::CCFileUtils::sharedFileUtils()->getDocumentPath();
//    path.append(file_name);
//    path.append(".csv");
//    std::streampos fsize = 0;
//    std::ifstream myfile (path.c_str());
//    int Size = 0;
//    if(myfile.is_open())
//    {
//        fsize = myfile.tellg();
//        myfile.seekg( 0, std::ios::end );
//        fsize = myfile.tellg() - fsize;
//        myfile.close();
//        Size = fsize;
//    };
//    return Size;
//};
//
////load static of file and after load remove file
//const char* CKStaticFile::load()
//{
//    std::string path = cocos2d::CCFileUtils::sharedFileUtils()->getDocumentPath();
//    path.append(file_name);
//    path.append(".csv");
//    CCLOG("Load statistic to %s",path.c_str());
//    std::ifstream myfile (path.c_str());
//    std::string line = "";
//    static std::string ret_str;
//    ret_str.clear();
//    
//    if (myfile.is_open())
//    {
//        while (!myfile.eof() )
//        {
//            getline (myfile,line);
//            ret_str.append(line);
//            ret_str.append("\n");
//        }
//        std::cout << ret_str << std::endl;
//        myfile.close();
//        remove(path.c_str());
//        return ret_str.c_str();
//    };
//    return line.c_str();
//};
//
//void CKStaticFile::save()
//{
//    std::string path = cocos2d::CCFileUtils::sharedFileUtils()->getDocumentPath();
//    path.append(file_name);
//    path.append(".csv");
//    CCLOG("Save statistic to %s",path.c_str());
//    std::ofstream out(path.c_str(), std::ios::app | std::ios::binary);
//    
//    std::cout << player_name << ","<<time_start<<","<<world<<",";
//    std::cout << "lvl_num = "<<lvl_number << "," << diff_number << "," << int(lvl_state) << ",";
//    std::cout << cube_count << "," << "spec_bomb = " << bomb_special << "," << int(time) << "," << base_airplane_height<<",";
//    std::cout << airplane_loop << "," << bomb_complite << "," << "bomb failed = "<<bomb_failed << ",";
//    std::cout << cube_destroy << "," << "bomb base count = " << bomb_base_count << ","  << bomb_count << "," << lvl_score<<std::endl;
//    
//    if (out.is_open() && player_name.size() > 2 && world < 255 && time > 0 && time < 1000)
//    {
//
//      ObjCCalls::sendEventFlurry("lvl_played", false, "world_num=%d,lvl_num=%d,difficulty_num=%d,lvl_state=%d,blocks_big_all=%d,bombs_available=%d,level_time=%d",world,lvl_number,diff_number,int(lvl_state),cube_count,bomb_special,int(time));
//      
//   // ObjCCalls::sendEventFlurry("lvl_played2", false, "plane_flights=%d,damaged_shots=%d,missed_shots=%d,frames_broken=%d,ordinary_shots=%d,special_shots=%d,level_score=%d,bombs_unused=%d",airplane_loop,bomb_complite,bomb_failed,cube_destroy,bomb_base_count,bomb_count,lvl_score,unused_spec_bomb);
//        
//        CCLOG("Player  %s world  %d",player_name.c_str(),world);
//        out << player_name << ","<<time_start<<","<<world<<",";
//        out << lvl_number << "," << diff_number << "," << int(lvl_state) << ",";
//        out << cube_count << "," << bomb_special << "," << int(time) << ","<< base_airplane_height <<",";
//        out << airplane_loop << "," << bomb_complite << "," << bomb_failed << ",";
//        out << cube_destroy << "," << bomb_base_count << "," << bomb_count << "," << lvl_score<<std::endl;
//       
//    };
//    out.close();
//    
//    clear();
//};
//
//
//
//
//
//
//
//
//
//
