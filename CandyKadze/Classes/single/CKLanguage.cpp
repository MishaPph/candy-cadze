//
//  CKLanguage.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 22.04.13.
//
//

#include "CKLanguage.h"
#include "CK.h"
#include <fstream>

using namespace SL;
using namespace cocos2d;

#define LCZ(varType) CKLanguage::Instance().getLocal(varType)

#pragma mark - CKLANGUAGE
static CKLanguage* newCKLanguage = NULL;

cocos2d::CCNode* CKLanguage::callback_object = NULL;
cocos2d::SEL_CallFuncND CKLanguage::callback_func = NULL;

void *CKLanguage::func_thread_lang(void *_d)
{
    bool p = *(bool *)_d;
    CKLanguage::Instance().createFontAtlas(p);
    if(callback_object)
    {
        (callback_object->*callback_func)(0,0);
    }
    pthread_exit(0);
};

CKLanguage::CKLanguage()
{
    dict_en = NULL;
    dict_current = NULL;
};

CKLanguage &CKLanguage::Instance()
{
    if(newCKLanguage == NULL)
    {
        newCKLanguage = new CKLanguage();
    };
    return *newCKLanguage;
};

const char *CKLanguage::getCurrent()const
{
    return current.c_str();
};

const char *CKLanguage::getLocal(const std::string &_key)const
{
    //   CCAssert(dict_en, "Not active language");
    if(dict_current && dict_current->objectForKey(_key))
    {
        return dictStr(dict_current, _key);
    }
    else if(dict_en && dict_en->objectForKey(_key))
    {
        return dictStr(dict_en, _key);
    };
    
    return _key.c_str();
};

std::string CKLanguage::langEqual(const std::string &_language)
{
    cocos2d::CCDictionary *lang_equal = cocos2d::CCDictionary::createWithContentsOfFile("lang_equal.plist");
    std::string tmp_lng = _language;
    if(lang_equal->objectForKey(_language))
    {
        return dictStr(lang_equal, _language);
    };
    return _language;
};

bool CKLanguage::load(const std::string &_language)
{
    CCLOG("CKLanguage::load %s",_language.c_str());
    current = _language;
    if(!dict_en)
    {
        cocos2d::CCDictionary *dict = cocos2d::CCDictionary::createWithContentsOfFile("language_en.plist");
        if(dict)
        {
            
            dict_en = (cocos2d::CCDictionary *)dict->objectForKey("lang");
            dict_en->retain();
            
        }
    };
    if(dict_current)
    {
        CC_SAFE_RELEASE_NULL(dict_current);
    };
    
    std::string tmp_lng = langEqual(_language);
    
    char str[NAME_MAX];
    sprintf(str, "language_%s.plist",tmp_lng.c_str());
    
    cocos2d::CCDictionary *dict = cocos2d::CCDictionary::createWithContentsOfFile(str);
    
    if(dict && dict->objectForKey("lang"))
    {
        dict_current = (cocos2d::CCDictionary *)dict->objectForKey("lang");
        dict_current->retain();
    }
    CCLOG("dict_current %s",_language.c_str());
    if(dict_current)
    {
        current = _language.c_str();
        return true;
    }
    else
    {
        current = "en";
        CCLOG("Error loading language");
        return false;
    }
};

void CKLanguage::readDict(cocos2d::CCDictionary *_param,SL::OneBMFontBuilder *_fnt)
{
    if(_param->objectForKey("Type"))
    {
        std::string type_str = dictStr(_param, "Type");
        if(type_str == "Label")
        {
            cocos2d::CCDictionary *param = (cocos2d::CCDictionary *)_param->objectForKey("Special");
            if(param->objectForKey("Dynamic") && dictInt(param, "Dynamic") == 1)
            {
                
            }
            else
            {
                _fnt->addString(LCZ(dictStr(param, "Text")),int(dictFloat(param, "TextSize") + 0.5f));
                 myfile << "<key>"<< dictStr(param, "Text")<<"_"<<int(dictFloat(param, "TextSize") + 0.5f)<<"</key>"<< "<string>" << dictStr(param, "Text")<<"</string>\n";
            };
        };
    }
    CCDictionary* frameDic = (CCDictionary*)_param->objectForKey("Childs");
    if(frameDic)
    {
        CCDictElement* pElement = NULL;
        CCDICT_FOREACH(frameDic, pElement)
        {
            readDict((CCDictionary*)pElement->getObject(),_fnt);
        };
    };
};

bool CKLanguage::readl(const char * _name,SL::OneBMFontBuilder *_fnt)
{
    CCDictionary *dict = CCDictionary::createWithContentsOfFile(_name);
    CCDictionary* frameDic = (CCDictionary*)dict->objectForKey("frames");
    if(!frameDic)
    {
        return false;
    }
    CCDictElement* pElement = NULL;

    CCDICT_FOREACH(frameDic, pElement)
    {
        //CCLOG("CKBaseNode::load:: dict key %s",pElement->getStrKey());
        readDict((CCDictionary*)pElement->getObject(),_fnt);
    }
    dict->release();
    return true;
};

bool CKLanguage::createFontDigital(bool _replase)
{
    return false;
    char str[NAME_MAX];
    sprintf(str, "%sDigital.png",CCFileUtils::sharedFileUtils()->getWriteablePath().c_str());
    std::ifstream f(str);
    if(!f || _replase)
    {
        m_digital = OneBMFontBuilder::create("Cookies", OneBMFontBuilder::funcGradBorder);
        
//        readl(CK::loadFile("c_mainMenu.plist").c_str(),m_digital);
//        readl(CK::loadFile("c_Upgrade.plist").c_str(),m_digital);
//        readl(CK::loadFile("c_Tutorial.plist").c_str(),m_digital);
//        readl(CK::loadFile("c_Shop.plist").c_str(),m_digital);
//        readl(CK::loadFile("c_Pause.plist").c_str(),m_digital);
//        readl(CK::loadFile("c_LevelFailed.plist").c_str(),m_digital);
//        readl(CK::loadFile("c_LevelComplete.plist").c_str(),m_digital);
//        readl(CK::loadFile("c_Inventory.plist").c_str(),m_digital);
        
        if(CCDirector::sharedDirector()->getWinSize().width == 1024)
        {
            m_digital->addString("0123456789",45);
            m_digital->addString("0123456789",50);
            m_digital->addString("0123456789|x-50%",40);
            m_digital->addString("0123456789|",22);
            m_digital->addString("0123456789",25);
            m_digital->addString("0123456789",60);
            
            m_digital->addString("0123456789",30);
            
            //game scene
            m_digital->addString("0123456789%",62);
            
            //dialog
            char str[NAME_MAX];
            sprintf(str, "Digital");
            m_digital->save(str);
        }
        else
        {

            m_digital->addString("01234567890x",25);
            m_digital->addString("01234567890",30);
            m_digital->addString("01234567890|",20);
            m_digital->addString("0123456789-%|",15);

            char str[NAME_MAX];
            sprintf(str, "Digital");
            m_digital->save(str);
        }
        
        
        CC_SAFE_DELETE(m_digital);
        return true;
    };
    return false;
};

bool CKLanguage::createFontAtlasThread(bool _replase,CCNode *_node,SEL_CallFuncND _func)
{
    callback_object = _node;
    callback_func = _func;
    pthread_cancel(m_bg_thread);
    pthread_create(&m_bg_thread, NULL, CKLanguage::func_thread_lang, &_replase);
    return true; 
};
bool CKLanguage::createFontAtlas(bool _replase)
{
    char str[NAME_MAX];
    sprintf(str, "%sCookies_%s.png",CCFileUtils::sharedFileUtils()->getWriteablePath().c_str(),CKLanguage::Instance().getCurrent());
    std::ifstream f(str);
    if(!f || _replase)
    {
        std::string font = "Boomboom";
        if(strcmp(CKLanguage::Instance().getCurrent(), "en") == 0)
            font = "Cookies";
        
        m_font = OneBMFontBuilder::create(font.c_str(), OneBMFontBuilder::funcGradBorder);
        std::string path = CCFileUtils::sharedFileUtils()->getDocumentPath();
        path.append("language.plist");
        myfile.open(path.c_str(), std::ios::out | std::ios::binary);
        if (myfile.is_open())
        {
            myfile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">";
            myfile << "<plist version=\"1.0\"><dict>";
        }
        readl(CK::loadFile("c_mainMenu.plist").c_str(),m_font);
        readl(CK::loadFile("c_Upgrade.plist").c_str(),m_font);
        readl(CK::loadFile("c_Tutorial.plist").c_str(),m_font);
        readl(CK::loadFile("c_TutorialScript.plist").c_str(),m_font);
        readl(CK::loadFile("c_Shop.plist").c_str(),m_font);
        readl(CK::loadFile("c_Pause.plist").c_str(),m_font);
        readl(CK::loadFile("c_LevelFailed.plist").c_str(),m_font);
        readl(CK::loadFile("c_LevelComplete.plist").c_str(),m_font);
        readl(CK::loadFile("c_LevelCompleteKM.plist").c_str(),m_font);
        readl(CK::loadFile("c_Inventory.plist").c_str(),m_font);
        readl(CK::loadFile("c_InfoMessage.plist").c_str(),m_font);
        readl(CK::loadFile("c_Options.plist").c_str(),m_font);
        readl(CK::loadFile("c_ConfirmationReset.plist").c_str(),m_font);
        readl(CK::loadFile("c_LevelCompleteAddShare.plist").c_str(),m_font);
        
        readl(CK::loadFile("c_Confirmation.plist").c_str(),m_font);
        
        readl(CK::loadFile("c_Leaderboard.plist").c_str(),m_font);
        readl(CK::loadFile("c_LeaderboardTable.plist").c_str(),m_font);

        readl(CK::loadFile("c_ModalWindowTurnOffKM.plist").c_str(),m_font);
        readl(CK::loadFile("c_ModalWindowTurnONKM.plist").c_str(),m_font);
        readl(CK::loadFile("c_ModalWindowFacebook.plist").c_str(),m_font);
        
        readl("c_SocialAttach.plist",m_font);
        readl("c_SocialAttachFinal.plist",m_font);
        
        readl(CK::loadFile("c_ModalWindowUpdate.plist").c_str(),m_font);
        readl(CK::loadFile("c_Info.plist").c_str(),m_font);
        
        
        if (myfile.is_open())
        {
            myfile  << "</dict></plist>";
        }
        myfile.close();
        CCDictionary * dic_word = CCDictionary::createWithContentsOfFile(CK::loadFile("word_list.plist").c_str());
        CCDictionary * d_size = static_cast<CCDictionary *>(dic_word->objectForKey("size"));
        if(d_size)
        {
            CCDictElement *element = NULL;
            CCDICT_FOREACH(d_size, element)
            {
                int size = atoi(element->getStrKey());
                CCArray * m_array = (CCArray *)element->getObject();
                CCObject * object = NULL;
                CCARRAY_FOREACH(m_array, object)
                {
                    CCString *str =  (CCString *)object;
                    m_font->addString(LCZ(str->getCString()),size);
                };
            };
        }
 
        char str[NAME_MAX];
        sprintf(str, "Cookies_%s",CKLanguage::Instance().getCurrent());
        m_font->save(str);
        
        CC_SAFE_DELETE(m_font);
        return true;
    };
    return false;
};
