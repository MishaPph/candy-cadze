//
//  CKBonusMgr.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 10.10.12.
//
//

#include "CKBonusMgr.h"


CKBonusMgr& CKBonusMgr::Instance()
{
    static CKBonusMgr newCKBonusMgr;
    return newCKBonusMgr;
};


CKBonusMgr::~CKBonusMgr()
{
    it = list_bomb.begin();
    while(it != list_bomb.end())
    {
        delete (*it);
        (*it) = NULL;
        it++;
    };
    list_bomb.clear();
};

const std::string& CKBonusMgr::getNameById(const unsigned int &_id)
{
    CCLOG("map_bomb[_id]->name %s",map_bomb[_id]->name.c_str());
    return map_bomb[_id]->name;
};

const std::string& CKBonusMgr::getBonusNameById(const unsigned char &_id)
{
    return map_bomb[_id]->bonus_name;
};

int CKBonusMgr::getIndexById(const unsigned int &_id) const
{
    int n = -1;
    std::list<CKBombInfo*>::const_iterator it = list_bomb.begin();
    while(it != list_bomb.end())
    {
        if((*it)->type ==_id)
            break;
        n++;
        it++;
    };
    CCLOG("getIndexById:: %d",(n/3));
    return (n/3);
};

const std::string& CKBonusMgr::getInventoryNameById(const unsigned char &_id)
{
 //   CCLOG("getInventoryNameById::id %d",int(_id));
    return map_bomb[_id]->inv_name;
};

void CKBonusMgr::printMap()
{
    map_it = map_bomb.begin();
    while(map_it != map_bomb.end())
    {
        CCLOG("printMap:: type %d name %s cost %d",(*map_it).first,(*map_it).second->name.c_str(),(*map_it).second->cost);
        map_it++;
    };
};

const CKBombInfo * CKBonusMgr::getInfoById(const unsigned char &_id)
{
    return map_bomb[_id];
};

unsigned int CKBonusMgr::begin()
{
    it = list_bomb.begin();
    return (*it)->type;
};

unsigned int CKBonusMgr::next()
{
    if(it != list_bomb.end())
    {
        it++;
        return (*it)->type;
    };
//    else
//    {
//        return 0;
//    };
    return 0;
};

void CKBonusMgr::load()
{
    CCDictionary *dict = CCDictionary::createWithContentsOfFile("bonus_struct.plist");
    
    CCArray* frameArray = (CCArray*)dict->objectForKey("bomb");
    CCObject* pObj = NULL;
    map_bomb.clear();
    CCARRAY_FOREACH(frameArray, pObj)
    {
        CCDictionary* info_dict = (CCDictionary*)(pObj);
        CKBombInfo *m_bomb = new CKBombInfo;
        m_bomb->type = dictInt(info_dict, "id");
        m_bomb->name = dictStr(info_dict, "name");
        m_bomb->inv_name = dictStr(info_dict, "inventory");
        
        char bonus_name[NAME_MAX];
        sprintf(bonus_name, "bonus_bomb%d.png",m_bomb->type);
        m_bomb->bonus_name = bonus_name;
        
        if(info_dict->objectForKey("max"))
            m_bomb->max = dictInt(info_dict, "max");
        
        if(info_dict->objectForKey("min"))
            m_bomb->min = dictInt(info_dict, "min");
        
        if(info_dict->objectForKey("pick_points"))
            m_bomb->pick_points = dictInt(info_dict, "pick_points");
        
        if(info_dict->objectForKey("cost"))
            m_bomb->cost = dictInt(info_dict, "cost");
        
        if(info_dict->objectForKey("speed_mult"))
            m_bomb->speed_mult = dictFloat(info_dict, "speed_mult");

        
        if(info_dict->objectForKey("start_action"))
            m_bomb->start_action = dictFloat(info_dict, "start_action");

        
        CCAssert(map_bomb[m_bomb->type] == NULL,"Dublicate bonus bomb id");
        map_bomb[m_bomb->type] = m_bomb;
        list_bomb.push_back(m_bomb);
    };
    //frameArray = (CCArray*)dict->objectForKey("airplane");
    pObj = NULL;
   // map_bomb.clear();
    CCARRAY_FOREACH((CCArray*)dict->objectForKey("airplane"), pObj)
    {
        CCDictionary* info_dict = (CCDictionary*)(pObj);
        CKBombInfo *m_bomb = new CKBombInfo;
        m_bomb->type = dictInt(info_dict, "id");
        m_bomb->name = dictStr(info_dict, "name");
        m_bomb->inv_name = dictStr(info_dict, "inventory");
        
        CCAssert(map_bomb[m_bomb->type] == NULL,"Dublicate bonus bomb id");
        map_bomb[m_bomb->type] = m_bomb;
        
        list_bomb.push_back(m_bomb);
    };
    dict->release();
};