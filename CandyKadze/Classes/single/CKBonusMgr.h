//
//  CKBonusMgr.h
//  CandyKadze
//
//  Created by PR1.Stigol on 10.10.12.
//
//

#ifndef __CandyKadze__CKBonusMgr__
#define __CandyKadze__CKBonusMgr__

#include <iostream>
#include "CK.h"
#include "cocos2d.h"
#include "list.h"

using namespace cocos2d;
class CKBonusMgr;


struct CKBombInfo
{
    unsigned int type;
    std::string name;
    std::string inv_name;
    std::string bonus_name;
    float speed_mult;
    int start_action;
    int max;
    int min;
    unsigned int pick_points;
    unsigned int cost;
    CKBombInfo()
    {
        min = 1;
        max = 1;
        start_action = 0;
        speed_mult = 1.0;
        pick_points = 0;
        cost = 0;
    };
 //   int animation_sprite;
};

class CKBonusMgr
{
    std::list<CKBombInfo*> list_bomb;
    std::list<CKBombInfo*>::iterator it;
    std::map<unsigned int, CKBombInfo*> map_bomb;
    std::map<unsigned int, CKBombInfo*>::iterator map_it;
    bool is_load;
public:
    ~CKBonusMgr();
    void load();
    const std::string& getNameById(const unsigned int &_id);
    const std::string& getInventoryNameById(const unsigned char &_id);
    const CKBombInfo * getInfoById(const unsigned char &_id);
    const std::string& getBonusNameById(const unsigned char &_id);
    int getIndexById(const unsigned int &_id) const;
    unsigned int begin();
    void printMap();
    unsigned int next();
    static CKBonusMgr& Instance();
};
#endif /* defined(__CandyKadze__CKBonusMgr__) */
