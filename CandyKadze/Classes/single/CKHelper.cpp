//
//  CKHelper.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 18.09.12.
//
//

#include "CKHelper.h"
#include "CK.h"

#include "CKBackNode.h"

static CKHelper* mCKHelper = NULL;
CKTutorial* CKHelper::m_tutorial = NULL;

CKHelper::~CKHelper()
{
    CC_SAFE_DELETE(m_tutorial);
    CC_SAFE_DELETE(m_dialog);
    CC_SAFE_DELETE(mCKHelper);
};

CKHelper::CKHelper()
{
    CCLOG("CKHelper::CKHelper");
    panel_position = 0;
    lvl_number = 0;
    lvl_speed = 0;
    TwitterID = "";
    FacebookID = "";
    UserID = "";
    FACEBOOK_USERNAME = "";
    TWITTER_USERNAME = "";
    menu_number = 0;
    FB_need_check_likes_main = false;
    leaderboard_login = false;
    TW_need_ckeck_follow = false;
    show_inventory_km_button = false;

    load_main_menu = true;
    world_number = 0;
    m_dialog = NULL;
    m_shop = NULL;

    show_layer = 0;
    async_load = false;
  //  m_tutorial = NULL;
    start_airplane_speed = 5.1;
    call_unlock_world = 0;
    show_shop_coins = 0;
   // CKFacebookHelper::Instance().FB_CloseSession();
};

CKDialog *CKHelper::getDialog()
{
    if(!m_dialog)
        m_dialog = new CKDialog();
    return m_dialog;
};

void CKHelper::createSocialAttach(const char *_name,int _lvl,const char *_world,int _point,int shots,int crash_blocks,int _flight,int _total)
{
    const int LVL_ID = 2;
    const int WORLD_NAME_ID = 3;
    const int POINT_ID = 5;
    const int SHOTS_ID = 7;
    const int CRASHBLOCK_ID = 9;
    const int FLIGHT_ID = 11;
    const int TOTAL_ID = 13;
    char str[255];
    
    float scale = CCDirector::sharedDirector()->getContentScaleFactor();
    CKBaseNode *m_gui = new CKBaseNode;
    CCNode *node = CCNode::create();
    m_gui->create(node);
    m_gui->load("c_SocialAttach.plist");
    
    sprintf(str, "%d",_lvl);
    static_cast<CCLabelBMFont *>(m_gui->getChildByTag(LVL_ID)->getNode())->setString(str);
    
    sprintf(str, "%s",_world);
    static_cast<CCLabelBMFont *>(m_gui->getChildByTag(WORLD_NAME_ID)->getNode())->setString(str);
    
    sprintf(str, "%d",_point);
    static_cast<CCLabelBMFont *>(m_gui->getChildByTag(POINT_ID)->getNode())->setString(str);
    
    sprintf(str, "%d",shots);
    static_cast<CCLabelBMFont *>(m_gui->getChildByTag(SHOTS_ID)->getNode())->setString(str);

    
    sprintf(str, "%d",crash_blocks);
    static_cast<CCLabelBMFont *>(m_gui->getChildByTag(CRASHBLOCK_ID)->getNode())->setString(str);
  
    sprintf(str, "%d",_flight);
    static_cast<CCLabelBMFont *>(m_gui->getChildByTag(FLIGHT_ID)->getNode())->setString(str);
    
    sprintf(str, "%d",_total);
    static_cast<CCLabelBMFont *>(m_gui->getChildByTag(TOTAL_ID)->getNode())->setString(str);
    
    CCSprite* m_sprite  = CCSprite::create("pictureSocialAttach.png");
    m_sprite->setScale(scale);
    m_sprite->setPosition(ccp(m_sprite->getContentSize().width/2*scale,m_sprite->getContentSize().height/2*scale));
    CCRenderTexture *t_rtt = CCRenderTexture::create(m_sprite->getContentSize().width*scale, m_sprite->getContentSize().height*scale);
    t_rtt->begin();
    m_sprite->visit();
    m_gui->get()->visit();
    t_rtt->end();
    t_rtt->setPosition(ccp(m_sprite->getContentSize().width/2,m_sprite->getContentSize().height/2));
    t_rtt->setScale(1.0/scale);
    CCRenderTexture *t_rtt2 = CCRenderTexture::create(m_sprite->getContentSize().width, m_sprite->getContentSize().height);
    t_rtt2->begin();
    t_rtt->visit();
    t_rtt2->end();
    t_rtt2->saveToFile(_name,kCCImageFormatJPEG);
    
    m_sprite->release();
    t_rtt->release();
    t_rtt2->release();
    CC_SAFE_DELETE(m_gui);
    node->release();
};

void CKHelper::updateLoading()
{
    static_cast<CKLoadingScene *>(loading_scene->getChildByTag(CK::LAYER_LOADING))->update(0.0f);
};

void CKHelper::compliteLoadScene(CCScene* _scene)
{
    static_cast<CKLoadingScene *>(loading_scene->getChildByTag(CK::LAYER_LOADING))->compliteLoadScene(_scene);
};
int CKHelper::getSceneID()const
{
  return static_cast<CKLoadingScene *>(loading_scene->getChildByTag(CK::LAYER_LOADING))->getCurrentSceneID();
};

void CKHelper::playScene(int _scene)
{
    static_cast<CKLoadingScene *>(loading_scene->getChildByTag(CK::LAYER_LOADING))->play(_scene);
};

CKLoadingScene* CKHelper::getLoadingScene()
{
    return static_cast<CKLoadingScene *>(loading_scene->getChildByTag(CK::LAYER_LOADING));
};

bool CKHelper::getIsIpad()
{
    return is_ipad;
};
int CKHelper::getCountVisibleChildren(CCNode *_node)
{
    if(!_node->isVisible())
        return 0;
    CCLOG("_node %d",_node->getZOrder());
    int count = 1;
    CCArray* m_array = _node->getChildren();
    CCObject* t = NULL;
    CCARRAY_FOREACH(m_array, t)
    {
        count += getCountVisibleChildren((CCNode*)t);
    };
    return count;
};

int CKHelper::getSizeAllChilden(CCNode *_node,char *_tap)
{
    if(!_node->isVisible())
        return 0;
    int seq = (_node->isVisible())?_node->getContentSize().width*_node->getContentSize().height*_node->getScaleX()*_node->getScaleY():0;
    CCArray* m_array = _node->getChildren();
    CCObject* t = NULL;

    CCLOG("getSizeAllChilden:%s tag(%d) size = %d (%.1fx%.1f)",_tap,_node->getTag(),seq,_node->getContentSize().width,_node->getContentSize().height);
    char str[255];
    
    sprintf(str, "%s\t",_tap);
    CCARRAY_FOREACH(m_array, t)
    {
        seq += getSizeAllChilden((CCNode*)t,str);
    };

    return seq;
};


void CKHelper::setWinSize(const cocos2d::CCSize &_win_size)
{
    win_size = _win_size;
    is_ipad = (win_size.width == 1024);
    if(!is_ipad)
    {
        BOX_SIZE = CK::BOX_SIZE_IPHONE;
       // static_bg = true;
    }
    else
    {
        BOX_SIZE = CK::BOX_SIZE_IPAD;
    }
};

CKTutorial * CKHelper::getTutorial()
{
    if(!m_tutorial)
        m_tutorial = new CKTutorial;
    return m_tutorial;
};

const cocos2d::CCSize& CKHelper::getWinSize()
{
    return win_size;
};

void CKHelper::addWorld(unsigned char _num,const std::string &_name)
{
    CCLOG("CKHelper::addWorld %d %s",_num,_name.c_str());
    map_world[_num] = _name;
};

void CKHelper::initSI()
{

};

void CKHelper::functAsunc(void * _sender)
{
    count_async--;
    if(count_async == 0)
    {
        initSI();
        async_load = true;
    };
};

const std::string& CKHelper::getWorld(int _num)
{
  if(_num == -1)
  {
      CCLOG("CKHelper::getWorld %d =  %s",world_number,map_world[world_number].c_str());
      return map_world[world_number];
      
  }
    else
    {
        CCLOG("CKHelper::getWorld %d =  %s",_num,map_world[_num].c_str());
      return map_world[_num];
    }
};

int CKHelper::getWorldNumber()
{
    return world_number;
}

void CKHelper::setWorldNumber(int _num)
{
    world_number = _num;
};
int CKHelper::nextWorldNumber()
{
    world_number++;
    return world_number;
};
CKHelper& CKHelper::Instance()
{
    if (!mCKHelper) {
        mCKHelper = new CKHelper();
    };
    return *mCKHelper;
};