//
//  CKLanguage.h
//  CandyKadze
//
//  Created by PR1.Stigol on 22.04.13.
//
//

#ifndef __CandyKadze__CKLanguage__
#define __CandyKadze__CKLanguage__

#include <iostream>
#include "cocos2d.h"
#include "OneBMFontBuilder.h"
#include <fstream>
#include <pthread.h>

class CKLanguage
{
    CKLanguage();
    cocos2d::CCDictionary *dict_en;
    cocos2d::CCDictionary *dict_current;
    std::string current;
    
    
    
    pthread_t m_bg_thread;
    void readDict(cocos2d::CCDictionary *_param,SL::OneBMFontBuilder *_fnt);
    std::ofstream myfile;
    SL::OneBMFontBuilder *m_font,*m_digital;
    
    static cocos2d::CCNode* callback_object;
    static cocos2d::SEL_CallFuncND callback_func;
    static void *func_thread_lang(void *_d);
public:
    std::string langEqual(const std::string &_language);
    void compliteLoad();
    static CKLanguage& Instance();
    bool createFontAtlasThread(bool _replase,cocos2d::CCNode *_node,cocos2d::SEL_CallFuncND _func);
    const char *getCurrent() const ;
    const char *getLocal(const std::string &_key)const;
    bool load(const std::string &_language);
    bool createFontAtlas(bool _replase);
    bool createFontDigital(bool _replase);
    bool readl(const char * _name,SL::OneBMFontBuilder *_fnt);
};

#define LCZ(varType) CKLanguage::Instance().getLocal(varType)

#endif /* defined(__CandyKadze__CKLanguage__) */
