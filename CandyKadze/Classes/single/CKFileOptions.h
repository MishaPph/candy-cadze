//
//  CKFileOptions.h
//  CandyKadze
//
//  Created by PR1.Stigol on 28.11.12.
//
//

#ifndef __CandyKadze__CKFileOptions__
#define __CandyKadze__CKFileOptions__

#include <iostream>
#include "CK.h"
#include <fstream>
#include <dirent.h>
#include <stdio.h>

class CKFileOptions
{
private:
    bool debug_mode;
    bool show_banner;
    bool play_music;
    bool play_effect;
    bool enable_icloud;
    bool show_everyplay;
    bool kids_mode_enabled;
    bool is_first_launch;
    float version_language;
    unsigned char bomb_lvl;
    char language[2];
    CKFileOptions();
public:
    ~CKFileOptions();
    void setDebugMode(bool _debug_mode);
    bool isDebugMode();
    bool isBannerShow();
    void setBannerShow(bool _show = true);
    bool isEveryPlayShow();
    void setEveryPlayShow(bool _show = true);
    
    void enableIcloud(bool _enable);
    bool isEnableIcloud();
    
    bool isPlayMusic();
    bool isPlayEffect();
    void setPlayMusic(bool _play);
    void setPlayEffect(bool _play);
    void setLanguage(const char* _lang,cocos2d::CCNode *_node=NULL,cocos2d::SEL_CallFuncND _func=NULL);
    const char* getLanguage();
    
    void setKidsMode(bool _enable);
    bool isKidsModeEnabled();
    
    bool isFirstLaunch();
    void setFirstLaunch(bool _enab);
    
    unsigned int getBombLvl();
    void loadLanguage();
    void setBombLvl(unsigned char _d);
    void load();
    void save();
    void clearFile();
    static CKFileOptions& Instance();
};

#endif /* defined(__CandyKadze__CKFileOptions__) */
