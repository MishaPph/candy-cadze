//
//  StatisticLog.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 25.07.13.
//
//

#include "StatisticLog.h"
#include "DeviceInfo.h"
#include <string>
#include <stdio.h>
#include <time.h>
#include <ctime>
#include <fstream>
#include "CKFileOptions.h"
#include "CKHelper.h"

using namespace CK;
using namespace cocos2d;

#pragma mark STATISTIC_LOG
static StatisticLog* m_StatisticLog = NULL;
StatisticLog::~StatisticLog()
{
    
};
StatisticLog &StatisticLog::Instance()
{
    if(!m_StatisticLog)
        m_StatisticLog = new StatisticLog;
    return *m_StatisticLog;
};

StatisticLog::StatisticLog()
{
    current_page_id = 0;
    time_in_page = 0;
};

void StatisticLog::createSession()
{
    
    //+++++++++++++++++++++++++++++++++++
    //time info
    time_t tim = time(0);
    struct tm  tstruct;
    char       data_str[80],time_str[80];
    tstruct = * std::localtime(&tim);
    strftime(data_str, sizeof(data_str), "%y/%m/%d", &tstruct);
    strftime(time_str, sizeof(time_str), "%H:%M:%S", &tstruct);
    //+++++++++++++++++++++++++++++++++++
    
    DeviceInfo::Instance().createDirectory();
    
    session_name = cocos2d::CCFileUtils::sharedFileUtils()->getDocumentPath();
    session_name.append("ck.statistic.info/");
    char data_time[80];
    char file_name[255];
    strftime(data_time, sizeof(data_time), "dt%y%m%dtm%H%M%S", &tstruct);
    
    sprintf(file_name, "%s_%s.txt",CKHelper::Instance().UserID.c_str(),data_time);
    
    session_name.append(file_name);

    CCLOG("Save statistic to %s %s",session_name.c_str());
    std::ofstream out(session_name.c_str(), std::ios::app | std::ios::binary);
    out << "{\"ver\": 1, \"date\":  \"" << data_str <<"\", \"time\": \""<<time_str<<"\",\"hash\":\""<<CKHelper::Instance().UserID << "\"}" << std::endl;
    out.close();
    
    //CK::DeviceInfo::Instance().saveJson();
};
void StatisticLog::updateTimer()
{
    time_in_page++;
};

//void StatisticLog::sendOld()
//{
//    ObjCCalls::sendToGameStatistic();
//};

void StatisticLog::setLink(cint _linkid)
{
    std::ofstream out(session_name.c_str(), std::ios::app | std::ios::binary);
    out << "{\"EL\":[\""<< _linkid<<"\",\"" << m_page.id <<"\"," << time_in_page << "," <<  current_page_id << "]}" << std::endl;
    out.close();
};

void StatisticLog::setTryBuy(const char *_id,cint _count)
{
    std::ofstream out(session_name.c_str(), std::ios::app | std::ios::binary);
    out << "{\"TB\":[\""<< _id<<"\"," << _count  << "]}"<< std::endl;
    out.close();
};

void StatisticLog::setBuyinApp(const char *_id,cint _total)
{
    std::ofstream out(session_name.c_str(), std::ios::app | std::ios::binary);
    out << "{\"IA\":[\""<< _id<<"\"," << _total  << "]}"<< std::endl;
    out.close();
};

void StatisticLog::setBuyBomb(cint _id,cint _count,cint _cost,cint _discount)
{
    std::ofstream out(session_name.c_str(), std::ios::app | std::ios::binary);
    out << "{\"BB\":[\""<< _id<<"\"," << _count <<"," << _cost <<"," << _discount << "]}" << std::endl;
    out.close();
};

void StatisticLog::setSpecBomb(cint _world,cint _lvl,cint _count)
{
    std::ofstream out(session_name.c_str(), std::ios::app | std::ios::binary);
    out << "{\"BS\":[\""<< _world<<"\"," << _lvl <<"," << _count << "]}"<< std::endl;
    out.close();
};

void StatisticLog::setLP(cint _world,cint _lvl,cint _speed,cint _state, cint _count_big_cube,cint _spec_bomb_allow,cint _time_game,cint _plane_start,cint _count_loop_plane,cint _bomb_hit,cint _bomb_fail,cint _destroy_cube,cint _bomb_default,cint _bomb_spec,cint _score)
{
//    //+++++++++++++++++++++++++++++++++++
//    //time info
//    time_t tim = time(0);
//    struct tm  tstruct;
//    char       data_str[80],time_str[80];
//    tstruct = * std::localtime(&tim);
//    strftime(data_str, sizeof(data_str), "%y/%m/%d", &tstruct);
//    strftime(time_str, sizeof(time_str), "%H:%M:%S", &tstruct);
//    //+++++++++++++++++++++++++++++++++++
    
    //{"LP":["FJ3","11/23/12","12:24:03","1",1,1,7,21,27,18,5,28,3,28,25,6,312]}
    std::ofstream out(session_name.c_str(), std::ios::app | std::ios::binary);
    out << "{\"LP\":[\"" << _world << "\"," <<  _lvl << "," << _speed <<"," << _state << ","<< _count_big_cube << ","\
    << _spec_bomb_allow << ","<< _time_game << ","<< _plane_start << ","<< _count_loop_plane << ","<< _bomb_hit << ","<< _bomb_fail << ","\
    << _destroy_cube << ","<< _bomb_default << ","<< _bomb_spec << ","<< _score << "]}" << std::endl;
    out.close();
};

void StatisticLog::setNewPage(const int &_id,int _status)
{
    
    cocos2d::CCDirector::sharedDirector()->getScheduler()->unscheduleSelector(schedule_selector(StatisticLog::updateTimer),this);
    cocos2d::CCDirector::sharedDirector()->getScheduler()->scheduleSelector(schedule_selector(StatisticLog::updateTimer), this, 1.0, false);
    
    if(m_page.number != 0)
    {
        std::ofstream out(session_name.c_str(), std::ios::app | std::ios::binary);
        out << "{\"PG\":[\""<< m_page.id <<"\",\"" <<_status << "\"," << time_in_page << "," <<  m_page.number<< "]}" << std::endl;
        out.close();
    };
    time_in_page = 0;
    current_page_id++;
    m_page.number = current_page_id;
    m_page.id = _id + ((CKFileOptions::Instance().isKidsModeEnabled()&& (_id < 16 && _id != CK::Page_Options))?20:0);
};

void StatisticLog::enterBackground()
{
    cocos2d::CCDirector::sharedDirector()->getScheduler()->pauseTarget(this);
    std::ofstream out(session_name.c_str(), std::ios::app | std::ios::binary);
    out << "{\"PG\":[\""<< m_page.id <<"\",\"" << 2 << "\"," << time_in_page << "," <<  m_page.number<< "]}" << std::endl;
    out.close();
};

void StatisticLog::enterForeground()
{
    cocos2d::CCDirector::sharedDirector()->getScheduler()->resumeTarget(this);
};

void StatisticLog::save()
{
    
};

void StatisticLog::showLog()
{
    ObjCCalls::showFile(session_name.c_str());
};


;