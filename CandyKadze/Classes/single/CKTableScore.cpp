//
//  CKTableScore.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 19.06.13.
//
//

#include "CKTableScore.h"
#include "cocos2d.h"
#include <fstream>
#include "CK.h"
#include "CKHelper.h"

static CKTableScoreBase* mCKTableScore = NULL;
static CKTableFacebook* mCKTableFacebook = NULL;
static CKTableTwitter* mCKTableTwitter = NULL;
using namespace cocos2d;


bool compare(CKScoreRecords* first, CKScoreRecords* second)
{
    if (first->fields[first->sort_field] > second->fields[first->sort_field])
        return true;
    else
        return false;
}

bool compare_rnd(CKFriendLvlRecords* first, CKFriendLvlRecords* second)
{
    if (rand()%3 == 0)
        return true;
    else
        return false;
};

CKTableScoreBase::~CKTableScoreBase()
{
    clearScoreRecords();
    clearLvlRecord();
    CC_SAFE_DELETE(mCKTableScore);
};

CKTableScoreBase::CKTableScoreBase()
{
    count_thread = 0;
    callback_object = NULL;
    // loadFriendScore();
};

CKTableScoreBase& CKTableScoreBase::Instance()
{
    if (!mCKTableScore) {
        mCKTableScore = new CKTableScoreBase();
    };
    return *mCKTableScore;
};


#pragma mark - FILE
void CKTableScoreBase::saveFriendProgress(const char *_file_name)
{
    std::string path = cocos2d::CCFileUtils::sharedFileUtils()->getWriteablePath();

    path.append(_file_name);
    CCLOG("saveFriendScore to %s",path.c_str());
    std::ofstream out(path.c_str(), std::ios::out | std::ios::binary);
    
    out<<"<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    out<<"<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">";
    out<<"<plist version=\"1.0\">";
    out<<"<dict>";

    out<<"<key>friends</key><array>";
    world_it = world_lvl.begin();
    while (world_it != world_lvl.end()) {
        out<<"<dict><key>name</key><string>"<< (*world_it)->name <<"</string>"<<"<key>world</key><array>";
        
        for (int i = 0; i < 5; ++i) {
            out<<"<string>"<<(*world_it)->world[i]<<"</string>";
        }
        
        out<<"</array></dict>";
        world_it++;
    };
    out<<"</array>";
    
    out<<"</dict></plist>";
    out.close();
};
//
void CKTableScoreBase::saveScoreRecords(const char *_file_name,int _type)
{
    if(score_list.empty())
        return;
    std::string path = cocos2d::CCFileUtils::sharedFileUtils()->getWriteablePath();
    path.append(_file_name);
    
    CCLOG("saveScoreRecords to %s",path.c_str());
    std::ofstream out(path.c_str(), std::ios::out | std::ios::binary);
    
    out<<"<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
    out<<"<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">";
    out<<"<plist version=\"1.0\">";
    out<<"<dict>";
    
    out<<"<key>score</key><array>";
    it = score_list.begin();
    while (it != score_list.end()) {
        out<<"<dict><key>m_id</key><string>"<< (*it)->m_id <<"</string><key>name</key><string>"<<(*it)->name<<"</string>"<<"<key>fields</key><array>";
        for (int i = 0; i < 12; ++i) {
            out<<"<string>"<<(*it)->fields[i]<<"</string>";
        }
        out<<"</array></dict>";
        it++;
    };
    out<<"</array>";
    
    out<<"<key>me</key><dict>";
    if(_type == 1)
    {
    out<<"<key>m_id</key><string>"<< CKHelper::Instance().FacebookID<<"</string><key>name</key><string>"<<CKHelper::Instance().FACEBOOK_USERNAME<<"</string>";
    }
    else
    {
        out<<"<key>m_id</key><string>"<< CKHelper::Instance().TwitterID<<"</string><key>name</key><string>"<<CKHelper::Instance().TWITTER_USERNAME<<"</string>";
    }
    out<<"</dict>";
    
    out<<"</dict></plist>";
    out.close();
};

void CKTableScoreBase::loadFriendProgress(const char *_file_name)
{
    std::string path = CCFileUtils::sharedFileUtils()->getWriteablePath();
    path.append(_file_name);
    
    CCDictionary * m_dict = CCDictionary::createWithContentsOfFile(path.c_str());
    if(!m_dict)
        return;
    
    if(m_dict->objectForKey("friends"))
    {
        clearLvlRecord();
        CCArray *t_array = (CCArray *)m_dict->objectForKey("friends");
        CCObject * elem = NULL;
        CCARRAY_FOREACH(t_array, elem)
        {
            CCDictionary * record = (CCDictionary *)elem;
            
            CKFriendLvlRecords *m_record = new CKFriendLvlRecords;
            sprintf(m_record->name, "%s",dictStr(record, "name"));
            
            CCArray *field_array = (CCArray *)record->objectForKey("world");
            CCObject * field = NULL;
            int k = 0;
            CCARRAY_FOREACH(field_array, field)
            {
                m_record->world[k] = static_cast<CCString *>(field)->intValue();
                k++;
            };
            
            world_lvl.push_back(m_record);
        };
    };
};
//
void CKTableScoreBase::loadScoreRecords(const char *_file_name,int _type)
{
    std::string path = CCFileUtils::sharedFileUtils()->getWriteablePath();
    path.append(_file_name);
    
    CCDictionary * m_dict = CCDictionary::createWithContentsOfFile(path.c_str());
    if(!m_dict)
        return;
    
    if(m_dict->objectForKey("score"))
    {
        clearScoreRecords();
        CCArray *t_array = (CCArray *)m_dict->objectForKey("score");
        CCObject * elem = NULL;
        CCARRAY_FOREACH(t_array, elem)
        {
            CCDictionary * record = (CCDictionary *)elem;
            
            CKScoreRecords *m_record = new CKScoreRecords;
            sprintf(m_record->m_id, "%s",dictStr(record, "m_id"));
            sprintf(m_record->name, "%s",dictStr(record, "name"));
            
            CCArray *field_array = (CCArray *)record->objectForKey("fields");
            CCObject * field = NULL;
            int k = 0;
            CCARRAY_FOREACH(field_array, field)
            {
                m_record->fields[k] = static_cast<CCString *>(field)->intValue();
                k++;
            };
            score_list.push_back(m_record);
        };
    };
    if(m_dict->objectForKey("me"))
    {
        CCDictionary *dict = (CCDictionary *)m_dict->objectForKey("me");
        if(_type == 1)
        {
            CKHelper::Instance().FacebookID = dictStr(dict, "m_id");
            CKHelper::Instance().FACEBOOK_USERNAME = dictStr(dict, "name");
        }
        else
        {
            CKHelper::Instance().TwitterID = dictStr(dict, "m_id");
            CKHelper::Instance().TWITTER_USERNAME = dictStr(dict, "name");
        }
    }
};

#pragma mark - OPERATION

void CKTableScoreBase::incThread()
{
    count_thread++;
    printf("count active thred %d",count_thread);
};

void CKTableScoreBase::addWaitThread(int _count)
{
    count_thread += _count;
};
const int &CKTableScoreBase::getCountTh()
{
    return count_thread;
}
void CKTableScoreBase::decThread()
{
    count_thread--;
    if(count_thread == 0)
    {
        if(callback_object)
            (callback_object->*m_func)();
    };
    printf("\n count active thread %d\n",count_thread);
};

void CKTableScoreBase::setCallbackFunc(CCObject *_obj,SEL_CallFunc _func)
{
    callback_object = _obj;
    m_func = _func;
};

void CKTableScoreBase::addLvlRecorld(std::string _name,int _world_pixel,int _world_caramel,int _world_choco,int _world_ice,int _world_waffel)
{
    CCLOG("CKTableScore::AddLvlRecorld %s",_name.c_str());
    CKFriendLvlRecords *m_record = new CKFriendLvlRecords;
    sprintf(m_record->name, "%s",_name.c_str());
    m_record->world[0] = _world_pixel;
    m_record->world[1] = _world_caramel;
    m_record->world[2] = _world_choco;
    m_record->world[3] = _world_ice;
    m_record->world[4] = _world_waffel;
    world_lvl.push_back(m_record);
};

void CKTableScoreBase::addRecords(const char *_id,const char *_name,int _total,int _best_hiscore,int _ordinary_using,int _special_using,int _accuracy_rating,int _block_destroy,int _plane_flights,int _world_pixel,int _world_caramel,int _world_choco,int _world_ice,int _world_waffel)
{
    CKScoreRecords *m_record = new CKScoreRecords;
    sprintf(m_record->m_id, "%s",_id);
    sprintf(m_record->name, "%s",_name);
    m_record->fields[0] = _total;
    m_record->fields[1] = _best_hiscore;
    
    m_record->fields[2] = _world_caramel;
    m_record->fields[3] = _world_choco;
    m_record->fields[4] = _world_ice;
    m_record->fields[5] = _world_waffel;
    m_record->fields[6] = _world_pixel;
    
    m_record->fields[7] = _ordinary_using;
    m_record->fields[8] = _special_using;
    m_record->fields[9] = _accuracy_rating;
    m_record->fields[10] = _block_destroy;
    m_record->fields[11] = _plane_flights;
    
    score_list.push_back(m_record);
};

void CKTableScoreBase::addFriend(std::string _id)
{
    CCLOG("CKTableScoreBase::addFriend %s",_id.c_str());
    friends.push_back(_id);
};


void CKTableScoreBase::clearFriends()
{
    friends.clear();
};


bool CKTableScoreBase::friendEmpty()
{
    return friends.empty();
}
bool CKTableScoreBase::scoreEmpty()
{
    return score_list.empty();
};

void CKTableScoreBase::clearLvlRecord()
{
    CCLOG("CKTableScore::clearLvlRecord");
    world_it = world_lvl.begin();
    while (world_it != world_lvl.end()) {
        delete *world_it;
        *world_it = NULL;
        world_it++;
    }
    world_lvl.clear();
};

void CKTableScoreBase::clearScoreRecords()
{
    it = score_list.begin();
    while (it != score_list.end()) {
        delete *it;
        *it = NULL;
        it++;
    }
    score_list.clear();
};


std::list<std::string> *CKTableScoreBase::getFriends()
{
    return &friends;
};

std::list<CKScoreRecords*> CKTableScoreBase::showField(int _num_field)
{
    std::list<CKScoreRecords*> tmp;
    //_write.clear();
    std::list<CKScoreRecords*>::const_iterator it = score_list.begin();
    while (it != score_list.end()) {
        bool p = true;
        if((*it)->fields[_num_field] == 0 || strlen((*it)->name) == 0)
        {
            it++;
            continue;
        };
        
        for (t_it = tmp.begin(); t_it != tmp.end(); t_it++) {//find dublicate
            if(strcmp((*t_it)->m_id, (*it)->m_id) == 0)
            {
                CCLOG("dublicate %s %s ",(*t_it)->m_id, (*it)->m_id);
                //
                p = false;
                if((*t_it)->fields[_num_field] <= (*it)->fields[_num_field])//replace less values
                {
                    tmp.erase(t_it);//remove low field
                    (*it)->sort_field = _num_field;
                    tmp.push_back(*it);
                };
                break;
            }
        }
        if(p)
        {
            //CCLOG("Insert %s",(*it)->name);
            (*it)->sort_field = _num_field;
            tmp.push_back(*it);
        }
        it++;
    };
    tmp.sort(compare);
    t_it = tmp.begin();
    while (t_it != tmp.end()) {//find dublicate
        CCLOG("showField %s %d",(*t_it)->name,(*t_it)->fields[_num_field]);
        t_it++;
    }
    return tmp;
};

std::list<CKScoreRecords*>* CKTableScoreBase::getField(int _num_field)
{
    sorted.clear();
    sorted = showField(_num_field);
    return &sorted;
};

void CKTableScoreBase::printList(std::list<CKScoreRecords*> *_list,int _num_filed)
{
    it = _list->begin();
    while (it != _list->end()) {
        printf("CKTableScore::printList %s %d",(*it)->name,(*it)->fields[_num_filed]);
        it++;
    };
};

std::list<CKFriendLvlRecords*>* CKTableScoreBase::getFriendsOpenLvlinWorld(int _world)
{
  //  printf("CKTableScore::getFBFriendOpenLvlinWorld\n");
    sorted_world.clear();
    world_it = world_lvl.begin();
    while (world_it != world_lvl.end()) {
      //  printf("CKTableScore::getFBFriendOpenLvlinWorld %s %d",(*world_it)->name,(*world_it)->world[_world]);
        if((*world_it)->world[_world] != 0)
        {
            sorted_world.push_back(*world_it);
        };
        world_it++;
    }
//    world_it = sorted_world.begin();
//    while (world_it != sorted_world.end()) {
//        printf("CKTableScore::getFBFriendOpenLvlinWorld %s %d %d/n",(*world_it)->name,_world,(*world_it)->world[_world]);
//        world_it++;
//    };
    sorted_world.sort(compare_rnd);
    return &sorted_world;
};
#pragma  mark - FACEBOOK_SCORE
CKTableFacebook::CKTableFacebook()
{
    // loadFriendScore();
};

CKTableFacebook& CKTableFacebook::Instance()
{
    if (!mCKTableFacebook) {
        mCKTableFacebook = new CKTableFacebook();
    };
    return *mCKTableFacebook;
};
CKTableFacebook::~CKTableFacebook()
{
};
#pragma  mark - TWITTER_SCORE
CKTableTwitter::CKTableTwitter()
{
    CC_SAFE_DELETE(mCKTableFacebook);
};

CKTableTwitter& CKTableTwitter::Instance()
{
    if (!mCKTableTwitter) {
        mCKTableTwitter = new CKTableTwitter();
    };
    return *mCKTableTwitter;
};
CKTableTwitter::~CKTableTwitter()
{
    CC_SAFE_DELETE(mCKTableTwitter);
};