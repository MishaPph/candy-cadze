//
//  CKHelper.h
//  CandyKadze
//
//  Created by PR1.Stigol on 18.09.12.
//
//

#ifndef __CandyKadze__CKHelper__
#define __CandyKadze__CKHelper__

#include <iostream>
#include <cocos2d.h>
#include <list.h>
#include "CKInventory.h"
#include "CKShop.h"
#include "CKTutorial.h"
#include "CKDialog.h"
#include "CKLoading.h"
#include "CKLoadingScene.h"

class CKShop;
class CKInventory;

class CKHelper
{
  //  std::string world_name;
    CKHelper();
    cocos2d::CCSize win_size;
    bool is_ipad;
    std::map<unsigned char, std::string> map_world;
    unsigned char world_number;
    CKDialog *m_dialog;
    CCScene* m_scene;
 //   CKSceneBaseSI* m_layer;
    int count_async;
    bool async_load;
    static CKTutorial *m_tutorial;
    
    void initSI();
public:
    ~CKHelper();
    void functAsunc(void * _sender);
    int panel_position;
    int show_shop_bomb;
    int show_shop_coins;
    int call_unlock_world;
    bool FB_need_check_likes_main;
    //bool FB_login_first_launch;
    bool TW_need_ckeck_follow;
    bool leaderboard_login;
    bool show_inventory_km_button;
    std::string TwitterID;
    std::string UserID;
    std::string FacebookID;
    std::string FACEBOOK_USERNAME;
    std::string TWITTER_USERNAME;
   // bool static_bg;
    int count_hit_for_loop;
    float start_airplane_speed;
  //  CKInventory *m_inventory;
    int show_layer;
    
    CKShop *m_shop;
   // CKLoading * m_loading;
    CKTutorial * getTutorial();
    CKDialog *getDialog();
    int getSizeAllChilden(CCNode *_node,char *_tap = "");
    int getCountVisibleChildren(CCNode *_node);
  //  CKGameSceneNew * game_scene;
    bool getIsIpad();
   // void setIsIpad(bool _b);
    bool load_main_menu;
    
    CCScene* loading_scene;
    CKLoadingScene* getLoadingScene();
    void compliteLoadScene(CCScene* _scene);
    void playScene(int _scene);
    int getSceneID()const;
    void updateLoading();
    const cocos2d::CCSize& getWinSize();
    void setWinSize(const cocos2d::CCSize &_win_size);

    void createSocialAttach(const char *_name,int _lvl,const char *_world,int _point,int shots,int crash_blocks,int _flight,int _total);
   // int count_coll;
    int BOX_SIZE;
    unsigned char lvl_number;
    unsigned char lvl_speed;
    unsigned char menu_number;
    //int coins_doubler;
    int getWorldNumber();
    void setWorldNumber(int _num);
    int nextWorldNumber();
    unsigned char select_world;
    int ground_height;
    int score;
    //void initShopInventory(CCScene* _scene, cocos2d::CCLayer* _layer,int _show = 0);
  //  void removeInventoryShop();
    
    void addWorld(unsigned char _num,const std::string &_name);
    
    const std::string& getWorld(int _num = -1);
    static CKHelper& Instance();
};
#endif /* defined(__CandyKadze__CKHelper__) */
