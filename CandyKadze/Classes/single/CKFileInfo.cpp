;//
//  CKFileInfo.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 26.09.12.
//
//

#include "CKFileInfo.h"
#include <fstream>
#include <dirent.h>
#include <stdio.h>
#include "CKNTPTime.h"
#include "CK.h"
#include "ICloudHelper.h"

#pragma mark - CKFILE_INFO
static CKFileInfo* newCKFileInfo = NULL;

int generateHash(const char * string, size_t len) {
    
    int hash = 0;
    for(size_t i = 0; i < len; ++i)
        hash = 65599 * hash + string[i];
    return hash ^ (hash >> 16);
}
#pragma mark - WORLD_AND_LEVEL
int CKFileInfo::getCurentOpenLevel()
{
    if(curent_world)
        return curent_world->open_lvl;
    
    return 0;
};

void CKFileInfo::setLevel(int _lvl){
    if(curent_world)
    {
        printf("setLevel[%d]",_lvl);
        curent_lvl = &(curent_world->m_level[_lvl]);
        printf("setLevel[%d]  %d ",_lvl,curent_world->m_level[_lvl].time);
    }
};
bool CKFileInfo::isOpenLvl(int _lvl)
{
    if(curent_world)
    {
        return (curent_world->m_level[_lvl].score > 0);
    }
    return false;
}
unsigned int CKFileInfo::getScoreCurrentWorld()
{
    unsigned int sum = 0;
    for(int i = 0; i< CK::COUNT_LEVEL_IN_WORLD; ++i)
        sum += curent_world->m_level[i].score;
    return sum;
};
unsigned int CKFileInfo::getScoreAllWorld()
{
    unsigned int sum = 0;
    std::list<World>::iterator it = m_data.m_world.begin();
    while(it != m_data.m_world.end())
    {
        for(int i = 0; i< CK::COUNT_LEVEL_IN_WORLD; ++i)
            sum += (*it).m_level[i].score;
        it++;
    };
    return sum;
};

int CKFileInfo::getCountOpenLvlInAllWorld()
{
    std::list<World>::iterator it = m_data.m_world.begin();
    int sum = 0;
    while(it != m_data.m_world.end())
    {
        for (int i = 0; i < CK::COUNT_WORLD; ++i)
        {
            sum += (*it).open_lvl;
        }
        it++;
    };
    return sum;
};

void CKFileInfo::addDifficult(int _number)
{
    if(!isOpenDifficult(_number))
    {
        m_data.open_difficult.push_back(_number);
    };
};

bool CKFileInfo::isOpenDifficult(int _num)
{
    std::list<unsigned char>::iterator it = m_data.open_difficult.begin();
    while(it != m_data.open_difficult.end())
    {
        if(_num == *it)
        {
            return true;
        };
        it++;
    };
    return false;
};

bool CKFileInfo::addScore(int _score)
{
    CCLOG("curent_lvl->score %d new score %d",curent_lvl->score,_score);
    bool p = false;
    if(curent_lvl->score < _score)
    {
        p = true;
        curent_lvl->score = _score;
    }
    
    addBigScore(_score,false);
    if(mult_koef > 0)
    {
        addBigScore(_score*(mult_koef - 1),true);
    };
    return p;
};

void CKFileInfo::addBigScore(const int _score,bool _buy_coins)
{
    CCLOG("addTotalScore:: score %d",_score);

    m_score->balance_score += _score;
    
    if(_score > 0)
    {
        m_score->icloud_score += _score;
        if(!_buy_coins)
            m_score->score_total += _score;
    }
};

//void CKScore::addAbsoluteScore(int _value)
//{
//    absolute_score += _value;
//}
//void CKScore::addAbsoluteHiScore(int _value)
//{
//    absolute_hiscore += _value;
//}
//unsigned long CKScore::getAbsoluteScore()
//{
//    return absolute_score;
//};
//
//unsigned long CKScore::getAbsoluteHiScore()
//{
//    return absolute_hiscore;
//};

unsigned int CKFileInfo::getTotalScore()
{
    return m_score->balance_score;
};

Level* CKFileInfo::getLevel(int _number)
{
    return &(curent_world->m_level[_number]);
}

int CKFileInfo::getLevelScore(int _number)
{
  //  CCAssert(curent_lvl, "not null");
    return curent_world->m_level[_number].score;
};

Level* CKFileInfo::getLevel()
{
    //if(curent_lvl)
    CCAssert(curent_lvl, "not null");
    return curent_lvl;
};

bool CKFileInfo::isOpenWorld(const char* _name)
{
    std::list<std::string>::iterator it = m_data.open_world.begin();
    while(it != m_data.open_world.end())
    {
        if(strcmp((*it).c_str(), _name) == 0)
        {
            CCLog("isOpenWorld:: name %s",_name);
            return true;
        };
        it++;
    };
    CCLog("isNotOpenWorld:: name %s",_name);
    return false;
};

void CKFileInfo::addWorld(const std::string &world_name)
{
    if(!isOpenWorld(world_name.c_str()))
    {
        m_data.open_world.push_back(world_name);
        genWorld(world_name);
    };
};

World * CKFileInfo::getCurentWorld()
{
    return curent_world;
};

int CKFileInfo::getLvlOpenWithWorld(const std::string &_name)
{
    std::list<World>::iterator it = m_data.m_world.begin();
    while(it != m_data.m_world.end())
    {
        //   CCLOG("find (%s) it(%s)",_name.c_str(),(*it).name.c_str());
        if(strcmp((*it).name.c_str(), _name.c_str())  == 0)
        {
            curent_world = &(*it);
            //CCLOG("open_lvl with curent world %s %d %d",_name.c_str(),_difficult,(*it).open_lvl);
            return (*it).open_lvl;
            // break;
        };
        it++;
    };
    genWorld(_name);
    return 0;
};
//add world into plist
void CKFileInfo::genWorld(const std::string &world_name)
{
    std::list<World>::iterator it = m_data.m_world.begin();
    while(it != m_data.m_world.end())
    {
        if((*it).name == world_name)
        {
            return;
        };
        it++;
    };
    World _world;
    _world.name = world_name;
    _world.open_lvl = 0;
    
    for(int i = 0; i < CK::COUNT_LEVEL_IN_WORLD; ++i)
    {
        Level _lvl;
        _lvl.time = 0;
        _lvl.score = 0;
        _world.m_level[i] = _lvl;
    };
    
    m_data.m_world.push_back(_world);
    m_data.open_world.push_back(world_name.c_str());
    
    CCLOG("insert new world %ld world(%s)",m_data.m_world.size(),world_name.c_str());
    curent_world = &_world;
};

void CKFileInfo::setCurentOpenLevel(int _lvl)
{
    curent_world->open_lvl = MAX(curent_world->open_lvl,_lvl);
};
bool CKFileInfo::isBlackList()
{
    return (black_list != 0);
}
#pragma mark - CKSCORE
unsigned int CKScore::getCountDestroyCube()
{
    return count_destroy_cube;
}
unsigned int CKScore::getCountLoopPlane()
{
    return count_loop_plane;
}
unsigned int CKScore::getAccuvateShots()
{
    return accuvate_shots;
}
unsigned int CKScore::getCountOrginaryBomb()
{
    return count_orginary_bomb;
}
unsigned int CKScore::getCountSpecialBomb()
{
    return count_special_bomb;
};
unsigned int CKScore::getCountHitBomb()
{
    return count_hit_bomb;
};

void CKScore::addCountDestroyCube(int _value)
{
    count_destroy_cube += _value;
}
void CKScore::addCountLoopPlane(int _value)
{
    count_loop_plane += _value;
}
void CKScore::addAccuvateShots(int _value)
{
    accuvate_shots += _value;
}
void CKScore::addCountOrginaryBomb(int _value)
{
    count_orginary_bomb += _value;
}
void CKScore::addCountSpecialBomb(int _value)
{
    count_special_bomb += _value;
}
void CKScore::addCountHitBomb(int _value)
{
    count_hit_bomb += _value;
};

void CKScore::clear()
{
    score_total = 0;
    icloud_score = 0;
    balance_score = 0;
    count_loop_plane = 0;
    count_destroy_cube = 0;
    accuvate_shots = 0;
    count_orginary_bomb = 0;
    count_special_bomb = 0;
    count_hit_bomb = 0;
};

void CKScore::load(CCDictionary *dict)
{
    if (dict->objectForKey("total_score")) {
        score_total = dictInt(dict, "total_score");
    };
    if (dict->objectForKey("icloud_score")) {
        icloud_score = dictInt(dict, "icloud_score");
    };
    if (dict->objectForKey("balance_score")) {
        balance_score = dictInt(dict, "balance_score");
    };
    
    if (dict->objectForKey("count_loop_plane")) {
        count_loop_plane = dictInt(dict, "count_loop_plane");
    };

    if (dict->objectForKey("count_destroy_cube")) {
        count_destroy_cube = dictInt(dict, "count_destroy_cube");
    }

    if (dict->objectForKey("accuvate_shots")) {
        accuvate_shots = dictInt(dict, "accuvate_shots");
    }

    if (dict->objectForKey("count_orginary_bomb")) {
        count_orginary_bomb = dictInt(dict, "count_orginary_bomb");
    }

    if (dict->objectForKey("count_special_bomb")) {
        count_special_bomb = dictInt(dict, "count_special_bomb");
    }

    if (dict->objectForKey("count_hit_bomb")) {
        count_hit_bomb = dictInt(dict, "count_hit_bomb");
    }
};

#pragma mark -LIKE_FOLLOW
bool CKFileInfo::isLikesOur()
{
    return likes_our;
};

void CKFileInfo::setLikesOur(bool _likes)
{
    likes_our = _likes;
};

bool CKFileInfo::isLikesAny()
{
    return likes_any;
};

void CKFileInfo::setLikesAny(bool _likes)
{
    likes_any = _likes;
};
bool CKFileInfo::isFollowOur()
{
    return follow_our;
};

void CKFileInfo::setFollowOur(bool _follow)
{
    follow_our = _follow;
};

bool CKFileInfo::isFollowAny()
{
    return follow_any;
};

void CKFileInfo::setFollowAny(bool _follow)
{
    follow_any = _follow;
};

bool CKFileInfo::isTellFriend()
{
    return tell_friend;
};

void CKFileInfo::setTellFriend(bool _enable)
{
    tell_friend = _enable;
};

bool CKFileInfo::isGift()
{
    return gift_disable;
};

void CKFileInfo::setGift(bool _en)
{
    gift_disable = _en;
};

bool CKFileInfo::isAppRate()
{
    return app_rate;
};

void CKFileInfo::setAppRate(bool _value)
{
    app_rate = _value;
};

#pragma mark - INVENTORY
int CKFileInfo::isNeedUpgradeBomb(const int &_id)
{
    std::vector<int>::iterator it = open_bonus.begin();
    while(it != open_bonus.end())
    {
        if(int(*it/10) == int(_id/10))//upgrade bonus type
        {
            CCLOG("type %d find type %d",*it,_id);
            if((*it) > _id)
                return ((*it)-_id);
            return 0;
        };
        it++;
    };
    return 0;
};

void CKFileInfo::addOpenBonus(unsigned int _id)
{
    std::vector<int>::iterator it = open_bonus.begin();
    if(_id < 1000)
    {
        while(it != open_bonus.end())
        {
            if(int(*it/10) == int(_id/10))//update bonus type
            {
                CCLOG("addOpenBonus:: %d %d",*it,_id);
                if((*it) < _id)
                    (*it) = _id;
                return;
            };
            it++;
        };
        open_bonus.push_back(_id);
    }
    it = open_bonus_airplane.begin();
    if(_id > 1000)
    {
        while(it != open_bonus_airplane.end())
        {
            if(*it == _id)
            {
                return;
            };
            it++;
        };
        open_bonus_airplane.push_back(_id);  
    }
    return;
};

bool CKFileInfo::isOpenBonus(int _t)
{
    CCLOG("isOpenBonus(%d) open_bonus.size %d",_t,open_bonus.size());
    std::vector<int>::iterator it = open_bonus.begin();
    if(_t < 1000)
    while(it != open_bonus.end())
    {
        if(int(*it/10) == int(_t/10))
        {
            CCLOG("isOpenBonus YES");
            return true;
        };
        it++;
    };
    
    it = open_bonus_airplane.begin();
    if(_t > 1000)
    while(it != open_bonus_airplane.end())
    {
        if(*it == _t)
        {
            CCLOG("isOpenBonus YES");
            return true;
        };
        it++;
    };
    CCLOG("isOpenBonus NO");
    return false;
}


bool CKFileInfo::isNeedShowTutorial(int _num)
{
    std::list<int>::iterator it = show_tutor.begin();
    while(it != show_tutor.end())
    {
      //  CCLOG("isNeedShowTutorial %d _num %d",*it,_num);
        if(*it == _num)
        {
            show_tutor.erase(it);
            return true;
        };
        it++;
    }
    return false;
};
void CKFileInfo::deleteShowTutorial(int _num)
{
    std::list<int>::iterator it = show_tutor.begin();
    while(it != show_tutor.end())
    {
        if(*it == _num)
        {
            show_tutor.erase(it);
            break;
        }
        it++;
    };
};


void CKFileInfo::removeInventorySelectBomb(unsigned char _type,unsigned int _count)
{
    for (int i = 0; i < CK::SIZE_INVENTORY_SELECT; ++i) {
        if(m_data.m_inventory_sel[i].type == _type)
        {
            m_data.m_inventory_sel[i].count -= _count;
            break;
        };
    }
};

bool CKFileInfo::addToInventorySelect(unsigned char _type,unsigned int _count)
{
  //  std::vector<CK::Inventory>::iterator it =  m_data.m_inventory_sel.begin();
    
CCLOG("addToInventorySelect:: _type%d _count %d size %d",_type,_count);
    
    for (int i = 0; i < CK::SIZE_INVENTORY_SELECT; ++i) {// add to slot
        if(m_data.m_inventory_sel[i].type == _type)
        {
            m_data.m_inventory_sel[i].count += _count;
            return true;
        };
    }
    
    for (int i = 0; i < CK::SIZE_INVENTORY_SELECT; ++i) {// insert to free slot
        if(m_data.m_inventory_sel[i].type == 0 || m_data.m_inventory_sel[i].count <= 0)
        {
            m_data.m_inventory_sel[i].count = _count;
            m_data.m_inventory_sel[i].type = _type;
            return true;
        };
    }
    return false;
};

void CKFileInfo::addToInventory(unsigned char _type,unsigned int _count)
{
    std::vector<CK::Inventory>::iterator it =  m_data.m_inventory.begin();
    CCLOG("addToInventory:: type %d count %d",_type,_count);
    while(it !=  m_data.m_inventory.end())
    {
        if((*it).type == _type)
        {
            CCLOG("addToInventory::prev count %d %d ",(*it).count,_count);
            (*it).count += _count;
            return;
        };
        it++;
    };
    m_data.m_inventory.push_back((CK::Inventory){_type,_count});
};

int CKFileInfo::getCountInInventory(unsigned char _type)
{
    std::vector<CK::Inventory>::iterator it =  m_data.m_inventory.begin();
    while(it !=  m_data.m_inventory.end())
    {
        if((*it).type == _type)
        {
            return (*it).count;
            break;
        };
        it++;
    };
    return 0;
};

int CKFileInfo::getCountInInventorySelect(unsigned char _type)
{
    for (int i = 0; i < CK::SIZE_INVENTORY_SELECT; ++i) {
        if(m_data.m_inventory_sel[i].type == _type)
        {
            return m_data.m_inventory_sel[i].count;
        };
    }
    return 0;
};

bool CKFileInfo::isEmptyInvSelect()
{
    for (int i = 0; i < CK::SIZE_INVENTORY_SELECT; ++i) {
        if(m_data.m_inventory_sel[i].type != 0 && m_data.m_inventory_sel[i].count > 0)
        {
            return false;
        };
        CCLOG("isEmptyInvSelect %d %d",m_data.m_inventory_sel[i].type,m_data.m_inventory_sel[i].count);
    }
    return true;
};

bool CKFileInfo::isEmptyInv()
{
    return (m_data.m_inventory.size() == 0);
};

CK::Inventory* CKFileInfo::getInvSelect(int i)
{
    return &(m_data.m_inventory_sel[i]);
}


void CKFileInfo::showInventory()
{
    std::vector<CK::Inventory>::iterator it = m_data.m_inventory.begin();
    while(it != m_data.m_inventory.end())
    {
        CCLOG("m_data.m_inventory type %d count %d",int((*it).type),int((*it).count));
        it++;
    };
};

std::vector<CK::Inventory> CKFileInfo::getInventory()
{
    return m_data.m_inventory;
};

const CK::Inventory &CKFileInfo::getInventorySelect(const int &_num) const 
{
    return m_data.m_inventory_sel[_num];
};

void CKFileInfo::setInventorySelect(int _num_cell, unsigned char _type, int _count)
{
    m_data.m_inventory_sel[_num_cell].type = _type;
    m_data.m_inventory_sel[_num_cell].count = _count;
};
void CKFileInfo::clearInventory()
{
    m_data.m_inventory.clear();
};

void CKFileInfo::clearInvetorySelect()
{
    //    m_data.m_inventory_sel.clear();
    for (int i = 0 ; i < CK::SIZE_INVENTORY_SELECT; ++i) {
        m_data.m_inventory_sel[i].type = 0;
        m_data.m_inventory_sel[i].count = 0;
    }
};

#pragma mark - GLOBAL (save, load, dump)
CKFileInfo::~CKFileInfo()
{
    CC_SAFE_DELETE(m_score);
    CC_SAFE_DELETE(newCKFileInfo);
};

CKFileInfo::CKFileInfo()
{
    curent_world = NULL;
    is_load = false;
    likes_any = false;
    likes_our = false;
    follow_any = false;
    follow_our = false;
    gift_disable = false;
    app_rate = false;
    m_score = new CKScore;
    m_data.m_inventory.reserve(50);
};
CKFileInfo& CKFileInfo::Instance()
{
    if(newCKFileInfo == NULL)
    {
        newCKFileInfo = new CKFileInfo();
    }
    return *newCKFileInfo;
};

void CKFileInfo::setFileName(const char* _filename)
{
    filename = std::string(_filename);
    is_load = false;
};


void CKFileInfo::reload()
{
    is_load = false;
    load();
};

void CKFileInfo::loadWithDict(CCDictionary *_dict ,bool documents, bool _local)
{
    m_data.clear();
    m_data.m_inventory.clear();
    
    version = dictFloat(dict, "version");
   // m_data.total_score = dictInt(dict, "total_score");

    if(dict->objectForKey("current_language"))
    {
        CKFileOptions::Instance().setLanguage(dictStr(dict, "current_language"));
    };
    likes_our = (dictInt(dict, "likes_our") == 1);
    likes_any = (dictInt(dict, "likes_any") == 1);
    
    follow_our = (dict->objectForKey("follow_our"))?(dictInt(dict, "follow_our") == 1):false;
    follow_any = (dict->objectForKey("follow_any"))?(dictInt(dict, "follow_any") == 1):false;
    tell_friend = (dict->objectForKey("tell_friend"))?(dictInt(dict, "tell_friend") == 1):false;
    gift_disable = (dict->objectForKey("gift"))?(dictInt(dict, "gift") == 1):false;
    app_rate = (dict->objectForKey("app_rate"))?(dictInt(dict, "app_rate") == 1):false;
    
    CCLOG("CKFileInfo::loadWithDict %d %d",follow_our,follow_any);
    
    black_list = 0;
    if (dict->objectForKey("black_list")) {
        black_list = dictInt(dict, "black_list");
    };
    
    m_score->clear();
   // m_score->addAbsoluteScore(dictInt(dict, "absolute_score"));
    if(dict->objectForKey("score_record"))
    {
        m_score->load((CCDictionary *)dict->objectForKey("score_record"));
    };
    
    if(dict->objectForKey("social"))
    {
        CCDictionary * soc = (CCDictionary *)dict->objectForKey("social");
        m_social.twID = dictStr(soc, "twID");
        m_social.twName = dictStr(soc, "twName");
        m_social.fbID = dictStr(soc, "fbID");
        m_social.fbName = dictStr(soc, "fbName");
        if(m_social.twName.length() > 1)
        {
            CKHelper::Instance().TwitterID = m_social.twID;
            CKHelper::Instance().TWITTER_USERNAME = m_social.twName;
        }
        if(m_social.fbName.length() > 1)
        {
            CKHelper::Instance().FacebookID = m_social.fbID;
            CKHelper::Instance().FACEBOOK_USERNAME = m_social.fbName;
        };
    };
    
    multi_data = (dict->objectForKey("m_date"))?((CCString *)dict->objectForKey("m_date"))->uintValue():time(0);
    mult_koef = (dict->objectForKey("multi_koef"))?dictInt(dict, "multi_koef"):0;

    hash_data = dictInt(dict, "hash");
    
    CKHelper::Instance().lvl_speed = dictInt(dict, "curent_difficult");
    
    current_world = dictInt(dict, "curent_world");
    CKHelper::Instance().setWorldNumber(current_world);
    
    CCLOG("CKHelper::Instance().world_number %d",CKHelper::Instance().getWorldNumber());
    CCArray* worldArray = (CCArray*)dict->objectForKey("list_world");
    CCObject* pObj = NULL;
    m_data.m_world.clear();
    int index = 0;
    CCARRAY_FOREACH(worldArray, pObj)
    {
        CCDictionary* dict_world = (CCDictionary*)(pObj);
        World _world;
        _world.name  = dictStr(dict_world, "world");

        index++;
        

        _world.open_lvl = dictInt(dict_world, "count_open_lvl");
        
        CCArray* lvlArray = (CCArray*)dict_world->objectForKey("Score");
        CCObject* plvlObj = NULL;
        int i = 0;
        CCARRAY_FOREACH(lvlArray, plvlObj)
        {
            CCDictionary* dict_lvl = (CCDictionary*)(plvlObj);
            Level _lvl;
            _lvl.time = dictInt(dict_lvl, "time");
            _lvl.score = dictInt(dict_lvl, "score");
            _world.m_level[i] = _lvl;
            ++i;
        };
        
        m_data.m_world.push_back(_world);
    };
    
    //load show tutorial
    show_tutor.clear();
    CCArray* openArray_tut = (CCArray*)dict->objectForKey("tutor");
    CCObject* pObj0 = NULL;
    CCARRAY_FOREACH(openArray_tut, pObj0)
    {
        CCString * str = (CCString*)(pObj0);
        show_tutor.push_back(str->intValue());
    };
    
    //load open world
    m_data.open_world.clear();
    CCArray* openArray = (CCArray*)dict->objectForKey("open_world");
    CCObject* pObj2 = NULL;
    CCARRAY_FOREACH(openArray, pObj2)
    {
        CCString * str = (CCString*)(pObj2);
        if(!isOpenWorld(str->getCString()))
            m_data.open_world.push_back(str->getCString());
    };
    
    //load open difficult
    m_data.open_difficult.clear();
    CCArray* openArray_d = (CCArray*)dict->objectForKey("open_difficult");
    CCObject* pObj_d = NULL;
    CCARRAY_FOREACH(openArray_d, pObj_d)
    {
        CCString * str = (CCString*)(pObj_d);
        if(!isOpenDifficult(str->intValue()))
            m_data.open_difficult.push_back(str->intValue());
    };
    
    //load open bonus
    CCArray* openArray_b = (CCArray*)dict->objectForKey("list_bonus");
    CCObject* pObj_b = NULL;
    open_bonus.clear();
    open_bonus_airplane.clear();
    CCARRAY_FOREACH(openArray_b, pObj_b)
    {
        CCString * str = (CCString*)(pObj_b);
        CCLOG("You open bonus id %d",str->intValue());
        addOpenBonus(str->intValue());
    };
    
    //show bonus
    std::vector<int>::iterator it = open_bonus.begin();
    while(it != open_bonus.end())
    {
        CCLOG("bonus bomb = %d",(*it));
        it++;
    };
    it = open_bonus_airplane.begin();
    while(it != open_bonus_airplane.end())
    {
        CCLOG("bonus airplane = %d",(*it));
        it++;
    };
    
    //load list open in inventory bomb
    m_data.m_inventory.clear();
    CCArray* invenArray = (CCArray*)dict->objectForKey("list_inventory");
    CCObject* inObj = NULL;
    CCARRAY_FOREACH(invenArray, inObj)
    {
        CCDictionary* dict_inv = (CCDictionary*)(inObj);
        addToInventory(dictInt(dict_inv, "type"),dictInt(dict_inv, "count"));
    };
    clearInvetorySelect();

    CCArray* invenArray_sel = (CCArray*)dict->objectForKey("list_selected_inventory");
    CCObject* inObjs = NULL;
    int num_sel = 0;
    
    CCARRAY_FOREACH(invenArray_sel, inObjs)
    {
        CCDictionary* dict_inv = (CCDictionary*)(inObjs);
        setInventorySelect(num_sel,dictInt(dict_inv, "type"),dictInt(dict_inv, "count"));
        num_sel++;
    };

    loadShopInfo(dict);
    
    if(!_local)
    {
        std::string str = calcHeshData();
        int hash_tmp = generateHash(str.c_str(), str.length());
        
        genShopBombWithCurentData();
        
        printf("%s\n",str.c_str());
        printf("%s\n",dictStr(dict, "hash_str"));
        CCLOG("file load :: %d %d",hash_tmp,hash_data);
        if(ENABLE_CHECKING_HASH && hash_tmp != hash_data)
        {
            CCLOG("Error incorrect HASH");
            
            std::string path = cocos2d::CCFileUtils::sharedFileUtils()->getDocumentPath();
            path.append("error_hash.txt");
            std::ofstream out(path.c_str(), std::ios::app | std::ios::binary);
            if (out.is_open())
            {
                out << "old_str = " << dictStr(dict, "hash_str") << endl;
                out << "new_str = " << str << endl;
            }
            out.close();
            
            load(true);
            return;
        };
        
        if(documents)
            save();
        CC_SAFE_RELEASE_NULL(dict);
        is_load = true;
        return;
    }
    else
    {
        genShopBombWithCurentData();
        save();
        return;
    };
    CC_SAFE_RELEASE_NULL(dict);
    is_load = true;
};

void CKFileInfo::load(bool _local)
{
 //   if(is_load)
 //       return;
    if(CKFileOptions::Instance().isEnableIcloud())
    {
        ICloudHelper::Instance().loadWithIcloud();
    };
    
    bool documents = true;

    CCLOG("load plist %s",filename.c_str());
    std::string path = CCFileUtils::sharedFileUtils()->getDocumentPath();
    path.append(filename);
    std::ifstream ifile(path.c_str());

    dict = CCDictionary::createWithContentsOfFile(filename.c_str());
    if(!_local)
    {
        if((strcmp(filename.c_str(),"data_debug.plist") != 0 ))
        if (ifile )// The file exists, and is open for input
        {

                CCDictionary *dict1 = CCDictionary::createWithContentsOfFile(path.c_str());
              //  std::ofstream out(path.c_str(), std::ios::in | std::ios::binary);
                dict = CCDictionary::createWithContentsOfFile(filename.c_str());
                if(dict1->objectForKey("version"))
                {
                CCLOG("CKFileInfo::load versio %f %f",dictFloat(dict1, "version"),dictFloat(dict, "version"));
                    if(dictFloat(dict1, "version") == dictFloat(dict, "version"))
                    {
                       // CC_SAFE_RELEASE_NULL(dict1);
                        dict = CCDictionary::createWithContentsOfFile(path.c_str());
                        documents = false;
                    }
                    else
                    {
                        ICloudHelper::Instance().saveFileToIcloud();
                    }
                }
        }
        ifile.close();
    };
    
    loadWithDict(dict,documents,_local);
};

void CKFileInfo::loadShopInfo(CCDictionary *dict)
{
    //----------------------------------------------------------------------------------------
    //load shop info
    if(dict->objectForKey("shop"))
    {
        std::map<int, ShopBomb*>::iterator bomb_it = bomb_info_map.begin();
        while (bomb_it != bomb_info_map.end()) {
            CC_SAFE_DELETE(bomb_it->second);
            bomb_it++;
        };
        bomb_info_map.clear();
        
        CCDictionary* shop_dict = (CCDictionary*)dict->objectForKey("shop");
        if(shop_dict->objectForKey("buy_with_pack"))
            ShopBomb::setBuyPack(dictInt(shop_dict, "buy_with_pack"));

        data_last_shop_gen = dictInt(shop_dict, "data");
        CCArray *shop_array = (CCArray *)shop_dict->objectForKey("ShopBomb");
        CCObject * obj = NULL;
        CCARRAY_FOREACH(shop_array, obj)
        {
            ShopBomb *shop = new ShopBomb;
            CCDictionary *t_dict = (CCDictionary *)obj;
            shop->type = dictInt(t_dict, "type");
            bomb_info_map[shop->type] = shop;
            int i = 0;

            CCArray * arr_free = (CCArray*)t_dict->objectForKey("free");
            CCObject * obj_free = NULL;
            i = 0;
            CCARRAY_FOREACH(arr_free, obj_free)
            {
                shop->free[i] = static_cast<CCString *>(obj_free)->floatValue();
                i++;
            };
            
            CCArray * arr_pack = (CCArray*)t_dict->objectForKey("free_pack");
            CCObject * obj_pack = NULL;
            
            CCARRAY_FOREACH(arr_pack, obj_pack)
            {
                shop->free_pack.push_back(static_cast<CCString *>(obj_pack)->intValue());
            };
            
            arr_pack = (CCArray*)t_dict->objectForKey("sold_pack");
            obj_pack = NULL;
            
            CCARRAY_FOREACH(arr_pack, obj_pack)
            {
                shop->sold_pack.push_back(static_cast<CCString *>(obj_pack)->intValue());
            };
            
        };
    };
    //-----------------------------------------------------------------------------------------
};

void CKFileInfo::setMultiBuyData(long _time)
{
    multi_data = _time;
};

long CKFileInfo::getMultiBuyData()const
{
    return multi_data;
};

void CKFileInfo::setMultiBuyKoef(const int &_value)
{
    mult_koef = _value;
};

int CKFileInfo::getMultiBuyKoef() const
{
    return mult_koef;
};

const std::string CKFileInfo::getCurentData()
{
    time_t tim = time(0);
    struct tm  tstruct;
    
    char       buf[80];
    tstruct = * std::localtime(&tim);
    std::string time_ntp = "";
    if(ObjCCalls::isActiveConnection())
    {
        time_ntp = CKNTPTime::ntpdate();
      //  CCLOG("ObjCCalls::getNTP %s",time_ntp.c_str());
    };
    
    CCLOG("ObjCCalls::getNTP %s",time_ntp.c_str());
    strftime(buf, sizeof(buf), "%y%m%d", &tstruct); //local time
    CCLOG("genShopBombWithCurentData local time :: %s",buf);
    
    if(ObjCCalls::isActiveConnection() && time_ntp.length() > 3)
    {
        strcpy(buf, time_ntp.c_str()); // get internet ntp server datatime
    };
    
    //cheking current file data saved
    //close if current date less than the previous saved datatime
    if(atoi(buf) < data_last_shop_gen && (ShopBomb::getBuyPack() != 0))
    {
        CCLOG("Error:: cheat buy_pack %d last data %d ",ShopBomb::getBuyPack(),data_last_shop_gen);
        black_list = 1;
        return "";
    };
    if(data_last_shop_gen == atoi(buf)) //we have current discount this day
    {
        return "";
    }
    ShopBomb::setBuyPack(0);
    data_last_shop_gen = atoi(buf);
    strftime(buf, sizeof(buf), "%y%m%d", &tstruct);
    
    if(ObjCCalls::isActiveConnection() && time_ntp.length() > 3)
    {
        strcpy(buf, time_ntp.substr(2, 4).c_str()); // get internet ntp server datatime
    }
    else
    {
        strftime(buf, sizeof(buf), "%m%d", &tstruct); //local time
    };
    
    return std::string(buf);
};

void CKFileInfo::genDiscountBombAllPeriodAndSave()
{
    return;
    int month[12] = {31,28,31,30,31,30,31,31,30,31,30,31};
    
   // int day;
    std::string path = CCFileUtils::sharedFileUtils()->getDocumentPath();
    path.append("discount.csv");
    std::ofstream out(path.c_str(), std::ios::out | std::ios::binary);
    char buf[255];
    for (int index = 0; index < 12; ++index) {
        for(int day = 0; day < month[index];day++)
        {
        // CCLOG("genDiscountBombAllPeriodAndSave %d %d",month[index],index);
            
            sprintf(buf, "%.2d%.2d",index+1,day+1);
            CCLOG("genDiscountBombAllPeriodAndSave %s",buf);

            //clear old
            std::map<int, ShopBomb*>::iterator bomb_it = bomb_info_map.begin();
            while (bomb_it != bomb_info_map.end()) {
                CC_SAFE_DELETE(bomb_it->second);
                bomb_it++;
            };
            bomb_info_map.clear();
            
            int R1 = buf[0] + buf[1] + buf[2] + buf[3] - 0x30*4;
            int R2 = buf[2] + buf[3] - 0x30*2; // convert char to int numer [char('3'/0x33/) - char('0'/0x30/) = int(3)]
            
            int R3 = (buf[0]- 0x30) + (buf[2]- 0x30) - (buf[1]- 0x30) - (buf[3] - 0x30);
            if(R1%2 != 0)
            {
                R1 = 20 - R1;
            };
            if(R2%2 == 0)
            {
                R2 = abs(12 - R2);
            };
            R3 = abs(R3);
            if(R3%2 != 0)
            {
                R3 = 19 - R3;
            };
            
            int R4 = R1*R2;
            int R5 = R1*R3;
            int R6 = R2*R3;
            
            //6 variants generated off of the current date
            initBombDiscount(R1,R2,R3);
            initBombDiscount(R2,R3,R4);
            initBombDiscount(R3,R4,R5);
            initBombDiscount(R4,R5,R6);
            initBombDiscount(R5,R6,R1);
            initBombDiscount(R6,R1,R2);
            
            ShopBomb *type_bomb = NULL;
            int count_bomb = 0;
            int max_cost = 0;
            bomb_it = bomb_info_map.begin();
            type_bomb = bomb_it->second;
            while (bomb_it != bomb_info_map.end()) {
                if(bomb_it->second->haveDiscount()) //dicount if(bomb_it->second->haveFreePack()) // free
                {
                    int max_bomb = CKBonusMgr::Instance().getInfoById(bomb_it->second->type)->cost;
                    if(max_bomb > max_cost)
                    {
                        max_cost = max_bomb;
                        type_bomb = bomb_it->second;
                    };
                }
                bomb_it++;
            };
            float tmp_discount = 0.0f;
            CCLOG("size %d",bomb_info_map.size());
            //if(!type_bomb)
            //    continue;
            
            int type_bom = int(type_bomb->type);
//            std::vector<int>::iterator tmp_it = type_bomb->free_pack.begin();
//            while (tmp_it != type_bomb->free_pack.end()) {
//                if(count_bomb < *tmp_it)
//                    count_bomb = *tmp_it;
//                tmp_it++;
//            };
//            tmp_discount = 1.0;
            
            for (int j = 0; j < 4; j++) {
               if( type_bomb->free[j] != 0)
               {
                   tmp_discount = type_bomb->free[j];
                   switch (j) {
                       case 0:
                           count_bomb = 1;
                           break;
                       case 1:
                           count_bomb = 5;
                           break;
                       case 2:
                           count_bomb = 15;
                           break;
                       case 3:
                           count_bomb = 50;
                           break;
                       default:
                           break;
                   }
                  // break;
               }
            }// for array discount
            
            sprintf(buf, "%d.%.2d.2013",day+1,index+1);
            out << buf <<";" << type_bom << ";" << count_bomb << ";" << tmp_discount << ";" << (max_cost * count_bomb)<<"\n";
        }
    }
    out.close();
};

void CKFileInfo::genShopBombWithCurentData()
{
   
    const char *buf = getCurentData().c_str();
    CCLOG("genShopBombWithCurentData %s ",buf);
    if(strlen(buf) < 2)
    {
        CCLOG("Error:: getCurentData ");
        return;
    }
    int R1 = buf[0] + buf[1] + buf[2] + buf[3] - 0x30*4;
    
    //clear old
    std::map<int, ShopBomb*>::iterator bomb_it = bomb_info_map.begin();
    while (bomb_it != bomb_info_map.end()) {
        CC_SAFE_DELETE(bomb_it->second);
        bomb_it++;
    };
    bomb_info_map.clear();
    
    int R2 = buf[2] + buf[3] - 0x30*2; // convert char to int numer [char('3'/0x33/) - char('0'/0x30/) = int(3)]
    
    int R3 = (buf[0]- 0x30) + (buf[2]- 0x30) - (buf[1]- 0x30) - (buf[3] - 0x30);
    if(R1%2 != 0)
    {
        R1 = 20 - R1;
    };
    if(R2%2 == 0)
    {
        R2 = abs(12 - R2);
    };
    R3 = abs(R3);
    if(R3%2 != 0)
    {
        R3 = 19 - R3;
    };
    
    int R4 = R1*R2;
    int R5 = R1*R3;
    int R6 = R2*R3;
    
    //6 variants generated off of the current date
    initBombDiscount(R1,R2,R3);
    initBombDiscount(R2,R3,R4);
    initBombDiscount(R3,R4,R5);
    initBombDiscount(R4,R5,R6);
    initBombDiscount(R5,R6,R1);
    initBombDiscount(R6,R1,R2);
//    save();
    
};

void CKFileInfo::initBombDiscount(int s1,int s2,int s3)
{
    int mas_bombs[13] = {20,30,40,50,60,70,80,110,120,130,140,150,160};
    int type_bomb = mas_bombs[s1%13] + s3%3 + 1;

   // CCLOG("time:: str %s sum %d n %d r %d type_bomb %d",buf,sum,n,r,type_bomb);
   // CCLOG("CKFileInfo::initBonus S1 %d S3 %d type %d",s1,s3,type_bomb);
    ShopBomb *tmp = new ShopBomb;
    tmp->type = type_bomb;
    for(int i = 0;i < COUNT_BUY_BUTTON;i++)
    {
        tmp->free[i] = 0.0;
    };
    switch (s2%10) {
        case 0:
            tmp->free_pack.push_back(1);
            break;
        case 1:
            tmp->free_pack.push_back(5);
            break;
        case 2:
            tmp->free[1] = 0.05;
            break;
        case 3:
            tmp->free[1] = 0.1;
            break;
        case 4:
            tmp->free[1] = 0.15;
            break;
        case 5:
            tmp->free[2] = 0.05;
            break;
        case 6:
            tmp->free[2] = 0.1;
            break;
        case 7:
            tmp->free[2] = 0.15;
            break;
        case 8:
            tmp->free[3] = 0.05;
            break;
        case 9:
            tmp->free[3] = 0.10;
            break;
        default:
            break;
    };
    bomb_info_map[type_bomb] = tmp;
};

void CKFileInfo::dump()
{
    printf("<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">");
    printf("<plist version=\"1.0\"><dict>");
    
   // printf("<key>total_score</key><integer>%ld</integer>",m_data.total_score);
   // printf("<key>last_difficult</key><integer>%d</integer>",int(m_data.last_difficult));
    printf("<key>list_world</key><array>\n");
    std::list<World>::iterator it = m_data.m_world.begin();
    while(it != m_data.m_world.end())
    {
        printf("<dict>");
        World _world = *it;
      //  printf("<key>world</key><string>%s</string><key>open_lvl</key><integer>%d</integer><key>last_speed</key><integer>%d</integer>\n",_world.name.c_str(),int(_world.open_lvl),int(_world.last_speed));
        printf("<key>lvl</key><array>\n");
        // std::list<Level>::iterator it_l = _world.m_level.begin();
        for(int i = 0; i < CK::COUNT_LEVEL_IN_WORLD; ++i)
        {
            printf("<dict><key>time</key><integer>%d</integer><key>score</key><integer>%d</integer></dict>\n",_world.m_level[i].time,_world.m_level[i].score);
        }
        printf("</array></dict>");
        it++;
    };
    printf("</array>");
    printf("</dict></plist>");
};

void CKFileInfo::saveTmp(const char *_message)
{
    std::string path = CCFileUtils::sharedFileUtils()->getDocumentPath();
    path.append("~tmp");
    std::ofstream out(path.c_str(), std::ios::app | std::ios::binary);
    if (out.is_open())
    {
        out << _message;
    }
    out.close();
};

std::string CKFileInfo::loadTmp()
{
    std::string path = CCFileUtils::sharedFileUtils()->getDocumentPath();
    path.append("~tmp");
    std::ifstream myfile (path.c_str());
    std::string line = "";
    if (myfile.is_open())
    {
        while (! myfile.eof() )
        {
            getline (myfile,line);

            std::cout << line << std::endl;
        }
        
        //close the stream:
        myfile.close();
        remove(path.c_str());
    };
    return line;
};

std::string CKFileInfo::calcHeshData()
{
    std::string str = "";
    char st[NAME_MAX];
    sprintf(st, "%.2f%ld%ld%d%d%d%d%d%d%d%d",version,m_score->score_total,m_score->balance_score,int(likes_our),int(likes_any),int(follow_our),int(follow_any),int(tell_friend),int(gift_disable),int(app_rate),black_list);
    str.append(st);
    
    sprintf(st, "%d%d%d%d%d%d",m_score->getCountLoopPlane(),m_score->getCountDestroyCube(),m_score->getAccuvateShots(),m_score->getCountOrginaryBomb(),m_score->getCountSpecialBomb(),m_score->getCountHitBomb());
    CCLOG("calcHeshData %s",st);
    str.append(st);
    
    std::list<unsigned char>::iterator it_d = m_data.open_difficult.begin();
    while(it_d != m_data.open_difficult.end())
    {
        sprintf(st,"%d",int(*it_d));
        str.append(st);
        it_d++;
    };
    
    std::list<World>::iterator it = m_data.m_world.begin();
    while(it != m_data.m_world.end())
    {
        World _world = *it;
        sprintf(st, "%d",int(_world.open_lvl));
        str.append(st);
        for(int i = 0; i < CK::COUNT_LEVEL_IN_WORLD; ++i)
        {
            sprintf(st, "%d%d",int(_world.m_level[i].time),int(_world.m_level[i].score));
            str.append(st);
        }
        it++;
    };

    //bonus open
    std::vector<int>::iterator it_bonus = open_bonus.begin();
    while(it_bonus != open_bonus.end())
    {
        sprintf(st, "%d",int(*it_bonus));
        str.append(st);
        it_bonus++;
    };
    //airplane bonus open
    it_bonus = open_bonus_airplane.begin();
    while(it_bonus != open_bonus_airplane.end())
    {
        sprintf(st, "%d",int(*it_bonus));
        str.append(st);
        it_bonus++;
    };

    std::vector<CK::Inventory>::iterator it_v = m_data.m_inventory.begin();
    it_v = m_data.m_inventory.begin();
    while(it_v != m_data.m_inventory.end())
    {
        sprintf(st, "%d%d",int((*it_v).type),(*it_v).count);
        str.append(st);
        it_v++;
    };
    
    for (int i = 0 ; i < CK::SIZE_INVENTORY_SELECT; ++i) {
        if(m_data.m_inventory_sel[i].count > 0)
        {
            sprintf(st, "%d%d",int(m_data.m_inventory_sel[i].type),m_data.m_inventory_sel[i].count);
            str.append(st);
        }
    };

    //Save Shop bomb
    sprintf(st, "%d%d",data_last_shop_gen,ShopBomb::getBuyPack());
    str.append(st);
    std::map<int, ShopBomb*>::iterator bomb_it = bomb_info_map.begin();
    while (bomb_it != bomb_info_map.end()) {

        sprintf(st, "%d",int(bomb_it->second->type));
        str.append(st);


        for (int i = 0; i < COUNT_BUY_BUTTON; ++i) {
            sprintf(st, "%.2f",bomb_it->second->free[i]);
            str.append(st);
        };

        std::vector<int>::iterator it = bomb_it->second->free_pack.begin();
        while (it != bomb_it->second->free_pack.end()) {
            sprintf(st, "%d",int(*it));
            str.append(st);
            it++;
        }
        bomb_it++;
    };
    str.append("end");
    return str;
};


void CKFileInfo::save()
{
    genShopBombWithCurentData();
    CCLOG("CKFileInfo::save save data %s",filename.c_str());

    std::string str = calcHeshData();
    int hash = generateHash(str.c_str(),str.length());
    
    std::string path = CCFileUtils::sharedFileUtils()->getDocumentPath();
    path.append(filename);
    std::ofstream out(path.c_str(), std::ios::out | std::ios::binary);
    if (out.is_open())
    {
        out << "<?xml version=\"1.0\" encoding=\"UTF-8\"?><!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">";
        out << "<plist version=\"1.0\"><dict>";
        out << "<key>version</key><string>"<< version <<"</string>";
        //out << "<key>total_score</key><string>"<< m_data.total_score <<"</string>";
        //out << "<key>absolute_score</key><string>"<< m_score->getAbsoluteScore() <<"</string>";
        
        out << "<key>social</key><dict>";
        out << "<key>twID</key><string>"<< CKHelper::Instance().TwitterID <<"</string>";
        out << "<key>twName</key><string>"<< CKHelper::Instance().TWITTER_USERNAME <<"</string>";
        out << "<key>fbID</key><string>"<< CKHelper::Instance().FacebookID <<"</string>";
        out << "<key>fbName</key><string>"<< CKHelper::Instance().FACEBOOK_USERNAME <<"</string>";
        out <<"</dict>";
        
        out << "<key>score_record</key><dict>";
        out << "<key>total_score</key><string>"<< m_score->score_total <<"</string>";
        out << "<key>icloud_score</key><string>"<< m_score->icloud_score <<"</string>";
        out << "<key>balance_score</key><string>"<< m_score->balance_score <<"</string>";
        out << "<key>count_loop_plane</key><string>"<< m_score->getCountLoopPlane() <<"</string>";
        out << "<key>count_destroy_cube</key><string>"<< m_score->getCountDestroyCube() <<"</string>";
        out << "<key>accuvate_shots</key><string>"<< m_score->getAccuvateShots() <<"</string>";
        out << "<key>count_orginary_bomb</key><string>"<< m_score->getCountOrginaryBomb() <<"</string>";
        out << "<key>count_special_bomb</key><string>"<< m_score->getCountSpecialBomb() <<"</string>";
        out << "<key>count_hit_bomb</key><string>"<< m_score->getCountHitBomb() <<"</string>";
        out << "</dict>";
        
        out << "<key>current_language</key><string>"<< CKFileOptions::Instance().getLanguage() <<"</string>";
        out << "<key>hash</key><string>"<< hash <<"</string>";
        out << "<key>hash_str</key><string>"<< str <<"</string>";
        out << "<key>black_list</key><string>"<< black_list <<"</string>";
        out << "<key>curent_world</key><integer>"<<int(CKHelper::Instance().getWorldNumber())<<"</integer>";
        out << "<key>curent_difficult</key><integer>"<<int(CKHelper::Instance().lvl_speed)<<"</integer>";
        //social flag
        out << "<key>likes_our</key><string>"<<int(likes_our)<<"</string>";
        out << "<key>likes_any</key><string>"<<int(likes_any)<<"</string>";
        out << "<key>follow_our</key><string>"<<int(follow_our)<<"</string>";
        out << "<key>follow_any</key><string>"<<int(follow_any)<<"</string>";
        out << "<key>tell_friend</key><string>"<<int(tell_friend)<<"</string>";
        out << "<key>gift</key><string>"<<int(gift_disable)<<"</string>";
        out << "<key>app_rate</key><string>"<<int(app_rate)<<"</string>";
        
        out << "<key>m_date</key><string>"<<multi_data<<"</string>";
        out << "<key>multi_koef</key><string>"<<mult_koef<<"</string>";
        
        
        out << "<key>open_difficult</key><array>";
        std::list<unsigned char>::iterator it_d = m_data.open_difficult.begin();
        while(it_d != m_data.open_difficult.end())
        {
            out << "<integer>" << int(*it_d) << "</integer>";
            it_d++;
        };
        out << "</array>";
        
        out << "<key>open_world</key><array>";
        std::list<std::string>::iterator it_s = m_data.open_world.begin();
        while(it_s != m_data.open_world.end())
        {
            out << "<string>" << *it_s << "</string>";
            it_s++;
        };
        out << "</array>";
        
        out << "<key>list_world</key><array>\n";
        std::list<World>::iterator it = m_data.m_world.begin();
        while(it != m_data.m_world.end())
        {
            out << "<dict>" ;
            World _world = *it;
            out << "<key>world</key><string>"<<_world.name<<"</string>";
            //out <<"<key>last_speed</key><integer>"<<int(_world.last_speed)<< "</integer>";
            
            out << "<key>count_open_lvl</key><string>"<<int(_world.open_lvl) <<"</string>";;
            out << "<key>Score</key><array>";
            for(int i = 0; i < CK::COUNT_LEVEL_IN_WORLD; ++i)
            {
                out << "<dict><key>time</key><integer>"<<_world.m_level[i].time<<"</integer><key>score</key><integer>"<<_world.m_level[i].score<<"</integer></dict>";
            }
            out <<"</array>";
            out<<"</dict>";
            it++;
        };
        out << "</array>";
        
        //tutorial show
        out << "<key>tutor</key><array>\n";
        std::list<int>::iterator it_tut = show_tutor.begin();
        while(it_tut != show_tutor.end())
        {
            out << "<string>"<< int(*it_tut) <<"</string>";
            it_tut++;
        };
         out << "</array>";
        
        //bonus open
        out << "<key>list_bonus</key><array>\n";
        std::vector<int>::iterator it_bonus = open_bonus.begin();
        while(it_bonus != open_bonus.end())
        {
            out << "<string>"<< int(*it_bonus) <<"</string>";
            it_bonus++;
        };
        //airplane bonus open
        it_bonus = open_bonus_airplane.begin();
        while(it_bonus != open_bonus_airplane.end())
        {
            out << "<string>"<< int(*it_bonus) <<"</string>";
            it_bonus++;
        };
        out << "</array>";
        
        out << "<key>list_inventory</key><array>\n";
        std::vector<CK::Inventory>::iterator it_v = m_data.m_inventory.begin();
        CCLOG("save m_inventory:size %d",m_data.m_inventory.size());
        while(it_v != m_data.m_inventory.end())
        {
            CCLOG("m_inventory: type(%d)",int((*it_v).type));
            it_v++;
        };
        it_v = m_data.m_inventory.begin();
        while(it_v != m_data.m_inventory.end())
        {
            out << "<dict>";
            out << "<key>type</key><integer>"<< int((*it_v).type) <<"</integer><key>count</key><integer>"<<(*it_v).count<< "</integer></dict>";
            it_v++;
        };
        out << "</array>";
        
        out << "<key>list_selected_inventory</key><array>\n";
        
        for (int i = 0 ; i < CK::SIZE_INVENTORY_SELECT; ++i) {
            
            if(m_data.m_inventory_sel[i].count > 0)
            {
                out << "<dict>" ;
                out << "<key>type</key><integer>"<< int(m_data.m_inventory_sel[i].type) <<"</integer><key>count</key><integer>"<<m_data.m_inventory_sel[i].count<< "</integer></dict>";
                CCLOG("save type %d count %d",int(m_data.m_inventory_sel[i].type),m_data.m_inventory_sel[i].count);
            }
        }
        out << "</array>";
//Save Shop bomb
        out << "<key>shop</key><dict>";
        out << "<key>data</key><string>"<<data_last_shop_gen<<"</string>";
        out << "<key>buy_with_pack</key><string>"<< ShopBomb::getBuyPack()<<"</string>";
        out << "<key>ShopBomb</key><array>";
        std::map<int, ShopBomb*>::iterator bomb_it = bomb_info_map.begin();
        while (bomb_it != bomb_info_map.end()) {
            if(!(*bomb_it).second->haveDiscount() && !(*bomb_it).second->haveFreePack() && !(*bomb_it).second->haveSoldPack())
            {
                bomb_it++;
                continue;
            };
            out << "<dict>";
            out << "<key>type</key><string>"<<int(bomb_it->second->type)<<"</string>";

            
            out <<"<key>free</key><array>";
            for (int i = 0; i < COUNT_BUY_BUTTON; ++i) {
                out << "<string>"<< bomb_it->second->free[i]<<"</string>";
            };
            out << "</array>";
            
            out <<"<key>free_pack</key><array>";
            std::vector<int>::iterator it = bomb_it->second->free_pack.begin();
            while (it != bomb_it->second->free_pack.end()) {
                out << "<string>"<<int(*it)<<"</string>";
                it++;
            }
            out << "</array>";
            
            out <<"<key>sold_pack</key><array>";
            it = bomb_it->second->sold_pack.begin();
            while (it != bomb_it->second->sold_pack.end()) {
                out << "<string>"<<int(*it)<<"</string>";
                it++;
            }
            out << "</array>";
            
            out << "</dict>";
            
            bomb_it++;
            
        };
        out << "</array>";
        out << "</dict>";
    
//------------------------------------------------------
        out << "</dict></plist>";
        out.close();
    };

    if(CKFileOptions::Instance().isEnableIcloud())
    {
        if(ICloudHelper::Instance().iCloudBetter())
        {
            ICloudHelper::Instance().replaceDataFileWithIcloud();
        }
        else
        {
            ICloudHelper::Instance().saveFileToIcloud();
        }
    }
};

void CKFileInfo::loadBin(const char *_filename)
{
//    std::string path = CCFileUtils::sharedFileUtils()->getDocumentPath();
//    path.append(_filename);
//    std::ifstream in(path.c_str(), std::ios::in | std::ios::binary);
//    if (in.is_open())
//    {
//        in.read((char* )&m_data.total_score, sizeof(int));
//        //in.read((char* )&m_data.last_difficult, sizeof(int));
//        std::list<World>::iterator it = m_data.m_world.begin();
//        while(it != m_data.m_world.end())
//        {
//            //out << "<dict>" ;
//            World _world = *it;
//            char tmp[255];
//            in.read(tmp, sizeof(255));
//            _world.name = tmp;
//            in.read((char *)&_world.open_lvl, sizeof(int));
//          //  in.read((char *)&_world.last_speed, sizeof(int));
//            
//            for(int i = 0; i < CK::COUNT_LEVEL_IN_WORLD; ++i)
//            {
////                in.read((char *)&_world.m_level[0][i].time, sizeof(int));
////                in.read((char *)&_world.m_level[0][i].score, sizeof(int));
//            }
//            
//            /*      std::list<Level>::iterator it_l = _world.m_level.begin();
//             while(it_l != _world.m_level.end())
//             {
//             Level _lvl = *it_l;
//             in.read((char *)&_lvl.time, sizeof(int));
//             in.read((char *)&_lvl.score, sizeof(int));
//             it_l++;
//             }*/
//            it++;
//        };
//        in.close();
//    }
}

void CKFileInfo::saveBin(const char *_filename)
{
//    // std::cout<<<<std::endl;
//    std::string path = CCFileUtils::sharedFileUtils()->getDocumentPath();
//    path.append(_filename);
//    std::ofstream out(path.c_str(), std::ios::out | std::ios::binary);
//   // printf("<key>total_score</key><integer>%d</integer>",m_data.total_score);
//  //  printf("<key>last_difficult</key><integer>%d</integer>",m_data.last_difficult);
//    
//    std::cout<<path<<std::endl;
//    if (out.is_open())
//    {
//        out.write((char* )&m_data.total_score, sizeof(int));
//      //  out.write((char* )&m_data.last_difficult, sizeof(int));
//        std::list<World>::iterator it = m_data.m_world.begin();
//        while(it != m_data.m_world.end())
//        {
//            //out << "<dict>" ;
//            World _world = *it;
//            out.write(_world.name.c_str(), sizeof(255));
//            out.write((char *)&_world.open_lvl, sizeof(int));
//          //  out.write((char *)&_world.last_speed, sizeof(int));
//         //   out.write((char *)&_world.last_speed, sizeof(int));
//            
////            for(int i = 0; i < CK::COUNT_LEVEL_IN_WORLD; ++i)
////            {
////                out.write((char *)&_world.m_level[0][i].time, sizeof(int));
////                out.write((char *)&_world.m_level[0][i].score, sizeof(int));
////            }
//            /*
//            std::list<Level>::iterator it_l = _world.m_level.begin();
//            while(it_l != _world.m_level.end())
//            {
//                Level _lvl = *it_l;
//                out.write((char *)&_lvl.time, sizeof(int));
//                out.write((char *)&_lvl.score, sizeof(int));
//                it_l++;
//            }*/
//            it++;
//        };
//        out.close();
//    }
}
