//
//  CKLoading.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 22.01.13.
//
//

#include "CKLoading.h"
#include "CKLoaderGUI.h"



CKLoading::~CKLoading()
{
    CCLOG("~CKLoading");
    m_gui->removeAtlasPlist();
    CC_SAFE_DELETE(m_gui);
    removeAllChildrenWithCleanup(true);
  //  CC_SAFE_RELEASE_NULL(this);
};

CKLoading::CKLoading()
{
    m_gui = NULL;
    m_parent = NULL;
    m_node = NULL;
    enable = true;
    initGui();
};

void CKLoading::initGui()
{
    m_gui = new CKGUI();
    m_gui->create(CCNode::create());
    m_gui->load(CK::loadFile("c_Loading.plist",true).c_str());
    m_gui->setTouchEnabled(false);
    m_gui->setTarget(this);
    addChild(m_gui->get(),1);
    this->retain();
};
 
void CKLoading::activeted(cocos2d::CCLayer *_node,CCScene* _scene)
{
    CCLOG("CKLoading::activeted");
    if(!enable)
    {
        CCLOG("activeted::Error::Loading disabled");
        return;
    }
    if(this->isVisible())
    {
        CCLOG("activeted::Error::Loading visible");
        return;
    }
    if(this->getParent())
        this->getParent()->removeChild(this, false);
    
    m_gui->reloadAtlas();
    m_node = _node;
   // m_node->setTouchEnabled(false);
    this->setTouchEnabled(false);
    this->setVisible(true);
    this->visit();
    if(!_scene)
        {
            m_parent = m_node->getParent();
            m_parent->addChild(this,100);
        }
    else
    {
        m_parent = _scene;
        _scene->setVisible(true);
        this->setVisible(true);
        _scene->addChild(this,100);
    };
    
    schedule(schedule_selector(CKLoading::update),1.0/60.0);
};

void CKLoading::deactived()
{
    CCLOG("CKLoading::deactived");
  //  return;
    if(m_node)
    {
        m_parent->removeChild(this, false);
       // m_node->setTouchEnabled(true);
        m_node->setVisible(true);
    };
    enable = false;
    m_node = NULL;
    this->setTouchEnabled(false);
    this->setVisible(false);
    unschedule(schedule_selector(CKLoading::update));
};

void CKLoading::update(const float &dt)
{
    m_gui->getChildByTag(1)->getNode()->setRotation(m_gui->getChildByTag(1)->getNode()->getRotation() + 1);
};