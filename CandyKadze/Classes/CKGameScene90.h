//
//  CKGameScene90.h
//  CandyKadze
//
//  Created by PR1.Stigol on 01.11.12.
//
//

#ifndef __CandyKadze__CKGameScene90__
#define __CandyKadze__CKGameScene90__

#include <iostream.h>
#include <cocos2d.h>
#include "CK.h"
#include "CKHelper.h"
#include "CKWordScene.h"
#include "CKAirplane.h"
#include "CKBomb.h"
#include "CKCubeArray.h"
#include "CKButton.h"
#include "CKStart90.h"
#include "CKMainMenuScene.h"
#include "CKLoaderParallax.h"
#include "CKGameSceneBase.h"
using namespace CK;

class CKGameScene90 : public cocos2d::CCLayer ,public CKAudioObject,CCAccelerometerDelegate {
public:
    virtual ~CKGameScene90();
    CKGameScene90();
    // returns a Scene that contains the HelloWorld as the only child
    static cocos2d::CCScene* scene();
    cocos2d::CCSize win_size;
    
    virtual void draw();
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesCancelled(CCSet *pTouches, CCEvent *pEvent){ccTouchesEnded(pTouches, pEvent);};
    bool touchShareEnded(cocos2d::CCSet* touches);
    bool touchShareBegan(cocos2d::CCSet* touches);
    
    virtual void onEnter();
    void update(float _dt);
    void addScore(int _score = 0);
    void updateBomb(const float &_dt);
    void showOrHidePauseMenu(bool _show = true);
    void setPause();
    void endShowPause();
    void enableEveryplay(CCNode *_sender);
    void initGuiWin();
    void initGuiOver();
    int getCurentScore()
    {
        return curent_score;
    }
private:

    const float TIME_SHOW_FRAME_ANIMATION = 0.3;
    int time_start,pause_time;
    unsigned int scoring_sound_id;
    float add_score_koef;

    int curent_score,last_score,last_curent_score;
    
    bool is_posted_in_facebook;
    
    bool is_game_win;
    bool score_active;
    bool is_hiscore;
    bool have_everyplay_record;

    bool is_pause;
    int ground_height;
    int count_row;
    int count_coll;
    
    int curen_load;
    int panel_left;
    int current_level_id,curent_world_id;
    int count_active_touch;

    std::string airplane_name;
    std::string bomb_name;

    void load();
    void createBanner();
    void functAsunc(CKGameScene90* _sender);
    void updateLoading(const float &_dt);

    void touchAirplane(const CCPoint &_point);
    void showBanner(bool _anim = true);
    void updateScore(float _dt);
    void menuCallback(CCNode* _sender);
    void startAddScoreFn();
    void animationLabelMult();
    
    void initGUI();
    void initAirpalne();
    void initGUIShare();
    
    void initBGStartAndCloud();
    void saveProgress();
    void initPauseMenu();
    void showEndMenu(bool _win = false);
    void menuCall(CCNode* _sender,void *_value);
    void shareCall(CCNode* _sender,void *_value);
    void gameWin();
    void restart();
    
   // void showLabelScore(const CCPoint& _pos,int _score);
  //  void hideNode(CCNode *_sender);
    int count_accuvate_shots;
    std::list<cocos2d::CCLabelTTF*> label_pool;
    std::list<cocos2d::CCLabelTTF*>::iterator label_pool_it;
    std::string font_name;
    int BOX_SIZE;
    void incLevelWorld();
    void createLabelScorePool(CCNode *_node);
    void hideNode(CCNode *_sender);
    void showLabelScore(const CCPoint& _pos,int _score);

    CCTexture2D *texture_banner;
    CCPoint touch_position;
    CCSprite* bg_mask90,*banner;
    CCMenu* menu_banner;
    CCLabelTTF* label_next;
    CCMenuItemSprite *pauseItem;
    CCLabelTTF* label_info;
    CCLabelTTF* label_score;
    CCSprite* button_active,*button_nonactive,*button_presse;
    CKParalaxNode m_loading;
    CKAirplane90* m_airplane;
    CKCubeArrayBase* m_cube_array;
    CKStart90* stars_spectrum;
    CKBomb90* m_bombs;
    CCSprite* moved_bomb;
    
    CKGUI* m_gui_pause;
    CKGUI* m_gui_win;
    CKGUI* m_gui_over;
    CKGUI* m_share;
};

#endif /* defined(__CandyKadze__CKGameScene__) */
