//
//  CKLoading.h
//  CandyKadze
//
//  Created by PR1.Stigol on 22.01.13.
//
//

#ifndef __CandyKadze__CKLoading__
#define __CandyKadze__CKLoading__

#include <iostream>
#include "cocos2d.h"
#include <list.h>
#include <vector.h>

class CKGUI;
class CKObject;
using namespace cocos2d;

class CKLoading: public cocos2d::CCLayer
{
    CKGUI *m_gui;
    CCLayer* m_node;
    CCNode* m_parent;
    void initGui();
    bool enable;
public:
    CKLoading();
    ~CKLoading();
    void setEnableActive(bool _b = true){
        enable = _b;
    };
    void activeted(CCLayer* _node,CCScene* _scene = NULL);
    void deactived();
    void update(const float &dt);
    //static CCScene* scene();
};
#endif /* defined(__CandyKadze__CKLoading__) */
