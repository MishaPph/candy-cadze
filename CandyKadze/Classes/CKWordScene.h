//
//  CKWordScene.h
//  CandyKadze
//
//  Created by PR1.Stigol on 18.09.12.
//
//

#ifndef __CandyKadze__CKWordScene__
#define __CandyKadze__CKWordScene__


#include <cocos2d.h>
#include <map.h>
#include "CKInventory.h"
#include "CKLoaderGUI.h"

using namespace cocos2d;

class CKWordScene : public cocos2d::CCLayer {
public:
    virtual ~CKWordScene();
    CKWordScene();
    static cocos2d::CCScene* scene();
    void showMsg(CCNode *_sender,void *_data);
private:
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesCancelled(CCSet *pTouches, CCEvent *pEvent){ccTouchesEnded(pTouches, pEvent);};
    
    bool init();
    CCLabelTTF* label_score;
    CCSize win_size;

    
    pthread_t m_thread;
    
    CCLabelTTF* m_label[16];
    int font_size;
    CCMenuItemImage *menuItem[16];
    CCMenuItemImage *diffItem[5];
  //  CCMenuItemImage *diffBuy[5];
    CCMenuItemImage *menuWorld[5];
    CCMenuItemImage *menuBuy[5];
    
    CKInventory *m_inventory;
    CKShop *m_shop;
    bool is_ipad,update_active;
    int curen_load;
    int previor_scroll_value;
    CCMenu *difficult_menu;
    CCSprite* message_sprite,*bg[5];
    CKGUI * m_info_msg,*m_dialog;
    CKGUI *m_backnode;
  //  void buyCallback(CCNode* _sender);
    void createDifficultyMenu();
    void diffCallback(CCNode* _sender);
    
    void loadAsync(void *_data);
    static void* thread_function( void *ptr );
    void load();
    
    static void load(CKWordScene *_scene);
    
    void initBackground();
    void unlockWorld();
    void buyWorld(void *_d);
   // int world_buy_number;
    signed int unlock_world,current_buy_world;
    void createMessages(std::string _filename);
    void showMessage(int _num = 0);
    void update(const float &_dt);
    //void createLvlMenu90();
    CKObject* m_scroll;
    void showAllWorl();
    void showLvlInWorld(int _world = -1);
    void initKMDialogGui();
    void initLock();
    void initGUI();
    void showAvatarInWorld(int _world = -1);
    void animationLock();
    void animationLockSkew(int _value);
    void functAsunc(CKWordScene * _sender);
    
    void showDialog(void *_d);
    void initShopInventory(CCScene* _scene, CKWordScene* _layer);
    void menuCallback(CCNode* _sender,void *_data);
    void menuCall(CCNode* _sender,void *_value);
    void compliteLoadFriend(CCNode* _sender,void *_value);
    void callBuy(CCNode* _sender,void *_value);
    void levelCall(CCNode* _sender,void *_value);
    void selectorCall(CCNode* _sender,void *_value);
    void menuBuyCallback(CCNode* _sender);
    void callDialog(CCNode *_sender,void *_value);
    
    void timeDisableKM();
    void levelCallback(CCNode* _sender);
    void loadFriendAvatar();
    void insertLvlAvatar(int _num_lvl,const char *user_id);
    void insertWorldAvatar(int _num_world,const char *user_id);
};

#endif /* defined(__CandyKadze__CKWordScene__) */
