//
//  CKShop.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 25.12.12.
//
//

#include "CKShop.h"
#include "CKLoaderGUI.h"
#include "CKInventory.h"
#include "CKWordScene.h"
#include "CKMainMenuScene.h"
#include "CKLanguage.h"
#include "StatisticLog.h"
#include "CKTwitterHelper.h"
#include "CKFacebookHelper.h"

#define LCZ(varType) CKLanguage::Instance().getLocal(varType)

CKShop::~CKShop()
{
    CCLOG("CKShop::~destroy");
    m_gui->removeAtlasPlist();
    CC_SAFE_DELETE(m_gui);
    CC_SAFE_DELETE(m_bomb);
    CC_SAFE_DELETE(m_coins);
    CC_SAFE_DELETE(m_dialog);
    CC_SAFE_DELETE(m_dialog_wait);
    CCTexture2D * texture =  CCTextureCache::sharedTextureCache()->textureForKey(CK::loadImage("a_InventoryShop.png").c_str());
    CCLOG("CKShop:: a_InventoryShop.png %d",(texture)?texture->retainCount():0);
};

CCScene* CKShop::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    // add layer as a child to scene
    CCLayer* layer = new CKShop();
    // CCLOG("scene:: scene %d layer %d ",scene,layer);
    if(layer->init())
    {
        scene->addChild(layer);
        layer->setTouchEnabled(true);
        layer->setTag(CK::LAYERID_SHOP);
        layer->release();
    };
    CKHelper::Instance().compliteLoadScene(scene);
    return scene;
};

CKShop::CKShop()
{
    CCLOG("/n __________________[ SHOP ]_______________");
    
    this->setTouchEnabled(false);
    anim_size = 0.0;
    sum_bouth_bomb = 0;
    enable_animation = false;
    show_button_km_inventory = false;
    curent_object = NULL;
    show_object = NULL;
    buy_object = NULL;
    dict_shop = NULL;
    m_dialog = NULL;
    curent_load = 0;
    
    CKHelper::Instance().show_inventory_km_button = false;
    ObjCCalls::setCallBack(this,callfuncND_selector(CKShop::callbackIAP));
    load();
};

void CKShop::load()
{
    if(CKHelper::Instance().menu_number == CK::SCENE_GAME)
    {
        CKAudioMng::Instance().playMenuSound();
        CKHelper::Instance().menu_number = CK::SCENE_WORLD;
    };
    
    CKFileInfo::Instance().reload();
    
    this->setTouchEnabled(true);
    win_size = CCDirector::sharedDirector()->getWinSize();
    initGUI();
    
    initScrollBomb();
    initScrollCoins();
    m_coins->m_scroll->setVisible(false);
    m_bomb->m_scroll->setVisible(true);
    
    
    if(CKFileOptions::Instance().isKidsModeEnabled())
        initKMDialogGui();
    
    initDialogWait();
    CKFileInfo::Instance().genShopBombWithCurentData();
    
    ObjCCalls::logPageView();
    ObjCCalls::logPageView(true,"/Shop");
    
    schedule(schedule_selector(CKShop::update), 1.0/60.0);
    
    
    if(CKHelper::Instance().show_shop_bomb > 0)
    {
        CK::StatisticLog::Instance().setNewPage(CK::Page_ShopBomb);
        int d = 1;
        callTopTap(NULL, &d);
        sort(255);
        showObjectType(CKHelper::Instance().show_shop_bomb);
        CKHelper::Instance().show_shop_bomb = 0;
        enable_animation = true;
        return;
    }else if(CKHelper::Instance().show_shop_coins > 0)
    {
        CK::StatisticLog::Instance().setNewPage(CK::Page_ShopCoins);
        sort(99);
        int d = 2;
        callTopTap(NULL, &d);
        
        int t = (CKHelper::Instance().show_shop_coins - 1);
        CKHelper::Instance().show_shop_coins = 0;
        
        callFilterCoins(NULL,&t);
        enable_animation = true;
        return;
    };
    CK::StatisticLog::Instance().setNewPage(CK::Page_ShopBomb);
    int d = 1;
    callTopTap(NULL, &d);
    sort(99);
    enable_animation = true;
};


void CKShop::initGUI()
{
    m_gui = new CKGUI();
    m_gui->create(CCNode::create());
    m_gui->load(CK::loadFile("c_Shop.plist",true).c_str());
    m_gui->setTouchEnabled(true);
    m_gui->setTarget(this);
    m_gui->addFuncToMap("menuCall", callfuncND_selector(CKShop::menuCall));
    m_gui->addFuncToMap("callFilter", callfuncND_selector(CKShop::callFilter));
    m_gui->addFuncToMap("callTopTap", callfuncND_selector(CKShop::callTopTap));
    m_gui->addFuncToMap("callHint", callfuncND_selector(CKShop::callHint));
    m_gui->addFuncToMap("callCoins", callfuncND_selector(CKShop::callCoins));
    m_gui->addFuncToMap("callBuyIAP", callfuncND_selector(CKShop::callBuyIAP));
    m_gui->addFuncToMap("callFilterCoins", callfuncND_selector(CKShop::callFilterCoins));
    
    addChild(m_gui->get(),1);
    

    top_tap[0] = m_gui->getChildByTag(101);
    top_tap[1] = m_gui->getChildByTag(102);
    
    bomb_tap_pos = top_tap[0]->getPos();
    
    top_tap[0]->setStateImage(1);
    for(int i = 0;i < COUNT_LEFT_TAP;++i)
    {
        left_tap[i] = m_gui->getChildByTag(200 + i);
    }
    
    for(int i = 0;i < COUNT_COINS_TAP;++i)
    {
        coins_tap[i] = m_gui->getChildByTag(300 + i);
    }
    for (int i = 0; i < COUNT_COINS_TAP; ++i) {
        if(coins_tap[i]->isTouch())
        {
            coins_tap[i]->getNode()->setScale(0.2);
            coins_tap[i]->setPosition(bomb_tap_pos.x,top_tap[1]->getPos().y);
            coins_tap[i]->setTouch(false);
        }
    };
    updateTotalScoreLabel();
    m_gui->getChildByTag(-1)->setVisible(false);
    m_gui->setMovedScroll(false);
    m_gui->getChildByTag(CK::Action_KidsMode)->setVisible(false);
    if(CKFileOptions::Instance().isKidsModeEnabled())
    {
        enableKidsMode();
    }    
};

void CKShop::initKMDialogGui()
{
    m_dialog = new CKGUI();
    m_dialog->create(CCNode::create());
    m_dialog->load(CK::loadFile("c_ModalWindowTurnOffKM.plist").c_str());
    m_dialog->setTouchEnabled(true);
    m_dialog->setTarget(this);
    m_dialog->addFuncToMap("callDialog", callfuncND_selector(CKShop::callDialog));
    m_dialog->get()->setVisible(false);
    if(win_size.width == 568)
    {
        m_dialog->get()->setPosition(ccp(44,0));
    };
    addChild(m_dialog->get(),99);
};

void CKShop::initDialogWait()
{
    m_dialog_wait = new CKGUI;
    m_dialog_wait->create(CCNode::create());
    m_dialog_wait->load(CK::loadFile("c_ModalWindowWait.plist").c_str());
    m_dialog_wait->setTarget(this);
    m_dialog_wait->get()->setVisible(false);
    m_dialog_wait->get()->setTag(0);
    m_dialog_wait->getChildByTag(3)->setText(LCZ("Connection..."));
    addChild(m_dialog_wait->get(),99);
    m_dialog_wait->get()->setPosition(ccp(win_size.width/2,win_size.height/2));
    if(win_size.width == 568)
    {
        m_dialog_wait->get()->setPositionX(win_size.width/2 + 44);
    };
    m_dialog_wait->getChildByTag(4)->getNode()->runAction(CCRepeatForever::create(CCRotateBy::create(1.0, 360)));
};
void CKShop::showDialogWait()
{
   // unschedule(schedule_selector(CKShop::hideDialogWait));
   // schedule(schedule_selector(CKShop::hideDialogWait),TIMEOUT_WAIT,true,TIMEOUT_WAIT);
    m_dialog_wait->get()->setVisible(true);
    //m_dialog_wait->get()->setTag(m_dialog_wait->get()->getTag() + 1);
    this->setTouchEnabled(false);
    CCLOG("CKShop::showDialogWait %d",m_dialog_wait->get()->getTag());
}
void CKShop::hideDialogWait()
{
    // return;
    //m_dialog_wait->get()->setTag(m_dialog_wait->get()->getTag() - 1);
    //if(m_dialog_wait->get()->getTag() == 0)
    {
        unschedule(schedule_selector(CKShop::hideDialogWait));
        m_dialog_wait->get()->setVisible(false);
        this->setTouchEnabled(true);
    };
    CCLOG(" CKShop::hideDialogWait %d",m_dialog_wait->get()->getTag());
};

void CKShop::updateTotalScoreLabel()
{
    char str_total[NAME_MAX];
    sprintf(str_total,"%d",CKFileInfo::Instance().getTotalScore());
    m_gui->getChildByTag(LABEL_TOTAL_SCORE)->getChildByTag(2)->setText(str_total);
};

void CKShop::setCoinsBuyValue(CKObject *_obj,int _count,float _price)
{
    char str[NAME_MAX];
    sprintf(str, "%.2f$",_price);
    static_cast<CCLabelBMFont *>(_obj->getChildByTag(4)->getNode())->setString(str);
    
    char str_count[NAME_MAX];
    sprintf(str_count, "%d %.3d",(_count/1000),_count%1000);
    static_cast<CCLabelBMFont *>(_obj->getChildByTag(5)->getNode())->setString(str_count);
    _obj->getChildByTag(3)->setValue(_count);
};

void CKShop::updateAdsRemove()
{
    CKObject *tmp = m_coins->m_scroll->getChildByTag(48);
    if(!tmp->isVisible())
        return;
    tmp->setVisible(false);
    float hei = tmp->getNode()->getContentSize().height*1.5;
    for (int i = 5; i > 0; i--) {
        CKObject *tmp = m_coins->m_scroll->getChildByTag(i);
        if(!tmp)
            continue;
        
        tmp->setPosition(tmp->getPos().x, tmp->getPos().y - hei);
        
    };
    m_coins->top_pos += hei;
};

void CKShop::setRemoveAdsPrice(CKObject *_obj,float _price)
{
    char str[NAME_MAX];
    sprintf(str, "%.2f$",_price);
    static_cast<CCLabelBMFont *>(_obj->getChildByTag(53)->getNode())->setString(str);
};

void CKShop::setKidsModePrice(CKObject *_obj,float _price)
{
    char str[NAME_MAX];
    sprintf(str, "%.2f$",_price);
    static_cast<CCLabelBMFont *>(_obj->getChildByTag(CK::Action_Buy_KidsMode + 2)->getNode())->setString(str);
};

void CKShop::setMultipliersValue(CKObject *_obj,int _count,float _price)
{
    char str[NAME_MAX];
    sprintf(str, "%.2f$",_price);
    _obj->getChildByTag(51)->setText(str);
    sprintf(str, "x%d",_count);
    _obj->getChildByTag(6)->setText(str);
    _obj->getChildByTag(CK::Action_Buy_Double)->setValue(_count*100);
};

void CKShop::setPlaneSkinValue(CKObject *_obj,const char * _name,float _price)
{
    static_cast<CCLabelBMFont *>(_obj->getChildByTag(5)->getNode())->setString(_name);
    char str[NAME_MAX];
    sprintf(str, "%.2f$",_price);
    static_cast<CCLabelBMFont *>(_obj->getChildByTag(4)->getNode())->setString(str);
};

void CKShop::updateMultiCoins(bool _start)
{
    double d = difftime(time(0),CKFileInfo::Instance().getMultiBuyData());
    char str[NAME_MAX];
    int hour = int(d/3600);
    int min = int((d - 3600*hour)/60);
    sprintf(str, "%d:%.2d",abs(hour),abs(min));
    CCLOG("CKShop::updateMultiCoins %s",str);
    if(hour < 1 && hour >= -24 && min <=0)
    {
        int count_prev_unvisible = 0;
        //if(!_start)
        for (int i = 1; i < 4; ++i) {
            if(!m_coins->m_scroll->getChildByTag(2)->getChildByTag(i)->isVisible())
            {
                count_prev_unvisible++;
            }
        };
        
        int n = 1;
        if(CKFileInfo::Instance().getMultiBuyKoef() == 3)
        {
            n = 2;
        }
        else if (CKFileInfo::Instance().getMultiBuyKoef() == 5)
        {
            n = 3;
        };
        
      //  float offsetY = 0.0;
        CKObject *tobj = m_coins->m_scroll->getChildByTag(2)->getChildByTag(n);
        tobj->getChildByTag(CK::Action_Buy_Double)->setTouch(false);
        static_cast<CCLabelBMFont *>(tobj->getChildByTag(CK::Action_Buy_Double + 2)->getNode())->setString(str);
        tobj->getChildByTag(12)->setVisible(true);
        
        float heig = tobj->getNode()->getContentSize().height;

        if(n == 2)
        {
            m_coins->m_scroll->getChildByTag(2)->getChildByTag(1)->setVisible(false);
        }
        if(n == 3)
        {
            m_coins->m_scroll->getChildByTag(2)->getChildByTag(1)->setVisible(false);
            m_coins->m_scroll->getChildByTag(2)->getChildByTag(2)->setVisible(false);
            
        }
        int count_unvisible = 0;
        for (int i = 1; i < 4; ++i) {
            if(!m_coins->m_scroll->getChildByTag(2)->getChildByTag(i)->isVisible())
            {
                count_unvisible++;
            }
        };
        
        if(count_unvisible - count_prev_unvisible != 0)
        {
            m_coins->m_scroll->getChildByTag(2)->moveAllChildren(0,-heig*(count_unvisible - count_prev_unvisible));
            for (int i = 1; i < 4; ++i) {
                CKObject *tobj = m_coins->m_scroll->getChildByTag(2)->getChildByTag(i);
                tobj->setPosition(tobj->getPos().x, tobj->getPos().y + heig*(count_unvisible - count_prev_unvisible));
            };
        }
    };
};
void CKShop::calcScrollCoinsSize()
{
    float pos_Y = win_size.height*0.35;
    //set position group object
    float f_bord = win_size.height/20.0;
    for (int i = 5; i > 0; i--) {
        CKObject *tmp = m_coins->m_scroll->getChildByTag(i);
        if(!tmp)
            continue;
        CCRect m_rect = tmp->getChildZoneRect();
        float size_Y = (m_rect.size.height - m_rect.origin.y);
        tmp->getNode()->setContentSize(CCSize(tmp->getNode()->getContentSize().width, size_Y));
        
        tmp->setPosition(tmp->getPos().x, pos_Y);
        pos_Y += size_Y + f_bord;
        CCLOG("%d #%f size_Y",i,size_Y);
    };
    
    m_coins->top_pos = win_size.height*0.85 - pos_Y;
    m_coins->bottom_pos = win_size.height/10;
}
void CKShop::showObjectType(const int &_type)
{
    it = shop_object_list.begin();
    while (it != shop_object_list.end()) {
        if(int(*(*it)->getValue())  == _type)
        {
            (*it)->setVisible(true);
            showBuyObjectInfo(*it);
            return;
        };
        *it++;
    };
};
void CKShop::initScrollCoins()
{
    m_coins = new CKSScroll;
    m_coins->start_anim = false;
    m_coins->m_scroll = m_gui->getChildByTag(5000);
    m_coins->m_scroll->setTouch(false);
    m_coins->posX = m_coins->m_scroll->getPos().x;
    
    //buy coins
    CKObject *buyCoins = m_coins->m_scroll->getChildByTag(1);
    float offset_y = buyCoins->getChildByTag(1)->getNode()->getContentSize().height;
    height_buy_coins = offset_y;
    CCDictionary *dict = CCDictionary::createWithContentsOfFile("shop_coins.plist");
    CCArray* a_buy_coins = (CCArray*)dict->objectForKey("BuyCoins");
    CCObject* pElement = NULL;
    int k = 0;
    CCARRAY_FOREACH(a_buy_coins, pElement)
    {
        k++;
        if(k > 1)
        {
            buyCoins->moveAllChildren(0,offset_y);
            buyCoins->copyObject(buyCoins->getChildByTag(1), k);
        };
        CCDictionary *elem = (CCDictionary*)pElement;
        setCoinsBuyValue(buyCoins->getChildByTag(k),dictInt(elem, "count"),dictFloat(elem, "price"));
    };
    height_buy_coins = k*offset_y + offset_y*0.5;
    
    CKObject *multi = m_coins->m_scroll->getChildByTag(2);
    offset_y = multi->getChildByTag(1)->getNode()->getContentSize().height;
    pElement = NULL;
    k = 0;
    
    int n = 1;
    if(CKFileInfo::Instance().getMultiBuyKoef() == 3)
    {
        n = 2;
    }
    else if (CKFileInfo::Instance().getMultiBuyKoef() == 5)
    {
        n = 3;
    };

    CCArray* a_skin = (CCArray*)dict->objectForKey("MultiCoins");
    CCARRAY_FOREACH(a_skin, pElement)
    {
        k++;
        if(k > 1)
        {
            multi->moveAllChildren(0,offset_y);
            multi->copyObject(multi->getChildByTag(1), k);
        };
        CCDictionary *elem = (CCDictionary*)pElement;
        setMultipliersValue(multi->getChildByTag(k),dictInt(elem, "count"),dictFloat(elem, "price"));
        m_coins->m_scroll->getChildByTag(2)->getChildByTag(k)->getChildByTag(12)->setVisible(false);
    };

    updateMultiCoins();
    calcScrollCoinsSize();

    
    CKObject *kids_obj = m_coins->m_scroll->getChildByTag(4)->getChildByTag(1);
    if(ObjCCalls::IAPHelper_isbuy("com.cc.kids_mode"))
    {
        kids_obj->getChildByTag(CK::Action_Buy_KidsMode)->setTouch(false);
        kids_obj->getChildByTag(CK::Action_Buy_KidsMode + 2)->setVisible(false);//label price
        kids_obj->getChildByTag(12)->setVisible(true);//picture sold
    }
    else
    {
         kids_obj->getChildByTag(12)->setVisible(false);//picture sold
        setKidsModePrice(kids_obj, 9.99);
    };
    
    if(ObjCCalls::IAPHelper_isbuy("com.cc.ads_removing"))
    {
        updateAdsRemove();
    }
    else
    {
        setRemoveAdsPrice(m_coins->m_scroll->getChildByTag(48), 0.99);
    }
    
    
};




void CKShop::initScrollBomb()
{
    m_bomb = new CKSScroll;
    m_bomb->start_anim = false;
    m_bomb->m_scroll = m_gui->getChildByTag(2000);
    m_bomb->m_scroll->setTouch(false);
    
    shop_object_list.clear();
    for(int i = 1; i <= COUNT_OBJECT_IN_SCROLL;++i)
    {
        CKObject * tmp = m_bomb->m_scroll->getChildByTag(1000 + i);
        shop_object_list.push_back(tmp);
        tmp->setValue(0);
        tmp->setVisible(false);
    };
    
    loadWithDict();
    
    price_list.clear();
    for(int i = 1; i <= COUNT_BUY_BUTTON;++i)
    {
        CKObject * tmp = m_bomb->m_scroll->getChildByTag(2000 + i);
        price_list.push_back(tmp);
        tmp->setValue(0);
        char str[NAME_MAX];
        sprintf(str, "%dx",mas_count[i-1]);
        tmp->getChildByTag(COUNT_BUY_ID)->setText(str);
        tmp->getChildByTag(COUNT_BUY_ID)->setValue(mas_count[i-1]);
        tmp->setVisible(false);
    };
    
    m_bomb->min_pos = MIN_SCROLL_FRAME;
    m_bomb->frame = m_bomb->m_scroll->getNode()->getContentSize().height/COUNT_OBJECT_IN_SCROLL;
    
    initScroll();
    
    m_bomb->height = (*shop_object_list.begin())->getNode()->getContentSize().height/2;
    m_bomb->top_pos = MIN_SCROLL_FRAME*m_bomb->frame;
    m_bomb->bottom_pos = m_bomb->max_pos*m_bomb->frame + m_bomb->height;
    m_bomb->posX = m_bomb->m_scroll->getPos().x;
    m_bomb->m_scroll->getNode()->setPosition(ccp(m_bomb->posX,m_bomb->top_pos));
    m_bomb->m_scroll->savePosition(ccp(m_bomb->posX,m_bomb->top_pos));
    
};

void CKShop::initScroll()
{
    it = shop_object_list.begin();
    int type = CKBonusMgr::Instance().begin();
    while (type != 0 && it != shop_object_list.end()) {
        if(type == 1 || type >= 1000|| type == 85 ||type == 86 ||type == 87||type == 67||type == 68)
        {
            type = CKBonusMgr::Instance().next();
            continue;
        };
        CCSprite* tmp = static_cast<CCSprite*>((*it)->getChildByTag(1)->getNode());
        const char* tex_name = CKBonusMgr::Instance().getInventoryNameById(type).c_str();
        CCSpriteFrame* s_frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(tex_name);
        tmp->setTexture(s_frame->getTexture());
        tmp->setTextureRect(s_frame->getRect(), s_frame->isRotated(), tmp->getContentSize());
        
        //set level number in bomb
        char str[NAME_MAX];
        // sprintf(str,"%d",type%10);
        
        std::string type_lvl = "|";
        switch (type%10) {
            case 2:
                type_lvl = "||";
                break;
            case 3:
                type_lvl = "|||";
                break;
            default:
                break;
        }
        
        (*it)->getChildByTag(1)->getChildByTag(1)->setText(type_lvl.c_str());
        
        //set cost in this type of bomb
        sprintf(str,"%d",CKBonusMgr::Instance().getInfoById(type)->cost);
        (*it)->getChildByTag(LABEL_PRICE_VALUE)->setText(str);
        
        //set count bomb in inventory and slots on label
        int count = CKFileInfo::Instance().getCountInInventory(type);
        count += CKFileInfo::Instance().getCountInInventorySelect(type);
        sprintf(str,"%d",count);
        (*it)->getChildByTag(LABEL_ININVENTORY_VALUE)->setText(str);
        //save curent bomb type in object value
        (*it)->setValue(type);
        
        (*it)->getChildByTag(BUTTON_HINT_ID)->setValue(type);
        
        it++;
        type = CKBonusMgr::Instance().next();
    };
};

void CKShop::updateValueScroll(CKObject *_obj)
{
    if(CKFileOptions::Instance().isKidsModeEnabled())
    {
        _obj->getChildByTag(LABEL_PRICE_ID)->setVisible(false);
        _obj->getChildByTag(LABEL_PRICE_VALUE)->setVisible(false);
        _obj->getChildByTag(CK::Action_KidsMode)->setVisible(true);
    }else
    {
        _obj->getChildByTag(CK::Action_KidsMode)->setVisible(FALSE);
        _obj->getChildByTag(LABEL_PRICE_ID)->setVisible(true);
        _obj->getChildByTag(LABEL_PRICE_VALUE)->setVisible(true);
    };
    _obj->getChildByTag(LABEL_FREE_ID)->setVisible(false);
    
    if(getBomb(*_obj->getValue())!= NULL)
    {
        if(CKFileInfo::Instance().bomb_info_map[*_obj->getValue()]->haveFreePack())
        {
            _obj->getChildByTag(LABEL_FREE_ID)->setVisible(true);
            _obj->getChildByTag(LABEL_PRICE_ID)->setVisible(false);
            _obj->getChildByTag(LABEL_PRICE_VALUE)->setVisible(false);
        }
    }
}
void CKShop::sort(unsigned char _type)
{
    CCLOG("sort:: type %d",_type);
    
    m_bomb->start_anim = false;
    top_tap[0]->setStateImage(1);
    top_tap[1]->setStateImage(0);
    for(int i = 0;i < COUNT_LEFT_TAP;i++)
        left_tap[i]->setStateImage(0);
    if(_type == 255) //coins
    {
        left_tap[0]->setStateImage(1);
    }
    else
    {
        if(_type != 99) // safe
            left_tap[_type]->setStateImage(1);
        else
            left_tap[4]->setStateImage(1);
    }
    
    float s_height = m_bomb->m_scroll->getNode()->getContentSize().height - m_bomb->height;
    float stepX = m_bomb->m_scroll->getNode()->getContentSize().width*1.2;
    m_bomb->m_scroll->getNode()->stopAllActions();
    it = shop_object_list.begin();
    active_object_list.clear();
    curent_sort_type = _type;
    int i = 0;
    first_object = NULL;
    last_object = NULL;
    while (it != shop_object_list.end()) {
        (*it)->setVisible(false);
        (*it)->setTouch(false);
        if(_type == 99)
        {
            ShopBomb * _shop_bomb = getBomb(int(*(*it)->getValue()));
            if(*(*it)->getValue() != 0 && _shop_bomb)//show all or show only filtered type (if _type = 1 then show bomb type 21,31,41,51 itc.)
            {
                (*it)->getNode()->stopAllActions();
                (*it)->setPosition(stepX/2,s_height - i*m_bomb->frame);
                active_object_list.push_back((*it));
                if(!first_object)
                    first_object = (*it);
                last_object = (*it);
                (*it)->setTouch(true);
                updateValueScroll(*it);
                
                (*it)->getChildByTag(BEST_PRICE_ID)->setVisible(true);
                if(!_shop_bomb->haveFreePack() && _shop_bomb->haveSoldPack())
                {
                    (*it)->getChildByTag(BEST_PRICE_ID)->setStateImage(1);
                };
                i++;
            };
        }
        else
        {
            if(_type == 255 || int(*(*it)->getValue()%10) == _type)//show all or show only filtered type (if _type = 1 then show bomb type 21,31,41,51 itc.)
            {
                if(*(*it)->getValue() != 0)
                {
                    (*it)->getNode()->stopAllActions();
                    (*it)->setPosition(stepX/2,s_height - i*m_bomb->frame);
                    active_object_list.push_back((*it));
                    if(!first_object)
                        first_object = (*it);
                    last_object = (*it);
                    (*it)->setTouch(true);
                    updateValueScroll(*it);
                    
                    if(getBomb(int(*(*it)->getValue())))
                        (*it)->getChildByTag(BEST_PRICE_ID)->setVisible(true);
                    else
                        (*it)->getChildByTag(BEST_PRICE_ID)->setVisible(false);
                    
                    i++;
                };
            };
        }
        it++;
    };
    count_shop_elements = i;
    prev_num = -1;
    updateVisibleActiveList(0);
};

std::vector<CKObject*>::iterator& CKShop::findObjectInShop(CKObject *_object)
{
    it = active_object_list.begin();
    while (it != active_object_list.end()) {
        if( *it == _object)
            return it;
        it++;
    };
    return it;
};

ShopBomb* CKShop::getBomb(const unsigned int &_type)
{
    bomb_it = CKFileInfo::Instance().bomb_info_map.begin();
    while (bomb_it != CKFileInfo::Instance().bomb_info_map.end()) {
        if(bomb_it->first == _type)
        {
            return bomb_it->second;
        }
        bomb_it++;
    }
    
    return NULL;
   // return CKFileInfo::Instance().bomb_info_map[_type];
};

void CKShop::setComboBoxValue(CKObject *_element,const char *_text,const int &value,bool _free)
{
    if(CKFileOptions::Instance().isKidsModeEnabled())
    {
        _element->getChildByTag(CK::Action_KidsMode)->setVisible(true);
        _element->getChildByTag(TOTAL_COST_ID)->setVisible(false);
    }
    else
    {
        _element->getChildByTag(BUTTON_BUY_ID)->setValue(value);
        _element->getChildByTag(DISCOUNT_ID)->setVisible(!_free);
        _element->getChildByTag(TOTAL_COST_ID)->setText(_text);
        _element->getChildByTag(CK::Action_KidsMode)->setVisible(false);
        _element->getChildByTag(19)->setVisible(_free);
        _element->getChildByTag(TOTAL_COST_ID)->setVisible(!_free);
        _element->getChildByTag(ICON_COINS_ID)->setVisible(!_free);
    }
    
}
void CKShop::updateComboBomb(CKObject *_obj)
{
    if(!_obj->isVisible() || *_obj->getValue() ==0)
        return;
    
    free_info = discount_mas;
    best_price = best_price_mas;
    if(getBomb(*_obj->getValue()) != NULL)
    {
        free_info = CKFileInfo::Instance().bomb_info_map[*_obj->getValue()]->free;
    };
    
    char str_cost[NAME_MAX],str_disc[255];
    for(int i = 0; i < price_list.size(); i++)
    {
        int tdiscount = free_info[i]*100;
        
        sprintf(str_cost, "%d",int(CKBonusMgr::Instance().getInfoById(*_obj->getValue())->cost*mas_count[i]*(1.0 - free_info[i])));
        

        bool b_free = false;
        if(_obj->getChildByTag(LABEL_FREE_ID)->isVisible() && CKFileInfo::Instance().bomb_info_map[*_obj->getValue()])
        {
            if(CKFileInfo::Instance().bomb_info_map[*_obj->getValue()]->isFreePack(*price_list[i]->getChildByTag(COUNT_BUY_ID)->getValue()))
            {
                setComboBoxValue(price_list[i],"0",*(_obj->getValue()),true);
                b_free = true;
            }
        };
        if(!b_free)
            setComboBoxValue(price_list[i],str_cost,*(_obj->getValue()),b_free);
        
        sprintf(str_disc,"-%d%s",tdiscount,"%");
        price_list[i]->getChildByTag(DISCOUNT_ID)->setVisible(tdiscount != 0);
        price_list[i]->getChildByTag(DISCOUNT_ID)->getChildByTag(1)->setText(str_disc);
    }
};

void CKShop::reload()
{
    initScroll();
    
    if(m_gui)
        schedule(schedule_selector(CKShop::update), 1.0/60.0);
};

void CKShop::buyBomb(void *_d)
{
    if(!_d)
    {
        CKAudioMng::Instance().playEffect("button_pressed");
        return;
    };
    int count_bomb = *buy_object->getParent()->getChildByTag(COUNT_BUY_ID)->getValue();
    int type_bomb = *buy_object->getValue();
    int total_cost = atoi(buy_object->getParent()->getChildByTag(TOTAL_COST_ID)->getText());
    
    if(total_cost <= CKFileInfo::Instance().getTotalScore())//we have enougth money for buy bomb
    {
        CKAudioMng::Instance().playEffect("buy_bombs");
        CKFileInfo::Instance().addBigScore(-total_cost,false);
        sum_bouth_bomb += count_bomb;//event Flurry variable
        
        int cost = CKBonusMgr::Instance().getInfoById(type_bomb)->cost*count_bomb;
       // float disc = 1.0 - float(total_cost)/float(cost);
        
        CK::StatisticLog::Instance().setBuyBomb(type_bomb, count_bomb, total_cost, total_cost != cost);
        
        if(!CKFileInfo::Instance().addToInventorySelect(type_bomb,count_bomb))
        {
            CKFileInfo::Instance().addToInventory(type_bomb,count_bomb);
        }
        ShopBomb * _shop_bomb = getBomb(type_bomb);
        if(getBomb(type_bomb))
        {
            if(CKFileInfo::Instance().bomb_info_map[type_bomb]->isFreePack(count_bomb))//is free bomb
            {
                CKFileInfo::Instance().bomb_info_map[type_bomb]->eraseFreePack(count_bomb);
                updateValueScroll(show_object);
                updateComboBomb(show_object);
            }
            else //register buy discount pack
            {
                //  ShopBomb::setBuyPack(1);
            };
            
            show_object->getChildByTag(BEST_PRICE_ID)->setVisible(true);
            if(!_shop_bomb->haveFreePack() && _shop_bomb->haveSoldPack())
            {
                (*it)->getChildByTag(BEST_PRICE_ID)->setStateImage(1);
            };
            
            ShopBomb::setBuyPack(1);
        };
        
        //set count bomb in inventory and slots on label
        int count = CKFileInfo::Instance().getCountInInventory(type_bomb);
        count += CKFileInfo::Instance().getCountInInventorySelect(type_bomb);
        char str[NAME_MAX];
        sprintf(str,"%d",count);
        show_object->getChildByTag(LABEL_ININVENTORY_VALUE)->setText(str);
        
        ObjCCalls::sendEventFlurry("shop_buy_bomb",false, "bombs_type=%d,bombs_count=%d",type_bomb,count_bomb);//Flurry
        
        //GA
        ObjCCalls::sendCustomGA(11,"bomb_buy_type",int(type_bomb));
        ObjCCalls::sendCustomGA(12,"bomb_buy_count",int(count_bomb));
        ObjCCalls::sendCustomGA(13,"bomb_buy_cost",total_cost);
        
        updateTotalScoreLabel();
        
        CKFileInfo::Instance().save();
    }
};

void CKShop::enableKidsMode()
{
    m_gui->getChildByTag(CK::Action_Inventory)->setVisible(false);
    top_tap[1]->setVisible(false);
    offset_bombs_tap = top_tap[1]->getPos().y - top_tap[0]->getPos().y;
    top_tap[0]->setPosition(top_tap[1]->getPos().x, top_tap[1]->getPos().y);
    for(int i = 0;i < COUNT_LEFT_TAP;++i)
    {
        left_tap[i]->setPosition(left_tap[i]->getPos().x, left_tap[i]->getPos().y + offset_bombs_tap);
    }
    m_gui->getChildByTag(CK::Action_Shop + 100)->setText(LCZ("Bombs"));
    m_gui->getChildByTag(LABEL_TOTAL_SCORE)->setVisible(false);
};

void CKShop::disableKidsMode()
{
    CKFileOptions::Instance().setKidsMode(false);
    CKFileOptions::Instance().save();
    CKAudioMng::Instance().playEffect("disable_kidsmode");
    
    CKObject * obj = m_gui->getChildByTag(CK::Action_Inventory);
    obj->setVisible(true);
    obj->getNode()->setScale(0.0);
    obj->getNode()->runAction(CCEaseElasticOut::create(CCScaleTo::create(1.0, 1.0)));
    
    obj = m_gui->getChildByTag(LABEL_TOTAL_SCORE);
    obj->setVisible(true);
    obj->getNode()->setScale(0.0);
    obj->getNode()->runAction(CCEaseElasticOut::create(CCScaleTo::create(1.0, 1.0)));
    
    
    obj = m_gui->getChildByTag(CK::Action_KidsMode);
    obj->setVisible(true);
    obj->getNode()->setScale(0.0);
    obj->getNode()->runAction(CCEaseElasticOut::create(CCScaleTo::create(1.0, 1.0)));
    
    top_tap[1]->setVisible(true);
    top_tap[1]->getNode()->setScale(0.0);
    top_tap[1]->getNode()->runAction(CCEaseElasticOut::create(CCScaleTo::create(1.0, 1.0)));
    
    top_tap[0]->getNode()->runAction(CCMoveBy::create(0.7, ccp(0,-offset_bombs_tap)));
    top_tap[0]->savePosition(ccp(top_tap[0]->getPos().x,top_tap[0]->getPos().x - offset_bombs_tap));
    
    for(int i = 0;i < COUNT_LEFT_TAP;++i)
    {
        left_tap[i]->getNode()->runAction(CCMoveBy::create(0.7, ccp(0,-offset_bombs_tap)));
        left_tap[i]->savePosition(ccp(left_tap[i]->getPos().x, left_tap[i]->getPos().y - offset_bombs_tap));
    }
    m_gui->getChildByTag(CK::Action_Shop + 100)->setText(LCZ("Shop"));

    it = active_object_list.begin();
    while (it != active_object_list.end()) {
        updateValueScroll(*it);
        it++;
    };
    
    if(show_object)
        updateComboBomb(show_object);
    
    m_dialog->get()->setVisible(false);
    m_dialog->getChildByTag(CK::Action_Yes)->setStateImage(0);
    show_button_km_inventory = true;
};


#pragma mark - CALLBACK

void CKShop::callFilter(CCNode* _sender,void *_value)
{
    if(enable_animation)
        CKAudioMng::Instance().playEffect("button_pressed");
    if(show_object)
        hideBuyObjectInfo(show_object);
    m_bomb->m_scroll->getNode()->setPosition(ccp(m_bomb->posX,m_bomb->top_pos));
    m_bomb->m_scroll->savePosition(m_bomb->m_scroll->getPos());
    sort(*static_cast<int*>(_value));
};

void CKShop::tweetCallBack(CCNode *_node,int *_value)
{
    CCLOG("CKShop::tweetCallBack(CCNode *_node,int *_value)");
    if (!CKFileInfo::Instance().isFollowOur())
    {
        CK::StatisticLog::Instance().setNewPage(CK::Page_LikeComplite);
        CKAudioMng::Instance().playEffect("new_free_coins");
        char str[NAME_MAX];
        sprintf(str, LCZ("You earn %d coins."),1000);
        CKHelper::Instance().getDialog()->setTarget(this);
        CKHelper::Instance().getDialog()->setInfoText(8,LCZ("Thanks!"),str);
        CKHelper::Instance().getDialog()->showMessage(8,callfuncND_selector(CKShop::menuCall));
        CKFileInfo::Instance().addBigScore(1000,true);
        CKFileInfo::Instance().setFollowOur(true);
        CKFileInfo::Instance().save();
        updateTotalScoreLabel();
    }
};

void CKShop::callCoins(CCNode *_node,void *_data)
{
    switch(*static_cast<int*>(_data))
    {
        case CK::Action_Facebook:
            CKAudioMng::Instance().playEffect("button_pressed");
#if !DISABLE_FACEBOOK
            CK::StatisticLog::Instance().setLink(CK::Link_StigolLike);
            CKHelper::Instance().FB_need_check_likes_main = false;
            if (!CKFileInfo::Instance().isLikesOur()) {
                CKHelper::Instance().FB_need_check_likes_main = true;
            };
            CKFacebookHelper::Instance().FB_goToPageLikeOurPage();
#endif
            break;
        case CK::Action_Tweet:
            CKAudioMng::Instance().playEffect("button_pressed");
#if !DISABLE_TWITTER
            CK::StatisticLog::Instance().setLink(CK::Link_StigolFollow);
            CKHelper::Instance().TW_need_ckeck_follow = false;
            if (!CKFileInfo::Instance().isFollowOur()) {
                CKHelper::Instance().TW_need_ckeck_follow = true;
            }
            CKTwitterHelper::Instance().goToTwitter(TWEET_FOLLOW);
#endif
            break;
    };
};

void CKShop::callShowCoint(void *_d)
{
    CKFileInfo::Instance().genDiscountBombAllPeriodAndSave();
    CKAudioMng::Instance().playEffect("button_pressed");
    if(!_d) //result dialog = 0 "NO"
        return;
    
    if(show_object)
        hideBuyObjectInfo(show_object);
    
    int d = 2;
    callTopTap(NULL, &d);
    int t = 0;
    callFilterCoins(NULL,&t);
};

void CKShop::callTopTap(CCNode* _sender,void *_value)
{
    if(_sender)
    CKAudioMng::Instance().playEffect("button_pressed");
    
    if(*static_cast<int*>(_value) == 2)//coins
    {
        m_bomb->start_anim = false;
        top_tap[1]->setStateImage(1);
        top_tap[0]->setStateImage(0);
        
        if(show_object)
            hideBuyObjectInfo(show_object);
        
        m_coins->m_scroll->setVisible(true);
        m_coins->m_scroll->setPosition(m_coins->m_scroll->getPos().x,m_coins->top_pos);
        m_bomb->m_scroll->setVisible(false);
        
        top_tap[0]->getNode()->runAction(CCMoveTo::create(0.25,  ccp(bomb_tap_pos.x,bomb_tap_pos.y - win_size.height*0.25)));
        for(int i = 0;i < COUNT_LEFT_TAP;++i) {
            
            left_tap[i]->getNode()->runAction(CCSequence::create(CCMoveTo::create(0.25, ccp(bomb_tap_pos.x,bomb_tap_pos.y - win_size.height*0.25)),CCScaleTo::create(0.1, 0.5),NULL));
            left_tap[i]->setTouch(false);
        }
        
        float koef = (CKHelper::Instance().getIsIpad())?1.0:0.4;
        
        for (int i = 0; i < COUNT_COINS_TAP; ++i) {
            if(!coins_tap[i]->isTouch())
            {
                coins_tap[i]->getNode()->runAction(CCSequence::create(CCScaleTo::create(0.1, 1.0),CCMoveBy::create(0.2, ccp(0,-i*70*koef-64*koef)),NULL));
                coins_tap[i]->setTouch(true);
            }
        }
    }
    else // bombs tap
    {
        if(CKFileOptions::Instance().isKidsModeEnabled())
            return;
        m_coins->start_anim = false;
    
        top_tap[0]->getNode()->runAction(CCMoveTo::create(0.3, bomb_tap_pos));
        float koef = (CKHelper::Instance().getIsIpad())?1.0:0.4;
        for(int i = 0;i < COUNT_LEFT_TAP;++i)
        {
            if(!left_tap[i]->isTouch())
            {
                
                left_tap[i]->getNode()->runAction(CCSequence::create(CCScaleTo::create(0.1, 1.0),CCMoveBy::create(0.2, ccp(0,win_size.height*0.25-i*70*koef-64*koef)),NULL));
                left_tap[i]->setTouch(true);
            }
        };
        
        for (int i = 0; i < COUNT_COINS_TAP; ++i) {
            if(coins_tap[i]->isTouch())
            {
                if(enable_animation)
                {
                    coins_tap[i]->getNode()->runAction(CCSequence::create(CCMoveTo::create(0.25, ccp(bomb_tap_pos.x,top_tap[1]->getPos().y)),CCScaleTo::create(0.1, 0.5),NULL));
                }
                else
                {
                    coins_tap[i]->setPosition(bomb_tap_pos.x,top_tap[1]->getPos().y);
                    coins_tap[i]->getNode()->setScale(0.5);
                }
                coins_tap[i]->setTouch(false);
            }
        }
        
        top_tap[1]->setStateImage(0);
        top_tap[0]->setStateImage(1);
        m_bomb->m_scroll->setVisible(true);
        m_coins->m_scroll->setVisible(false);
    };
};

void CKShop::callFilterCoins(CCNode* _sender,void *_value)
{
    if(enable_animation)
        CKAudioMng::Instance().playEffect("run_scroll");
    
    m_coins->start_anim = false;
    switch(*static_cast<int*>(_value)) {
        case 0:
            
            if(enable_animation)
            {
                m_coins->m_scroll->getNode()->runAction(CCMoveTo::create(0.2,ccp(m_coins->posX,m_coins->top_pos)));
                m_coins->m_scroll->savePosition(ccp(m_coins->posX,m_coins->top_pos));
            }
            else
            {
                m_coins->m_scroll->setPosition(m_coins->posX,m_coins->top_pos);
            }
            break;
        case 1:
            if(enable_animation)
            {
                m_coins->m_scroll->getNode()->runAction(CCMoveTo::create(0.2,ccp(m_coins->posX,m_coins->top_pos + height_buy_coins)));
                m_coins->m_scroll->savePosition(ccp(m_coins->posX,m_coins->top_pos+height_buy_coins));
            }
            else
            {
                m_coins->m_scroll->setPosition(m_coins->posX,m_coins->top_pos+height_buy_coins);
            }
            break;
        case 2:
            if(enable_animation)
            {
                m_coins->m_scroll->getNode()->runAction(CCMoveTo::create(0.2,ccp(m_coins->posX,m_coins->bottom_pos-m_coins->m_scroll->getChildByTag(5)->getPos().y)));
                m_coins->m_scroll->savePosition(ccp(m_coins->posX,m_coins->bottom_pos-m_coins->m_scroll->getChildByTag(5)->getPos().y));
            }
            else
            {
                m_coins->m_scroll->setPosition(m_coins->posX,m_coins->bottom_pos-m_coins->m_scroll->getChildByTag(5)->getPos().y);
            }
            break;
        default:
            break;
    }
};

void CKShop::callHint(CCNode* _sender,void *_value)
{
    CKAudioMng::Instance().playEffect("button_pressed");
    if(DISABLE_HINTS)
        return;
    CKHelper::Instance().getTutorial()->activeted(this,CK::SCENE_SHOP);
    int type_bomb = *static_cast<int*>(_value);
    CKHelper::Instance().getTutorial()->start(3,type_bomb);
};

void CKShop::callBuyIAP(CCNode * _sender,void *_value)
{
    
    CKAudioMng::Instance().playEffect("button_pressed");
    CCLOG("CKShop::callBuyIAP %d",*static_cast<int*>(_value));
    showDialogWait();
    int n = *static_cast<int*>(_value);
    click_buyIapp = *static_cast<int*>(_value);
    if(n >= 100 && n < 10000) //multipliers
    {
        if(n == 200)
        {
            ObjCCalls::IAPHelper_buy("com.cc.coins.x2");
        }else if(n == 300)
        {
            ObjCCalls::IAPHelper_buy("com.cc.coins.x3");
        }else if(n == 500)
        {
            ObjCCalls::IAPHelper_buy("com.cc.coins.x5");
        };
        return;
    }else if(n >= 10000)
    {
        if(*static_cast<int*>(_value) == 10000)
        {
            ObjCCalls::IAPHelper_buy("com.cc.coins.buy10000");
        }
        else if(*static_cast<int*>(_value) == 20000)
        {
            ObjCCalls::IAPHelper_buy("com.cc.coins.buy20000");
        }
        else if(*static_cast<int*>(_value) == 50000)
        {
            ObjCCalls::IAPHelper_buy("com.cc.coins.buy50000");
        }
        else if(*static_cast<int*>(_value) == 80000)
        {
            ObjCCalls::IAPHelper_buy("com.cc.coins.buy80000");
        }
        else if(*static_cast<int*>(_value) == 100000)
        {
            ObjCCalls::IAPHelper_buy("com.cc.coins.buy100000");
        }
        else if(*static_cast<int*>(_value) == 200000)
        {
            ObjCCalls::IAPHelper_buy("com.cc.coins.buy200000");
        }
        else if(*static_cast<int*>(_value) == 300000)
        {
            ObjCCalls::IAPHelper_buy("com.cc.coins.buy300000");
        }
        else if(*static_cast<int*>(_value) == 500000)
        {
            ObjCCalls::IAPHelper_buy("com.cc.coins.buy500000");
        }
        else if(*static_cast<int*>(_value) == 999999)
        {
            ObjCCalls::IAPHelper_buy("com.cc.coins.buy1million");
        }
        return;
    };
    
    switch (*static_cast<int*>(_value)) {
        case CK::Action_Buy_KidsMode:
            ObjCCalls::IAPHelper_buy("com.cc.kids_mode");
            break;
        case CK::Action_Buy_AdsRemoving:
            ObjCCalls::IAPHelper_buy("com.cc.ads_removing");
            break;
        case CK::Action_Buy_Restore:
            
            ObjCCalls::IAPHelper_restore();
            break;
        default:
            
            break;
    };
    
};

void CKShop::menuCall(CCNode * _sender,void *_value)
{
    CKAudioMng::Instance().playEffect("button_pressed");
    switch (*static_cast<int*>(_value)) {
        case CK::Action_KidsMode:
        {
            show_button_km_inventory = false;
            CKFileOptions::Instance().setKidsMode(true);
            CKFileOptions::Instance().save();
            //enableKidsMode();
            //m_gui->getChildByTag(CK::Action_KidsMode)->setVisible(false);
            //break;
        }
        case CK::Action_Back:
        {
            ObjCCalls::sendEventFlurry("shop_exit",false, "bombs_bought=%d",sum_bouth_bomb);//Flurry
            ObjCCalls::sendCustomGA(10,"num_purchased_bombs",sum_bouth_bomb);//GA
            
            CCDirector::sharedDirector()->pause();
            
            CCLOG("previous_scene_id %d %d",CKHelper::Instance().getLoadingScene()->getPreviourSceneID(),CKHelper::Instance().getLoadingScene()->getCurrentSceneID());

            if(CKHelper::Instance().getLoadingScene()->getPreviourSceneID() == CK::SCENE_INVENTORY)
            {
                CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
                CKHelper::Instance().playScene(CK::SCENE_INVENTORY);
            }else if(CKHelper::Instance().getLoadingScene()->getPreviourSceneID() == CK::SCENE_OPTION)
            {
                CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
                CKHelper::Instance().playScene(CK::SCENE_OPTION);
            }
            else
            {
                CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
                CKHelper::Instance().playScene(CKHelper::Instance().menu_number);
            }
            

            CCDirector::sharedDirector()->resume();
            this->setVisible(false);
            this->setTouchEnabled(false);
        }
            
            break;
        case CK::Action_Inventory:
        {
            ObjCCalls::sendEventFlurry("shop_exit",false, "bombs_bought=%d",sum_bouth_bomb);//Flurry
            ObjCCalls::sendCustomGA(10,"num_purchased_bombs",sum_bouth_bomb);//GA
            
            CKHelper::Instance().show_inventory_km_button = show_button_km_inventory;
            
            unschedule(schedule_selector(CKShop::update));
            this->setVisible(false);
            this->setTouchEnabled(false);
            // CCScene *pScene = CKInventory::scene();
            // CCDirector::sharedDirector()->replaceScene(pScene);
            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(CK::SCENE_INVENTORY);
            break;
        };
        case CK::Action_Ok:
        {
            CK::StatisticLog::Instance().setNewPage(CK::Page_ShopCoins);
        };
        default:
            break;
    };
};

void CKShop::callbackIAP(CCNode * _sender,void *_value)
{
    char *str = (char *)_value;
    CCLOG("CKShop::callbackIAP %s,%s",(char *)_value,strstr(str, "com.cc.coins.buy"));
    hideDialogWait();
    
    if(click_buyIapp == CK::Action_Buy_Restore)
    {
        CK::StatisticLog::Instance().setBuyinApp("restore", CKFileInfo::Instance().getTotalScore());
    }
    else if(strcmp(str, "end") != 0)
    {
        CK::StatisticLog::Instance().setBuyinApp(str, CKFileInfo::Instance().getTotalScore());
    }
    
    if(strstr(str, "com.cc.coins.buy") != NULL)
    {
        
        std::string tmp_str = str;
        int nPosRight  = std::string("com.cc.coins.buy").length();
        std::string str_value = tmp_str.substr(nPosRight, tmp_str.length() - nPosRight);
        CCLOG("str_value %s",str_value.c_str());
        
        CKAudioMng::Instance().playEffect("buy_coins");
        
        if(strcmp(str_value.c_str(), "1million") == 0)
        {
            CKFileInfo::Instance().addBigScore(999999, true);
        }
        else
        {
            CKFileInfo::Instance().addBigScore(atoi(str_value.c_str()), true);
        }
        updateTotalScoreLabel();
        CKFileInfo::Instance().save();
        return;
    }else if (strstr(str, "com.cc.coins.x") != NULL)
    {
        
        std::string tmp_str = str;
        int nPosRight  = std::string("com.cc.coins.x").length();
        std::string str_value = tmp_str.substr(nPosRight, tmp_str.length() - nPosRight);
        CCLOG("str_value %s",str_value.c_str());
        
        int vl = atoi(str_value.c_str());
        
        
        CKFileInfo::Instance().setMultiBuyKoef(vl);
        
        long dd = time(0);
        dd += (24*60*60 - 60);//one day
        
        CKFileInfo::Instance().setMultiBuyData(mktime(localtime(&dd)));
        
        updateMultiCoins();
        calcScrollCoinsSize();
        
        CKFileInfo::Instance().save();
    };
    
    if(strcmp(str, "com.cc.ads_removing") == 0)
    {
        updateAdsRemove();
    }else if(strcmp(str, "com.cc.kids_mode") == 0)
    {
        CKObject *kids_obj = m_coins->m_scroll->getChildByTag(4)->getChildByTag(1);
        kids_obj->getChildByTag(CK::Action_Buy_KidsMode)->setTouch(false);
        kids_obj->getChildByTag(CK::Action_Buy_KidsMode + 2)->setVisible(false);//label price
        kids_obj->getChildByTag(12)->setVisible(true);//picture sold
        
    };
};

void CKShop::callDialog(CCNode *_sender,void *_value)
{
    switch(*static_cast<int*>(_value))
    {
        case CK::Action_Yes:
            break;
        case CK::Action_No:
        case CK::Action_Close:
            m_dialog->get()->setVisible(false);
            CKAudioMng::Instance().playEffect("new_free_coins");
            break;
    };
};

#pragma mark - TOUCH
void CKShop::ccTouchesBegan(cocos2d::CCSet* pTouches, cocos2d::CCEvent* event)
{
    
    if(!this->isVisible())
        return;
    
    CCTouch* touch =(CCTouch*)*pTouches->begin();
    if(touch->getID() != 0)//first touch
        return;
    
    location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    
    if(m_dialog && m_dialog->get()->isVisible())
    {
        m_dialog->setTouchBegan(location);
        if (m_dialog->getCurrnet() && m_dialog->getCurrnet()->getTag() == CK::Action_Yes) {
            CKAudioMng::Instance().playEffect("button_pressed");
            schedule(schedule_selector(CKShop::timeDisableKM),CK::KM_TIME_RESET_GAME,0, CK::KM_TIME_RESET_GAME);
        };
        return;
    };
    
    prev_location = location;
    last_point = location;
    buy_object = NULL;
    if(m_gui && m_gui->getChildByTag(-1))
    {
        if(m_gui->getChildByTag(-1)->isPointInObject(location))//touch point in touch zone scroll
        {
            if (m_bomb->m_scroll->isVisible()) { //active scroll bombs
                touch_scroll = true;

                m_bomb->m_scroll->setTouch(true);
                m_bomb->start_anim = false;
                curent_object =  m_gui->getTouchObject(location);
                CCLOG("ccTouchesBegan:: getPos%f",m_bomb->m_scroll->getPos().y);
                
                if(!show_object)
                {
                    m_bomb->max_pos = count_shop_elements + m_bomb->min_pos - ((!show_object)?COUNT_BUY_BUTTON:0);
                    m_bomb->bottom_pos = m_bomb->max_pos*m_bomb->frame + m_bomb->height;
                }
                m_bomb->m_scroll->setTouch(false);
                
                if(curent_object)
                {
                    CCLOG("ccTouchesBegan:: curent_object->getTag() %d",curent_object->getTag());
                    if(curent_object->getTag() == BUTTON_BUY_ID)
                    {
                        buy_object = curent_object;
                        buy_object->setStateImage(1);
                        curent_object = NULL;
                    }
                    else if(curent_object->getType() == CK::GuiButton)
                    {
                        curent_object->setStateImage(1);
                    }
                }
            }
            if(m_coins->m_scroll->isVisible())
            {
                m_coins->m_scroll->setTouch(true);
                curent_object =  m_gui->getTouchObject2(location);
                if(curent_object)
                {
                    CCLOG("ccTouchesBegan:: curent_object->getTag() %d",curent_object->getTag());
                    if(curent_object->getType() == CK::GuiButton)
                    {
                        curent_object->setStateImage(1);
                    }
                }
                m_coins->m_scroll->setTouch(false);
                touch_scroll = true;
                m_coins->start_anim = false;
            }
        }
    }
    m_gui->setTouchBegan(location);
};



void CKShop::ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent)
{
    CCTouch* touch =(CCTouch*)*pTouches->begin();
    if(touch->getID() != 0)//first touch
        return;
    CCLOG("CKShop::ccTouchesMoved %d",touch->getID());
    prev_location = location;
    location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    if(touch_scroll)
    {
        if(curent_object && curent_object->getType() == CK::GuiButton)
        {
            curent_object->setStateImage(0);
        }
        curent_object = NULL;
        if (m_bomb->m_scroll->isVisible()) {
            m_bomb->start_anim = false;
            if(buy_object)
            {
                buy_object->setStateImage(0);
                buy_object = NULL;
            };
            if(show_object)
            {
                scrollCombo(location);
            }
            else
            {
                float moved = location.y - last_point.y;
                b_touchmove = true;
                if((m_bomb->m_scroll->getPos().y - m_bomb->top_pos) < 0 && moved < 0)
                    moved *= fabs( 1.0 - MIN(1.0,fabs(m_bomb->m_scroll->getPos().y - m_bomb->top_pos)/(m_bomb->frame*5)));
                
                if((m_bomb->m_scroll->getPos().y - m_bomb->bottom_pos) > 0 && moved > 0)
                    moved *= fabs( 1.0 - MIN(1.0,fabs(m_bomb->m_scroll->getPos().y - m_bomb->bottom_pos)/(m_bomb->frame*5)));
                
                m_bomb->m_scroll->moveY(moved);
            }
        };
        if(m_coins->m_scroll->isVisible())
        {
            float moved = location.y - last_point.y;
           // CCLOG("m_coins->m_scroll->isVisible() %f",m_coins->m_scroll->getPos().y);
            b_touchmove = true;
            m_coins->start_anim = false;
            
            if((m_coins->m_scroll->getPos().y - m_coins->top_pos) < 0 && moved < 0)
                moved *= fabs( 1.0 - MIN(1.0,fabs(m_coins->m_scroll->getPos().y - m_coins->top_pos)/(m_bomb->frame*5)));
            
            if((m_coins->m_scroll->getPos().y - m_coins->bottom_pos) > 0 && moved > 0)
                moved *= fabs( 1.0 - MIN(1.0,fabs(m_coins->m_scroll->getPos().y - m_coins->bottom_pos)/(m_bomb->frame*5)));
            m_coins->m_scroll->moveY(moved);
        }
    }
    m_gui->setTouchMove(location);
};
void CKShop::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
{
    CCTouch* touch =(CCTouch*)*pTouches->begin();
    if(touch->getID() != 0)//first touch
        return;
    
    location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    
    if(m_dialog && m_dialog->get()->isVisible())
    {
        unschedule(schedule_selector(CKShop::timeDisableKM));
        m_dialog->setTouchEnd(location);
        return;
    };
    
    if(touch_scroll)
    {
        touch_scroll = false;
       
//        if(b_touchmove)
//            CKAudioMng::Instance().playEffect("run_scroll");
        if(m_bomb->m_scroll->isVisible())
        {
            next_show = NULL;
            if(b_touchmove)
                m_bomb->start_anim = true;
            
            b_touchmove = false;
            anim_size =  (prev_location.y - location.y)*1.0;
            
            touchEndBombsScroll();
        };
        if(m_coins->m_scroll->isVisible())
        {
            if(curent_object && curent_object->getType() == CK::GuiButton)
            {
                m_gui->runFuncObject(curent_object);
                curent_object->setStateImage(0);
            }
            curent_object = NULL;
            if(b_touchmove)
                m_coins->start_anim = true;
            
            b_touchmove = false;
            anim_size =  (prev_location.y - location.y)*1.0;
            
            if((m_coins->m_scroll->getPos().y - m_coins->top_pos) < 0)
            {
                m_coins->m_scroll->getNode()->runAction(CCMoveTo::create(0.1,ccp(m_coins->posX,m_coins->top_pos) ));
                m_coins->start_anim = false;
                m_coins->m_scroll->savePosition(ccp(m_coins->posX,m_coins->top_pos));
            }
            else
                if((m_coins->m_scroll->getPos().y - m_coins->bottom_pos - size_combo) > 0)
                {
                    m_coins->start_anim = false;
                    m_coins->m_scroll->getNode()->runAction(CCMoveTo::create(0.1,ccp(m_coins->posX,m_coins->bottom_pos)));
                    m_coins->m_scroll->savePosition(ccp(m_coins->posX,m_coins->bottom_pos));
                }
                else
                {
                    m_coins->m_scroll->savePosition(m_coins->m_scroll->getPos());
                }
            
        }
    };
    m_gui->setTouchEnd(location);
};
void CKShop::touchEndBombsScroll()
{

    returnScrollPosEndTouch();
    
    if(buy_object)
    {
        buy_object->setStateImage(0);
        CKAudioMng::Instance().playEffect("button_pressed");
        //m_bomb->m_scroll->setTouch(true);
        m_bomb->m_scroll->setTouch(false);
        showDialogBuy();
    }
    else
        if(curent_object)//check is touch buy object
        {
            
            m_bomb->m_scroll->setTouch(true);
            if(curent_object == m_gui->getTouchObject(location) && curent_object->getTag() != 22)
            {
                CCLOG("ccTouchesEnded:: tmp %d tag %d ",curent_object->getType(),curent_object->getTag());
                if(show_object)
                {
                    if(show_object != curent_object && curent_object->getType() == CK::GuiButton && curent_object->getTag() < 2000)
                    {
                        next_show = curent_object;
                    };
                    CKAudioMng::Instance().playEffect("run_scroll");
                    hideBuyObjectInfo(show_object);
                }
                else
                {
                    if(curent_object->getType() == CK::GuiButton) // show buy combo only touch in bomb
                    {
                        showBuyObjectInfo(curent_object);
                        CKAudioMng::Instance().playEffect("run_scroll");
                    }
                }
                curent_object = NULL;
            }
            else
            {
                if(curent_object->getType() == CK::GuiButton)
                {
                    curent_object->setStateImage(0);
                    callHint(curent_object->getNode(),curent_object->getValue());
                }
            }
            m_bomb->m_scroll->setTouch(false);
        };
};



void CKShop::scrollCombo(CCPoint &_p)
{
    size_combo -= _p.y - last_point.y;
    
    b_touchmove = true;
    if(size_combo < 0)
    {
        size_combo = 0;
        hideBuyObjectInfo(show_object);
        show_object = NULL;
    }
    else
    {
        float size_m = max_size_combo;
        if(size_combo > size_m)
        {
            _p.y += (size_combo - size_m);
            size_combo = size_m;
        };
        
        updatePositionDownObject(_p.y - last_point.y);
    }
    last_point = _p;
};

void CKShop::returnScrollPosEndTouch()
{
    if((m_bomb->m_scroll->getPos().y - m_bomb->top_pos) < 0)
    {
        m_bomb->m_scroll->getNode()->runAction(CCMoveTo::create(0.1,ccp(m_bomb->posX,m_bomb->top_pos) ));
        m_bomb->start_anim = false;
        m_bomb->m_scroll->savePosition(ccp(m_bomb->posX,m_bomb->top_pos));
    }
    else
        if((m_bomb->m_scroll->getPos().y - m_bomb->bottom_pos - size_combo) > 0)
        {
            m_bomb->start_anim = false;
            if(!show_object)
            {
                m_bomb->m_scroll->getNode()->runAction(CCMoveTo::create(0.1,ccp(m_bomb->posX,m_bomb->bottom_pos) ));
                m_bomb->m_scroll->savePosition(ccp(m_bomb->posX,m_bomb->bottom_pos));
            }
            else
            {
                if(size_combo < m_bomb->frame/2)
                {
                    hideBuyObjectInfo(show_object);
                    // return;
                };
                alignForLastObject();
            }
        }
        else
        {
            if(size_combo)
                if(size_combo < m_bomb->frame/2)
                {
                    hideBuyObjectInfo(show_object);
                }
            m_bomb->m_scroll->savePosition(m_bomb->m_scroll->getPos());
        }
};

void CKShop::showDialogBuy()
{
    if (CKFileOptions::Instance().isKidsModeEnabled()) {
        m_dialog->get()->setVisible(true);
        return;
    };
    
    CCLOG("showDialogBuy:: %d %d",atoi(buy_object->getParent()->getChildByTag(TOTAL_COST_ID)->getText()),CKFileInfo::Instance().getTotalScore());
    if(atoi(buy_object->getParent()->getChildByTag(TOTAL_COST_ID)->getText()) > CKFileInfo::Instance().getTotalScore())//we don't have enougth money for buy bomb
    {
        CKHelper::Instance().getDialog()->setTarget(this);
        CKHelper::Instance().getDialog()->setConfirmText(LCZ("You don't have enougth money. Buy money?"));
        CKHelper::Instance().getDialog()->show(callfuncN_selector(CKShop::callShowCoint));
        buy_object->setStateImage(0);
    }
    else{
        CKHelper::Instance().getDialog()->setTarget(this);
        CKHelper::Instance().getDialog()->setConfirmText(LCZ("Buy this bombs?"));
        CKHelper::Instance().getDialog()->show(callfuncN_selector(CKShop::buyBomb));
        
        int count_bomb = *buy_object->getParent()->getChildByTag(COUNT_BUY_ID)->getValue();
        int type_bomb = *buy_object->getValue();
        char str[255];
        sprintf(str, "%d",type_bomb);
        
        CK::StatisticLog::Instance().setTryBuy(str, count_bomb);
        buy_object->setStateImage(0);
    }
};

void CKShop::updateVisibleActiveList(const int &_num)
{
    if(prev_num != _num)
    {
        // return;
        int tmin = -3;
        int tmax = 4;
        if(prev_num < _num)
            tmax++;
        else
            tmin--;
        for (int i = MAX(0,_num + tmin); i < MIN(active_object_list.size(),(_num + tmax)); i++) {
            if(i > (_num + tmin) && i < (_num + tmax))
            {
                active_object_list[i]->setVisible(true);
                //CCLOG("active_object_list %d %d",active_object_list.size(),CKHelper::Instance().getCountVisibleChildren( active_object_list[i]->getNode()));
            }
        }
        if(prev_num < _num)
        {
            if(_num < active_object_list.size())
            {
                if((_num + tmin) > 0)
                    active_object_list[_num + tmin]->setVisible(false);
                if((_num + tmin-1) > 0)
                    active_object_list[_num + tmin -1]->setVisible(false);
            }
        }
        else
        {
            if((_num + tmax) < active_object_list.size())
                active_object_list[_num + tmax]->setVisible(false);
            if((_num + tmax + 1) < active_object_list.size())
                active_object_list[_num + tmax + 1]->setVisible(false);
        }
        //CCLOG("active_object_list %d",active_object_list.size());
    }
    
    prev_num = _num;
};

bool CKShop::updatePositionDownObject(float _value)
{
    if(size_combo <= max_size_combo)
    {
        //  CCLOG("updatePositionDownObject:: %f",size_combo/max_size_combo);
        std::vector<CKObject*>::iterator t_it = findObjectInShop(show_object);
        t_it++;
        int size = price_list.size();
        for(int i = 0; i < size; i++)
        {
            price_list[i]->setPosition(price_list[i]->getPos().x, price_list[i]->getPos().y + _value);
        };
        if((size_combo/max_size_combo) < 0.73)
        {
            price_list[0]->setVisible(false);
        }
        else
        {
            price_list[0]->setVisible(true);
        };
        
        if((size_combo/max_size_combo) < 0.48)
        {
            price_list[1]->setVisible(false);
        }
        else
        {
            price_list[1]->setVisible(true);
        };
        
        if((size_combo/max_size_combo) < 0.23)
        {
            price_list[2]->setVisible(false);
        }
        else
        {
            price_list[2]->setVisible(true);
        };
        
        while (t_it!= active_object_list.end()) {
            if((*t_it)->isVisible())
                (*t_it)->getNode()->setPosition(ccp((*t_it)->getPos().x, (*t_it)->getPos().y + _value));
            t_it++;
        }
        return true;
    }
    else
    {
        size_combo =  max_size_combo;
        return false;
    }
    return false;
};

bool CKShop::alignForLastObject()
{
    if(show_object != last_object)
    {
        if( (last_object->getPos().y + m_bomb->m_scroll->getPos().y) > m_bomb->frame )
        {
            m_bomb->m_scroll->getNode()->runAction(CCMoveTo::create(0.2,ccp(m_bomb->posX,-last_object->getPos().y + m_bomb->frame) ));
            m_bomb->m_scroll->savePosition(ccp(m_bomb->posX,-last_object->getPos().y + m_bomb->frame));
            return true;
        }
    }
    else
    {
        if( (last_object->getPos().y + m_bomb->m_scroll->getPos().y) > m_bomb->frame )
        {
            m_bomb->m_scroll->getNode()->runAction(CCMoveTo::create(0.2,ccp(m_bomb->posX,-last_object->getPos().y + m_bomb->frame + size_combo) ));
            m_bomb->m_scroll->savePosition(ccp(m_bomb->posX,-last_object->getPos().y + m_bomb->frame + size_combo));
            return true;
        }
    }
    return false;
};
void CKShop::timeDisableKM()
{
    m_dialog->get()->setVisible(false);
    disableKidsMode();
};
void CKShop::update(const float &dt)
{
    if(!m_gui)
        return;
    if(m_bomb->m_scroll->isVisible())
    {
        updateVisibleActiveList(int(m_bomb->m_scroll->getPos().y/m_bomb->frame) - m_bomb->min_pos);
        if(m_bomb->start_anim)
        {
            if(show_object)
            {
                // return;
                anim_size *= 0.85;
                size_combo += anim_size;
                
                if(fabs(anim_size) < 1.0)
                {
                    m_bomb->start_anim = false;
                    if(alignForLastObject())
                        return;
                    
                };
                if(size_combo < m_bomb->frame*0.5)
                {
                    if( (last_object->getPos().y + m_bomb->m_scroll->getPos().y) > 0 )
                    {
                        m_bomb->start_anim = false;
                    };
                    if(size_combo < 0.0)
                        size_combo = 0.0;
                    hideBuyObjectInfo(show_object);
                    return;
                }
                else
                {
                    if(size_combo > max_size_combo)
                    {
                        anim_size -= (size_combo - max_size_combo);
                        size_combo = max_size_combo;
                    }
                    
                    updatePositionDownObject(-anim_size);
                }
                
            }
            else
            {
                anim_size *= 0.92;
                
                if((m_bomb->m_scroll->getPos().y - m_bomb->top_pos) < 0 && anim_size > 0)
                {
                    
                    m_bomb->m_scroll->getNode()->runAction(CCMoveTo::create(0.2,ccp(m_bomb->posX,m_bomb->top_pos) ));
                    m_bomb->m_scroll->savePosition(ccp(m_bomb->posX,m_bomb->top_pos));
                    anim_size = 0;
                }
                
                if((m_bomb->m_scroll->getPos().y - m_bomb->bottom_pos) > 0 && anim_size < 0)
                    anim_size = 0;
                
                //  CCLOG("update:: anim_size %f ",anim_size);
                if(fabs(anim_size) < 1.0)
                    m_bomb->start_anim = false;
                m_bomb->m_scroll->moveY(-anim_size);
                m_bomb->m_scroll->savePosition(m_bomb->m_scroll->getPos());
                alignForLastObject();
            }
        };
    };
    if(m_coins->m_scroll->isVisible())
    {
        if(m_coins->start_anim)
        {
                anim_size *= 0.92;
                
                if((m_coins->m_scroll->getPos().y - m_coins->top_pos) < 0 && anim_size > 0)
                {
                    
                    m_coins->m_scroll->getNode()->runAction(CCMoveTo::create(0.2,ccp(m_coins->posX,m_coins->top_pos) ));
                    m_coins->m_scroll->savePosition(ccp(m_coins->posX,m_coins->top_pos));
                    anim_size = 0;
                }
                
                if((m_coins->m_scroll->getPos().y - m_coins->bottom_pos) > 0 && anim_size < 0)
                    anim_size = 0;
                
                //  CCLOG("update:: anim_size %f ",anim_size);
                if(fabs(anim_size) < 1.0)
                    m_coins->start_anim = false;
                m_coins->m_scroll->moveY(-anim_size);
                m_coins->m_scroll->savePosition(m_coins->m_scroll->getPos());
        };
    };
};

void CKShop::hideNode(CCNode *_obj)
{
    static_cast<CCNode *>(_obj)->setVisible(false);
};

void CKShop::initDiscount(const unsigned char _type)
{
    free_info = discount_mas;
    best_price = best_price_mas;
    if(CKFileInfo::Instance().bomb_info_map[_type] != NULL)
    {
        //  best_price = CKFileInfo::Instance().bomb_info_map[_type]->best_price;
        free_info = CKFileInfo::Instance().bomb_info_map[_type]->free;
    };
    
    return;
};

void CKShop::showBuyObjectInfo(CKObject *_object,bool _anim)
{
    if(!_object || !_object->isVisible())
        return;
    
    
    show_object = _object;
    
    float diff = m_bomb->m_scroll->getPos().y + _object->getPos().y + _object->getNode()->getContentSize().height/2  - m_bomb->frame*(price_list.size() + 0);
    
    float time = TIME_ANIMATION_BUY*price_list.size();//animation buy button
    show_object->setUserData(*findObjectInShop(_object));
    
    updateComboBomb(_object);
    size_combo = max_size_combo = 0.0;
    
    float size_f = _object->getNode()->getContentSize().height/2 + price_list[0]->getNode()->getContentSize().height/2;
    
    for(int i = 0; i < price_list.size(); i++)
    {
        price_list[i]->getNode()->stopAllActions();
        price_list[i]->setPosition(_object->getPos().x, _object->getPos().y);
        price_list[i]->setVisible(true);
        
        CCPoint new_pos = ccp(_object->getPos().x, _object->getPos().y -  size_f - price_list[i]->getNode()->getContentSize().height*i);
        if(_anim)
        {
            CCAction *move = CCMoveTo::create(TIME_ANIMATION_BUY + TIME_ANIMATION_BUY*i, new_pos);
            price_list[i]->getNode()->runAction(move);
        }
        else
        {
            price_list[i]->getNode()->setPosition(new_pos);
        }
        
        max_size_combo += price_list[i]->getNode()->getContentSize().height;
    };
    size_combo = max_size_combo;
    
    //animation down scroll object
    
    std::vector<CKObject*>::iterator it3 = findObjectInShop(_object);
    if(it3 != active_object_list.end())
        it3++;
    int i = 0;
    while (it3 != active_object_list.end())
    {
        i++;
        (*it3)->getNode()->stopAllActions();
        
        CCPoint new_pos = ccp(_object->getPos().x, _object->getPos().y - max_size_combo - i*m_bomb->frame);
        if(_anim)
        {
            CCAction *move = CCMoveTo::create(TIME_ANIMATION_BUY + time, ccp(_object->getPos().x, _object->getPos().y - max_size_combo - i*m_bomb->frame));
            (*it3)->getNode()->runAction(move);
        }
        else
        {
            (*it3)->getNode()->setPosition(new_pos);
        }
        it3++;
    };
    
    //animation scroll object revers any object
    m_bomb->m_scroll->getNode()->stopAllActions();
    CCFiniteTimeAction *move = CCMoveTo::create(TIME_ANIMATION_BUY + time, ccp(m_bomb->posX, m_bomb->m_scroll->getPos().y - diff));
    CCLog("showBuyObjectInfo:: delay time %f size",diff/m_bomb->frame,price_list.size());
    m_bomb->m_scroll->getNode()->runAction(move);
};


void CKShop::hideBuyObjectInfo(CKObject *_object)
{
    if(!_object)
        return;
    
    // std::vector<CKObject*>::iterator it2 = price_list.begin();
    // float height = (*it2)->getNode()->getContentSize().height * price_list.size();
    float time = TIME_ANIMATION_BUY*price_list.size();
    float persent = size_combo/max_size_combo;
    for(int i = 0; i < price_list.size(); i++)
    {
        CCFiniteTimeAction *move = CCMoveTo::create(TIME_ANIMATION_BUY + TIME_ANIMATION_BUY*i*persent, ccp(_object->getPos().x, _object->getPos().y));
        CCFiniteTimeAction * func = CCCallFuncN::create(price_list[i]->getNode(), callfuncN_selector(CKShop::hideNode));
        price_list[i]->getNode()->runAction(CCSequence::create(move,func,NULL));
        //     it2++;
    };
    
    std::vector<CKObject*>::iterator it3 = findObjectInShop(_object);
    
    if(it3 != active_object_list.end())
        it3++;
    
    int count_visible = 0;
    while (it3 != active_object_list.end())
    {
        count_visible++;
        CCAction *move = CCMoveTo::create(TIME_ANIMATION_BUY + time*persent, ccp(show_object->getPos().x, show_object->getPos().y - count_visible*m_bomb->frame));
        (*it3)->getNode()->runAction(move);
        it3++;
    };
    show_object = NULL;
    buy_object = NULL;
    if(count_visible < COUNT_BUY_BUTTON)
    {
        m_bomb->m_scroll->getNode()->runAction(CCMoveTo::create(0.2 + TIME_ANIMATION_BUY + time*persent,ccp(m_bomb->posX,m_bomb->bottom_pos) ));
        m_bomb->m_scroll->savePosition(ccp(m_bomb->posX,m_bomb->bottom_pos));
    };
    
    if(next_show != NULL)
    {
        schedule(schedule_selector(CKShop::schedureNextShow),TIME_ANIMATION_BUY + time*persent);
    }
    size_combo = 0.0;
};

void CKShop::schedureNextShow(const float &dt)
{
    unschedule(schedule_selector(CKShop::schedureNextShow));
    for(int i = 0; i < price_list.size(); i++)
    {
        price_list[i]->setVisible(false);
    };
    
    showBuyObjectInfo(next_show);
    next_show = NULL;
};

void CKShop::loadWithDict()
{
    
    dict_shop = NULL;
    dict_shop = CCDictionary::createWithContentsOfFile(CK::file_shop_bomb);
    std::string path = CCFileUtils::sharedFileUtils()->getDocumentPath();
    
    path.append(CK::file_shop_bomb);
    bool save = true;
    std::ifstream ifile(path.c_str());
    
    dict_shop = CCDictionary::createWithContentsOfFile(CK::file_shop_bomb);
    
    if (ifile )// The file exists, and is open for input
    {
        CCDictionary *dict1 = CCDictionary::createWithContentsOfFile(path.c_str());
        std::ofstream out(path.c_str(), std::ios::in | std::ios::binary);
        dict_shop = CCDictionary::createWithContentsOfFile(CK::file_shop_bomb);
        if(dict1->objectForKey("version"))
            if(dictFloat(dict1, "version") == dictFloat(dict_shop, "version"))
            {
                save = false;
                dict_shop = CCDictionary::createWithContentsOfFile(path.c_str());
            };
    }
    version = dictFloat(dict_shop, "version");
    CCDictionary* dict_meta = (CCDictionary*)dict_shop->objectForKey("meta");
    int i = 0;
    
    for(int i = 0;i < COUNT_BUY_BUTTON;i++)
    {
        discount_mas[i] = 0.0;
        best_price_mas[i] = false;
    };
    
    if(dict_meta)
    {
        CCArray* frames = (CCArray*)dict_meta->objectForKey("item");
        CCObject* pObj = NULL;
        CCARRAY_FOREACH(frames, pObj)
        {
            mas_count[i] = ((CCString*)pObj)->intValue();
            i++;
        };
    };
    
    CC_SAFE_RELEASE_NULL(dict_shop);
};


void CKShop::setLikes()
{
    CCLOG("CKShop::setLikes");
    if (!CKFileInfo::Instance().isLikesOur())
    {
        CK::StatisticLog::Instance().setNewPage(CK::Page_LikeComplite);
        char str[NAME_MAX];
        CKAudioMng::Instance().playEffect("new_free_coins");
        sprintf(str, LCZ("You earn %d coins."),1000);
        CKHelper::Instance().getDialog()->setTarget(this);
        CKHelper::Instance().getDialog()->setInfoText(8,LCZ("Thanks!"),str);
        CKHelper::Instance().getDialog()->showMessage(8,callfuncND_selector(CKShop::menuCall));
        CKFileInfo::Instance().addBigScore(1000,true);
        
        CKFileInfo::Instance().setLikesOur(true);
        CKFileInfo::Instance().save();
        updateTotalScoreLabel();
    }
};

void CKShop::functAsunc(CKShop * _sender)
{
    curent_load--;
    if(curent_load == 0)
    {
        load();
    };
};
void CKShop::activeted(cocos2d::CCLayer *_node)
{
    cocos2d::CCLayer *m_layer = (cocos2d::CCLayer *)this;
    reload();
    m_parent = _node;
    m_parent->setVisible(false);
    m_parent->setTouchEnabled(false);
    m_layer->setTouchEnabled(true);
    m_layer->setVisible(true);
}