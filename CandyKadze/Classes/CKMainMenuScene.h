//
//  CKMainMenuScene.h
//  CandyKadze
//
//  Created by PR1.Stigol on 18.09.12.
//
//

#ifndef CandyKadze_CKMainMenuScene_h
#define CandyKadze_CKMainMenuScene_h

#include <cocos2d.h>
#include "CKFileInfo.h"
#include "CKInventory.h"
#include "CKShop.h"
#include "CKFileOptions.h"
#include "CKAudioMgr.h"
#include "CKLoaderGUI.h"
#include "TBSpriteMask.h"

using namespace cocos2d;
class CKLeaderboard;
class CKParalaxNode;
class CKObject;
class CKTutorialScriptOneScene;
class CKMainMenuScene : public cocos2d::CCLayer {
public:
    ~CKMainMenuScene();
    CKMainMenuScene();
    
    // returns a Scene that contains the HelloWorld as the only child
    static cocos2d::CCScene* scene();
    
    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
    virtual void ccTouchesCancelled(CCSet *pTouches, CCEvent *pEvent){ccTouchesEnded(pTouches, pEvent);};
    //virtual void onEnter();
    void load();
    void functAsunc(CKMainMenuScene * _sender);
    void setLikes();
    void tweetCallBack(CCNode *_node,int *_value);
    void mailCallBack(CCNode *_node,int *_value);
    void callDialog(CCNode *_sender,void *_value);
    void callUpdate(CCNode *_sender,void *_value);
    
    void callFbDialog(CCNode *_sender,void *_value);
    void menuSelector(CCNode *_sender);
    void refreshLeaderboard();
//    virtual bool init();
private:
    enum
    {
        SKY_MARMELAD = 0,
        SKY_CHOCKO = 1,
        SKY_ICE = 2,
        SKY_WAFFEL = 3
    };
    CKTutorialScriptOneScene *script_scene;
    CKGUI* m_backnode;
    CKParalaxNode * m_menu_back;
    CKParalaxNode * m_plane;
    
    int curent_load;
    int font_size;
    bool showed_app;
    float offset_mask_play;
    CCSize win_size;
    CCLabelTTF *label_score;
    CCMenu *main_menu,*campaing_menu;
    CKGUI *m_dialog,*m_fb_dialog,*m_update;
    
    std::string update_url;
    CKLeaderboard * m_lead;
    CCMenuItemImage *menuItem[5];
    CCMenuItemImage *menuItem90[5];
   // CKFileInfo m_file_info;
    void update(const float _dt);
    void createMainMenu();
    void createPlane();
    void timeResetGame();
    void createCampaingMenu();
    void disableKidsMode();
    
    void getDeviceInfo();
    void showDialog(void *_d);
    void menuCall(CCNode* _sender,void *_value);
    
    void syncCloud(void* _d);
    void showMsg();
    void initGui();
    void initKMGui();
    void initFbGui();
    void initUpdateGui();
    
    CCPoint end_positon_cloud;
    CCPoint end_positon_hill_left,end_positon_hill_right;
    std::list<CCNode *> cloud_list;
    std::list<CCNode *> hill_list;
    std::list<CCNode *> ice_list;
    std::list<CCNode *>::iterator node_it;
    std::list<CCNode *>::iterator object_it;
    void updateCloud(const float & _dt);
    void updateHill(const float & _dt);
    void updateHillSpriteFrame(CCSprite *_sprite);
    void animationCloud(CCNode*_sender,void *_value);
    void shineIce(CCNode *_sender,void *_d);
    void callbackFunc(CCNode *_sender,void *_d);
    
    void actionPlayButton();
    void initSky();
    void nextSky();
    CKObject *waffel_cloud;
    //CCNode *plane_scarf;
    CKObject *left_bar,*right_bar,*left_eye,*right_eye;
    CCPoint left_bar_position,right_bar_positon;
    CCPoint position_eye;
    void skewCloud(CCNode *_sender,void *_d);
    void hideNode(CCNode *_sender);
    void flyPlaneAnimation();
    CCSprite * m_sky[4];
  //  CCRenderTexture* rtt_scarf;
    TBSpriteMask * tb_sprite,*spr_mask_play;
    float koef_device;
    void lookEyeToPoint(const CCPoint &_pos);
    int current_sky;
    void animationEye();
    void animationPart();
    void animationScarf();
};

#endif
