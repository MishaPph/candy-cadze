//
//  CKWordScene.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 18.09.12.
//
//

#include "CKWordScene.h"
#include "CKGameScene.h"
#include "CKGameScene90.h"
#include "CK.h"
#include "CKMainMenuScene.h"
#include "CKHelper.h"
#include "CKAdBanner.h"
#include "CKLanguage.h"
#include "CKTableScore.h"
#include "DeviceInfo.h"
#include "StatisticLog.h"
#include "CKParseHelper.h"
#include "CKFacebookHelper.h"

#pragma mark - BASIC

CKWordScene::~CKWordScene()
{
    CC_SAFE_DELETE(m_backnode);
   // CC_SAFE_DELETE(m_info_msg);
    CC_SAFE_DELETE(m_dialog);
    removeAllChildrenWithCleanup(true);
   // CKTableScore::Instance().saveFriendScore();
    CCLOG("destroy ~CKWordScene");
    if(!CKTableFacebook::Instance().friendEmpty())
        CKTableFacebook::Instance().saveFriendProgress("fb_friends.plist");
    CKFacebookHelper::Instance().setCallBackFunc(NULL, NULL);
    CKParseHelper::Instance().setCallbackFunc(NULL, NULL);
};

CCScene* CKWordScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    // add layer as a child to scene
    CCLayer* layer = new CKWordScene();
    if(layer->init())
    {
        scene->addChild(layer);
        layer->setTouchEnabled(true);
        layer->setTag(LAYERID_MAIN);
        layer->release();
    };
    static_cast<CKWordScene*>(layer)->load();
    CKHelper::Instance().compliteLoadScene(scene);
    return scene;
}

#pragma mark - INIT
CKWordScene::CKWordScene()
{
    CK::StatisticLog::Instance().setNewPage(Page_LevelSelect);
    
    m_dialog = NULL;
    current_buy_world = -1;
    m_backnode = NULL;
    message_sprite = NULL;
    m_info_msg = NULL;
    CCLOG("create::CKWordScene");
    win_size = CCDirector::sharedDirector()->getWinSize();
    font_size = win_size.width/50;
    setTouchEnabled(true);
    
    CKFileInfo::Instance().load();
    is_ipad = CKHelper::Instance().getIsIpad();
    curen_load = 1;
    

    //CK::DeviceInfo::Instance().sendAllInfoFileToAmazon();
    CKAmazonHelper::Instance().uploadPlayerInfo();
};

void CKWordScene::compliteLoadFriend(CCNode* _sender,void *_value)
{
    CCLOG("CKWordScene::compliteLoadFriend %d",*static_cast<int *>(_value));
    if(*static_cast<int *>(_value) == 1)
    {
        if(!CKTableFacebook::Instance().friendEmpty())
        {
            CKParseHelper::Instance().parseLoadOpenLvl(CKTableFacebook::Instance().getFriends(),this, callfuncND_selector(CKWordScene::compliteLoadFriend));
        };
    }else if(*static_cast<int *>(_value) == 3 )
    {
        loadFriendAvatar();
    }
}

void CKWordScene::initGUI()
{
    timeval t;
    gettimeofday(&t,NULL);
    CCLOG("CKWordScene::load time sec: %d::%d",t.tv_sec,t.tv_usec);
    
    m_backnode = new CKGUI;
    m_backnode->create(CCNode::create());
    m_backnode->load(loadFile("c_levelSelect.plist",true).c_str());
    
    CCLOG("CKWorldScene:: world_number %d",CKHelper::Instance().getWorldNumber());
    
    m_backnode->get()->setPosition(ccp(0,0));
    m_backnode->setTarget(this);
    m_backnode->addFuncToMap("menuCall", callfuncND_selector(CKWordScene::menuCall));
    m_backnode->addFuncToMap("levelCall", callfuncND_selector(CKWordScene::levelCall));
    m_backnode->addFuncToMap("selectorCall", callfuncND_selector(CKWordScene::selectorCall));
    m_backnode->addFuncToMap("callBuy", callfuncND_selector(CKWordScene::callBuy));
    m_backnode->setTouchEnabled(true);
    // m_backnode->reloadAtlas();
    addChild(m_backnode->get(),1);
    
    gettimeofday(&t,NULL);
    CCLOG("CKWordScene::load time sec: %d::%d",t.tv_sec,t.tv_usec);
    
    m_backnode->setActiveScrollFrame(-CKHelper::Instance().getWorldNumber(),false);
    m_scroll = m_backnode->getChildByType(CK::GuiScrollView);
    
    if(CKFileOptions::Instance().isKidsModeEnabled())
    {
        m_backnode->getChildByTag(CK::Action_Inventory)->setVisible(false);
        m_backnode->getChildByTag(CK::Action_Shop)->getChildByTag(2)->setText(LCZ("Bombs"));
    }
};
void* CKWordScene::thread_function( void *ptr )
{
    static_cast<CKWordScene *>(ptr)->load();
    pthread_exit(0);
    CKHelper::Instance().compliteLoadScene((CCScene *)static_cast<CKWordScene *>(ptr)->getParent());
    return NULL;
};

void CKWordScene::loadAsync(void *_data)
{
   // pthread_cancel(m_thread);
    pthread_create(&m_thread, NULL, &CKWordScene::thread_function, _data);
};

void CKWordScene::load()
{
    CKAmazonHelper::Instance().downloadBanner();
    
    if(CKHelper::Instance().menu_number == CK::SCENE_GAME)
    {
        CKAudioMng::Instance().playMenuSound();
    };

//    if(CKHelper::Instance().menu_number == CK::SCENE_MAIN) //previor scene main menu
//    {
//        if(this->getParent() != NULL && !ObjCCalls::isExistFile())
//            if(rand()%5 == 0)
//            {
//                CKHelper::Instance().getDialog()->setTarget(this);
//                CKHelper::Instance().getDialog()->setConfirmText("Do you want to send game statistic?");
//                CKHelper::Instance().getDialog()->show(callfuncN_selector(CKWordScene::showDialog));
//            };
//    };

    setVisible(true);
    
    unlock_world = 0;
    
    srand(time(0));
    
    if(!CKFileOptions::Instance().isKidsModeEnabled())
    {
        
        if(CKFileInfo::Instance().isOpenWorld(CKHelper::Instance().getWorld().c_str()))
        {
            int count_open_lvl = CKFileInfo::Instance().getLvlOpenWithWorld(CKHelper::Instance().getWorld());
            CCLOG("count_open_lvl %d",count_open_lvl);
            
            if(count_open_lvl > 3)
                if(CKFileInfo::Instance().isEmptyInvSelect())
                {
                    if(CKFileInfo::Instance().isEmptyInv())
                        showMessage(1);
                    else
                        showMessage(0);
                }
        };
        if(rand()%5 == 0)//20 percent then show message open world
        {
            for(int i = 2; i < COUNT_WORLD;++i)
            {
                
                // CCLOG("isOpend world %d , %d",i,CKFileInfo::Instance().isOpenWorld(CKHelper::Instance().getWorld(i).c_str()));
                if(!CKFileInfo::Instance().isOpenWorld(CKHelper::Instance().getWorld(i).c_str()))
                {
                    //CCLOG("world_cost[i] %d score %d",world_cost[i],CKFileInfo::Instance().getTotalScore());
                    if(CKFileInfo::Instance().getTotalScore() > world_cost[i])
                    {
                        unlock_world = i;
                        showMessage(2);//unlock world
                        break;
                    }
                }
            }
        }
        else{
            if(!CKFileOptions::Instance().isKidsModeEnabled() && CKFileOptions::Instance().isBannerShow())
                ObjCCalls::showChartboost("Pause");
        };
    }else
    {
        initKMDialogGui();
    }
    
    
    ObjCCalls::hideBanner(true);
    if(CKFileOptions::Instance().isBannerShow() && !CKFileOptions::Instance().isKidsModeEnabled())
    {
        ObjCCalls::createBanner();
        CKAdBanner::Instance().load();
    }
    

    initGUI();
    initBackground();
    
    CKHelper::Instance().updateLoading();
    CKHelper::Instance().menu_number = CK::SCENE_WORLD;
    
    this->sortAllChildren();
    
    update_active = false;
    
    initLock();
    
    showAllWorl();
    
    //CCTextureCache::sharedTextureCache()->dumpCachedTextureInfo();
   // CCLOG("CKWordScene::Free memory %d",CKSystemInfo::Instance().getFreeMemory());

    unlockWorld();
    
    if(!CKFileOptions::Instance().isKidsModeEnabled())
    {
        if (ObjCCalls::getDeviceVersion() >= 6.0 && CKTableFacebook::Instance().friendEmpty()) {
            if(!ObjCCalls::isActiveConnection())
            {
                CKTableFacebook::Instance().loadFriendProgress("fb_friends.plist");
                loadFriendAvatar();
            }else
            {
                CKFacebookHelper::Instance().FB_TryLogin();
                CKFacebookHelper::Instance().setCallBackFunc(this, callfuncND_selector(CKWordScene::compliteLoadFriend));
                CKFacebookHelper::Instance().FB_LoadFriendsList();
            }
        }
        else if(!CKTableFacebook::Instance().friendEmpty())
        {
            CKParseHelper::Instance().parseLoadOpenLvl(CKTableFacebook::Instance().getFriends(),this, callfuncND_selector(CKWordScene::compliteLoadFriend));
        }
    };
    
    
};

void CKWordScene::unlockWorld()
{
    if(CKHelper::Instance().call_unlock_world != 0)
    {
        unlock_world = CKHelper::Instance().call_unlock_world;
        m_backnode->setActiveScrollFrame(-unlock_world,true);
        this->runAction(CCSequence::create(CCDelayTime::create(0.5),CCCallFuncND::create(this, callfuncND_selector(CKWordScene::callBuy),&unlock_world),NULL));
        schedule( schedule_selector(CKWordScene::update),1.0f/60.0f);
        CKHelper::Instance().call_unlock_world = 0;
    };
};
void CKWordScene::initKMDialogGui()
{
    m_dialog = new CKGUI();
    m_dialog->create(CCNode::create());
    m_dialog->load(CK::loadFile("c_ModalWindowTurnOffKM.plist").c_str());
    m_dialog->setTouchEnabled(true);
    m_dialog->setTarget(this);
    m_dialog->addFuncToMap("callDialog", callfuncND_selector(CKWordScene::callDialog));
    m_dialog->get()->setVisible(false);
    if(win_size.width == 568)
    {
        m_dialog->get()->setPosition(ccp(44,0));
    };
    addChild(m_dialog->get(),99);
};

void CKWordScene::initBackground()
{
    bg[0] = CCSprite::createWithSpriteFrameName("back90.png");
    bg[1] = CCSprite::createWithSpriteFrameName("backEasy.png");
    bg[3] = CCSprite::createWithSpriteFrameName("backMedium.png");
    bg[2] = CCSprite::createWithSpriteFrameName("backStandart.png");
    bg[4] = CCSprite::createWithSpriteFrameName("backHard.png");
    
    for(int i = 0; i < 5;++i)
    {
        bg[i]->setTag(i+1);
        bg[i]->setOpacity(0);
        bg[i]->setVisible(false);
        addChild(bg[i]);
        bg[i]->setPosition(ccp(win_size.width/2,win_size.height/2));
        bg[i]->setScaleX(win_size.width/bg[i]->getContentSize().width);
        bg[i]->setScaleY(win_size.height/bg[i]->getContentSize().height);
    }
    bg[CKHelper::Instance().getWorldNumber()]->setOpacity(255);
    bg[CKHelper::Instance().getWorldNumber()]->setVisible(true);
};



bool CKWordScene::init()
{
    if(CKHelper::Instance().show_layer != 0)
    {
   //     menuCall(NULL,&CKHelper::Instance().show_layer);
    };
    CKHelper::Instance().show_layer = 0;
    return true;
};

void CKWordScene::createDifficultyMenu()
{

};

void CKWordScene::showDialog(void *_d)
{
    if(_d != NULL)
        ObjCCalls::checkSendStatistic();
};

void CKWordScene::createMessages(std::string _filename )
{
//    message_sprite = CCSprite::create(loadImage(_filename.c_str()).c_str());
//    
//    addChild(message_sprite,zOrder_Menu);
//    message_sprite->setPosition(ccp(win_size.width*0.5,win_size.height*0.5));
//    CKButton::Instance().setButtonNames("BtnLong.png","BtnLongPressed.png","BtnLong.png");
//    CCSize size = message_sprite->getContentSize();
//    CCMenuItemSprite* item1 = CKButton::Instance().create("OK",Action_Ok,font_size,this,menu_selector(CKWordScene::menuCallback));
//    item1->setPosition(size.width*0.5,size.height*0.1);
//    CCMenu* menu_pause = CCMenu::create(item1,NULL);
//    menu_pause->setPosition(0,0);
//    message_sprite->addChild(menu_pause);
};

void CKWordScene::initLock()
{
    if(!m_scroll)
        return;
    for (int j = 1; j <= COUNT_WORLD;++j)
    {
        bool open_world = CKFileInfo::Instance().isOpenWorld(CKHelper::Instance().getWorld(j-1).c_str());
        if(open_world)
        {
            m_scroll->getChildByTag(9+j)->getNode()->setVisible(false);
            m_scroll->getChildByTag(9+j)->setTouch(false);
            m_scroll->getChildByTag(19+j)->setVisible(false);
            for (int i = 0; i < COUNT_LEVEL_IN_WORLD; i++)
            {
                CKObject* tmp = m_scroll->getChildByTag(j*1000 + i + 1);
                if(tmp)
                    tmp->getNode()->setVisible(true);
            };
        }
        else
        {
            char cost_world[NAME_MAX];
            sprintf(cost_world,"%d",CK::world_cost[j-1]);
            m_scroll->getChildByTag(9 + j)->getChildByTag(12)->getChildByType(CK::GuiLabel)->setText(cost_world);
            for (int i = 0; i < COUNT_LEVEL_IN_WORLD; i++)
            {
                CKObject* tmp = m_scroll->getChildByTag(j*1000 + i + 1);
                if(tmp)
                    tmp->getNode()->setVisible(false);
            };
        };
    };
};
void CKWordScene::insertWorldAvatar(int _num_world,const char *user_id)
{
    CCLOG("CKWordScene::insertWorldAvatar %d %s",_num_world,user_id);
    CKObject* world_group = m_scroll->getChildByTag(_num_world);
    CKObject* ava_group = world_group->getChildByTag(3);
    if(!ava_group)
        return;
    
    char str[NAME_MAX];
    sprintf(str, "%s.jpg",user_id);
    std::string path = CCFileUtils::sharedFileUtils()->getWriteablePath().append(str);
    std::ifstream ifile(path.c_str());
    if(!ifile)
    {
        return;
    }
    ifile.close();
    
    if(!ava_group->isVisible())
    {
        ava_group->setVisible(true);
        for (int i = 0; i < 12; ++i) {
            ava_group->getChildByTag(i + 1)->setVisible(false);
        }
    };

    for (int i = 0; i < 12; ++i) {
        CKObject * tmp = ava_group->getChildByTag(i + 1);
        if(!tmp->isVisible())
        {
            CCSprite * ava = static_cast<CCSprite *>(tmp->getChildByTag(1)->getNode());
            CCLOG("insertWorldAvatar name %s %f %f",user_id,ava->getContentSize().width,ava->getContentSize().height);
                tmp->setVisible(true);
                CCTexture2D *tex =  CCTextureCache::sharedTextureCache()->addImage(str);
                ava->setTexture(tex);
                ava->setTextureRect(CCRect(0, 0, tex->getContentSize().width, tex->getContentSize().height),false,ava->getContentSize());
                ava->setScaleX(ava->getContentSize().width/tex->getContentSize().width);
                ava->setScaleY(ava->getContentSize().height/tex->getContentSize().height);
            return;
        }
    }
    return;
};
void CKWordScene::insertLvlAvatar(int _num_lvl,const char *user_id)
{
    CCLOG("CKWordScene:: insertLvlAvatar name %d %s",_num_lvl,user_id);
    char str[NAME_MAX];
    sprintf(str, "%s.jpg",user_id);
    std::string path = CCFileUtils::sharedFileUtils()->getWriteablePath().append(str);
    std::ifstream ifile(path.c_str());
    if(!ifile)
    {
        return;
    }
    ifile.close();
    
    CKObject* lvl_group = m_scroll->getChildByTag(_num_lvl);
    CKObject* ava_group = lvl_group->getChildByTag(3);
    if(!ava_group)
        return;
    if(!ava_group->getNode()->isVisible())
    {
        ava_group->getNode()->setVisible(true);
        for (int i = 0; i < 4; ++i) {
            ava_group->getChildByTag(i + 1)->setVisible(false);
        }
    };
    
    for (int i = 0; i < 4; ++i) {
        CKObject * tmp = ava_group->getChildByTag(i + 1);
        if(!tmp->isVisible())
        {
            CCSprite * ava = static_cast<CCSprite *>(tmp->getChildByTag(1)->getNode());
            CCLOG("insertLvlAvatar name %s %f %f",user_id,ava->getContentSize().width,ava->getContentSize().height);
            
            CCTexture2D *tex =  CCTextureCache::sharedTextureCache()->addImage(str);
            tmp->setVisible(true);
            ava->setTexture(tex);
            ava->setTextureRect(CCRect(0, 0, tex->getContentSize().width, tex->getContentSize().height),false,ava->getContentSize());
            ava->setScaleX(ava->getContentSize().width/tex->getContentSize().width);
            ava->setScaleY(ava->getContentSize().height/tex->getContentSize().height);
            return;
        }
    }
    return;
};
void CKWordScene::showAvatarInWorld(int _world)
{
    int j = (current_buy_world != -1)?current_buy_world:_world;
    current_buy_world = -1;
    
    CKObject* world_group = m_scroll->getChildByTag(10 + j);
    if(world_group)
    {
        if(world_group->getChildByTag(3))
        {
            world_group->getChildByTag(3)->setVisible(false);
        }
    }
    
    std::list<CKFriendLvlRecords*> *f_list = CKTableFacebook::Instance().getFriendsOpenLvlinWorld(j);
    if(f_list->empty())
        return;
    std::list<CKFriendLvlRecords*>::iterator it = f_list->begin();
    CCLOG("CKWordScene::loadFriendAvatar size %d",f_list->size());
    if(CKFileInfo::Instance().isOpenWorld(CKHelper::Instance().getWorld(j).c_str()))
    {
        while (it != f_list->end()) {
            insertLvlAvatar((j+1)*1000 + (*it)->world[j],(*it)->name);
            it++;
        }
    }
    else
    {
        while (it != f_list->end()) {
            insertWorldAvatar(10 + j,(*it)->name);
            it++;
        }
    }
}
void CKWordScene::loadFriendAvatar()
{
    for (int j = 0; j < COUNT_WORLD;++j)
    {
        showAvatarInWorld(j);
    };
};
void CKWordScene::showLvlInWorld(int _world)
{
    int j = _world + 1;
    if(current_buy_world != -1)
        j = current_buy_world + 1;
    //current_buy_world = -1;
    
    int count_open_lvl = 0;
    bool open_world = CKFileInfo::Instance().isOpenWorld(CKHelper::Instance().getWorld(j - 1).c_str());
    bool is_kids_mode = CKFileOptions::Instance().isKidsModeEnabled();
    
    if(open_world)
    {
        
        count_open_lvl = CKFileInfo::Instance().getLvlOpenWithWorld(CKHelper::Instance().getWorld(j - 1));
        for (int i = 0; i < COUNT_LEVEL_IN_WORLD; i++)
        {
            CKObject* tmp = m_scroll->getChildByTag(j*1000 + i + 1);
            if(!tmp)
                continue;
            tmp->setVisible(true);
            
            if(tmp->getChildByTag(3))
                tmp->getChildByTag(3)->getNode()->setVisible(false);
            
            if(i <= count_open_lvl || CKFileOptions::Instance().isDebugMode())
            {
                tmp->setEnabled(true);
                
                if(tmp->getChildByTag(1))
                {
                    tmp->getChildByTag(1)->getNode()->setVisible(true);
                    tmp->getChildByTag(2)->getNode()->setVisible(true);
                    
                    char num_lvl[NAME_MAX];
                    sprintf(num_lvl, "%d",*tmp->getValue() + 1);
                    tmp->getChildByTag(2)->setText(num_lvl);
                    
                    if(is_kids_mode)
                    {
                        tmp->getChildByTag(1)->setVisible(false);
                    }
                    else
                    {
                        char score_str[NAME_MAX];
                        sprintf(score_str, " %d",MAX(int(CKFileInfo::Instance().getLevelScore(i)),0));
                        tmp->getChildByTag(1)->setText(score_str);
                    }
                }
            }
            else
            {
                tmp->setEnabled(false);
                if(tmp->getChildByTag(1))
                {
                    tmp->getChildByTag(1)->getNode()->setVisible(false);
                    tmp->getChildByTag(2)->getNode()->setVisible(false);
                }
            }
        };
        
        CCPoint s_pos = m_scroll->getChildByTag(j*1000 + 1)->getPos();
        for (int i = 0; i < 4; i++)
            for (int k = 0; k < 4; k++)
            {
                CKObject* tmp = m_scroll->getChildByTag(j*1000 + i*4 + k + 1);
                if(!tmp)
                    continue;
                if(is_ipad)
                {
                    tmp->setPosition(s_pos.x + 220*k , s_pos.y - i*140);
                }
                else if(win_size.width != 568)
                {
                    tmp->setPosition(s_pos.x + 105*k , s_pos.y - i*57);
                }
                else
                {
                    tmp->setPosition(s_pos.x + 115*k , s_pos.y - i*57);
                }
            };
    }
    else
    {
        m_scroll->getChildByTag(10 + (j - 1))->getChildByTag(3)->setVisible(false);
    }
};

void CKWordScene::showAllWorl()
{
    bool is_kids_mode = CKFileOptions::Instance().isKidsModeEnabled();
    if(is_kids_mode)
    {
        m_backnode->getChildByTag(5)->setVisible(false);
    }
    else
    {
        char total_str[NAME_MAX];
        sprintf(total_str, "%d",CKFileInfo::Instance().getTotalScore());
        m_backnode->getChildByTag(5)->getChildByTag(5)->setText(total_str);
    }

    if(!m_scroll)
        return;
    for (int j = 0; j < COUNT_WORLD;++j)
    {
        showLvlInWorld(j);
        //position
    }

};

void CKWordScene::showMsg(CCNode *_sender,void *_data)
{
    int _num = *static_cast<int *>(_data);
    CKHelper::Instance().getDialog()->setTarget(this);
    if(_num == 1)
    {
        CKHelper::Instance().getDialog()->showMessage(6,callfuncND_selector(CKWordScene::menuCall));
    }else if(_num == 2)
    {
        CKHelper::Instance().getDialog()->showMessage(5,callfuncND_selector(CKWordScene::menuCall));
    }else
    {
        CKHelper::Instance().getDialog()->showMessage(1,callfuncND_selector(CKWordScene::menuCall));
    };
};

void CKWordScene::showMessage(int _num)
{
    if(CKHelper::Instance().call_unlock_world != 0)
    {
        return;
    };
    if (getParent() == NULL) {
        runAction(CCSequence::create(CCDelayTime::create(0.2),CCCallFuncND::create(this, callfuncND_selector(CKWordScene::showMsg), &_num),NULL));
        return;
    };
    showMsg(this,&_num);
};

#pragma mark - CALLBACK
void CKWordScene::diffCallback(CCNode* _sender)
{
};

void CKWordScene::levelCall(CCNode* _sender,void *_value)
{
    CKHelper::Instance().lvl_number = *static_cast<int*>(_value);
    CKAudioMng::Instance().playEffect("button_pressed");

//    if(CKFileOptions::Instance().isPlayMusic())
//    {
//        CKAudioMng::Instance().fadeBgToVolume(0.0);
//    };
    
    unschedule( schedule_selector(CKWordScene::update));
    
    
  //  setTouchEnabled(false);
  //  CCDirector::sharedDirector()->setDepthTest(false);
    
    ObjCCalls::sendEventGA("start_lvl","lvl_start","start_lvl",1);//GA Фіксуємо кількість завантажень ігрових сцен

    CKFileInfo::Instance().save();
    if(CKHelper::Instance().getWorldNumber() == 0)
    {
        CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
        CKHelper::Instance().playScene(CK::SCENE_GAME90);
    }
    else
    {
        CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
        CKHelper::Instance().playScene(SCENE_GAME);
    };
};

void CKWordScene::selectorCall(CCNode* _sender,void *_value)
{
    //m_backnode->getScrollValue();
    int n = (*static_cast<int*>(_value) - 100);
    //CCLOG("m_backnode->getScrollValue() %d %d",n,m_backnode->getScrollValue());
    int m = CKHelper::Instance().getWorldNumber();
    if((n - m) > 1)
    {
        n = m + 1;
    }
    else
    if((n - m) < -1)
    {
        n = m - 1;
    };
    m_backnode->setActiveScrollFrame(-n);
    CKHelper::Instance().setWorldNumber(n);
    CKHelper::Instance().lvl_speed = n-1;
};

void CKWordScene::callBuy(CCNode* _sender,void *_value)
{
  
    if(CKFileOptions::Instance().isKidsModeEnabled())
    {
        m_dialog->get()->setVisible(true);
        return;
    }
    current_buy_world = -1;
    if(unlock_world == 0)
    {
        unlock_world = *static_cast<int *>(_value);
    };
    if(CKFileInfo::Instance().getTotalScore() >= world_cost[unlock_world])
    {
        CKAudioMng::Instance().playEffect("button_pressed");
        CKHelper::Instance().getDialog()->setTarget(this);
        CKHelper::Instance().getDialog()->setConfirmText(LCZ("Buy this world?"));
        CKHelper::Instance().getDialog()->show(callfuncN_selector(CKWordScene::buyWorld));
    };
};

void CKWordScene::animationLockSkew(int _value)
{
//    CKObject *lock = m_scroll->getChildByTag(unlock_world + 10);
};

void CKWordScene::animationLock()
{
    float time_move = 0.7;
    CKObject *lock = m_scroll->getChildByTag(unlock_world + 10);
    CCNode *label = lock->getChildByTag(12)->getNode();
    CCNode *middle = lock->getChildByTag(11)->getNode();
    label->runAction(CCMoveBy::create(time_move/2, ccp(0,-win_size.height)));
    middle->runAction(CCSequence::create(CCMoveBy::create(time_move/2, ccp(0,-win_size.height)),NULL));
    
    CCNode *left = lock->getChildByTag(7)->getNode();
    CCNode *right = lock->getChildByTag(8)->getNode();
    CCNode *top = lock->getChildByTag(9)->getNode();
    
    left->runAction(CCMoveBy::create(time_move, ccp(0,win_size.height*0.7)));
    right->runAction(CCMoveBy::create(time_move, ccp(0,win_size.height*0.7)));
    top->runAction(CCMoveBy::create(time_move, ccp(0,win_size.height*0.7)));
    runAction(CCSequence::create(CCDelayTime::create(time_move),CCCallFunc::create(this, callfunc_selector(CKWordScene::showLvlInWorld)),CCCallFunc::create(this, callfunc_selector(CKWordScene::showAvatarInWorld)),NULL));
};

void CKWordScene::buyWorld(void *_d)
{
    if(!_d)
    {
        CKAudioMng::Instance().playEffect("button_pressed");
        unlock_world = 0;
        return;
    };
    CK::StatisticLog::Instance().setNewPage(Page_UlockWorld);
    CK::StatisticLog::Instance().setNewPage(Page_LevelSelect);
    
    CKFileInfo::Instance().addWorld(CKHelper::Instance().getWorld(unlock_world).c_str());
    CKFileInfo::Instance().addDifficult(unlock_world);
    CKFileInfo::Instance().addBigScore(-world_cost[unlock_world],false);
    CKFileInfo::Instance().save();
    m_scroll->getChildByTag(unlock_world + 20)->setVisible(false);
    current_buy_world = unlock_world;
    animationLock();
    CKAudioMng::Instance().playEffect("unlock_world");
    if(!CKFileOptions::Instance().isKidsModeEnabled())
    {
        if (ObjCCalls::getDeviceVersion() >= 6.0 && CKTableFacebook::Instance().friendEmpty()) {
            if(!ObjCCalls::isActiveConnection())
            {
                CKTableFacebook::Instance().loadFriendProgress("fb_friends.plist");
            }
        }
        //loadFriendAvatar();
        //showAvatarInWorld(unlock_world);
    };
    unlock_world = 0;
};

void CKWordScene::menuCall(CCNode* _sender,void *_value)
{
    CKAudioMng::Instance().playEffect("button_pressed");
    switch(*static_cast<int*>(_value))
    {
        case CK::Action_Menu:
        {
            m_backnode->setTouchEnabled(true);
           // m_info_msg->setVisible(false);
            if(unlock_world != 0)
            {
                CKHelper::Instance().call_unlock_world = unlock_world;
                unlockWorld();
            }
        };
            break;
            
        case CK::Action_Back:
        {

            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(CK::SCENE_MAIN);
        };
            break;
        case CK::Action_Ok:
        {
            if(message_sprite)
                message_sprite->setVisible(false);
        };
            break;
        case Action_Difficult:
//        {
//            CKAudioMng::Instance().playEffect("button_pressed");
//            m_backnode->setTouchEnabled(false);
//           // difficult_menu->setVisible(true);
//            for(int i = 0; i < COUNT_DIFFICULT;++i)
//            {
//                diffItem[i]->setEnabled(CKFileInfo::Instance().isOpenDifficult(diffItem[i]->getTag()));
//                if(CKFileOptions::Instance().isDebugMode())
//                    diffItem[i]->setEnabled(true);
//            };
//            
//        };
            break;
        case CK::Action_Inventory:
        {
#ifndef DISABLE_INVENTORY
            
            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(CK::SCENE_INVENTORY);
#endif
        }
            break;
        case CK::Action_Shop_Coins:
            CKHelper::Instance().show_shop_coins = 1;
            CKHelper::Instance().show_shop_bomb = 0;
        case CK::Action_Shop:
#ifndef DISABLE_SHOP
            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(CK::SCENE_SHOP);
#endif
            break;
        default:
            CCAssert(0, "Uknow tag");
    };
};

void CKWordScene::menuCallback(CCNode* _sender,void *_data)
{
    switch(*static_cast<int *>(_data))
    {
        case CK::Action_Ok:
        {
            m_backnode->setTouchEnabled(true);
           // m_info_msg->setVisible(false);
        };
            break;
        default:
            CCAssert(0, "Uknow tag");
    }
};
void CKWordScene::callDialog(CCNode *_sender,void *_value)
{
    switch(*static_cast<int*>(_value))
    {
        case CK::Action_Yes:
            // resetGame();
        case CK::Action_No:
        case CK::Action_Close:
            CKAudioMng::Instance().playEffect("button_pressed");
            m_dialog->get()->setVisible(false);
            break;
    };
};
void CKWordScene::timeDisableKM()
{
    m_dialog->get()->setVisible(false);
    
    CKObject * _obj = m_backnode->getChildByTag(CK::Action_Inventory);
    
    _obj->setVisible(true);
    _obj->getNode()->setScale(0.0);
    _obj->getNode()->runAction(CCEaseElasticOut::create(CCScaleTo::create(1.0, 1.0)));
    
    m_backnode->getChildByTag(CK::Action_Shop)->getChildByTag(2)->setText(LCZ("Shop"));
    
    CKFileOptions::Instance().setKidsMode(false);
    CKFileOptions::Instance().save();
};

#pragma mark - UPDATE OBJECT
void CKWordScene::update(const float &_dt)
{
   // CKAudioMng::Instance().update(_dt);
    float value = -m_backnode->getScrollValue()/win_size.width;
    
    int frame =int(value);
    value -= int(value);
    for(int i = 0; i < 5;++i)
    {
        bg[i]->setOpacity(0);
        bg[i]->setVisible(false);
    }
    
    if(frame >= 0 && frame < 5)
    {
        bg[frame]->setOpacity(255 - fabs(value)*255);
        bg[frame]->setVisible(true);
    }
    
    if(value >= 0.0)
        if((frame + 1) < 5)
        {
            bg[frame + 1]->setOpacity(fabs(value)*255);
            bg[frame + 1]->setVisible(true);
        };
    
    if(CKHelper::Instance().lvl_speed != MAX(0, (frame - 1)))
    {
        //CKAudioMng::Instance().playEffect("change_world");
        CKHelper::Instance().lvl_speed = MAX(0, (frame - 1));
    }
    else if(value*10 == 0)
    {
        update_active = false;
        unschedule( schedule_selector(CKWordScene::update));
    }
   //    CCLOG("scrool value %f %f %d",fabs(m_backnode->getScrollValue()/1024.0),value,frame);
};

void CKWordScene::ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    CCTouch* touch =(CCTouch*)*touches->begin();
    if(touch->getID() != 0)//first touch
        return;
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    
    if(m_dialog && m_dialog->get()->isVisible())
    {
        unschedule(schedule_selector(CKWordScene::timeDisableKM));
        m_dialog->setTouchEnd(location);
        return;
    };
    int prev_frame = m_backnode->getNumberScroll();
    if(m_backnode)
    {
        m_backnode->setTouchEnd(location);
    };
    
    if(previor_scroll_value != m_backnode->getScrollValue() )
    {
        if(prev_frame != m_backnode->getNumberScroll())
        {
            CKAudioMng::Instance().playEffect("run_scroll");
        }
    };
};

void CKWordScene::ccTouchesMoved(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    CCTouch* touch =(CCTouch*)*touches->begin();
    if(touch->getID() != 0)//first touch
        return;
    
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    
    if(m_dialog && m_dialog->get()->isVisible())
    {
        return;
    };
    
    if(!update_active)
    {
        schedule( schedule_selector(CKWordScene::update),1.0/60.0 );
        update_active = true;
    }
    m_backnode->setTouchMove(location);
};

void CKWordScene::ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    CCTouch* touch =(CCTouch*)*touches->begin();
    if(touch->getID() != 0)//first touch
        return;
    
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    
    if(m_dialog && m_dialog->get()->isVisible())
    {
        m_dialog->setTouchBegan(location);
        if (m_dialog->getCurrnet() && m_dialog->getCurrnet()->getTag() == CK::Action_Yes) {
            schedule(schedule_selector(CKWordScene::timeDisableKM),KM_TIME_RESET_GAME,0, KM_TIME_RESET_GAME);
        };
        return;
    };
    
    if(m_backnode)
        m_backnode->setTouchBegan(location);
    
    previor_scroll_value = m_backnode->getScrollValue();
};

