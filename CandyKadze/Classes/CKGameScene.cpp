//
//  CKGameScene.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 18.09.12.
//
//

#include "CKGameScene.h"
#include "CKTutorialScriptOneScene.h"
#include "CKLoaderGUI.h"
#include "CKAdBanner.h"
#include "CKLanguage.h"
#include "CKParseHelper.h"
#include "CKTwitterHelper.h"
#include "CKFacebookHelper.h"

using namespace cocos2d;

#define PTM_RATIO 32
#define BOMB_SPEED 3
#define SIZE_ICON 140
#define BASIC_SCALE_ICON 1.0
#define DEFAULT_BOMB_COUNT 999
#define SIZE_LABEL_POOL 10
#define TIME_TO_SHOW_BANNER 0.3

const char *sound_str1[2] = {"Sound: Off","Sound: On "};
const char *effect_str1[2] = {"Effect: Off","Effect: On "};

#pragma mark  - BASE
CKGameSceneNew::~CKGameSceneNew()
{
    CCLOG("destroy ~CKGameSceneNew");

    shine_star_pool.clear();
    
    ObjCCalls::everyplayStopRecording();
    
    CC_SAFE_DELETE(m_airplane);
    CC_SAFE_DELETE(m_bombs);
    CC_SAFE_DELETE(m_bomb_ghost);
    CC_SAFE_DELETE(m_cube_array);
    CC_SAFE_DELETE(m_bonus);
    CC_SAFE_DELETE(m_gui_game);
    CC_SAFE_DELETE(m_gui_pause);
    CC_SAFE_DELETE(m_gui_complite);
    CC_SAFE_DELETE(m_gui_failed);
  //  CC_SAFE_DELETE(m_accelerometer);
    CC_SAFE_DELETE(m_share);
    
    CC_SAFE_DELETE(m_backnode);
    this->removeAllChildrenWithCleanup(true);
    ObjCCalls::hideBanner(true);
    
};

void CKGameSceneNew::clearScene(bool _full)
{
    shine_star_pool.clear();
    
    CC_SAFE_DELETE(m_airplane);
    CC_SAFE_DELETE(m_bombs);
    CC_SAFE_DELETE(m_bomb_ghost);
    CC_SAFE_DELETE(m_cube_array);
    CC_SAFE_DELETE(m_bonus);
    CC_SAFE_DELETE(m_gui_game);
    CC_SAFE_DELETE(m_gui_pause);
    CC_SAFE_DELETE(m_gui_complite);
    CC_SAFE_DELETE(m_gui_failed);
  //  CC_SAFE_DELETE(m_accelerometer);
    CC_SAFE_DELETE(m_share);
    // label_pool.clear();
    if(_full)
    {
        this->removeAllChildrenWithCleanup(true);
        //this->removeFromParentAndCleanup(true);
    };
    ObjCCalls::everyplayStopRecording();
    
};

CCScene* CKGameSceneNew::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    // add layer as a child to scene
    CKGameSceneNew* layer = new CKGameSceneNew();
    if(layer->init())
    {
        //layer->retain();
        layer->initGame();
        scene->addChild(layer);
        layer->setTag(LAYERID_GAMESCENE);
        layer->setTouchEnabled(true);
        CCDirector::sharedDirector()->getAccelerometer()->setDelegate(layer);
        layer->release();
    };
    CKHelper::Instance().compliteLoadScene(scene);
    return scene;
};


void CKGameSceneNew::draw()
{

};

void CKGameSceneNew::restart()
{
    if(!pause_menu_sprite->isVisible())
    {
        if(game_state == GAME_OVER)
            ObjCCalls::sendEventGA("lvl_crash","restart_lvl","lvl_crash",1);//GA Фіксуємо кількість перегравань левела після крешу
    }
    
    CKHelper::Instance().lvl_number = curent_level_id;
    CKHelper::Instance().setWorldNumber(curent_world_id);
    unscheduleUpdate();
    CCDirector::sharedDirector()->getTouchDispatcher()->removeAllDelegates();
    
    this->stopAllActions();
    clearScene(false);
    this->setTouchEnabled(false);
    
    CCDirector::sharedDirector()->pause();
    CCScene *pScene = CKGameSceneNew::scene();
    //   // run
    CCDirector::sharedDirector()->replaceScene(pScene);
    CCDirector::sharedDirector()->resume();
};

bool CKGameSceneNew::initGame()
{
    win_size = CCDirector::sharedDirector()->getWinSize();
    
    CKFileInfo::Instance().getLvlOpenWithWorld(CKHelper::Instance().getWorld(CKHelper::Instance().getWorldNumber()));
    CKFileInfo::Instance().setLevel(CKHelper::Instance().lvl_number);
    
    curent_level_id = CKHelper::Instance().lvl_number;
    curent_world_id = CKHelper::Instance().getWorldNumber();
    if (!is_ipad) {
        ground_height = 16;
        if(win_size.width == 568)
        {
            panel_left = 44;
        }
    }
    else
        ground_height = 74;
    
    font_name = "Agent Orange";
    
    CCLOG("CCFileUtils::sharedFileUtils()->getResourceDirectory() %s",CCFileUtils::sharedFileUtils()->getResourceDirectory());
    
    if(CKHelper::Instance().menu_number == CK::SCENE_GAME)
    {
        CKAudioMng::Instance().stopBackground();
    };
    
    load();
    
    return true;
};
void CKGameSceneNew::onEnter()
{
    CCLayer::onEnter();
    if(CKFileOptions::Instance().isPlayEffect())
    {
        CKAudioMng::Instance().resumeAllEffect();
        
        if(!CKAudioMng::Instance().isPlayEffect("plane_fly"))
        {
            CKAudioMng::Instance().playEffect("plane_fly");
        };
    };
    if(CKFileOptions::Instance().isPlayMusic())
    {
        CKAudioMng::Instance().playBackground("bg",0.0);
        CKAudioMng::Instance().fadeBgToVolume(1.0);
    }
    if(script_scene)
    {
        script_scene->setEnable(true);
    }
};


CKGameSceneNew::CKGameSceneNew()
{

    //ObjCCalls::setMultiTouch(true);
    CCLOG("\n______________________[CKGameSceneNew]____________________________\n");
    timeval t;
    gettimeofday(&t,NULL);
    time_start = t.tv_sec;
    m_emiter_snow = NULL;
    is_ipad = CKHelper::Instance().getIsIpad();
    this->setTag(LAYERID_GAMESCENE);
    
    game_state = GAME_RUN;
    is_posted_in_facebook = false;
    
    count_accuvate_shots = 0;
    count_active_touch = 0;
    count_create_default_bomb = 0;
    
    have_everyplay_record = CKFileOptions::Instance().isEveryPlayShow();
    
    BOX_SIZE = CKHelper::Instance().BOX_SIZE;
    
    switch (CKHelper::Instance().getWorldNumber()) {
        case 1:
            CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneCaramel);
            break;
        case 2:
            CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneChoco);
            break;
        case 3:
            CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneIce);
            break;
        case 4:
            CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneWaffles);
            break;
        default:
            break;
    };
    
    
    banner_height = 0.0;
    pause_time = 0;
    count_special_bomb = 0;
    curent_koef_cube_destroy = 0;
    score = 0;
    score = 0;
    score_active = false;
    end_lvl = false;
    is_hiscore = false;
    last_score = 0;
    m_gui_game = NULL;
    m_gui_pause = NULL;
    m_gui_complite = NULL;
    m_gui_failed = NULL;
    label_score = NULL;
    m_bombs = NULL;
    progress_sprite[0] = NULL;
    progress_sprite[1] = NULL;
    m_bomb_ghost = NULL;
    message_sprite = NULL;
    rt = NULL;
    baner_showed = false;
    m_cube_array = NULL;
    m_airplane = NULL;
    script_scene = NULL;
    m_share = NULL;
    m_backnode = NULL;
    cub_mask = NULL;
    panel_left = 0;
    // this->setTouchEnabled(false);
    this->setAccelerometerEnabled(false);
};


//#define CC_TEXTURE_NPOT_SUPPORT 1
void CKGameSceneNew::initParallax()
{
    CCLOG("CKGameSceneNew::initParallax:: start with plist world %s",CKHelper::Instance().getWorld().c_str());
    CCAssert(!m_backnode, "init rdy");
    CCDictionary *dict = CCDictionary::createWithContentsOfFile(CKHelper::Instance().getWorld().c_str());
    CCDictionary * dict_meta = (CCDictionary *)dict->objectForKey("meta");
    airplane_name = dictStr(dict_meta, "airplane");
    bomb_name = dictStr(dict_meta, "bomb");
    file_bach =  dictStr(dict_meta, "image");
    m_backnode = new CKParalaxNode;

    m_backnode->create(CCNode::create());
    m_backnode->load(loadFile(dictStr(dict_meta, "parallax"),true).c_str());
    CC_SAFE_RELEASE_NULL(dict);

    createLabelScorePool(this);

    //reorder ground
    for (int i = 31 ; i < 40; ++i) {
        CKObject* tmp = m_backnode->getChildByTag(i);
        if(tmp)
        {
            tmp->getNode()->getParent()->removeChild(tmp->getNode(),true);
            this->addChild(tmp->getNode(),CK::zOrder_Airplane);
        }
    };

    if(ObjCCalls::isLowCPU() && CCDirector::sharedDirector()->getContentScaleFactor() != 1)
    {
        rt = CCRenderTexture::create(win_size.width, win_size.height, kCCTexture2DPixelFormat_RGB565, GL_DEPTH24_STENCIL8_OES);
        rt->setPosition(ccp(win_size.width/2, win_size.height/2));
        rt->retain();
        rt->begin();
        m_backnode->get()->visit();
        rt->end();
        m_backnode->get()->removeFromParentAndCleanup(true);
        CC_SAFE_DELETE(m_backnode);
        addChild(rt,0);
        rt->release();
    }
    else
    {
       // m_backnode->get()->setVisible(false);
        addChild(m_backnode->get(),0);
    };
};

void CKGameSceneNew::initAnimationParallax()
{
    if(m_backnode)
    {
        srand(time(0));
        if(CKHelper::Instance().getWorld() == "marmelad.plist")
        {
            for (int i = 81; i < 84; i++) {
                CCSprite * sprite = static_cast<CCSprite *>(m_backnode->getChildByTag(i)->getNode());
                action_node.push_back(sprite);
                float distance = sprite->getPositionY()/win_size.height*40.0;
                sprite->runAction(CCSequence::create(CCSpawn::create(CCMoveTo::create(distance, ccp(sprite->getPositionX(),0)),CCScaleTo::create(distance, -0.2),NULL),CCCallFuncND::create(this, callfuncND_selector(CKGameSceneNew::newLoopCloud),(void *)sprite),NULL));
            };
            CCSprite * sprite = static_cast<CCSprite *>(m_backnode->getChildByTag(91)->getNode());
            sprite->runAction(CCRepeatForever::create(CCRotateBy::create(30.0, 180)));
            
            for (int i = 0; i < 30; ++i) {
                CCSprite *sprite = CCSprite::createWithSpriteFrameName("shineStar.png");
                this->addChild(sprite,zOrderParticle);
                sprite->setOpacity(0);
                shine_star_pool.push_back(sprite);
            };
            loadPositionPool("marmelad_stars.plist");
            
            for (int i = 0; i < 10; i++) {
                createShineStar(NULL,NULL);
            };
        };
        
        if(CKHelper::Instance().getWorld() == "choco.plist")
        {
            //cloud
            for (int i = 81; i < 89; i++) {
                CKObject *object = m_backnode->getChildByTag(i);
                if(!object)
                    continue;
                CCSprite * sprite = static_cast<CCSprite *>(m_backnode->getChildByTag(i)->getNode());
                
                sprite->runAction(CCSequence::create(CCDelayTime::create(rand()%10 + rand()%10*0.02),CCCallFuncND::create(this, callfuncND_selector(CKGameSceneNew::actionCloudChocko),(void *)sprite),NULL));
                action_node.push_back(sprite);
            };
            
            //bubble animation
            loadPositionPool("ckoko_bub_pos.plist");
            for (int i = 91; i <= 94; i++) {
                CCSprite * sprite = static_cast<CCSprite *>(m_backnode->getChildByTag(i)->getNode());
                sprite->setOpacity(0);
                sprite->setUserData(m_backnode->getChildByTag(i));
                sprite->setTag(sprite->getZOrder());
                newBubleAnim(NULL,sprite);
            }
            
            for (int i = 0; i < 6; i++) {
                CCParticleSystem* m_emitter = CCParticleSystemQuad::create(CK::loadFile("buble_chocko.plist").c_str());
                emitter_pool.push_back(m_emitter);
                m_emitter->setTexture(CCTextureCache::sharedTextureCache()->addImage(CK::loadFile("bubble.png").c_str()));
                m_emitter->stopSystem();
                m_emitter->setAutoRemoveOnFinish(false);
                m_backnode->get()->addChild(m_emitter,32);
            }
        };
        
        if(CKHelper::Instance().getWorld() == "ice.plist")
        {
            m_emiter_snow = CCParticleSystemQuad::create(CK::loadFile("snow_ice.plist").c_str());
            m_emiter_snow->setTexture(CCTextureCache::sharedTextureCache()->addImage(CK::loadImage("snow_ice_particle.png").c_str() ));
            m_emiter_snow->setPosition(win_size.width/2, win_size.height);
            m_emiter_snow->setAutoRemoveOnFinish(false);
            this->addChild(m_emiter_snow ,zOrder_Bonuses);
            
            for (int i = 0; i < 30; ++i) {
                CCSprite *sprite = CCSprite::createWithSpriteFrameName("shineStar.png");
                this->addChild(sprite,zOrderParticle);
                sprite->setOpacity(0);
                shine_star_pool.push_back(sprite);
            };
            
            loadPositionPool("ShigningStar.plist");
            
            for (int i = 0; i < 10; i++) {
                createShineStar(NULL,NULL);
            };
            
            for (int i = 81; i < 85; i++) {
                CCSprite * sprite = static_cast<CCSprite *>(m_backnode->getChildByTag(i)->getNode());
                sprite->setBlendFunc((ccBlendFunc){GL_ONE,GL_ONE});
                int r = 255/3*(i-81);
                CCLOG("setOpacity %d",r);
                sprite->setOpacity(r);
                sprite->runAction(CCCallFuncND::create(this, callfuncND_selector(CKGameSceneNew::shineIce),(void *)sprite));
                action_node.push_back(sprite);
            }
        };
        if(CKHelper::Instance().getWorld() == "vafli.plist")
        {
            CCSprite *sprite  = static_cast<CCSprite *>(m_backnode->getChildByTag(91)->getNode());
            float keof_skew = 0.02;
            
            sprite->runAction(CCSequence::create(CCSkewBy::create(5.0, win_size.width*0.5*keof_skew, 0),CCCallFuncND::create(this, callfuncND_selector(CKGameSceneNew::skewCloud),sprite),NULL));
        }
    };
}

void CKGameSceneNew::incLevelWorld()
{
    if(CKFileOptions::Instance().isKidsModeEnabled())
    {
        if(CKHelper::Instance().lvl_number < COUNT_LEVEL_IN_WORLD)
        {
            if(CKFileInfo::Instance().getCurentWorld()->open_lvl > CKHelper::Instance().lvl_number)
            {
                CKHelper::Instance().lvl_number++;
            }
        }
    }
    else
    {
        if(CKHelper::Instance().lvl_number < COUNT_LEVEL_IN_WORLD)
        {
            CKHelper::Instance().lvl_number++;
        }
    }
    
    if(CKHelper::Instance().lvl_number == COUNT_LEVEL_IN_WORLD)
    {
        CKHelper::Instance().lvl_number = 0;
        CKHelper::Instance().lvl_speed++;
        if(CKHelper::Instance().lvl_speed >= COUNT_DIFFICULT)
            CKHelper::Instance().lvl_speed = 0;
    };
    
    CKFileInfo::Instance().setCurentOpenLevel(CKHelper::Instance().lvl_number);
};
void CKGameSceneNew::createLabelScorePool(CCNode *_node)
{
    for(int i = 0; i < 10; i++ )
    {
        CCLabelTTF *tmp = CCLabelTTF::create("0", font_name.c_str(), BOX_SIZE/2);
        label_pool.push_back(tmp);
        tmp->setVisible(false);
        tmp->setColor((ccColor3B){220,255,20});
        _node->addChild(tmp,zOrder_LabelInfo);
    };
};

void CKGameSceneNew::hideNode(CCNode *_sender)
{
    _sender->setVisible(false);
};

void CKGameSceneNew::showLabelScore(const CCPoint& _pos,int _score)
{
    if(CKFileOptions::Instance().isKidsModeEnabled())
        return;
    label_pool_it = label_pool.begin();
    while (label_pool_it != label_pool.end()) {
        if(!(*label_pool_it)->isVisible())
        {
            (*label_pool_it)->setPosition(_pos);
            (*label_pool_it)->setVisible(true);
            (*label_pool_it)->setScale(0.0);
            (*label_pool_it)->setOpacity(255);
            CCFiniteTimeAction *move = CCMoveTo::create(0.7/float(CKHelper::Instance().lvl_speed+1), ccp((*label_pool_it)->getPositionX(),(*label_pool_it)->getPositionY() + BOX_SIZE));
            CCFiniteTimeAction *scale = CCScaleTo::create(0.9/float(CKHelper::Instance().lvl_speed+1), 1.0);
            CCFiniteTimeAction *swap = CCSpawn::create(move,scale, NULL);
            CCFiniteTimeAction *fade = CCFadeOut::create(0.4/float(CKHelper::Instance().lvl_speed+1));
            CCFiniteTimeAction *func = CCCallFuncN::create((*label_pool_it), callfuncN_selector(CKGameSceneNew::hideNode));
            CCAction *seq = CCSequence::create(swap,fade,func,NULL);
            (*label_pool_it)->runAction(seq);
            
            char str[NAME_MAX];
            sprintf(str,"+%d",_score);
            (*label_pool_it)->setString(str);
            switch (curent_koef_cube_destroy) {
                case 2:
                    (*label_pool_it)->setColor((ccColor3B){0,133,0});
                    break;
                case 3:
                    (*label_pool_it)->setColor((ccColor3B){236,252,0});
                    break;
                case 4:
                    (*label_pool_it)->setColor((ccColor3B){255,188,0});
                    break;
                case 5:
                    (*label_pool_it)->setColor((ccColor3B){255,116,0});
                    break;
                case 6:
                    count_accuvate_shots++;
                    (*label_pool_it)->setColor((ccColor3B){255,0,0});
                    break;
                default:
                    (*label_pool_it)->setColor((ccColor3B){0,133,0});
                    break;
            };
            
            return;
        }
        label_pool_it++;
    };
};
void CKGameSceneNew::skewCloud(CCNode *_sender,void *_d)
{
    float keof_skew = 0.02;
    ((CCSprite *)_d)->runAction(CCSequence::create(CCSkewBy::create(10.0, -win_size.width*keof_skew,0),CCSkewBy::create(10.0, win_size.width*keof_skew, 0),CCCallFuncND::create(this, callfuncND_selector(CKGameSceneNew::skewCloud),_d),NULL));
};

void CKGameSceneNew::loadPositionPool(const char *_file_name)
{
   CCDictionary* dict = CCDictionary::createWithContentsOfFile(CK::loadFile(_file_name).c_str());
    
    CCDictionary* frameDic = (CCDictionary*)dict->objectForKey("frames");
    if(!frameDic)
    {
        return;
    };
    CCDictElement* pElement = NULL;
    position_pool.clear();
    CCDICT_FOREACH(frameDic, pElement)
    {
        CCDictionary *elem =(CCDictionary*)pElement->getObject();
        position_pool.push_back(CCPointFromString(dictStr(elem, "Position")));
    };
};

void CKGameSceneNew::createShineStar(CCNode *_sender,CCSprite* _sprite)
{
        std::list<CCSprite* >::iterator s_it = shine_star_pool.begin();
        while (s_it != shine_star_pool.end()) {
            if((*s_it)->getOpacity() == 0)
            {
                (*s_it)->stopAllActions();
                (*s_it)->setOpacity(1);
                (*s_it)->setScale(1.0);
                CCAction * func_call = NULL;
                if(!_sprite)
                    func_call = CCCallFuncND::create(this, callfuncND_selector(CKGameSceneNew::createShineStar),NULL);
                
                (*s_it)->runAction(CCSequence::create(CCDelayTime::create(rand()%35*0.1),CCFadeIn::create(0.01),CCSpawn::create(CCRotateBy::create(1.2, rand()%360 - 180),CCScaleTo::create(0.7, 0.0),NULL),CCFadeOut::create(0.1),func_call,NULL));
                
                if(_sprite)
                {
                    CCPoint p = _sprite->getPosition();
                    p.x += rand()%(BOX_SIZE/4) - BOX_SIZE/8;
                    p.y += rand()%(BOX_SIZE/4) - BOX_SIZE/8;
                    (*s_it)->setScale(2.0);
                    (*s_it)->setPosition(p);
                }
                else
                {
                    if(!position_pool.empty())
                    {
                        int index = rand()%position_pool.size();
                        (*s_it)->setPosition(position_pool[index]);
                    }
                }
                return;
            };
            s_it++;
        }
};

void CKGameSceneNew::actionCloudChocko(CCNode *_sender,void *_d)
{
    CCSprite * sprite = static_cast<CCSprite *>(_d);

    CCFiniteTimeAction *action1 = CCEaseSineOut::create(CCScaleTo::create(10.0, 1.6));
    CCFiniteTimeAction *action2 = CCEaseSineIn::create(CCScaleTo::create(10.0, 2.2));
    
    sprite->runAction(CCRepeatForever::create( (CCActionInterval*)CCSequence::create(action1,action2,NULL)));
};

void CKGameSceneNew::shineIce(CCNode *_sender,void *_d)
{
    if(end_lvl)
        return;
    
    int k = rand()%25;
    if(k == 0)
    {
        m_emiter_snow->setGravity(ccp(-75,-50));
    } else if(k == 3)
    {
        m_emiter_snow->setGravity(ccp(50,-50));
    }
    else if(k < 20)
    {
        m_emiter_snow->setGravity(ccp(0,-10));
        if (k < 10) {
            m_emiter_snow->setRadialAccelVar(-5);
        }
        else
        {
            m_emiter_snow->setRadialAccelVar(-15);
        }
    };
    
    for (int i = 0; i < m_cube_array->getCountActiveCube()/10; ++i) {
        createShineStar(NULL,m_cube_array->getRndVisibleMiniCube());
    };
    
    CCSprite * sprite = (CCSprite *)_d;
    float time_hide = (sprite->getOpacity()/255.0)*6.0;
    float time_show = 6.0;
    
    float value = (is_ipad)?12:6;

    float koef_move = (rand()%2 == 0)?-value:value;
    float move_hide = -koef_move*time_show;
    float move_show = koef_move*time_show;
    
    CCFiniteTimeAction * actionFade = CCSequence::create(CCFadeOut::create(time_hide),CCFadeIn::create(time_show/2),CCDelayTime::create(time_show/2),NULL);
    CCFiniteTimeAction * actionMove = CCSequence::create(CCEaseSineInOut::create(CCMoveBy::create(time_hide, ccp(move_hide,0))),CCEaseSineInOut::create(CCMoveBy::create(time_show, ccp(move_show,0))),NULL);
    CCFiniteTimeAction * action = CCSpawn::create(actionFade,actionMove,NULL);
    
    sprite->runAction(CCSequence::create(action,CCCallFuncND::create(this, callfuncND_selector(CKGameSceneNew::shineIce),(void *)sprite),NULL));
};

void CKGameSceneNew::newBubleAnim(CCNode *_sender,void *_d)
{
    if(end_lvl)
        return;
    CCSprite * sprite = (CCSprite *)_d;
  
    sprite->getParent()->reorderChild(sprite, sprite->getTag());
    sprite->setScaleX(0.35);
    sprite->setScaleY(0.0);
    sprite->stopAllActions();
    
    if(!position_pool.empty())
    {
        int index = rand()%position_pool.size();
      //  sprite->setPosition(position_pool[index]);
       // ((CKObject *)sprite)->savePosition(position_pool[index]);
        ((CKObject *)sprite->getUserData())->setPosition(position_pool[index].x, position_pool[index].y);
    };
    
    float scale = rand()%5*0.1 + 0.6;//sprite->getScale();
    CCAnimation* fly_anim = CCAnimation::create();
    char str[NAME_MAX];
    for(int i = 0; i < 9; ++i )
    {
        sprintf(str, "bulb%d.png",i+1);
        fly_anim->addSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str));
    };
    sprintf(str, "bulb%d.png",12);
    fly_anim->addSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str));
    fly_anim->setDelayPerUnit(0.075);
    CCAnimate* animate = CCAnimate::create(fly_anim);
    
    CCSpriteFrame* frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("bulb1.png");
    sprite->setTextureRect(frame->getRect(),frame->isRotated(),sprite->getContentSize());

    sprite->runAction(CCSequence::create(CCDelayTime::create(scale*(rand()%12)),CCFadeIn::create(0.01),CCScaleTo::create(scale*5.0,scale,scale),CCCallFuncND::create(this, callfuncND_selector(CKGameSceneNew::createEmitterChocko),(void *)sprite),animate,CCCallFuncND::create(this, callfuncND_selector(CKGameSceneNew::circleChocko),(void *)sprite),CCSpawn::create(CCScaleBy::create(scale*6.0,scale*4.0),CCFadeOut::create(scale*6.0),NULL),CCCallFuncND::create(this, callfuncND_selector(CKGameSceneNew::newBubleAnim),(void *)sprite),NULL));
};

void CKGameSceneNew::circleChocko(CCNode *_sender,void *_d)
{
    CCSprite * sprite = (CCSprite *)_d;
    sprite->getParent()->reorderChild(sprite, 28);
};

void CKGameSceneNew::createEmitterChocko(CCNode *_sender,void *_d)
{
   // CCLOG("createEmitterChocko");
    CCSprite * sprite = (CCSprite *)_d;
    std::list<CCParticleSystem*>::iterator emitter_it = emitter_pool.begin();
    while (emitter_it != emitter_pool.end()) {
        if(!(*emitter_it)->isActive())
        {
            (*emitter_it)->setScale(sprite->getScaleX());
            (*emitter_it)->setPosition(sprite->getPosition());
            (*emitter_it)->resetSystem();
            return;
        }
        emitter_it++;
    };
};

void CKGameSceneNew::newLoopCloud(CCNode *_sender,void *_d)
{
    CCSprite * sprite = (CCSprite *)_d;
    sprite->setPosition(ccp(sprite->getPositionX(),win_size.height*1.1));
    sprite->setScale(2.0);
    
    for (int i = 0; i < m_cube_array->getCountActiveCube()/5; ++i) {
        createShineStar(NULL,m_cube_array->getRndVisibleMiniCube());
    };
    
    float distance = sprite->getPositionY()/win_size.height*40.0;
    sprite->runAction(CCSequence::create(CCSpawn::create(CCMoveTo::create(distance, ccp(sprite->getPositionX(),0)),CCScaleTo::create(distance, -0.2),NULL),CCCallFuncND::create(this, callfuncND_selector(CKGameSceneNew::newLoopCloud),(void *)sprite),NULL));
};

//void CKGameSceneNew::onEnter()
//{
//    CCLayer::onEnter();
//};

void CKGameSceneNew::load()
{
    
    //retain();
    if(!CKFileOptions::Instance().isDebugMode())
    {
        CKHelper::Instance().lvl_speed = curent_world_id - 1;
    };
    setVisible(true);
    timeval t;
    gettimeofday(&t,NULL);
    time_start = t.tv_sec;
    CCLOG("CKGameSceneNew::load:: Start load scene ------------------,time %d",time_start);

   // CCDirector::sharedDirector()->getTouchDispatcher()->addStandardDelegate(this, 0);
    //m_accelerometer = new CKAccelerometerDelegat;
    //m_accelerometer->m_scene = this;
    
   
    //removeAllChildrenWithCleanup(true);
    
//    CCTexture2D *mar_t = CCTextureCache::sharedTextureCache()->textureForKey("marmelad_world@2x.png");
//    if(mar_t)
//    {
//        CCLOG("marmelad_world@2x.png mar_t %d",mar_t->retainCount());
//    };
//
    
    initParallax();
    initAnimationParallax();
    
    
    
//    mar_t = CCTextureCache::sharedTextureCache()->textureForKey("marmelad_world@2x.png");
//    if(mar_t)
//    {
//        CCLOG("marmelad_world@2x.png mar_t %d",mar_t->retainCount());
//    };
//    mar_t = CCTextureCache::sharedTextureCache()->textureForKey("marmelad_world@2x.png");
//    if(mar_t)
//    {
//        CCLOG("marmelad_world@2x.png mar_t %d",mar_t->retainCount());
//    };
    
    initGamePanelGui();
    initPauseGui();

    initLevelFailedGui();
    initLevelCompliteGui();
    
    initPanelBomb();
    initPanelAirplane();
    
    CKHelper::Instance().ground_height = ground_height;
    m_cube_array = new CKCubeArray;
    //m_cube_array->setPanelSize(0);
    m_cube_array->setLayer(this);
    m_cube_array->createBatch(CK::loadImage(file_bach.c_str()).c_str());
    //_cub->setVisible(false);
    m_cube_array->setPanelSize(panel_left);
    
    m_cube_array->setGroundHeight(ground_height);
    m_cube_array->parsingLevel("");
    m_cube_array->createLevel(CKHelper::Instance().lvl_speed,CKHelper::Instance().lvl_number);
    
    m_bonus = new CKBonus;
    m_bonus->setCubeArray((CKCubeArray*)m_cube_array);
    m_bonus->setLayer(this);
    m_cube_array->setBonus(m_bonus);
    
    m_bonus->genBonus(0);  //load bonus with plist
    //create bombs object
    m_bombs = new CKBombs;
    m_bomb_ghost = new CKBombs;
    

    CCLOG("initPanelBomb::m_bonus->getFirstBonus() %d",m_bonus->getFirstBonus());

    initAirpalne();//after init bomb
    limitet_top_zone_touch =  win_size.height*0.85;
    initBanner(m_airplane->getAirplaneHeight());
    
    m_bombs->setBombName(bomb_name);
    m_bomb_ghost->setBombName(bomb_name);
    m_bombs->setHeight(ground_height);
    m_bomb_ghost->setHeight(ground_height);
    m_bombs->setLayer(this);
    m_bomb_ghost->setLayer(this);
    m_bombs->setSpeed(BOX_SIZE*5);
    m_bomb_ghost->setSpeed(BOX_SIZE*5);
    m_bombs->init();
    m_bomb_ghost->init();
    m_bombs->setAirplane(m_airplane);
    m_bomb_ghost->setAirplane(m_airplane);
    m_bombs->setBonus(m_bonus);
    m_bomb_ghost->setBonus(m_bonus);
    m_bombs->setCubeArray(m_cube_array);
    m_bomb_ghost->setCubeArray(m_cube_array);
   // CCNode * node = CCNode::create();
    m_bombs->createDestroyEffect();
    m_bomb_ghost->createDestroyEffect();
    //this->addChild(node,CK::zOrderParticle);
    
    score_destroy_one_cube = (CKHelper::Instance().lvl_speed + 1) * 4;
    
   // CKStaticFile::Instance().setStartInfo(ObjCCalls::getUserName(),(CKHelper::Instance().select_world + 1), CKHelper::Instance().lvl_number + 1, CKHelper::Instance().lvl_speed + 1,m_airplane->getAirplaneHeight());
  //  CKStaticFile::Instance().setStartTime(time(0));
    
    bg_mask = NULL;
    bg_mask = CCSprite::create("gray_mask.png");
    bg_mask->setPosition(ccp(win_size.width/2,win_size.height/2));
    bg_mask->setScaleX(win_size.width/bg_mask->getContentSize().width);
    bg_mask->setScaleY(win_size.height/bg_mask->getContentSize().height);
    bg_mask->setVisible(false);
    addChild(bg_mask,zOrder_Bonus);
   // CCTexture2D::setDefaultAlphaPixelFormat(kCCTexture2DPixelFormat_RGBA8888);
    
    game_state = GAME_RUN;
    
    initGuiShare();
    
    message_sprite =  CKHelper::Instance().getDialog()->getInfoGui()->get();
    
    if(!showTutorialScript())
    {
#if ENABLE_EVERYPLAY
        if(CKFileOptions::Instance().isEveryPlayShow())
            ObjCCalls::everyplayStartCapture();
#endif
        scheduleUpdate();
    }
    else
    {
       bg_mask->setVisible(true);
    };
    
    gettimeofday(&t,NULL);
    time_start = t.tv_sec;
    CCLOG("CKGameSceneNew::load:: End load scene ------------------ time %d",time_start);

  //  CCTextureCache::sharedTextureCache()->removeUnusedTextures();
    CCTextureCache::sharedTextureCache()->dumpCachedTextureInfo();

    //send to Flurry static
    ObjCCalls::logPageView();
    
    ObjCCalls::sendEventFlurry("lvl_start",false,"num_airplane_speed=%d,num_playing_world=%d,num_playing_lvl=%d",int(m_airplane->getSpeed()*100),(CKHelper::Instance().getWorldNumber() + 1),(CKHelper::Instance().lvl_number + 1)*(CKHelper::Instance().getWorldNumber()+1));
    
    //send static to GA
    ObjCCalls::logPageView(true,"/StartGame");
    ObjCCalls::sendCustomGA(5,"num_playing_speed",(CKHelper::Instance().lvl_speed + 1)*100);
    ObjCCalls::sendCustomGA(6,"num_playing_world",CKHelper::Instance().getWorldNumber() + 1);
    ObjCCalls::sendCustomGA(7,"num_playing_lvl",(CKHelper::Instance().lvl_number + 1)*(CKHelper::Instance().getWorldNumber()+1));
    
    
    if(CKFileOptions::Instance().isPlayEffect())
        CKAudioMng::Instance().pauseAllEffect();
    
    m_audio.initAudio(CKHelper::Instance().menu_number);
    CKHelper::Instance().menu_number = CK::SCENE_GAME;
    
    // CCLOG("CKGameSceneNew:: getSizeAllChilden %d ",CKHelper::Instance().getSizeAllChilden(this,"\t"));
};

void CKGameSceneNew::initLabelSysInfo()
{
    char str[NAME_MAX];
    sprintf(str, "L#%d D#%d S#%.2f",CKHelper::Instance().lvl_number + 1,CKHelper::Instance().lvl_speed + 1,m_airplane->getSpeed()/BOX_SIZE);
    label_info = CCLabelTTF::create(str, "Arial", BOX_SIZE/3);
    label_info->setPosition(ccp( win_size.width*0.9, win_size.height*0.8));
    label_info->setColor(ccc3(255,255,255));
  //  label_info->setVisible(false);
    addChild(label_info, 15);
    
    sprintf(str, "%dKb CPU %.2f",CKSystemInfo::Instance().getFreeMemory()/1024,CKSystemInfo::Instance().getCPU());
    label_sysinfo = CCLabelTTF::create(str, "Arial", BOX_SIZE/4);
    label_sysinfo->setPosition(ccp( win_size.width*0.9, win_size.height*0.75));
    label_sysinfo->setColor(ccc3(255,255,255));
   // label_sysinfo->setVisible(false);
    addChild(label_sysinfo, 15);
    
};


void CKGameSceneNew::initBanner(float _plane_height)
{
    if(_plane_height <= SHOW_BANNER_AFTER_HEIGHT && CKFileOptions::Instance().isBannerShow() && !CKFileOptions::Instance().isKidsModeEnabled())
    {
        float k = float(rand()%100)/100.0f; //random persent
        CCLOG("Iad,Admob banner Showed %f %f",CKAdBanner::Instance().koef_showed,k);
        // if(ObjCCalls::isActiveConnection())
        {
            if(CKAdBanner::Instance().koef_showed < k) //iad or adMob
            {
                ObjCCalls::showBanner();
                CKAdBanner::Instance().setDisable(true);
            }
            else // our banner
            {
                CKAdBanner::Instance().setDisable(false);
            }
        };
        
        if(!CKAdBanner::Instance().isDisable())
        {
            CKAdBanner::Instance().generated();
        }
        
        createBanner();
        m_gui_game->get()->setPosition(ccp(m_gui_game->get()->getPositionX(),(is_ipad)?-90:-32));
    };
}



void CKGameSceneNew::functAsunc(CKGameSceneNew * _sender)
{

};

#pragma mark  - INIT
void CKGameSceneNew::initGamePanelGui()
{
     m_gui_game = new CKGUI();
     m_gui_game->create(CCNode::create());
     m_gui_game->load(loadFile("c_GameScreen.plist",true).c_str());
     m_gui_game->get()->setPosition(ccp(0,0));
     m_gui_game->setTarget(this);
     m_gui_game->addFuncToMap("menuCall", callfuncND_selector(CKGameSceneNew::menuCall));
     m_gui_game->addFuncToMap("slotCall", callfuncND_selector(CKGameSceneNew::slotCall));
     m_gui_game->setTouchEnabled(true);
     m_gui_game->getChildByTag(5)->setText("0");
     char str[NAME_MAX];
     sprintf(str,"Level %d",CKHelper::Instance().lvl_number + 1);
     m_gui_game->getChildByTag(6)->setText(str);
     addChild(m_gui_game->get(),zOrder_Panel);
    label_score =  m_gui_game->getChildByTag(5);
    if(CKFileOptions::Instance().isKidsModeEnabled())
        label_score->setVisible(false);
};
void CKGameSceneNew::initPauseGui()
{
    m_gui_pause = new CKGUI();
    m_gui_pause->create(CCNode::create());
    m_gui_pause->load(loadFile("c_Pause.plist").c_str());
    
    m_gui_pause->setTarget(this);
    m_gui_pause->addFuncToMap("menuCall", callfuncND_selector(CKGameSceneNew::menuCall));
    m_gui_pause->setTouchEnabled(true);
    
    CKObject* _node = m_gui_pause->getChildByTag(Action_Music);
    if(_node)
    {
        if(_node->getType() == CK::GuiRadioBtn)
        {
            _node->setStateChild(int(!CKFileOptions::Instance().isPlayMusic()));
        };
    }
    _node = m_gui_pause->getChildByTag(Action_Effect);
    if(_node)
    {
        if(_node->getType() == CK::GuiRadioBtn)
        {
            _node->setStateChild(int(!CKFileOptions::Instance().isPlayEffect()));
        };
    }
    pause_menu_sprite = m_gui_pause->get();
    pause_menu_sprite->setPosition(ccp(-win_size.width*1.0,win_size.height*0.0));

    addChild(m_gui_pause->get(),zOrder_Menu);
};

void CKGameSceneNew::initLevelCompliteGui()
{
    m_gui_complite = new CKGUI();
    m_gui_complite->create(CCNode::create());
    
    if(CKFileOptions::Instance().isKidsModeEnabled())
    {
        m_gui_complite->load(loadFile("c_LevelCompleteKM.plist").c_str());
    }
    else
        m_gui_complite->load(loadFile("c_LevelComplete.plist").c_str());
    m_gui_complite->setTarget(this);
    m_gui_complite->setTouchEnabled(true);
    m_gui_complite->addFuncToMap("menuCall", callfuncND_selector(CKGameSceneNew::menuCall));
    m_gui_complite->addFuncToMap("shopCall", callfuncND_selector(CKGameSceneNew::shopCall));
   // m_gui_complite->getChildByTag(0)->setTouch(true);
    
    CKObject *obj_cup = m_gui_complite->getChildByTag(62);
    
    CCSpriteFrame *frame =  CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("cup.png");
    cub_mask = TBSpriteMask::createWithTexture(frame->getTexture(), frame->getRect(),frame->isRotated());
    cub_mask->setPosition(obj_cup->getPos());
    m_gui_complite->get()->addChild(cub_mask,obj_cup->getzOrder());
    cub_mask->setAnchorPoint(obj_cup->getNode()->getAnchorPoint());
    
    CCTexture2D *texture = CCTextureCache::sharedTextureCache()->addImage(CK::loadImage("cupMask.png").c_str());
    ccTexParams params = {GL_LINEAR,GL_LINEAR,GL_REPEAT,GL_REPEAT};
    texture->setTexParameters(&params);
    cub_mask->buildMaskWithTexture(texture,CCRect(texture->getContentSize().width*0.2, 0, texture->getContentSize().width*0.4, texture->getContentSize().height),false,2);
    m_gui_complite->get()->setVisible(false);
    addChild(m_gui_complite->get(),zOrder_Menu);
    
    if(!CKFileOptions::Instance().isKidsModeEnabled())
    {
        std::map<int,ShopBomb*>::iterator it = CKFileInfo::Instance().bomb_info_map.begin();
        int i = 0;
        while (it != CKFileInfo::Instance().bomb_info_map.end() && i < 3) {
            CCSprite *sprite = static_cast<CCSprite*>(m_gui_complite->getChildByTag(i + 138)->getChildByTag(1)->getNode());
            if(it->second->haveDiscount())
            {
                char str[8];
                sprintf(str, "-%d%s",int(it->second->getMaxDiscount()*100),"%");
                m_gui_complite->getChildByTag(i + 138)->getChildByTag(2)->setText(str);
            };
            m_gui_complite->getChildByTag(i + 138)->setValue(it->second->type);
            CCSpriteFrame* s_frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(CKBonusMgr::Instance().getBonusNameById(it->second->type).c_str());
            sprite->setTexture(s_frame->getTexture());
            sprite->setTextureRect(s_frame->getRect(), s_frame->isRotated(), sprite->getContentSize());
            i++;
            it++;
        }
    }
};

void CKGameSceneNew::shopCall(CCNode* _sender,void *_value)
{
    CKHelper::Instance().show_shop_bomb = *static_cast<int *>(_value);
    int d = CK::Action_Shop;
    menuCall(NULL, &d);
};

void CKGameSceneNew::initLevelFailedGui()
{
    m_gui_failed = new CKGUI();
    m_gui_failed->create(CCNode::create());
    m_gui_failed->load(loadFile("c_LevelFailed.plist").c_str());
    m_gui_failed->setTarget(this);
    m_gui_failed->addFuncToMap("menuCall", callfuncND_selector(CKGameSceneNew::menuCall));
    m_gui_failed->setTouchEnabled(false);
    m_gui_failed->get()->setVisible(false);
    //m_gui_failed->getChildByTag(1)->setTouch(true);
    addChild(m_gui_failed->get(),zOrder_Menu);
    
    if(CKFileOptions::Instance().isEveryPlayShow())
        m_gui_failed->getChildByTag(CK::Action_EveryPlay_Play)->getChildByTag(3)->setVisible(false);
    
};

void CKGameSceneNew::initGuiShare()
{
    m_share = new CKGUI;
    m_share->create(CCNode::create());
    m_share->load(loadFile("c_LevelCompleteAddShare.plist",false).c_str());
    m_share->get()->setPosition(ccp(win_size.width/2,win_size.height/2));
    m_share->setTarget(this);
    m_share->addFuncToMap("menuCall", callfuncND_selector(CKGameSceneNew::menuCall));
    m_share->addFuncToMap("shareCall", callfuncND_selector(CKGameSceneNew::shareCall));
    m_share->setTouchEnabled(false);
    m_share->get()->setVisible(false);
    
    if(CKFileOptions::Instance().isEveryPlayShow())
        m_share->getChildByTag(CK::Action_EveryPlay_Play)->getChildByTag(3)->setVisible(false);
    
    m_share->get()->setPosition(ccp(win_size.width/2,win_size.height/2));
    addChild(m_share->get(),30);
};

void CKGameSceneNew::initAirpalne()
{
    //create new airlpane object
    m_airplane = new CKAirplane;
    m_airplane->setLayer(this);
    m_airplane->setSelector(callfuncND_selector(CKGameSceneNew::callbackAirplane));
    m_airplane->setCubeArray(m_cube_array);
    m_airplane->setBombs(m_bombs);
    m_airplane->setGhostBomb(m_bomb_ghost);
    
    m_airplane->createAnim("plane.png");
    m_airplane->setGroundHeight(ground_height);
    
    if(CKHelper::Instance().lvl_speed == 0 && CKHelper::Instance().lvl_number < 6)
    {
        if(CKHelper::Instance().lvl_number < 2)//lvl 1 and lvl 2
        {
            m_airplane->setSlowFall(-3,3,2,true);
        }
        else //lvl 3 and lvl 4
            if(CKHelper::Instance().lvl_number == 2 || CKHelper::Instance().lvl_number == 3)
            {
                m_airplane->setSlowFall(-1,3,2,true);
            }
            else
            {
                m_airplane->setSlowFall(0,3,2,true);
            }
    }
    else
        m_airplane->setSlowFall(0,0,0);
    m_airplane->setBaseSpeed(m_airplane->getSpeed());
};

void CKGameSceneNew::showSlotBomb(PanelObject &_slot,int _count,int _type)
{
    _slot.count = _count;
    CCLOG("CKGameSceneNew::showSlot %d %d",_count,_type);
    _slot.type = _type;
    _slot.bomb->setVisible(true);
    _slot.bomb->setScale(1.0);
    _slot.label->setVisible(true);
    CCSpriteFrame* s_frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(CKBonusMgr::Instance().getBonusNameById(_type).c_str());
    _slot.bomb->setTexture(s_frame->getTexture());
    _slot.bomb->setTextureRect(s_frame->getRect(), s_frame->isRotated(), _slot.bomb->getContentSize());
    char str[NAME_MAX];
    _slot.slot->setTouch(true);
    sprintf(str,"%d",_slot.count);
    _slot.label->setString(str);
    
    _slot.slot->getChildByTag(2)->setVisible(true);
    _slot.slot->getChildByTag(7)->setVisible(true);
    _slot.slot->getChildByTag(8)->setVisible(true);
    _slot.slot->getChildByTag(3)->setVisible(true);
    _slot.slot->getChildByTag(4)->setVisible(true);
    
    switch (_type%10) {
        case 1:
            _slot.slot->getChildByTag(7)->setText("|");
            break;
        case 2:
            _slot.slot->getChildByTag(7)->setText("||");
            break;
        case 3:
            _slot.slot->getChildByTag(7)->setText("|||");
            break;
        default:
            break;
    }
};

void CKGameSceneNew::clearSlotBomb(PanelObject &_slot)
{
    _slot.slot->getChildByTag(2)->setVisible(false);
    _slot.slot->getChildByTag(7)->setVisible(false);
    _slot.slot->getChildByTag(8)->setVisible(false);
    _slot.slot->getChildByTag(3)->setVisible(false);
    _slot.slot->getChildByTag(4)->setVisible(false);
    _slot.slot->getChildByTag(25)->setVisible(false);
    _slot.slot->getChildByTag(25)->getNode()->pauseSchedulerAndActions();
    _slot.slot->getChildByTag(1)->setStateImage(0);
    _slot.slot->getChildByTag(3)->setStateImage(0);
    _slot.slot->getChildByTag(8)->setStateImage(0);
    _slot.slot->setTouch(false);
    _slot.type  = 0;
    _slot.count = 0;
};

void CKGameSceneNew::initPanelBomb()
{
    for(int i = 100;i < 105;++i)
    {
        m_gui_game->getChildByTag(i)->getChildByTag(1)->setStateImage(0);
        bonus_bomb[i - 100].bomb = static_cast<CCSprite *>(m_gui_game->getChildByTag(i)->getChildByTag(4)->getNode());
        bonus_bomb[i - 100].label = static_cast<CCLabelTTF *>(m_gui_game->getChildByTag(i)->getChildByTag(2)->getNode());
        m_gui_game->getChildByTag(i)->getChildByTag(25)->setVisible(false);
        m_gui_game->getChildByTag(i)->getChildByTag(25)->getNode()->runAction(CCRepeatForever::create(CCRotateBy::create(10.0, 360)));
        bonus_bomb[i - 100].type = 0;
        bonus_bomb[i - 100].count = 0;
        bonus_bomb[i - 100].bomb->setVisible(false);
        bonus_bomb[i - 100].label->setVisible(false);
        bonus_bomb[i - 100].slot = m_gui_game->getChildByTag(i);
        bonus_bomb[i - 100].slot->setTouch(false);
        clearSlotBomb(bonus_bomb[i - 100]);
    };
    
    //load bonus with inventory and one bonus of bomb
    curent_bomb = &bonus_bomb[0];
    curent_bomb->slot->getChildByTag(1)->setStateImage(1);
    curent_bomb->slot->getChildByTag(25)->setVisible(true);
    curent_bomb->slot->getChildByTag(25)->getNode()->resumeSchedulerAndActions();
    curent_bomb->bomb->setVisible(true);
    curent_bomb->label->setVisible(true);
    
    
    CCSpriteFrame* s_frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("bomb131.png");
    curent_bomb->bomb->setTexture(s_frame->getTexture());
    curent_bomb->bomb->setTextureRect(s_frame->getRect(), s_frame->isRotated(), curent_bomb->bomb->getContentSize());
    
    curent_bomb->slot->setTouch(true);
    bonus_bomb[0].count = DEFAULT_BOMB_COUNT;
    bonus_bomb[0].type  = 1;
    bonus_bomb[0].label->setVisible(false);
    
    //char str[NAME_MAX];
    //sprintf(str,"%d",bonus_bomb[0].count);
    //curent_bomb->label->setString(str);
   // count_special_bomb = 0;
    
    for(int i = 1; i < COUNT_PANEL_BOMB_CELL; ++i)
    {
        if(CKFileInfo::Instance().isEmptyInvSelect())
            break;

        if(CKFileInfo::Instance().getInvSelect(i - 1)->count <= 0 || CKFileInfo::Instance().getInvSelect(i - 1)->type <= 0)
            continue;
       // CCLOG("initBombPanel:: count %d type %d",CKFileInfo::Instance().getInvSelect(i - 1)->count,CKFileInfo::Instance().getInvSelect(i - 1)->type);
        count_special_bomb += CKFileInfo::Instance().getInvSelect(i - 1)->count;
        showSlotBomb(bonus_bomb[i],CKFileInfo::Instance().getInvSelect(i - 1)->count,CKFileInfo::Instance().getInvSelect(i - 1)->type);
    };
    if(!CKFileOptions::Instance().isKidsModeEnabled())
        CK::StatisticLog::Instance().setSpecBomb(curent_world_id, curent_level_id + 1, count_special_bomb);
};

void CKGameSceneNew::initPanelAirplane()
{
    // const int SIZE_ICON = 150;
    for(int i = 105; i < 105 + COUNT_PANEL_PLANE_CELL; i++)   //create Inventory
    {
        m_gui_game->getChildByTag(i)->getChildByTag(1)->setStateImage(0);
        bonus_airplane[i - 105].slot = m_gui_game->getChildByTag(i);
        bonus_airplane[i - 105].slot->setTouch(false);
       
        bonus_airplane[i - 105].bomb = static_cast<CCSprite *>(m_gui_game->getChildByTag(i)->getChildByTag(4)->getNode());
        bonus_airplane[i - 105].label = static_cast<CCLabelTTF *>(m_gui_game->getChildByTag(i)->getChildByTag(2)->getNode());
        
         clearSlotPlane(bonus_airplane[i - 105]);
        bonus_airplane[i - 105].bomb->setVisible(false);
        bonus_airplane[i - 105].slot->getChildByTag(BONUS_PROGRESS_ID)->setProgress(0.0);
        bonus_airplane[i - 105].slot->getChildByTag(BONUS_PROGRESS_ID)->setVisible(false);
        char str[NAME_MAX];
        sprintf(str,"%d",bonus_airplane[i-105].count);
        bonus_airplane[i-105].label->setString(str);
    };
};


#pragma mark - UPDATE
void CKGameSceneNew::updateInfo(const float &_dt)
{
    char str[NAME_MAX];
    sprintf(str, "%d Kb CPU %.2f",CKSystemInfo::Instance().getFreeMemory()/1024,CKSystemInfo::Instance().getCPU());
    label_sysinfo->setString(str);
};

void CKGameSceneNew::updateBomb(const float &dt)
{
    //update bomb
    std::list<CCNode*>* n_bonus_list = m_bonus->getOpenBonus(); //m_bombs->getBonusCheck();
    std::list<CCNode*>::iterator it;
    it = n_bonus_list->begin();
    while (it != n_bonus_list->end())
    {
        CKAudioMng::Instance().playEffect("take_bonuses");
        bonus_drag_up = true;
        runBonusAnimation(*it);
        it++;
    };
    n_bonus_list->clear();

    if(m_bomb_ghost && m_bomb_ghost->isActiveBomb())
    {
        n_bonus_list = m_bomb_ghost->getBonusCheck();
        it = n_bonus_list->begin();
        while (it != n_bonus_list->end())
        {
            runBonusAnimation(*it);
            it++;
        };
        n_bonus_list->clear();
    }
    
    //tremor parallax
    if(m_bombs)
    {
            if(m_bombs->update(dt) == 1 && m_backnode && m_airplane->getLastBombID() != 67)
            {
                CCLOG("m_airplane->getLastBombID() %d",m_airplane->getLastBombID());
                m_backnode->runAction(m_bombs->getPosition());
            }
    }
    if(m_bomb_ghost && m_bomb_ghost->isActiveBomb())
    {
        if(m_bomb_ghost->update(dt) == 1 && m_backnode)
            m_backnode->runAction(m_bombs->getPosition());
    }
    
};

void CKGameSceneNew::update(float dt)
{
    if(cub_mask)
    {
        cub_mask->moveMask(0.0, 0.05);
    }
    
    if(game_state != GAME_RUN)    //don't update if pause
        return;

    m_bonus->update(dt);
    
    if(!m_airplane->isDoubleSpeed() && count_active_touch == 1 && m_bombs->isNewBomb())
    {
        if(int(curent_bomb->type/10) != 5 && int(curent_bomb->type/10) != 7) // not lazer and not touch bomb
        if(m_airplane->canAutoShot())
        {
            if(!m_gui_game || !m_gui_game->getTouchObject(touch_position))
            {
                touchAirplane(touch_position);
                m_bombs->touchAutoFind(touch_position);
                if(m_bomb_ghost->isActiveBomb())
                    m_bomb_ghost->touchAutoFind(touch_position);
            }
        }
    };
    
    m_airplane->update(dt);
   
    updateBonusProgress(dt);
    
    if(m_airplane->endAnimation() != 0)
    {
        m_gui_game->setTouchEnabled(false);
        if(m_gui_game->getChildByTag(CK::Action_Pause))
        m_gui_game->getChildByTag(CK::Action_Pause)->setEnabled(false);
    };
    updateBomb(dt);

    int tmp_score = m_cube_array->getScore()*score_destroy_one_cube;
    if(tmp_score > 0)
    {
        float score_value = tmp_score + tmp_score*CCRANDOM_MINUS1_1()*ADD_SCORE_PERSENT;

        curent_koef_cube_destroy = m_cube_array->getDestroyCube();
        if(curent_koef_cube_destroy > 1)
        {
            if(curent_koef_cube_destroy > MAX_KOEF_DESTROY_COUNT_CUBE)
            {
                curent_koef_cube_destroy = MAX_KOEF_DESTROY_COUNT_CUBE;
            }
            score_value *= (1.0 + (curent_koef_cube_destroy - 1)*0.4);
            score_value +=0.5;
        };
        CCLOG("update::add score %d %f speed %d destroy_cube %d",tmp_score,score_value,CKHelper::Instance().lvl_speed,curent_koef_cube_destroy);
        showLabelScore(m_bombs->getPosition(),score_value);
        addScore(score_value);
    }
    
    if(m_backnode)
        m_backnode->update(dt);
    
};

void CKGameSceneNew::updatePlanePanelProgress(const int &_num)
{
    if(progress_sprite[_num])
    {
        if(m_airplane->getBonusProgress(progress_sprite[_num]->number) > 0.02)
        {
            progress_sprite[_num]->slot->getChildByTag(BONUS_PROGRESS_ID)->setProgress(m_airplane->getBonusProgress(progress_sprite[_num]->number));
        }
        else
        {
            if(progress_sprite[_num]->count == 0)
            {
                progress_sprite[_num]->bomb->setVisible(false);
                // CCFiniteTimeAction *fade = CCFadeOut::create(0.1);
                //  CCFiniteTimeAction *func = CCCallFuncN::create((progress_sprite[0]->bomb), callfuncN_selector(CKGameSceneNew::hideNode));
                // CCAction *seq = CCSequence::create(fade,func,NULL);
                // progress_sprite[0]->bomb->runAction(seq);
                
                progress_sprite[_num]->label->setVisible(false);
                progress_sprite[_num]->slot->getChildByTag(BONUS_PROGRESS_ID)->setProgress(0.0);
            }
            else
                progress_sprite[_num]->slot->getChildByTag(BONUS_PROGRESS_ID)->setProgress(1.0);
            
            progress_sprite[_num]->slot->getChildByTag(1)->setStateImage(0);
            progress_sprite[_num]->slot->getChildByTag(8)->setVisible(false);
            progress_sprite[_num] = NULL;
            // CKAudioMng::Instance().playEffect("end_active_bonus");
        };
    };
};

void CKGameSceneNew::updateBonusProgress(const float &_dt)
{
    updatePlanePanelProgress(0);
    updatePlanePanelProgress(1);
//    if(progress_sprite[0] != NULL && progress_sprite[1] != NULL)
//    {
//        CCLOG("CKGameSceneNew::updateBonusProgress %f %f",m_airplane->getBonusProgress(progress_sprite[0]->number),m_airplane->getBonusProgress(progress_sprite[1]->number));
//        
//    }
};

void CKGameSceneNew::updateParticle(const float &_dt)
{
   /* if(particle_list.empty())
        return;
    particle_it = particle_list.begin();
    while (particle_it != particle_list.end()) {
        (*particle_it)->update(_dt);
        particle_it++;
    }*/
    
};

void CKGameSceneNew::animationLabelMult()
{
    label_score->getNode()->setScale(0.0);
    CCFiniteTimeAction * action = CCSpawn::create(CCScaleTo::create(0.5, 1.0),CCFadeIn::create(0.5),NULL);
    label_score->getNode()->runAction(action);
    
    char tmp[NAME_MAX];
    sprintf(tmp, "%d",last_score*CKFileInfo::Instance().getMultiBuyKoef());
    if(label_score)
        label_score->setText(tmp);
    
};

void CKGameSceneNew::updateScore(float _dt)
{
    if(last_score == score)
    {
        unschedule(schedule_selector(CKGameSceneNew::updateScore));
        score_active = false;
        CKAudioMng::Instance().stopEffect("add_score");
        if(game_state == GAME_WIN)
        {
            if(CKFileInfo::Instance().getMultiBuyKoef() > 0)
            {
                char str[255];
                sprintf(str, "x%d",CKFileInfo::Instance().getMultiBuyKoef());
                CCLabelBMFont * labl = static_cast<CCLabelBMFont *>(m_gui_complite->getChildByTag(53)->getNode());
                labl->setVisible(true);
                labl->setString(str);
                labl->setOpacity(0);
                CCFiniteTimeAction * action = CCSequence::create(CCFadeIn::create(0.5),CCSpawn::create(CCScaleTo::create(0.5, 1.3),CCFadeOut::create(0.5),NULL),NULL);
                
                CCFiniteTimeAction * action2 = CCSequence::create(CCDelayTime::create(0.5),CCSpawn::create(CCScaleTo::create(0.5, 1.3),CCFadeOut::create(0.5),NULL),CCCallFunc::create(this, callfunc_selector(CKGameSceneNew::animationLabelMult)),NULL);
                
                label_score->getNode()->runAction(action2);
                labl->runAction(action);
            };
        }
    }
    else
    {
        if(score - last_score > 5)
        {
            last_score += int((score - last_score)*0.2);
        }
        else
            ++last_score;
        
        if(label_score->isVisible())
        {
            char tmp[NAME_MAX];
            sprintf(tmp, "%d",last_score);
            if(label_score)
                label_score->setText(tmp);
            //m_gui_game->getChildByTag(5)->setText(tmp);
        }

    }
};
void CKGameSceneNew::touchAirplane(const CCPoint& _pos)
{
    //limit airplane posiotion
    if(!m_airplane->endAnimation())
        if((m_airplane->getPosition().x > - m_airplane->getSize().width/4) && (m_airplane->getPosition().x < (win_size.width - m_airplane->getSize().width/4)))
        {
            if(_pos.y < limitet_top_zone_touch)
            {
                if(m_airplane->touch(_pos,curent_bomb->type))
                {
                    bonus_drag_up = false;
                    char str[NAME_MAX];
                    curent_bomb->count--;
                    if(curent_bomb == &bonus_bomb[0])
                    {
                        count_create_default_bomb++;
                    };
                    //CKFileInfo::Instance().removeInventorySelectBomb(curent_bomb->type);
                    sprintf(str,"%d",curent_bomb->count);
                    curent_bomb->label->setString(str);
                    if(curent_bomb->count <= 0)
                    {
                        CKAudioMng::Instance().playEffect("change_to_default_bomb");
                        m_airplane->setEnableMachineGun(false);
                        clearSlotBomb(*curent_bomb);
                        curent_bomb = &bonus_bomb[0];
                        curent_bomb->slot->getChildByTag(1)->setStateImage(1);
                        curent_bomb->bomb->setScale(BASIC_SCALE_ICON);
                        curent_bomb->slot->setTouch(true);
                        curent_bomb->slot->getChildByTag(25)->setVisible(true);
                        curent_bomb->slot->getChildByTag(25)->getNode()->resumeSchedulerAndActions();
                    };
                    
                }
            }
        };
};
bool CKGameSceneNew::touchShareEnded(cocos2d::CCSet* touches)
{
    if(m_share && m_share->isTouchEnabled()&& m_share->get()->isVisible())
    {
        CCSetIterator it;
        CCTouch* touch;
        for( it = touches->begin(); it != touches->end(); it++)
        {
            touch = (CCTouch*)(*it);
            CCPoint location = touch->getLocationInView();
            location = CCDirector::sharedDirector()->convertToGL(location);
            m_share->setTouchEnd(location);
            return true;
        };
    };
    return false;
};

void CKGameSceneNew::ccTouchesEnded(CCSet* touches, CCEvent* event)
{
    if(touchShareEnded(touches))
        return;
//    if(touchShareBegan(touches))
//        return;
    //Add a new body/atlas sprite at the touched location
    CCSetIterator it;
    CCTouch* touch;
    for( it = touches->begin(); it != touches->end(); it++)
    {
        count_active_touch--;
    };
    
    if(count_active_touch < 0)
        count_active_touch = 0;
    
   // if(count_active_touch == 0)
   // {
       // speed_airplane_start = false;
       // been_faster_fly = false;
  //  };
    CCLOG("ccTouchesEnded %d",count_active_touch);
    if(count_active_touch < 2)
    {
        if(!m_airplane->isWaitCreateBomb())
            m_airplane->setDoubleSpeed(false);
    };
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);

        if(!touch)
            break;
        
        if(count_active_touch > 1)
            break;
        
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        
        if(m_gui_game)
            m_gui_game->setTouchEnd(location);
        
        if(m_gui_pause)
            m_gui_pause->setTouchEnd(location);
        
        if(m_gui_complite)
            m_gui_complite->setTouchEnd(location);
        
        if(m_gui_failed)
            m_gui_failed->setTouchEnd(location);
    
        if(game_state == GAME_PAUSE)
            return;
        
    }
};



bool CKGameSceneNew::touchShareBegan(cocos2d::CCSet* touches)
{
    if(m_share && m_share->isTouchEnabled() && m_share->get()->isVisible())
    {
        CCSetIterator it;
        CCTouch* touch;
        for( it = touches->begin(); it != touches->end(); it++)
        {
            touch = (CCTouch*)(*it);
            CCPoint location = touch->getLocationInView();
            location = CCDirector::sharedDirector()->convertToGL(location);
            
            m_share->setTouchBegan(location);

            return true;
        }
    }
    return false;
};

void CKGameSceneNew::ccTouchesBegan(CCSet* touches, CCEvent* event)
{
    
    if(touchShareBegan(touches))
        return;
    //Add a new body/atlas sprite at the touched location
    CCSetIterator it;
    CCTouch* touch;
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        count_active_touch++;
    };
    
    if(count_active_touch > 1 && game_state == GAME_RUN)
    {
        m_airplane->setDoubleSpeed(true);
    };
    CCLOG("ccTouchesBegan:: %d %d",count_active_touch,touches->count());
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        if(touches->count() > 1)
            break;
        
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        touch_position = location;

        
        if(m_gui_game && m_gui_game->setTouchBegan(location))
            return;
        
        if(m_gui_pause && m_gui_pause->setTouchBegan(location))
            return;
        
        if(m_gui_complite && m_gui_complite->setTouchBegan(location))
            return;
        
        if(m_gui_failed)
        {
            if(m_gui_failed->setTouchBegan(location))
            {
                return;
            }
        }

        if(game_state == GAME_PAUSE)
            return;
        
        if(!m_gui_game || !m_gui_game->getTouchObject(location))
        {
            bool new_bomb = m_bombs->isNewBomb();
            
            m_bombs->touchTap();
            
            if(new_bomb)
                touchAirplane(location);
            else 
                CKAudioMng::Instance().playEffect("cant_create_bomb");
                
            m_bombs->touchAutoFind(location);
            if(m_bomb_ghost->isActiveBomb())
                m_bomb_ghost->touchAutoFind(location);
            
        }
    };
    
    return;
};

void CKGameSceneNew::didAccelerate(CCAcceleration* pAccelerationValue)
{
    if(game_state != GAME_RUN)
        return;
    if(m_backnode)
        m_backnode->didAccelerate(pAccelerationValue);

    if(m_bombs && m_bombs->isActiveBomb())
        m_bombs->didAccelerate(pAccelerationValue);
    
    if(m_bomb_ghost)
        if(m_bomb_ghost->isActiveBomb())
            m_bomb_ghost->didAccelerate(pAccelerationValue);
};

#pragma mark GLOBAL OPERATINO
void CKGameSceneNew::clearSlotPlane(PanelObject &_slot)
{
    _slot.count = 0;
    _slot.type = 0;
    _slot.label->setVisible(false);
   // _slot.slot->getChildByTag(8)->setVisible(false);
    _slot.slot->getChildByTag(7)->setVisible(false);
    _slot.slot->getChildByTag(2)->setVisible(false);
};

void CKGameSceneNew::runBonusPlaneSlot(const int &_num,const int &_ind)
{
    CCLog("slotCall:: runBonus %d :: %d",_ind,bonus_airplane[_ind].type);

    progress_sprite[_num] = &bonus_airplane[_ind];
    progress_sprite[_num]->number = static_cast<CKAirplane*>(m_airplane)->runBonus(bonus_airplane[_ind].type);//run Bonus Animation
    progress_sprite[_num]->slot->getChildByTag(8)->setVisible(true);
    
    bonus_airplane[_ind].slot->getChildByTag(1)->setStateImage(1);
    bonus_airplane[_ind].count--;
    if(bonus_airplane[_ind].count <= 0)
    {
        clearSlotPlane(bonus_airplane[_ind]);
    };
    char str[NAME_MAX];
    sprintf(str,"%d",bonus_airplane[_ind].count);
    bonus_airplane[_ind].label->setString(str);
};

void CKGameSceneNew::slotCall(CCNode* _sender,void *_value)
{
    int num = static_cast<int>(*(int*)_value);
    CCLOG("slotCall %d",num);
    if(num < 105)//bomb slot
    {
        CKAudioMng::Instance().playEffect("change_bombs_slots");
        
        for(int i = 100;i < 105;++i)
        {
            bonus_bomb[i - 100].slot->getChildByTag(1)->setStateImage(0);
            bonus_bomb[i - 100].slot->getChildByTag(3)->setStateImage(0);
            bonus_bomb[i - 100].slot->getChildByTag(8)->setStateImage(0);
            bonus_bomb[i - 100].slot->getChildByTag(25)->setVisible(false);
            bonus_bomb[i - 100].slot->getChildByTag(25)->getNode()->pauseSchedulerAndActions();
        };
    
        bonus_bomb[num - 100].slot->getChildByTag(8)->setStateImage(1);
        bonus_bomb[num - 100].slot->getChildByTag(3)->setStateImage(1);
        bonus_bomb[num - 100].slot->getChildByTag(1)->setStateImage(1);
        bonus_bomb[num - 100].slot->getChildByTag(25)->setVisible(true);
        bonus_bomb[num - 100].slot->getChildByTag(25)->getNode()->resumeSchedulerAndActions();
        curent_bomb = &bonus_bomb[num - 100];

        m_airplane->setEnableMachineGun((curent_bomb->type/10 == 16));
    }
    else
    {
        int number = num - 105;
        if(progress_sprite[0] == NULL)
        {
            if(bonus_airplane[number].count > 0 && progress_sprite[0] != &bonus_airplane[number])
            {
                runBonusPlaneSlot(0,number);
            };
        }
        else if(progress_sprite[1] == NULL)
        {
            if(bonus_airplane[number].count > 0 && progress_sprite[1] != &bonus_airplane[number] && progress_sprite[0] != &bonus_airplane[number])
            {
                runBonusPlaneSlot(1,number);
            };
        }
    };
};

void CKGameSceneNew::callbackAirplane(CCNode *_sender,void *data)
{
    switch (m_airplane->getTypeEndFly()) {
        case rAirplane_Win: //You win
        {
            game_state = GAME_WIN;
            CK::StatisticLog::Instance().setNewPage(CK::Page_LevelComplete);
            end_lvl = true;
            //send static to GA
            ObjCCalls::sendCustomGA(3,"num_lvl_completed",(CKHelper::Instance().lvl_number+1)*(CKHelper::Instance().getWorldNumber() + 1));
            int sum = 0;
            for(int i = 1; i < COUNT_PANEL_BOMB_CELL;++i)
                sum += bonus_bomb[i].count;
            ObjCCalls::sendCustomGA(4,"unused_bombs_lvl_completed",sum);
            stopAllActions();
            gameWin();
            break;
        }
        case rAirplane_Over: //Game Over
        {
            CK::StatisticLog::Instance().setNewPage(CK::Page_LevelFail);
            end_lvl = true;
            //send static to GA
            ObjCCalls::sendCustomGA(1,"num_lvl_failed",(CKHelper::Instance().lvl_number+1)*(CKHelper::Instance().getWorldNumber() + 1));
            int sum = 0;
            for(int i = 1; i < COUNT_PANEL_BOMB_CELL;++i)
                sum += bonus_bomb[i].count;
            ObjCCalls::sendCustomGA(2,"unused_bombs_lvl_failed",sum);
            game_state = GAME_OVER;
            gameOver();
            break;
        }
        default:
            break;
    };
};
void CKGameSceneNew::enableEveryplay(CCNode *_sender)
{
    CKAudioMng::Instance().playEffect("button_pressed");
//Issue #172
    if(m_share && m_share->get()->isVisible())
    {
        CK::StatisticLog::Instance().setNewPage(CK::Page_ShareWindow);
    }
    else
    {
        switch (CKHelper::Instance().getWorldNumber()) {
            case 1:
                CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneCaramel);
                break;
            case 2:
                CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneChoco);
                break;
            case 3:
                CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneIce);
                break;
            case 4:
                CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneWaffles);
                break;
            default:
                break;
        };
    }
//------------------
    if(!_sender)
        return;
    CKObject* _node = NULL;
    if(m_share && m_share->get()->isVisible())
    {
        _node = m_share->getChildByTag(CK::Action_EveryPlay_Play);
    }
    else if(m_gui_failed)
    {
        _node = m_gui_failed->getChildByTag(CK::Action_EveryPlay_Play);
    }
    if(_node)
    {
        CKFileOptions::Instance().setEveryPlayShow(true);
        _node->getChildByTag(3)->setVisible(false);
    }
};

void CKGameSceneNew::shareCall(CCNode* _sender,void *_value)
{
    switch(*static_cast<int*>(_value))
    {
        case Action_EveryPlay_Play:
            CKAudioMng::Instance().playEffect("button_pressed");
            if(!CKFileOptions::Instance().isEveryPlayShow())
            {
                CK::StatisticLog::Instance().setNewPage(CK::Page_EveryplayOn);
                CKHelper::Instance().getDialog()->setTarget(this);
                CKHelper::Instance().getDialog()->setConfirmText(LCZ("Enable Everyplay ?"),LCZ("May cause lower performance on weak device"));
                CKHelper::Instance().getDialog()->show(callfuncN_selector(CKGameSceneNew::enableEveryplay));
            }
            else
            {
                if(have_everyplay_record)
                {
                    ObjCCalls::everyplayPlayLastVideo(score, curent_level_id + 1,curent_world_id);
                }
                else
                {
                    if(ObjCCalls::isLittleMemory())
                    {
                        ObjCCalls::goToURL(EVERYPLAY_URL);
                    }
                    else
                    {
                        ObjCCalls::everyplayShow();
                    }
                }
            }
            break;
        case Action_FB_Challenge:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            CK::StatisticLog::Instance().setLink(CK::Link_ChallengeScore);
            CKFacebookHelper::Instance().FB_Challenge(score, curent_level_id + 1,curent_world_id);
        }
            break;
        case Action_FB_Brag:
        {
            
            CKAudioMng::Instance().playEffect("button_pressed");
            if(is_posted_in_facebook && ObjCCalls::getDeviceVersion() < 6.0)
                break;
            CK::StatisticLog::Instance().setLink(CK::Link_PostScore);
            is_posted_in_facebook = true;
            std::string fullpath = CCFileUtils::sharedFileUtils()->getWriteablePath() + "attach.jpg";
            char str[255];
            sprintf(str, "%s\n%s",LCZ("Checkout my score at CandyKaze game"),POST_APP_URL);
            CKFacebookHelper::Instance().FB_PostImage(str, fullpath.c_str());
            //ObjCCalls::FB_SendBrag(score,curent_level_id + 1,curent_world_id);
        }
            break;
        case Action_Tweet_Post:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            char str[NAME_MAX];
            CK::StatisticLog::Instance().setLink(CK::Link_TweetScore);
            sprintf(str, LCZ("%s  game. My score in lvl  %d of %s is %d\n"),GAME_NAME,curent_level_id + 1,WORLD_NAMES[curent_world_id],score);
            
            std::string fullpath = CCFileUtils::sharedFileUtils()->getWriteablePath() + "attach.jpg";
            CKTwitterHelper::Instance().TweetPost(str,fullpath.c_str());
        }
            break;
        case Action_Mail:
        {
            CK::StatisticLog::Instance().setLink(CK::Link_EmailScore);
            CKAudioMng::Instance().playEffect("button_pressed");
            std::string fullpath = CCFileUtils::sharedFileUtils()->getWriteablePath() + "attach.jpg";
            ObjCCalls::sendMail(score,curent_level_id + 1,curent_world_id,fullpath.c_str());
        }
            break;
        case Action_IMessage:
            CK::StatisticLog::Instance().setLink(CK::Link_ShareImassage);
            CKAudioMng::Instance().playEffect("button_pressed");
            ObjCCalls::sendMessage(curent_level_id + 1,curent_world_id,score);
            break;
        case Action_Close:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            //m_share->setTouchEnabled(false);
            m_share->get()->setVisible(false);
            switch (CKHelper::Instance().getWorldNumber()) {
                case 1:
                    CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneCaramel);
                    break;
                case 2:
                    CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneChoco);
                    break;
                case 3:
                    CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneIce);
                    break;
                case 4:
                    CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneWaffles);
                    break;
                default:
                    break;
            };
        };
            break;
    };
    if(*static_cast<int*>(_value) != Action_Close)
    {
        CK::StatisticLog::Instance().setNewPage(CK::Page_ShareWindow);
    };
};

void CKGameSceneNew::setLikes()
{
    CCLOG("CKGameSceneNew::setLikes");
    if (!CKFileInfo::Instance().isLikesOur())
    {
        char str[NAME_MAX];
        sprintf(str, LCZ("You earn %d coins."),1000);
        CKHelper::Instance().getDialog()->setTarget(this);
        CKHelper::Instance().getDialog()->setInfoText(8,LCZ("Thanks!"),str);
        CKHelper::Instance().getDialog()->showMessage(8,callfuncND_selector(CKGameSceneNew::messageCall));
        
        
        CKFileInfo::Instance().setLikesOur(true);
        CKFileInfo::Instance().save();
    };
};

void CKGameSceneNew::messageCall(CCNode* _sender,void *_value)
{
    CCLOG("messageCall %d",static_cast<int>(*(int*)_value));
    switch(static_cast<int>(*(int*)_value))
    {
        case CK::Action_Play:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
        }
            break;

        case CK::Action_Ok:
            CKAudioMng::Instance().playEffect("button_pressed");
            message_sprite = NULL;
            if(m_gui_complite)
                m_gui_complite->setTouchEnabled(true);
            if(m_gui_failed)
                m_gui_failed->setTouchEnabled(true);
            break;
        case CK::Action_Shop:
#ifndef DISABLE_SHOP
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            int d = CK::Action_Shop;
            menuCall(NULL, &d);
        }
#endif
            break;
        case CK::Action_FBLike:
            CKAudioMng::Instance().playEffect("button_pressed");
            CKHelper::Instance().FB_need_check_likes_main = false;
            if (!CKFileInfo::Instance().isLikesOur()) {
                CKHelper::Instance().FB_need_check_likes_main = true;
            };
            CKFacebookHelper::Instance().FB_goToPageLikeAnyPage();
            break;
        case CK::Action_Inventory:
#ifndef DISABLE_INVENTORY
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            int d = CK::Action_Inventory;
            menuCall(NULL, &d);
        }
#endif
            break;

            default:
            CCAssert(0, "Uknow tag");
    }
};

void CKGameSceneNew::menuCall(CCNode* _sender,void *_value)
{
    CCLOG("menuCall %d",static_cast<int>(*(int*)_value));

    switch(static_cast<int>(*(int*)_value))
    {
        case CK::Action_Play:
        {
            if(script_scene != NULL)
            {
                script_scene->setVisible(false);
                script_scene = NULL;
#if ENABLE_EVERYPLAY    
                if(CKFileOptions::Instance().isEveryPlayShow())
                    ObjCCalls::everyplayStartCapture();
#endif
                scheduleUpdate();
                game_state = GAME_RUN;
                CKFileInfo::Instance().save();
                
                CKAudioMng::Instance().stopAllEffect();
                
                CKAudioMng::Instance().playEffect("button_pressed");
                CKAudioMng::Instance().playEffect("plane_fly");
                
                bg_mask->setVisible(false);
                m_gui_game->setTouchEnabled(true);
            };
        }
            break;
        case Action_EveryPlay_Play:
            CKAudioMng::Instance().playEffect("button_pressed");
            if(!CKFileOptions::Instance().isEveryPlayShow())
            {
                CKHelper::Instance().getDialog()->setTarget(this);
                CKHelper::Instance().getDialog()->setConfirmText(LCZ("Enable Everyplay ?"),LCZ("May cause lower performance on weak device"));
                CKHelper::Instance().getDialog()->show(callfuncN_selector(CKGameSceneNew::enableEveryplay));
            }
            else
            {
                
                ObjCCalls::everyplayPlayLastVideo(0, curent_level_id + 1,curent_world_id);
            }
            break;
        case CK::Action_Menu:
        {
            //CKAudioMng::Instance().playEffect("button_pressed");
            ObjCCalls::hideBanner(true);
            //GA Фіксуємо кількість переходів на наступний левел
            if(game_state == GAME_PAUSE)
            {
                ObjCCalls::sendEventGA("mnu_pause","menu_after_pause","mnu_pause",1);//GA Фіксуємо кількість виходів в меню після натиснення паузи
            }
            else if(game_state == GAME_WIN)
            {
                ObjCCalls::sendEventGA("lvl_complete","menu_after_lvl","lvl_complete",1);//GA Фіксуємо кількість повернення в меню після успішного завершення левел
            }
            else if(game_state == GAME_OVER)
            {
                ObjCCalls::sendEventGA("lvl_crash","menu_after_crash","lvl_crash",1);//GA Фіксуємо кількість виходів в меню після крешу левела
            };
            
            CKAudioMng::Instance().removeAllEffect();
            
            this->setTouchEnabled(false);
            this->setAccelerometerEnabled(false);
            CCDirector::sharedDirector()->getAccelerometer()->setDelegate(NULL);
            
            CCDirector::sharedDirector()->getTouchDispatcher()->removeAllDelegates();
            //release();
            CKAudioMng::Instance().fadeBgToVolume(0.0,0.1);
            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(SCENE_WORLD);
            
        };
            break;
        case CK::Action_Ok:
            
            break;
        case CK::Action_Shop:
            CKAudioMng::Instance().playEffect("button_pressed");
#ifndef DISABLE_SHOP
        {

            ObjCCalls::hideBanner(true);
            CKHelper::Instance().show_layer = CK::Action_Shop;
            
            CKAudioMng::Instance().removeAllEffect();
            this->setTouchEnabled(false);
            this->setAccelerometerEnabled(false);
            CCDirector::sharedDirector()->getAccelerometer()->setDelegate(NULL);
            
            CKAudioMng::Instance().fadeBgToVolume(0.0,0.1);
            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(CK::SCENE_SHOP);
        }
#endif
            break;

        case CK::Action_Share:
            
            if(!m_share->isTouchEnabled())
            {
            CKHelper::Instance().createSocialAttach("attach.jpg", curent_level_id + 1, WORLD_NAMES[curent_world_id], score, m_bombs->getCountCreateBomb(), int((m_cube_array->getCountCube()*4 - m_cube_array->getCountActiveCube())/4), int( MAX(0,m_airplane->getAirplaneHeight() -  m_airplane->getBonusFinish())), CKFileInfo::Instance().getScore()->balance_score);
            }
            CK::StatisticLog::Instance().setNewPage(CK::Page_ShareWindow);
            CKAudioMng::Instance().playEffect("show_modal");
            m_share->get()->setVisible(true);
            m_share->setTouchEnabled(true);
            break;
        case CK::Action_Inventory:
            CKAudioMng::Instance().playEffect("button_pressed");
#ifndef DISABLE_INVENTORY
        {
            ObjCCalls::hideBanner(true);
            CKHelper::Instance().show_layer = CK::Action_Inventory;

            CKAudioMng::Instance().removeAllEffect();
            CKAudioMng::Instance().fadeBgToVolume(0.0,0.1);
            
            setTouchEnabled(false);
            setAccelerometerEnabled(false);
            CCDirector::sharedDirector()->getAccelerometer()->setDelegate(NULL);
            

            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(CK::SCENE_INVENTORY);
            
        }
#endif
            break;
        case CK::Action_Restart:
        {
            CKAudioMng::Instance().stopAllEffect();
            CKAudioMng::Instance().playEffect("button_pressed");
            CKAudioMng::Instance().fadeBgToVolume(0.0,0.1);
            this->setTouchEnabled(false);
            this->runAction(CCSequence::create(CCDelayTime::create(0.2),CCCallFunc::create(this, callfunc_selector(CKGameSceneNew::restart)),NULL));
        }
            break;
        case CK::Action_Next:
        {
            CKAudioMng::Instance().stopAllEffect();
            CKAudioMng::Instance().playEffect("button_pressed");
            ObjCCalls::sendEventGA("lvl_crash","next_lvl","lvl_crash",1);//GA Фіксуємо кількість переходів на наступний левел
            ObjCCalls::hideBanner(true);
            
            CKAudioMng::Instance().fadeBgToVolume(0.0,0.1);
            
            clearScene();
            
            if(curent_world_id ==  CKHelper::Instance().getWorldNumber())
            {
                CCScene *pScene = CKGameSceneNew::scene();
                
                CCDirector::sharedDirector()->replaceScene(pScene);
                CCDirector::sharedDirector()->resume();
            }
            else //load next world
            {
                CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
                CKHelper::Instance().playScene(CK::SCENE_GAME);
            }

        }
            break;
        case CK::Action_Pause:
        {
            
            CCLOG("Action_Pause");
            if(game_state == GAME_PAUSE)
                game_state = GAME_RUN;
            else
                game_state = GAME_PAUSE;
            
            showOrHidePauseMenu(game_state == GAME_PAUSE);
            CKAudioMng::Instance().playEffect("show_modal");
        }
            break;

        case Action_Effect:
        {
            
            CKFileOptions::Instance().setPlayEffect(!CKFileOptions::Instance().isPlayEffect());
            CKAudioMng::Instance().playEffect("button_pressed");
            
            CKObject* _node = m_gui_pause->getChildByTag(Action_Effect);
            if(_node)
            {
                if(_node->getType() == CK::GuiRadioBtn)
                {
                    _node->setStateChild(int(!CKFileOptions::Instance().isPlayEffect()));
                };
            };
        };
            break;
        case Action_Music:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            m_audio.changeMusic();

            CKObject* _node = m_gui_pause->getChildByTag(Action_Music);
            if(_node)
            {
                if(_node->getType() == CK::GuiRadioBtn)
                {
                    _node->setStateChild(!CKFileOptions::Instance().isPlayMusic());
                };
            }
        };
            break;
        case Action_Feedback:
        {
//            CKAudioMng::Instance().playEffect("button_pressed");
//            pause_menu_sprite->setVisible(false);
//            bg_mask->setVisible(false);
//            this->visit();
//            this->draw();
//            CCDirector::sharedDirector()->drawScene();
////            ObjCCalls::sendFeedback(CKStaticFile::Instance().getFileName().c_str());
//            bg_mask->setVisible(true);
//            pause_menu_sprite->setVisible(true);
        };
            break;
        case Action_Hint:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            if(DISABLE_HINTS)
                break;

            CKHelper::Instance().getTutorial()->activeted(this,SCENE_WORLD);
            CCLOG("m_bonus->getFirstBonus() %d",m_bonus->getFirstBonus());
            if(m_bonus->getFirstBonus() < MAX_BOMB_ID)
            {
                if(m_bonus->getFirstBonus() == 0 || CKHelper::Instance().getWorldNumber() > 3)
                {
                    CKHelper::Instance().getTutorial()->start(1,-1); //default tap
                }
                else
                    CKHelper::Instance().getTutorial()->start(3,m_bonus->getFirstBonus());
            }
            else //show airplane bonus
            {
                CKHelper::Instance().getTutorial()->start(2,m_bonus->getFirstBonus());
            }
        };
            break;
        default:
            CCAssert(0, "Uknow tag");
    }
};


void CKGameSceneNew::enabledGui(CCNode *_sender,void *_gui)
{
    if(_gui)
    {
        static_cast<CKGUI*>(_gui)->setTouchEnabled(false);
    }
};

void CKGameSceneNew::menuCallback(CCNode* _sender,void *_data)
{
    int _tag = *static_cast<int*>(_data);
    CCLOG("menuCallback %d",_tag);
    
    if(_sender->getTag() == 99)
    {
        
        ObjCCalls::goToURL(CKAdBanner::Instance().getURL());
        return;
    };
    
    if(_tag == CK::Action_Ok)
    {
        CKAudioMng::Instance().playEffect("button_pressed");
        CKHelper::Instance().call_unlock_world = 0;
        if(message_sprite != NULL)
        {
            message_sprite = NULL;
            if(m_gui_complite)
                m_gui_complite->setTouchEnabled(true);
            if(m_gui_failed)
                m_gui_failed->setTouchEnabled(true);
        }   
        
        return;
    };
    
//    switch(_tag)
//    {
//        case CK::Action_Play:
//        {
//            CKAudioMng::Instance().playEffect("button_pressed");
//            if(message_sprite != NULL)
//            {
//                removeChild(message_sprite, true);
//                message_sprite = NULL;
//                scheduleUpdate();
//                game_state = GAME_RUN;
//                CKFileInfo::Instance().save();
//                bg_mask->setVisible(false);
//            }
//        }
//            break;
//        default:
//            CCAssert(0, "Uknow tag");
//    }
};

void CKGameSceneNew::saveUserProgress()
{
    CKFileInfo::Instance().setCurentOpenLevel(CKHelper::Instance().lvl_number);
    CKFileInfo::Instance().clearInvetorySelect();
    //gen inventory select list
    for(int i = 1; i < COUNT_PANEL_BOMB_CELL ; i++)
    {
            CKFileInfo::Instance().addToInventorySelect(bonus_bomb[i].type,bonus_bomb[i].count);
    };
    
    if(CKFileOptions::Instance().isKidsModeEnabled())
    {
        CKFileInfo::Instance().save();
        return;
    }
    
    _it = tmp_bonus_list.begin();
    while(_it != tmp_bonus_list.end())
    {
        CKFileInfo::Instance().addToInventory((*_it).first,(*_it).second);
        _it++;
    };
    //
    int count_org_bomb = (DEFAULT_BOMB_COUNT -  bonus_bomb[0].count);
    int count_create_spec_bomb = abs(m_bombs->getCountCreateBomb() - count_org_bomb);
    
    int count_loop_plane = MAX(0,m_airplane->getAirplaneHeight() -  m_airplane->getBonusFinish());
    int count_destroy_cube = (m_cube_array->getCountCube()*4 - m_cube_array->getCountActiveCube())/4;
    
    CKFileInfo::Instance().getScore()->addAccuvateShots(count_accuvate_shots);
    CKFileInfo::Instance().getScore()->addCountOrginaryBomb(count_org_bomb);
    CKFileInfo::Instance().getScore()->addCountSpecialBomb(count_create_spec_bomb);
    CKFileInfo::Instance().getScore()->addCountDestroyCube(count_destroy_cube);
    CKFileInfo::Instance().getScore()->addCountLoopPlane(count_loop_plane);
    CKFileInfo::Instance().getScore()->addCountHitBomb(m_bombs->getCountHitBomb());
    CKFileInfo::Instance().save();
    
    int acc = (CKFileInfo::Instance().getScore()->getCountHitBomb() > 0)?float(CKFileInfo::Instance().getScore()->getAccuvateShots())*100000.0f/float(CKFileInfo::Instance().getScore()->getCountHitBomb()):0;
    
    CCLOG("count_create_spec_bomb getAbsoluteScore %d %d %d ",count_accuvate_shots,m_bombs->getCountHitBomb(),bonus_bomb[0].count);
    
    CCAssert(acc > 0 || acc < 10000, "incorect accuracy");
    
    if(!CKFileInfo::Instance().isBlackList())
    {
        CCLOG("leaderboardSendAllScore %d %d %d",CKFileInfo::Instance().getScore()->getCountHitBomb(),CKFileInfo::Instance().getScore()->getAccuvateShots(),acc);
        ObjCCalls::leaderboardSendAllScore(CKFileInfo::Instance().getScore()->score_total, CKFileInfo::Instance().getScoreAllWorld(), CKHelper::Instance().getWorldNumber(), CKFileInfo::Instance().getScoreCurrentWorld(), CKFileInfo::Instance().getScore()->getCountOrginaryBomb(), CKFileInfo::Instance().getScore()->getCountSpecialBomb(), acc, CKFileInfo::Instance().getScore()->getCountDestroyCube(), CKFileInfo::Instance().getScore()->getCountLoopPlane());
        
        if(game_state == GAME_WIN)
        {
            CKParseHelper::Instance().parseSendOpenWorldLvl(CKFileInfo::Instance().getLvlOpenWithWorld(CKHelper::Instance().getWorld(0)),CKFileInfo::Instance().getLvlOpenWithWorld(CKHelper::Instance().getWorld(1)),CKFileInfo::Instance().getLvlOpenWithWorld(CKHelper::Instance().getWorld(2)),CKFileInfo::Instance().getLvlOpenWithWorld(CKHelper::Instance().getWorld(3)),CKFileInfo::Instance().getLvlOpenWithWorld(CKHelper::Instance().getWorld(4)));
        }
    };
    
};

void CKGameSceneNew::showDialog()//the end game show some message
{
    unschedule(schedule_selector(CKGameSceneNew::showDialog));
    message_sprite = NULL;
    
    int sum = 0;
    bool free_slot = false;
    for(int i = 1; i < COUNT_PANEL_BOMB_CELL;++i)
    {
        sum += bonus_bomb[i].count;
        if(bonus_bomb[i].count == 0)
            free_slot = true;
    }
    CCLOG("CKGameSceneNew::showDialog sum %d inventory %d empty",sum,CKFileInfo::Instance().isEmptyInv());
    if(sum == 0 /*|| (game_state == GAME_OVER && rand()%3 == 0)*/)//33 percent add bomb to slot
    {
        if(CKFileInfo::Instance().isEmptyInv())
        {
            showTutorial(t_NoBombsAnyWhere);// buy many bomb //You can buy some more in SHOP
        }
        else
        {
            showTutorial(t_NoBombsInSlots);//add bomb in your inventory
        }
    };
    if(free_slot)
    {
        showTutorial(t_OutOfBombs);
    };
    
    if(CKFileInfo::Instance().isEmptyInv())
    {
        CCLOG("showDialog::inventory empty");
    };
    // CCLOG("CKHelper::Instance().world_number %d %d %d",CKHelper::Instance().getWorldNumber(),CKHelper::Instance().lvl_number,is_been_clear_lvl);
    if(CKHelper::Instance().lvl_number == COUNT_LEVEL_IN_WORLD)//it's last level the world
    {
        showTutorial(t_WorldComplite);//world complite
        CCLOG("CKHelper::Instance().world_number %d",CKHelper::Instance().getWorldNumber());
    };

    
    if((CKHelper::Instance().getWorldNumber() + 1) < COUNT_WORLD)
        if( !CKFileInfo::Instance().isOpenWorld(CKHelper::Instance().getWorld(CKHelper::Instance().getWorldNumber()+ 1).c_str()) && CKFileInfo::Instance().getTotalScore() > world_cost[CKHelper::Instance().getWorldNumber() + 1]) //we have enough money to buy some world
        {
            CCLOG("Total score %d world cost %d world #%d",CKFileInfo::Instance().getTotalScore(),world_cost[CKHelper::Instance().getWorldNumber() + 1],CKHelper::Instance().getWorldNumber() + 1);
            CKHelper::Instance().call_unlock_world = CKHelper::Instance().getWorldNumber() + 1;
            showTutorial(t_UnlockWorld);//you have by next world
        };
    
    if(rand()%5 == 0)   //20 percent for that upgrade bombs if need
    {
        bool p = false;
        for(int i = 0; i < COUNT_PANEL_BOMB_CELL;++i) // find bombs  with need upgrade
            if(CKFileInfo::Instance().isNeedUpgradeBomb(bonus_bomb[i].type))
                p = true;
        if(p)
        {
            showTutorial(t_UpgradeBombs);//need upgrade bomb in you inventory
        };
    };
    

   // showTutorial(t_UpgradeBombs);//test
    
    if(game_state == GAME_OVER && rand()%3 == 1)
        showTutorial(t_OutOfBombs);//You can buy some more in SHOP
};

void CKGameSceneNew::gameWin()
{
    CCLOG("CKGameSceneNew::gameWin");
   // CCDirector::sharedDirector()->getAccelerometer()->setDelegate(NULL);
    
    int score_value = int(MAX(m_airplane->getBonusFinish() - 2,0)*score_destroy_one_cube*m_cube_array->getColumnBaseHeight()*2);
    CCLOG("gameWin ::score finish airplane height %d",score_value);
   for(int i = 1; i < COUNT_PANEL_PLANE_CELL ; i++)
    {
        if(bonus_bomb[i].count == 0)
            continue;
        score_value += int(bonus_airplane[i].count*score_destroy_one_cube + bonus_airplane[i].count*score_destroy_one_cube*CCRANDOM_MINUS1_1()*ADD_SCORE_PERSENT);
    };
    
   

    CCLOG("CKFileInfo::Instance().getLevel()->score %d",CKFileInfo::Instance().getLevel()->score);
   // is_been_clear_lvl = (CKFileInfo::Instance().getLevel()->score == 0);
    is_hiscore = false;
    if(int(CKFileInfo::Instance().getLevel()->score) > 0)
    {
        if( CKFileInfo::Instance().getLevel()->score < score)
        {
            is_hiscore = true;
        };
    };
    
    addScore(score_value);
    if(!CKFileOptions::Instance().isKidsModeEnabled())
        CKFileInfo::Instance().addScore(score);
    
    incLevelWorld();
    
    CCLOG("CKHelper::Instance().lvl_number %d score %d",CKHelper::Instance().lvl_number,score);


    game_state = GAME_PAUSE;
    
    saveUserProgress();

#if ENABLE_EVERYPLAY    
    ObjCCalls::everyplayStopRecording();
#endif
    
    showEndMenu(true);
};

void CKGameSceneNew::gameOver()
{
   // CCDirector::sharedDirector()->getAccelerometer()->setDelegate(NULL);
#if ENABLE_EVERYPLAY    
    ObjCCalls::everyplayStopRecording();
#endif
    saveUserProgress();
    showEndMenu(false);
};

#pragma mark  OPERATION

void CKGameSceneNew::animationWinScore()
{
    
    if(last_score < score)
    {
        if(!score_active)
            schedule(schedule_selector(CKGameSceneNew::updateScore),0.03);
        score_active = true;
        
        if(!CKAudioMng::Instance().isPlayEffect("add_score"))
            CKAudioMng::Instance().playEffect("add_score");
    };
}
void CKGameSceneNew::addScore(int _score)
{
    score += _score;
    if(last_score < score)
    {
        if(!score_active)
            schedule(schedule_selector(CKGameSceneNew::updateScore),0.03);
        score_active = true;
        
        if(!CKAudioMng::Instance().isPlayEffect("add_score"))
            CKAudioMng::Instance().playEffect("add_score");
    };
};

bool CKGameSceneNew::showTutorialScript()
{
    if(!m_bonus)
        return false;
    int id = m_bonus->isNewBonus();
    if(id != 0)
    {
        if((id > MAX_BOMB_ID && CKFileInfo::Instance().isNeedShowTutorial(id))|| (id <  MAX_BOMB_ID && CKFileInfo::Instance().isNeedShowTutorial(int(id/10))))
        {
            script_scene = CKTutorialScriptOneScene::create();
            script_scene->init();
            
            this->addChild(script_scene,99);
            script_scene->setPosition(ccp(panel_left,-banner_height/2));
            
            int type = (id > MAX_BOMB_ID)?(id - MAX_BOMB_ID):id;
            if (CKHelper::Instance().getIsIpad()) {
                script_scene->initFrame(type,CCSize(714,616),ccp(542,388));
            }
            else
            {
                script_scene->initFrame(type,CCSize(297,258),ccp(256,164));
            };
            m_gui_game->setTouchEnabled(false);
            game_state = GAME_PAUSE;
            return true;
        }
  
    };
    return false;
};

bool CKGameSceneNew::showTutorial(int _tag)
{
    if(CKFileOptions::Instance().isKidsModeEnabled())
        return false;
    
    if(script_scene != NULL || message_sprite != NULL)
        return false;
    
    CCLOG("showTutorial %d",_tag);
    
    CKHelper::Instance().getDialog()->setTarget(this);
    CKHelper::Instance().getDialog()->showMessage(_tag,callfuncND_selector(CKGameSceneNew::messageCall));
    message_sprite =  CKHelper::Instance().getDialog()->getInfoGui()->get();
    message_sprite->setPosition(ccp(win_size.width*0.5,-win_size.height*1.0));
    message_sprite->setPosition(ccp(panel_left ,0 -banner_height/2));
    CCFiniteTimeAction *move = CCMoveTo::create(TIME_SHOW_FRAME_ANIMATION, ccp(0 + panel_left,0));
    CCFiniteTimeAction *delay = CCDelayTime::create(TIME_TO_START_SHOW_TUTORIAL);
    CCFiniteTimeAction *delay2 = CCDelayTime::create(TIME_TO_START_SHOW_TUTORIAL*0.8);
    CCFiniteTimeAction *func = CCCallFuncND::create(message_sprite, callfuncND_selector(CKGameSceneNew::enabledGui),(game_state == GAME_WIN)?m_gui_complite:m_gui_failed);
    CCFiniteTimeAction *seq =  CCSequence::create(delay,move,NULL);
    CCFiniteTimeAction *seq2 =  CCSequence::create(delay2,func,NULL);
    CCAction *spaw = CCSpawn::create(seq,seq2,NULL);
    
    spaw->setTag(2);
    message_sprite->runAction(spaw);
    message_sprite->setTag(_tag);
    CKAudioMng::Instance().playEffect("show_modal");
    return true;
};

void CKGameSceneNew::showEndMenu(bool _win)
{
    CKAudioMng::Instance().playEffect("show_modal");
    //game_state = GAME_PAUSE;

    char total_str[NAME_MAX];
    sprintf(total_str, "%d",CKFileInfo::Instance().getTotalScore());
    
    schedule(schedule_selector(CKGameSceneNew::showDialog), TIME_SHOW_FRAME_ANIMATION*2, 0, TIME_SHOW_FRAME_ANIMATION*2);
    
    
    if(!_win)
    {

        m_gui_failed->setTouchEnabled(true);
        m_gui_failed->get()->setVisible(true);
        m_gui_failed->getChildByTag(LABEL_TOTAL_SCORE_ID)->setText(total_str);
        m_gui_failed->get()->setPosition(ccp(panel_left,-win_size.height));
        CCFiniteTimeAction* m_action1 =  CCSequence::create(CCEaseElasticOut::create(CCMoveTo::create(1.5,ccp(panel_left,-banner_height/2))),NULL); //show menu
        m_gui_failed->get()->runAction(m_action1);
    }
    else
    {
        
         m_gui_complite->get()->setVisible(true);
        if(!CKFileOptions::Instance().isKidsModeEnabled())
        {
            
            char score_str[NAME_MAX];
            last_score = score;
            if(_win)
                last_score -= int(MAX(m_airplane->getBonusFinish() - 2,0)*score_destroy_one_cube*m_cube_array->getColumnBaseHeight()*2);
            
            if(is_hiscore)
                CKAudioMng::Instance().playEffect("label_new_score");
            
            sprintf(score_str, "%d",last_score);
            m_gui_complite->getChildByTag(LABEL_TOTAL_SCORE_ID)->setText(total_str);
            m_gui_complite->getChildByTag(52)->setText(score_str);
            label_score = m_gui_complite->getChildByTag(52);
            m_gui_complite->getChildByTag(53)->setVisible(false);
            
            if(!is_hiscore)
            {
                m_gui_complite->getChildByTag(61)->getNode()->removeFromParentAndCleanup(true);
                m_gui_complite->getChildByTag(62)->getNode()->removeFromParentAndCleanup(true);
                cub_mask->removeFromParentAndCleanup(true);
                if(m_gui_complite->getChildByTag(60)) // backShigning.png
                    m_gui_complite->getChildByTag(60)->getNode()->removeFromParentAndCleanup(true);// new score show cup?
            }
            //m_gui_complite->getChildByTag(61)->setVisible(is_hiscore); //cup new score label
           // m_gui_complite->getChildByTag(62)->setVisible(is_hiscore); // new score show cup?
            
//            if(m_gui_complite->getChildByTag(63)) // cup mask
//                m_gui_complite->getChildByTag(63)->setVisible(is_hiscore); // new score show cup?
//            

            
            CCLOG("CKHelper::Instance().lvl_number %d",CKHelper::Instance().lvl_number);
            
            m_gui_complite->get()->setPosition(ccp(panel_left,-win_size.height-banner_height/2));
            CCFiniteTimeAction* m_action1 =  CCEaseElasticOut::create(CCMoveTo::create(1.5,ccp(panel_left,-banner_height/2))); //show menu
            
            CCAction *sequer = CCSequence::create(CCDelayTime::create(0.1),m_action1,CCDelayTime::create(0.1),CCCallFunc::create(this, callfunc_selector(CKGameSceneNew::startAddScoreFn)), NULL);

            m_gui_complite->get()->runAction(sequer);
        
            //17 Також на 16 левелі кнопка next має бути невидима.
            if(CKHelper::Instance().lvl_number == COUNT_LEVEL_IN_WORLD)
            {
                CKHelper::Instance().lvl_number = 0;
                m_gui_complite->getChildByTag(CK::Action_Next)->setVisible(false);
            }
        }else
        {
            m_gui_complite->get()->setPosition(ccp(panel_left,-win_size.height-banner_height/2));
            CCFiniteTimeAction* m_action1 =  CCEaseElasticOut::create(CCMoveTo::create(1.5,ccp(panel_left,-banner_height/2))); //show menu
            m_gui_complite->get()->runAction(m_action1);
        }
    }

    bg_mask->setVisible(true);
    bg_mask->setOpacity(0);
    CCAction* m_action1 = CCMoveTo::create(0.5,ccp(0,win_size.height-banner_height/2));//hide panel
    m_gui_game->get()->runAction(m_action1);
    
    //save static file
    timeval t;
    gettimeofday(&t,NULL);
    time_start = t.tv_sec - pause_time - time_start;
    
    bg_mask->runAction(CCFadeIn::create(0.5));
    
    int sum = 0;
    for(int i = 1; i < COUNT_PANEL_BOMB_CELL ; i++)
    {
        if(bonus_bomb[i].count <= 0 || bonus_bomb[i].type <= 0)
            sum +=0;
        else
            sum += bonus_bomb[i].count;
    };
    
    CCAssert( score < 20000, "unreal score");
    
    int count_default_bomb = (DEFAULT_BOMB_COUNT -  bonus_bomb[0].count);
    CCAssert(count_create_default_bomb == count_default_bomb, "incorect deffaul bomb");
    int count_create_bomb = m_bombs->getCountCreateBomb();
    int count_create_spec_bomb = count_create_bomb - count_default_bomb;
    CCAssert(m_bombs->getCountCreateSpecBomb() == count_create_spec_bomb, "incorect count create special bombs");
    count_create_spec_bomb = m_bombs->getCountCreateSpecBomb();
    int count_loop_plane = MAX(0,m_airplane->getAirplaneHeight() -  m_airplane->getBonusFinish());
    int count_destroy_cube = (m_cube_array->getCountCube()*4 - m_cube_array->getCountActiveCube());
    int count_hit_bombs = int(count_create_bomb - m_bombs->getCountFailBomb());
    int state = 0;
    CCAssert(count_create_spec_bomb >= 0 , "incorect count bobms");
    if(game_state != GAME_OVER)
        state = 1;
    if(CKFileOptions::Instance().isKidsModeEnabled())
    {
        state = 2;
        ObjCCalls::sendEventFlurry("kids_level_complete", false, "world_num=%d,lvl_num=%d,difficulty_num=%d,lvl_state=%d,blocks_big_all=%d,bombs_available=%d,level_time=%d",curent_world_id,curent_level_id,CKHelper::Instance().lvl_speed + 1,int(_win),m_cube_array->getCountCube(),count_create_spec_bomb,int(time_start));
    };
     StatisticLog::Instance().setLP(curent_world_id, curent_level_id + 1,m_airplane->getStartSpeed()*10, state, m_cube_array->getCountCube(), count_special_bomb, time_start, m_airplane->getStartHeight(), count_loop_plane, count_hit_bombs, m_bombs->getCountFailBomb(), count_destroy_cube,count_default_bomb, count_create_spec_bomb, score);
};

void CKGameSceneNew::startAddScoreFn()
{
    animationWinScore();
};
void CKGameSceneNew::createBanner()
{
    CCTexture2D *texture = CCTextureCache::sharedTextureCache()->addImage(CKAdBanner::Instance().show());
    banner = CCSprite::createWithTexture(texture);
    limitet_top_zone_touch += banner->getContentSize().height;
    
    if(banner)
    {
        CCMenuItemSprite *item =  CCMenuItemSprite::create(banner, banner, this, menu_selector(CKGameSceneNew::menuCallback));
        item->setScale(CCDirector::sharedDirector()->getContentScaleFactor());
        item->setTag(99);
        menu_banner = CCMenu::create(item,NULL);
        
        menu_banner->setPosition(ccp(win_size.width/2,win_size.height - item->getContentSize().height/2*item->getScaleY()));
        addChild(menu_banner,99);
        banner_height = item->getContentSize().height*item->getScale();
    };
};

void CKGameSceneNew::showBanner(bool _animation)
{

    baner_showed = true;
    if(_animation)
    {
        CCAction* m_action1 = CCMoveTo::create(TIME_TO_SHOW_BANNER,ccp(m_gui_game->get()->getPositionX(),(is_ipad)?-90:-32));//move panel down
        m_gui_game->get()->runAction(m_action1);
    }
    else
    {
        m_gui_game->get()->setPosition(ccp(m_gui_game->get()->getPositionX(),(is_ipad)?-90:-32));
    }
};
void CKGameSceneNew::setPause()
{
    ObjCCalls::everyplayPauseRecording();
    if(game_state != GAME_RUN)
        return;
    
    CK::StatisticLog::Instance().setNewPage(CK::Page_Pause);
    //#if ENABLE_EVERYPLAY

    //#endif
    
    count_active_touch = 0;
    std::vector<CCNode*>::iterator  s_it = action_node.begin();
    while (s_it != action_node.end()) {
        (*s_it)->pauseSchedulerAndActions();
        s_it++;
    }
    if(m_emiter_snow)
        m_emiter_snow->pauseSchedulerAndActions();
    
    game_state = GAME_PAUSE;
    
    timeval t;
    gettimeofday(&t,NULL);
    pause_time = t.tv_sec - pause_time;
    
    if(m_airplane)
        m_airplane->getSprite()->pauseSchedulerAndActions();
    
    if(m_cube_array)
        m_cube_array->pause();
    
    if(m_gui_game)
        m_gui_game->get()->setVisible(game_state == GAME_RUN);
    
    if(pause_menu_sprite)
    {
        pause_menu_sprite->setVisible(true);
        pause_menu_sprite->setTag(1);
        pause_menu_sprite->setPosition(ccp(win_size.width*0.0 + panel_left,win_size.height*0.0));
    }
    if(bg_mask)
    {
        bg_mask->setVisible(true);
        bg_mask->setOpacity(255);
    };
    m_audio.pauseAudio(false);
};

void CKGameSceneNew::endShowPause()
{
    this->resumeSchedulerAndActions();
#if ENABLE_EVERYPLAY
    ObjCCalls::everyplayResumeRecording();
#endif
    scheduleUpdate();
    
    std::vector<CCNode*>::iterator  s_it = action_node.begin();
    while (s_it != action_node.end()) {
        (*s_it)->resumeSchedulerAndActions();
        s_it++;
    }
    
    m_cube_array->resume();
    m_airplane->resume();
    game_state = GAME_RUN;
    timeval t;
    gettimeofday(&t,NULL);
    pause_time = t.tv_sec - pause_time;
    CCLOG("pause_time = %d",pause_time);
    
    if(m_emiter_snow)
        m_emiter_snow->resumeSchedulerAndActions();
    
}

void CKGameSceneNew::showOrHidePauseMenu(bool _show)
{
    
#if ENABLE_EVERYPLAY
    ObjCCalls::everyplayPauseRecording();
#endif
    
    m_gui_game->getChildByTag(CK::Action_Pause)->getNode()->setVisible(game_state == GAME_RUN);
    m_gui_game->get()->setVisible(game_state == GAME_RUN);
    if(!_show)
    {
        switch (CKHelper::Instance().getWorldNumber()) {
            case 1:
                CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneCaramel);
                break;
            case 2:
                CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneChoco);
                break;
            case 3:
                CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneIce);
                break;
            case 4:
                CK::StatisticLog::Instance().setNewPage(CK::Page_GameSceneWaffles);
                break;
            default:
                break;
        };
        
        unscheduleUpdate();
        
        m_audio.resumeAudio();
        
        if(!CKAudioMng::Instance().isPlayEffect("plane_fly"))
            CKAudioMng::Instance().playEffect("plane_fly");
        
        schedule(schedule_selector(CKGameSceneNew::endShowPause), TIME_SHOW_FRAME_ANIMATION, 0, TIME_SHOW_FRAME_ANIMATION);
        pause_menu_sprite->setTag(0);
        
        CCAction* action = CCSequence::create(CCMoveTo::create(TIME_SHOW_FRAME_ANIMATION, ccp(win_size.width*1.0,win_size.height*0.0 - banner_height/2)),
                                              CCCallFuncN::create(pause_menu_sprite, callfuncN_selector(CKGameSceneNew::hideNode)),NULL);
        pause_menu_sprite->runAction(action);

        
        if(bg_mask)
        {
            CCAction* fin = CCSequence::create(CCFadeOut::create(TIME_SHOW_FRAME_ANIMATION),CCCallFuncN::create(bg_mask, callfuncN_selector(CKGameSceneNew::hideNode)),NULL);;
            bg_mask->runAction(fin);
        };
    }
    else
    {
        CK::StatisticLog::Instance().setNewPage(CK::Page_Pause);
        
            count_active_touch = 0;
//        animation_back_sprite
        std::vector<CCNode*>::iterator  s_it = action_node.begin();
        while (s_it != action_node.end()) {
            (*s_it)->pauseSchedulerAndActions();
            s_it++;
        }

        //is_pause = _show;
        timeval t;
        gettimeofday(&t,NULL);
        pause_time = t.tv_sec - pause_time;
        
        if(m_emiter_snow)
            m_emiter_snow->pauseSchedulerAndActions();

        if(m_airplane)
        {
            m_airplane->pause();
            m_cube_array->pause();
            
            bg_mask->setVisible(true);
            if(bg_mask)
            {
                CCAction* fin = CCFadeIn::create(TIME_SHOW_FRAME_ANIMATION);
                bg_mask->runAction(fin);
            };
        }
        pause_menu_sprite->setVisible(true);
        pause_menu_sprite->setTag(1);
        pause_menu_sprite->setPosition(ccp(-win_size.width*1.0,win_size.height*0.0 - banner_height/2));
        CCAction* action = CCMoveTo::create(TIME_SHOW_FRAME_ANIMATION, ccp(win_size.width*0.0 + panel_left,win_size.height*0.0 - banner_height/2));
        pause_menu_sprite->runAction(action);
        m_audio.pauseAudio(true);
    };

};

bool CKGameSceneNew::addBonusToPlanePanel(CCNode *_node)
{
    CKBonusInfo *m_info = static_cast<CKBonusInfo *>(_node->getUserData());
    for (int i = 0; i < COUNT_PANEL_PLANE_CELL; ++i)
    {
        if(bonus_airplane[i].type  == m_info->type)
        {
            bonus_airplane[i].count++;
            char str[NAME_MAX];
            sprintf(str,"%d", bonus_airplane[i].count);
            bonus_airplane[i].label->setString(str);
            bonus_airplane[i].label->setVisible(true);
            bonus_airplane[i].slot->getChildByTag(7)->setVisible(true);
            return true;
        };
    };
    
    for (int i = 0; i < COUNT_PANEL_PLANE_CELL; ++i)
    {
        if(bonus_airplane[i].type == 0)  //add bonus into free cell
        {
            bonus_airplane[i].type = m_info->type;
            CCSpriteFrame *frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(CKBonus::getPlaneName(bonus_airplane[i].type).c_str()) ;
            bonus_airplane[i].bomb->setTextureRect(frame->getRect(),frame->isRotated(),bonus_airplane[i].bomb->getContentSize());
            
            bonus_airplane[i].bomb->setScale(0.0);
            bonus_airplane[i].bomb->setVisible(true);
            bonus_airplane[i].bomb->setOpacity(255);
            bonus_airplane[i].count = 0;
            
            CCAction *spaw = CCSequence::create(CCDelayTime::create(TIME_TO_SHOW_BONUS_PALNE),CCScaleTo::create(TIME_TO_SHOW_BONUS_PALNE, BASIC_SCALE_ICON),NULL);
            bonus_airplane[i].bomb->runAction(spaw);
            bonus_airplane[i].slot->setTouch(true);
            bonus_airplane[i].slot->getChildByTag(BONUS_PROGRESS_ID)->setProgress(1.0);
            bonus_airplane[i].slot->getChildByTag(8)->setVisible(false);
            
            bonus_airplane[i].slot->getChildByTag(7)->setVisible(true);
            bonus_airplane[i].slot->getChildByTag(2)->setVisible(true);

            bonus_airplane[i].count = m_info->count;
            
            char str[NAME_MAX];
            sprintf(str,"%d", bonus_airplane[i].count);
            bonus_airplane[i].label->setString(str);
            bonus_airplane[i].label->setVisible(true);
            return true;
        };
    };
return false;
};

bool CKGameSceneNew::addBonusToBombPanel(CCNode *_node)
{
    CKBonusInfo *m_info = static_cast<CKBonusInfo *>(_node->getUserData());
    //find bonus in panel
    for (int i = 0; i < COUNT_PANEL_BOMB_CELL; ++i)
    {
        if(bonus_bomb[i].type == _node->getTag())
        {

            count_special_bomb += m_info->count;
            showSlotBomb(bonus_bomb[i], bonus_bomb[i].count + m_info->count, bonus_bomb[i].type);
            return true;
        };
    };
    //insert new bomb bonus
    for (int i = 0; i < COUNT_PANEL_BOMB_CELL; ++i)
    {
        if(bonus_bomb[i].type == 0)  //add bonus into free cell
        {
            
            count_special_bomb += m_info->count;
            showSlotBomb(bonus_bomb[i], m_info->count,_node->getTag());
            
            bonus_bomb[i].bomb->setScale(0.0);
            CCAction *spaw = CCSequence::create(CCDelayTime::create(TIME_TO_BONUS_ANIM),CCScaleTo::create(TIME_TO_BONUS_ANIM, BASIC_SCALE_ICON),NULL);
            bonus_bomb[i].bomb->runAction(spaw);

            return true;
        };
    }
    return false;
};

void CKGameSceneNew::runBonusAnimation(CCNode *_node)
{
    if(!_node)
        return;
    int score_value = 0;
    CKBonusInfo *m_info = static_cast<CKBonusInfo *>(_node->getUserData());
    if(!m_info->is_bomb)
    {
        score_value = score_destroy_one_cube + score_destroy_one_cube*CCRANDOM_MINUS1_1()*ADD_SCORE_PERSENT;
    }
    else
    {
        score_value = CKBonusMgr::Instance().getInfoById(_node->getTag())->pick_points;
    };
    
    
    CKFileInfo::Instance().addOpenBonus((m_info->is_bomb)?m_info->type:(m_info->type + 1000));
    //move bonus to panel
    _node->runAction(CCScaleTo::create(TIME_TO_BONUS_ANIM,0.0));
    //add  bonus into the panel
    if(m_info->is_bomb)
    {
        if(!addBonusToBombPanel(_node))
        {
        
            count_special_bomb += m_info->count;
            bool insert_ = false;
            _it = tmp_bonus_list.begin();
            while(_it!=tmp_bonus_list.end())
            {
                if((*_it).first == _node->getTag())
                {
                    (*_it).second += m_info->count;
                    
                    insert_ = true;
                    break;
                }
                _it++;
            }
            if(!insert_)
                tmp_bonus_list.push_back(m_pair(_node->getTag(),m_info->count));
        };
    }
    else//bonus add to airplane bonus panel
    {
        addBonusToPlanePanel(_node);
    }
};
