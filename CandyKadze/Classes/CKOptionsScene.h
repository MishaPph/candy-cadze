//
//  CKOptionsScene.h
//  CandyKadze
//
//  Created by PR1.Stigol on 26.09.12.
//
//

#ifndef __CandyKadze__CKOptionsScene__
#define __CandyKadze__CKOptionsScene__

#include <iostream>
#include <cocos2d.h>
#include "CKFileOptions.h"
#include "CCControlSlider.h"
using namespace cocos2d;

class CKOptionsScene : public cocos2d::CCLayer {
public:
    ~CKOptionsScene();
    CKOptionsScene();
    
    // returns a Scene that contains the HelloWorld as the only child
    static cocos2d::CCScene* scene();
    CCMenuItemLabel* item_banner,*item_upgrade,*item_static,*item_cloud,*item_everyplay;
    CCLabelTTF *slide,*label_language;
  //  cocos2d::extension::CCControlSlider *_slider;
    void updateSlide(const float &_dt);
    int num_language;
    //    virtual bool init();
private:
    void menuCallback(CCNode* _sender);
    void menuCallbackSpeed(CCNode* _sender);
};

#endif /* defined(__CandyKadze__CKOptionsScene__) */
