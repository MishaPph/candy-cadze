//
//  CKLogoScene.h
//  CandyKadze
//
//  Created by PR1.Stigol on 02.05.13.
//
//

#ifndef __CandyKadze__CKLogoScene__
#define __CandyKadze__CKLogoScene__

#include <cocos2d.h>


using namespace cocos2d;

class CKLogoScene : public cocos2d::CCLayerColor {
public:
    ~CKLogoScene();
    CKLogoScene();
    
    // returns a Scene that contains the HelloWorld as the only child
    static cocos2d::CCScene* scene();
    int time;
    void nextScene(CCNode *_sender);
    void update(const float &_dt);
    void functAsunc(CKLogoScene * _sender);
    void playAnimation();
    void logoStigol();
private:
    int curen_load;
    CCSprite * stigol_logo ;
    cocos2d::CCSprite * m_sprite;
};

#endif /* defined(__CandyKadze__CKLogoScene__) */
