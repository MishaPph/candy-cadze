//
//  CKLoadingScene.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 10.04.13.
//
//

#include "CKLoadingScene.h"
#include "CKLoaderGUI.h"
#include "CKWordScene.h"
#include "CKMainMenuScene.h"
#include "CKGameScene.h"
#include "CKLanguage.h"
#include "CKGameScene90.h"
#include "CKOptionLayer.h"

//#include <pthread.h>
//#include <semaphore.h>
//
//pthread_t m_thread;
//pthread_mutex_t region_mutex = PTHREAD_MUTEX_INITIALIZER;
//static sem_t* s_Sem = NULL;

#pragma mark - LOADING_SCENE
int CKLoadingScene::number_scene = 0;
CKLoadingScene::~CKLoadingScene()
{
    CCLOG("~CKLoading");
    //m_gui->removeAtlasPlist();
    CC_SAFE_DELETE(m_gui);
    removeAllChildrenWithCleanup(true);
    //  CC_SAFE_RELEASE_NULL(this);
};

CKLoadingScene::CKLoadingScene()
{
    m_gui = NULL;
    enable_complite = true;
    initGui();
    is_scene_loading = false;
    previous_scene_id = -1;
    CCTextureCache::sharedTextureCache()->addImageAsync(CK::loadImage("a_Confirmation.png").c_str(), this,NULL);

};

CCScene* CKLoadingScene::scene()
{
    CCScene *scene = CCScene::create();
    CCLayer* layer = new CKLoadingScene();
    
    if(layer->init())
    {
        scene->addChild(layer);
        layer->setTouchEnabled(false);
        layer->setTag(CK::LAYER_LOADING);
        layer->release();
    };
    return scene;
};

void CKLoadingScene::unloadPreviousScene()
{
    CCLOG("\n__________________[ unloadPreviousScene %d]_____________________\n",previous_scene_id);

    //CCLabelBMFont::purgeCachedData();
    //CCTextureCache::sharedTextureCache()->removeUnusedTextures();
    //CCFileUtils::sharedFileUtils()->purgeCachedEntries();
    
   // CCDirector::sharedDirector()->purgeCachedData();
    if(previous_scene_id == CK::SCENE_WORLD)
    {
        CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile(CK::loadImage("a_LevelSelect.plist").c_str());
        
        CCTexture2D * texture =  CCTextureCache::sharedTextureCache()->textureForKey(CK::loadImage("a_LevelSelect.png").c_str());
        CCLOG("CKLoadingScene:: a_LevelSelect.png %d",(texture)?texture->retainCount():0);
        
        CCTextureCache::sharedTextureCache()->removeTextureForKey(CK::loadImage("a_LevelSelect.png").c_str());
    
    } else if(previous_scene_id == CK::SCENE_MAIN)
    {
        
         CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile(CK::loadImage("a_MainMenuAnimation.plist").c_str());

        CCTexture2D *texture = CCTextureCache::sharedTextureCache()->textureForKey(CK::loadImage("a_MainMenuAnimation.png").c_str());

        CCTextureCache::sharedTextureCache()->removeTextureForKey(CK::loadImage("StigolLogo.png").c_str());
        if(texture && texture->retainCount() < 2)
        {
            CCTextureCache::sharedTextureCache()->removeTextureForKey(CK::loadImage("a_MainMenuAnimation.png").c_str());
        };
        
        CCTextureCache::sharedTextureCache()->removeTextureForKey(CK::loadImage("a_MainMenuAnimation.png").c_str());
        CCTextureCache::sharedTextureCache()->removeTextureForKey(CK::loadImage("StigolLogo.png").c_str());
        
        CCTextureCache::sharedTextureCache()->removeTextureForKey(CK::loadImage("pilot_scarf.png").c_str());

    } else if(previous_scene_id == CK::SCENE_GAME)
    {
        try {
            CCDirector::sharedDirector()->getTouchDispatcher()->removeAllDelegates();
        } catch (int) {
            
        }
        
        CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile(CK::loadImage("a_GameScreen.plist").c_str());
      //  CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile(CK::loadImage("a_Boom.plist").c_str());
        CCTextureCache::sharedTextureCache()->removeTextureForKey(CK::loadImage("a_GameScene.png").c_str());
       // CCTextureCache::sharedTextureCache()->removeTextureForKey(CK::loadImage("a_Boom.png").c_str());
        
        CCDictionary *dict = CCDictionary::createWithContentsOfFile(CKHelper::Instance().getWorld().c_str());
        CCDictionary * dict_meta = (CCDictionary *)dict->objectForKey("meta");
        CCLOG("CKLoadingScene::unloadPreviousScene world %s",CKHelper::Instance().getWorld().c_str());
        if(dict_meta)
        {

//            CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile(CK::loadImage(dictStr(dict_meta, "atlas")).c_str());
            CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile(CK::loadImage(dictStr(dict_meta, "atlas")).c_str());         
            CCTexture2D * texture = CCTextureCache::sharedTextureCache()->textureForKey(CK::loadImage(dictStr(dict_meta, "image")).c_str());
            CCLOG("CKLoadingScene::unloadPreviousScene world %s %d",CKHelper::Instance().getWorld().c_str(),texture->retainCount());
            
            CCTextureCache::sharedTextureCache()->removeTextureForKey(CK::loadImage(dictStr(dict_meta, "image")).c_str());
            
        };
    } else if((previous_scene_id == CK::SCENE_SHOP  || previous_scene_id == CK::SCENE_INVENTORY )&& (number_scene != CK::SCENE_INVENTORY && number_scene != CK::SCENE_SHOP))
    {
        CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile(CK::loadImage("a_InventoryShop.plist").c_str());
        CCTexture2D * texture =  CCTextureCache::sharedTextureCache()->textureForKey(CK::loadImage("a_InventoryShop.png").c_str());
        CCLOG("CKLoadingScene:: a_InventoryShop.png %d",(texture)?texture->retainCount():0);
        
        CCTextureCache::sharedTextureCache()->removeTextureForKey(CK::loadImage("a_InventoryShop.png").c_str());
        CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile(CK::loadImage("a_Bomb.plist").c_str());
        
        texture =  CCTextureCache::sharedTextureCache()->textureForKey(CK::loadImage("a_Bomb.png").c_str());
        CCLOG("CKLoadingScene:: a_Bomb.png %d",(texture)?texture->retainCount():0);
        
        CCTextureCache::sharedTextureCache()->removeTextureForKey(CK::loadImage("a_Bomb.png").c_str());
    }else if (previous_scene_id == CK::SCENE_GAME90)
    {
        CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile(CK::loadImage("a_World90.plist").c_str());
        CCTexture2D * texture90 = CCTextureCache::sharedTextureCache()->textureForKey(CK::loadImage("a_World90.png").c_str());
        CCLOG("CKLoadingScene::unloadPreviousScene a_World90.png %d",(texture90)?texture90->retainCount():0);
        
        CCTextureCache::sharedTextureCache()->removeTextureForKey(CK::loadImage("a_World90.png").c_str());
    }
    //CCTextureCache::sharedTextureCache()->dumpCachedTextureInfo();
};
int CKLoadingScene::getCurrentSceneID() const
{
    return number_scene;
};

int CKLoadingScene::getPreviourSceneID() const
{
    return previous_scene_id;
};

void CKLoadingScene::play(int _num_scne)
{
    CCLOG("CKLoadingScene::play");
    scheduleUpdate();
    CCDirector::sharedDirector()->resume();
    
    if(m_gui)
    {
        m_gui->reloadText();
    };

    
    //CCDirector::sharedDirector()->drawScene();
    is_scene_loading = false;
    previous_scene_id = number_scene;
    number_scene = _num_scne;
   // unloadPreviousScene();
    
    float time = 0.3;
    if(number_scene == CK::SCENE_GAME)
    {
        time = 0.4;
    }
  //  if(previous_scene_id == CK::SCENE_GAME)
    {
       // enable_complite = false; //not loading next scene
    }
    //else
    {
        enable_complite = true;
    };
    
    CCAction* action = CCSequence::create(CCDelayTime::create(time),CCCallFunc::create(this, callfunc_selector(CKLoadingScene::initResource)),NULL);
    runAction(action);
    
    if(CKFileOptions::Instance().isPlayMusic())
    {
        if(number_scene != CK::SCENE_GAME && number_scene != CK::SCENE_GAME90)
        {
            CKAudioMng::Instance().fadeBgToVolume(CK::SOUND_FADE_LOADING);
        }
        else
        {
            CKAudioMng::Instance().fadeBgToVolume(0.0,0.3);
        };
    };

};

void CKLoadingScene::initResource()
{

    CCLOG("CKLoadingScene::initResource");
    unloadPreviousScene();
    curen_load = 0;
    std::string tex_name = "";
    int _num_scne = number_scene;
    if(previous_scene_id == number_scene)
    {
    //    return;
    }
    CCDirector::sharedDirector()->getTouchDispatcher()->removeAllDelegates();
    if(_num_scne == CK::SCENE_WORLD)
    {
        tex_name = CKBaseNode::getAtlasTextureName(CK::loadFile("c_levelSelect.plist").c_str()).c_str();
        if(!tex_name.empty())
        {
            curen_load++;
            CCTextureCache::sharedTextureCache()->addImageAsync(tex_name.c_str(), this,callfuncO_selector(CKLoadingScene::functAsunc));
        };
    } else if(_num_scne == CK::SCENE_MAIN)
    {
        tex_name = CKBaseNode::getAtlasTextureName(CK::loadFile("c_mainMenu.plist").c_str()).c_str();
        if(!tex_name.empty())
        {
            curen_load++;
            CCTextureCache::sharedTextureCache()->addImageAsync(tex_name.c_str(), this,callfuncO_selector(CKLoadingScene::functAsunc));
        };
        curen_load++;
        CCTextureCache::sharedTextureCache()->addImageAsync(CK::loadImage("a_Plane.png").c_str(), this,callfuncO_selector(CKLoadingScene::functAsunc));
        
    } else if(_num_scne == CK::SCENE_GAME)
    {
        if(CKFileOptions::Instance().isPlayMusic())
        {
            CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
            CocosDenshion::SimpleAudioEngine::sharedEngine()->releaseBackgroundMusic();
            CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("world 1_2_3_4.mp3");
        };
        
        tex_name = CKBaseNode::getAtlasTextureName(loadFile("c_GameScreen.plist").c_str()).c_str();
        
        if(!tex_name.empty())
        {
            curen_load++;
            CCTextureCache::sharedTextureCache()->addImageAsync(tex_name.c_str(), this,callfuncO_selector(CKLoadingScene::functAsunc));
        };
        tex_name = loadFile("a_TutorialPause.png").c_str();
        
        if(!tex_name.empty())
        {
            curen_load++;
            CCTextureCache::sharedTextureCache()->addImageAsync(tex_name.c_str(), this,callfuncO_selector(CKLoadingScene::functAsunc));
        };
        CCDictionary *dict = CCDictionary::createWithContentsOfFile(CKHelper::Instance().getWorld().c_str());
        CCDictionary * dict_meta = (CCDictionary *)dict->objectForKey("meta");
        if(dict_meta)
        {
            curen_load++;
            CCTextureCache::sharedTextureCache()->addImageAsync(CK::loadImage(dictStr(dict_meta, "image")).c_str(), this,callfuncO_selector(CKLoadingScene::functAsunc));
        };
    } else if(_num_scne == CK::SCENE_INVENTORY || _num_scne == CK::SCENE_SHOP)
    {
        if(previous_scene_id != CK::SCENE_INVENTORY && previous_scene_id != CK::SCENE_SHOP)
        {
            std::string tex_name = "";
            tex_name = CK::loadImage("a_InventoryShop.png").c_str();
            if(!tex_name.empty())
            {
                curen_load++;
                CCTextureCache::sharedTextureCache()->addImageAsync(tex_name.c_str(), this,callfuncO_selector(CKLoadingScene::functAsunc));
            };
            curen_load++;
            CCTextureCache::sharedTextureCache()->addImageAsync(CK::loadImage("a_Bomb.png").c_str(), this,callfuncO_selector(CKLoadingScene::functAsunc));
        }
        else
        {
            nextScene();
        }
    } else if(_num_scne == CK::SCENE_GAME90)
    {
        if(CKFileOptions::Instance().isPlayMusic())
        {
            CocosDenshion::SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
            CocosDenshion::SimpleAudioEngine::sharedEngine()->releaseBackgroundMusic();
            CocosDenshion::SimpleAudioEngine::sharedEngine()->preloadBackgroundMusic("world 90.mp3");
        };
        tex_name = CK::loadImage("a_World90.png");//CKBaseNode::getAtlasTextureName(loadFile("a_World90.plist").c_str()).c_str();
        if(!tex_name.empty())
        {
            curen_load++;
            CCTextureCache::sharedTextureCache()->addImageAsync(tex_name.c_str(), this,callfuncO_selector(CKLoadingScene::functAsunc));
        };
    }else if (_num_scne == CK::SCENE_OPTION)
    {
       curen_load = 1;
       functAsunc(NULL);
    };
};

void CKLoadingScene::functAsunc(CKLoadingScene * _sender)
{
    CCLOG("CKLoadingScene::functAsunc");
  //  pthread_mutex_lock(&region_mutex);
    curen_load--;
  //  pthread_mutex_unlock(&region_mutex);
    if(curen_load == 0)
    {
        CCLOG("functAsunc::playNextScene");
        nextScene();
    };
    
};
//
//void *CKLoadingScene::thread_func(void *vptr_args)
//{
//  //  int i;
//   // CKLoadingScene * my
//    CCLOG("CKLoadingScene::thread_func");
//

// //   pthread_mutex_lock(&region_mutex);
// //   static_cast<CKLoadingScene *>(vptr_args)->update(0.1);
// //   pthread_mutex_unlock(&region_mutex);
//
//    while (true)
//    {
//        // wait for rendering thread to ask for loading if s_pAsyncStructQueue is empty
//        int semWaitRet = sem_wait(s_Sem);
//        if( semWaitRet < 0 )
//        {
//            CCLOG( "CCTextureCache async thread semaphore error: %s\n", strerror( errno ) );
//            break;
//        }
//    };
//    
//    int k = 0;
//    int n = 0;
//    while (k < 100) {
//        if (n%20 == 0) {
//           // pthread_mutex_lock(&region_mutex);
//            CCLOG("CKLoadingScene::thread_func");
//          //  pthread_mutex_unlock(&region_mutex);
//            k++;
//        }
//    };
//    
//    if( s_Sem != NULL )
//    {
//
//        sem_unlink("ccAsync1");
//        sem_close(s_Sem);
//
//        s_Sem = NULL;
//    }
//    pthread_exit(NULL);
//    return 0;
//};
//
void CKLoadingScene::nextScene()
{
    CCLOG("CKLoadingScene::nextScene");
    if(!enable_complite)
        return;
    if(is_scene_loading)
        return;
    is_scene_loading = true;
  
    if(number_scene == CK::SCENE_WORLD)
    {
        CKWordScene::scene();
    }
    else if(number_scene == CK::SCENE_MAIN)
    {
        CKMainMenuScene::scene();
    }
    else if(number_scene == CK::SCENE_GAME)
    {
        CKGameSceneNew::scene();
    }
    else if(number_scene == CK::SCENE_SHOP)
    {
        CKShop::scene();
    }
    else if(number_scene == CK::SCENE_INVENTORY)
    {
        CKInventory::scene();
    }
    else if(number_scene == CK::SCENE_GAME90)
    {
        CKGameScene90::scene();
    }else if(number_scene == CK::SCENE_OPTION)
    {
        CKOptionLayer::scene();
    };
};

void CKLoadingScene::compliteLoadScene(CCScene* _scene)
{
    if(_scene)
    {
        
        if(CKFileOptions::Instance().isPlayMusic())
        {
            if(number_scene != CK::SCENE_GAME90 && number_scene != CK::SCENE_GAME)
                CKAudioMng::Instance().fadeBgToVolume(1.0);
        };
        
        CCDirector::sharedDirector()->replaceScene(_scene);
    };
};

void CKLoadingScene::initGui()
{
    m_gui = new CKGUI();
    m_gui->create(CCNode::create());
    m_gui->load(CK::loadFile("c_Loading.plist",true).c_str());
    m_gui->setTouchEnabled(false);
    m_gui->setTarget(this);
    addChild(m_gui->get(),1);
};

void CKLoadingScene::update(float dt)
{
    if(m_gui)
        m_gui->getChildByTag(1)->getNode()->setRotation(m_gui->getChildByTag(1)->getNode()->getRotation() + 1);
};



