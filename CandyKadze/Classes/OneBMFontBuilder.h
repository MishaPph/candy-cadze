//
//  OneBMFontBuilder.h
//  fgfa
//
//  Created by PR1.Stigol on 16.04.13.
//
//

#ifndef __fgf__OneBMFontBuilder__
#define __fgf__OneBMFontBuilder__

#include <iostream>
#include "cocos2d.h"
#include <list>

typedef void (*render_func)(cocos2d::CCLabelTTF *,cocos2d::CCRenderTexture *&,const int &_font_size);

namespace SL {
    
    class TextureSymbol
    {
    private:
        int x,y;
        cocos2d::CCSprite *sprite;
        unsigned short m_id;
    public:
        ~TextureSymbol();
        TextureSymbol();
        bool build(const unsigned short _sh, const char *fontName, float fontSize,render_func &_func);
        void setPosition(const float &_x,const float &_y)
        {
            sprite->setPosition(ccp(_x,_y));
            x = _x - sprite->getContentSize().width*sprite->getAnchorPoint().x;
            y = _y + sprite->getContentSize().height*sprite->getAnchorPoint().y;
        };
        bool operator<(const TextureSymbol &) const;
        const cocos2d::CCSize& getSize() const
        {
            return sprite->getContentSize();
        };
        void visit() const
        {
            sprite->visit();
        };
        const int getID()const
        {
            return m_id;
        };
        int getX() const
        {
            return x;
        };
        int getY() const
        {
            return y;
        };
    };
    
    //http://www.blackpawn.com/texts/lightmaps/
    class NodeGraph
    {
        NodeGraph* child[2];
        cocos2d::CCRect rc;
        bool is_full;
    public:
        static int count;
        ~NodeGraph();
        NodeGraph(const cocos2d::CCRect &_rc);
        virtual bool Insert(TextureSymbol *_node);
    };
    
    class OneBMFontBuilder
    {
        struct CHAR_DICT
        {
            //std::string dict;
            // std::map<unsigned short,unsigned short > charSet;
            std::set<unsigned short> char_set;
            std::list<TextureSymbol *> list;
        };
        std::map<int, CHAR_DICT *> map_font_size;
        std::map<int, CHAR_DICT *>::iterator m_it;
        
        std::list<TextureSymbol *> list_all_char;
        std::list<TextureSymbol *>::reverse_iterator r_it;
        std::list<TextureSymbol *>::iterator it;
        
        std::string font_name;
        std::string atlas_name;
        cocos2d::CCRenderTexture* atlas;
        render_func m_render_func;
        float square;
        OneBMFontBuilder();
      //  void removeDublicate(int _index);
        void saveFont(int _font_size);
        float scale;
        bool atlas_created;
        //  int StringToWString(std::wstring &ws, const std::string &s);
        cocos2d::CCSize calcAtlasSize();
    public:
        static void func2(cocos2d::CCLabelTTF *_label,cocos2d::CCRenderTexture *&_rtt,const int &_font_size);
        static void funcGradBorder(cocos2d::CCLabelTTF* label,cocos2d::CCRenderTexture *&rtt,const int &_font_size);
        ~OneBMFontBuilder();
        void setScale(float _scale)
        {
            scale = _scale;
        }
        static OneBMFontBuilder* create(const char *fontName,render_func _func);
        void addString(const char *string, int fontSize);
        void save(const char *file_name = "");
        void build();
        void createAtlas(const cocos2d::CCSize &_size, bool _auto_size = false);
        void saveAllFonts();
    };
}
#endif /* defined(__fgf__OneBMFontBuilder__) */
