
//
//  CKMainMenuScene.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 18.09.12.
//
//

#include "CKMainMenuScene.h"
#include "CK.h"
#include "CKWordScene.h"
#include "CKOptionsScene.h"
#include "CKButton.h"
#include "CKBonusMgr.h"
#include "CKHelper.h"
#include "CKStaticInfo.h"
#include "CKNTPTime.h"
#include "CKLoaderParallax.h"
#include "CKLanguage.h"
#include "CKOptionLayer.h"
#include "CKTableScore.h"
#include "CKLeaderboard.h"
#include "StatisticLog.h"
#include "DeviceInfo.h"
#include "CKTwitterHelper.h"
#include "ICloudHelper.h"
#include "CKFacebookHelper.h"

using namespace CK;
#define CLOUD_SCALE_KOEF 0.005

#define HILL_SCALE_KOEF 0.002
#define HILL_OPASITY_KOEF 0.5

#define TIME_NEXT_SKY 15.0
#define TIME_FADE_SKY 2.0
#define UPDATE_POSITION_CLOUD_KOEF 0.003
#define UPDATE_POSITION_HILL_KOEF 0.005
#define DISTANCE_MOVE_APP_X win_size.width*0.2

CCScene* CKMainMenuScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    // add layer as a child to scene
    CCLayer* layer = new CKMainMenuScene();
    CCLOG("scene:: scene %d layer %d ",scene,layer);
    if(layer->init())
    {
        scene->addChild(layer);

        layer->setTouchEnabled(true);
        layer->setTag(LAYERID_MAIN);
        layer->release();
    };
    

    
    CKHelper::Instance().compliteLoadScene(scene);
    return scene;
};

CKMainMenuScene::~CKMainMenuScene()
{
    CCLOG("~CKMainMenuScene:: destroy");
    CKAudioMng::Instance().fadeOutEffect("plane_fly");

    if(tb_sprite)
        tb_sprite->removeFromParentAndCleanup(true);
    cloud_list.clear();
    hill_list.clear();
    ice_list.clear();
    
    CC_SAFE_DELETE(m_lead);
    CC_SAFE_DELETE(m_backnode);

    CC_SAFE_DELETE(m_menu_back);
    CC_SAFE_DELETE(m_plane);
    CC_SAFE_DELETE(m_dialog);
    CC_SAFE_DELETE(m_fb_dialog);
    
    removeAllChildrenWithCleanup(true);
    
    CKFacebookHelper::Instance().setCallBackFunc(NULL, NULL);
    CC_SAFE_DELETE(m_update);
};

#pragma mark - INIT
CKMainMenuScene::CKMainMenuScene()
{
    CK::StatisticLog::Instance().setNewPage(Page_MainMenu);
    
    m_lead = NULL;
    tb_sprite = NULL;
    showed_app = false;
    left_eye = NULL;
    right_eye = NULL;

    m_plane = NULL;
    waffel_cloud = NULL;
    m_dialog = NULL;
    m_fb_dialog = NULL;
    spr_mask_play = NULL;
    m_update = NULL;
    
    this->setTouchEnabled(false);
    win_size = CCDirector::sharedDirector()->getWinSize();
    
    font_size = win_size.width/50;

    load();
    
    CCDictionary *dict = CCDictionary::createWithContentsOfFile("list.plist");
    CCArray* frameArray = (CCArray*)dict->objectForKey("list");
    
    CCObject* pObj = NULL;
    unsigned char index = 0;
    CCARRAY_FOREACH(frameArray, pObj)
    {
        CCDictionary* name = (CCDictionary*)(pObj);
        CKHelper::Instance().addWorld(index, dictStr(name,"name"));
        ++index;
    };
    dict->release();
    
    CKAudioMng::Instance().playEffect("plane_fly");
    CKAudioMng::Instance().fadeInEffect("plane_fly");
     
    if(!CKFileOptions::Instance().isKidsModeEnabled())
    {
        if (ObjCCalls::getDeviceVersion() >= 6.0) {
            CKFacebookHelper::Instance().FB_TryLogin();
            CKFacebookHelper::Instance().setCallBackFunc(this, callfuncND_selector(CKMainMenuScene::callbackFunc));
            CKFacebookHelper::Instance().FB_LoadFriendsList();
            CKTwitterHelper::Instance().TweetLoadFriendList(this, callfuncND_selector(CKMainMenuScene::callbackFunc));
        }
        
        CKAmazonHelper::Instance().needUpdateTestFlightAppVersion(this, callfuncND_selector(CKMainMenuScene::callbackFunc));
    };
    if(CKHelper::Instance().getLoadingScene()->getPreviourSceneID() == -1)
    {
        //CK::StatisticLog::Instance().sendOld();
        
        CKAmazonHelper::Instance().uploadStatisticsJson();
        
        CCLOG("CKHelper::Instance().getPreviourSceneID %d",CKHelper::Instance().getLoadingScene()->getPreviourSceneID());
        schedule(schedule_selector(CKMainMenuScene::getDeviceInfo), 1.0, 0, 1.0);
    }
};

void CKMainMenuScene::getDeviceInfo()
{
    DeviceInfo::Instance().collectAllInfo();
    DeviceInfo::Instance().deleteOldInfo();
    CK::DeviceInfo::Instance().saveJson();
    
    unschedule(schedule_selector(CKMainMenuScene::getDeviceInfo));
    CK::StatisticLog::Instance().createSession();
    
//    CK::DeviceInfo::Instance().collectAllInfo();
//    CK::DeviceInfo::Instance().deleteOldInfo();
//    CK::DeviceInfo::Instance().saveJson();
};

void CKMainMenuScene::callbackFunc(CCNode *_sender,void *_d)
{
    CCLOG("CKMainMenuScene::callbackFunc %d",*static_cast<int *>(_d));
    if(*static_cast<int *>(_d) == 1 && !_sender)//facebook call back
    {
        //ObjCCalls::parseLoadOpenLvl(CKTableFacebook::Instance().getFriends());
    }
    else
    {
        if(m_update && _sender)
        {
            CCDirector::sharedDirector()->pause();
            update_url = (char *)_sender;
            m_update->get()->setVisible(true);
            //char str[255];
            CCLOG("CKMainMenuScene::callbackFunc %s %s",update_url.c_str(),(char *)_d);
            //sprintf(str, "%s  game. I just completed Level # %d in %s. My score is %d \n Get this game here:  \n","");
            m_update->getChildByTag(7)->setText((char *)_d);
            CCDirector::sharedDirector()->resume();
        }
    }
};


void CKMainMenuScene::initGui()
{
    ObjCCalls::optimezeALLFileInDir();
    
    m_backnode = new CKGUI;
    m_backnode->create(CCNode::create());
    m_backnode->load(loadFile("c_mainMenu.plist",true).c_str());
    m_backnode->get()->setPosition(ccp(0,0));
    m_backnode->setTarget(this);
    m_backnode->addFuncToMap("menuCall", callfuncND_selector(CKMainMenuScene::menuCall));
    m_backnode->setTouchEnabled(true);
    addChild(m_backnode->get(),3);
    //hide app
    
    if(CKFileOptions::Instance().isKidsModeEnabled())
    {
        m_backnode->getChildByTag(Action_Rating)->setVisible(false);
        m_backnode->getChildByTag(Action_Shop_Coins)->setVisible(false);
        m_backnode->getChildByTag(Action_Show)->setVisible(false);
        m_backnode->getChildByTag(Action_Options)->getChildByTag(1)->setStateImage(1);
        m_backnode->getChildByTag(Action_Shop)->setVisible(false);
    };
    
    left_bar = m_backnode->getChildByTag(61);
    right_bar = m_backnode->getChildByTag(62);
    
    left_bar_position = left_bar->getPos();
    right_bar_positon = right_bar->getPos();
    left_bar->setPosition(left_bar_position.x - DISTANCE_MOVE_APP_X, left_bar_position.y);
    right_bar->setPosition(right_bar_positon.x + DISTANCE_MOVE_APP_X, right_bar_positon.y);
    
    CKObject* _node = m_backnode->getChildByTag(Action_Music);
    if(_node)
    {
        if(_node->getType() == CK::GuiRadioBtn)
        {
            _node->setStateChild(int(!CKFileOptions::Instance().isPlayMusic()));
        };
    }
    _node = m_backnode->getChildByTag(Action_Effect);
    if(_node)
    {
        if(_node->getType() == CK::GuiRadioBtn)
        {
            _node->setStateChild(int(!CKFileOptions::Instance().isPlayEffect()));
        };
    };
    
    CKObject *m_play = m_backnode->getChildByTag(1)->getChildByTag(1);
    m_play->setVisible(false);
    CCSpriteFrame *frame =  CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("iconPlay.png");
    
    spr_mask_play = TBSpriteMask::createWithTexture(frame->getTexture(), frame->getRect(),frame->isRotated());
    spr_mask_play->setPosition(m_play->getPos());
    m_backnode->getChildByTag(1)->getNode()->addChild(spr_mask_play,m_play->getzOrder());
    spr_mask_play->setAnchorPoint(m_play->getNode()->getAnchorPoint());
    
    CCTexture2D *texture = CCTextureCache::sharedTextureCache()->addImage(CK::loadImage("cupMask.png").c_str());
    spr_mask_play->buildMaskWithTexture(texture,CCRect(texture->getContentSize().width*0.2, 0, texture->getContentSize().width*0.4, texture->getContentSize().height),false,3);
    offset_mask_play = 0.03*20;
    actionPlayButton();
    
    
//    CCLabelTTF *label_a = CCLabelTTF::create("A", "Cookies", 64);
//    CCMenuItemLabel * item_a = CCMenuItemLabel::create(label_a, this, menu_selector(CKMainMenuScene::menuSelector));
//    item_a->setPosition(ccp(win_size.width*0.0,win_size.height*0.45));
//    item_a->setTag(1);
//    
//    CCMenu *menu = CCMenu::create(item_a,NULL);
//    addChild(menu,99);
    
    char version_str[NAME_MAX];
    sprintf(version_str, "%.2f",ObjCCalls::getVersion());
    CCLOG("Bundle version %f",version_str);
    CCLabelTTF *label = CCLabelTTF::create(version_str, "Arial", win_size.width*0.05);
    label->setPosition(ccp(win_size.width*0.1,win_size.height*0.9));
    addChild(label,4);
};

void CKMainMenuScene::menuSelector(CCNode *_sender)
{
    CK::StatisticLog::Instance().showLog();
    if(_sender->getTag() == 1)
    {
        //DeviceInfo::Instance().showLastSaveFile();
    }else
    {
        //DeviceInfo::Instance().showInfo();
    }
};

void CKMainMenuScene::actionPlayButton()
{
    spr_mask_play->moveMask(0.0, -offset_mask_play);
    offset_mask_play = 0.0;
    spr_mask_play->runAction(CCSequence::create(CCDelayTime::create(3.0),CCCallFunc::create(this, callfunc_selector(CKMainMenuScene::actionPlayButton)),NULL));
};

void CKMainMenuScene::initUpdateGui()
{
    m_update = new CKGUI();
    m_update->create(CCNode::create());
    m_update->load(CK::loadFile("c_ModalWindowUpdate.plist").c_str());
    m_update->setTouchEnabled(true);
    m_update->setTarget(this);
    m_update->addFuncToMap("callUpdate", callfuncND_selector(CKMainMenuScene::callUpdate));
    m_update->get()->setVisible(false);
    if(win_size.width == 568)
    {
        m_update->get()->setPosition(ccp(44,0));
    };
    addChild(m_update->get(),99);
};

void CKMainMenuScene::initKMGui()
{
    m_dialog = new CKGUI();
    m_dialog->create(CCNode::create());
    m_dialog->load(CK::loadFile("c_ModalWindowTurnOffKM.plist").c_str());
    m_dialog->setTouchEnabled(true);
    m_dialog->setTarget(this);
    m_dialog->addFuncToMap("callDialog", callfuncND_selector(CKMainMenuScene::callDialog));
    m_dialog->get()->setVisible(false);
    if(win_size.width == 568)
    {
        m_dialog->get()->setPosition(ccp(44,0));
    };
    addChild(m_dialog->get(),99);

    
};

void CKMainMenuScene::initFbGui()
{
    m_fb_dialog = new CKGUI();
    m_fb_dialog->create(CCNode::create());
    m_fb_dialog->load(CK::loadFile("c_ModalWindowFacebook.plist").c_str());
    m_fb_dialog->setTouchEnabled(true);
    m_fb_dialog->setTarget(this);
    m_fb_dialog->addFuncToMap("callFbDialog", callfuncND_selector(CKMainMenuScene::callFbDialog));
    m_fb_dialog->get()->setVisible(false);
    
    if(win_size.width == 568)
    {
        m_fb_dialog->get()->setPosition(ccp(44,0));
    };
    addChild(m_fb_dialog->get(),99);
};

void CKMainMenuScene::load()
{
    CKFileInfo::Instance().load();
    CKHelper::Instance().getTutorial(); // create tutorial layer

    current_sky = SKY_MARMELAD;

    initGui();
    
    initUpdateGui();
    
    if(CKFileOptions::Instance().isKidsModeEnabled())
        initKMGui();
    
    if(CKFileOptions::Instance().isFirstLaunch() && CKTableFacebook::Instance().friendEmpty())
    {
        initFbGui();
    };
    
    this->setTouchEnabled(true);
    this->setVisible(true);
    
    CCTextureCache::sharedTextureCache()->dumpCachedTextureInfo();
    schedule(schedule_selector(CKMainMenuScene::update), 1.0/60.0);
    runAction(CCSequence::create(CCDelayTime::create(0.1),CCCallFunc::create(this, callfunc_selector(CKMainMenuScene::showMsg)),NULL));
    koef_device = 1.0f;
    if(win_size.width != 1024)
    {
        koef_device = 3.0;
    };

    initSky();
    
    m_menu_back  = new CKParalaxNode;
    m_menu_back->create(CCNode::create());
    m_menu_back->load(CK::loadFile("c_MainMenuAnimation.plist",true).c_str());
    addChild(m_menu_back->get(),0);
    
    for (int i = 10; i < 30; ++i) {
        CKObject * tmp = m_menu_back->getChildByTag(i);
        if(tmp)
        {
            tmp->getNode()->setTag(current_sky);
            
            cloud_list.push_back(tmp->getNode());
        }
    };
    
    int kr =0,kl = 0;
    for (int i = 32; i < 48; ++i) {
        //CKObject * tmp = m_menu_back->getChildByTag(i);
      //  if(tmp)
        {
            
            TBSpriteMask * sprite = TBSpriteMask::createWithSpriteFrameName("hill_marmelad.png");
           // CCSpriteFrame *frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("plane_scarf_mask.png");
            CCTexture2D *texture = CCTextureCache::sharedTextureCache()->addImage("gradhill.png");
            sprite->buildMaskWithTexture(texture,CCRect(),0,4);
            sprite->setMaskRect(0.0, 1.0, 0.02, 0.0);
            sprite->setTag(current_sky);
            m_menu_back->get()->addChild(sprite,30);
            sprite->setRotation(rand()%360);
            if(i < 40)
            {
                //tmp->setValue(2); //right
                sprite->setUserData(this);
                kr++;
                float x = win_size.width*(0.5 + float(kr)/5.0 + (rand()%10 - 5.0f)/90.0);
                float y = win_size.height*(float(kr)/8.0 - 0.3 + (rand()%10 - 5.0f)/90.0);
                sprite->setPosition(ccp(x, y));
            }
            else
            {
                kl++;
                float x = win_size.width*(0.5 - float(kl)/5.0 + (rand()%10 - 5.0f)/90.0);
                float y = win_size.height*(float(kl)/8.0 - 0.3 + (rand()%10 - 5.0f)/90.0);
                sprite->setPosition(ccp(x, y));
              //  CCLOG("win_size.height*(float(kl)/10.0 - 0.2) %f",(float(kl)/10.0 - 0.2));
                sprite->setUserData(NULL);//left
            }
            hill_list.push_back(sprite);
        }
    };
    float down_end = 0.3;
    if(win_size.width != 1024)
    {
        down_end = 0.4;
    };
    end_positon_hill_left = ccp(win_size.width*0.5, -win_size.height*down_end);
    end_positon_hill_right = ccp(win_size.width*0.5, -win_size.height*down_end);
    updateHill(0);
    
    for (int i = 80; i < 90; i++) {
        CKObject * tmp = m_menu_back->getChildByTag(i);
        if(tmp)
        {
            CCSprite * sprite = static_cast<CCSprite *>(tmp->getNode());
            sprite->setBlendFunc((ccBlendFunc){GL_ONE,GL_ONE});
            int r = 255/3*(i-80);
            sprite->setOpacity(r);
            sprite->setVisible(false);
            sprite->runAction(CCCallFuncND::create(this, callfuncND_selector(CKMainMenuScene::shineIce),(void *)sprite));
            ice_list.push_back(sprite);
        };
    };
    
    waffel_cloud =  m_menu_back->getChildByTag(101);
    waffel_cloud->setVisible(false);
    

    end_positon_cloud = ccp(win_size.width*0.5, -win_size.height*0.15);

    
    createPlane();

    schedule(schedule_selector(CKMainMenuScene::nextSky),TIME_NEXT_SKY);
    
    if(m_plane)
    {
        schedule(schedule_selector(CKMainMenuScene::animationEye),3);
        
        schedule(schedule_selector(CKMainMenuScene::animationPart),1);
        schedule(schedule_selector(CKMainMenuScene::animationScarf),1);
    };

    CKHelper::Instance().getDialog();
    if(CKHelper::Instance().menu_number != CK::SCENE_MAIN && CKHelper::Instance().menu_number != CK::SCENE_WORLD)
        CKAudioMng::Instance().playMenuSound();
    CKHelper::Instance().menu_number = CK::SCENE_MAIN;
};

void CKMainMenuScene::initSky()
{
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(CK::loadImage("a_MainMenuAnimation.plist").c_str());
    m_sky[0] = CCSprite::createWithSpriteFrameName("sky_marmelad.png");
    m_sky[1] = CCSprite::createWithSpriteFrameName("sky_choko.png");
    m_sky[2] = CCSprite::createWithSpriteFrameName("sky_ice.png");
    m_sky[3] = CCSprite::createWithSpriteFrameName("sky_waffel.png");
    
    for (int i = 0; i < 4; ++i) {
        m_sky[i]->setPosition(ccp(win_size.width*0.5,win_size.height*0.5));
        m_sky[i]->setScaleX(win_size.width/m_sky[i]->getContentSize().width);
        m_sky[i]->setScaleY(win_size.height/m_sky[i]->getContentSize().height);
        m_sky[i]->setVisible(false);
        addChild(m_sky[i],0);
    };
    m_sky[current_sky]->setVisible(true);
};

void CKMainMenuScene::createPlane()
{
    m_plane = new CKParalaxNode;
    m_plane->create(CCNode::create());
    m_plane->load(CK::loadFile("c_MainMenuPlane.plist").c_str());
    m_plane->getChildByTag(35)->getNode()->runAction(CCRepeatForever::create(CCRotateBy::create(0.5, 360)));
    m_plane->get()->setPosition(m_backnode->getChildByTag(1)->getPos());
    
    CCNode *plane_scarf = m_plane->getChildByTag(1)->getNode();
    
    tb_sprite = TBSpriteMask::create(CK::loadImage("pilot_scarf.png").c_str());
    tb_sprite->setPosition(plane_scarf->getPosition());
    m_plane->get()->addChild(tb_sprite,plane_scarf->getZOrder());
    tb_sprite->setRotation(plane_scarf->getRotation());
    CCSpriteFrame *frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("plane_scarf_mask.png");
    tb_sprite->setAnchorPoint(plane_scarf->getAnchorPoint());
    tb_sprite->buildMaskWithTexture(frame->getTexture(),frame->getRect(),frame->isRotated());

    plane_scarf->setRotation(0);
    plane_scarf->removeFromParentAndCleanup(false);
    
    left_eye = m_plane->getChildByTag(38);
    position_eye =  ccpAdd(m_plane->get()->getPosition(),left_eye->getPos());
    right_eye = m_plane->getChildByTag(39);
    addChild(m_plane->get(),1);
};

void CKMainMenuScene::animationScarf()
{
    float time = 1.0;
    int angle = rand()%12;
    if(tb_sprite->getRotation() > 25)
    {
        angle *= -1;
    };
    tb_sprite->runAction(CCEaseSineInOut::create(CCRotateTo::create(time,25+angle)));
};

void CKMainMenuScene::animationPart()
{
    float time = 1.0;
    int angle = rand()%12;
    if(m_plane->get()->getRotation() > 0)
    {
        angle *= -1;
    };
    m_plane->get()->runAction(CCEaseSineInOut::create(CCRotateTo::create(time, angle)));
};

void CKMainMenuScene::animationEye()
{
    int value = (rand()%36)*20 - 360;
    float time_rotate = MIN(3.0,abs(value)/20.0);
    
    left_eye->getNode()->runAction(CCEaseSineInOut::create(CCRotateBy::create(time_rotate, value)));
    left_eye->getChildByTag(1)->getNode()->runAction(CCEaseSineInOut::create(CCRotateBy::create(time_rotate, -value)));
    
    right_eye->getNode()->runAction(CCEaseSineInOut::create(CCRotateBy::create(time_rotate, value)));
    right_eye->getChildByTag(1)->getNode()->runAction(CCEaseSineInOut::create(CCRotateBy::create(time_rotate, -value)));
};

void CKMainMenuScene::lookEyeToPoint(const CCPoint &_pos)
{
    float angle_left = getAngleBeetwenTwoPoint(_pos,position_eye) - 40;
    if(!left_eye)
        return;
    
    left_eye->getNode()->stopAllActions();
    right_eye->getNode()->stopAllActions();
    left_eye->getChildByTag(1)->getNode()->stopAllActions();
    right_eye->getChildByTag(1)->getNode()->stopAllActions();
    
    left_eye->getNode()->setRotation(angle_left);
    right_eye->getNode()->setRotation(angle_left);
    right_eye->getChildByTag(1)->getNode()->setRotation(-angle_left);
    left_eye->getChildByTag(1)->getNode()->setRotation(-angle_left);
};

void CKMainMenuScene::shineIce(CCNode *_sender,void *_d)
{
    if(current_sky != SKY_ICE)
    {
        return;
    };
    CCSprite * sprite = (CCSprite *)_d;
    //CCLOG("get Opacity %f",sprite->getOpacity());
    float time_hide = (sprite->getOpacity()/255.0)*6.0;
    float time_show = 6.0;
    
    float value = (win_size.width == 1024)?12:6;
    
    float koef_move = (rand()%2 == 0)?-value:value;
    float move_hide = -koef_move*time_show;
    float move_show = koef_move*time_show;
    
    CCFiniteTimeAction * actionFade = NULL;
    
    if(sprite->getOpacity() > 250)
    {

        actionFade = CCSequence::create(CCFadeOut::create(time_hide),CCFadeIn::create(time_show/2),CCDelayTime::create(time_show/2),NULL);
    }
    else
    {
         actionFade = CCSequence::create(CCFadeIn::create(time_show/2),CCDelayTime::create(time_show/2),NULL);

    }
    
    CCFiniteTimeAction * actionMove = CCSequence::create(CCEaseSineInOut::create(CCMoveBy::create(time_hide, ccp(move_hide,0))),CCEaseSineInOut::create(CCMoveBy::create(time_show, ccp(move_show,0))),NULL);
    CCFiniteTimeAction * action = CCSpawn::create(actionFade,actionMove,NULL);
    CCAction * call_func = NULL;
    call_func = CCCallFuncND::create(this, callfuncND_selector(CKMainMenuScene::shineIce),(void *)sprite);
    sprite->runAction(CCSequence::create(action,call_func,NULL));
};

void CKMainMenuScene::flyPlaneAnimation()
{
    CCSprite * sprite = static_cast<CCSprite *>(m_plane->get());
    sprite->getTexture()->setAntiAliasTexParameters();
    CCAction * rotate_first = CCEaseSineInOut::create(CCRotateBy::create(1.0, 45));
    CCAction * rotate_1 = CCEaseSineInOut::create(CCRotateBy::create(2.0, -90));
    
    CCAction *action = CCSequence::create((CCActionInterval *)rotate_first,rotate_1,rotate_first,CCCallFunc::create(sprite, callfunc_selector(CKMainMenuScene::flyPlaneAnimation)),NULL);
    sprite->runAction(action);
};

void CKMainMenuScene::hideNode(CCNode *_sender)
{
    _sender->setVisible(false);
};

void CKMainMenuScene::skewCloud(CCNode *_sender,void *_d)
{
//    float keof_skew = 0.02;
//    ((CCSprite *)_d)->runAction(CCSequence::create(CCSkewBy::create(10.0, -win_size.width*keof_skew,0),CCSkewBy::create(10.0, win_size.width*keof_skew, 0),CCCallFuncND::create(this, callfuncND_selector(CKMainMenuScene::skewCloud),_d),NULL));
};

void CKMainMenuScene::nextSky()
{
  //  unschedule(schedule_selector(CKMainMenuScene::nextSky));
    m_sky[current_sky]->runAction(CCSequence::create(CCDelayTime::create(3.0),CCFadeOut::create(TIME_FADE_SKY),CCCallFuncN::create(m_sky[current_sky], callfuncN_selector(CKMainMenuScene::hideNode)),NULL));
    current_sky++;
    if(current_sky >= 4)
    {
        current_sky = SKY_MARMELAD;
    };
    
    if(current_sky == SKY_ICE)
    {
        node_it = ice_list.begin();
        while (node_it != ice_list.end()) {
            CCSprite *sprite = static_cast<CCSprite *>(*node_it);
            CCAction *call_func = CCCallFuncND::create(this, callfuncND_selector(CKMainMenuScene::shineIce),(void *)sprite);
            sprite->setVisible(true);
            sprite->setOpacity(0);
            sprite->runAction(CCSequence::create(CCDelayTime::create(1.0 + rand()%20/10.0),call_func,NULL));
            node_it++;
        };
    }
    else
    {
        node_it = ice_list.begin();
        while (node_it != ice_list.end()) {
            CCSprite *sprite = static_cast<CCSprite *>(*node_it);
            if(sprite->isVisible())
            {
                CCAction *call_func = CCCallFuncN::create(sprite, callfuncN_selector(CKMainMenuScene::hideNode));
                sprite->stopAllActions();
                sprite->runAction(CCSequence::create(CCDelayTime::create(1.0),CCFadeOut::create(1.5),call_func,NULL));
            }
            node_it++;
        };
    };
    if (current_sky == SKY_WAFFEL) {
        waffel_cloud->setVisible(true);
        CCSprite *sprite = static_cast<CCSprite *>(waffel_cloud->getNode());
        sprite->setOpacity(0);
        sprite->runAction(CCFadeIn::create(1.5));
    }
    else if(waffel_cloud->isVisible())
    {
        CCSprite *sprite = static_cast<CCSprite *>(waffel_cloud->getNode());
        sprite->runAction(CCSequence::create(CCDelayTime::create(1.0),CCFadeOut::create(1.5),CCCallFuncN::create(sprite, callfuncN_selector(CKMainMenuScene::hideNode)),NULL));
    };
    
    m_sky[current_sky]->setOpacity(0);
    m_sky[current_sky]->setVisible(true);
    m_sky[current_sky]->runAction(CCSequence::create(CCDelayTime::create(3.0),CCFadeIn::create(TIME_FADE_SKY),NULL));
    
  //  schedule(schedule_selector(CKMainMenuScene::nextSky),TIME_NEXT_SKY);
};

void CKMainMenuScene::showMsg()
{
    std::string path = CCFileUtils::sharedFileUtils()->getDocumentPath();
    path.append(CK::file_data_name);
    std::ifstream ifile(path.c_str());
    
    if((CKFileOptions::Instance().isEnableIcloud() && ICloudHelper::Instance().iCloudBetter()) || !ifile.is_open())
    {
        std::string path = CCFileUtils::sharedFileUtils()->getWriteablePath();
        path.append("~data.plist");
        std::ifstream source(path.c_str(), ios::binary);
        if(source.is_open())
        {
            CKHelper::Instance().getDialog()->setTarget(this);
            CKHelper::Instance().getDialog()->setConfirmText("Do you want to sync game progress with iCloud?");
            CKHelper::Instance().getDialog()->show(callfuncN_selector(CKMainMenuScene::syncCloud));
            source.close();
        }
    }
    ifile.close();
};

void CKMainMenuScene::syncCloud(void* _sender)
{
    if(_sender)
    {
        ICloudHelper::Instance().replaceDataFileWithIcloud();
        CKFileInfo::Instance().load();
    }
    else
    {
        CKFileInfo::Instance().load();
        CKFileOptions::Instance().enableIcloud(false);
    }
};

void CKMainMenuScene::updateCloud(const float & _dt)
{
    node_it = cloud_list.begin();
    while (node_it != cloud_list.end()) {
        CCPoint direct = ccpSub(end_positon_cloud,(*node_it)->getPosition());
        float length = ccpLength(direct);
        CCPoint new_pos = ccpAdd((*node_it)->getPosition(), ccpMult(ccpNormalize(direct),length*UPDATE_POSITION_CLOUD_KOEF));
        
        if(new_pos.y < -win_size.height*0.05)
        {
            (*node_it)->getParent()->reorderChild((*node_it), 29);
            new_pos = ccpSub((*node_it)->getPosition(),ccpMult( ccpNormalize(direct), win_size.height*(1.4) ));
            if((*node_it)->getTag() != current_sky)
            {
                if(current_sky == SKY_CHOCKO)
                {
                    (*node_it)->setVisible(true);
                    CCSpriteFrame * fr = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("cloud_choko.png");
                    static_cast<CCSprite*>(*node_it)->setTextureRect(fr->getRect(),fr->isRotated(),(*node_it)->getContentSize());
                }
                else if (current_sky == SKY_MARMELAD)
                {
                    (*node_it)->setVisible(true);
                    CCSpriteFrame * fr = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("cloud_marmelad.png");
                    static_cast<CCSprite*>(*node_it)->setTextureRect(fr->getRect(),fr->isRotated(),(*node_it)->getContentSize());
                }
                else
                {
                    (*node_it)->setVisible(false);
                }
                (*node_it)->setTag(current_sky);
            }
        }
            (*node_it)->setPosition(new_pos);
        
        (*node_it)->setScale(length*CLOUD_SCALE_KOEF);

        node_it++;
    };
};

void CKMainMenuScene::updateHill(const float & _dt)
{
    object_it = hill_list.begin();
    while (object_it != hill_list.end()) {
        CCPoint direct;
        CCSprite *sprite = static_cast<CCSprite *>(*object_it);
        if( sprite->getUserData() != NULL )
        {
            direct = ccpSub(end_positon_hill_left,sprite->getPosition());
        }
        else
        {
            direct = ccpSub(end_positon_hill_right,sprite->getPosition());
        }
        float length = ccpLength(direct);
        CCPoint new_pos = ccpAdd(sprite->getPosition(), ccpMult(ccpNormalize(direct),3.0/koef_device));//length*UPDATE_POSITION_HILL_KOEF
        //CCLOG("sasdsa %f",fabs(new_pos.x - win_size.width/2)/(win_size.width*1.5));
        float offset_f = 1.0 - fabs(new_pos.x - win_size.width/2)/(win_size.width*1.5);
        //CCLOG("sasdsa offset_f %f",offset_f);
        static_cast<TBSpriteMask*>(sprite)->setMaskRect(offset_f - 0.02, 1.0, offset_f, 0.0);
        if(new_pos.y < end_positon_hill_left.y*0.9)
        {
            new_pos.y = win_size.height*0.6;
            
            if( sprite->getUserData() != NULL)
            {
                new_pos.x = -win_size.width*0.5;
            }
            else
            {
                new_pos.x = win_size.width*1.5;
            };
            
            static_cast<TBSpriteMask*>(sprite)->setMaskRect(0.0, 1.0, 0.02, 0.0);
            sprite->setOpacity(255);
            sprite->setPosition(new_pos);
            sprite->getParent()->reorderChild(sprite, 39);
            sprite->setScale(2.8/koef_device);
            sprite->setRotation(rand()%360);
            
            updateHillSpriteFrame(sprite);
        }
        else
        {
            sprite->setPosition(new_pos);
        }
        sprite->setScale(2.0 + length*HILL_SCALE_KOEF);
       // float opacity_koef = MAX(0,MIN(255,length*HILL_OPASITY_KOEF));
        //sprite->setOpacity(MAX(0,MIN(255,length*HILL_OPASITY_KOEF*koef_device)));
       // sprite->setColor((ccColor3B){opacity_koef,opacity_koef,opacity_koef});
        object_it++;
    };
};

void CKMainMenuScene::updateHillSpriteFrame(CCSprite *_sprite)
{
    if(_sprite->getTag() != current_sky)
    {
        _sprite->setVisible(true);

        CCSpriteFrame * fr = NULL;
        
        if(current_sky == SKY_CHOCKO)
        {
            fr = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("hill_choko.png");
            
        } else if (current_sky == SKY_MARMELAD)
        {
           fr = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("hill_marmelad.png");
        } else if (current_sky == SKY_ICE)
        {
            fr = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("hill_ice.png");
        } else if (current_sky == SKY_WAFFEL)
        {
            fr = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("hill_waffel.png");
        };
        CCAssert(fr, "incorrecly curent number sky");
        _sprite->setTextureRect(fr->getRect(),fr->isRotated(),_sprite->getContentSize());
        _sprite->setTag(current_sky);

    }
};

void CKMainMenuScene::animationCloud(CCNode*_sender,void *_value)
{
    
};

void CKMainMenuScene::update(const float _dt)
{
    updateCloud(_dt);
    updateHill(_dt);
    if(waffel_cloud && waffel_cloud->isVisible())
    {
        waffel_cloud->update(ccp(0,0));
    };
    
    if(tb_sprite)
    {
        tb_sprite->move(0.01, 0.0);
    }
    if(spr_mask_play)
    {
        float step = 0.03*60.0*_dt;
        offset_mask_play += step;
        spr_mask_play->moveMask(0.0, step);
    }
};

void CKMainMenuScene::setLikes()
{
    CCLOG("CKMainMenuScene::setLikes");
    if (!CKFileInfo::Instance().isLikesAny())
    {
        CK::StatisticLog::Instance().setNewPage(Page_LikeComplite);
        CKAudioMng::Instance().playEffect("new_free_coins");
        char str[NAME_MAX];
        sprintf(str, LCZ("You earn %d coins."),1000);
        CKHelper::Instance().getDialog()->setTarget(this);
        CKHelper::Instance().getDialog()->setInfoText(8,LCZ("Thanks!"),str);
        CKHelper::Instance().getDialog()->showMessage(8,callfuncND_selector(CKMainMenuScene::menuCall));
        CKFileInfo::Instance().addBigScore(1000,true);
        CKFileInfo::Instance().setLikesAny(true);
        CKFileInfo::Instance().save();
        
    }
};

void CKMainMenuScene::tweetCallBack(CCNode *_node,int *_value)
{
    CCLOG("KMainMenuScene::tweetCallBack(CCNode *_node,int *_value)");
    if (!CKFileInfo::Instance().isFollowAny())
    {
        CKAudioMng::Instance().playEffect("new_free_coins");
        CK::StatisticLog::Instance().setNewPage(Page_LikeComplite);
        char str[NAME_MAX];
        sprintf(str, LCZ("You earn %d coins."),1000);
        CKHelper::Instance().getDialog()->setTarget(this);
        CKHelper::Instance().getDialog()->setInfoText(8,LCZ("Thanks!"),str);
        CKHelper::Instance().getDialog()->showMessage(8,callfuncND_selector(CKMainMenuScene::menuCall));
        CKFileInfo::Instance().addBigScore(1000,true);
        CKFileInfo::Instance().setFollowAny(true);
        CKFileInfo::Instance().save();
    }
};

void CKMainMenuScene::mailCallBack(CCNode *_node,int *_value)
{
    CCLOG("KMainMenuScene::mailCallBack(CCNode *_node,int *_value)");
    if (!CKFileInfo::Instance().isTellFriend())
    {
        CKAudioMng::Instance().playEffect("new_free_coins");
        char str[NAME_MAX];
        sprintf(str, LCZ("You earn %d coins."),1000);
        CKHelper::Instance().getDialog()->setTarget(this);
        CKHelper::Instance().getDialog()->setInfoText(8,LCZ("Thanks!"),str);
        CKHelper::Instance().getDialog()->showMessage(8,callfuncND_selector(CKMainMenuScene::menuCall));
        CKFileInfo::Instance().addBigScore(1000,true);
        CKFileInfo::Instance().setTellFriend(true);
        CKFileInfo::Instance().save();
    }
};

void CKMainMenuScene::refreshLeaderboard()
{
    if(m_lead)
        m_lead->refresh();
};

void CKMainMenuScene::callUpdate(CCNode *_sender,void *_value)
{
    switch(*static_cast<int*>(_value))
    {
        case CK::Action_Yes:
            CKAudioMng::Instance().playEffect("button_pressed");
            CK::StatisticLog::Instance().setLink(Link_UpdateApp);
            ObjCCalls::goToURL(update_url.c_str());
            break;
        case CK::Action_No:
        case CK::Action_Close:
            m_update->get()->setVisible(false);
            CKAudioMng::Instance().playEffect("button_pressed");
            break;
    };
};

void CKMainMenuScene::callDialog(CCNode *_sender,void *_value)
{
    switch(*static_cast<int*>(_value))
    {
        case CK::Action_Yes:
            break;
           // resetGame();
        case CK::Action_No:
        case CK::Action_Close:
            CK::StatisticLog::Instance().setNewPage(Page_MainMenu);
            m_dialog->get()->setVisible(false);
            CKAudioMng::Instance().playEffect("button_pressed");
            break;
    };
};

void CKMainMenuScene::callFbDialog(CCNode *_sender,void *_value)
{
    switch(*static_cast<int*>(_value))
    {
        case CK::Action_Yes:
            CK::StatisticLog::Instance().setLink(CK::Link_FacebookLogin);
            if(!CKFacebookHelper::Instance().FB_isOpenSession())
            {
                CKFacebookHelper::Instance().setNextCallFuncName("FB_LoadFriendsList");
                CKFacebookHelper::Instance().FB_Login();
            }
        case CK::Action_No:
        case CK::Action_Close:
            m_fb_dialog->get()->setVisible(false);
            CC_SAFE_DELETE(m_fb_dialog);
            CKAudioMng::Instance().playEffect("button_pressed");
            break;
    };
    CKFileOptions::Instance().setFirstLaunch(false);
    CKFileOptions::Instance().save();
};

void CKMainMenuScene::menuCall(CCNode* _sender,void *_value)
{
     CCLOG("_value %d node_tag %d" ,*static_cast<int*>(_value),_sender->getTag());

    switch(*static_cast<int*>(_value))
    {
        case CK::Action_Play:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            if(m_fb_dialog && !CKFileOptions::Instance().isKidsModeEnabled() && ObjCCalls::isActiveConnection())
            {
                m_fb_dialog->get()->setVisible(true);
                return;
            };
            CKFileInfo::Instance().load();
            ObjCCalls::sendEventGA("start_session","session_start","start_session",1);//GA Фіксуємо кількість натискань кнопки Play в меню, тобто кількість ігрових сесій

            unscheduleUpdate();
                        // run
            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(SCENE_WORLD);
        };
            break;
        case CK::Action_Facebook:
            CKAudioMng::Instance().playEffect("button_pressed");
#if !DISABLE_FACEBOOK      
            CK::StatisticLog::Instance().setLink(CK::Link_ChilibiteLike);
            CKHelper::Instance().FB_need_check_likes_main = false;
            if (!CKFileInfo::Instance().isLikesAny()) {
                CKHelper::Instance().FB_need_check_likes_main = true;
            };
            CKFacebookHelper::Instance().FB_goToPageLikeAnyPage();

#endif
            break;
        case CK::Action_Tweet:
            
            CKAudioMng::Instance().playEffect("button_pressed");
#if !DISABLE_TWITTER
            CK::StatisticLog::Instance().setLink(CK::Link_ChilibiteFollow);
            CKFileInfo::Instance().load();
            CKHelper::Instance().TW_need_ckeck_follow = true;
           // char str_url[NAME_MAX];
          //  sprintf(str_url, "twitter://user?id=%s",TWEET_FOLLOW_CHBT);
            //ObjCCalls::goToURL(str_url);
            CKTwitterHelper::Instance().goToTwitter(TWEET_FOLLOW_CHBT);
            //ObjCCalls::TweetFollowing(this,callfuncND_selector(CKMainMenuScene::tweetCallBack),TWEET_FOLLOW);
#endif
            break;

        case CK::Action_Options:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            if(CKFileOptions::Instance().isKidsModeEnabled())
            {
                CK::StatisticLog::Instance().setNewPage(Page_KM_OFF);
                m_dialog->get()->setVisible(true);
                break;
            }
#if !DISABLE_OPTION_SCENE//CKOptionsScene
//            CCScene *pScene = CKOptionLayer::scene();
//            // run
//            CCDirector::sharedDirector()->replaceScene(pScene);
            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(SCENE_OPTION);
#endif
        }
            break;
            case CK::Action_Show:
            {
                CKAudioMng::Instance().playEffect("back_menu");
                left_bar->getNode()->stopAllActions();
                right_bar->getNode()->stopAllActions();
                
                if(!showed_app)
                {
                    CK::StatisticLog::Instance().setNewPage(Page_Social);
                    //show bar
                    left_bar->getNode()->runAction(CCEaseElasticOut::create(CCMoveTo::create(1.5, left_bar_position)));
                    right_bar->getNode()->runAction(CCEaseElasticOut::create(CCMoveTo::create(1.5, right_bar_positon)));
                }
                else
                {
                    CK::StatisticLog::Instance().setNewPage(Page_MainMenu);
                    //hide bar
                    left_bar->getNode()->runAction(CCMoveTo::create(0.25, ccp(left_bar_position.x - DISTANCE_MOVE_APP_X, left_bar_position.y)));
                    right_bar->getNode()->runAction(CCMoveTo::create(0.25, ccp(right_bar_positon.x + DISTANCE_MOVE_APP_X, right_bar_positon.y)));
                };
                showed_app = !showed_app;
            }
            break;
        case Action_Music:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            bool sound = !CKFileOptions::Instance().isPlayMusic();
            
          //  CKFileOptions::Instance().setPlayEffect(sound);
            CKFileOptions::Instance().setPlayMusic(sound);
            if(sound)
            {
                if(!CKAudioMng::Instance().isPlayBackground())
                {
                    CKAudioMng::Instance().playBackground("bg");
                }
                else
                {
                    CKAudioMng::Instance().resumeBackground();
                }
                CKAudioMng::Instance().setVolumeBgd(0.0);
                CKAudioMng::Instance().fadeBgToVolume(1.0);
            }
            else
            {
                CKAudioMng::Instance().pauseBackground();
                CKAudioMng::Instance().setVolumeBgd(0.0);
            }
            
            CKObject* _node = m_backnode->getChildByTag(Action_Music);
            if(_node)
            {
                if(_node->getType() == CK::GuiRadioBtn)
                {
                    _node->setStateChild(!sound);
                };
            };
            
            
        };
            break;

        case Action_Rating:
            CKAudioMng::Instance().playEffect("button_pressed");
            CK::StatisticLog::Instance().setLink(CK::Link_GamecenterLeaderboard);
            if(!m_lead)
            {
                m_lead = new CKLeaderboard;
            }
            m_lead->activeted(this);
            ObjCCalls::showLeaderboard();
            break;

        case Action_TellFriends:
            CKAudioMng::Instance().playEffect("button_pressed");
            CK::StatisticLog::Instance().setLink(CK::Link_TellFriend);
            ObjCCalls::tellFriend();
            break;
        case CK::Action_Inventory:
            CKAudioMng::Instance().playEffect("button_pressed");
#ifndef DISABLE_INVENTORY
            CKFileInfo::Instance().load();
            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(SCENE_INVENTORY);
#endif
            break;
        case CK::Action_Shop_Coins:
            CKAudioMng::Instance().playEffect("button_pressed");
            CKHelper::Instance().show_shop_coins = 3;
            CKHelper::Instance().show_shop_bomb = 0;
        case CK::Action_Shop:
            CKAudioMng::Instance().playEffect("button_pressed");
#ifndef DISABLE_SHOP
            CKFileInfo::Instance().load();
            CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
            CKHelper::Instance().playScene(SCENE_SHOP);
#endif
            break;
            
            case Action_EveryPlay_Show:
            CK::StatisticLog::Instance().setLink(Link_EveryplayVideo);
            CKAudioMng::Instance().playEffect("button_pressed");
            if(ObjCCalls::isLittleMemory())
            {
                ObjCCalls::goToURL(EVERYPLAY_URL);
            }
            else
            {
                ObjCCalls::everyplayShow();
            }
            break;
        case Action_Rate_App:
        {
            CK::StatisticLog::Instance().setLink(CK::Link_RateApp);
            CKAudioMng::Instance().playEffect("button_pressed");
            char str[NAME_MAX];
            sprintf(str, "itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=%s",APPLE_ID);
            ObjCCalls::goToURL(str);
        }
            break;
        case Action_Gift:
        {
            CKAudioMng::Instance().playEffect("button_pressed");
            CK::StatisticLog::Instance().setLink(CK::Link_SendGift);
            ObjCCalls::goToGift();
        }
            break;
        case Action_Ok:
            CK::StatisticLog::Instance().setNewPage(Page_MainMenu);
            break;
        default:
            CKAudioMng::Instance().playEffect("button_pressed");
            break;
            //CCAssert(0, "Uknow tag");
    };
    
};


void CKMainMenuScene::disableKidsMode()
{
    CKFileOptions::Instance().setKidsMode(false);
    CKFileOptions::Instance().save();
    
    CKAudioMng::Instance().playEffect("disable_kidsmode");
    
    CKObject *obj = m_backnode->getChildByTag(Action_Rating);
    obj->setVisible(true);
    obj->getNode()->setScale(0.0);
    obj->getNode()->runAction(CCEaseElasticOut::create(CCScaleTo::create(1.0, 1.0)));
    
    obj = m_backnode->getChildByTag(Action_Shop_Coins);
    obj->setVisible(true);
    obj->getNode()->setScale(0.0);
    obj->getNode()->runAction(CCEaseElasticOut::create(CCScaleTo::create(1.0, 1.0)));
    
    obj = m_backnode->getChildByTag(Action_Show);
    obj->setVisible(true);
    obj->getNode()->setScale(0.0);
    obj->getNode()->runAction(CCEaseElasticOut::create(CCScaleTo::create(1.0, 1.0)));
    
    
    obj = m_backnode->getChildByTag(Action_Shop);
    obj->setVisible(true);
    obj->getNode()->setScale(0.0);
    obj->getNode()->runAction(CCEaseElasticOut::create(CCScaleTo::create(1.0, 1.0)));
    
    m_backnode->getChildByTag(Action_Options)->getChildByTag(1)->setStateImage(0);
    
    CK::StatisticLog::Instance().setNewPage(Page_MainMenu);
   // ObjCCalls::FB_LoadFriendsList();
};

void CKMainMenuScene::timeResetGame()
{
    m_dialog->get()->setVisible(false);
    disableKidsMode();
};

#pragma mark - UPDATE
void CKMainMenuScene::ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    if(!isVisible())
        return;
    CCSetIterator it;
    CCTouch* touch;
    
    if(touches->count() <= 1 && left_eye)
    {
        schedule(schedule_selector(CKMainMenuScene::animationEye),1);
    };
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        
        if(m_update && m_update->get()->isVisible())
        {
            m_update->setTouchEnd(location);
            return;
        };
        
        if(m_dialog && m_dialog->get()->isVisible())
        {
            unschedule(schedule_selector(CKMainMenuScene::timeResetGame));
            m_dialog->setTouchEnd(location);
            return;
        };
        
        if(m_fb_dialog && m_fb_dialog->get()->isVisible())
        {
            m_fb_dialog->setTouchEnd(location);
            return;
        };
        
        if(m_backnode)
            m_backnode->setTouchEnd(location);

    }
};

void CKMainMenuScene::ccTouchesMoved(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    if(!isVisible())
        return;
    CCSetIterator it;
    CCTouch* touch;
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        lookEyeToPoint(location);
        if(m_backnode)
            m_backnode->setTouchMove(location);
    }
};

void CKMainMenuScene::ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
  //  CCLOG("ccTouchesBegan:: scene %d layer %d ",this->getParent(),this);
    if(!isVisible())
        return;
    CCSetIterator it;
    CCTouch* touch;
    
    if(touches->count() > 0)
    {
        unschedule(schedule_selector(CKMainMenuScene::animationEye));
    };
    
    for( it = touches->begin(); it != touches->end(); it++)
    {
        touch = (CCTouch*)(*it);
        
        if(!touch)
            break;
        CCPoint location = touch->getLocationInView();
        location = CCDirector::sharedDirector()->convertToGL(location);
        lookEyeToPoint(location);
        
        if(m_update && m_update->get()->isVisible())
        {
            m_update->setTouchBegan(location);
            return;
        };
        
        if(m_dialog && m_dialog->get()->isVisible())
        {
            
            m_dialog->setTouchBegan(location);
            if (m_dialog->getCurrnet() && m_dialog->getCurrnet()->getTag() == CK::Action_Yes) {
                CKAudioMng::Instance().playEffect("button_pressed");
                schedule(schedule_selector(CKMainMenuScene::timeResetGame),KM_TIME_RESET_GAME,0, KM_TIME_RESET_GAME);
            };
            return;
        };
        
        if(m_fb_dialog && m_fb_dialog->get()->isVisible())
        {
            m_fb_dialog->setTouchBegan(location);
            return;
        };
        if(m_backnode)
            m_backnode->setTouchBegan(location);
    }
};
