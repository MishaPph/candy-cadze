//
//  CKOptionsScene.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 26.09.12.
//
//

#include "CKOptionsScene.h"
#include "CKMainMenuScene.h"
#include "CK.h"
#include "CKHelper.h"
#include "CKLanguage.h"
#include "ICloudHelper.h"
#include <stdio.h>

const char *panel_str[4] = {"top","left","right","bottom"};
const char *dbg_mode[2] = {"Open all levels: NO ","Open all levels: YES"};
const char *banner[2] = {"BANNER: HIDE","BANNER: SHOW"};
const char *everyplay[2] = {"Everyplay: NO","Everyplay: YES"};
const char *static_bg[2] = {"STATIC BG: NO","STATIC BG: YES"};
const char *upgrade[3] = {"Set bomb power: 1","Set bomb power: 2","Set bomb power: 3"};

const char *cloud_str[2] = {"Use iCloud: NO","Use iCloud: YES"};

CKOptionsScene::CKOptionsScene()
{
    CCSize win_size = CCDirector::sharedDirector()->getWinSize();
    CCMenu *itemMenu = CCMenu::create(NULL);
    
    int mult = (win_size.width != 1024)?1:2;
       
    CCLabelTTF *label2 = CCLabelTTF::create("Back","Arial",24*mult);
    label2->setColor(ccc3(255,255,255));
    CCMenuItemLabel *menuItem2 = CCMenuItemLabel::create(label2,this,menu_selector(CKOptionsScene::menuCallback));
    
    menuItem2->setPosition(ccp(win_size.width*0.1,win_size.height*0.9));
    menuItem2->setTag(CK::Action_Back);
    itemMenu->addChild(menuItem2,5);
    itemMenu->setPosition(CCPointZero);
 //   itemMenu->alignItemsVerticallyWithPadding(10*mult);
    
  //  std::string sp= "speed:";
    
    CCMenu *glbMenu = CCMenu::create(NULL);
    
    CCLabelTTF *debug_mode = CCLabelTTF::create(dbg_mode[CKFileOptions::Instance().isDebugMode()],"Arial",24*mult);
    debug_mode->setColor(ccc3(255,50,255));
    debug_mode->setTag(CKFileOptions::Instance().isDebugMode());
    CCMenuItemLabel *menuItem = CCMenuItemLabel::create(debug_mode,this,menu_selector(CKOptionsScene::menuCallbackSpeed));
    menuItem->setTag(2);
    menuItem->setPosition(win_size.width*0.5,win_size.height*0.1);
    
    glbMenu->addChild(menuItem);
    
    CCLabelTTF *banner_s = CCLabelTTF::create(banner[CKFileOptions::Instance().isBannerShow()],"Arial",24*mult);
    banner_s->setColor(ccc3(210,50,255));
    banner_s->setTag(CKFileOptions::Instance().isBannerShow());
    item_banner = CCMenuItemLabel::create(banner_s,this,menu_selector(CKOptionsScene::menuCallbackSpeed));
    item_banner->setTag(3);
    item_banner->setPosition(win_size.width*0.5,win_size.height*0.2);
    glbMenu->addChild(item_banner);
    
    CCLabelTTF *every_s = CCLabelTTF::create(everyplay[CKFileOptions::Instance().isEveryPlayShow()],"Arial",24*mult);
    every_s->setColor(ccc3(210,50,255));
    every_s->setTag(CKFileOptions::Instance().isEveryPlayShow());
    item_everyplay = CCMenuItemLabel::create(every_s,this,menu_selector(CKOptionsScene::menuCallbackSpeed));
    item_everyplay->setTag(6);
    item_everyplay->setPosition(win_size.width*0.5,win_size.height*0.3);
    glbMenu->addChild(item_everyplay);
    
    CCLabelTTF *cloud_s = CCLabelTTF::create(cloud_str[CKFileOptions::Instance().isEnableIcloud()],"Arial",24*mult);
    banner_s->setColor(ccc3(110,150,55));
    banner_s->setTag(CKFileOptions::Instance().isBannerShow());
    item_cloud = CCMenuItemLabel::create(cloud_s,this,menu_selector(CKOptionsScene::menuCallbackSpeed));
    item_cloud->setTag(7);
    item_cloud->setPosition(win_size.width*0.5,-win_size.height*0.05);
    glbMenu->addChild(item_cloud);
    
    CCLabelTTF *l_stat = CCLabelTTF::create(static_bg[ObjCCalls::isLowCPU()],"Arial",16*mult);
    l_stat->setColor(ccc3(210,50,255));
    item_static= CCMenuItemLabel::create(l_stat,this,menu_selector(CKOptionsScene::menuCallbackSpeed));
    item_static->setTag(14);
    item_static->setPosition(win_size.width*0.5,-win_size.height*0.2);
    glbMenu->addChild(item_static);
    
//    find language id
    num_language = 0;
    for(int i = 0;i < CK::COUNT_LANGUAGE;i++)
    {
        if(strcmp(CK::language_str[i], CKLanguage::Instance().getCurrent()) == 0)
        {
            num_language = i;
            break;
        };
    }
    CCLabelTTF *language = CCLabelTTF::create(CK::language_str[num_language],"Arial",38*mult);
    language->setColor(ccc3(60,150,255));
    language->setTag(19);
    menuItem = CCMenuItemLabel::create(language,this,menu_selector(CKOptionsScene::menuCallbackSpeed));
    menuItem->setTag(19);
    menuItem->setPosition(win_size.width*0.5,-win_size.height*0.3);
     glbMenu->addChild(menuItem);
    glbMenu->setPosition(0,win_size.height*0.5);
    
    label_language = CCLabelTTF::create(CKLanguage::Instance().getLocal("Language") ,"Arial",18*mult);
    label_language->setColor(ccc3(60,150,255));
    label_language->setPosition(ccp(win_size.width*0.3,win_size.height*0.2));
    addChild(label_language,5);

    setTouchEnabled(true);
    addChild(itemMenu);
    addChild(glbMenu);
    schedule(schedule_selector(CKOptionsScene::updateSlide), 0.1);
    
};

void CKOptionsScene::updateSlide(const float &_dt)
{
//    char str[NAME_MAX];
//    sprintf(str, "speed:%.1f",_slider->getValue());
//    
  //  slide->setString(str);
};

void CKOptionsScene::menuCallbackSpeed(CCNode* _sender)
{
    switch (_sender->getTag()) {
        case 1:
        {
            CKFileOptions::Instance().setBombLvl(CKFileOptions::Instance().getBombLvl() + 1);
            CCLabelTTF * label = (CCLabelTTF *)((CCMenuItemLabel*)_sender)->getLabel();
            label->setTag(CKFileOptions::Instance().getBombLvl());
            label->setString(upgrade[CKFileOptions::Instance().getBombLvl()-1]);
        }
            break;
        case 2:
        {
//            CKFileOptions::Instance().setDebugMode(!CKFileOptions::Instance().isDebugMode());
//            CCLabelTTF * label = (CCLabelTTF *)((CCMenuItemLabel*)_sender)->getLabel();
//            label->setTag(CKFileOptions::Instance().isDebugMode());
//            label->setString(dbg_mode[CKFileOptions::Instance().isDebugMode()]);
//            item_upgrade->setVisible(CKFileOptions::Instance().isDebugMode());
        }
            break;
        case 3:
        {
            CKFileOptions::Instance().setBannerShow(!CKFileOptions::Instance().isBannerShow());
            CCLabelTTF * label = (CCLabelTTF *)((CCMenuItemLabel*)_sender)->getLabel();
            label->setTag(CKFileOptions::Instance().isBannerShow());
            label->setString(banner[CKFileOptions::Instance().isBannerShow()]);
        }
            break;
        case 4:
        {
            CKFileOptions::Instance().setPlayMusic(!CKFileOptions::Instance().isPlayMusic());
            CCLabelTTF * label = (CCLabelTTF *)((CCMenuItemLabel*)_sender)->getLabel();
            label->setTag(CKFileOptions::Instance().isPlayMusic());
            label->setString(CK::sound_str[CKFileOptions::Instance().isPlayMusic()]);
        }
            break;
        case 6:
        {
            CKFileOptions::Instance().setEveryPlayShow(!CKFileOptions::Instance().isEveryPlayShow());
            CCLabelTTF * label = (CCLabelTTF *)((CCMenuItemLabel*)_sender)->getLabel();
            label->setTag(CKFileOptions::Instance().isEveryPlayShow());
            label->setString(everyplay[CKFileOptions::Instance().isEveryPlayShow()]);
        }
            break;
        case 5:
        {
            CKFileOptions::Instance().setPlayEffect(!CKFileOptions::Instance().isPlayEffect());
            CCLabelTTF * label = (CCLabelTTF *)((CCMenuItemLabel*)_sender)->getLabel();
            label->setTag(CKFileOptions::Instance().isPlayEffect());
            label->setString(CK::effect_str[CKFileOptions::Instance().isPlayEffect()]);
        }
            break;
        case 7:
        {
            CKFileOptions::Instance().enableIcloud(!CKFileOptions::Instance().isEnableIcloud());
            CCLabelTTF * label = (CCLabelTTF *)((CCMenuItemLabel*)_sender)->getLabel();
            label->setString(cloud_str[CKFileOptions::Instance().isEnableIcloud()]);
            if(CKFileOptions::Instance().isEnableIcloud())
            {
                ICloudHelper::Instance().loadWithIcloud();
            };
        }
            break;
        case 14:
        {
          //  CKHelper::Instance().static_bg = !ObjCCalls::low_cpu;
            CCLabelTTF * label = (CCLabelTTF *)((CCMenuItemLabel*)_sender)->getLabel();
            label->setString(static_bg[ObjCCalls::isLowCPU()]);
        }
            break;
        case 19:
        {
            //  CKHelper::Instance().static_bg = !ObjCCalls::low_cpu;
            CCLabelTTF * label = (CCLabelTTF *)((CCMenuItemLabel*)_sender)->getLabel();
            
            char str[255];
            sprintf(str, "Cookies_%s.png",CK::language_str[num_language]);
            CCTextureCache::sharedTextureCache()->removeTextureForKey(str);
            
            num_language++;
            if (num_language >= CK::COUNT_LANGUAGE) {
                num_language = 0;
            }
            label->setString(CK::language_str[num_language]);
            
            CKLanguage::Instance().load(CK::language_str[num_language]);
            CKLanguage::Instance().createFontAtlas(false);
            label_language->setString(CKLanguage::Instance().getLocal("Language"));
        }
            break;
        default:
            break;
    };
};

CKOptionsScene::~CKOptionsScene()
{
    
};

void CKOptionsScene::menuCallback(CCNode* _sender)
{
    switch(_sender->getTag())
    {
        case CK::Action_Back:
        {
            unscheduleAllSelectors();
            if(CKFileOptions::Instance().isDebugMode())
                CKFileInfo::Instance().setFileName(CK::file_data_debug_name);
            else
                CKFileInfo::Instance().setFileName(CK::file_data_name);
            
            CKAudioMng::Instance().stopBackground();
            CKAudioMng::Instance().removeAllEffect();
            CKAudioMng::Instance().removeBackground();
            CKFileOptions::Instance().save();
            
            CCScene *pScene = CKMainMenuScene::scene();
            // run
            CCDirector::sharedDirector()->replaceScene(pScene);
        };
            break;
        case CK::Action_Inventory:
        {

        }
            break;
        case CK::Action_Options:
            break;
        case CK::Action_Info:
            break;
        default:
            CCAssert(0, "Uknow tag");
    }
    
};

CCScene* CKOptionsScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    
    // add layer as a child to scene
    CCLayer* layer = new CKOptionsScene();
    if(layer->init())
    {
        scene->addChild(layer);
        layer->release();
    };
    return scene;
}