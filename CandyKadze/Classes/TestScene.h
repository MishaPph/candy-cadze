//
//  TestScene.h
//  CandyKadze
//
//  Created by PR1.Stigol on 20.05.13.
//
//

#ifndef __CandyKadze__TestScene__
#define __CandyKadze__TestScene__

#include <iostream>
#include "cocos2d.h"
#include "CKBackNode.h"

using namespace cocos2d;
class CKAirplane;
class CKCuneArray;
class TestScene : public cocos2d::CCLayer {
public:
    ~TestScene();
    TestScene();
    CKBaseNode* m_gui;
    // returns a Scene that contains the HelloWorld as the only child
    static cocos2d::CCScene* scene();
    
//    virtual void ccTouchesEnded(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
    virtual void ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event);
//    virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);
    void update(float _dt);
    virtual void draw();
    CKCubeArray *m_cube_array;
    CCNode *cube_node;
    bool b_win;
    //    virtual bool init();
private:
    float pos_x;
    CCSize win_size;
};

#endif /* defined(__CandyKadze__TestScene__) */
