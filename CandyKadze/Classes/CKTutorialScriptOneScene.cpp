//
//  CKTutorialScriptOneScene.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 08.04.13.
//
//

#include "CKTutorialScriptOneScene.h"
#include "CKTutorialScriptLayer.h"
#include "CKHelper.h"
#include "CKLoaderGUI.h"
#include "CKGameScene.h"

#pragma matk - ONE SCRIPT SCENE
CKTutorialScriptOneScene::CKTutorialScriptOneScene()
{
    win_size = CCDirector::sharedDirector()->getWinSize();
    
};

CKTutorialScriptOneScene *CKTutorialScriptOneScene::create()
{
    CKTutorialScriptOneScene *ref = new CKTutorialScriptOneScene;
    if(ref && ref->init())
    {
        return ref;
    }
    else
    {
        CC_SAFE_DELETE(ref);
    }
    return NULL;
};

CKTutorialScriptOneScene::~CKTutorialScriptOneScene()
{
    CCLOG("~CKTutorialScriptOneScene");
    m_gui->get()->removeFromParentAndCleanup(true);
    CC_SAFE_DELETE(m_gui);
    cur_sc->layer->getLayer()->removeFromParentAndCleanup(true);

    CCLOG("m_rtt %d",m_rtt->retainCount());
    m_rtt->removeFromParentAndCleanup(true);
    CC_SAFE_DELETE(cur_sc);
    CC_SAFE_DELETE(script);
    CC_SAFE_RELEASE_NULL(m_dict);
    removeAllChildrenWithCleanup(true);
};

void CKTutorialScriptOneScene::initGUI()
{
    m_gui = new CKGUI();
    m_gui->create(CCNode::create());
    m_gui->load(CK::loadFile("c_TutorialMono.plist").c_str());
    m_gui->setTouchEnabled(true);
    m_gui->setTarget(this);
    m_gui->addFuncToMap("menuCall", callfuncND_selector(CKTutorialScriptOneScene::menuCall));
    // m_gui->setVisible(false);
    this->addChild(m_gui->get(),20);
    
    CCNode *m_node = m_gui->getChildByTag(30)->getNode(); // background
    m_node->retain();
    m_node->setVisible(true);
    m_node->removeFromParentAndCleanup(false);
    this->addChild(m_node,0);
    m_node->release();
};

void CKTutorialScriptOneScene::initFrame(int _type,const CCSize &_sizeframe,const CCPoint &_pos)
{
    CCLOG("CKTutorialScriptOneScene type %d %f",_type,getPositionX());
    
    CKHelper::Instance().lvl_speed = 0;
    cur_sc = NULL;
    m_dict = CCDictionary::createWithContentsOfFile("tutorial_scripts.plist");
    m_dict->retain();

    initGUI();
    
   // return;
    
    size_frame = _sizeframe;
    
    cur_sc = new ScripRT;
    script = new CKLayerScript;
    script->initObject();
    if(_type < 10)//airplane
    {
        cur_sc->layer = new CKPlaneScriptLayer(script);
    }
    else
    {
        cur_sc->layer = new CKBombScriptLayer(script);
    }
    
    srand(time(0));
    
    size_frame_x = size_frame.width;
    m_rtt = CCRenderTexture::create(size_frame.width, size_frame.height, kCCTexture2DPixelFormat_RGBA8888, GL_DEPTH24_STENCIL8_OES);
    addChild(m_rtt,5);
    m_rtt->setPosition(_pos);
    
    m_node = CCNode::create();
    addChild(m_node,2);
    
    this->setTouchEnabled(true);
    
    m_node->setPositionX(size_frame_x/2);
    //cur_sc->layer->initObject();
    cur_sc->layer->setMono(true);
    cur_sc->layer->initWithType(m_dict,_type,size_frame.width/2);
    
    addChild(cur_sc->layer->getLayer(),1);

    
    if(CKHelper::Instance().getIsIpad())
    {
        cur_sc->layer->getLayer()->setPosition(ccp(-size_frame.width + size_frame.width/2 + CKHelper::Instance().BOX_SIZE*2.5 - 5,0));
    }
    else
    {
        float panel_left = (win_size.width == 568)?-44:0;
        cur_sc->layer->getLayer()->setPosition(ccp(-size_frame.width + panel_left + size_frame.width/2 + CKHelper::Instance().BOX_SIZE*1.5 - 6,0));
    };
    
    cur_sc->layer->getLayer()->setVisible(false);
    
    cur_sc->rt = CCRenderTexture::create(size_frame_x, size_frame.height, kCCTexture2DPixelFormat_RGBA8888, GL_DEPTH24_STENCIL8_OES);
    cur_sc->rt->setPosition(ccp(0*size_frame_x,size_frame.height/2));
    //cur_sc->rt->retain();
    m_node->addChild(cur_sc->rt,2);
    m_node->setVisible(false);
    cur_sc->rt->setVisible(false);
    
    cur_sc->rt->clear(0,0,0, 0);
    
    cur_sc->rt->begin();
    // bg->visit();
    cur_sc->layer->getLayer()->setVisible(true);
    cur_sc->layer->getLayer()->visit();
    cur_sc->rt->end();
    
    cur_sc->layer->getLayer()->setVisible(false);
    //cur_sc->layer->start();
    
    update(0);
    scheduleUpdate();
};

void CKTutorialScriptOneScene::update(float dt)
{
    if(cur_sc)
    {
        cur_sc->rt->clear(0,0, 0, 0);
        cur_sc->rt->begin();
        cur_sc->layer->getLayer()->setVisible(true);
        cur_sc->layer->getLayer()->visit();
        cur_sc->rt->end();
        cur_sc->layer->getLayer()->setVisible(false);
    };
    m_rtt->clear(0, 0, 0, 0);
    m_rtt->begin();
    m_node->setVisible(true);
    cur_sc->rt->setVisible(true);
    m_node->visit();
    m_node->setVisible(false);
    cur_sc->rt->setVisible(false);
    m_rtt->end();
};


void CKTutorialScriptOneScene::menuCall(CCNode *_sender,void *_data)
{
    setTouchEnabled(false);
    cur_sc->layer->pause();
    dynamic_cast<CKGameSceneNew *>(getParent())->menuCall(_sender, _data);
    this->removeFromParentAndCleanup(true);
    this->release();
    //    delete this;
};

void CKTutorialScriptOneScene::setEnable(bool _en)
{
    setVisible(_en);
    if(!_en)//stop animation
    {
        cur_sc->layer->pause();
    }else
    {
        cur_sc->layer->start();
        update(0);
    }
};

void CKTutorialScriptOneScene::ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    if(!isVisible())
        return;
    CCTouch* touch =(CCTouch*)*touches->begin();
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    location = convertToNodeSpace(location);
  //  CCLOG("ccTouchesBegan:: loactio x %f cont %f",location.x,convertToNodeSpace(location).x);
    m_gui->setTouchBegan(location);
    if(cur_sc)
    {
        if(win_size.width == 568)
            location.x +=44;
        cur_sc->layer->setTouchBegan(location);
    }
};

void CKTutorialScriptOneScene::ccTouchesMoved(CCSet *touches, CCEvent *pEvent)
{
    return;
    CCTouch* touch =(CCTouch*)*touches->begin();
    CCPoint location = touch->getLocationInView();
    
    location = CCDirector::sharedDirector()->convertToGL(location);
    
};


void CKTutorialScriptOneScene::ccTouchesEnded(CCSet *touches, CCEvent *pEvent)
{
    CCTouch* touch =(CCTouch*)*touches->begin();
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    location = convertToNodeSpace(location);
    m_gui->setTouchEnd(location);
    if(cur_sc)
    {
        cur_sc->layer->setTouchEnd(location);
    }
};