//
//  CK.h
//  CandyKadze
//
//  Created by PR1.Stigol on 18.09.12.
//
//

#ifndef CandyKadze_CK_h
#define CandyKadze_CK_h

#define dictStr(__dict__,__str__) ((CCString *)__dict__->objectForKey(__str__))->getCString()
#define dictInt(__dict__,__str__)((CCString *) __dict__->objectForKey(__str__))->intValue()
#define dictFloat(__dict__,__str__)((CCString *) __dict__->objectForKey(__str__))->floatValue()

#define SHOW_PARTICLE true

#define DISABLE_OPTION_SCENE false
#define DISABLE_TWITTER false
#define DISABLE_FACEBOOK false
#define DISABLE_SHOP_COINS true
#define DISABLE_HINTS false
#define ENABLE_CHECKING_HASH true
#define DISABLE_LIKE_FACEBOOK true
#define ENABLE_EVERYPLAY true

#define DISABLE_PARSE false
//#define DEBUG_MODE true
//#define DISABLE_SHOW_PLANE
#include "cocos2d.h"
#include "ObjCCalls.h"
#include "CKID.h"
#include "StatisticLog.h"

#pragma once


namespace CK
{
    const unsigned char COUNT_ROWS = 10;
    const unsigned char COUNT_COLUMNS = 15;
    const unsigned char BOX_SIZE_IPHONE = 32;
    const unsigned char BOX_SIZE_IPAD = 68;
    
    const unsigned char BONUS_AIRPLANE_COUNT = 0.6;
    
    const unsigned char COUNT_LEVEL_IN_WORLD = 16;
    const unsigned char COUNT_DIFFICULT = 4;
    const unsigned char COUNT_WORLD = 5;
    const unsigned char START_AIRPLANE_HEIGHT  = 40;
    const unsigned char SHOW_BANNER_AFTER_HEIGHT = 30;
    
    const float CURRENT_VERSION_LANGUAGE  = 5.3f;
    const float CURRENT_VERSION_SETTING  = 0.5f;
    
    //const float BASE_SOUND_VOLUME = 0.02f;
    
    const float TIME_TO_FADE_BEACKGROUND_SOUND = 0.3;
    const float SOUND_FADE_VALUE = 0.35;
    const float SOUND_FADE_LOADING = 0.2;
    
    const float LOW_SOUND_VOLUME = 0.04f;
    const float PI          = 3.14159265f;
    const int MAX_KOEF_DESTROY_COUNT_CUBE = 6;
    const int SIZE_INVENTORY_SELECT  = 4;
    
    static const int KM_TIME_RESET_GAME = 3;
    
    static const char* c_difficult[COUNT_DIFFICULT] = {"Easy","Standart","Fast","Hard"/*,"Crazy"*/};
    static const char* file_data_name = "data.plist";
    static const char* file_data_debug_name = "data_debug.plist";
    static const char* file_setting = "setting.inf";
    static const char* file_shop_bomb = "shop_bomb.plist";
    static int world_cost[COUNT_WORLD] = {0,0,5000,15000,40000};
    
    static const char *sound_str[2] = {"Sound: Off","Sound: On "};
    static const char *effect_str[2] = {"Effect: Off","Effect: On "};
    const int COUNT_LANGUAGE = 10;
    static const char *language_str[COUNT_LANGUAGE] = {"en","fr","de","es","ko","zh","ja","ru","pt","uk"};
    
    
    struct Inventory
    {
        unsigned char type;
        unsigned int  count;
    };
    enum
    {
      sBg = 0,
        
    };
    enum  {
        top_left = (1 << 0),
        top_right = (1 << 1),
        bottom_left = (1 << 2),
        bottom_right = (1 << 3),
    };
    enum
    {
        LAYERID_MAIN = 10,
        LAYERID_SHOP = 30,
        LAYERID_GAMESCENE = 20,
        LAYER_LOADING = 44,
        LAYER_OPTION = 45
    };
    enum
    {
        SCENE_WORLD = 2,
        SCENE_MAIN = 1,
        SCENE_INVENTORY = 3,
        SCENE_SHOP = 4,
        SCENE_GAME = 5,
        SCENE_GAME90 = 6,
        SCENE_OPTION = 7
        
    };
    enum
    {
      rAirplane_Win = 1,
      rAirplane_Over = 2,
      rAirplane_Splash = 3,
      rAirplane_Banner = 4
    };
    enum
    {
        GuiNode = 0,
        GuiPicture = 1,
        GuiButton = 2,
        GuiLabel = 3,
        GuiRadioBtn = 4,
        GuiSelector = 5,
        GuiScrollView = 6,
        GuiSlot = 7,
        GuiProgressBar = 8,
        GuiSlider = 9
    };
    enum
    {
        Bonus_Airplane_Up = 1,
        Bonus_Airplane_Slow = 2,
        Bonus_Airplane_Armor = 3,
        Bonus_Airplane_Speed = 4,
        Bonus_Airplane_Kamikadze = 5,
        Bonus_Airplane_Gun = 6,
        Bonus_Airplane_Bomber = 7,
        Bonus_Airplane_Phantom = 8,
    };
    
    enum {
        Action_Play = 1,
        Action_Inventory = 2,
        Action_Options = 3,
        Action_Next = 4,
        Action_Info = 5,
        Action_Login = 6,
        Action_Difficult = 7,
        Action_Back = 8,
        Action_Facebook = 9,
        Action_FBLike = 10,
        Action_Tweet = 11,
        Action_Ok = 12,
        Action_World90 = 13,
        Action_Pause  = 14,
        Action_Campaing = 15,
        Action_Menu = 16,
        Action_Extras = 17,
        Action_Restart = 18,
        Action_Yes = 19,
        Action_No = 20,
        Action_Music = 21,
        Action_Effect = 22,
        Action_Feedback = 23,
        Action_Shop = 24,
        Action_Hint = 25,
        Action_Show = 26,
        Action_Shop_Coins = 27,
        Action_EveryPlay_Show = 28,
        Action_Rate_App = 29,
        Action_FB_Challenge = 30,
        Action_FB_Brag = 31,
        Action_Tweet_Post = 32,
        Action_EveryPlay_Play = 33,
        Action_Mail = 34,
        Action_IMessage = 35,
        Action_Close = 36,
        Action_Gift = 37,
        Action_TellFriends = 38,
        Action_Share = 39,
        Action_Chillibites = 40,
        Action_Language = 41,
        Action_KidsMode = 42,
        Action_RemoveAds = 43,
        Action_Icloud = 44,
        Action_RestorePurchases = 45,
        Action_ResetGame = 46,
        Action_GameCenter = 47,
        Action_Rating = 48,
        Action_Buy_Double = 49,
        Action_Buy_KidsMode = 50,
        Action_Buy_AdsRemoving = 51,
        Action_Buy_Restore = 52,
        Action_Buy_10000 = 53,
        Action_Buy_999999 = 54
    };
    enum
    {
        Callback_State_FB_Complite = 1,
        Callback_State_TW_Complite = 2,
        Callback_State_TW_NoGranted = 4,
        Callback_State_FB_Error = 5,
        Callback_State_Error = 7,
        Callback_State_TW_Login_Error = 8
    };
    enum
    {
        Page_Options = 1,
        Page_Credits = 2,
        Page_KM_ON = 3,
        Page_Social = 4,
        Page_LeadBoard = 5,
        Page_MainMenu = 6,
        Page_LevelSelect = 7,
        Page_GameSceneCaramel = 8,
        Page_GameSceneChoco = 9,
        Page_GameSceneIce = 10,
        Page_GameSceneWaffles = 11,
        Page_GameScene90 = 12,
        Page_LevelComplete = 13,
        Page_Pause = 14,
        Page_ShopBomb = 15,
        Page_ShopCoins = 16,
        Page_LevelFail = 17,
        Page_Inventory = 18,
        Page_BombUpgrade = 19,
        Page_UlockWorld = 20,
        Page_ShareWindow = 21,
        Page_KM_OFF = 22,
        Page_EveryplayOn = 23,
        Page_Tutorial = 24,
        Page_LikeComplite = 25,
        
        Page_KM_MainMenu = 26,
        Page_KM_LevelSelect = 27,
        Page_KM_GameSceneCaramel = 28,
        Page_KM_GameSceneChoco = 29,
        Page_KM_GameSceneIce = 30,
        Page_KM_GameSceneWaffles = 31,
        Page_KM_GameScene90 = 32,
        Page_KM_LevelComplite = 33,
        Page_KM_Pause = 34,
        Page_KM_Shop = 35,

    };
    enum
    {
        Link_UpdateApp = 0,
        Link_EveryplayVideo = 1,
        Link_ChilibiteFollow = 2,
        Link_ChilibiteLike = 3,
        Link_SendGift = 4,
        Link_TellFriend = 5,
        Link_RateApp = 6,
        Link_GamecenterLeaderboard = 7,
        Link_GamepageFacebook = 8,
        Link_GamepageTwitter = 9,
        Link_ChilibiteMoreGames = 10,
        Link_ChartboostIntercitial = 11,
        Link_iAdBanner = 12,
        Link_adMobBanner =13,
        Link_StigolBanner = 14,
        Link_OfflineBanner = 15,
        Link_FacebookLogin = 16,
        Link_WatchReplay = 17,
        Link_PostScore = 18,
        Link_TweetScore = 19,
        Link_EmailScore = 20,
        Link_ShareImassage = 21,
        Link_ChallengeScore = 22,
        Link_StigolLike = 23,
        Link_StigolFollow = 24
    };
    enum
    {
        Left_Top = 0x0001,
        Right_Top = 0x0010,
        Left_Bottom = 0x0100,
        Right_Bottom = 0x1000
    };
    enum
    {
        zOrder_BackGround = 3,
        zOrder_Bonuses = 4,
        zOrder_Cube = 5,
        zOrder_Plane = 7,
        zOrderParticle  = 8,
        zOrder_Airplane = 10,
        zOrder_Panel = 11,
        zOrder_Bonus = 15,
        zOrder_Menu = 16,
        zOrder_LabelInfo = 20
    };
    
    static int ck_rand()
    {
        return rand();
    };
    static std::string loadImage(const char* _name)
    {
        char tmp[NAME_MAX];
        std::string tmp_str = _name;
        int nPosRight  = tmp_str.find('.');
        
        std::string _sufix = tmp_str.substr(nPosRight + 1,tmp_str.length());
        tmp_str = tmp_str.substr(0, nPosRight);
        
    //   CCLOG("String %s.%s",tmp_str.c_str(),_sufix.c_str());
        
        if(cocos2d::CCDirector::sharedDirector()->getContentScaleFactor() != 2.0)
        {
        if(cocos2d::CCDirector::sharedDirector()->getWinSize().width != 1024)
            sprintf(tmp, "%s.%s", tmp_str.c_str(),_sufix.c_str());
        else
            sprintf(tmp, "%s_p.%s", tmp_str.c_str(),_sufix.c_str());
        }
        else
        {
            if(cocos2d::CCDirector::sharedDirector()->getWinSize().width != 1024)
                sprintf(tmp, "%s@2x.%s", tmp_str.c_str(),_sufix.c_str());
            else
                sprintf(tmp, "%s_p@2x.%s", tmp_str.c_str(),_sufix.c_str());
        }
       // CCLOG("new file name %s",tmp);
        return tmp;
    };
    

   // int ShopBomb::buy_with_pack = 0;
    
    static std::string loadFile(const char* _name,bool _iphone5 = false)
    {
        char tmp[NAME_MAX];
        std::string tmp_str = _name;
        int nPosRight  = tmp_str.find('.');
        
        std::string _sufix = tmp_str.substr(nPosRight + 1,tmp_str.length());
        tmp_str = tmp_str.substr(0, nPosRight);
        
        if(cocos2d::CCDirector::sharedDirector()->getWinSize().width != 1024)
        {
            if(_iphone5 && cocos2d::CCDirector::sharedDirector()->getWinSize().width == 568)
                sprintf(tmp, "%s_568h.%s", tmp_str.c_str(),_sufix.c_str());
            else
                sprintf(tmp, "%s.%s", tmp_str.c_str(),_sufix.c_str());
        }
        else
            sprintf(tmp, "%s_p.%s", tmp_str.c_str(),_sufix.c_str());

        return tmp;
    };
    
    static cocos2d::ccColor3B strHextoColor(const std::string &_hex)
    {
        _hex.find("#", 0, 1);
        std::string pointStr = _hex.substr(1, _hex.length());
        unsigned int x;
        std::stringstream ss;
        ss << std::hex << pointStr;
        ss >> x;
       // CCLOG("strHextoColor %s %d",pointStr.c_str(),x);
        unsigned char r,g,b;
        r = (unsigned char)x;
        g = (unsigned char)(x >> 8);
        b = (unsigned char)(x >> 16);
      //  CCLOG("strHextoColor %s %d (%d,%d,%d)",pointStr.c_str(),x,r,g,b);
        return {b, g, r};
    };
    
    static cocos2d::CCPoint strToPoint(const std::string &_name)
    {
      //  char tmp[NAME_MAX];
        std::string tmp_str = _name;
        int nPosRight  = tmp_str.find(';');
        std::string tmp_x = tmp_str.substr(0, nPosRight);
        std::string tmp_y = tmp_str.substr(nPosRight + 1, tmp_str.length() - nPosRight);
         
      //  CCLOG("String Point %f_%f",atof(tmp_x.c_str()),atof(tmp_y.c_str()));

        return ccp(atof(tmp_x.c_str()),atof(tmp_y.c_str()));
    };
    
    static bool strToTwoStr(const std::string &_str, std::string &_left,std::string &_right,const char _sep)
    {
        std::string tmp_str = _str;
        int nPosRight  = tmp_str.find(_sep);
        _left = tmp_str.substr(0, nPosRight);
        _right = tmp_str.substr(nPosRight + 1, tmp_str.length() - nPosRight);
        return !(nPosRight == -1);
    };
    
    static float getPointLength(const cocos2d::CCPoint &_p1,const cocos2d::CCPoint &_p2)
    {
        
        return sqrtf( (_p1.x -_p2.x)*(_p1.x -_p2.x) + (_p1.y -_p2.y)*(_p1.y -_p2.y) );
    };
    static float getAngleBeetwenTwoPoint(const cocos2d::CCPoint &_p1,const cocos2d::CCPoint &_p2)
    {
        float n1 = _p2.y - _p1.y, n2 = _p2.x - _p1.x;
        float angle = atan2f(n2,n1) * 180 / PI;
        if(int(angle + 0.5) == 180 )
            angle = 0;
        return angle;
    };
    
    static float getAngleBeetwenTwoPoint2(const cocos2d::CCPoint &_p1,const cocos2d::CCPoint &_p2)
    {
        float n1 = _p2.y - _p1.y, n2 = _p2.x - _p1.x;
        float angle = atan2f(n2,n1) * 180 / PI;

        return angle;
    };
    
    static cocos2d::CCPoint RotatePoint(const cocos2d::CCPoint &_p0,float _angl,const cocos2d::CCPoint &_p)
    {
        cocos2d::CCPoint _p_new;
        
        _p_new.x = _p0.x + (_p.x-_p0.x)*cos(_angl) - (_p.y - _p0.y)*sin(_angl);
        _p_new.y = _p0.y + (_p.x-_p0.x)*sin(_angl) + (_p.y - _p0.y)*cos(_angl);

        return _p_new;
    }
    static cocos2d::CCPoint getRotatePoint(float _angl,const cocos2d::CCPoint &_p)
    {
        cocos2d::CCPoint _p_new;
        
        _p_new.x = (_p.x)*cos(_angl) - (_p.y)*sin(_angl);
        _p_new.y = (_p.x)*sin(_angl) + (_p.y)*cos(_angl);
        
        return _p_new;
    };
    
    static bool isPointInNode(const cocos2d::CCPoint &_p1,cocos2d::CCNode *_node)
    {
      //  CCLOG("distanse _p1.x %f size %f _node->getContentSize().width %f",_p1.x,(_node->getPositionX() + _node->getContentSize().width*(_node->getAnchorPoint().x - 0.5)));
        if(fabs(_p1.x -  (_node->getPositionX() - _node->getContentSize().width*(_node->getAnchorPoint().x - 0.5))) <  _node->getContentSize().width/2 && fabs(_p1.y -  _node->getPositionY()) <  _node->getContentSize().height/2)
        {
            return true;
        };
        return false;
    }
};

#endif
