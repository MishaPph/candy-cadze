//
//  CKGameSceneBase.h
//  CandyKadze
//
//  Created by PR1.Stigol on 05.07.13.
//
//

#ifndef __CandyKadze__CKGameSceneBase__
#define __CandyKadze__CKGameSceneBase__

#include <iostream>
#include "cocos2d.h"
#include <list>
class CKAudioObject
{
public:
    CKAudioObject(){};
    void initAudio(int _previour_scene);
    void pauseAudio(bool _anim);
    void resumeAudio();
    void stopMusic();
    void playMusic();
    void changeMusic();
};

class CKGameSceneBase
{
protected:
    int BOX_SIZE;
    
    std::string font_name;
    int count_accuvate_shots;
    std::list<cocos2d::CCLabelTTF*> label_pool;
    std::list<cocos2d::CCLabelTTF*>::iterator label_pool_it;
    
    void incLevelWorld();
    void createLabelScorePool(cocos2d::CCNode *_node);
    void showLabelScore(const cocos2d::CCPoint& _pos,int _score,cocos2d::SEL_CallFuncN _selector);
    void hideNode(cocos2d::CCNode *_sender);
public:
    CKGameSceneBase();
    virtual ~CKGameSceneBase();
};

#endif /* defined(__CandyKadze__CKGameSceneBase__) */
