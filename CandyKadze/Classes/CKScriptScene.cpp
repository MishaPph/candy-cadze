//
//  CKScriptScene.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 08.04.13.
//
//

#include "CKScriptScene.h"
#include "CKTutorialScriptLayer.h"
#include "CKHelper.h"
#include "CKLoaderGUI.h"
#include "CKAudioMgr.h"

#pragma mark  - TUTORIAL SCRIPT SCENE
const float TIME_VISIBLE_DESTROY_CUBE = 0.6;

//CCRenderTexture* m_rtt = NULL;
FrameRTT::FrameRTT()
{
    frame = -10;
    rtt = NULL;
};

void FrameRTT::render()
{
    rtt->clear(0,0,0, 0);
    rtt->begin();
    layer->getLayer()->setVisible(true);
    layer->getLayer()->visit();
    layer->getLayer()->setVisible(false);
    rtt->end();
};

CKTutorialScriptScene::CKTutorialScriptScene()
{
    win_size = CCDirector::sharedDirector()->getWinSize();
    CKHelper::Instance().lvl_speed = 0;
    //CKHelper::Instance().lvl_number = 0;
    
    m_node = CCNode::create();
    addChild(m_node,2);
    srand(time(0));
    //cur_sc = NULL;
    m_rtt = NULL;
    curent_frame = -1;
    curent_k = -1;
    start_scroll = false;
    setTouchEnabled(true);
    active_rtt_create = false;
    previour_lvl_bomb = 1;
};

CKTutorialScriptScene::~CKTutorialScriptScene()
{
    CC_SAFE_RELEASE_NULL(m_rtt);
    for (int i = 0; i < COUNT_RTT_SCRIPT; ++i) {
      //  delete sc_rt[i]->layer;
        delete sc_rt[i];
    };
    for (int i = 0; i < 0; i++) {
        delete frame[i].layer;
    };
   // CC_SAFE_RELEASE_NULL(m_dict);
};

void CKTutorialScriptScene::setRTT(CCRenderTexture* _rtt,CCRenderTexture **_rtt_frame)
{
    m_rtt = _rtt;
    //if(!active_rtt_create)
    {
        for(int i = 0; i < FRAME_POOL_SIZE;++i)
        {
            frame[i].rtt = _rtt_frame[i];
            //frame[i].rtt->setPosition(sc_rt[i]->_pos);
            //frame[i].render();
        };
        // active_rtt_create = true;
    }
};

void CKTutorialScriptScene::initPlaneFrame(const CCSize &_sizeframe,const CCPoint &_pos)
{
    COUNT_RTT_SCRIPT = 8;
    size_frame = _sizeframe;
    size_frame_x = size_frame.width;
    
    left_scroll_pos = size_frame_x/2;
    right_scroll_pos = left_scroll_pos - (COUNT_RTT_SCRIPT - 1)*size_frame.width;
    m_node->setPositionX(left_scroll_pos);
    
    for (int i = 0; i < COUNT_RTT_SCRIPT; ++i) {
        sc_rt[i] = new ScripRT;
        sc_rt[i]->index = i;
    };
    
    sc_rt[0]->type = 4;
    sc_rt[1]->type = 1;
    sc_rt[2]->type = 2;
    sc_rt[3]->type = 3;
    sc_rt[4]->type = 5;
    sc_rt[5]->type = 7;
    sc_rt[6]->type = 6;
    sc_rt[7]->type = 8;
};

void CKTutorialScriptScene::reload()
{
    for (int i = 0; i < FRAME_POOL_SIZE; ++i) {
        frame[i].layer->reload();
        frame[i].frame = -10;
        frame[i].rtt->setPosition(sc_rt[i]->_pos);
    };
    m_node->setPositionX(left_scroll_pos);
    curent_frame = -1;
    curent_k = -1;
};


void CKTutorialScriptScene::initBombFrame(const CCSize &_sizeframe,const CCPoint &_pos)
{
    COUNT_RTT_SCRIPT = 13;
    size_frame = _sizeframe;
    size_frame_x = size_frame.width;

    left_scroll_pos = size_frame_x/2;
    right_scroll_pos = left_scroll_pos - (COUNT_RTT_SCRIPT - 1)*size_frame.width;
    m_node->setPositionX(left_scroll_pos);
    
    for (int i = 0; i < COUNT_RTT_SCRIPT; ++i) {
        sc_rt[i] = new ScripRT;
        sc_rt[i]->index = i;
    }
    
    sc_rt[0]->type = 31;
    sc_rt[1]->type = 141;
    sc_rt[2]->type = 71;
    sc_rt[3]->type = 131;
    sc_rt[4]->type = 151;
    sc_rt[5]->type = 51;
    sc_rt[6]->type = 61;
    sc_rt[7]->type = 111;
    sc_rt[8]->type = 81;
    sc_rt[9]->type = 161;
    sc_rt[10]->type = 121;
    sc_rt[11]->type = 21;
    sc_rt[12]->type = 41;
};

//create script layer bomb or plane
// we never change position this layers
// is layer always hiden
// please not change position layer after init
void CKTutorialScriptScene::initLayer()
{
    for (int i = 0; i < FRAME_POOL_SIZE; ++i) {
        if(COUNT_RTT_SCRIPT == 13)
        {
            frame[i].layer = new CKBombScriptLayer(script[i]);
        }
        else
        {
            frame[i].layer = new CKPlaneScriptLayer(script[i]);
        };
        
       
        frame[i].layer->initWithType(m_dict,sc_rt[i]->type,size_frame.width/2);

        
        if(CKHelper::Instance().getIsIpad())
        {
            frame[i].layer->getLayer()->setPosition(ccp(-size_frame.width + size_frame.width/2 + CKHelper::Instance().BOX_SIZE*2.5 - 5,0));
        }
        else
        {
            float panel_left = (win_size.width == 568)?-44:0;
            frame[i].layer->getLayer()->setPosition(ccp(-size_frame.width + panel_left + size_frame.width/2 + CKHelper::Instance().BOX_SIZE*1.5 - 6,0));
        };

        frame[i].layer->getLayer()->setVisible(false);
        frame[i].layer->pause();
        addChild(frame[i].layer->getLayer());
        sc_rt[i]->_pos = ccp(i*size_frame_x,size_frame.height/2);
    };
};

void CKTutorialScriptScene::createAllFrame(CCDictionary* _dict)
{
    for (int i = 0; i < COUNT_RTT_SCRIPT; ++i) {
        sc_rt[i]->_pos = ccp(i*size_frame_x,size_frame.height/2);
        script[i] = new CKLayerScript;
        script[i]->initObject();
    };

    m_dict =  _dict;//CCDictionary::createWithContentsOfFile("tutorial_scripts.plist");

    
    initLayer();
    
    m_node->setVisible(false);
};

void CKTutorialScriptScene::setEnable(bool _en)
{
    setVisible(_en);
    this->setTouchEnabled(_en);
    if(_en)
    {
        addChild(m_rtt,5);
        for (int i = 0; i < 3; ++i) {
            m_node->addChild(frame[i].rtt,4);
        }
    }
    else //stop animation
    {
       // unscheduleUpdate();
        CKAudioMng::Instance().stopAllEffect();
        if(curent_k > -1 )
        {
            frame[curent_k].layer->pause();
        }
        if(m_rtt)
            m_rtt->removeFromParentAndCleanup(false);
        for(int i = 0; i < FRAME_POOL_SIZE;++i)
        {
            frame[i].frame = -10;
            if(frame[i].rtt)
                frame[i].rtt->removeFromParentAndCleanup(false);
        };
        curent_k = -1;
        curent_frame = -1;
    }
}

void CKTutorialScriptScene::draw()
{
    if(curent_k > -1 && !start_scroll)
    {
        frame[curent_k].render();
    };
    
    m_rtt->clear(0, 0, 0.0, 0.0);
    m_rtt->begin();
    m_node->setVisible(true);
    m_node->visit();
    m_node->setVisible(false);
    m_rtt->end();
};

void CKTutorialScriptScene::setPreviourLvl(const int _lvl)
{
    previour_lvl_bomb = _lvl;
};

void CKTutorialScriptScene::update(float dt)
{

};

int CKTutorialScriptScene::getFreeRTTFrame(int _index)
{
    for (int j = 0; j < FRAME_POOL_SIZE; j++) {
        if(frame[j].frame < (_index - 1) || frame[j].frame > (_index + 1))
        {
            return j;
        };
    };
//    CCLOG("Not getFreeRTTFrame");
    return -1; // don't find free rtt frame
};

bool CKTutorialScriptScene::isActiveRTTFrame(int _index)
{
    for (int j = 0; j < FRAME_POOL_SIZE; j++) {
        if(frame[j].frame == _index)
        {
            return true;
        };
    };
    return false;
};

/***************************************************************************
*   find free rtt frame and move to new frame pos and reload type script layer
*   not change frame if is current of the frame or is frame with new position stay
*   in left or right position as to current frame
***************************************************************************/
void CKTutorialScriptScene::setActiveRTTFrame(int _index)
{
    int k = 0;
    for (int i = 0; i < FRAME_POOL_SIZE; i++) {
        if(sc_rt[i]->type > 10)
        {
            int t = (frame[i].layer->getType()/10)*10 + previour_lvl_bomb;
            frame[i].layer->showType(NULL, &t);
        }
        frame[i].layer->reloadForType(frame[i].layer->getType());
        frame[i].layer->getLayer()->setVisible(false);
        frame[i].layer->pause();
    };
    k = getFreeRTTFrame(_index);

    int i = MAX(0,_index - 1);
    while (k != -1 && i < MIN(COUNT_RTT_SCRIPT, _index + 2)) {
        if(isActiveRTTFrame(i))
        {
            CCLOG("isActiveRTTFrame %d value %d",i,frame[i].frame);
           // frame[i].layer->reloadForType(sc_rt[i]->type);
            i++;
            continue;
        };
        CCLOG("setActiveRTTFrame %d %d %f %d",k,i,sc_rt[i]->_pos.x,sc_rt[i]->type);

            frame[k].frame = i;
        
        //save previours frame bomb select lvl
            if(sc_rt[i]->type > 10) //type bombs
            {
                frame[k].layer->reloadForType((sc_rt[i]->type/10)*10 + previour_lvl_bomb);
            }
            else //type plane bonus
            {
                frame[k].layer->reloadForType(sc_rt[i]->type);
            }

            frame[k].render();
        
            frame[k].rtt->setPosition(sc_rt[i]->_pos);
        
        k = getFreeRTTFrame(_index);
        i++;
    };
    
    if(frame[0].frame == frame[1].frame||frame[0].frame == frame[2].frame||frame[1].frame == frame[2].frame)
    {
        CCLOG("duble frame in one position %d %d %d",frame[0].frame,frame[1].frame,frame[2].frame);
    }
    //run curent (visible) frame
    for (int i = 0; i < FRAME_POOL_SIZE; i++) {
        if(frame[i].frame  == _index)
        {
            curent_k = i;
        }
    };
};

void CKTutorialScriptScene::setCurentFrameNumb(int _frame,bool _anim,int _type)
{
    CCLOG("setCurentFrameNumb %d",_frame);
    
    if(curent_k > -1)
    {
        frame[curent_k].layer->pause();
    };
    int prev_frame = curent_frame;
    curent_frame = _frame;
    
    //frame index in limited
    if(curent_frame < 0)
        curent_frame = 0;
    if (curent_frame >= COUNT_RTT_SCRIPT) {
        curent_frame = COUNT_RTT_SCRIPT - 1;
    };
    
    if(_anim)
    {
        //  CCLOG("anim");
        CCActionInterval* move = CCMoveTo::create(0.2, ccp(-curent_frame*size_frame_x + size_frame_x/2,m_node->getPositionY()));
        
        CCActionInterval* move_ease_out = CCEaseSineOut::create((CCActionInterval*)(move->copy()->autorelease()) );
        
        m_node->runAction(move_ease_out);
    }
    else
    {
        //  CCLOG("no anim");
        m_node->setPosition(ccp(-curent_frame*size_frame_x + size_frame_x/2,m_node->getPositionY()));
    }
    
    if(curent_k > -1) // pause previous frame
    {
        frame[curent_k].layer->pause();
        previour_lvl_bomb  = frame[curent_k].layer->getType()%10;
    }

     //three frames redraw if we have a new index of the frame
    if(curent_frame != prev_frame)
    {
        setActiveRTTFrame(curent_frame);
    }
    else
    {
        CCLOG("is Frame active don't need reload");
    }

    if(curent_k == -1)
    {
       setActiveRTTFrame(curent_frame);
    }
    else if(curent_k > -1)
    {
        if(_type != 0)
        {
            frame[curent_k].layer->showType(NULL, &_type);
        };
        
        for(int i = 0; i < FRAME_POOL_SIZE ;++i  )
        {
            frame[i].layer->pause();
        };
        
        frame[curent_k].layer->start();
        
        CCLOG("setCurentFrameNumb %d frame start %d type %d parent %d",curent_frame,curent_k,_type,frame[curent_k].rtt->getParent());
    }

};

void CKTutorialScriptScene::ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    if(!isVisible())
        return;
    CCTouch* touch =(CCTouch*)*touches->begin();
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    
    CCPoint p = m_rtt->convertToNodeSpace(location);
    if((fabs(p.x) < size_frame.width/2) && (fabs(p.y) < size_frame.height/2)) //point in frame
    {
        touch_in_scroll = true;
        if(curent_k > -1) //pause frame
        {
            frame[curent_k].layer->setTouchBegan(location); //send touch to script layer, (buy button and slot)
        }
        last_posotion = location;
        touch_began_position = location;
    };
};

void CKTutorialScriptScene::ccTouchesMoved(CCSet *touches, CCEvent *pEvent)
{
    CCTouch* touch =(CCTouch*)*touches->begin();
    CCPoint location = touch->getLocationInView();
    
    location = CCDirector::sharedDirector()->convertToGL(location);
    
    if(touch_in_scroll)
    {
        
        if(fabs(touch_began_position.x - location.x) > win_size.width*0.1)
            if(curent_k > -1)
            {
                frame[curent_k].layer->pause();
                start_scroll = true;
            }
        float moved =  location.x - last_posotion.x;
        last_posotion = location;
        //
        
        if(m_node->getPositionX() + moved > left_scroll_pos) // limited move node left
        {
            float move2 = m_node->getPositionX()- left_scroll_pos;
            moved *= (1.0 - move2/(size_frame_x*0.5));
        }
        if(m_node->getPositionX() + moved < right_scroll_pos)// limited move node right
        {
            float move2 = m_node->getPositionX() - right_scroll_pos; 
            moved *= (1.0 + move2/(size_frame_x*0.5));
        }
        
        m_node->setPositionX(m_node->getPositionX() + moved);
    }
};


void CKTutorialScriptScene::ccTouchesEnded(CCSet *touches, CCEvent *pEvent)
{
    
    if(touch_in_scroll) // was began touched on scroll
    {
        start_scroll = false;
        CCTouch* touch =(CCTouch*)*touches->begin();
        CCPoint location = touch->getLocationInView();
        
        location = CCDirector::sharedDirector()->convertToGL(location);
    
        int prev_frame = curent_k;
        //move base node  to positions of the curent frame
        if((touch_began_position.x - location.x) > win_size.width*0.1) // move to next frame
        {
            setCurentFrameNumb(curent_frame + 1);
        }
        else if((touch_began_position.x - location.x) < -win_size.width*0.1) //move to previous frame
        {
            setCurentFrameNumb(curent_frame - 1);
        }
        else // don't change frame
        {
             m_node->runAction(CCMoveTo::create(0.2, ccp(-curent_frame*size_frame_x + size_frame_x/2,m_node->getPositionY())));
        };
        
        if(curent_k > -1)// start frame
        {
            if(prev_frame != curent_k)
            {
                CKAudioMng::Instance().stopAllEffect();
                CKAudioMng::Instance().playEffect("run_scroll");
            };
            frame[curent_k].layer->setTouchEnd(location);
            for(int i = 0; i < FRAME_POOL_SIZE ;++i  )
            {
                frame[i].layer->pause();
            };
            frame[curent_k].layer->start();
        };
    };
    touch_in_scroll = false;
};