//
//  CKLoadingScene.h
//  CandyKadze
//
//  Created by PR1.Stigol on 10.04.13.
//
//

#ifndef __CandyKadze__CKLoadingScene__
#define __CandyKadze__CKLoadingScene__


#include <iostream>
#include "cocos2d.h"
#include <list.h>
#include <vector.h>

class CKGUI;
class CKObject;
using namespace cocos2d;

class CKLoadingScene: public cocos2d::CCLayer
{
    CKGUI *m_gui;
    void initGui();
    void nextScene();
    int previous_scene_id;
    bool enable_complite;
    int curen_load;
    bool is_scene_loading;
    void initResource();
    static int number_scene;
    static void* thread_func(void *vptr_args);
    void unloadPreviousScene();
public:
    void functAsunc(CKLoadingScene * _sender);
    void compliteLoadScene(CCScene * _scene);
    int getCurrentSceneID() const;
    int getPreviourSceneID() const;
    CKLoadingScene();
    virtual ~CKLoadingScene();
    void play(int _num_scne);
    void update(float dt);
    static CCScene* scene();
};


#endif /* defined(__CandyKadze__CKLoadingScene__) */
