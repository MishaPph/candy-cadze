//
//  CandyKadzeAppDelegate.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 18.09.12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#include "AppDelegate.h"
#include "cocos2d.h"

#include "CKHelper.h"
#include "CKGameScene.h"
#include "CKGameScene90.h"
#include "CKAudioMgr.h"
#include "CKTutorialScriptLayer.h"
#include "CKLoadingScene.h"
#include "CKAdBanner.h"
#include "CKLogoScene.h"
#include "TestScene.h"
#include "CKOptionLayer.h"
#include "CKLeaderboard.h"
//#include "DeviceInfo.h"
#include "StatisticLog.h"
#include "CKTwitterHelper.h"
#include "ICloudHelper.h"
#include "CKFacebookHelper.h"

USING_NS_CC;

AppDelegate::AppDelegate()
{
}

AppDelegate::~AppDelegate()
{
}

bool AppDelegate::applicationDidFinishLaunching()
{
    // initialize director
    CCDirector *pDirector = CCDirector::sharedDirector();
    pDirector->setOpenGLView(CCEGLView::sharedOpenGLView());
  //  [CCTexture2D setDefaultAlphaPixelFormat:kCCTexture2DPixelFormat_RGBA4444];
    
    // enable High Resource Mode(2x, such as iphone4) and maintains low resource on other devices.
    ObjCCalls::recognizingModel();
    
    ObjCCalls::isActiveConnection();
    
    pDirector->enableRetinaDisplay(!(ObjCCalls::isLowCPU()&&ObjCCalls::isLittleMemory()));
    //pDirector->enableRetinaDisplay(false);
    CCDirector::sharedDirector()->setDepthTest(false);
    // turn on display FPS
    pDirector->setDisplayStats(true);
    pDirector->setDepthTest(false);
    pDirector->setAlphaBlending(true);

     if(int(pDirector->getWinSize().width) == 480)
        pDirector->setAnimationInterval(1.0 / 60);
    else
        pDirector->setAnimationInterval(1.0 / 60);
    
    CKHelper::Instance().setWinSize(pDirector->getWinSize());
    
    CKFileOptions::Instance().load();

   // return false;
    CCScene *pScene =  CKLogoScene::scene();
    pDirector->runWithScene(pScene);
    
    CCDirector::sharedDirector()->drawScene();
    
    CKFileOptions::Instance().loadLanguage();
    // create a scene. it's an autorelease object

    
    
    //CCScene *pScene =  TestScene::scene();
    //pDirector->runWithScene(pScene);
    
  //  CCScene *pScene = CKOptionLayer::scene();
    
    
    if(CKFileOptions::Instance().isDebugMode())
        CKFileInfo::Instance().setFileName(CK::file_data_debug_name);
    else
        CKFileInfo::Instance().setFileName(CK::file_data_name);
    
    CKBonusMgr::Instance().load();
    CKSystemInfo::Instance().init();
    

    timeval t;
    gettimeofday(&t,NULL);
    
    int min = t.tv_sec/60;
    int hour = min/60;
    t.tv_sec -= int(t.tv_sec/60)*60;
    min -= int(min/60)*60;
    hour -= int(hour/24)*24;
    
    CCLOG("time sec: %d::%d::%d %f %s",hour,min,t.tv_sec,pDirector->getContentScaleFactor(),ObjCCalls::getIPAddress().c_str());

    
    ObjCCalls::load();
    ObjCCalls::initStatic();
    
    
    std::string path = CCFileUtils::sharedFileUtils()->getDocumentPath();
    path.append(CK::file_data_name);
    std::ifstream ifile(path.c_str());
    
    
    if(!ifile.is_open())
    {
        
        if(CKFileOptions::Instance().isEnableIcloud())
        {
            ICloudHelper::Instance().loadWithIcloud();
        };
    }
    ifile.close();
    
    return true;
};

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground()
{
    CK::StatisticLog::Instance().enterBackground();
    CCLOG("AppDelegate::applicationDidEnterBackground");
    cocos2d::CCNode* pScene = cocos2d::CCDirector::sharedDirector()->getRunningScene()->getChildByTag(CK::LAYERID_GAMESCENE);
    
    if(pScene)
    {
        if(CKHelper::Instance().getWorldNumber() == 0)
            dynamic_cast<CKGameScene90 *>(pScene)->setPause();
        else
            dynamic_cast<CKGameSceneNew *>(pScene)->setPause();
    };
    CCDirector::sharedDirector()->stopAnimation();
    
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground()
{
    CCLOG("AppDelegate::applicationWillEnterForeground");
    CCDirector::sharedDirector()->startAnimation();
    
    CK::StatisticLog::Instance().enterForeground();
    //CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
    //CocosDenshion::SimpleAudioEngine::sharedEngine()->resumeAllEffects();
    
    cocos2d::CCNode* pScene = (cocos2d::CCDirector::sharedDirector()->getRunningScene() != NULL)?cocos2d::CCDirector::sharedDirector()->getRunningScene()->getChildByTag(CK::LAYERID_GAMESCENE):NULL;
    if(!pScene)
    {
        if(CKFileOptions::Instance().isPlayEffect())
        {
            CKAudioMng::Instance().resumeAllEffect();
        }
    };
    
    if(CKFileOptions::Instance().isPlayMusic())
    {
        CKAudioMng::Instance().resumeBackground();
        CKAudioMng::Instance().setVolumeBgd(0.0);
        if(!CKAudioMng::Instance().isPlayBackground())
            CKAudioMng::Instance().playBackground("bg");

        CKAudioMng::Instance().fadeBgToVolume((pScene)?CK::SOUND_FADE_VALUE:1.0);
    }
    
    
    CCDirector::sharedDirector()->getTouchDispatcher()->setDispatchEvents(true);

    // if you use SimpleAudioEngine, it must resume here
    
    if(CKHelper::Instance().FB_need_check_likes_main)
    {
        CCLOG("FB_need_check_likes_main");
        CCNode* pLayer = CCDirector::sharedDirector()->getRunningScene()->getChildByTag(CK::LAYERID_MAIN);
        if(pLayer)
        {
            //CKHelper::Instance().FB_need_check_likes_main = false;
            CKFacebookHelper::Instance().FB_GetLike(pLayer, callfunc_selector(CKMainMenuScene::setLikes),FB_LIKE_ANY_ID);
        }
        CCNode* pLayerShop = CCDirector::sharedDirector()->getRunningScene()->getChildByTag(CK::LAYERID_SHOP);
        if(pLayerShop)
        {
            //CKHelper::Instance().FB_need_check_likes_main = false;
            CKFacebookHelper::Instance().FB_GetLike(pLayerShop, callfunc_selector(CKShop::setLikes),FB_LIKE_OUR_ID);
        }
        
        CCNode* pLayerGame = CCDirector::sharedDirector()->getRunningScene()->getChildByTag(CK::LAYERID_GAMESCENE);
        if(pLayerGame)
        {
            //CKHelper::Instance().FB_need_check_likes_main = false;
            CKFacebookHelper::Instance().FB_GetLike(pLayerGame, callfunc_selector(CKGameSceneNew::setLikes),FB_LIKE_OUR_ID);
        }
    };
    if(CKHelper::Instance().TW_need_ckeck_follow)
    {
        CCNode* pLayer = CCDirector::sharedDirector()->getRunningScene()->getChildByTag(CK::LAYERID_MAIN);
        if(pLayer)
        {
            //CKHelper::Instance().TW_need_ckeck_follow = false;
            CKTwitterHelper::Instance().TweetIsFollowing(pLayer, callfuncND_selector(CKMainMenuScene::tweetCallBack),TWEET_FOLLOW_CHBT);
        }
        CCNode* pShop = CCDirector::sharedDirector()->getRunningScene()->getChildByTag(CK::LAYERID_SHOP);
        if(pShop)
        {
            //CKHelper::Instance().TW_need_ckeck_follow = false;
            CKTwitterHelper::Instance().TweetIsFollowing(pShop, callfuncND_selector(CKShop::tweetCallBack),TWEET_FOLLOW);
        }
    };
    
    if(CKHelper::Instance().leaderboard_login)
    {
        CCNode* pLayer = CCDirector::sharedDirector()->getRunningScene()->getChildByTag(CK::LAYERID_MAIN);
        if(pLayer)
        {
            static_cast<CKMainMenuScene*>(pLayer)->refreshLeaderboard();
        };
        CKHelper::Instance().leaderboard_login = false;
    };
}
