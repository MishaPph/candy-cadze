//
//  CKLogoScene.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 02.05.13.
//
//

#include "CKLogoScene.h"
#include "CKHelper.h"
#include "CKLoadingScene.h"
#include "CKMainMenuScene.h"


using namespace cocos2d;

#pragma mark - LOGO_SCENE

using namespace CK;

CCScene* CKLogoScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    // CKHelper::Instance().m_loading->setEnableActive(true);
    // add layer as a child to scene
    CCLayerColor* layer = new CKLogoScene();
    //CKHelper::Instance().m_loading->activeted(layer,scene);
    // CCLOG("scene:: scene %d layer %d ",scene,layer);
    CCSize s = CCDirector::sharedDirector()->getWinSize();

    if(layer->initWithColor(ccc4(255,255,255,255), s.width, s.height))
    {
        scene->addChild(layer);
        //CKHelper::Instance().m_loading->activeted(layer);
        layer->setTouchEnabled(false);
       // layer->setTag(CK::LAYERID_MAIN);
        layer->release();
    };
  //  CKHelper::Instance().compliteLoadScene(scene);
    return scene;
};

CKLogoScene::~CKLogoScene()
{
    CCLOG("destroy ~CKLogoScene");
};

#pragma mark - INIT

void CKLogoScene::playAnimation()
{
    CCAnimation* animat1 = CCAnimation::create();
    CCAnimation* animat2 = CCAnimation::create();
    for (int i = 1; i < 11; i++) {
        char str[255];
        sprintf(str, "Chilibite_Games_%d.png",i);
        CCTexture2D *texture =  CCTextureCache::sharedTextureCache()->addImage(CK::loadImage(str).c_str());
        if(i < 4)
        {
            animat1->addSpriteFrameWithTexture(texture, CCRect(0, 0, texture->getContentSize().width, texture->getContentSize().height));
          //  animat1->addSpriteFrameWithFileName(CK::loadImage(str).c_str());
        }
        else
        {
            animat2->addSpriteFrameWithTexture(texture, CCRect(0, 0, texture->getContentSize().width, texture->getContentSize().height));
          //  animat2->addSpriteFrameWithFileName(CK::loadImage(str).c_str());
        }
    };
    
    animat1->setDelayPerUnit(0.08);
    animat2->setDelayPerUnit(0.1);
    CCFiniteTimeAction* action1 = CCAnimate::create(animat1);
    //animat1->release();
    CCFiniteTimeAction* action2 = CCAnimate::create(animat2);
    //animat2->release();
    
    m_sprite->runAction(CCSequence::create(action1,CCDelayTime::create(1.2),action2,CCDelayTime::create(0.5),CCFadeOut::create(0.3), NULL) );
    
    if(CKFileOptions::Instance().isPlayEffect())
        CocosDenshion::SimpleAudioEngine::sharedEngine()->playEffect("Apple_Bite-Simon_Craggs-1683647397.mp3");

    stigol_logo = CCSprite::create(CK::loadImage("StigolLogo.png").c_str());
    addChild(stigol_logo);
    stigol_logo->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width/2,CCDirector::sharedDirector()->getWinSize().height/2));
    stigol_logo->setOpacity(0);
    schedule(schedule_selector(CKLogoScene::logoStigol), 3.0);
};

void CKLogoScene::logoStigol()
{
    stigol_logo->runAction(CCSequence::create(CCFadeIn::create(0.4), NULL));
    schedule(schedule_selector(CKLogoScene::nextScene), 0.5);
    unschedule(schedule_selector(CKLogoScene::logoStigol));
    
    m_sprite->stopAllActions();
    m_sprite->removeFromParentAndCleanup(true);
    
    for (int i = 1; i < 11; i++) {
        char str[255];
        sprintf(str, "Chilibite_Games_%d.png",i);
        CCTextureCache::sharedTextureCache()->removeTextureForKey(CK::loadImage(str).c_str());
    };
   
};

CKLogoScene::CKLogoScene()
{
    time = 0;
    curen_load = 0;

    m_sprite = CCSprite::create(CK::loadImage("Chilibite_Games_1.png").c_str());
    m_sprite->setPosition(ccp(CCDirector::sharedDirector()->getWinSize().width/2,CCDirector::sharedDirector()->getWinSize().height/2));
    addChild(m_sprite);
    
    //CCDirector::sharedDirector()->drawScene();
    
    curen_load++;
    CCTextureCache::sharedTextureCache()->addImageAsync(CK::loadImage("StigolLogo.png").c_str(), this,callfuncO_selector(CKLogoScene::functAsunc));
    //CCDirector::sharedDirector()->drawScene();
    CCLOG("CKLogoScene");
    for (int i = 1; i < 11; i++) {
        char str[255];
        sprintf(str, "Chilibite_Games_%d.png",i);
       // CCTexture2D *texture =  CCTextureCache::sharedTextureCache()->addImage(CK::loadImage(str).c_str());
        curen_load++;
        CCTextureCache::sharedTextureCache()->addImageAsync(CK::loadImage(str).c_str(), this,callfuncO_selector(CKLogoScene::functAsunc));
    };

    
    //runAction(CCSequence::create(CCCallFunc::create(this, callfunc_selector(CKLogoScene::playAnimation) ),NULL));
};

void CKLogoScene::functAsunc(CKLogoScene * _sender)
{
    curen_load--;
    if(curen_load == 0)
    {
        playAnimation();
    };
};

void CKLogoScene::update(const float &_dt)
{

};

void CKLogoScene::nextScene(CCNode *_sender)
{
   unschedule(schedule_selector(CKLogoScene::nextScene));

   // CCTextureCache::sharedTextureCache()->removeTextureForKey(CK::loadImage("StigolLogo.png").c_str());
    CCTextureCache::sharedTextureCache()->removeUnusedTextures();
    CCTextureCache::sharedTextureCache()->addImage(CK::loadImage("a_TutorialPause.png").c_str());
    CCTexture2D *texture = CCTextureCache::sharedTextureCache()->addImage(CK::loadImage("a_Boom.png").c_str());
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(CK::loadImage("a_Boom.plist").c_str(),texture);
    
    CCTextureCache::sharedTextureCache()->dumpCachedTextureInfo();
    

    CKHelper::Instance().loading_scene = CKLoadingScene::scene();
    CKHelper::Instance().loading_scene->retain();
    
    CCDirector::sharedDirector()->replaceScene(CKMainMenuScene::scene());
 //   CKHelper::Instance().playScene(CK::SCENE_MAIN);

};
