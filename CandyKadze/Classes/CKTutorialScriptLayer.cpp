//
//  CKTutorialScriptLayer.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 03.04.13.
//
//

#include "CKTutorialScriptLayer.h"
#include "CKCubeArray.h"
#include "CKAirplane.h"
#include "CKLoaderGUI.h"
#include "CKLanguage.h"
//CCDictionary* CKScriptLayerBase::m_dict = NULL;
#pragma mark - LAYER SCRIPT
CKLayerScript::~CKLayerScript()
{
    CCLOG("~CKScriptLayerBase destroy");
    CC_SAFE_DELETE(m_gui);
    CC_SAFE_DELETE(m_airplane);
    CC_SAFE_DELETE(m_bombs);
    CC_SAFE_DELETE(m_cube_array);
    CC_SAFE_DELETE(m_bomb_ghost);
}
CKLayerScript::CKLayerScript()
{
    m_gui = NULL;
    m_airplane = NULL;
    m_bombs = NULL;
    m_cube_array = NULL;
    m_bomb_ghost = NULL;
    
    sprite_hand = NULL;
    sprite_accelerate = NULL;
};

void CKLayerScript::initObject()
{
    m_gui = new CKGUI();
    
    m_gui->create(CCNode::create());
    m_gui->setTouchEnabled(true);
    m_gui->setTarget(this);
    m_gui->addFuncToMap("showType", callfuncND_selector(CKLayerScript::showType));
    m_gui->addFuncToMap("menuCall", callfuncND_selector(CKLayerScript::menuCall));
    addChild(m_gui->get(),20);
    
    m_airplane = new CKAirplane;
    m_cube_array = new CKCubeArray;
    m_bombs = new CKBombs;
    m_bomb_ghost = new CKBombs;
    
    m_bombs->setBombName("bomb.png");
    m_bombs->setLayer(this);
    m_bombs->createDestroyEffect(NULL);
    m_bombs->init();
    
    m_bomb_ghost->setBombName("bomb.png");
    m_bomb_ghost->setLayer(this);
    m_bomb_ghost->createDestroyEffect(NULL);
    m_bomb_ghost->init();
    
    m_cube_array->setPanelSize(0);
    if(CCDirector::sharedDirector()->getWinSize().width == 568)
    {
        m_cube_array->setPanelSize(44);
    }
    m_cube_array->setLayer(this);
    m_cube_array->createBatch(CK::loadImage("a_TutorialPause.png").c_str());
    m_cube_array->parsingLevel("tutorial_cube.plist");
    
};

void CKLayerScript::initBomb(int ground_height,int BOX_SIZE)
{
    m_bombs->setSpeed(BOX_SIZE*5);
    m_bombs->setHeight(ground_height);
    m_bombs->setCubeArray(m_cube_array);
    m_bombs->setAirplane(m_airplane);
    
    m_bomb_ghost->setCubeArray(m_cube_array);
    m_bomb_ghost->setAirplane(m_airplane);
    m_bomb_ghost->setHeight(ground_height);
    m_bomb_ghost->setSpeed(BOX_SIZE*5);
};

void CKLayerScript::initCube(int ground_height,CCArray *_array)
{
    m_cube_array->setGroundHeight(ground_height);
    m_cube_array->createCubeWithCCArray(_array);
};

float CKLayerScript::initPlane(int ground_height)
{
    m_airplane->setEnableParticle(false);//
    m_airplane->setLayer(this);
    m_airplane->create("");
    m_airplane->setEmulationFly(true);
    
    m_airplane->setSlowFall(0,0,0);
    
    m_airplane->setGroundHeight(ground_height);
    m_airplane->setBombs(m_bombs);
    m_airplane->setCubeArray(m_cube_array);
    m_airplane->setGhostBomb(m_bomb_ghost);
    return m_airplane->getSpeed();
};

void CKLayerScript::update(float _dt)
{
    m_base->update(_dt);
};

void CKLayerScript::menuCall(CCNode* _sender,void *_data)
{
    m_base->menuCall(_sender, _data);
};

void CKLayerScript::showType(CCNode *_node,void *_d)
{
    m_base->showType(_node, _d);
};

void CKLayerScript::startActionAcc()
{
    schedule(schedule_selector(CKLayerScript::schedureAction),0.1);
    
}

void CKLayerScript::stopActionAcc()
{
    unschedule(schedule_selector(CKLayerScript::schedureAction));
}

void CKLayerScript::schedureAction()
{
    CCAcceleration  cc_accel;
    cc_accel.x = -0.15;
    cc_accel.timestamp = 0.1;
    m_bombs->didAccelerate(&cc_accel);
}

#pragma mark - BASE SCRIPT
CKScriptLayerBase::~CKScriptLayerBase()
{

   // CC_SAFE_DELETE(m_dict);
};

CKScriptLayerBase::CKScriptLayerBase()
{
    is_mono = false;
    is_load = false;
    win_size = CCDirector::sharedDirector()->getWinSize();
    if (win_size.width == 568) {
        panel_left = 44;
    };
};

void CKScriptLayerBase::copy(const CKScriptLayerBase &_base)
{

};

CKGUI* CKScriptLayerBase::getGui()
{
    return m_script->m_gui;
};

void CKScriptLayerBase::setMono(bool _enable)
{
    is_mono = _enable;
};

void CKScriptLayerBase::setTouchBegan(const CCPoint &_pos)
{
    CCLOG("CKTutorialScriptLayer::setTouchBegan %f %f %f",_pos.x,_pos.y,m_script->getPositionY());
    float offset_x = (win_size.width == 1024)?0:(win_size.width == 568)?0:0;
    
    m_script->curent_node = m_script->m_gui->getTouchObject(ccp(_pos.x + offset_x,_pos.y - ((win_size.width == 1024)?80:30)));
    if(m_script->curent_node && m_script->curent_node->getType() == CK::GuiButton)
    {
        m_script->curent_node->setStateImage(1);
    }
    else
    {
        m_script->curent_node = NULL;
    }
};

void CKScriptLayerBase::setTouchEnd(const CCPoint &_pos)
{
 //   CCLOG("CKTutorialScriptLayer::setTouchEnd %f %f %f",_pos.x,_pos.y,getPositionY());
    if(m_script->curent_node)
    {
        if(int(m_script->curent_node->getTag()/10) == 2) // is slot button
        {
            CKAudioMng::Instance().playEffect("change_bombs_slots");
            showType(NULL,m_script->curent_node->getValue());
        }
        else
        {
            menuCall(NULL,m_script->curent_node->getValue());
        };
    };
    m_script->curent_node = NULL;
};

void CKScriptLayerBase::newLoop()
{
    m_script->m_cube_array->reinitCubeWithCCArray(m_array);
    m_script->scheduleUpdate();
    can_run_bonus = true;
    need_reload = false;
};

void CKScriptLayerBase::pause()
{
    m_script->unscheduleUpdate();
    m_script->stopAllActions();
    m_script->setVisible(false);
    CKAudioMng::Instance().pauseAllEffect();
    CKAudioMng::Instance().stopEffect("plane_fly");
    
};

void CKScriptLayerBase::start()
{
    if(is_end)
        return;
    m_script->m_base = this;
    m_script->unscheduleUpdate();
    m_script->scheduleUpdate();
    m_script->setTouchEnabled(true);
    m_script->setVisible(true);
    
    //CKAudioMng::Instance().pauseAllEffect();
    CKAudioMng::Instance().resumeAllEffect();
    CKAudioMng::Instance().playEffect("plane_fly");
};

void CKScriptLayerBase::initGui()
{
    m_script->m_gui->load(CK::loadFile("c_TutorialScript.plist").c_str());
    ground_height = m_script->m_gui->getChildByTag(1)->getNode()->getContentSize().height*m_script->m_gui->getChildByTag(1)->getNode()->getScaleY();
    m_script->m_gui->getChildByTag(21)->setStateImage(1);
    
    if(is_mono)
    {
        //button buy
        m_script->m_gui->getChildByTag(BUTTON_BUY_ID)->setVisible(false);
        //label buy
       m_script->m_gui->getChildByTag(LABEL_BUY_ID)->setVisible(false);
    }

     m_script->m_gui->get()->setPosition(ccp(panel_left,0));
};

void CKScriptLayerBase::reload()
{
    is_end = false;
};


void CKScriptLayerBase::initWithType(CCDictionary *_dict,const int &_type,const float &_frame_size_x)
{

    is_load = true;
    can_run_bonus = true;
    is_end = false;
    type = _type;
    m_script->curent_node = NULL;
    
    frame_size_x = _frame_size_x;
    have_action = false;
    BOX_SIZE = CKHelper::Instance().BOX_SIZE;
    
    m_dict = _dict;
    
    need_reload = false;
    
    frame_size_x = _frame_size_x;
    create_new_bomb = 0.0f;
    
    initGui();
};

void CKScriptLayerBase::menuCall(CCNode* _sender,void *_data)
{
    CKAudioMng::Instance().playEffect("button_pressed");
    is_end = true;
    m_script->unscheduleUpdate();
    m_script->setVisible(false);
    m_script->setTouchEnabled(false);
   
    CKHelper::Instance().show_shop_bomb = type;
    
    CCNode * m_layer = CKHelper::Instance().getTutorial()->getParent();

    CKShop *m_shop = static_cast<CKShop *>(m_layer->getChildByTag(CK::LAYERID_SHOP));
    if(m_shop)
    {
        CKHelper::Instance().getTutorial()->deactived();
         CCLOG("CK::LAYERID_SHOP %d",m_shop->getTag());
        m_shop->sort(255);
        m_shop->showObjectType(type);
    }
    else
    {
        CKHelper::Instance().getTutorial()->deactived();
        CCDirector::sharedDirector()->replaceScene(CKHelper::Instance().loading_scene);
        CKHelper::Instance().playScene(CK::SCENE_SHOP);
    }
};

void CKScriptLayerBase::update(float dt)
{
    CCLOG("CKScriptLayerBase::update");
};


#pragma mark - BOMB SCRIPT LAYER
CKBombScriptLayer::CKBombScriptLayer(CKLayerScript * _layer)
{
    is_mono = false;
    m_script = _layer;
    panel_left = 0;
    if(CCDirector::sharedDirector()->getWinSize().width == 568)
    {
        panel_left = 44;
    }
};

CKBombScriptLayer::~CKBombScriptLayer()
{
    
};

void CKBombScriptLayer::showType(cocos2d::CCNode *_sender, void *_data)
{
    can_run_bonus = true;
    active_action_hand = false;
    type =  *static_cast<int *>(_data);
    CCLOG("CKTutorialScriptLayer::showType %d",type);
    
    initWithDict(m_dict);
    m_script->m_cube_array->reinitCubeWithCCArray(m_array);
    m_script->m_airplane->setCurentHeight(plane_height);
    
    if(m_script->m_bombs)
    {
        m_script->m_bombs->deleteAllActiveBombs();
        if(type == 141 || type == 142 || type == 143)
        {
            m_script->m_bombs->setSpeed(BOX_SIZE*10);
        }
        else
        {
            m_script->m_bombs->setSpeed(BOX_SIZE*5);
        }
        
    }
    m_script->m_bombs->setAirplane(m_script->m_airplane);
    m_script->m_bombs->setLayer(m_script);
    
    m_script->m_airplane->setCurentPositionX(win_size.width*0.5 - frame_size_x - m_script->m_airplane->getSprite()->getContentSize().width/2);
    updateSlotState();
    if(type/10 == 16)
    {
        m_script->m_airplane->setEnableMachineGun(true);
    }
    else
    {
        m_script->m_airplane->setEnableMachineGun(false);
    }
    //button buy
    m_script->m_gui->getChildByTag(BUTTON_BUY_ID)->setVisible(CKHelper::Instance().menu_number != CK::SCENE_GAME && !CKFileOptions::Instance().isKidsModeEnabled());
    //label buy
    m_script->m_gui->getChildByTag(LABEL_BUY_ID)->setVisible(CKHelper::Instance().menu_number != CK::SCENE_GAME && !CKFileOptions::Instance().isKidsModeEnabled());
    
    m_script->m_gui->getChildByTag(43)->setVisible(false);//slot plane
    
//    if(m_script->m_gui)
//    {
//        CCLabelBMFont* label = static_cast<CCLabelBMFont *>(m_script->m_gui->getChildByTag(LABEL_BUY_ID)->getNode());
//        std::string st1 = label->getFntFile();
//        int pos = st1.find("_");
//        int pos_s = st1.find(".");
//        std::string st2 = st1.substr(pos+3,pos_s - pos - 3);
//        char str[NAME_MAX];
//        sprintf(str,"%s_%s%s.fnt",st1.substr(0,pos).c_str(),CKLanguage::Instance().getCurrent(),st2.c_str());
//        CCLOG("setFntFile:: %s",str);
//        if(strcmp(str, label->getFntFile()) != 0)
//        {
//            label->setString("");
//            label->setFntFile(str);
//            label->setString(CKLanguage::Instance().getLocal("Buy"), str);
//        }
//    };
    
};

void CKBombScriptLayer::updateSlotState()
{
    for (int i = 1 ; i < 4; ++i) {
        m_script->m_gui->getChildByTag(20 + i)->setStateImage(0);
        
        if(type%10 == i)
            m_script->m_gui->getChildByTag(20 + i)->setStateImage(1);
    };
    m_script->m_gui->getChildByTag(43)->setVisible(false);//slot plnae
};



void CKBombScriptLayer::initWithDict(cocos2d::CCDictionary *_dict)
{
    CCDictionary * m_dict_mombs = static_cast<CCDictionary*>(_dict->objectForKey("bombs"));
    char str[NAME_MAX];
    sprintf(str, "bomb_%d",type);
    CCDictionary *dict_bomb =  static_cast<CCDictionary*>(m_dict_mombs->objectForKey(str));
    action_start = static_cast<CCString *>(dict_bomb->objectForKey("bomb_start"))->intValue()*BOX_SIZE/2;


    action_start += panel_left;

    plane_height = static_cast<CCString *>(dict_bomb->objectForKey("plane height"))->intValue()*BOX_SIZE + ground_height;
    m_array = static_cast<CCArray *>(dict_bomb->objectForKey("block_places"));
    action_type = 0;
    if(dict_bomb->objectForKey("action"))
    {
        have_action = true;
        action_type = static_cast<CCString *>(dict_bomb->objectForKey("action"))->intValue();
    };
    if(action_type == 21 || action_type == 2)
    {
        if(!m_script->sprite_hand)
        {
            m_script->sprite_hand = CCSprite::createWithSpriteFrameName("hand.png");
            m_script->addChild(m_script->sprite_hand,60);
        }
        else
        {
            m_script->sprite_hand->setVisible(true);
        }
        m_script->sprite_hand->setPosition(ccp(BOX_SIZE/2*21 + BOX_SIZE/4 + panel_left,ground_height + BOX_SIZE/2));
    };
    if(action_type == 1 && !m_script->sprite_accelerate)
    {
        m_script->sprite_accelerate = CCSprite::createWithSpriteFrameName("iPhone.png");
        m_script->sprite_accelerate->setPosition(ccp(win_size.width*0.5,win_size.height*0.35));
        m_script->sprite_accelerate->setVisible(false);
        m_script->sprite_accelerate->setOpacity(0);
        m_script->addChild(m_script->sprite_accelerate,60);
    }
};

void CKBombScriptLayer::saveCurentBombType()
{
    char str[NAME_MAX];
    for (int i = 1 ; i < 4; ++i) {
        int  new_type = int(type/10)*10+i;
        sprintf(str, "bonus_bomb%d.png",new_type);
        
        CCSprite* tmp = static_cast<CCSprite *>(m_script->m_gui->getChildByTag(10 + i)->getNode());
        m_script->m_gui->getChildByTag(10 + i)->setValue(new_type);
        m_script->m_gui->getChildByTag(20 + i)->setValue(new_type);
        
        CCSpriteFrame* s_frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);
        if(s_frame)
        {
            tmp->setTexture(s_frame->getTexture());
            tmp->setTextureRect(s_frame->getRect(), s_frame->isRotated(), tmp->getContentSize());
        }
    };
};

void CKBombScriptLayer::initWithType(CCDictionary *_dict,const int &_type,const float &_frame_size_x)
{
    CKScriptLayerBase::initWithType(_dict, _type, _frame_size_x);
    
    initWithDict(m_dict);
    
    saveCurentBombType();
    
    m_script->initCube(ground_height,m_array);
    //create bombs object
    m_script->initPlane(ground_height);
    m_script->initBomb(ground_height,BOX_SIZE);

    start_pos_plane = win_size.width*0.5 - frame_size_x - m_script->m_airplane->getSprite()->getContentSize().width/2;
    m_script->m_airplane->setStartPositionX(start_pos_plane);
    m_script->m_airplane->getSprite()->setPosition(ccp(start_pos_plane,plane_height));
    m_script->m_airplane->setEndPositionX(win_size.width*0.8 + frame_size_x + m_script->m_airplane->getSprite()->getContentSize().width/2);
    speed_plane = m_script->m_airplane->getSpeed();
    m_script->m_airplane->setCurentPositionX(start_pos_plane);
    m_script->m_airplane->setCurentHeight(plane_height);

    m_script->m_gui->getChildByTag(43)->setVisible(false);//slot plane
    
    m_script->m_airplane->setEnableMachineGun(type/10 == 16);

};

//void CKBombScriptLayer::schedureAction()
//{
//    if(action_type == 1)
//    {
//        CCAcceleration  cc_accel;
//        cc_accel.x = -0.15;
//        cc_accel.timestamp = 0.1;
//        m_script->m_bombs->didAccelerate(&cc_accel);
//    };
//};

void CKBombScriptLayer::actionHand()
{
    active_action_hand = true;
    m_script->sprite_hand->setScale(1.0);
    m_script->sprite_hand->runAction(CCSequence::create(CCScaleTo::create(0.1, 0.7),CCScaleTo::create(0.2, 1.0),NULL));
    m_script->sprite_hand->setVisible(true);
};

void CKBombScriptLayer::actionAccelat()
{
    m_script->sprite_accelerate->setRotation(0);
    m_script->sprite_accelerate->runAction(CCSequence::create(CCDelayTime::create(0.3),CCFadeIn::create(0.3),CCRotateTo::create(0.2, -45),CCDelayTime::create(0.8),CCRotateTo::create(0.3, 0),CCFadeOut::create(0.3),NULL));
    m_script->sprite_accelerate->setVisible(true);
};

void CKBombScriptLayer::reloadForType(int _newtype)
{
    type = _newtype;
    is_reload_loop = true;
    //m_gui->reloadAtlas();
    m_script->m_airplane->stopAndHideAllBonus();
   
    if(m_script->sprite_hand)
        m_script->sprite_hand->setVisible(false);
    if(m_script->sprite_accelerate)
        m_script->sprite_accelerate->setVisible(false);
    
    m_script->m_airplane->stopMachineGun();
    saveCurentBombType();
    showType(NULL,&_newtype);
};

void CKBombScriptLayer::update(float dt)
{
    if(m_script->m_airplane->getPosition().x < previor_pos_plane_x) // new loop
    {
        m_script->unscheduleUpdate();
        need_reload = true;
        m_script->m_airplane->setCurentHeight(plane_height);
        newLoop();
        active_action_hand = false;
        m_script->stopActionAcc();
    };
    previor_pos_plane_x = m_script->m_airplane->getPosition().x;
    m_script->m_airplane->update(dt);
    if(can_run_bonus)
        if(m_script->m_airplane->getPosition().x > (action_start) && m_script->m_airplane->getPosition().x < (action_start + BOX_SIZE/2))
        {
            m_script->m_bombs->createNewBomb(true);
            m_script->m_airplane->touch(ccp(0,0),type);
            
            if(action_type == 1)
            {
                actionAccelat();
               m_script->startActionAcc();//emulation accelerate
            } else if(action_type == 21) //autofind
            {
                m_script->m_bombs->touchAutoFind(m_script->sprite_hand->getPosition());
                if(!active_action_hand)
                    actionHand();
            }
            
            can_run_bonus = false;
        };
    if(m_script->m_bombs)
        m_script->m_bombs->update(dt);
    if(action_type == 2 && !can_run_bonus)
    {
        if(m_script->m_bombs->getTouchBomb())
        {
            if(m_script->m_bombs->getTouchBomb()->distanse < (ground_height + BOX_SIZE*2.0) )
            {
                m_script->m_bombs->touchTap();
                if(!active_action_hand)
                    actionHand();
            };
        };
    };
};

#pragma mark - PLANE SCRIPT LAYER
CKPlaneScriptLayer::CKPlaneScriptLayer(CKLayerScript *_layer)
{
    is_mono = false;
    m_script = _layer;
    panel_left = 0;
    if(CCDirector::sharedDirector()->getWinSize().width == 568)
    {
        panel_left = 44;
    }
};

CKPlaneScriptLayer::~CKPlaneScriptLayer()
{
    
};

void CKPlaneScriptLayer::showType(cocos2d::CCNode *_sender, void *_data)
{
    //button buy
    m_script->m_gui->getChildByTag(BUTTON_BUY_ID)->setVisible(false);
    //label buy
    m_script->m_gui->getChildByTag(LABEL_BUY_ID)->setVisible(false);
    
    m_script->m_gui->getChildByTag(43)->setVisible(true);
    
    m_script->m_gui->getChildByTag(51)->setVisible(false);

    m_script->m_bombs->setLayer(m_script);
};

void CKPlaneScriptLayer::initWithType(CCDictionary *_dict,const int &_type,const float &_frame_size_x)
{
    CKScriptLayerBase::initWithType(_dict,_type,_frame_size_x);
    
    updateSlotState();
    
    initWithDict(m_dict);
    
    m_script->initCube(ground_height,m_array);
    //create bombs object
    m_script->initPlane(ground_height);
    m_script->initBomb(ground_height,BOX_SIZE);
    speed_plane = m_script->m_airplane->getSpeed();
    setPlaneSetting();
    m_script->m_airplane->setKamikadzeStartPosition(ccp(win_size.width*0.85,win_size.height*0.7));
};

void CKPlaneScriptLayer::actionHand()
{
    active_action_hand = true;
    m_script->sprite_hand->setScale(1.0);
    m_script->sprite_hand->runAction(CCSequence::create(CCScaleTo::create(0.07, 0.7),CCScaleTo::create(0.1, 1.0),NULL));
    m_script->sprite_hand->setVisible(true);
};

void CKPlaneScriptLayer::setPlaneSetting()
{
    start_pos_plane = win_size.width*0.5 - frame_size_x;// - m_airplane->getSprite()->getContentSize().width/2;
    m_script->m_airplane->setStartPositionX(start_pos_plane);
    m_script->m_airplane->setCurentHeight(plane_height);

    m_script->m_airplane->getSprite()->setPosition(ccp(start_pos_plane,plane_height));
    
    m_script->m_airplane->setSpeed(speed_plane);
    m_script->m_airplane->setEndPositionX(win_size.width*0.8*((type == Bonus_Airplane_Bomber)?1.2:1.0) + frame_size_x + m_script->m_airplane->getSprite()->getContentSize().width/2);
    
    m_script->m_airplane->setCurentPositionX(start_pos_plane);
    
    if(type == Bonus_Airplane_Bomber)
    {
        m_script->m_airplane->setLimitBomberX(win_size.width*0.75);
        m_script->m_airplane->setSpeed(speed_plane*0.9);
    }
};

void CKPlaneScriptLayer::reloadForType(int _newtype)
{
    is_reload_loop = true;
    m_script->m_airplane->stopAndHideAllBonus();
    m_script->m_bombs->deleteAllActiveBombs();
    
    type = _newtype;
    initWithDict(m_dict);
    setPlaneSetting();
    m_script->m_cube_array->reinitCubeWithCCArray(m_array);
    updateSlotState();

    m_script->m_bombs->setSpeed(BOX_SIZE*5);
    can_run_bonus = true;
    need_reload = false;
    m_script->m_gui->getChildByTag(43)->setStateImage(0);
    
    m_script->m_gui->getChildByTag(51)->setVisible(false);
    m_script->m_gui->getChildByTag(52)->setVisible(false);
    m_script->m_gui->getChildByTag(53)->setVisible(false);
    
    m_script->sprite_hand->setVisible(false);
    m_script->m_airplane->stopAndHideAllBonus();
    m_script->m_airplane->stopMachineGun();
};

void CKPlaneScriptLayer::initWithDict(cocos2d::CCDictionary *_dict)
{
    CCDictionary * m_dict_mombs = static_cast<CCDictionary*>(_dict->objectForKey("plane_bonuses"));
    char str[NAME_MAX];
    sprintf(str, "bonus_%d",type);
    CCDictionary *dict_bomb =  static_cast<CCDictionary*>(m_dict_mombs->objectForKey(str));
    action_start = static_cast<CCString *>(dict_bomb->objectForKey("activation"))->intValue()*BOX_SIZE/2;
    action_start += panel_left;
    plane_height = static_cast<CCString *>(dict_bomb->objectForKey("plane height"))->intValue()*BOX_SIZE + ground_height;
    m_array = static_cast<CCArray *>(dict_bomb->objectForKey("block_places"));
    if(!m_script->sprite_hand)
    {
        m_script->sprite_hand = CCSprite::createWithSpriteFrameName("hand.png");
        m_script->addChild(m_script->sprite_hand,60);
    }
    else
    {
        m_script->sprite_hand->setVisible(true);
    }
    m_script->sprite_hand->setPosition(ccp(win_size.width*0.75 + panel_left,win_size.height*0.25));
};

void CKPlaneScriptLayer::updateSlotState()
{
    std::map<int, std::string> map_tutorial_plane;
    map_tutorial_plane[0] = "plane_up.png";
    map_tutorial_plane[1] = "plane_slow.png";
    map_tutorial_plane[2] = "plane_armor.png";
    map_tutorial_plane[3] = "plane_speed.png";
    map_tutorial_plane[4] = "plane_kamikadze.png";
    map_tutorial_plane[5] = "plane_gun.png";
    map_tutorial_plane[6] = "plane_bomber.png";
    map_tutorial_plane[7] = "plane_phantom.png";
    
    //hide unused slot
    for (int i = 1; i < 4; ++i) {
        m_script->m_gui->getChildByTag(10 + i)->setVisible(false);
        m_script->m_gui->getChildByTag(20 + i)->setVisible(false);
    };
    m_script->m_gui->getChildByTag(13)->setVisible(true);
    
    m_script->m_gui->getChildByTag(43)->setVisible(true);//slot plnae
    
    m_script->m_gui->getChildByTag(BUTTON_BUY_ID)->setVisible(false);
    //label buy
    m_script->m_gui->getChildByTag(LABEL_BUY_ID)->setVisible(false);
    
    m_script->m_gui->getChildByTag(51)->setVisible(false);
    m_script->m_gui->getChildByTag(52)->setVisible(false);
    m_script->m_gui->getChildByTag(53)->setVisible(false);
    
    //show slot active
    CCSprite* tmp = static_cast<CCSprite*>(m_script->m_gui->getChildByTag(13)->getNode());
    CCSpriteFrame* s_frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(map_tutorial_plane[type - 1].c_str());
    if(s_frame)
    {
        tmp->setTexture(s_frame->getTexture());
        tmp->setTextureRect(s_frame->getRect(), s_frame->isRotated(), tmp->getContentSize());
    }
};

void CKPlaneScriptLayer::createBomb()
{
    m_script->m_airplane->touch(CCPointZero);
};

void CKPlaneScriptLayer::update(float dt)
{
    if(!can_run_bonus && !is_reload_loop)
        if(m_script->m_airplane->getPosition().x < previor_pos_plane_x) // new loop
        {
            
            m_script->unscheduleUpdate();
            need_reload = true;
            m_script->m_airplane->setCurentHeight(plane_height);
            
            //m_script->m_gui->getChildByTag(43)->setStateImage(0);
            newLoop();
            create_new_bomb = 0.0f;
        }
    previor_pos_plane_x = m_script->m_airplane->getPosition().x;
    
    create_new_bomb += dt;
    if(m_script->m_airplane->getPosition().x > win_size.width*0.95 && m_script->m_gui->getChildByTag(43)->getStateImage() == 1)
    {
       // m_script->m_airplane->stopBonus(type);
        m_script->m_airplane->stopBonus(type);
        m_script->m_gui->getChildByTag(43)->setStateImage(0);
        m_script->sprite_hand->setVisible(false);
    }
    if(type == CK::Bonus_Airplane_Gun)
    {
        
        if(create_new_bomb > 0.2 && m_script->m_airplane->getPosition().x < win_size.width*0.8 && m_script->m_airplane->getPosition().x > win_size.width*0.3)
        {
            m_script->m_airplane->touch(CCPointZero);
            actionHand();
            create_new_bomb = 0.0;
        };
    }
    else
    {
        if(create_new_bomb > 0.1 && m_script->m_airplane->getPosition().x < win_size.width*0.8 && m_script->m_airplane->getPosition().x > win_size.width*0.3)
        {
            m_script->m_airplane->touch(CCPointZero);

            create_new_bomb = 0.0;
        };
    }

    m_script-> m_airplane->update(dt);
    if(can_run_bonus)
        if(m_script->m_airplane->getPosition().x > (action_start))
        {
            
            m_script->m_airplane->runBonus(type);
            m_script->m_gui->getChildByTag(43)->setStateImage(1);
            if(type == 1)
                m_script->m_airplane->setStepUpBonus(BOX_SIZE/40.0);
            is_reload_loop = false;
            can_run_bonus = false;
        };
    
    m_script->m_bombs->update(dt);
    if(type == 8)
        m_script->m_bomb_ghost->update(dt);
};

