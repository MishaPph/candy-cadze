//
//  CKTutorial.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 17.01.13.
//
//
#include "CKAudioMgr.h"
#include "CKTutorial.h"
#include "CKLoaderGUI.h"
#include "CKScriptScene.h"
#include "CKTutorialScriptLayer.h"
#include "CKCubeArray.h"

const CCSize SIZE_RTT_FRATE_IPAD = CCSize(714, 616);
const CCSize SIZE_RTT_FRATE_IPHONE = CCSize(297,258);
const CCPoint POSITON_FRAME_IPAD = ccp(542,388);
const CCPoint POSITON_FRAME_IPHONE = ccp(256,167);

CKTutorial::~CKTutorial()
{
    CCLOG("~CKTutorial");
    CC_SAFE_DELETE(m_gui);
    removeAllChildrenWithCleanup(true);
    CC_SAFE_RELEASE_NULL(m_rtt);
    CC_SAFE_RELEASE_NULL(m_dict);
    CC_SAFE_RELEASE_NULL(script_bomb);
    CC_SAFE_RELEASE_NULL(script_plane);    
    
    for (int i = 0; i < 3; ++i) {
        CC_SAFE_RELEASE_NULL(frame_rtt[i]);
        CC_SAFE_DELETE(frame_base[i]);
    }
};

CKTutorial::CKTutorial()
{
    m_gui = NULL;
    m_parent = NULL;
    last_tap = NULL;
    panel_left = 0.0;
    win_size = CCDirector::sharedDirector()->getWinSize();
    
    if(win_size.width == 568)
    {
        panel_left = 44;
    };
    
    map_tutorial_bomb.clear();
    list_plane_name.clear();
    list_bomb.clear();
    list_general.clear();
    list_plane.clear();
    
    initGui();
    
    initList();
    
    m_bg_mask = CCSprite::create("gray_mask.png");
   // m_bg_mask->setVisible(false);
    addChild(m_bg_mask,-1);
    m_bg_mask->setScaleX(win_size.width/m_bg_mask->getContentSize().width);
    m_bg_mask->setScaleY(win_size.height/m_bg_mask->getContentSize().height);
    m_bg_mask->setPosition(ccp(win_size.width/2,win_size.height/2));
    
    map_tutorial_bomb[3] = 1;// "t_b2.png";
    map_tutorial_bomb[14] = 2;//"t_b3.png";
    map_tutorial_bomb[7] = 3;//"t_b4.png";
    map_tutorial_bomb[13] = 4;//"t_b5.png";
    map_tutorial_bomb[15] = 5;//"t_b6.png";
    map_tutorial_bomb[5] = 6;//"t_b7.png";
    map_tutorial_bomb[6] = 7;//"t_b8.png";
    map_tutorial_bomb[11] = 8;//"t_b11.png";
    map_tutorial_bomb[8] = 9;//"t_b12.png";
    map_tutorial_bomb[16] = 10;//"t_b13.png";
    
    map_tutorial_bomb[12] = 11;//"t_b14.png";
    map_tutorial_bomb[2] = 12;//"t_b15.png";
    map_tutorial_bomb[4] = 13;//"t_b16.png";
    
    list_bomb_name.push_back(3);
    list_bomb_name.push_back(14);
    list_bomb_name.push_back(7);
    list_bomb_name.push_back(13);
    list_bomb_name.push_back(15);
    list_bomb_name.push_back(5);
    list_bomb_name.push_back(6);
    list_bomb_name.push_back(11);
    list_bomb_name.push_back(8);
    list_bomb_name.push_back(16);
    list_bomb_name.push_back(12);
    list_bomb_name.push_back(2);
    list_bomb_name.push_back(4);
    
    list_plane_name.push_back(4);
    list_plane_name.push_back(1);
    list_plane_name.push_back(2);
    list_plane_name.push_back(3);
    list_plane_name.push_back(5);
    list_plane_name.push_back(7);
    list_plane_name.push_back(6);
    list_plane_name.push_back(8);
    
    initBombList();
    initPlaneList();
    initGeneralList();
    
    m_dict = CCDictionary::createWithContentsOfFile("tutorial_scripts.plist");
    m_dict->retain();

    script_bomb = new CKTutorialScriptScene;
    initFramesScriptBomb();
    
    script_plane = new CKTutorialScriptScene;
    initFramesScriptPlane();
    
    //int n = 1;
    //this->tapCall(NULL,&n);
    //start(3,-1);
    
   if(!ObjCCalls::isLittleMemory())
      createRTT();
    
    CCLOG("-----------------CKTutorial ::dump texture------------------");
    CCTextureCache::sharedTextureCache()->dumpCachedTextureInfo();
    
    
};

void CKTutorial::createRTT()
{
    rtt = CCRenderTexture::create(win_size.width, win_size.height, kCCTexture2DPixelFormat_RGB888, GL_DEPTH24_STENCIL8_OES);
    rtt->setPosition(ccp(win_size.width*0.5,win_size.height*0.5));
    rtt->setVisible(false);
    addChild(rtt,-1);
    
    for (int i = 0; i < 3; ++i) {
        if (CKHelper::Instance().getIsIpad()) {
            frame_rtt[i] = CCRenderTexture::create(SIZE_RTT_FRATE_IPAD.width,SIZE_RTT_FRATE_IPAD.height, kCCTexture2DPixelFormat_RGBA8888, GL_DEPTH24_STENCIL8_OES);
            m_rtt = CCRenderTexture::create(SIZE_RTT_FRATE_IPAD.width,SIZE_RTT_FRATE_IPAD.height, kCCTexture2DPixelFormat_RGBA8888, GL_DEPTH_STENCIL_OES);
            m_rtt->setPosition(POSITON_FRAME_IPAD);
        }
        else
        {
            frame_rtt[i] = CCRenderTexture::create(SIZE_RTT_FRATE_IPHONE.width,SIZE_RTT_FRATE_IPHONE.height, kCCTexture2DPixelFormat_RGBA8888, GL_DEPTH24_STENCIL8_OES);
            m_rtt = CCRenderTexture::create(SIZE_RTT_FRATE_IPHONE.width,SIZE_RTT_FRATE_IPHONE.height, kCCTexture2DPixelFormat_RGBA8888, GL_DEPTH_STENCIL_OES);
            m_rtt->setPosition(ccp(POSITON_FRAME_IPHONE.x + panel_left,POSITON_FRAME_IPHONE.y));
        };
        frame_rtt[i]->retain();
    };
    m_rtt->retain();
    script_bomb->setRTT(m_rtt,frame_rtt);
    script_plane->setRTT(m_rtt,frame_rtt);
};

void CKTutorial::releaseRTT()
{
    for (int i = 0; i < 3; ++i) {
        frame_rtt[i]->removeFromParentAndCleanup(false);
        CC_SAFE_RELEASE_NULL(frame_rtt[i]);
    };
    m_rtt->removeFromParentAndCleanup(false);
    CC_SAFE_RELEASE_NULL(m_rtt);
    
    rtt->retain();
    rtt->removeFromParentAndCleanup(false);
    CC_SAFE_RELEASE_NULL(rtt);
};

void CKTutorial::initFramesScriptBomb()
{
    m_gui->get()->addChild(script_bomb,2);
    if (CKHelper::Instance().getIsIpad()) {
        script_bomb->initBombFrame(CCSize(SIZE_RTT_FRATE_IPAD.width,SIZE_RTT_FRATE_IPAD.height),POSITON_FRAME_IPAD);
    }
    else
    {
        script_bomb->initBombFrame(CCSize(SIZE_RTT_FRATE_IPHONE.width,SIZE_RTT_FRATE_IPHONE.height),ccp(POSITON_FRAME_IPHONE.x + panel_left,POSITON_FRAME_IPHONE.y));
    };
    script_bomb->createAllFrame(m_dict);
    script_bomb->setEnable(false);
};

void CKTutorial::initFramesScriptPlane()
{
    
    m_gui->get()->addChild(script_plane,2);
    
    if (CKHelper::Instance().getIsIpad()) {
        script_plane->initPlaneFrame(CCSize(SIZE_RTT_FRATE_IPAD.width,SIZE_RTT_FRATE_IPAD.height),POSITON_FRAME_IPAD);
    }
    else
    {
        script_plane->initPlaneFrame(CCSize(SIZE_RTT_FRATE_IPHONE.width,SIZE_RTT_FRATE_IPHONE.height),ccp(POSITON_FRAME_IPHONE.x + panel_left,POSITON_FRAME_IPHONE.y));
    };

    script_plane->createAllFrame(m_dict);
    script_plane->setEnable(false);
};

void CKTutorial::initList()
{
    for(int i = 1; i <= COUNT_FRAME_BOMB; i++)
    {
        list_bomb.push_back(m_gui->getChildByTag(UNDERLAER_ID)->getChildByTag(1000+i));
        m_gui->getChildByTag(UNDERLAER_ID)->getChildByTag(1000+i)->setVisible(false);
    };
    
    for(int i = 1; i <= COUNT_FRAME_PLANE; i++)
    {
        list_plane.push_back(m_gui->getChildByTag(UNDERLAER_ID)->getChildByTag(2000+i));
        m_gui->getChildByTag(UNDERLAER_ID)->getChildByTag(2000+i)->setVisible(false);
    };
    
    for(int i = 1; i <= COUNT_FRAME_GENERAL; i++)
    {
        list_general.push_back(m_gui->getChildByTag(UNDERLAER_ID)->getChildByTag(3000+i));
        m_gui->getChildByTag(UNDERLAER_ID)->getChildByTag(3000+i)->setVisible(false);
    };
}

void CKTutorial::start(int _tap,int _frame)
{
    CCLOG("CKTutorial::start _tap %d frame %d",_tap,_frame);
    this->tapCall(NULL,&_tap);
    if(_frame >= 0)
    {
        if(_frame > 1000)
        {
            int d = _frame - 1001;
            this->showHintPlane(NULL,&d);
        }
        else
        {
            this->showHintBomb(NULL,&_frame);
        }
    }
};

void CKTutorial::initGui()
{
    CCLOG("CKTutorial::initGui");
    m_gui = new CKGUI();
    m_gui->create(CCNode::create());
    m_gui->load(CK::loadFile("c_Tutorial.plist",true).c_str());
    m_gui->setTouchEnabled(true);
    m_gui->setTarget(this);
    m_gui->addFuncToMap("menuCall", callfuncND_selector(CKTutorial::menuCall));
    m_gui->addFuncToMap("tapCall", callfuncND_selector(CKTutorial::tapCall));
    m_gui->addFuncToMap("showHintBomb", callfuncND_selector(CKTutorial::showHintBomb));
    m_gui->addFuncToMap("showHintPlane", callfuncND_selector(CKTutorial::showHintPlane));
    //  m_gui->addFuncToMap("selectorCall", callfuncND_selector(CKTutorial::selectorCall));
    m_gui->get()->setPositionY(-win_size.height);
    addChild(m_gui->get(),1);
};


void CKTutorial::setVisibleList(const std::vector<CKObject*> &_list,bool _visible)
{
    std::vector<CKObject*>::const_iterator _it = _list.begin();
    while (_it != _list.end()) {
        (*_it)->setVisible(_visible);
        //(*_it)->setVisibleAll(_visible);
        if(_visible)
        {
            
        };
        //(*_it)->setTouch(_visible);
        _it++;
    }
};


void CKTutorial::initPlaneList()
{
    it = list_plane.begin();
    // float border = 60;
    float height = m_gui->getChildByTag(UNDERLAER_ID)->getNode()->getContentSize().height;
    float siz = win_size.width*0.1;
    float width = m_gui->getChildByTag(UNDERLAER_ID)->getNode()->getContentSize().width;
    int n = 0;
    for(int i = 0; i < 2;i++)
    {
        if(it == list_plane.end())
            return;
        for(int j = 0; j < 4;j++)
        {
            if(it == list_plane.end())
                return;
            
            //(*it)->setPosition(j*width/4 + siz, height*0.5 + (1 - i)*height/4 + siz/2);
            
            (*it)->setPosition(j*width*0.2 + siz*1.2, height*0.35 + (1 - i)*height/4 + siz*0.5);
            
            (*it)->setFunctionName(std::string("showHintPlane"));
            
            CCLOG("map_tutorial_plane[n].c_str() %s",CKBonus::getPlaneName(list_plane_name[n]).c_str());
            
            CCSprite* tmp = static_cast<CCSprite*>((*it)->getChildByTag(1)->getNode());
         
            CCSpriteFrame* s_frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(CKBonus::getPlaneName(list_plane_name[n]).c_str());
            tmp->setTexture(s_frame->getTexture());
            tmp->setTextureRect(s_frame->getRect(), s_frame->isRotated(), tmp->getContentSize());
            
            (*it)->setTouch(true);
            (*it)->setVisible(true);
            it++;
            n++;
        };
    }
};

void CKTutorial::initGeneralList()
{
    it = list_general.begin();
    
    float height = m_gui->getChildByTag(UNDERLAER_ID)->getNode()->getContentSize().height;
    
    float width = m_gui->getChildByTag(UNDERLAER_ID)->getNode()->getContentSize().width;
    
    for(int i = 0; i < 1;i++)
    {
        if(it == list_general.end())
            return;
        for(int j = 0; j < 4;j++)
        {
            if(it == list_general.end())
                return;
            (*it)->setPosition(width*0.5 + (j - 1)*width/4, height*0.5);
            (*it)->setFunctionName(std::string("showHint"));
            (*it)->setTouch(true);
            (*it)->setVisible(true);
            it++;
        };
    }
};

void CKTutorial::initBombList()
{
    it = list_bomb.begin();
    //float border = 60;
    float height = m_gui->getChildByTag(UNDERLAER_ID)->getNode()->getContentSize().height;
    float siz = win_size.width*0.1;
    float width = m_gui->getChildByTag(UNDERLAER_ID)->getNode()->getContentSize().width;
    
   // std::vector<int>::iterator it_name = list_bomb_name.begin();
    map_it = map_tutorial_bomb.begin();
    int k = 0;
    for(int i = 0; i < 3;i++)
    {
        if(it == list_bomb.end())
            return;
        for(int j = 0; j < 5;j++)
        {
            if(it == list_bomb.end())
                return;
            
            (*it)->setPosition(j*width*0.15 + siz*1.6, height - i*height*0.2 - siz*1.6);
            
            char str[NAME_MAX];
            sprintf(str, "bonus_bomb%d1.png",list_bomb_name[k]);
            
            CCSprite* tmp = static_cast<CCSprite*>((*it)->getChildByTag(1)->getNode());
            
            CCSpriteFrame* s_frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str);
            tmp->setTexture(s_frame->getTexture());
            tmp->setTextureRect(s_frame->getRect(), s_frame->isRotated(), tmp->getContentSize());
            
            (*it)->setFunctionName(std::string("showHintBomb"));
            (*it)->setValue(list_bomb_name[k]);
            (*it)->setTouch(true);
            (*it)->setVisible(true);
            it++;
            //it_name++;
            k++;
            map_it++;
        };
    };
};


void CKTutorial::activeted(cocos2d::CCLayer *_node,int scene_num)
{
    if(CKFileOptions::Instance().isPlayMusic() && CKHelper::Instance().menu_number != CK::SCENE_GAME)
    {
        CKAudioMng::Instance().fadeBgToVolume(CK::SOUND_FADE_VALUE);
    };
  //  start(3,-1);
  //  CCSpriteFrameCache::sharedSpriteFrameCache()->removeUnusedSpriteFrames();
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(CK::loadImage("a_Plane.plist").c_str());
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(CK::loadImage("a_TutorialPause.plist").c_str());    
    cocos2d::CCLayer *m_layer = (cocos2d::CCLayer *)this;
    
    if(ObjCCalls::isLittleMemory())
        createRTT();
    
    if(m_parent && m_parent->getParent())
        m_parent->getParent()->removeChild(m_layer, false);
    {
        m_parent = _node;
        CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(m_parent);

        m_layer->setTouchEnabled(true);
        m_layer->setVisible(true);
        m_bg_mask->setVisible(true);
        
        m_parent->getParent()->addChild(m_layer,100);
        
        rtt->clear(0, 0, 0, 0);
        rtt->begin();
        
        CCDirector::sharedDirector()->getRunningScene()->visit();

        rtt->end();
        
        m_bg_mask->setVisible(false);
        m_parent->pauseSchedulerAndActions();
        m_parent->setVisible(false);
        rtt->setVisible(true);
    };
    //rtt->getSprite()->setOpacity(0);
    //rtt->getSprite()->runAction(CCFadeIn::create(0.5));
    m_gui->get()->runAction(CCEaseElasticOut::create(CCMoveBy::create(1.0, ccp(0,win_size.height))));
};

void CKTutorial::deactived()
{
    m_gui->get()->setPositionY(-win_size.height);
    
    if(CKFileOptions::Instance().isPlayMusic() && CKHelper::Instance().menu_number != CK::SCENE_GAME)
    {
        CKAudioMng::Instance().fadeBgToVolume(1.0);
    };
    cocos2d::CCLayer *m_layer = (cocos2d::CCLayer *)this;
    if(m_parent)
    {
        m_parent->setTouchEnabled(true);
        m_parent->getParent()->removeChild(m_layer, false);
        m_parent->resumeSchedulerAndActions();
        m_parent->setVisible(true);
        CCDirector::sharedDirector()->getTouchDispatcher()->addStandardDelegate(m_parent, 0);
    };
    m_parent = NULL;
    rtt->setVisible(false);
    
    if(ObjCCalls::isLittleMemory())
        releaseRTT();
    
    CCDirector::sharedDirector()->getTouchDispatcher()->removeDelegate(this);
    
};
/*
 void CKTutorial::funcActive()
 {
 updateRTT();
 schedule(schedule_selector(CKTutorial::update),1.0/60.0);
 };
 
 void CKTutorial::funcDeactive()
 {
 unschedule(schedule_selector(CKTutorial::update));
 };
 */
void CKTutorial::showHintPlane(CCNode* _sender,void *_value)
{
    int n = *static_cast<int*>(_value);
    CCLOG("showHintPlane %d",n);
    if(_sender)
    CKAudioMng::Instance().playEffect("button_pressed");
    script_plane->setEnable(true);
    script_plane->reload();
    script_plane->setCurentFrameNumb(n,false);
    
    setVisibleList(list_bomb, false);
    setVisibleList(list_plane, false);
    setVisibleList(list_general, false);
};
void CKTutorial::showHintBomb(CCNode* _sender,void *_value)
{
    int n = *static_cast<int*>(_value);
    CCLOG("showHintBomb %d",n);
    if(_sender)
    CKAudioMng::Instance().playEffect("button_pressed");
    //n = (n > 20)?(map_tutorial_bomb[int(n/10)] - 1):(map_tutorial_bomb[n] - 1);
    script_bomb->setEnable(true);
    int frame = (n > 20)?(map_tutorial_bomb[int(n/10)] - 1):(map_tutorial_bomb[n] - 1);
    script_bomb->reload();
    if(n > 20)
    {
        script_bomb->setCurentFrameNumb(frame,false,n);
    }
    else
    {

        script_bomb->setCurentFrameNumb(frame,false);
    }

    setVisibleList(list_bomb, false);
    setVisibleList(list_plane, false);
    setVisibleList(list_general, false);
};

void CKTutorial::menuCall(CCNode* _sender,void *_value)
{
    switch (*static_cast<int*>(_value)) {
        case CK::Action_Back:
            CKAudioMng::Instance().stopAllEffect();
            CKAudioMng::Instance().playEffect("button_pressed");
            deactived();
            break;
            
        default:
            break;
    }
};

void CKTutorial::tapCall(CCNode* _sender,void *_value)
{
    script_bomb->setEnable(false);
    script_plane->setEnable(false);
    script_bomb->reload();
    script_plane->reload();

    curent_tap  = *static_cast<int*>(_value);
    if(last_tap)
        last_tap->setStateImage(0);
    last_tap = m_gui->getChildByTag(20 + *static_cast<int*>(_value));
    last_tap->setStateImage(1);
    
    switch (*static_cast<int*>(_value)) {
        case 3://bomb
            if(_sender != NULL)
                CKAudioMng::Instance().playEffect("button_pressed");
            setVisibleList(list_plane, false);
            setVisibleList(list_general, false);
            setVisibleList(list_bomb, true);
            break;
        case 2://plane
            if(_sender)
                CKAudioMng::Instance().playEffect("button_pressed");
            setVisibleList(list_bomb, false);
            setVisibleList(list_plane, true);
            setVisibleList(list_general, false);
            break;
        case 1: //general
            if(_sender)
                CKAudioMng::Instance().playEffect("button_pressed");
            setVisibleList(list_bomb, false);
            setVisibleList(list_plane, false);
            setVisibleList(list_general, true);
            break;
        default:
            break;
    }
};

void CKTutorial::ccTouchesBegan(cocos2d::CCSet* pTouches, cocos2d::CCEvent* event)
{
    if(!isVisible())
        return;
    CCTouch* touch =(CCTouch*)*pTouches->begin();
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    
    if(!m_gui->setTouchBegan(location))
        if(location.x > win_size.width*0.1 && location.x < win_size.width*0.9)
            if(location.y > win_size.height*0.1 && location.y < win_size.height*0.9)
            {
                deactived();
            }
};

void CKTutorial::ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent)
{
    if(!isVisible())
        return;
    CCTouch* touch =(CCTouch*)*pTouches->begin();
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    
    m_gui->setTouchMove(location);
    
};

void CKTutorial::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
{
    if(!isVisible())
        return;
    CCTouch* touch =(CCTouch*)*pTouches->begin();
    CCPoint location = touch->getLocationInView();
    location = CCDirector::sharedDirector()->convertToGL(location);
    
    m_gui->setTouchEnd(location);
};
