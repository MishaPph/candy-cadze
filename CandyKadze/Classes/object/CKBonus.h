//
//  CKBonus.h
//  CandyKadze
//
//  Created by PR1.Stigol on 25.09.12.
//
//

#ifndef __CandyKadze__CKBonus__
#define __CandyKadze__CKBonus__

#include <iostream>
#include <list.h>
#include <cocos2d.h>
#include "CK.h"
#include "CKCubeArray.h"

using namespace CK;
using namespace cocos2d;

class CKCubeArray;
class TBSpriteMask;
struct CKBonusInfo
{
    unsigned int count,type;
    bool is_bomb;
    float offset_mask;
    TBSpriteMask *mask;
};

class CKBonus
{
    CKCubeArray* m_array;
    std::list<CKBonusInfo *> info_list;
    std::list<CCNode *> open_bonus;
    std::list<CCNode *> bonus_list;
    std::list<CCNode *>::iterator bonus_it;
    std::vector< pair<unsigned char,unsigned char> > list_bomb_bonus;
    std::vector< pair<unsigned char,unsigned char> > list_airplane_bonus;
    std::vector< pair<unsigned char,unsigned char> >::iterator it;
    
    CCNode* m_layer;
    int ground_height;
    int BOX_SIZE,panel_left;
    void createBonus(unsigned char _x,unsigned char _y,unsigned char type,bool _bonus_bomb = true);
    unsigned char loadBonusWithPlist();
    bool genBonusArray(unsigned char _type,bool _bonus_bomb = true);
    CCAnimation * anim;
    unsigned int first_bonus;
public:
    CKBonus();
    ~CKBonus();
    void update(const float &_dt);
    void restart();
    unsigned int isNewBonus();
    unsigned int getFirstBonus() const
    {
        return first_bonus;
    };
    void setLayer(CCNode *_layer);
    void genBonus(int _count);
    //CCNode* findActiveBonus(const CCPoint &_p,const CCSize &_size);
    list<CCNode*>* getOpenBonus();
    void findOpenBonus();
    static std::string getPlaneName(int _type);
    void create();
    void setCubeArray(CKCubeArray* _array){m_array = _array;};
};
#endif /* defined(__CandyKadze__CKBonus__) */
