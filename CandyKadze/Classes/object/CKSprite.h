//
//  CKSprite.h
//  CandyKadze
//
//  Created by PR1.Stigol on 02.10.12.
//
//

#ifndef __CandyKadze__CKSprite__
#define __CandyKadze__CKSprite__

#include <cocos2d.h>
#include <iostream>

using namespace cocos2d;

class CKSprite: public CCSprite
{
public:
    void move(float x,float y);
    const ccTex2F& getMove();
    void ondraw(void);
};

class CKMaskSprite: public CCNode
{
    public:
    CKMaskSprite();
    // the mask
    //virtual void draw(void);
    void setSprite(CCSprite *_sprite)
    {
        m_sprite = _sprite;
    };
    void setMask(CCSprite *_mask)
    {
        
        m_mask = _mask;
    };
    virtual void visit(void);
    static CKMaskSprite *create(int w ,int h, CCTexture2DPixelFormat eFormat, GLuint uDepthStencilFormat);
    bool initWithWidthAndHeight(int w ,int h, CCTexture2DPixelFormat eFormat, GLuint uDepthStencilFormat);
protected:
    CCSprite *m_mask,*m_sprite;
    CCRenderTexture *m_rtt;
    virtual ~CKMaskSprite();
};

#endif /* defined(__CandyKadze__CKSprite__) */
