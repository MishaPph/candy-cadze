//
//  CKGUI.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 06.12.12.
//
//

#include "CKLoaderGUI.h"
void CKGUI::setSelector(SEL_MenuHandler _selector)
{
    
};

void CKGUI::addFuncToMap(const std::string& _name,SEL_CallFuncND _func_sel)
{
    CCLOG("addFuncToMap:: name %s",_name.c_str());
    func_map[_name] = _func_sel;
};

CKGUI::CKGUI()
{
    curent_node = NULL;
    scroll_object = NULL;
    scroll_move = true;
 //   m_scroll = NULL;
};
CKGUI::~CKGUI()
{
    CCLOG("~CKGUI:: destroy ");
  //  CC_SAFE_DELETE(m_scroll);
};
void CKGUI::setMovedScroll(bool _true)
{
    scroll_move = _true;
};

int CKGUI::getScrollFrame(const CCPoint &_pos)
{
    if(scroll_object == NULL)
    {
        scroll_object = getChildByType(CK::GuiScrollView);
    };
    
    float sizeFrame = scroll_object->getNode()->getContentSize().width/scroll_object->m_scroll_object->scroll_num_window;
    
    float diff = _pos.x + _pos.y;
    if(fabs(diff) > sizeFrame*scroll_object->m_scroll_object->slide_percent){
        if(diff > 0){
            curent_scroll_frame--;
        }
        else{
            curent_scroll_frame++;
        };
    }
    if(curent_scroll_frame > (scroll_object->m_scroll_object->scroll_num_window - 1))
        curent_scroll_frame--;
    if(curent_scroll_frame < 0)
        curent_scroll_frame++;
    
    return curent_scroll_frame;
};

float CKGUI::getScrollValue()
{
    return scroll_object->getPos().x;
};

int CKGUI::getNumberScroll()
{
    return curent_scroll_frame;
};

//void CKGUI::setScroll(CKObject *_rec,int _count,int _min,int _max)
//{
//    CC_SAFE_DELETE(m_scroll);
//    m_scroll = new CKScrollObject();
//    m_scroll->sizeFrameY =  _rec->getNode()->getContentSize().height/_count;
//    m_scroll->count_object = _count;
//    m_scroll->min = _min;
//    m_scroll->max = _max;
//    m_scroll->object = _rec;
//};

//CKScrollObject *CKGUI::getScroll()
//{
//    return m_scroll;
//};

CKObject *CKGUI::getCurrnet()
{
    return curent_node;
}

void CKGUI::setActiveScrollFrame(int _frame,bool _animation)
{
    
    if(scroll_object == NULL)
    {
        scroll_object = getChildByType(CK::GuiScrollView);
    };
    if(!scroll_object)
        return;
    curent_scroll_frame = abs(_frame);
    

    if(scroll_object->m_scroll_object->scroll_num_window == 5)
    {
        CKHelper::Instance().setWorldNumber(abs(_frame));
        float sizeFrameX = scroll_object->getNode()->getContentSize().width/scroll_object->m_scroll_object->scroll_num_window;
        float movX = _frame*sizeFrameX;
        
       // CCLOG("setActiveScrollFrame::size Frame %f",sizeFrameX);
        if(_animation)
        {
            CCAction* move = CCMoveTo::create(MIN(fabs(movX - scroll_object->getPos().x)/2000.0,0.5), ccp(movX,scroll_object->getPos().y));
            scroll_object->getNode()->runAction(move);
        }
        else
            scroll_object->getNode()->setPosition(ccp(movX,scroll_object->getPos().y));
        scroll_object->savePosition(ccp(movX,scroll_object->getPos().y));
    };
    
    if(scroll_object->m_scroll_object->scroll_num_window == 13 || scroll_object->m_scroll_object->scroll_num_window == 52)
    {
        float sizeFrameY = scroll_object->getNode()->getContentSize().height/scroll_object->m_scroll_object->scroll_num_window;
        float movY = _frame*sizeFrameY;
        
     //   CCLOG("setActiveScrollFrame::size Frame %f movY %f frame %d",sizeFrameY,movY,curent_scroll_frame);
        if(_animation)
        {
            scroll_object->getNode()->stopAllActions();
            CCAction* move = CCMoveTo::create(MIN(fabs(movY - scroll_object->getPos().y)/2000.0,0.5), ccp(scroll_object->getPos().x,movY));
            scroll_object->getNode()->runAction(move);
        }
        else
            scroll_object->getNode()->setPosition(ccp(scroll_object->getPos().x,movY));
        scroll_object->savePosition(ccp(scroll_object->getPos().x,movY));
    }
};

bool CKGUI::setTouchBegan(const CCPoint &_pos)
{
    if(!touch_enabled|| !m_node->isVisible())
    {
        CCLOG("CKGUI::setTouchBegan::touch_enabled %d %d",touch_enabled,m_node->isVisible());
        return false;
    }
//    if(curent_node)
//        curent_node->setStateImage(0);
    
    curent_node = getTouchObject(_pos);
    if(curent_node)
    {
        curent_state = curent_node->getStateImage();
        CCLOG("CKGUI::setTouchBegan curent_state %d tag %d",curent_state,curent_node->getTag());
        curent_node->setStateImage(1);
        //curent_node->setVisible(false);
        last_pos = _pos;
        return true;
    };
    return false;
};


void CKGUI::setTouchMove(const CCPoint &_pos)
{
    if(!touch_enabled || !m_node->isVisible())
        return;
   if(!curent_node)
       return;
    
    if(scroll_move && curent_node->getType() == CK::GuiScrollView)
    {

        
        float moved = _pos.x - last_pos.x;
        
        if(curent_node->getPos().x > 0 && moved > 0)
        {
            moved *= 0.2;
        }
        else if(curent_node->getPos().x < -curent_node->getNode()->getContentSize().width*0.8 && moved < 0)
        {
            moved *= 0.2;
        };
        
        curent_node->moveY(_pos.y - last_pos.y);
        curent_node->moveX(moved);
        
        curent_node->setStateImage(0);
    }
    else
        if(curent_node->getType() == CK::GuiButton || curent_node->getType() == CK::GuiNode)
        {
            if(scroll_move)
            {
                if(scroll_object == NULL)
                {
                    scroll_object = getChildByType(CK::GuiScrollView);
                }
                else
                {
                    if(scroll_object->isPointInObject(_pos))
                    {
                        curent_node->setStateImage(0);
                        curent_node = scroll_object;
                    };
                }
            }
        };
};

void CKGUI::runFuncObject(CKObject *_obj)
{
    if(func_map[_obj->getFunctionName()] != NULL)
    {
        (m_listener->*func_map[_obj->getFunctionName()])(_obj->getNode(),_obj->getValue());
    }
    else
    {
        CCLOG("setTouchEnd:: not function name %s",_obj->getFunctionName().c_str());
    }
};

void CKGUI::setTouchEnd(const CCPoint &_pos)
{
    if(!touch_enabled|| !m_node->isVisible())
        return;
    
    if(curent_node)
    {
        
        curent_node->setStateImage(curent_state);
        
        if(scroll_move && curent_node->getType() == CK::GuiScrollView)
        {
            getScrollFrame(ccp(_pos.x - last_pos.x,_pos.y - last_pos.y));
            //CCLOG("setTouchEnd:: num_frame %d",curent_scroll_frame);
            setActiveScrollFrame(-curent_scroll_frame);
        };
        if(curent_node == getTouchObject(_pos))
        {
            if(func_map[curent_node->getFunctionName()] != NULL)
            {
                (m_listener->*func_map[curent_node->getFunctionName()])(curent_node->getNode(),curent_node->getValue());
            }
            else
            {
                CCLOG("setTouchEnd:: not function name %s %d",curent_node->getFunctionName().c_str(),curent_node->getTag());
            }
        }
    };

 //   curent_node = NULL;
};



void CKGUI::setTouchEnabled(bool _touch)
{
    touch_enabled = _touch;
}

bool CKGUI::isTouchEnabled()
{
    return touch_enabled;
};

void CKGUI::setTarget(CCObject *_rec)
{
    m_listener = _rec;
};
