//
//  CKButton.h
//  CandyKadze
//
//  Created by PR1.Stigol on 27.09.12.
//
//

#ifndef __CandyKadze__CKButton__
#define __CandyKadze__CKButton__

#include <iostream>
#include <cocos2d.h>
#include <list.h>

#include "CK.h"

using namespace cocos2d;

class CKButton
{
    CKButton(){font_name = "Agent Orange";};
    CCLabelTTF* m_label;
    std::string active_str,pressed_str,disable_str;
    std::string font_name;
    std::list<CCMenuItemSprite*> item_list;
    std::list<CCMenuItemSprite*>::iterator  it;
public:
    void setFontName(std::string _font)
    {
        font_name = _font;
    };
    void setButtonNames(std::string _active,std::string _pressed,std::string _disable)
    {
        active_str = _active;pressed_str = _pressed; disable_str = _disable;
    };
    
    CCMenuItemSprite* create(const char* _label,int _tag,int _font_size,CCLayer *_layer,SEL_MenuHandler _selector)
    {
        CCSprite *act = CCSprite::createWithSpriteFrameName(active_str.c_str());
        CCSprite *sel = CCSprite::createWithSpriteFrameName(pressed_str.c_str());
        CCSprite *dis = CCSprite::createWithSpriteFrameName(disable_str.c_str());
        CCMenuItemSprite* item  = CCMenuItemSprite::create(act, sel,dis, _layer,_selector);
//        CCMenuItemImage* item = CCMenuItemImage::create(active_str.c_str(), pressed_str.c_str(),disable_str.c_str(), _layer,_selector);
        item->setTag(_tag);
        m_label = CCLabelTTF::create(_label, font_name.c_str(), _font_size);
        m_label->setPosition(ccp(item->getContentSize().width/2,item->getContentSize().height/2));
        m_label->setColor(ccc3(0,0,0));
        item->addChild(m_label, 1);
        item_list.push_back(item);
        return item;
    };
    int countActiveItem()
    {
        int count = 0;
        it = item_list.begin();
        while(it != item_list.end())
        {
            if(*it != NULL)
                count++;
            it++;
        };
        item_list.clear();
        return count;
    };
    CCLabelTTF* getLabel()
    {
        return m_label;
    };
    static CKButton& Instance()
    {
        static CKButton newCKButton;
        return newCKButton;
    }
};
#endif /* defined(__CandyKadze__CKButton__) */
