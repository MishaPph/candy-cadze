//
//  CKBackNode.h
//  CandyKadze
//
//  Created by PR1.Stigol on 28.09.12.
//
//

#ifndef __CandyKadze__CKBackNode__
#define __CandyKadze__CKBackNode__

#include <iostream>
#include <cocos2d.h>
#include <list.h>
#include "CK.h"
#include "CKHelper.h"
#include "CKSprite.h"
#include "CKFileInfo.h"

class CKLoaderGUI;
class CKBaseNode;
using namespace cocos2d;

    enum
    {
        IsActive =      0x000000000001,
        IsEnabled =     0x000000000010,
        IsVisible =     0x000000000100,
        IsAnimation =   0x000000001000,
        OpMove =        0x000000010000,
        OpTouch =       0x000000100000,
        OpAccelerator = 0x000001000000,
        OpAction =      0x000010000000,
        OpJump =        0x000100000000,
        OpTexMove =     0x001000000000
    };

class CKObject
{
private:
    static int count;
protected:
    struct AnimObject
    {
        CCPoint move;
        CCPoint texture_move;
        float jumping;
        bool b_jump;
        bool b_rotate;
        int amplitude_rotate;
        bool b_texture_move;
        AnimObject(){
            b_texture_move = false;
            b_jump = false;
            b_rotate = false;
            jumping = 0.0;
        }
    };
    struct LabelObject
    {
        bool bmfont;
        CCTextAlignment alig_label;
        float label_size;
        std::string base_string;
    };
    struct SlideObject
    {
        CCPoint slide_limit;
        bool vertical;
    };
    struct ScrollObject
    {
        CCPoint slide_sensivity;
        int scroll_num_window;
        float slide_percent;
    };
    
    LabelObject* label_object;
    SlideObject* slider_object;
    ScrollObject* m_scroll_object;
    
    CCNode* m_node;

    void* m_user_data;
    CKObject *m_parent;
    CCDictionary *m_dict;
    
    int count_child;
    
    
    CCSize win_size;
    CCSize size;
    
    
    AnimObject *anim;
    
    CCPoint scale;
    CCPoint skew;
    
    CCPoint start_pos,start_scale,start_skew;

    bool is_enabled;

    bool is_render_label;
    
    int tag;
    int m_state;
    int operation_state;
    int z_order;
    
    int func_value;
    int curent_state_image;
    unsigned int type;
    
    float start_rotation;
    
    std::string func_name;
    
//    std::list<CKObject* >::iterator it;
//    std::list<CKObject* > list_asp;
    typedef std::list<CKObject* > type_collect;
    type_collect list_asp;
    type_collect::iterator it;
    
    std::map<int ,std::string > map_file;
   
    void reloadText();
    
    static CCNode* createLabel(LabelObject *lb,const std::string &_font,const CCSize &_size,CCDictionary *_param = NULL);
    static CCNode* createPicture(const std::string &_file,CCDictionary *_param = NULL);
    //static CCNode* createButton(const std::string &_file,CCDictionary *_param = NULL);
    //CCNode* createSelector(CCDictionary *_param = NULL);
    //CCNode* createRadioButton(CCDictionary *_param = NULL);
    static CCNode* createScrollView( ScrollObject* so,const std::string &_file,CCDictionary *_param = NULL);
   //static CCNode* createSlot(const std::string &_file,CCDictionary *_param = NULL);
   // static CCNode* createProgressBar(const std::string &_file,CCDictionary *_param = NULL);
    static CCNode* createSlider(SlideObject *sl,const std::string &_file,CCDictionary *_param = NULL);
    
    void createGradStroke(CCLabelTTF* _label ,float _size,ccColor3B _cor,const char * _grad_name,CCRenderTexture* rt2);
    void createCopy(CCDictionary *_dict,int _tag,int count = 1);
    void createNode();
    void loadAnimation(CCDictionary *_anim);
    void loadChild(CCDictionary *_child);
    static void loadParametrs(CKObject *_obj,CCDictionary *_child);
public:
    friend CKGUI;
    friend CKBaseNode;
    CKObject();
    virtual ~CKObject();
    
    bool isTouch();
    bool isAccelerator();
    bool isAction();
    void copyObject(CKObject *_object,int _tag);
    int getStateImage() const;
    const std::string &getName();
    int getType();
    int getzOrder();
    float getLong();
    void attachChild(CKObject* _object);
    void setText(const std::string &_text);
    const char* getText();
    void setProgress(const float &_pr);
    float getProgress();
    const float& getSlidePersent();
    void savePosition(const CCPoint &_po);
    CCRect getChildZoneRect();
    
    const std::string& getFunctionName();
    void setFunctionName(const std::string& _str);
    int getTag();
    int* getValue();
    void setValue(int value);
    
    void setVisible(bool _v);
    bool isVisible();
    CCRect rect();
    CKObject* objectForTouch(const CCPoint &_p);
    
    CKObject* getParent();
    CKObject* clone();
    CKObject* getTouchObject(const CCPoint &_pos);
    CKObject* getTouchObject2(const CCPoint &_pos);
    CKObject* getChildByTag(const int &_tag);
    CKObject* getChildByType(int _type);
    CKObject* next();
    CCNode* getNode();
    void setNode(CCNode* _node);
    CCPoint getPos();
  //  bool pointInObject(const CCPoint &p);
    void setUserData(void *_data);
    void* getUserData();
    void setEnabled(bool _b = true);
    void setTouch(bool _b = true);
    //void setVisibleAll(bool _vis = false);
    bool isEnabled();
    
    bool isPointInObject(const CCPoint &_p);
    void setSize(const CCSize &_size);
    const CCSize& getSize();
    void setPosition(const float &_x,const float &_y);
    void moveAllChildren(const float &_x,const float &_y);
    virtual void loadWithDict(CCDictionary *_dict);
    static CKObject* createWithDict(CCDictionary *_dict,const CCSize &_size,CCNode *_parent);
    void setStateImage(int _state = 0);
    void setStateChild(int _state = 0);
    void update(const CCPoint &_point);
    void setSlideSensivity(const CCPoint &_point);
    void moveX(const float &_x);
    void moveY(const float &_y);
    void onClick();
    //void move(const float &_x,const float &_y);
};


class CKBaseNode
{
protected:
    std::list<CKObject* >::iterator it;
    std::list<CKObject* > list_asp;
    std::list<std::string> list_atlas;
    std::list<std::string> list_texture_name;
    bool is_create;
    //bool is_visible;
    CCDictionary *m_dict;
    CCNode* m_node;
    std::string name;
public:
    CKBaseNode();
    void reloadText();
    void reloadAtlas();
    void removeAtlasPlist();
    CKObject* getTouchObject(const CCPoint &_pos);
    CKObject* getTouchObject2(const CCPoint &_pos);
    CKObject* getChildByTag(const int &_tag);
    CKObject* getChildByType(int _type);
    virtual void load(const char * _name);
    virtual void create(CCNode *);
    static std::string getAtlasTextureName(const char * _name);
    //bool isVisible();
    //void setVisible(bool _v);
    CCNode *get();
    virtual void update(const float &_dt);
    void setPosition(const CCPoint& pos);
    virtual ~CKBaseNode();
};
#endif /* defined(__CandyKadze__CKBackNode__) */
