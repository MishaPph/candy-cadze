//
//  CKLoaderGUI.h
//  CandyKadze
//
//  Created by PR1.Stigol on 06.12.12.
//
//

#ifndef __CandyKadze__CKLoaderGUI__
#define __CandyKadze__CKLoaderGUI__

#include <iostream>
#include <cocos2d.h>
#include "CKBackNode.h"

using namespace cocos2d;
class CKBaseNode;
class CKObjectGUI: public CKObject
{
public:
    
};

struct CKScrollObject
{
    CKObject *object;
    CKObject *slider;
    int min;
    int max;
    float sizeFrameY,sizeFrameX;
    int count_object;
    void updateSlider()
    {
        float pos = object->getPos().y/sizeFrameY;
        if(pos > max)
            pos = max;
        if(pos < min)
            pos = min;
        pos -= min;
        pos /=(max - min);
        slider->setProgress(1.0 - pos);
    }
    void showSlider()
    {
        slider->getNode()->stopAllActions();
        slider->getChildByTag(1)->getNode()->runAction(CCFadeIn::create(0.2));
        slider->getChildByTag(2)->getNode()->runAction(CCFadeIn::create(0.2));
        slider->getChildByTag(3)->getNode()->runAction(CCFadeIn::create(0.3));
        slider->getNode()->runAction(CCFadeIn::create(0.2));
    };
    void hideSlider()
    {
        slider->getNode()->stopAllActions();
        slider->getChildByTag(1)->getNode()->runAction(CCFadeOut::create(0.2));
        slider->getChildByTag(2)->getNode()->runAction(CCFadeOut::create(0.2));
        slider->getChildByTag(3)->getNode()->runAction(CCFadeOut::create(0.3));
        slider->getNode()->runAction(CCFadeOut::create(0.2));
    };
};

class CKGUI:public CKBaseNode
{
private:
    CCObject*  m_listener;
    std::map<std::string, SEL_CallFuncND> func_map;
    CKObject* curent_node,*scroll_object;
    int curent_scroll_frame;
    CCPoint last_pos;
    
    int curent_state;
    bool touch_enabled;
    bool scroll_move;
//    CKScrollObject *m_scroll;
public:
    CKGUI();
    virtual ~CKGUI();
    int getNumberScroll();
    void setScroll(CKObject *_rec,int _count,int _min,int _max);
//    CKScrollObject *getScroll();
    void setMovedScroll(bool _true);
    float getScrollValue();
    void setActiveScrollFrame(int _frame = 0,bool _animation = true);
    void setTarget(CCObject *rec);
    void setSelector(SEL_MenuHandler _selector);
    void addFuncToMap(const std::string& _name,SEL_CallFuncND _func_sel);
    bool setTouchBegan(const CCPoint &_pos);
    void setTouchMove(const CCPoint &_pos);
    void setTouchEnd(const CCPoint &_pos);
    void setTouchEnabled(bool _touch);
    void runFuncObject(CKObject *_obj);
   // void setVisible(bool _visible);
    int getScrollFrame(const CCPoint &_pos);
    CKObject *getCurrnet();
    bool isTouchEnabled();
};

#endif /* defined(__CandyKadze__CKLoaderGUI__) */
