//
//  CKBaseBomb.h
//  CandyKadze
//
//  Created by PR1.Stigol on 17.10.12.
//
//

#ifndef __CandyKadze__CKBaseBomb__
#define __CandyKadze__CKBaseBomb__


#include <iostream>
#include <list.h>
#include "cocos2d.h"
#include "CK.h"
#include "CKHelper.h"
#include "CKAudioMgr.h"


using namespace CK;
using namespace cocos2d;

class CKBombBase {
protected:
    float speed;
    float line_velosity;
    int ground_height;
    int panel_left;
    int height;
    bool is_spectrum;
    int BOX_SIZE;
    CCSize win_size;
    std::string filename;
    CCPoint last_pos_bomb;
    CCLayer* m_layer;
    unsigned int count_create_bomb,count_hit_bomb,count_fail_bomb;
public:
    CKBombBase();
    virtual ~CKBombBase();
    const CCPoint& getPosition();
    void setSpeed(float _s);
    float getSpeed();
    void setHeight(const int &_h);
    void setLayer(CCLayer *_layer);
    void setBombName(const std::string &_str);
    int getCountCreateBomb();
    int getCountHitBomb();
    int getCountFailBomb();

    virtual bool create(const CCPoint& _p,const unsigned char &_id = 0) = 0;
    virtual const int update(const float &_dt) = 0;
    virtual void deleteBomb(int _t) = 0;
    virtual bool init() = 0;
};

class CKBomb90:public CKBombBase
{
private:
    CCSprite * last_bomb;
    CCSprite* m_bomb;
    CCParticleSystemQuad* m_emitter;
    list<pair<float, CCSprite*> > list_bomb;
    list<pair<float, CCSprite*> >::iterator it;
    int curent_accuracy;
public:
    CKBomb90();
    int getAccuracy()const {return curent_accuracy;};
    virtual ~CKBomb90(){};
    virtual bool create(const CCPoint& _p,const unsigned char &_id = 0);
    virtual const int update(const float &_dt);
    virtual void deleteBomb(int _t);
    virtual bool init();
    bool canCreateBomb();
    CCSprite* getCurentBomb();
};

#endif /* defined(__CandyKadze__CKBaseBomb__) */
