//
//  CKAirplaneBase.h
//  CandyKadze
//
//  Created by PR1.Stigol on 21.05.13.
//
//

#ifndef __CandyKadze__CKAirplaneBase__
#define __CandyKadze__CKAirplaneBase__

#include <iostream>
#include <iostream>
#include "cocos2d.h"
#include "CKCubeArray.h"
#include "CKBomb.h"

using namespace cocos2d;

#define COUNT_FRAME_PREUPDATE 10
class CKAirplaneBase :public CCObject
{
protected:
    SEL_CallFuncND m_selector;
    float speed,start_speed;
    float curent_height;
    float alig_fps;
    bool is_ipad;
    int step;
    int count_loop;
    int BOX_SIZE;
    int sound_id;
    int count_diff;
    int diff;
    int max_value,low_value;
    int count_destroy;
    int airplane_height;
    int airlpane_start_height;
    int panel_left;
    bool slow_fall;
    float base_speed;
    float speed_step;
    bool wait_create_bomb,wait_new_loop;
    float wait_position_x,wait_create_pos_x;
    
    int wait_type;
    float pos_x,pos_y;
    int ground_height,ground_air;
    bool b_crash_anim,b_win_anim,win_anim,crash_anim;
    bool is_emulation_fly;
    bool double_speed,is_double_speed;
 //   bool double_speedw,is_double_speedw;
    
    unsigned char end_game;
    CCSize win_size;
    float start_pos_x,end_pos_x;
    
    CCActionInterval* action_fly;
    CCNode* m_layer;
    CCSprite* m_sprite;
    CCParticleSystem* m_emitter,*m_emitter_splash;
    CKCubeArrayBase* m_array;
    
    void createBaner();
    
    void showBaner();
    void initStartHeight();
    
    void callFuncGame(CCNode *_sender, void *data);
    virtual int winUpdate(const float &dt) = 0;

    virtual int gameOverUpdete() = 0;
    virtual void checkCrash() = 0;
    virtual void reset() = 0;
    virtual void updatePosition(const float &dt);
    void calcSpeedStep(const float &dt);
    virtual void setBombs(CKBombBase *_bomb) = 0;
    virtual void updateDoubleSpeed() = 0;
    virtual bool createBomb(unsigned char _type = 1) = 0;
    virtual void updateWaitBomb();
    void calcDoubleSpeed();
public:
    float getStartSpeed();
    int getStartHeight();
    bool isWaitCreateBomb();
    void setDoubleSpeed(bool _enable);
    bool isDoubleSpeed();
    virtual bool createWinAnimation() = 0;
    virtual bool createCrashAnimation() = 0;
    virtual bool touch(CCPoint _pos,unsigned char _type = 1) = 0;
    CKAirplaneBase();
    virtual ~CKAirplaneBase();
    CCSize getSize();
    void setStartPositionX(float _start);
    void setEndPositionX(float _end);

    void setSpeed(float);
    void setBaseSpeed(float);
    float getSpeed();
    void setSelector(SEL_CallFuncND _selector)
    {
        m_selector = _selector;
    };
    const unsigned char &getTypeEndFly() const {
        return end_game;
    }
    void setEmulationFly(bool _e);
    void setCurentPositionX(float _pos);
    CCSprite* getSprite();
    int getBonusFinish();
    void setLayer(CCNode* _layer);
    void setGroundHeight(float _h);
    void setCurentHeight(float _h);
    void setCubeArray(CKCubeArrayBase* _array);
    const CCPoint& getPosition();
    void setSlowFall(int _low, int _min,int _max,bool _slf = false);
    bool endAnimation();
    void moveToNewLoop();
    int getAirplaneHeight();
    virtual CCSprite* create(const char* _name) = 0;
    virtual const unsigned char update(const float &dt) = 0;
    
    virtual void pause() = 0;
    virtual void resume() = 0;
};
#endif /* defined(__CandyKadze__CKAirplaneBase__) */
