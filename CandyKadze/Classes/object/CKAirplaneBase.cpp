//
//  CKAirplaneBase.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 21.05.13.
//
//

#include "CKAirplaneBase.h"
#define IPAD_HEIGHT 584
#define IPHONE_HEIGHT 252

#pragma mark  - BASE
CKAirplaneBase::~CKAirplaneBase()
{
    CCLOG("CKAirplaneBase::~CKAirplaneBase");
    release();
};

void CKAirplaneBase::initStartHeight()
{
    airplane_height = START_AIRPLANE_HEIGHT;
    //load cube map with plist
    CCDictionary *dict = CCDictionary::createWithContentsOfFile("levels.plist");
    if(dict->objectForKey("difficult"))
    {
        
        CCDictionary *diff  = (CCDictionary *)dict->objectForKey("difficult");
        if(CKHelper::Instance().getWorldNumber() != 0)
        {
            CCArray* ar_level = (CCArray*)diff->objectForKey(CK::c_difficult[CKHelper::Instance().lvl_speed]);
            CCObject* pObj_l = NULL;
            int lvl = 0;
            CCARRAY_FOREACH(ar_level, pObj_l)
            {
                if(lvl == CKHelper::Instance().lvl_number)//load first list
                {
                    CCDictionary *dict_lvl = (CCDictionary*)pObj_l;
                    if(dict_lvl->objectForKey("plane_altitude"))
                    {
                        airplane_height  = dictInt(dict_lvl, "plane_altitude");
                    }
                }
                lvl++;
            }
        }
        else
        {
            CCArray* ar_level = (CCArray*)diff->objectForKey("90x");
            CCObject* pObj_l = NULL;
            int lvl = 0;
            CCARRAY_FOREACH(ar_level, pObj_l)
            {
                if(lvl == CKHelper::Instance().lvl_number)//load first list
                {
                    CCDictionary *dict_lvl = (CCDictionary*)pObj_l;
                    if(dict_lvl->objectForKey("plane_altitude"))
                    {
                        airplane_height  = dictInt(dict_lvl, "plane_altitude");
                        speed = dictFloat(dict_lvl, "plane_speed");
                    }
                }
                lvl++;
            }
        }
    }
    
    dict->release();
    // CCLOG("CKAirplaneBase:: airplane_height %d",airplane_height);
};
int CKAirplaneBase::getStartHeight()
{
    return airlpane_start_height;
}

CKAirplaneBase::CKAirplaneBase()
{
    CCLOG("CKAirplaneBase::CKAirplaneBase()");
    m_selector = NULL;
    speed = 100;
    end_game = 0;
    is_emulation_fly = false;
    m_emitter_splash = NULL;
    
    is_double_speed = false;
    
    wait_create_bomb = false;
    
    win_size = CCDirector::sharedDirector()->getWinSize();
    panel_left = 0;
    initStartHeight();
    airlpane_start_height = airplane_height;
    
    if(win_size.width != 1024)
    {
        BOX_SIZE = BOX_SIZE_IPHONE;
        step = BOX_SIZE/4;
        diff = 2;
        curent_height = step*START_AIRPLANE_HEIGHT - 16;
        is_ipad = false;
       
        if(win_size.width == 568)
        {
            panel_left = 44;
        }
    }
    else
    {
        diff = -1;
        is_ipad = true;
        BOX_SIZE = BOX_SIZE_IPAD;
        step = BOX_SIZE/4;
        curent_height = step*START_AIRPLANE_HEIGHT + 50;
    };
    
    count_diff = 8;
    
    int n = START_AIRPLANE_HEIGHT;
    
    while (n > airplane_height) {
        if(count_diff > 0)
        {
            curent_height += diff;
            count_diff--;
        }
        curent_height -= step;
        n--;
    };
    
    m_emitter = NULL;
    win_anim = false;
    crash_anim = false;
    if(CKHelper::Instance().getWorldNumber() != 0)
    {
        //   float k = 0.05;//для підбору швидкості
        //   float start = CKHelper::Instance().start_airplane_speed;
        switch (CKHelper::Instance().lvl_speed ) {
            case 0:
                speed = (3.5 + CKHelper::Instance().lvl_number*0.15)*BOX_SIZE;
                break;
            case 1:
                speed = (5.1 + CKHelper::Instance().lvl_number*0.05)*BOX_SIZE;
                break;
            case 2:
                speed = (5.9 + CKHelper::Instance().lvl_number*0.05)*BOX_SIZE;
                break;
            case 3:
                speed = (6.7 + CKHelper::Instance().lvl_number*0.1)*BOX_SIZE;
                break;
            default:
                break;
        };
    }
    else
    {
        speed *= BOX_SIZE;
    };
    
    start_speed = speed/BOX_SIZE;
    alig_fps = 0.01666666*COUNT_FRAME_PREUPDATE;
    retain();
};
float CKAirplaneBase::getStartSpeed()
{
    return start_speed;
};

void CKAirplaneBase::setSlowFall(int _low, int _min,int _max,bool _slf)
{
    low_value = _low;count_destroy = _min;max_value = _max;slow_fall = _slf;
};
bool CKAirplaneBase::isWaitCreateBomb()
{
    return wait_create_bomb;
}
void CKAirplaneBase::setDoubleSpeed(bool _enable)
{
    double_speed = _enable;
    if(_enable)
        is_double_speed = true;
};
bool CKAirplaneBase::isDoubleSpeed()
{
    return is_double_speed;
};

void CKAirplaneBase::callFuncGame(CCNode *_sender, void *data)
{
    if(m_selector)
        (m_layer->*m_selector)(_sender,data);
};

int CKAirplaneBase::getAirplaneHeight()
{
    //CCLOG("getAirplaneHeight:: airplane_height %d",airplane_height);
    if(airplane_height > START_AIRPLANE_HEIGHT)
        return START_AIRPLANE_HEIGHT;
    else
        return airplane_height;
};

void CKAirplaneBase::setLayer(cocos2d::CCNode *_layer)
{
    m_layer = _layer;
};

CCSprite* CKAirplaneBase::getSprite()
{
    return m_sprite;
};

float CKAirplaneBase::getSpeed()
{
    return speed;
};
void CKAirplaneBase::setCurentPositionX(float _pos)
{
    pos_x = _pos;
}
void CKAirplaneBase::setSpeed(float _speed)
{
    speed = _speed;
    
};
void CKAirplaneBase::setBaseSpeed(float _speed)
{
    base_speed = _speed;
}
void CKAirplaneBase::setGroundHeight(float _h)
{
    ground_height = _h;
    ground_air = _h + m_sprite->getContentSize().height/2;
};
//need for tutorial
void CKAirplaneBase::setCurentHeight(float _h)
{
    curent_height = _h;
};
void CKAirplaneBase::setEmulationFly(bool _e)
{
    is_emulation_fly = _e;
}
void CKAirplaneBase::setCubeArray(CKCubeArrayBase* _array)
{
    m_array = _array;
};

const CCPoint& CKAirplaneBase::getPosition()
{
    return m_sprite->getPosition();
};

CCSize CKAirplaneBase::getSize()
{
    return m_sprite->getContentSize();
};

int CKAirplaneBase::getBonusFinish()//bonus airplane height
{
    //CCLOG("CKAirplaneBase::getBonusFinish curent_height %f",curent_height);
    return ((curent_height - ground_height/* - step*2*/)/(BOX_SIZE/4) - 1);
};

bool CKAirplaneBase::endAnimation()
{
    return end_game;
};

void CKAirplaneBase::setStartPositionX(float _start)
{
    start_pos_x = _start;
};

void CKAirplaneBase::setEndPositionX(float _end)
{
    end_pos_x = _end;
};
void CKAirplaneBase::moveToNewLoop()
{
    if(CKFileOptions::Instance().isKidsModeEnabled())
    {
        // CCLOG("curent_height %f %f",curent_height,m_array->getMaxHeightColumn()*BOX_SIZE + BOX_SIZE*2.0 + ground_height);
        if((m_array->getMaxHeightColumn()*BOX_SIZE + BOX_SIZE*2.0 + ground_height) < curent_height)
        {
            curent_height = curent_height - step;
            if(count_diff > 0)
            {
                curent_height += diff;
                count_diff--;
            };
        }
    }
    else
    {
        
        if(m_array && (curent_height - (m_array->getMaxHeightColumn()*BOX_SIZE + ground_height)) < step*3)
        {
            CKAudioMng::Instance().playEffect("atention_cube");
        };
        curent_height = curent_height - step;
        if(count_diff > 0)
        {
            curent_height += diff;
            count_diff--;
        };
    }
};
void CKAirplaneBase::calcDoubleSpeed()
{
    if(double_speed)
    {
        if(base_speed*3.5 > speed)
        {
            speed *= 1.05;
            
            CKAudioMng::Instance().setEffectSpeed("plane_fly",speed/base_speed);
        };
    }
    else
    {
        speed *= 0.94;
        
        if(speed < base_speed)
        {
            CKAudioMng::Instance().setEffectSpeed("plane_fly",1.0);
            is_double_speed = false;
            speed = base_speed;
        }
        else
        {
            CKAudioMng::Instance().setEffectSpeed("plane_fly",speed/base_speed);
        }
    };
};

void CKAirplaneBase::calcSpeedStep(const float &dt)
{
    float fps = alig_fps/COUNT_FRAME_PREUPDATE;
    alig_fps = alig_fps - fps + dt;
    speed_step = speed*fps;
};

void CKAirplaneBase::updatePosition(const float &dt)
{

    pos_x += speed_step;
    
    m_sprite->setPosition(ccp(pos_x,curent_height));
    if(m_sprite->getPositionX() >= end_pos_x)
    {
        pos_x = start_pos_x;
        wait_new_loop = false;
        m_sprite->setPosition(ccp(pos_x,curent_height));
        
        if(slow_fall)
        {
            if(m_array && m_array->getCountDestroyCube() >= count_destroy)
            {
                CKHelper::Instance().count_hit_for_loop++;
                if(CKHelper::Instance().count_hit_for_loop > max_value)
                    CKHelper::Instance().count_hit_for_loop = max_value;
            }
            else
            {
                CKHelper::Instance().count_hit_for_loop--;
                if(CKHelper::Instance().count_hit_for_loop < low_value)
                    CKHelper::Instance().count_hit_for_loop = max_value;
            };
            if(m_array)
                m_array->setCountDestroyCube(0);
            
            if(CKHelper::Instance().count_hit_for_loop > 0)
            {
                moveToNewLoop();
            };
        }
        else //tutorial layer
        {
            moveToNewLoop();
        };
        
        if(m_emitter_splash)
            m_emitter_splash->stopSystem();
    };
};

void CKAirplaneBase::updateWaitBomb()
{
    if(wait_create_bomb && !wait_new_loop)
    {
        float plane_pos = m_sprite->getPosition().x + m_sprite->getContentSize().width/4;
        if(double_speed && (plane_pos >= (wait_position_x - BOX_SIZE*2)))
        {
            setDoubleSpeed(false);
        };
        
        if(plane_pos >= wait_position_x )
        {
            setDoubleSpeed(false);
            wait_create_bomb = false;
            createBomb(wait_type);
        };
    };
};