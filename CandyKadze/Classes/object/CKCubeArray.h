//
//  CKCubeArray.h
//  CandyKadze
//
//  Created by PR1.Stigol on 20.09.12.
//
//

#ifndef __CandyKadze__CKCubeArray__
#define __CandyKadze__CKCubeArray__

#include <iostream>
#include <map.h>
#include <vector.h>
#include "CK.h"
#include "cocos2d.h"
#include <list.h>
#include "CKHelper.h"
#include "CKBonus.h"
#include "CKBaseBomb.h"
#include "CKBomb.h"

//#define BATCH_NODE

using namespace cocos2d;
using namespace CK;

class CKCubeArrayBase
{
protected:
    const float TIME_MOVE_CUBE_DOWN = 0.3;
    const unsigned char BASE_COUNT_COLUNMS = 14;
    
    map< int, std::string> cube_map;
    std::vector<CCParticleSystem*>  emitter_list1;
    std::vector<CCParticleSystem*>::iterator  emitter_it;
    std::vector<int> cube_type;
    std::vector<unsigned char> v_temp;
    std::vector<std::string> mask_list;
    
    unsigned char large_map[COUNT_ROWS*2][COUNT_COLUMNS*2];
    unsigned char small_map[COUNT_ROWS][COUNT_COLUMNS];
    
    CCLabelTTF* label_info;
    CCSprite* sprite_min_map[COUNT_ROWS*2][COUNT_COLUMNS*2];
    CCNode* m_layer;
    CCSpriteBatchNode* cube_batch;
    
    std::string batch_file_name;
    GLuint batch_texture_name;

    
    int destroy_sound_id;
    int count_coll,count_row,panel_size,ground_height;
    int score;
    int count_active_cube;
    int count_destroy_cube;
    int count_cube;
    int full_cube;
    int BOX_SIZE;
    int base_height;
    
    bool is_ipad;
    bool is_restart;

    void genCube(int _type,int _count);
    void createColumn(unsigned char _x,unsigned char _h = 1,int type = 0);
    void genColumns(unsigned char _column_h = 1,unsigned char _need = 1);
    void resizeColumn(unsigned char _height = 1,unsigned char _need = 1);
    void checkMiniCube(int _x,int _y);
    void copyCubeTominiCube();
    virtual void createMap(int size_box,int count_col,int count_row);
    virtual void createMiniCube(CCSprite* _sprite,int _x,int _y,int _tag = 0);
    void hideNode(CCNode *_sender);
    virtual void createCubeWithCCArray(CCArray *_array){};
    virtual void reinitCubeWithCCArray(CCArray *_array){};
    void addBlock(int _column);
    void diffBlock(int _column);
    virtual void createEmitterPool(int count = 16);
    
public:
    int getColumnHeight(int _column);
    int getColumnHeight(float _pos);
    bool isTopCube(const CCPoint &_pos);
    bool isTopMiniCube(const CCPoint &_pos);
    int getMaxHeightColumn();
    const int getCountDestroyCube() const
    {
        return count_destroy_cube;
    };
    void setCountDestroyCube(const int &_count)
    {
        count_destroy_cube = _count;
    };
    unsigned char getBigCube(const int &_x,const int &_y) const;
    unsigned char getMiniCube(const int &_x,const int &_y) const;
    int getGroundHeight()const;
    virtual void restart();
    CCNode * createBatch(const char* _filename);
    virtual bool destroyMiniCub(const unsigned char &_x,const unsigned char &_y,const unsigned char _effect = 0);
    bool destroyMiniCub2(int _x,int _y);
    virtual int checkCollision(CCSprite* _bomb);
    virtual int checkAirplane(const CCPoint &_fan,const CCPoint &_bott);
    virtual void createLevel(int _lvl_diff,int _lvl_num);
    int getCountActiveCube();
    int getCountCube();
    void setPanelSize(int _s);
    void setGroundHeight(int _h);
    void setLayer(CCNode *);
    void parsingLevel(const std::string &_str);
    int getScore();
    
    CKCubeArrayBase();
    virtual ~CKCubeArrayBase();
};

class CKCubeArray : public CKCubeArrayBase
{

    bool is_bonus,is_freez_destroy;
    int destroy_cube;
    
    CCRenderTexture *rt;
    CKBonus *m_bonus;
    CCSprite* splash_sprite;
    
    std::list<CCRenderTexture*>  rt_list;
    std::map<int, CCTexture2D*> render_texture_map;
    std::map<int, CCTexture2D*> render_texture_map2;
    std::map<int, CCTexture2D*>::iterator it_map;
    
    std::vector<pair<unsigned char,unsigned char> > destroy_list;
    std::vector<pair<unsigned char,unsigned char> >::iterator destroy_it,jump_it;
    std::vector<pair<unsigned char,unsigned char> > jump_list;
    std::vector<CCParticleSystem*>  emitter_list2;
    
#ifdef BATCH_NODE
    CCSpriteBatchNode* sprite_min_map[COUNT_ROWS*2][COUNT_COLUMNS*2];
#endif
    
    CCTexture2D* createMiniTexture(CCSprite* _base,const CCRect&,int _tag = 0 );
    virtual void createMap(int size_box,int count_col,int count_row);
    virtual void createMiniCube(CCSprite* _sprite,int _x,int _y,int _tag = 0);

    
    void createRTT(CCSprite* _base,int _tag = 0);
    virtual void createEmitterPool(int count = 16);
    void createMiniCubeBatch(const std::string &_name,int _x,int _y,int _tag);
    
    void createEffectDectroyCube(const CCPoint &_pos,const int &_tag,const unsigned char _effect);
    void moveCubeToDown(const unsigned char _x,const unsigned char _y);
    void moveHalfCube(const unsigned char _x,const unsigned char _y,unsigned char _right = 0);
    void checkMove(const unsigned char _x,const unsigned char _y);


    void addMaskOnCube(const unsigned char _x,const unsigned char _y);
    void showMaskCube(const unsigned char _x,const unsigned char _y,bool _b = false);
    
public:
    CCSprite* getRndVisibleMiniCube();
    void addJumpCube(const unsigned char _x,const unsigned char _y);
    void jumpCube();
    bool destroyCube(const unsigned char &_x,const unsigned char &_y,const unsigned char _effect = 0);
    void pause();
    void resume();
    int getDestroyCube() const
    {
        return destroy_cube;
    };
    virtual void restart();
    virtual bool destroyMiniCub(const unsigned char &_x,const unsigned char &_y,const unsigned char _effect = 0);
    virtual void createLevel(int lvl_dif,int _lvl_num);
    virtual void createCubeWithCCArray(CCArray *_array);
    virtual void reinitCubeWithCCArray(CCArray *_array);
    virtual int checkCollision(CCSprite* _bomb);
    virtual int checkAirplane(const CCPoint &_fan,const CCPoint &_bott);
    void setBonus(CKBonus *_bonus);
    void setFreezDestroyCount(bool _b = false);
    const int &getColumnBaseHeight() const;
    bool updateAfterDestroy();
    int getHeightFirstCollision(const CCPoint &_pos,int _height,int _width,bool big_cube = false);
    std::list<CCNode*> bonus_list;
    bool isBonus();
    CKCubeArray();
    virtual ~CKCubeArray();
};

#endif /* defined(__CandyKadze__CKCubeArray__) */
