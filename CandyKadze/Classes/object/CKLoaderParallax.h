//
//  CKLoaderParallax.h
//  CandyKadze
//
//  Created by PR1.Stigol on 06.12.12.
//
//

#ifndef __CandyKadze__CKLoaderParallax__
#define __CandyKadze__CKLoaderParallax__

#include <iostream>
#include "CKBackNode.h"
class CKObjectParallax: public CKObject
{
public:
    void runAction(const CCPoint &_pos);
    //void update(const CCPoint &_point);
};

class CKParalaxNode:public CKBaseNode
{
    CCPoint last_accel;
    CCPoint diff_accel,direct;
    CCPoint update_accel;
public:
    virtual void update(const float &_dt);
    void didAccelerate(CCAcceleration* pAccelerationValue);
    void runAction(const CCPoint &_pos);
    virtual ~CKParalaxNode();
};

#endif /* defined(__CandyKadze__CKLoaderParallax__) */
