//
//  CKBaseBomb.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 17.10.12.
//
//

#include "CKBaseBomb.h"

#pragma mark - BASE

CKBombBase::CKBombBase()
{
    panel_left = 0;
    if(CCDirector::sharedDirector()->getWinSize().width == 568)
    {
        panel_left = 44;
    };
    BOX_SIZE = CKHelper::Instance().BOX_SIZE;
    count_create_bomb = 0;
    count_fail_bomb = 0;
    count_hit_bomb = 0;
    line_velosity = 0.1*(CKHelper::Instance().lvl_speed + 1);
};

void CKBombBase::setBombName(const std::string &_str)
{
    filename = _str;
};

int CKBombBase::getCountCreateBomb()
{
    CCLog("getCountCreateBomb %d",count_create_bomb);
    return count_create_bomb;
};

int CKBombBase::getCountHitBomb()
{
    return count_hit_bomb;
};

int CKBombBase::getCountFailBomb()
{
    return count_fail_bomb;
};

//set start speed for bomb
void CKBombBase::setSpeed(float _s)
{
    speed = _s;
};

float CKBombBase::getSpeed()
{
    return speed;
};

const CCPoint& CKBombBase::getPosition()
{
    return last_pos_bomb;
};

void CKBombBase::setLayer(CCLayer *_layer)
{
    m_layer = _layer;
};

void CKBombBase::setHeight(const int &_h)
{
    ground_height = _h;
};

CKBombBase::~CKBombBase()
{
    
};

#pragma mark - BOMB90
CKBomb90::CKBomb90()
{
    BOX_SIZE = CKHelper::Instance().BOX_SIZE;
    list_bomb.clear();
    last_bomb = NULL;
    m_bomb = NULL;
    m_emitter = NULL;
    curent_accuracy = 1;

};
bool CKBomb90::init()
{
    m_emitter = CCParticleSystemQuad::create("bomb_destroy.plist");
    m_layer->addChild(m_emitter ,zOrderParticle);
    if(!CKHelper::Instance().getIsIpad())
        m_emitter->setScale(0.5);
    m_emitter->stopSystem();
    m_bomb = CCSprite::createWithSpriteFrameName("bomb_90.png");// (loadImage(filename.c_str()).c_str());
    m_bomb->setVisible(false);
    m_layer->addChild(m_bomb,CK::zOrder_Cube);
    return true;
};

CCSprite* CKBomb90::getCurentBomb()
{
    return last_bomb;
}
bool CKBomb90::canCreateBomb()
{
    return list_bomb.empty();
};

bool CKBomb90::create(const CCPoint& _p,const unsigned char &_id)
{
    if(!canCreateBomb())
    {
        return false;
    };
    
    count_create_bomb++;

    CKAudioMng::Instance().playEffect("launch");
    
    CCAssert(m_bomb != NULL, "Error::need init bomb sprite");
    m_bomb->setPosition(_p);
    m_bomb->setVisible(true);
    
    list_bomb.push_back(pair<int,CCSprite*>(0,m_bomb));
    last_bomb = m_bomb;
    return true;
};

void CKBomb90::deleteBomb(int _t)
{
    if(_t == 0)
    {
        list_bomb.pop_back();
        last_bomb->setVisible(false);
        CKAudioMng::Instance().playEffect("hit_block");
        last_bomb = NULL;
        curent_accuracy++;
        count_hit_bomb++;
    }
    else
        if(_t == 1)
        {
            curent_accuracy = 1;
            CKAudioMng::Instance().playEffect("hit_ground");

            CCAssert(m_emitter != NULL, "Error:: need init particle");

            m_emitter->setPosition(ccp(last_bomb->getPosition().x,ground_height));
            m_emitter->resetSystem();
            
            count_fail_bomb++;
            list_bomb.pop_back();
            last_bomb->setVisible(false);
            last_bomb = NULL;
        };
};

const int CKBomb90::update(const float &_dt)
{
    last_bomb = NULL;
    if(!list_bomb.empty())
    {
        for (it = list_bomb.begin(); it != list_bomb.end(); it++)
        {
            if(it->second == NULL)
                continue;
            last_bomb = it->second;
            if(!last_bomb)
                continue;
            it->first += line_velosity;
            float velosity = it->first;
            
            last_bomb->setPosition(ccp(last_bomb->getPositionX(),last_bomb->getPositionY() - speed*velosity*_dt));
            last_pos_bomb = last_bomb->getPosition();
        };
    }
    return 1;
};