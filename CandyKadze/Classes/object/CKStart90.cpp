//
//  CKStart90.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 04.10.12.
//
//

#include "CKStart90.h"
#include "CKHelper.h"
#include "CK.h"

//CKStart90* CKStart90::create()
//{
//    CKStart90 * pRet = new CKStart90();
//	//pRet->retain();
//	return pRet;
//};

CKStart90::CKStart90()
{
    star_list.clear();
    cloud_list.clear();
    star_name1 = "";
    star_name2 = "";
};

CKStart90::~CKStart90()
{
//    std::vector<const char *>::iterator its = start_name.begin();
//    while (its != start_name.end()) {
//        CCLOG("CKStart90::~CKStart90 %s ",*its);
//        CC_SAFE_DELETE(*its);
//        *its = NULL;
//        its++;
//    }
    for (int i = 0; i < cloud_name.size(); i++) {
        CC_SAFE_DELETE(cloud_name[i]);
        cloud_name[i] = NULL;
    };
    //start_name.clear();
    cloud_name.clear();
    CCLOG("CKStart90::~CKStart90");
    it = star_list.begin();
    while(it != star_list.end())
    {
        CC_SAFE_DELETE(*it);
        it++;
    };
    star_list.clear();
    it = cloud_list.begin();
    while(it != cloud_list.end())
    {
        CC_SAFE_DELETE(*it);
        it++;
    };
    cloud_list.clear();
};

void CKStart90::setMinHeight(const float &_height)
{
    min_height = _height;
}
void CKStart90::setLayer(CCLayer* _m_layer)
{
    m_layer  = _m_layer;
};

void CKStart90::update(const float &_dt)
{
    if(rand()%50 == 0)
    {
        addStars(win_size.width*CCRANDOM_0_1(),min_height + (win_size.height - min_height)*CCRANDOM_0_1());
    };
    CCSprite * sprite,*sprite1 ;
    it = cloud_list.begin();
    bool create_new = false;
    while(it != cloud_list.end())
    {
        sprite = (*it)->sprite;
        sprite->setPosition(ccp(sprite->getPosition().x + (*it)->speed,sprite->getPosition().y));
        it++;
        
        if(sprite->getPosition().x > win_size.width*1.2)
        {
            //addClouds(-sprite->getContentSize().width/2 - win_size.width*CCRANDOM_0_1(),min_height + (win_size.height*0.9 - min_height)*CCRANDOM_0_1() );
            create_new = true;
            sprite1 = sprite;
            (*it)->speed = speed + speed_var*CCRANDOM_MINUS1_1();
        }
    };
    
    if(create_new)
    {
        float length = 999;
        CCPoint point;
        while(int(length))
        {
            point = ccp(-sprite1->getContentSize().width/2 - win_size.width*CCRANDOM_0_1()*0.5 ,min_height + (win_size.height*0.9 - min_height)*CCRANDOM_0_1());
            it = cloud_list.begin();
            length = 999;
            while(it != cloud_list.end())
            {
                length = std::min(length,CK::getPointLength(point,(*it)->sprite->getPosition()));
                it++;
            };
            if(length > sprite1->getContentSize().width)
            {
                break;
            }
        };
        sprite1->setPosition(point);
    }
        
    return;
};

#pragma mark - STARTS METHOD
void CKStart90::addStars(const float &_x,const float &_y)
{

    CCSprite* sprite = NULL;
    it = star_list.begin();
    //find free stars in pool
    while(it != star_list.end())
    {
        if(!(*it)->sprite->getActionByTag(2))
        {
            sprite = (*it)->sprite;
            break;
        };
        it++;
    };

    if(sprite == NULL)  //if not free stars in pool then create new stars
    {
        if(star_list.size() >= max_count)
            return;
        int num = CK::ck_rand()%2;
        if(num)
        {
            sprite = CCSprite::createWithSpriteFrameName(star_name1.c_str());
        }
        else
        {
            sprite = CCSprite::createWithSpriteFrameName(star_name2.c_str());
        }
        CKElement * elem = new CKElement;
        elem->sprite = sprite;
        elem->speed = speed + speed_var*CCRANDOM_MINUS1_1();
        star_list.push_back(elem);
        m_layer->addChild(sprite,0);
    };
    
    it = star_list.begin();
    while(it != star_list.end())
    {
        if(abs((*it)->sprite->getPositionX() - _x) < win_size.width*0.02 )
            if(abs((*it)->sprite->getPositionY() - _y) < win_size.width*0.02 )
            {
                addStars(win_size.width*CCRANDOM_0_1(),min_height + (win_size.height - min_height)*CCRANDOM_0_1());
                return;
            };
        it++;
    };
    if((_x < sprite->getContentSize().width)||(_x > (win_size.width - sprite->getContentSize().width)) || _y > (win_size.height - sprite->getContentSize().height) )
    {
        addStars(win_size.width*CCRANDOM_0_1(),min_height + (win_size.height - min_height)*CCRANDOM_0_1());
        return;
    }
    sprite->setPosition(ccp(_x,_y));
    sprite->setVisible(true);
    sprite->setOpacity(0);
    CCFiniteTimeAction* f_out = CCFadeOut::create(0.5);
    CCFiniteTimeAction* f_in = CCFadeIn::create(0.5);
    CCAction *seq = CCSequence::create(CCDelayTime::create(rand()%10),f_in,CCDelayTime::create(rand()%10),f_out,NULL);
    seq->setTag(2);
    sprite->runAction(seq);
};


void CKStart90::createStarts(int _count,float _speed)
{
    max_count = _count;
    speed = _speed;
    win_size = CKHelper::Instance().getWinSize();
    for(int i = 0; i < _count; ++i)
    {
        addStars(win_size.width*CCRANDOM_0_1(),min_height + (win_size.height - min_height)*CCRANDOM_0_1());
    }
};

void CKStart90::addStartName(const std::string &_str)
{
    //char *str = new char[32];
    //sprintf(str, "%s",_str.c_str());
    //CCLOG("CKStart90::addStartName %s ",str);
    if(star_name1.length() < 3)
    {
        star_name1 = _str;
    }
    else
    {
        star_name2 = _str;
    }
    //start_name.push_back(str);
}

#pragma mark - CLOUD METHOD
void CKStart90::addCloudName(const std::string &_str)
{
    char *str = new char[32];
    sprintf(str, "%s",_str.c_str());
    cloud_name.push_back(str);
};

void CKStart90::addClouds(const float &_x,const float &_y)
{
    CCLOG("int (_x %f) (_y %f)",_x,_y);
    it = cloud_list.begin();
    while(it != cloud_list.end())
    {
        CCSprite * sprite = (*it)->sprite;
        if(abs(sprite->getPositionX() - _x) < sprite->getContentSize().width)
            if(abs(sprite->getPositionY() - _y) < sprite->getContentSize().height)
            {
                addClouds(win_size.width*CCRANDOM_0_1(),min_height + (win_size.height*0.9 - min_height)*CCRANDOM_0_1());
                return;
            };
        it++;
    };
    
    CCSprite* sprite = CCSprite::createWithSpriteFrameName(cloud_name[CK::ck_rand()%int(cloud_name.size())]);// (CK::loadImage(cloud_name[CK::ck_rand()%int(cloud_name.size())].c_str()).c_str());
    CKElement * elem = new CKElement;
    elem->sprite = sprite;
    elem->speed = speed + speed_var*CCRANDOM_MINUS1_1();
    //star_list.push_back(pair<CCSprite* ,float>(sprite,speed + speed_var*CCRANDOM_MINUS1_1()));
    cloud_list.push_back(elem);
    //cloud_list.push_back(pair<CCSprite* ,float>(sprite, speed + speed_var*CCRANDOM_MINUS1_1()));
    m_layer->addChild(sprite,0);
    sprite->setPosition(ccp(_x,_y));
};

void CKStart90::createCloud(int _max_count,float _speed,float _speed_var)
{
    win_size = CKHelper::Instance().getWinSize();
    speed_var = _speed_var;
    for(int i = 0; i < _max_count; ++i)
    {
        addClouds(win_size.width*CCRANDOM_0_1(),min_height + (win_size.height*0.9 - min_height)*CCRANDOM_0_1());
    }
}