//
//  ShopBomb.h
//  CandyKadze
//
//  Created by PR1.Stigol on 03.04.13.
//
//

#ifndef __CandyKadze__ShopBomb__
#define __CandyKadze__ShopBomb__

#include <iostream>
#include <vector>

class ShopBomb
{
    
    static int buy_with_pack;
public:
    unsigned char type;
    float free[4];
     
    std::vector<int> free_pack;
    std::vector<int> sold_pack;
    static int getBuyPack();
    static void setBuyPack(int _value);
    
    bool isFreePack(const int &_pack);
    bool isSoldPack(const int &_pack);
    void eraseFreePack(const int &_pack);
    float getMaxDiscount();
    bool haveFreePack();
    bool haveSoldPack();
    bool haveDiscount();
};
#endif /* defined(__CandyKadze__ShopBomb__) */
