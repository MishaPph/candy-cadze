//
//  ShopBomb.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 03.04.13.
//
//

#include "ShopBomb.h"

int ShopBomb::buy_with_pack = 0;//buy discount or free pack bomb, need for cheater's

int ShopBomb::getBuyPack()
{
    return ShopBomb::buy_with_pack;
};
void ShopBomb::setBuyPack(int _value)
{
    ShopBomb::buy_with_pack = _value;
};

bool ShopBomb::isFreePack(const int &_pack)
{
    std::vector<int>::iterator it = free_pack.begin();
    while (it != free_pack.end()) {
        if(*it == _pack)
            return true;
        it++;
    }
    return false;
};
bool ShopBomb::isSoldPack(const int &_pack)
{
    std::vector<int>::iterator it = sold_pack.begin();
    while (it != sold_pack.end()) {
        if(*it == _pack)
            return true;
        it++;
    }
    return false;
};

void ShopBomb::eraseFreePack(const int &_pack)
{
    std::vector<int>::iterator it = free_pack.begin();
    while (it != free_pack.end()) {
        if(*it == _pack)
        {
            sold_pack.push_back(*it);
            free_pack.erase(it);
            return;
        }
        it++;
    }
    return;
};
float ShopBomb::getMaxDiscount()
{
    float max_d  = 0.0;
    for(int i = 0; i < 4;++i)
    {
        if(free[i] > max_d)
            max_d = free[i];
    };
    return max_d;
}
bool ShopBomb::haveDiscount()
{
    for(int i = 0; i < 4;++i)
    {
        if(free[i] != 0.0)
            return true;
    };
    return false;
};

bool ShopBomb::haveFreePack()
{
    return !free_pack.empty();
};

bool ShopBomb::haveSoldPack()
{
    return !sold_pack.empty();
}