//
//  CKCubeArray.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 20.09.12.
//
//

#include "CKCubeArray.h"
#include "CKBonus.h"


#pragma mark - BASE
CKCubeArrayBase::CKCubeArrayBase()
{
    full_cube = 4;
    is_restart = false;
    is_ipad = (CKHelper::Instance().getWinSize().width == 1024);
    BOX_SIZE = CKHelper::Instance().BOX_SIZE;
    cube_batch = NULL;
};

CKCubeArrayBase::~CKCubeArrayBase()
{
    mask_list.clear();
    emitter_it = emitter_list1.begin();
    while(emitter_it != emitter_list1.end())
    {
        m_layer->removeChild(*emitter_it, true);
        CC_SAFE_RELEASE_NULL(*emitter_it);
        emitter_it++;
    };
    emitter_list1.clear();
    
};

void CKCubeArrayBase::setLayer(cocos2d::CCNode *_layer)
{
    m_layer = _layer;
};

int CKCubeArrayBase::getScore()
{
    if(score == 0)
        return 0;
    int tmp = score;
    score = 0;
    return tmp;
};

int CKCubeArrayBase::getCountCube()
{
    return count_cube;
};

void CKCubeArrayBase::setPanelSize(int _s)
{
    panel_size = _s;
};

void CKCubeArrayBase::setGroundHeight(int _h)
{
    ground_height = _h;
};

int CKCubeArrayBase::getCountActiveCube()
{
    return count_active_cube;
};

CCNode * CKCubeArrayBase::createBatch(const char* _filename)
{
    if(cube_batch)
    {
        cube_batch->removeAllChildrenWithCleanup(true);
        cube_batch->removeFromParentAndCleanup(true);
    };
    
    batch_file_name = _filename;
    cube_batch = CCSpriteBatchNode::create(_filename);
    //cube_batch->setVisible(false);
    batch_texture_name = cube_batch->getTexture()->getName();
    
    m_layer->addChild(cube_batch,zOrder_Cube);
    return cube_batch;
};

unsigned char CKCubeArrayBase::getBigCube(const int &_x,const int &_y) const
{
    return small_map[_x][_y];
};

unsigned char CKCubeArrayBase::getMiniCube(const int &_x,const int &_y) const
{
    return large_map[_x][_y];
};

int CKCubeArrayBase::getGroundHeight() const
{
    return ground_height;
};

int CKCubeArrayBase::getMaxHeightColumn()
{
    int max = 0;
    for(int j = 0; j < COUNT_COLUMNS; ++j)
    {
        int k = getColumnHeight(j);
        if(k > max)
            max = k;
    }

    return max;
};

#pragma mark - OPERATION
void CKCubeArrayBase::resizeColumn(unsigned char _height, unsigned char _need)
{
    //resorting vector
    for(unsigned char j = 0; j < v_temp.size(); ++j)
    {
        unsigned char k1 = ck_rand()%v_temp.size();
        unsigned char k2 = ck_rand()%v_temp.size();
        if(k1 != k2)
        {
            unsigned char t =  v_temp[k1];
            v_temp[k1] = v_temp[k2];
            v_temp[k2] = t;
        };
    };
    std::vector<unsigned char>::iterator _it = v_temp.begin();
    while( _it != v_temp.end() )
    {
         CCLOG("v_temp = %d ",*_it);
        _it++;
    }
    // ceilf(3);
    unsigned char resize = char(_need*0.4 + 0.5); //count column not resize
    
    while(resize < v_temp.size() )
        v_temp.pop_back();
    
    //CCLOG("size temp = %d", v_temp.size());
    unsigned char count_free_box = 0;
    unsigned char remove_box = char(_need*0.2 + 0.5);   //count box to remove
    
    while(remove_box < v_temp.size() )
    {
        count_free_box++;
        if(_height > 1)
        {
            small_map[_height - 1][v_temp.back()] = 0;
            if(CCRANDOM_0_1() > 0.5)
            {
                count_free_box++;
                if(_height > 2)
                    small_map[_height - 2][v_temp.back()] = 0;
            };
        };
        if(v_temp.size() > 2)
            v_temp.pop_back();
        else
        {
            break;
        }
    };
    //CCLOG("size temp = %d count free %d", v_temp.size(),count_free_box);
    //print
    
    while(!v_temp.empty())
    {
        //CCLOG("size v_temp = %d", v_temp.size());
        unsigned char k = 1 + char(CCRANDOM_0_1()+0.7);
        
        if(v_temp.size() == 1)
            k = count_free_box;//1 + char(CCRANDOM_0_1()+0.5);
        if(k > count_free_box)
            k = count_free_box;
        
        if ( k == 0) k++;
        if ( k > 2) k = 2;
        
        --count_free_box;
        for(unsigned char i = _height; i < _height + k ;++i)
        {
            small_map[i][v_temp.back()] = full_cube;
        };
        v_temp.pop_back();
    };
};

void CKCubeArrayBase::restart()
{
    
};

void CKCubeArrayBase::genColumns(unsigned char _column_h,unsigned char _need)
{
    unsigned char last_it = 0;
    unsigned char count_column = 0;
    v_temp.clear();
    srand(time(0));
    unsigned char free_columns = BASE_COUNT_COLUNMS - _need;
    
    unsigned char start_column = 0;
    if((CKHelper::Instance().lvl_number <= 4 ) && CKHelper::Instance().lvl_speed == 0)
    {
        start_column++;
        free_columns--;
        if(CKHelper::Instance().lvl_number == 0 || CKHelper::Instance().lvl_number == 1)
            start_column++;
        free_columns--;
    }
    else
    {
        if(rand()%MAX(1,free_columns) == 0)
        {
            ++count_column;
            createColumn(0,_column_h);
            v_temp.push_back(0);
            last_it = 0;
            start_column++;
        }
    };
        
    for(unsigned char j = start_column; j < BASE_COUNT_COLUNMS; ++j)
    {

        float randi = CCRANDOM_0_1();
        if((randi > (1.08 - float(_need)/float(BASE_COUNT_COLUNMS))) || (last_it > 1) || (free_columns == 0))
        {
            if(free_columns > 4  && count_column == 1 )
            {
                --free_columns;
                ++last_it;
                continue;
            };
            
            ++count_column;
            createColumn(j,_column_h);
            v_temp.push_back(j);
            last_it = 0;
        }
        else
        {
            --free_columns;
            ++last_it;
        };

        if(count_column == _need)
            break;
    };//old version world 90
    
    
};

void CKCubeArrayBase::copyCubeTominiCube()
{
    memset(large_map, 0, COUNT_ROWS*COUNT_COLUMNS*4);
    count_active_cube = 0;
    for(int j = 0; j < COUNT_COLUMNS; ++j)
    {
        for(int i = 0; i < COUNT_ROWS; ++i )
        {
            if(int(small_map[i][j]) != 0)
            {
                large_map[i*2][j*2] = 1;//small_map[i][j];
                large_map[i*2+1][j*2] = 1;// small_map[i][j];
                large_map[i*2][j*2+1] = 1;//small_map[i][j];
                large_map[i*2+1][j*2+1] = 1;// small_map[i][j];
                
                sprite_min_map[i*2][j*2] = NULL;
                sprite_min_map[i*2 + 1][j*2] = NULL;
                sprite_min_map[i*2][j*2 + 1] = NULL;
                sprite_min_map[i*2 + 1][j*2 + 1] = NULL;
                count_active_cube += 4;
                
            };
        };
    };
};

void CKCubeArrayBase::createEmitterPool(int _count)
{
    for(int i = 0; i < 3;i++)
    {
        CCParticleSystem* m_emitter2 = CCParticleSystemQuad::create("test90.plist");
        m_layer->addChild(m_emitter2 ,zOrderParticle);
        m_emitter2->stopSystem();
        m_emitter2->retain();
        m_emitter2->setAutoRemoveOnFinish(false);
        if(!CKHelper::Instance().getIsIpad())
            m_emitter2->setScale(0.5);
        emitter_list1.push_back(m_emitter2);
    }
};

void CKCubeArrayBase::createLevel(int _lvl_diff,int _lvl_num)
{
//    srand((unsigned)time(0));
//    BOX_SIZE = CKHelper::Instance().BOX_SIZE;
//    
//    for(int i = 0; i < 3;i++)
//    {
//        CCParticleSystem* m_emitter2 = CCParticleSystemQuad::create("test90.plist");
//        m_layer->addChild(m_emitter2 ,zOrderParticle);
//        m_emitter2->stopSystem();
//        m_emitter2->retain();
//        m_emitter2->setAutoRemoveOnFinish(false);
//        if(!CKHelper::Instance().getIsIpad())
//            m_emitter2->setScale(0.5);
//        emitter_list1.push_back(m_emitter2);
//    }
//    
//    memset(small_map, 0, COUNT_ROWS*COUNT_COLUMNS);
//    
//    unsigned char column_height = int(_lvl_num/4) + 2;
//    unsigned char need_column = int(_lvl_num/4) + 8 + _lvl_num%4;
//    CCLOG("column_height(%d) need_column*(%d)",column_height,need_column);
//    
//    // CCLOG("need_column(%d) column_height(%d)",need_column,column_height);
//    count_cube = 0;
//    genColumns(column_height,need_column);
//    CCLOG("column_height(%d) need_column*(%d)",column_height,need_column);
//    resizeColumn(column_height,need_column);
//    
//    copyCubeTominiCube();
//    CCLOG("count_cube = %d,count_active_cube = %d count bonus %d",count_cube,count_active_cube,(count_cube+5)/10);
//    createMap(CKHelper::Instance().BOX_SIZE, count_coll, count_row);
//    
//    score = 0;
    
    srand((unsigned)time(0));
    
    
    memset(small_map, 0, COUNT_ROWS*COUNT_COLUMNS);
    //load cube map with plist
    CCDictionary *dict = CCDictionary::createWithContentsOfFile("levels.plist");
    base_height = 2;
    unsigned char need_column = 8;
    unsigned char need_cube = 15;
    if(dict->objectForKey("difficult"))
    {
        CCDictionary *diff  = (CCDictionary *)dict->objectForKey("difficult");
        CCLOG("createLevel:: CK::c_speed[CKHelper::Instance().lvl_speed] %s",CK::c_difficult[_lvl_diff]);
        CCArray* ar_level = (CCArray*)diff->objectForKey(CK::c_difficult[_lvl_diff]);
        CCObject* pObj_l = NULL;
        int lvl = 0;
        CCARRAY_FOREACH(ar_level, pObj_l)
        {
            if(lvl == _lvl_num)//load first list
            {
                CCDictionary *dict_lvl = (CCDictionary*)pObj_l;
                if(dict_lvl->objectForKey("cube"))
                {
                    CCDictionary *dict_cube = (CCDictionary *)dict_lvl->objectForKey("cube");
                    base_height = dictInt(dict_cube, "height");
                    need_column = dictInt(dict_cube, "columns");
                    need_cube = dictInt(dict_cube, "count_cube");
                }
            }
            lvl++;
        }
    }
    
    CCLOG("ccreateLevel:: column_height(%d) need_column*(%d)",base_height,need_column);
    count_cube = 0;
    
    genColumns(base_height,need_column);
    
    //resorting vector
    for(unsigned char j = 0; j < v_temp.size(); ++j)
    {
        unsigned char k1 = ck_rand()%v_temp.size();
        unsigned char k2 = ck_rand()%v_temp.size();
        if(k1 != k2)
        {
            unsigned char t =  v_temp[k1];
            v_temp[k1] = v_temp[k2];
            v_temp[k2] = t;
        };
    };
    
    int l = 0;
    CCLOG("createLevel::count_cube %d %d",count_cube,need_cube);
    while (l < 10) {
        addBlock(v_temp[rand()%v_temp.size()]);
        diffBlock(v_temp[rand()%v_temp.size()]);
        l++;
    };
    
    std::vector<unsigned char>::iterator v_it0 =  v_temp.begin(),v_it =  v_temp.begin();
    while(v_it != v_temp.end())
    {
        //  CCLog("createLevel:: num_col %d base_height %d",*v_it,base_height);
        if(*v_it == 0)
        {
            v_it0 = v_it;
            if(getColumnHeight(0) >= base_height)
            {
                int nn = getColumnHeight(0) - base_height + 1;
                for(int ii = 0; ii < nn;ii++)
                    diffBlock(0);
            }
            //v_temp.erase(v_it);
            //   break;
        };
        v_it++;
    };
    // CCLOG("........................");
    if(*v_it0 == 0)
        //     v_temp.erase(v_it0);
        //CCLOG("........................");
        v_it =  v_temp.begin();
    while(v_it != v_temp.end())
    {
        //    CCLog("createLevel::show num_col %d base_height %d",*v_it,base_height);
        v_it++;
    }
    if(count_cube < need_cube)
        while (count_cube < need_cube) {
            // CCLOG("createLevel:: count_cube %d need_cube %d",count_cube,need_cube);
            addBlock(v_temp[rand()%v_temp.size()]);
        };
    
    if(count_cube > need_cube)
        while (count_cube > need_cube) {
            //   CCLOG("createLevel:: count_cube %d need_cube %d",count_cube,need_cube);
            diffBlock(v_temp[rand()%v_temp.size()]);
        };
    
    copyCubeTominiCube();
    CCLOG("createLevel:: count_cube = %d,count_active_cube = %d count bonus %d",count_cube,count_active_cube,(count_cube+5)/10);
    createMap(CKHelper::Instance().BOX_SIZE, BASE_COUNT_COLUNMS, count_row);
    createEmitterPool();
    score = 0;
};

void CKCubeArrayBase::parsingLevel(const std::string &_str)
{
    
    CCDictionary *dict = NULL;
    if(_str.length() > 3)
    {
        CCLOG("CKCubeArrayBase::parsingLevel %s",_str.c_str());
       dict = CCDictionary::createWithContentsOfFile(_str.c_str());
        
    }
    else
    {
        CCLOG("CKCubeArrayBase::parsingLevel world %s",CKHelper::Instance().getWorld().c_str());
        dict = CCDictionary::createWithContentsOfFile(CKHelper::Instance().getWorld().c_str());
    };

    
    CCArray* frameArray = (CCArray*)dict->objectForKey("cube");
    CCObject* pObj = NULL;
    unsigned char index = 0;
    count_coll = BASE_COUNT_COLUNMS;
    
    CCARRAY_FOREACH(frameArray, pObj)
    {
        CCDictionary* name = (CCDictionary*)(pObj);
        ++index;
        cube_map[dictInt(name,"type")] = dictStr(name,"name");
        cube_type.push_back(dictInt(name,"type"));
    };
    if(dict->objectForKey("mask"))
    {
        frameArray = (CCArray*)dict->objectForKey("mask");
        pObj = NULL;
        CCARRAY_FOREACH(frameArray, pObj)
        {
            mask_list.push_back(static_cast<CCString*>(pObj)->getCString());
        };
    };
    CC_SAFE_RELEASE_NULL(dict);
};

//create sprite "big cube" with array cube_map
//and generation random texture sprite in map
void CKCubeArrayBase::createMap(int _size_box,int _count_col, int _count_row)
{
    for(int i = 1; i <= COUNT_ROWS; ++i)
        for(int j = 0; j < _count_col; ++j)
        {
            if(int(small_map[i-1][j]) != 0)
            {
                int number = cube_type[ck_rand()%cube_type.size()];
                createMiniCube(CCSprite::createWithSpriteFrameName(cube_map[number].c_str()),i-1,j,number);
            };
        };
};

void CKCubeArrayBase::createColumn(unsigned char _x,unsigned char _h,int _type)
{
    for(unsigned char i = 0; i < _h; ++i)
    {
        small_map[i][_x] = full_cube;
        count_cube++;
    }
};
void CKCubeArrayBase::hideNode(CCNode *_sender)
{
    _sender->setVisible(false);
};

bool CKCubeArrayBase::destroyMiniCub(const unsigned char &_x,const unsigned char &_y,const unsigned char _effect)
{
    if(_x < 0)
        return false;
    
//    if(_y < 0)
//        _y = 0;
    
    if(large_map[_y][_x] == 0)
        return false;
    
    int x = _x/2;
    int y = _y/2;
    
    if(small_map[y][x] != 0)
    {
        //calculation score for cube destroy
        score++;
        small_map[y][x]--;
    };

    CCActionInterval* action = CCMoveTo::create(0.15, ccp(sprite_min_map[_y][_x]->getPositionX(),sprite_min_map[_y][_x]->getPositionY() - BOX_SIZE));
    CCActionInterval* action3 = CCFadeOut::create(0.05);
    CCAction* action4 = CCSequence::create(action,action3,NULL);
    if(sprite_min_map[_y][_x])
        sprite_min_map[_y][_x]->runAction(action4);

    emitter_it = emitter_list1.begin();
    while (emitter_it != emitter_list1.end()) {
        if(!(*emitter_it)->isActive())
        {
            (*emitter_it)->setPosition(sprite_min_map[_y][_x]->getPosition());
            (*emitter_it)->resetSystem();
            break;
        }
        emitter_it++;
    }

    large_map[_y][_x] = 0;
    --count_active_cube;
    return true;
};

bool CKCubeArrayBase::destroyMiniCub2(int _x, int _y)
{
    if(_x < 0)
        return false;
    if(_y < 0)
        _y = 0;
    
    if(large_map[_y][_x] == 0)
        return false;
    
    int x = _x/2;
    int y = _y/2;
    
    if(small_map[y][x] != 0)
    {
        //calculation score for cube destroy
        score++;
        small_map[y][x]--;
        score++;
        small_map[y][x]--;
    };

    large_map[_y][_x] = 0;
    large_map[_y - 1][_x] = 0;
    --count_active_cube;
    --count_active_cube;
    
    CCActionInterval* action = CCMoveTo::create(0.15, ccp(sprite_min_map[_y][_x]->getPositionX(),sprite_min_map[_y][_x]->getPositionY() - BOX_SIZE));
  //  CCActionInterval* action3 = CCFadeOut::create(0.05);
    CCAction* action4 = CCSequence::create(action,CCCallFuncN::create(sprite_min_map[_y][_x], callfuncN_selector(CKCubeArrayBase::hideNode)),NULL);
    if(sprite_min_map[_y][_x])
        sprite_min_map[_y][_x]->runAction(action4);
    
    
    CCActionInterval* action_2 = CCMoveTo::create(0.15, ccp(sprite_min_map[_y-1][_x]->getPositionX(),sprite_min_map[_y-1][_x]->getPositionY() - BOX_SIZE));
   // CCActionInterval* action3_2 = CCFadeOut::create(0.05);
    CCAction* action4_2 = CCSequence::create(action_2,CCCallFuncN::create(sprite_min_map[_y - 1][_x], callfuncN_selector(CKCubeArrayBase::hideNode)),NULL);
    if(sprite_min_map[_y - 1][_x])
        sprite_min_map[_y - 1][_x]->runAction(action4_2);
    
    emitter_it = emitter_list1.begin();
    while (emitter_it != emitter_list1.end()) {
        if(!(*emitter_it)->isActive())
        {
            (*emitter_it)->setPosition(ccp(sprite_min_map[_y][_x]->getPosition().x,sprite_min_map[_y][_x]->getPosition().y - BOX_SIZE/4));
            (*emitter_it)->resetSystem();
            break;
        }
        emitter_it++;
    }
    
    return true;
};


int CKCubeArrayBase::checkCollision(CCSprite* _bomb)
{
    if(!_bomb)
        return 0;
    int result = 0;
    
        int x1 = (_bomb->getPositionX() - panel_size - BOX_SIZE/2 - BOX_SIZE/6)/(BOX_SIZE/2);
        int y1 = (_bomb->getPositionY() - ground_height - _bomb->getContentSize().height/2)/(BOX_SIZE/2);
        int x2 = (_bomb->getPositionX() - panel_size - BOX_SIZE/2 + BOX_SIZE/6)/(BOX_SIZE/2);

    while (large_map[y1 + 1][x1] != 0 || large_map[y1 + 1][x2] != 0) {
        y1++;
    };
     if(large_map[y1][x1] != 0 )
     {
         result = 1;
         destroyMiniCub2(x1, y1);
     }
    else
    {
        if(large_map[y1][x2] != 0 )
        {
            result = 1;
            destroyMiniCub2(x2, y1);
        }
    }
    //  CCLOG("result checkCollision %d",result);
    return result;
};

void CKCubeArrayBase::createMiniCube(cocos2d::CCSprite *_sprite,int _x,int _y,int _tag)
{
    CCTexture2D* t_texture = _sprite->getTexture();
    // CCLOG("createMiniCube %d %d",t_texture->getName(),cube_batch->getTexture()->getName());
    float offsetX = _sprite->getTextureRect().origin.x;
    float offsetY = _sprite->getTextureRect().origin.y;
    
    CCSprite* sprite = CCSprite::createWithTexture(t_texture, CCRectMake(0 + offsetX,offsetY + BOX_SIZE / 2,BOX_SIZE / 2,BOX_SIZE / 2));
    
    sprite->setPosition(ccp((_y+1)*BOX_SIZE + panel_size - BOX_SIZE / 4,(_x)*BOX_SIZE + ground_height +  BOX_SIZE / 4) );
    large_map[_x*2][_y*2] = 1;
    sprite_min_map[_x*2][_y*2] = sprite;
    sprite->setTag(_tag + 1);
   // sprite->setUserData(_sprite);
    sprite->setColor((ccColor3B){0,255,0});
    
    if(!cube_batch)
        m_layer->addChild(sprite,zOrder_Cube);
    else
    {
        // CCAssert(sprite->getTexture()->getName() == cube_batch->getTexture()->getName(), "");
        if (sprite->getTexture()->getName() != cube_batch->getTexture()->getName()) {
            cube_batch->setTexture(t_texture);
            // createBatch(batch_file_name.c_str());
        };
        cube_batch->addChild(sprite);
    };
    
    sprite = CCSprite::createWithTexture(t_texture, CCRectMake(BOX_SIZE / 2 + offsetX ,offsetY + BOX_SIZE / 2,BOX_SIZE / 2,BOX_SIZE / 2));
    
    sprite->setPosition(ccp((_y+1)*BOX_SIZE + panel_size - BOX_SIZE / 4 + BOX_SIZE / 2,(_x)*BOX_SIZE + ground_height +  BOX_SIZE / 4) );
    large_map[_x*2][_y*2 + 1] = 1;
    sprite_min_map[_x*2][_y*2 + 1] = sprite;
    sprite->setTag(_tag + 2);
  //  sprite->setUserData(_sprite);
    sprite->setColor((ccColor3B){0,255,0});
    if(!cube_batch)
        m_layer->addChild(sprite,zOrder_Cube);
    else
        cube_batch->addChild(sprite);
    
    sprite = CCSprite::createWithTexture(t_texture, CCRectMake(BOX_SIZE / 2 + offsetX ,offsetY + 0,BOX_SIZE / 2,BOX_SIZE / 2));
    
    sprite->setPosition(ccp((_y+1)*BOX_SIZE + panel_size - BOX_SIZE / 4 + BOX_SIZE / 2,(_x)*BOX_SIZE + ground_height +  BOX_SIZE / 4 + BOX_SIZE / 2) );
    large_map[_x*2 + 1][_y*2 + 1] = 1;
    sprite_min_map[_x*2 + 1][_y*2 + 1] = sprite;
    sprite->setTag(_tag + 3);
  //  sprite->setUserData(_sprite);
    sprite->setColor((ccColor3B){0,255,0});
    if(!cube_batch)
        m_layer->addChild(sprite,zOrder_Cube);
    else
        cube_batch->addChild(sprite);
    
    sprite = CCSprite::createWithTexture(t_texture, CCRectMake(0 + offsetX,offsetY + 0,BOX_SIZE / 2,BOX_SIZE / 2));
    sprite->setPosition(ccp((_y+1)*BOX_SIZE + panel_size - BOX_SIZE / 4,(_x)*BOX_SIZE + ground_height +  BOX_SIZE / 4 + BOX_SIZE / 2) );
    large_map[_x*2 + 1][_y*2] = 1;
    sprite_min_map[_x*2 + 1][_y*2] = sprite;
    sprite->setTag(_tag + 4);
  //  sprite->setUserData(_sprite);
    sprite->setColor((ccColor3B){0,255,0});
    if(!cube_batch)
        m_layer->addChild(sprite,zOrder_Cube);
    else
        cube_batch->addChild(sprite);
};

//add cube to column
void CKCubeArrayBase::addBlock(int _column)
{
    for(int i = 0; i < (base_height+2); i++)
    {
        if(small_map[i][_column] == 0)
        {
            small_map[i][_column] = full_cube;
            count_cube++;
            return;
        }
    }
};

//subtract one cube from column
void CKCubeArrayBase::diffBlock(int _column)
{
    for(int i = MAX(1,base_height - 2); i < COUNT_ROWS; i++)
    {
        if(small_map[i][_column] != 0 && small_map[i + 1][_column] == 0)
        {
            small_map[i][_column] = 0;
            count_cube--;
            return;
        }
    }
};

bool CKCubeArrayBase::isTopMiniCube(const CCPoint &_pos)
{
    int x = (_pos.x - panel_size - BOX_SIZE/2)/(BOX_SIZE/2);
    int y = (_pos.y - ground_height)/(BOX_SIZE/2);
    
    if(x < 0 || x > COUNT_COLUMNS*2)
        return false;
    if(y < 0 || y > COUNT_ROWS*2)
        return false;
    if(large_map[y][x] != 0 && large_map[y + 2][x] == 0)
    {
        return true;
    };
    return false;
};

bool CKCubeArrayBase::isTopCube(const CCPoint &_pos)
{
    int x = (_pos.x - panel_size - BOX_SIZE/2)/BOX_SIZE;
    int y = (_pos.y - ground_height)/BOX_SIZE;
    
    if(x < 0 || x > COUNT_COLUMNS)
        return false;
    if(y < 0 || y > COUNT_ROWS)
        return false;
    if(small_map[y][x] != 0 && small_map[y+1][x] == 0)
    {
        return true;
    };
    return false;
};

int CKCubeArrayBase::getColumnHeight(float _pos)
{
   int x = (_pos - panel_size - BOX_SIZE/2)/BOX_SIZE;
    CCLOG("x %d",x);
   return getColumnHeight(x);
};

int CKCubeArrayBase::getColumnHeight(int _column)
{
    int i = 0;
    while(small_map[i][_column] != 0)
    {
        i++;
    };
    //CCLOG("getColumnHeight:: height %d",i);
    return i;
};

int CKCubeArrayBase::checkAirplane(const cocos2d::CCPoint &_fan,const cocos2d::CCPoint &_bott)
{
    if(count_active_cube == 0)
        return rAirplane_Win;
    int x1 = (_fan.x - panel_size - BOX_SIZE/2)/(BOX_SIZE/2);
    int y1 = (_fan.y - ground_height)/(BOX_SIZE/2);
    if(x1 < 0)
        return 0;
    
    if(x1 < COUNT_COLUMNS*2 && y1 < COUNT_ROWS*2)
        if(large_map[y1][x1] != 0 ) //airplane fan collision with cube
        {
            //destroyMiniCub(x1, y1);
            if(large_map[y1 - 1][x1] != 0)
            {
              //  destroyMiniCub(x1, y1 - 1);
                //animate cube after destroy spectrum
                sprite_min_map[y1 - 1][x1]->stopAllActions();
                sprite_min_map[y1 - 1][x1]->setOpacity(255);
                sprite_min_map[y1 - 1][x1]->setVisible(true);
                sprite_min_map[y1 - 1][x1]->setColor((ccColor3B){255,0,0});
                sprite_min_map[y1 - 1][x1]->runAction(CCBlink::create(3.0, 20));
            }

                sprite_min_map[y1][x1]->stopAllActions();
                sprite_min_map[y1][x1]->setOpacity(255);
                sprite_min_map[y1][x1]->setColor((ccColor3B){255,0,0});
                sprite_min_map[y1][x1]->setVisible(true);
                sprite_min_map[y1][x1]->runAction(CCBlink::create(3.0, 20));

            if(count_active_cube == 0)
                return rAirplane_Win;
            
            return rAirplane_Over;
        };
    int x2 = (_bott.x - panel_size - BOX_SIZE/2)/(BOX_SIZE/2);
    int y2 = (_bott.y - ground_height)/(BOX_SIZE/2);
    if(large_map[y2][x2] != 0 )
    {
        return rAirplane_Splash;
    };
    return 0;
};

#pragma mark - CUBE_ARRAY
CKCubeArray::CKCubeArray()
{
    cube_batch = NULL;
    m_bonus = NULL;
    //rt_list.clear();
    destroy_cube = 0;
    full_cube = 4;
    splash_sprite = NULL;
    splash_sprite = CCSprite::create(CK::loadImage("splash.png").c_str());
    splash_sprite->retain();
};

CKCubeArray::~CKCubeArray()
{
    CCLOG("~CKCubeArray");
    rt = NULL;
    CC_SAFE_RELEASE_NULL(splash_sprite);
    std::map<int, CCTexture2D*>::iterator it;
    
    it = render_texture_map.begin();
    while(it != render_texture_map.end())
    {
        it->second->release();
        it++;
    };
    render_texture_map.clear();
    
    it = render_texture_map2.begin();
    while(it != render_texture_map2.end())
    {
        it->second->release();
        it++;
    };
    render_texture_map2.clear();
    
    emitter_it = emitter_list2.begin();
    while(emitter_it != emitter_list2.end())
    {
        m_layer->removeChild(*emitter_it, true);
        CC_SAFE_RELEASE_NULL(*emitter_it);
        emitter_it++;
    };
    emitter_list2.clear();
    
//    std::list<CCRenderTexture*>::iterator rt_it = rt_list.begin();
//    while (rt_it != rt_list.end()) {
//        CC_SAFE_RELEASE_NULL(*rt_it);
//        rt_it++;
//    }
//    rt_list.clear();
};

void CKCubeArray::setBonus(CKBonus *_bonus)
{
    m_bonus = _bonus;
};

bool CKCubeArray::isBonus()
{
    return is_bonus;
};

#pragma mark CUBE_CREATE



void CKCubeArray::restart()
{
    is_restart = true;
/*    for(int j = 0; j < COUNT_COLUMNS; ++j)
    {
        for(int i = 0; i < COUNT_ROWS; ++i )
        {
            if(sprite_min_map[i*2][j*2] !=NULL)
            {
                m_layer->removeChild(sprite_min_map[i*2][j*2] , true);
            }
            if(sprite_min_map[i*2+1][j*2] !=NULL)
            {
                m_layer->removeChild(sprite_min_map[i*2+1][j*2] , true);
            }
            if(sprite_min_map[i*2][j*2+1] !=NULL)
            {
                m_layer->removeChild(sprite_min_map[i*2][j*2+1] , true);
            }
            if(sprite_min_map[i*2+1][j*2+1] !=NULL)
            {
                m_layer->removeChild(sprite_min_map[i*2+1][j*2+1] , true);
            }
            
        };
    };*/
    
    destroy_cube = 0;
    full_cube = 4;
    
    emitter_it = emitter_list2.begin();
    while (emitter_it != emitter_list2.end()) {
            (*emitter_it)->stopAllActions();
            (*emitter_it)->stopSystem();
        emitter_it++;
    }
    
    emitter_it = emitter_list1.begin();
    while (emitter_it != emitter_list1.end()) {
            (*emitter_it)->stopAllActions();
            (*emitter_it)->stopSystem();
        emitter_it++;
    };
    
    memset(small_map, 0, COUNT_ROWS*COUNT_COLUMNS);
    
    if(false)//old version
    {
        unsigned char column_height = int(CKHelper::Instance().lvl_number/4) + 2;
        unsigned char need_column = int(CKHelper::Instance().lvl_number/4) + 8 + CKHelper::Instance().lvl_number%4;
        CCLOG("column_height(%d) need_column*(%d)",column_height,need_column);
        
        // CCLOG("need_column(%d) column_height(%d)",need_column,column_height);
        count_cube = 0;
        genColumns(column_height,need_column);
        resizeColumn(column_height,need_column);
    }
    else
    {
        //load cube map with plist
        CCDictionary *dict = CCDictionary::createWithContentsOfFile("levels.plist");
        base_height = 2;
        unsigned char need_column = 8;
        unsigned char need_cube = 15;
        if(dict->objectForKey("difficult"))
        {
            CCDictionary *diff  = (CCDictionary *)dict->objectForKey("difficult");
            CCLOG("CK::c_speed[CKHelper::Instance().lvl_speed] %s",CK::c_difficult[CKHelper::Instance().lvl_speed]);
            CCArray* ar_level = (CCArray*)diff->objectForKey(CK::c_difficult[CKHelper::Instance().lvl_speed]);
            CCObject* pObj_l = NULL;
            int lvl = 0;
            CCARRAY_FOREACH(ar_level, pObj_l)
            {
                if(lvl == CKHelper::Instance().lvl_number)//load first list
                {
                    CCDictionary *dict_lvl = (CCDictionary*)pObj_l;
                    if(dict_lvl->objectForKey("cube"))
                    {
                        CCDictionary *dict_cube = (CCDictionary *)dict_lvl->objectForKey("cube");
                        base_height = dictInt(dict_cube, "height");
                        need_column = dictInt(dict_cube, "columns");
                        need_cube = dictInt(dict_cube, "count_cube");
                    }
                }
                lvl++;
            }
        }
        
        CCLOG("column_height(%d) need_column*(%d)",base_height,need_column);
        count_cube = 0;
        
        genColumns(base_height,need_column);
        
        //resorting vector
        for(unsigned char j = 0; j < v_temp.size(); ++j)
        {
            unsigned char k1 = ck_rand()%v_temp.size();
            unsigned char k2 = ck_rand()%v_temp.size();
            if(k1 != k2)
            {
                unsigned char t =  v_temp[k1];
                v_temp[k1] = v_temp[k2];
                v_temp[k2] = t;
            };
        };
        
        int l=0;
        CCLOG("count_cube %d %d",count_cube,need_cube);
        while (l < 10) {
            addBlock(v_temp[rand()%v_temp.size()]);
            diffBlock(v_temp[rand()%v_temp.size()]);
            l++;
        };

        if(count_cube < need_cube)
            while (count_cube < need_cube) {
                addBlock(v_temp[rand()%v_temp.size()]);
            };
        
        if(count_cube > need_cube)
            while (count_cube > need_cube) {
                //   CCLOG("count_cube %d %d",count_cube,need_cube);
                diffBlock(v_temp[rand()%v_temp.size()]);
            };
    }
    
    copyCubeTominiCube();
    CCLOG("count_cube = %d,count_active_cube = %d count bonus %d",count_cube,count_active_cube,(count_cube+5)/10);
    createMap(CKHelper::Instance().BOX_SIZE, BASE_COUNT_COLUNMS, count_row);
    if(!is_restart)
    {
        createEmitterPool();
    }
    score = 0;
};

void CKCubeArray::createLevel(int _lvl_diff,int _lvl_num)
{
    CKCubeArrayBase::createLevel(_lvl_diff, _lvl_num);
    createEmitterPool();
};

void CKCubeArray::createCubeWithCCArray(CCArray *_array)
{
  //  srand((unsigned)time(0));
    BOX_SIZE = CKHelper::Instance().BOX_SIZE;
    memset(small_map, 0, COUNT_ROWS*COUNT_COLUMNS);
    CCObject * obj = NULL;
    CCARRAY_FOREACH(_array, obj)
    {
       CCPoint pos =  CCPointFromString(static_cast<CCString *>(obj)->getCString());
        small_map[int(pos.x)-1][int(pos.y)-1] = full_cube;
    };
    
    copyCubeTominiCube();
    //CCLOG("createLevel:: count_cube = %d,count_active_cube = %d count bonus %d",count_cube,count_active_cube,(count_cube+5)/10);
    createMap(CKHelper::Instance().BOX_SIZE, BASE_COUNT_COLUNMS, count_row);
    createEmitterPool();
    score = 0;
};

void CKCubeArray::reinitCubeWithCCArray(CCArray *_array)
{
    srand((unsigned)time(0));
    if(!cube_batch)
        m_layer->removeAllChildrenWithCleanup(true);
    else
        cube_batch->removeAllChildrenWithCleanup(true);
    
    if(cube_batch && batch_texture_name != cube_batch->getTexture()->getName())
    {
        batch_texture_name = cube_batch->getTexture()->getName();
        createBatch(batch_file_name.c_str());
    };
    
    memset(small_map, 0, COUNT_ROWS*COUNT_COLUMNS);
    
    CCObject * obj = NULL;
    CCARRAY_FOREACH(_array, obj)
    {
        CCPoint pos =  CCPointFromString(static_cast<CCString *>(obj)->getCString());
        small_map[int(pos.x)-1][int(pos.y)-1] = full_cube;
    };
    
    copyCubeTominiCube();
   // CCLOG("createLevel:: count_cube = %d,count_active_cube = %d count bonus %d",count_cube,count_active_cube,(count_cube+5)/10);
    createMap(CKHelper::Instance().BOX_SIZE, BASE_COUNT_COLUNMS, count_row);
    score = 0;
};
//create sprite "big cube" with array cube_map
//and generation random texture sprite in map
void CKCubeArray::createMap(int _size_box,int _count_col, int _count_row)
{
 //   int add_x = panel_size+_size_box;
//    int add_y = ground_height - _size_box/2;
    for(int i = 1; i <= COUNT_ROWS; ++i)
        for(int j = 0; j < _count_col; ++j)
        {
            if(int(small_map[i-1][j]) != 0)
            {
                int number = cube_type[ck_rand()%cube_type.size()];

                createRTT(CCSprite::createWithSpriteFrameName(cube_map[number].c_str()),number);
#ifdef BATCH_NODE
                
                createMiniCubeBatch(cube_map[number],i-1,j,number);
#else
                
                createMiniCube(CCSprite::createWithSpriteFrameName(cube_map[number].c_str()),i-1,j,number);
#endif
                
            };
        };
    
    for(int i = 0; i < COUNT_ROWS*2; ++i)
        for(int j = 0; j < COUNT_COLUMNS*2; ++j)
        {
            if(int(small_map[i/2][j/2]) != 0)
            {
                addMaskOnCube(j,i);
            };
        };
    destroy_list.reserve(24);
};

CCSprite* CKCubeArray::getRndVisibleMiniCube()
{
    //bool find = false;
    int k;
    int l;
    int loop = 0;
    while (true) { // find cube in array
        k = rand()%COUNT_ROWS;
        l = rand()%COUNT_COLUMNS;
        if(small_map[k][l] != 0)
        {
            //find = true;
            break;
        };
        loop++;
        if(loop > 20)
            return NULL;
    };
    
    int k1 = rand()%2,l1 = rand()%2;
    while (large_map[k*2 + k1][l*2 + l1] == 0) {
        k1 = rand()%2;
        l1 = rand()%2;
    };
   // CCPoint p = sprite_min_map[k*2 + k1][l*2 + l1]->getPosition();

  //  CCLOG("getRndVisibleMiniCube %d %d",k*2 + k1,l*2 + l1);
    return sprite_min_map[k*2 + k1][l*2 + l1];
};

void CKCubeArray::createMiniCube(cocos2d::CCSprite *_sprite,int _x,int _y,int _tag)
{
    CCTexture2D* t_texture = _sprite->getTexture();
   // CCLOG("createMiniCube %d %d",t_texture->getName(),cube_batch->getTexture()->getName());
    float offsetX = _sprite->getTextureRect().origin.x;
    float offsetY = _sprite->getTextureRect().origin.y;
    
    CCSprite* sprite = CCSprite::createWithTexture(t_texture, CCRectMake(0 + offsetX,offsetY + BOX_SIZE / 2,BOX_SIZE / 2,BOX_SIZE / 2));

    sprite->setPosition(ccp((_y+1)*BOX_SIZE + panel_size - BOX_SIZE / 4,(_x)*BOX_SIZE + ground_height +  BOX_SIZE / 4) );
    large_map[_x*2][_y*2] = 1;
    sprite_min_map[_x*2][_y*2] = sprite;
    sprite->setTag(_tag + 1);
    sprite->setUserData(_sprite);
    
    if(!cube_batch)
        m_layer->addChild(sprite,zOrder_Cube);
    else
    {
       // CCAssert(sprite->getTexture()->getName() == cube_batch->getTexture()->getName(), "");
        if (sprite->getTexture()->getName() != cube_batch->getTexture()->getName()) {
            cube_batch->setTexture(t_texture);
           // createBatch(batch_file_name.c_str());
        };
        cube_batch->addChild(sprite);
    };
    
    sprite = CCSprite::createWithTexture(t_texture, CCRectMake(BOX_SIZE / 2 + offsetX ,offsetY + BOX_SIZE / 2,BOX_SIZE / 2,BOX_SIZE / 2));

    sprite->setPosition(ccp((_y+1)*BOX_SIZE + panel_size - BOX_SIZE / 4 + BOX_SIZE / 2,(_x)*BOX_SIZE + ground_height +  BOX_SIZE / 4) );
    large_map[_x*2][_y*2 + 1] = 1;
    sprite_min_map[_x*2][_y*2 + 1] = sprite;
    sprite->setTag(_tag + 2);
    sprite->setUserData(_sprite);
    if(!cube_batch)
        m_layer->addChild(sprite,zOrder_Cube);
    else
        cube_batch->addChild(sprite);
    
    sprite = CCSprite::createWithTexture(t_texture, CCRectMake(BOX_SIZE / 2 + offsetX ,offsetY + 0,BOX_SIZE / 2,BOX_SIZE / 2));

    sprite->setPosition(ccp((_y+1)*BOX_SIZE + panel_size - BOX_SIZE / 4 + BOX_SIZE / 2,(_x)*BOX_SIZE + ground_height +  BOX_SIZE / 4 + BOX_SIZE / 2) );
    large_map[_x*2 + 1][_y*2 + 1] = 1;
    sprite_min_map[_x*2 + 1][_y*2 + 1] = sprite;
    sprite->setTag(_tag + 3);
    sprite->setUserData(_sprite);
    if(!cube_batch)
        m_layer->addChild(sprite,zOrder_Cube);
    else
        cube_batch->addChild(sprite);
   
    sprite = CCSprite::createWithTexture(t_texture, CCRectMake(0 + offsetX,offsetY + 0,BOX_SIZE / 2,BOX_SIZE / 2));
    sprite->setPosition(ccp((_y+1)*BOX_SIZE + panel_size - BOX_SIZE / 4,(_x)*BOX_SIZE + ground_height +  BOX_SIZE / 4 + BOX_SIZE / 2) );
    large_map[_x*2 + 1][_y*2] = 1;
    sprite_min_map[_x*2 + 1][_y*2] = sprite;
    sprite->setTag(_tag + 4);
    sprite->setUserData(_sprite);
    if(!cube_batch)
        m_layer->addChild(sprite,zOrder_Cube);
    else
        cube_batch->addChild(sprite);

};

#pragma mark CUBE_DESTROY
bool CKCubeArray::destroyCube(const unsigned char &_x,const unsigned char &_y,const unsigned char _effect)
{
        destroyMiniCub(_x + 1, _y,_effect);
        destroyMiniCub(_x, _y,_effect);
        destroyMiniCub(_x, _y + 1,_effect);
        destroyMiniCub(_x + 1, _y + 1,_effect);
    return false;
};

bool CKCubeArray::destroyMiniCub(const unsigned char &_x,const unsigned char &_y,const unsigned char _effect)
{
    if(_y < 0 || _x < 0 || _x >= COUNT_COLUMNS*2 || _y >= COUNT_ROWS*2)
        return false;
    
    if(0 == large_map[_y][_x])
        return false;
    
    int x = _x/2;
    int y = _y/2;
    
    if(small_map[y][x] != 0)
    {
        //calculation score while cube not full destroy
        score++;
        small_map[y][x]--;
    };
    
    if(sprite_min_map[_y][_x])
    {
        sprite_min_map[_y][_x]->setVisible(false);
        
        if(SHOW_PARTICLE)
            createEffectDectroyCube(sprite_min_map[_y][_x]->getPosition(),sprite_min_map[_y][_x]->getTag(),_effect);
        sprite_min_map[_y][_x]->setVisible(false);
        cube_batch->removeChild(sprite_min_map[_y][_x],true);
        sprite_min_map[_y][_x] = NULL;
        
       // if(large_map[_y][_x] != 0)
        {
            --count_active_cube;
        };
        
        showMaskCube(_x,_y,true);
        
        large_map[_y][_x] = 0;
    }
    else
        CCLOG("Error: destroyMiniCub It's not active sprite");
    
    large_map[_y][_x] = 0;
    
    destroy_list.push_back(pair<unsigned char, unsigned char>(x,y));

    if(small_map[y][x] == 0)
    {
        CCLOG("it's cube (%d,%d) clear",x,y);
    };
    return true;
};

const int &CKCubeArray::getColumnBaseHeight() const
{
    return base_height;
};

void CKCubeArray::setFreezDestroyCount(bool _b)
{
    is_freez_destroy = _b;
};

void CKCubeArray::jumpCube()
{
   // return;
    //remove dublicate
    std::sort(jump_list.begin(), jump_list.end());
    jump_list.erase(std::unique(jump_list.begin(), jump_list.end()), jump_list.end());
    
    CCLOG("CKCubeArray::jumpCube size %d",jump_list.size());
    unsigned char min_y = COUNT_ROWS;
    jump_it = jump_list.begin();
    while(jump_it != jump_list.end())
    {
        if(jump_it->second < min_y)
        {
            min_y = jump_it->second;
        };
        jump_it++;
    };
    
    jump_it = jump_list.begin();
    while(jump_it != jump_list.end())
    {
        float k_time = float((rand()%10))/80.0f;
        if(jump_it->second == min_y)
        {
        //    k_time = 0.0f;
        };
        
        for (int i = jump_it->first*2; i <= jump_it->first*2 + 1;++i) {
            if(i >= COUNT_COLUMNS*2)
                continue;
            for (int j = jump_it->second*2; j <= jump_it->second*2 + 1;++j) {
                if(large_map[j][i] != 0 && !sprite_min_map[j][i]->getActionByTag(2))
                {

                    sprite_min_map[j][i]->stopAllActions();
                    CCPoint start_pos = ccp(i*BOX_SIZE/2 + panel_size + BOX_SIZE*0.75,j*BOX_SIZE/2 + ground_height+BOX_SIZE/4);
                    CCFiniteTimeAction * jump1 = CCJumpBy::create(0.16, ccp(0,0), float(BOX_SIZE)/10.0, 1);
                    CCFiniteTimeAction * jump2 = CCJumpBy::create(0.1, ccp(0,0), float(BOX_SIZE)/20.0, 1);
                    CCFiniteTimeAction * jump3 = CCJumpBy::create(0.06, ccp(0,0), float(BOX_SIZE)/26.0, 1);
                    CCAction * seq  = CCSequence::create(CCDelayTime::create(k_time),jump1,jump2,jump3,CCMoveTo::create(0.01, start_pos), NULL);
                    sprite_min_map[j][i]->runAction(seq);
                }
            }
        }
        jump_it++;
    };
    jump_list.clear();
};

void CKCubeArray::addJumpCube(const unsigned char _x,const unsigned char _y)
{
    //CCLOG("CKCubeArray::addJumpCube %d %d",_x,_y);
    if(ObjCCalls::isLowCPU())
    {
        if(small_map[_y + 1][_x] == 0)
            jump_list.push_back(pair<unsigned char,unsigned char>(_x,_y));
    }
    else
    {
        int k = _y;
        while (small_map[k][_x] != 0) {
            jump_list.push_back(pair<unsigned char,unsigned char>(_x,k));
            k++;
        };
    }
};

bool CKCubeArray::updateAfterDestroy()
{
    if(destroy_list.empty())
    {
        if(!is_freez_destroy)
            destroy_cube = 0;
        return false;
    };
    count_destroy_cube++;
    destroy_cube++;
    if(destroy_cube > MAX_KOEF_DESTROY_COUNT_CUBE)
        destroy_cube = MAX_KOEF_DESTROY_COUNT_CUBE;
    
    destroy_it = destroy_list.begin();
    int column_x[COUNT_COLUMNS];//columns to update
    memset(column_x, 0, COUNT_COLUMNS*4);//clear array
    
    while(destroy_it != destroy_list.end())
    {
        unsigned char x = (*destroy_it).first;//,y = (*destroy_it).second;
        column_x[x] = 1; //set column for update
        
        addJumpCube((*destroy_it).first,(*destroy_it).second);
        
        //CCLOG("column_x %d",x);
        destroy_it++;
    };
    
    for (int x = 0; x < COUNT_COLUMNS; x++) {
        if(column_x[x] != 0) // if column need update
        {
            int up_cube = (COUNT_ROWS-1),down_cube = 0;//upper cube
            while (small_map[up_cube][x] == 0) { // max height column
                up_cube--;
            };
            if(up_cube == 0)
            {
                continue;
            };
            down_cube = 0;
            while (small_map[down_cube][x] == full_cube && down_cube < up_cube) {
                down_cube++;
            };
            
            CCLOG("updateAfterDestroy::(%d) %d %d",x,down_cube,up_cube);
          //  up_cube++;
            int p = up_cube;
            for(int u = 0; u < (up_cube - down_cube);u++)
            {
                for(int j = down_cube;j < p;j++)
                {
                    moveCubeToDown(x,j);
                };
                p--;
            }
            
            for(int u = down_cube; u < up_cube;u++)
                for(char i = 0; i < 2; ++i)
                    for(char j = 0; j < 2; ++j)
                    {
                        if(large_map[u*2 + i][x*2 + j] != 0)
                        {
                            CCAction * action = CCMoveTo::create(TIME_MOVE_CUBE_DOWN, ccp((x*2 + j)*BOX_SIZE/2 + panel_size + BOX_SIZE/2 + BOX_SIZE/4,(u*2 + i)*BOX_SIZE/2 + ground_height + BOX_SIZE/4));
                            action->setTag(2);
                            sprite_min_map[u*2 + i][x*2 + j]->runAction(action);
                        };
                    }
        }
    }
    
    if(m_bonus)
        m_bonus->findOpenBonus();
    CCLOG("count updateAfterDestroy cube %d",destroy_list.size());
    destroy_list.clear();
    return destroy_cube;
};

void CKCubeArray::showMaskCube(const unsigned char _x,const unsigned char _y,bool show)
{
    
    //add mask to active cube
    if(_y%2 == 1)
    {
        if(large_map[_y - 1][_x]!= 0)
        if(sprite_min_map[_y - 1][_x] != NULL && sprite_min_map[_y - 1][_x]->getChildren())
        {
            if(sprite_min_map[_y - 1][_x]->getChildByTag(1))
                sprite_min_map[_y -1][_x]->getChildByTag(1)->setVisible(show);
        }
    };
    if(_x%2 == 0)
    {
        if(large_map[_y][_x + 1]!= 0)
        if(sprite_min_map[_y][_x + 1] != NULL && sprite_min_map[_y][_x + 1]->getChildren())
        {
            if(sprite_min_map[_y][_x + 1]->getChildByTag(2))
                sprite_min_map[_y][_x + 1]->getChildByTag(2)->setVisible(show);
        }
    }
    else
    {
        if(large_map[_y][_x - 1] != 0)
        if(sprite_min_map[_y][_x - 1] != NULL && sprite_min_map[_y][_x - 1]->getChildren())
        {
            if(sprite_min_map[_y][_x - 1]->getChildByTag(3))
                sprite_min_map[_y][_x - 1]->getChildByTag(3)->setVisible(show);
        }
    }
};

void CKCubeArray::addMaskOnCube(const unsigned char _x,const unsigned char _y)
{
    //add mask to active cube
    if(_y%2 == 0)
    {
        if(large_map[_y][_x] != 0 && sprite_min_map[_y][_x])
        {
            CCSprite* sprite = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(mask_list[ck_rand()%mask_list.size()].c_str()));
            sprite->setPosition(ccp(sprite->getContentSize().width/2,sprite_min_map[_y][_x]->getContentSize().height/2 + ((is_ipad)?3:1)));
            sprite->setTag(1);
            sprite_min_map[_y][_x]->addChild(sprite);
            sprite->setVisible(false);
        };
    };
    if(_x%2 == 1)
    {
        if(large_map[_y][_x] != 0 && sprite_min_map[_y][_x])
        {
            CCSprite* sprite = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(mask_list[ck_rand()%mask_list.size()].c_str()));
            sprite->setPosition(ccp(sprite->getContentSize().height/2 - ((is_ipad)?3:1),sprite_min_map[_y][_x]->getContentSize().height/2));
            sprite->setRotation(-90);
            sprite->setTag(2);
            sprite_min_map[_y][_x]->addChild(sprite);
            sprite->setVisible(false);
        };
    }
    else
    {
        if(large_map[_y][_x] != 0 && sprite_min_map[_y][_x])
        {
            CCSprite* sprite = CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(mask_list[ck_rand()%mask_list.size()].c_str()));
            sprite->setPosition(ccp(sprite->getContentSize().height/2  + ((is_ipad)?3:1) ,sprite_min_map[_y][_x]->getContentSize().height/2));
            sprite->setRotation(90);
            sprite->setTag(3);
            sprite_min_map[_y][_x]->addChild(sprite);
            sprite->setVisible(false);
        };
    }
};


int CKCubeArray::getHeightFirstCollision(const CCPoint &_pos,int _height,int _width,bool big_cube)
{
    int x1 = (_pos.x - panel_size - BOX_SIZE/2 - _width)/(BOX_SIZE/2);
    int y1 = (_pos.y - ground_height - _height)/(BOX_SIZE/2);
    
    float x2f = (_pos.x - panel_size - BOX_SIZE/2 + _width)/(BOX_SIZE/2);
    if(x2f < 0.0)
        return 0;
    
    int x2 = x2f;

    if(big_cube)
    {
        for(int i = y1/2; i >= 0; --i)
            for(int j = x1/2; j <= x2/2;j++)
            {
                if(small_map[i][j] != 0)
                {
                    CCLOG("find i%d j%d",i,j);
                    return (i*BOX_SIZE + ground_height + _height*2);
                };
            }
    }
    else
    for(int i = y1; i >= 0; --i)
        for(int j = x1; j <= x2;j++)
        {
            if(large_map[i][j] == 1)
            {
                CCLOG("getHeightFirstCollision pos int cubei%d j%d",i,j);
                return (i*BOX_SIZE/2 + ground_height + _height*2);
            }
        }
    return 0;
};

int CKCubeArray::checkCollision(CCSprite* _bomb)
{
        int x1 = (_bomb->getPositionX() - panel_size - BOX_SIZE/2 - 0.3*_bomb->getContentSize().width)/(BOX_SIZE/2);
        int y1 = (_bomb->getPositionY() - ground_height - _bomb->getContentSize().height/2)/(BOX_SIZE/2);
        int x2 = (_bomb->getPositionX() - panel_size - BOX_SIZE/2 + 0.3*_bomb->getContentSize().width)/(BOX_SIZE/2);
        int y2 = y1;
    
        if(large_map[y1][x1] != 0 || large_map[y2][x2] != 0)
        {
            int x,y,count = 0;
            if(large_map[y1][x1] != 0)
            {
                destroyMiniCub(x1, y1);
                ++count;
                x = x1;
                y = y1;
            };
            if(large_map[y2][x2] != 0)
            {
                destroyMiniCub(x2, y2);
                ++count;
                x = x2;
                y = y2;
            }
            if(count == 1)
            {
                if(large_map[y - 1][x] != 0)
                {
                    destroyMiniCub(x, y - 1);
                }
            };
            return 1;
        };
    return 0;
};

#pragma mark CUBE_EFFECT
void CKCubeArray::createEmitterPool(int _count)
{
  //  CCDictionary *dict2 = CCDictionary::createWithContentsOfFile(CK::loadFile("destroy_cube1.plist").c_str());//circle
   // CCDictionary *dict1 = CCDictionary::createWithContentsOfFile(CK::loadFile("destroy_cube2.plist").c_str());//cube
    
    for(int i = 0; i < _count;i++)
    {

        CCParticleSystem*  m_emitter1 = CCParticleSystemQuad::create(CK::loadFile("destroy_cube2.plist").c_str());
       // m_emitter->initWithDictionary(dict1);
        
        m_layer->addChild(m_emitter1 ,zOrderParticle);
        //m_emitter->autorelease();

        CCParticleSystem*  m_emitter2 = CCParticleSystemQuad::create(CK::loadFile("destroy_cube1.plist").c_str());
        //m_emitter2->initWithDictionary(dict2);
       // m_emitter2->autorelease();
        m_layer->addChild(m_emitter2 ,zOrderParticle + 1);
        
        m_emitter1->setVisible(false);
        m_emitter2->setVisible(false);
        
        m_emitter1->stopSystem();
        m_emitter2->stopSystem();
        m_emitter1->retain();
        m_emitter2->retain();
        m_emitter1->setAutoRemoveOnFinish(false);
        m_emitter2->setAutoRemoveOnFinish(false);

        //m_emitter->sortAllChildren();
        emitter_list1.push_back(m_emitter1);
        emitter_list2.push_back(m_emitter2);

    }
};

//find particle in pool and run particle on position cube of destroy
void CKCubeArray::createEffectDectroyCube(const CCPoint &_pos,const int &_tag,const unsigned char _effect)
{
    if(!ObjCCalls::isLowCPU())
    {
        emitter_it = emitter_list1.begin();
        while (emitter_it != emitter_list1.end()) {
            if(!(*emitter_it)->isActive())
            {
             //   CCLOG("emitter_it %d %d",(*emitter_it)->isRunning(),(*emitter_it)->isActive());
                (*emitter_it)->setPosition(_pos);
                if(render_texture_map[_tag] == NULL)
                {
                    CCLOG("need prerender %d",_tag);
                    CCAssert(0, "need prerender all texture");
                }
                else
                {
                    if(_effect == 1)
                    {
                        (*emitter_it)->setSpeed(150);
                        (*emitter_it)->setSpeedVar(75);
                        (*emitter_it)->setLife(0.6);
                        (*emitter_it)->setGravity(ccp(0,-400));
                    }
                    else if(_effect == 2)
                    {
                        (*emitter_it)->setSpeed(300);
                        (*emitter_it)->setSpeedVar(120);
                        (*emitter_it)->setLife(1.0);
                        (*emitter_it)->setGravity(ccp(0,-700));
                    }else
                    {
                        (*emitter_it)->setSpeed(200);
                        (*emitter_it)->setSpeedVar(90);
                        (*emitter_it)->setLife(0.8);
                        (*emitter_it)->setGravity(ccp(0,-600));
                    }
                    (*emitter_it)->setTexture(render_texture_map[_tag]);
                    (*emitter_it)->setVisible(true);
                    (*emitter_it)->resetSystem();
                }
                break;
            }
            emitter_it++;
        }
        emitter_it = emitter_list1.begin();
        while (emitter_it != emitter_list1.end()) {
            if(!(*emitter_it)->isActive())
            {
                (*emitter_it)->setVisible(false);
            }
            emitter_it++;
        }
    }
        emitter_it = emitter_list2.begin();
        while (emitter_it != emitter_list2.end()) {
            if(!(*emitter_it)->isActive())
            {
                // CCLOG("emitter_it %d %d",(*emitter_it)->isRunning(),(*emitter_it)->isActive());
                (*emitter_it)->setPosition(_pos);
                if(render_texture_map2[_tag] == NULL)
                {
                    CCLOG("need prerender %d",_tag);
                    CCAssert(0, "need prerender all texture");
                }
                else
                {
                    if(_effect == 1)
                    {
                        (*emitter_it)->setSpeed(80);
                        (*emitter_it)->setSpeedVar(120);
                        (*emitter_it)->setLife(0.6);
                        (*emitter_it)->setGravity(ccp(0,-300));
                    }else if(_effect == 2)
                    {
                        (*emitter_it)->setSpeed(320);
                        (*emitter_it)->setSpeedVar(260);
                        (*emitter_it)->setLife(1.0);
                        (*emitter_it)->setGravity(ccp(0,-700));
                    }
                    else
                    {
                        (*emitter_it)->setSpeed(196);
                        (*emitter_it)->setSpeedVar(223);
                        (*emitter_it)->setLife(0.8);
                        (*emitter_it)->setGravity(ccp(0,-600));
                    }
                    (*emitter_it)->setTexture(render_texture_map2[_tag]);
                    (*emitter_it)->setVisible(true);
                    (*emitter_it)->resetSystem();
                }
                break;
            }
            emitter_it++;
        };
    emitter_it = emitter_list2.begin();
    while (emitter_it != emitter_list2.end()) {
        if(!(*emitter_it)->isActive())
        {
            (*emitter_it)->setVisible(false);
        }
        emitter_it++;
    }

};

void CKCubeArray::createRTT(CCSprite* _base,int _tag)
{
//    CCLOG("createRTT _tag %d",_tag);
//    bool p = false;
    it_map = render_texture_map.begin();
    while (it_map != render_texture_map.end()) {
        if(it_map->first == (_tag + 1))
        {
            //p = true;
            return;
        }
        it_map++;
    };
//    if(p)
//        return;
    
        render_texture_map[_tag + 1] = createMiniTexture(_base,CCRectMake(0,BOX_SIZE / 2,BOX_SIZE / 2,BOX_SIZE / 2));
        render_texture_map2[_tag + 1] = createMiniTexture(_base,CCRectMake(0,BOX_SIZE / 2,BOX_SIZE / 2,BOX_SIZE / 2),1);
    
        render_texture_map[_tag + 2] = createMiniTexture(_base,CCRectMake(BOX_SIZE / 2,BOX_SIZE / 2,BOX_SIZE / 2,BOX_SIZE / 2));
        render_texture_map2[_tag + 2] = createMiniTexture(_base,CCRectMake(BOX_SIZE / 2,BOX_SIZE / 2,BOX_SIZE / 2,BOX_SIZE / 2),1);

        render_texture_map[_tag + 3] = createMiniTexture(_base,CCRectMake(BOX_SIZE / 2,0,BOX_SIZE / 2,BOX_SIZE / 2));
        render_texture_map2[_tag + 3] = createMiniTexture(_base,CCRectMake(BOX_SIZE / 2,0,BOX_SIZE / 2,BOX_SIZE / 2),1);

        render_texture_map[_tag + 4] = createMiniTexture(_base,CCRectMake(0,0,BOX_SIZE / 2,BOX_SIZE / 2));
        render_texture_map2[_tag + 4] = createMiniTexture(_base,CCRectMake(0,0,BOX_SIZE / 2,BOX_SIZE / 2),1);

    
//    for(int i = 1; i < 5;++i)
//    {
//        render_texture_map[_tag + i]->retain();
//        render_texture_map2[_tag + i]->retain();
//    }
};

//Render to texture the base sprite for create particle of destroy effect
CCTexture2D* CKCubeArray::createMiniTexture(CCSprite* _base,const CCRect&_rect,int _tag)
{
    //CCLOG("create new mini texture");
    
    float koef = 1.0;//scale render texture
    
    CCPoint base_pos = _base->getPosition();
    ccBlendFunc m_blend = _base->getBlendFunc();
    
    CCSize size = CCSize(BOX_SIZE/2, BOX_SIZE/2);
   // CCLOG("CKCubeArray::createMiniTexture size %d",int(size.width*size.height*4));
    if(_tag == 1)
    {
        _base->setBlendFunc((ccBlendFunc){GL_DST_ALPHA,GL_SRC_COLOR});
        rt = CCRenderTexture::create(size.width, size.height, kCCTexture2DPixelFormat_RGBA8888, GL_DEPTH24_STENCIL8_OES);
    }
    else
        rt = CCRenderTexture::create(size.width, size.height, kCCTexture2DPixelFormat_RGBA8888, GL_DEPTH24_STENCIL8_OES);
    
    _base->setPosition(ccp(size.width, size.height));
    
    
    splash_sprite->setScaleX(size.width/splash_sprite->getContentSize().width);
    splash_sprite->setScaleY(size.height/splash_sprite->getContentSize().height);
    
    splash_sprite->setPosition(ccp(splash_sprite->getContentSize().width*splash_sprite->getScaleX()*koef/2.0, splash_sprite->getContentSize().height*splash_sprite->getScaleY()*koef/2.0));
    
    _base->setScale(1.0*koef);
    //splash_sprite->setScale(1.0*koef);
    rt->begin();
    if(_tag == 1)
        splash_sprite->visit();
    _base->visit();
    rt->end();
    rt->retain();
  //  CC_SAFE_RELEASE_NULL(rt);
  //  rt_list.push_back(rt);
    
    _base->setBlendFunc(m_blend);
    _base->setPosition(base_pos);
    _base->setScale(1.0);
    
    CCTexture2D *text = rt->getSprite()->getTexture();
    text->retain();
    rt->release();
    return text;
};
#pragma mark CUBE_OPERATINO
//check collision airplane with cube
//return 1 - if not active cube
//return 2 - airplane fan collise with cube
//return 3 - airplane bottom collise with cube, need splash effect
int CKCubeArray::checkAirplane(const cocos2d::CCPoint &_fan,const cocos2d::CCPoint &_bott)
{
  if(count_active_cube <= 0)
      return rAirplane_Win;
    
    int x1 = (_fan.x - panel_size - BOX_SIZE/2)/(BOX_SIZE/2);
    int y1 = (_fan.y - ground_height)/(BOX_SIZE/2);
    if(x1 < 0)
        return 0;
    if(x1 < COUNT_COLUMNS*2 && y1 < COUNT_ROWS*2)
        if(large_map[y1][x1] != 0 ) //airplane fan collision with cube
        {
            return rAirplane_Over;
        };
    int x2 = (_bott.x - panel_size - BOX_SIZE/2)/(BOX_SIZE/2);
    int y2 = (_bott.y - ground_height)/(BOX_SIZE/2);
    if(x2 >= 0 && large_map[y2][x2] != 0)
    {
        return rAirplane_Splash;
    };
    return 0;
};

//pause all effect and shedure in game paused
void CKCubeArray::pause()
{
    emitter_it = emitter_list2.begin();
    while (emitter_it != emitter_list2.end()) {
        //if((*emitter_it)->isActive())
        {
            (*emitter_it)->pauseSchedulerAndActions();
        }
        emitter_it++;
    }
    
    emitter_it = emitter_list1.begin();
    while (emitter_it != emitter_list1.end()) {
        // if((*emitter_it)->isActive())
        {
            (*emitter_it)->pauseSchedulerAndActions();
        }
        emitter_it++;
    };
};

//resuming all shedure after pause
void CKCubeArray::resume()
{
    emitter_it = emitter_list1.begin();
    while (emitter_it != emitter_list1.end()) {
        //     if((*emitter_it)->isActive())
        {
            (*emitter_it)->resumeSchedulerAndActions();
        }
        emitter_it++;
    };
    emitter_it = emitter_list2.begin();
    while (emitter_it != emitter_list2.end()) {
        //if((*emitter_it)->isActive())
        {
            (*emitter_it)->resumeSchedulerAndActions();
        }
        emitter_it++;
    }
};

//phisics move half of cube
void CKCubeArray::moveHalfCube(const unsigned char _x1,const unsigned char _y1,unsigned char _right)
{
    for(char i = 0; i < 2; ++i)
    {
        if(large_map[(_y1 + 1)*2 + i][_x1*2 + _right] == 0)
            continue;
        if(large_map[(_y1)*2 + i][_x1*2 + _right] != 0)
            continue;
        
        large_map[_y1*2 + i][_x1*2 + _right] = large_map[(_y1+1)*2 + i][_x1*2 + _right];
        large_map[(_y1+1)*2 + i][_x1*2 + _right] = 0;
        
        if(sprite_min_map[(_y1 + 1)*2 + i][_x1*2 + _right] != NULL)
        {
            sprite_min_map[_y1*2 + i][_x1*2 + _right] = sprite_min_map[(_y1+1)*2 + i][_x1*2 + _right];
            sprite_min_map[(_y1+1)*2 + i][_x1*2 + _right] = NULL;
        }
    }
};

//move the cube one position down if it can
void CKCubeArray::moveCubeToDown(const unsigned char _x,const unsigned char _y)
{
    if(_y < COUNT_ROWS && _x < COUNT_COLUMNS)
    {
       // CCLOG("moveCubeToDown %d %d value %d",_x,_y + 1,small_map[_y + 1][_x]);
        unsigned char y2 = _y + 1;
        bool p = false;
        
        if(large_map[y2*2][_x*2] != 0 && large_map[y2*2][_x*2 + 1] != 0 && sprite_min_map[y2*2][_x*2]->getUserData() == sprite_min_map[y2*2][_x*2 + 1]->getUserData()) // is two half have one big cube
        {
            if(large_map[_y*2][_x*2] == 0 && large_map[_y*2][_x*2 + 1] == 0)
            {
                moveHalfCube(_x,_y,1);
                moveHalfCube(_x,_y);
                p = true;
            }
        }
        else // move left and right half (isn't one big cube)
        {
            moveHalfCube(_x,_y,1);
            moveHalfCube(_x,_y);
            p = true;
        };
        
        //recalc big cube
        if(p)
        {
            small_map[_y][_x] = 0;
            small_map[y2][_x] = 0;
            for(char i = 0; i < 2; ++i)
                for(char j = 0; j < 2; ++j)
                {
                    small_map[_y][_x] += large_map[_y*2 + i][_x*2 + j];
                    small_map[y2][_x] += large_map[y2*2 + i][_x*2 + j];
                }
        };
    };
};

void CKCubeArray::checkMove(const unsigned char x,const unsigned char y)
{
    unsigned char y1 = y + 1;
    
    int sum = 0;
    if(small_map[y][x] != full_cube)
    {
        
        if(sprite_min_map[y*2][x*2]->getUserData() == sprite_min_map[y*2][x*2 + 1]->getUserData())
        {
            for(int i = 0; i < 2; ++i)
                for(int j = 0; j < 2; ++j)
                {
                    sum += large_map[y*2 + i][x*2 + j]*large_map[y1*2 + i][x*2 + j];
                };
        }
        else
        {
            CCLOG("error");
        }
    }
    else
        for(int i = 0; i < 2; ++i)
            for(int j = 0; j < 2; ++j)
            {
                sum += large_map[y*2 + i][x*2 + j]*large_map[y1*2 + i][x*2 + j];
            };
    
    // if(sum == 0)    //if collision between mini cubes  false that move cube block
   // {
        moveCubeToDown(x,y);
    
  //  }
}

