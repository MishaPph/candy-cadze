//
//  CKLoaderParallax.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 06.12.12.
//
//

#include "CKLoaderParallax.h"
const float MULT_ACCEL = 3.0;
void CKObjectParallax::runAction(const CCPoint &_pos)
{
    if(anim)
    {
        if(anim->b_jump)//is not node it's sprite
        {
            float force = (win_size.width - fabs(m_node->getPositionX() - _pos.x))/win_size.width;
            //CCLOG("CKObjectParallax::runAction force %f",force);
            if(force > 0.2) //not tremor if action of far
            {
                force *= anim->jumping;
                float time = 0.1;
                
                m_node->setPosition(start_pos);
                CCAction *action = CCSequence::create(CCDelayTime::create(rand()%10*0.01*force), CCMoveBy::create(time, ccp(0,6*force)),CCMoveBy::create(time, ccp(0,-9*force)),CCMoveBy::create(time*0.8, ccp(0,6*force)),CCMoveBy::create(time*0.8, ccp(0,-4*force)),CCMoveBy::create(time*0.5, ccp(0,2*force)),CCMoveBy::create(time*0.5, ccp(0,-1*force)),NULL);
                m_node->runAction(action);
            }
            
        };
        
        if(anim->b_rotate) //tremor object
        {
            float force = (win_size.width - fabs(m_node->getPositionX() - _pos.x))/win_size.width;
            //   CCLOG("CKObjectParallax::runAction force %f",force);
            if(force > 0.3) //not tremor if action of far
            {
                force *= anim->amplitude_rotate;
                float time = 0.1;
                m_node->setRotation(0);
                CCAction *action = CCSequence::create(CCDelayTime::create(rand()%10*0.01*force),CCRotateBy::create(time, 6*force),CCRotateBy::create(time, -9*force),CCRotateBy::create(time*0.8, 6*force),CCRotateBy::create(time*0.8, -4*force),CCRotateBy::create(time*0.5, 2*force),CCRotateTo::create(time*0.5, 0),NULL);
                m_node->runAction(action);
            }
        };
    }
    
    it = list_asp.begin();
    while(it != list_asp.end())
    {
        if((*it)->isAction())
            static_cast<CKObjectParallax*>(*it)->runAction(_pos);
        it++;
    };
};

#pragma mark - PARALLAX NODE
CKParalaxNode::~CKParalaxNode()
{
    
};

void CKParalaxNode::update(const float &_dt)
{
    if(!is_create)
        return;
    update_accel.x += (diff_accel.x - update_accel.x)*0.05;
    update_accel.y += (diff_accel.y - update_accel.y)*0.05;

    it = list_asp.begin();
    while(it != list_asp.end())
    {
        static_cast<CKObjectParallax*>(*it)->update(update_accel);
        it++;
    };
};

void CKParalaxNode::didAccelerate(CCAcceleration* pAccelerationValue)
{
    if(pAccelerationValue->x > 0.0)
    {
        diff_accel.x = MIN(pAccelerationValue->x*MULT_ACCEL,1.0);
    }
    else
    {
        diff_accel.x = MAX(pAccelerationValue->x*MULT_ACCEL,-1.0);
    }
    if(pAccelerationValue->y > 0.0)
    {
        diff_accel.y = MIN(pAccelerationValue->y*MULT_ACCEL,1.0);
    }
    else
    {
        diff_accel.y = MAX(pAccelerationValue->y*MULT_ACCEL,-1.0);
    }
};

void CKParalaxNode::runAction(const CCPoint &_pos)
{
    it = list_asp.begin();
    while(it != list_asp.end())
    {
        if((*it)->isAction())
        {
            static_cast<CKObjectParallax*>(*it)->runAction(_pos);
        };
        it++;
    };
};
