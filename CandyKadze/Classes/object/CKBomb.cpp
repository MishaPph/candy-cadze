//
//  CKBomb.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 20.09.12.
//
//

#include "CKBomb.h"
#include "CKAirplane.h"

const unsigned char BOMB_POOL_SIZE = 30;
#define left_border _bomb->sprite->getPositionX() - BOX_SIZE/2 - panel_left

#pragma mark - BOMB_EFFECT
BombEffect::BombEffect()
{
    BOX_SIZE = CKHelper::Instance().BOX_SIZE;
};
BombEffect::~BombEffect()
{
    it_exp = list_explose.begin();
    while (it_exp != list_explose.end()) {
        //(*it_exp)->sprite->removeFromParentAndCleanup(true);
        (*it_exp)->sprite->release();
        (*it_exp)->anim->release();
        CC_SAFE_DELETE(*it_exp);
        it_exp++;
    };
    list_explose.clear();
};

void BombEffect::setGroundHeight(int _ground_height)
{
    ground_height = _ground_height;
};

void BombEffect::create(cocos2d::CCNode *_layer, int _count)
{
    CCAnimation* animat = CCAnimation::create();
 //   CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(CK::loadImage("a_Boom.plist").c_str());
    for (int i = 0; i < 15; i++) {
        char str[255];
        sprintf(str, "boom%d.png",i+1);
        animat->addSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str));
    };
    animat->setDelayPerUnit(0.04);
    
    for(int k = 0; k < _count; k++)
    {
        CCSprite* sprite = CCSprite::createWithSpriteFrameName("boom15.png");
        sprite->setVisible(false);
        sprite->retain();
        _layer->addChild(sprite,zOrderParticle);
        CCFiniteTimeAction* action = CCAnimate::create(animat);
        ExploseEff *expl = new ExploseEff;

        expl->sprite = sprite;
        expl->is_active = false;
        
        CCAction* seq = CCSequence::create(action,CCCallFuncND::create(sprite, callfuncND_selector(BombEffect::callEnd), expl),NULL);
        seq->setTag(2);
        seq->retain();
        expl->anim = seq;
        list_explose.push_back(expl);
    };
};

void BombEffect::run(const cocos2d::CCPoint &_p,const float &_size)
{
    it_exp = list_explose.begin();
    while (it_exp != list_explose.end()) {
        if(!(*it_exp)->is_active)
        {
            (*it_exp)->is_active = true;
            (*it_exp)->sprite->setPosition(ccp(_p.x,MAX(_p.y,ground_height)));
            (*it_exp)->sprite->setRotation(rand()/180 - 90);
            (*it_exp)->sprite->setVisible(true);
            (*it_exp)->sprite->runAction((*it_exp)->anim);
            (*it_exp)->sprite->setScale(_size);
            break;
        };
        it_exp++;
    };
};

void BombEffect::show(const cocos2d::CCPoint &_p,const unsigned char &_id)
{
    CCLOG("BombEffect::show %d",_id);
    switch (_id) {
        case 1:
            run(_p,0.5);
            break;
        case 21:
            run(ccp(_p.x,_p.y),1.0);
            break;
        case 22:
            run(ccp(_p.x,_p.y),1.5);
            break;
        case 23:
            run(ccp(_p.x,_p.y),1.5);
            run(ccp(_p.x + BOX_SIZE/2,_p.y - BOX_SIZE/2),1.00);
            run(ccp(_p.x - BOX_SIZE/2,_p.y - BOX_SIZE/2),1.00);
            break;
        case 31: //big width
            run(_p,1.0);
            break;
        case 32:
            run(ccp(_p.x + BOX_SIZE/2,_p.y),0.8);
            run(ccp(_p.x,_p.y),0.8);
            break;
        case 33:
            run(ccp(_p.x + BOX_SIZE/2,_p.y),0.75);
            run(ccp(_p.x,_p.y),1.0);
            run(ccp(_p.x - BOX_SIZE/2,_p.y),0.75);
            break;
        case 41: //deep
            run(_p,1.0);
            run(ccp(_p.x + BOX_SIZE*0.75,_p.y),0.8);
            run(ccp(_p.x - BOX_SIZE*0.75,_p.y),0.8);
            break;
        case 42:
            run(_p,1.0);
            run(ccp(_p.x + BOX_SIZE*0.75,_p.y),0.8);
            run(ccp(_p.x - BOX_SIZE*0.75,_p.y),0.8);
            run(ccp(_p.x + BOX_SIZE*1.5,_p.y),0.6);
            run(ccp(_p.x - BOX_SIZE*1.5,_p.y),0.6);
            break;
        case 43:
            run(_p,1.0);
            run(ccp(_p.x + BOX_SIZE*0.75,_p.y),1.0);
            run(ccp(_p.x - BOX_SIZE*0.75,_p.y),1.0);
            run(ccp(_p.x + BOX_SIZE*1.6,_p.y),0.8);
            run(ccp(_p.x - BOX_SIZE*1.6,_p.y),0.8);
            run(ccp(_p.x + BOX_SIZE*2.45,_p.y),0.6);
            run(ccp(_p.x - BOX_SIZE*2.45,_p.y),0.6);
            break;
        case 67:
            run(_p,0.4);
            break;
        case 71: //touch
            run(_p,1.0);
            run(ccp(_p.x + BOX_SIZE*0.5,_p.y),0.65);
            run(ccp(_p.x - BOX_SIZE*0.5,_p.y),0.65);
            break;
        case 72:
            run(_p,1.0);
            run(ccp(_p.x + BOX_SIZE*0.75,_p.y),1.0);
            run(ccp(_p.x - BOX_SIZE*0.75,_p.y),1.0);
            run(ccp(_p.x + BOX_SIZE*1.3,_p.y),0.65);
            run(ccp(_p.x - BOX_SIZE*1.3,_p.y),0.65);
            break;
        case 73:
            run(_p,1.0);
            run(ccp(_p.x + BOX_SIZE*0.75,_p.y),1.0);
            run(ccp(_p.x - BOX_SIZE*0.75,_p.y),1.0);
            run(ccp(_p.x + BOX_SIZE*1.3,_p.y),0.85);
            run(ccp(_p.x - BOX_SIZE*1.3,_p.y),0.85);
            run(ccp(_p.x + BOX_SIZE*2.0,_p.y),0.65);
            run(ccp(_p.x - BOX_SIZE*2.0,_p.y),0.65);
            break;
        case 81: //parabola
            run(_p,0.5);
            break;
        case 82: //parabola
            run(_p,0.75);
            break;
        case 83: //parabola
            run(_p,1.0);
            break;
        case 85: //parabola
            run(_p,0.5);
            break;
        case 86: //parabola
            run(_p,0.75);
            break;
        case 87: //parabola
            run(_p,1.0);
            break;
        case 131://kaskad
        case 132:
        case 133:
            run(_p,0.5);
            break;
        case 121://krest
            run(ccp(_p.x,_p.y),1.50);
            run(ccp(_p.x,_p.y - BOX_SIZE/2),1.00);
            run(ccp(_p.x + BOX_SIZE/2,_p.y+ BOX_SIZE/2),1.00);
            run(ccp(_p.x - BOX_SIZE/2,_p.y+ BOX_SIZE/2),1.00);
            break;
        case 122:
            run(ccp(_p.x,_p.y),1.50);
            run(ccp(_p.x,_p.y - BOX_SIZE/2),1.20);
            run(ccp(_p.x + BOX_SIZE*0.75,_p.y+ BOX_SIZE/2),1.20);
            run(ccp(_p.x - BOX_SIZE*0.75,_p.y+ BOX_SIZE/2),1.20);
            break;
        case 123:
            run(_p,1.50);
            run(ccp(_p.x,_p.y - BOX_SIZE*0.75),1.20);
            run(ccp(_p.x + BOX_SIZE*0.75,_p.y+ BOX_SIZE/2),1.20);
            run(ccp(_p.x - BOX_SIZE*0.75,_p.y+ BOX_SIZE/2),1.20);
            break;
        default:
            run(_p,1.0);
            break;
    }
};

void BombEffect::hideAll()
{
    it_exp = list_explose.begin();
    while (it_exp != list_explose.end()) {
        if((*it_exp)->is_active)
        {
            (*it_exp)->is_active = false;
            (*it_exp)->sprite->setVisible(false);
        };
        it_exp++;
    };
};

void BombEffect::callEnd(CCNode *_node,void *_data)
{
    ExploseEff *expl = static_cast<ExploseEff *>(_data);
    expl->is_active = false;
    expl->sprite->setVisible(false);
}

#pragma mark - BOMB NEW
CKBombs::~CKBombs()
{
    CCLOG("~CKBombs");
    it = list_bomb.begin();
    while(it != list_bomb.end())
    {
        delete *it;
        (*it) = NULL;
        it++;
    };
    CC_SAFE_DELETE(m_effect);
  //  CC_SAFE_RELEASE_NULL(anim);
};

CKBombs::CKBombs()
{
    stop_update = false;
    streak = NULL;
    is_airplane_bomb = true;
    count_free_bomb = 0;
    curent_kaskad_type = 1;
    count_kaskad_bomb = 0;
    need_bombs = 0;
    tmp_sprite = NULL;
    create_new_bomb = true;
    firs_bomb = true;
    touch_bomb = NULL;
    count_fail_bomb = 0;
    count_hit_bomb = 0;
    count_spec_bombs = 0;
    m_effect = new BombEffect;
};

bool CKBombs::init()
{
    CCLOG("CKBombs::init");
    win_size = CKHelper::Instance().getWinSize();
    BOX_SIZE = CKHelper::Instance().BOX_SIZE;
    //preload bombs
    for(int i = 0;i < BOMB_POOL_SIZE;++i)
    {
        Sbomb *mbomb = new Sbomb;
        mbomb->is_active = false;
        mbomb->sprite = CCSprite::createWithSpriteFrameName(filename.c_str());
        mbomb->sprite->setVisible(false);
        mbomb->sprite->setAnchorPoint(ccp(0.5,0.5));
        mbomb->previor_type = 0;
        m_layer->addChild(mbomb->sprite,CK::zOrder_Cube);
        count_free_bomb++;
        
        mbomb->type = 1;
        list_bomb.push_back(mbomb);
    };
    return true;
}; 

void CKBombs::copy(const CKBombs *_bomb)
{
    this->m_layer = _bomb->m_layer;
    this->speed = _bomb->speed;
    // m_bombs->setBombName(bomb_name);
    this->ground_height = _bomb->ground_height;
    this->m_airplane = _bomb->m_airplane;
    this->m_bonus = _bomb->m_bonus;
    this->m_array = _bomb->m_array;
};

void CKBombs::setAirplane(CKAirplane *_airplane)
{
    m_airplane = _airplane;
};

void CKBombs::createNewBomb(bool _t)
{
    create_new_bomb = _t;
};

bool CKBombs::isNewBomb()
{
    return create_new_bomb;
};

bool CKBombs::isActiveBomb()
{
    return !(count_free_bomb == BOMB_POOL_SIZE);
};

int CKBombs::getCountCreateSpecBomb()
{
    return count_spec_bombs;
};

bool CKBombs::create(const CCPoint& _p,const unsigned char &_id)
{
    if(!tmp_sprite)
    {
        tmp_sprite = CCSprite::create("cube90.png");
        m_layer->addChild(tmp_sprite,55);
        tmp_sprite->setVisible(false);
    };
    
    touch_bomb = NULL;
    start_accele = false;
    
    if(is_airplane_bomb && !create_new_bomb) //67 is kulyet 68 - kamikadze and bomber and ghost
            return false;
    
    CCLOG("CKBombs::create count_free_bomb %d type %d create_new_bomb(%d)",count_free_bomb ,_id,create_new_bomb);
    for (it = list_bomb.begin(); it != list_bomb.end(); it++)
    {
        if((*it)->is_active == false)
        {
            if(is_airplane_bomb)
            {
                create_new_bomb = false;
            };
            
            count_create_bomb++;
            if(_id != 1 && is_airplane_bomb)
                count_spec_bombs++;
            
            bomb_blink = false;
            
            if(int(_id/10) == 16 )//kylimet
            {
                CKAudioMng::Instance().playEffect("machine_gun");
                m_airplane->startMachineGun();
            }
            else if(_id == 67)
            {
                CKAudioMng::Instance().playEffect("machine_down");
            }
            else if(int(_id/10) == 14)//speed
            {
                CKAudioMng::Instance().playEffect("speed_bomb");
            }
            else if(int(_id/10) == 7)//tap
            {
                CKAudioMng::Instance().playEffect("tap_bomb_flight");
            }
            else if(int(_id/10) == 5)//lazer
            {
                CKAudioMng::Instance().playEffect("laser");
            }
            else if(int(_id/10) == 6 || int(_id/10) == 11)//lazer
            {
                CKAudioMng::Instance().playEffect("tomahawk_flight");
            }else
            {
                CKAudioMng::Instance().playEffect("launch");
            }
            
            
            collision_kaskad_bomb = false;
            collision_parabola  = false;
            CCAssert((*it)->sprite, "Error:: not init sprite bomb");
            
            (*it)->sprite->setAnchorPoint(ccp(0.5,0.5));
            
            m_array->setFreezDestroyCount(false);
            CCSprite& sprite = *(*it)->sprite;
            if((*it)->previor_type != _id)
            {
                CCSpriteFrame* s_frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(CKBonusMgr::Instance().getNameById(_id).c_str());
                if(s_frame)
                {
                    sprite.setTexture(s_frame->getTexture());
                    sprite.setTextureRect(s_frame->getRect(), s_frame->isRotated(), sprite.getContentSize());
                    sprite.setContentSize(s_frame->getRect().size);
                }
                else{
                    CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile(CK::loadImage("a_Plane.plist").c_str());
                    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(CK::loadImage("a_Plane.plist").c_str());
                    CCSpriteFrame* s_frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(CKBonusMgr::Instance().getNameById(_id).c_str());
                    if(s_frame)
                    {
                        sprite.setTexture(s_frame->getTexture());
                        sprite.setTextureRect(s_frame->getRect(), s_frame->isRotated(), sprite.getContentSize());
                        sprite.setContentSize(s_frame->getRect().size);
                    }
                    CCLOG("create:: Error loading spriteFrame %s",CKBonusMgr::Instance().getNameById(_id).c_str());
                }
            };

            CCSpriteFrame* s_frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(CKBonusMgr::Instance().getNameById(_id).c_str());
            if(s_frame)
            {
                sprite.setTexture(s_frame->getTexture());
                sprite.setTextureRect(s_frame->getRect(), s_frame->isRotated(), sprite.getContentSize());
                sprite.setContentSize(s_frame->getRect().size);
            };
            
            sprite.setVisible(true);
            sprite.stopAllActions();
            sprite.setOpacity(255);
            sprite.setScale(1.0);
            sprite.setRotation(0);
            
          //  (*it)->sprite->setContentSize(sprite->getTexture()->getContentSize());
            sprite.setPosition(_p);
            sprite.setTag(2);
            
            (*it)->line_velosity = 0.1;
            
            (*it)->is_active = true;
            (*it)->type = _id;
            (*it)->previor_type = _id;
            (*it)->is_airplane_bomb = is_airplane_bomb;
            is_airplane_bomb = true;
            (*it)->previor_pos = CCPointZero;
            
            (*it)->speed = speed* CKBonusMgr::Instance().getInfoById(_id)->speed_mult;
            (*it)->m_func = getFuncById(_id);
            
            if(CKBonusMgr::Instance().getInfoById(_id)->start_action == 1)
            {
                sprite.setScale(0.5);
                CCAction *action = CCScaleTo::create(50/speed, 1.0);
                sprite.runAction(action);
            }
            else if(CKBonusMgr::Instance().getInfoById(_id)->start_action == 2)
            {
                sprite.setScaleX(0.5);
                CCAction *action = CCScaleTo::create(50/speed, 1.0,1.0);
                sprite.runAction(action);
            }else if(CKBonusMgr::Instance().getInfoById(_id)->start_action == 10)//animation sprite
            {
                
                sprite.setScale(0.5);
                CCActionInterval *scale = CCScaleTo::create(50/speed, 1.0);
                //sprite.runAction(action);
                
                CCAnimation * anim = CCAnimation::create();
                std::string tmp_str = CKBonusMgr::Instance().getNameById(_id);
                
              //  anim->addSpriteFrameWithFileName(CK::loadImage(tmp_str.c_str()).c_str());
                anim->addSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(tmp_str.c_str()));
              //  anim->addSpriteFrameWithFileName(tmp_str.c_str());
                char str[NAME_MAX];
                
                int nPosRight  = tmp_str.find('.');
                std::string _sufix = tmp_str.substr(nPosRight + 1,tmp_str.length());
                tmp_str = tmp_str.substr(0, nPosRight);
                
                sprintf(str, "%s_button.png",tmp_str.c_str());
                //anim->addSpriteFrameWithFileName(CK::loadImage(str).c_str());
                anim->addSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str));
                anim->setDelayPerUnit(0.075);
                CCAnimate* animate = CCAnimate::create( anim );
                CCActionInterval* action = CCRepeatForever::create( animate );
                action->setTag(9);
                sprite.runAction(CCSequence::create(scale,action,NULL) );
            }else
            {
                sprite.setScale(0.0);
                CCAction *action = CCScaleTo::create(20/speed, 1.0,1.0);
                sprite.runAction(action);
            }
            
            switch (_id) {
                case 1:
                    (*it)->distanse = _p.y - m_array->getHeightFirstCollision(_p,sprite.getContentSize().height/2,0.44*sprite.getContentSize().width);
                { 
                    (*it)->sprite->setRotation(-45);
                    CCAction *rotate = CCRotateTo::create(0.2, 0);
                    (*it)->sprite->runAction(rotate);
                }
                    break;
                case 21:
                case 22:
                case 23:
                    (*it)->distanse = _p.y - m_array->getHeightFirstCollision(_p,sprite.getContentSize().height/2,sprite.getContentSize().width*0.5,true);
                    //  tmp_sprite->setPosition(ccp(_p.x,m_array->getHeightFirstCollision(_p,sprite->getContentSize().height/2,sprite->getContentSize().width*0.5,true)));
                    break;
                case 31:
                    (*it)->distanse = _p.y - m_array->getHeightFirstCollision(_p,sprite.getContentSize().height/2,sprite.getContentSize().width*0.48);
                    break;
                case 32:
                    (*it)->distanse = _p.y - m_array->getHeightFirstCollision(_p,sprite.getContentSize().height/2,sprite.getContentSize().width*0.44);
                    break;
                case 33:
                    (*it)->distanse = _p.y - m_array->getHeightFirstCollision(_p,sprite.getContentSize().height/2,sprite.getContentSize().width*0.48);
                    break;
                case 41:
                case 42:
                case 43:
                    (*it)->distanse = _p.y - m_array->getHeightFirstCollision(_p,sprite.getContentSize().height/2,0.3*sprite.getContentSize().width);
                    bomb_blink = true;
                    break;
                case 51:
                case 52:
                case 53://lazer
                {
                    (*it)->sprite->setAnchorPoint(ccp(0.5,0.0));
                    (*it)->sprite->setPosition(ccp(_p.x,_p.y));
                    (*it)->previor_pos =  ccp(_p.x,_p.y + (*it)->sprite->getContentSize().height);
                    (*it)->distanse = _p.y - m_array->getHeightFirstCollision(_p,sprite.getContentSize().height/2,0) + BOX_SIZE*2 - BOX_SIZE*0.27;
                    
                    (*it)->distanse += BOX_SIZE*2;
                    
                    if((*it)->type == 52)
                        (*it)->distanse += BOX_SIZE;
                    if((*it)->type == 53)
                        (*it)->distanse += BOX_SIZE*4;
                    float scale = ((*it)->previor_pos.y - (*it)->sprite->getPositionY() - (*it)->sprite->getContentSize().height)/(*it)->sprite->getContentSize().height;
                    (*it)->sprite->stopAllActions();
                    sprite.setScale(1.0);
                    (*it)->sprite->setScaleY(scale);
                    m_layer->reorderChild(&sprite, CK::zOrder_Cube - 1);
                }
                    break;
                case 61:
                case 62:
                case 63://auto find max height
                {
                    if(!streak)
                    {
                        streak = CCMotionStreak::create(0.3, 0.2, BOX_SIZE/4, ccWHITE, "streak_red.png");
                        m_layer->addChild(streak,CK::zOrder_Cube-1);
                    }
                    else
                    {
                        streak->setVisible(true);
                    };
                    streak->setTexture(CCTextureCache::sharedTextureCache()->addImage("streak_red.png"));
                    streak->reset();
                    
                    tmp_sprite->setPosition(ccp(_p.x,_p.y));
                    touch_bomb = (*it);
                    (*it)->distanse = 1;
                    point_touch = _p;
                    direct.x = 0;
                    direct.y = 1.0;
                    direct2.x = 0;
                    direct2.y = 1.0;
                    CCPoint posS = (*it)->sprite->getPosition();
                    CCPoint pos = findMaxHeight((*it));
                    if(int(pos.x) == 0)
                    {
                        pos.x = _p.x;
                    };
                   // pos = ccp(0,0);
                    touch_bomb = NULL;
                    createTrackMoved(posS,pos);
                }
                    break;
                case 68://kamikadze
                {
                    CCPoint posS = (*it)->sprite->getPosition();
                    CCPoint pos = ccp((*it)->sprite->getPositionX(),0);
                    (*it)->speed *= 0.5;
                    int y1 = (posS.y - ground_height - (*it)->sprite->getContentSize().height/2)/(BOX_SIZE);
                    bool find = false;
                    (*it)->distanse = 1;
                    for(int j = y1; j >= 0; --j)
                    {
                        if(find)
                            break;
                        for(int i = 0; i < COUNT_COLUMNS;++i)
                        {
                            if(m_array->getBigCube(j,i) != 0)
                            {
                                pos =  ccp((i)*(BOX_SIZE) + panel_left + BOX_SIZE ,j*(BOX_SIZE)+ ground_height + (*it)->sprite->getContentSize().height/2 + BOX_SIZE/2);
                                find = true;
                                (*it)->previor_pos =  pos;
                                break;
                            }
                        };
                    };

                    
                }
                    break;
                case 71:
                case 72:
                case 73:
                    touch_bomb = (*it);
                    (*it)->distanse = _p.y - m_array->getHeightFirstCollision(_p,sprite.getContentSize().height/2,0.45*sprite.getContentSize().width) + BOX_SIZE/4;
                    break;
                case 81:
                case 82:
                case 83:
                    collision_parabola = true;
                    
                    (*it)->distanse = _p.y - m_array->getHeightFirstCollision(_p,sprite.getContentSize().height/2,0.3*sprite.getContentSize().width);
                    break;
                case 111:
                case 112:
                case 113://touch and moved
                    
                    if(!streak)
                    {
                        streak = CCMotionStreak::create(0.3, 0.2, BOX_SIZE/4, ccWHITE, "streak_blue.png");
                        m_layer->addChild(streak,CK::zOrder_Cube-1);
                    }
                    else
                    {
                        streak->setVisible(true);
                    };
                    streak->setTexture(CCTextureCache::sharedTextureCache()->addImage("streak_blue.png"));
                    streak->reset();
                    tmp_sprite->setPosition(ccp(_p.x,_p.y));
                    touch_bomb = (*it);
                    (*it)->distanse = 1;
                    point_touch = _p;
                    direct.x = 0;
                    direct.y = 1.0;
                    direct2.x = 0;
                    direct2.y = 1.0;
                    break;
                case 121:
                case 122:
                case 123:
                    (*it)->distanse = _p.y - m_array->getHeightFirstCollision(_p,sprite.getContentSize().height/2,0.4*sprite.getContentSize().width);
                    break;
                case 131:
                case 132:
                case 133://kaskad
                    collision_kaskad_bomb = true;
                    if(toPoint.x != _p.x)//firs kaskad bomb
                    {
                        hit_kaskad_bomb = false;
                        count_kaskad_bomb = 0;
                        delete_kaskad_bomb = 0;
                        need_bombs = 2;
                        if(_id == 132)
                            need_bombs = 3;
                        if(_id == 133)
                            need_bombs = 4;
                        curent_kaskad_type = _id;
                        toPoint = _p;
                        toPoint.x = int(toPoint.x/(BOX_SIZE*0.5))*BOX_SIZE*0.5 + BOX_SIZE*0.25;
                        // CCLOG("toPoint x(%f)",toPoint.x);
                        (*it)->sprite->setPosition(toPoint);
                    }
                    (*it)->distanse = toPoint.y - m_array->getHeightFirstCollision(toPoint,sprite.getContentSize().height/2,0.45*sprite.getContentSize().width);
                    break;
                case 151:
                case 152:
                case 153://accele
                    //(*it)->sprite->setAnchorPoint(ccp(0.5,0.47));
                    //CCLOG("setAnchorPoint %f %f",sprite->getAnchorPoint().x,sprite->getAnchorPoint().y);
                    is_ground = false;
                    bomb_speed = 0.0;
                    (*it)->distanse = 1.0;
                    break;
                    
                case 161:
                case 162:
                case 163:
                {
                    (*it)->distanse = 1.0;
                    (*it)->sprite->setVisible(false);
                    gun_posX =  m_airplane->getPosition().x;
                }
                    break;
                default:
                    (*it)->distanse = _p.y - m_array->getHeightFirstCollision(_p,sprite.getContentSize().height/2,0.3*sprite.getContentSize().width);
                    break;
            }
            count_free_bomb--;
            break;
        };
    };
    return true;
};

void CKBombs::createMini(const CCPoint& _p,const CCPoint& _end,const unsigned char &_id)
{
    for (list< Sbomb* >::iterator it2 = list_bomb.begin(); it2 != list_bomb.end(); it2++)
    {
        if((*it2)->is_active == false)
        {
            //CKAudioMng::Instance().playEffect("launch");
            CCSprite* sprite = (*it2)->sprite;//CCSprite::create(loadImage(CKBonusMgr::Instance().getNameById(_id).c_str()).c_str());
            
            CCSpriteFrame* s_frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(CKBonusMgr::Instance().getNameById(_id).c_str());
            if(s_frame)
            {
                sprite->setTexture(s_frame->getTexture());
                sprite->setTextureRect(s_frame->getRect(), s_frame->isRotated(), sprite->getContentSize());
            }
            else{
                CCLOG("createMini:: Error loading spriteFrame");
            }

            sprite->setPosition(_p);
            sprite->setTag(2);
            
          //  m_layer->addChild(sprite,CK::zOrder_Cube+2);
            
            (*it2)->sprite = sprite;
            sprite->setScale(1.0);
            sprite->setVisible(true);
            (*it2)->line_velosity = 0.1;
            (*it2)->is_active = true;
            (*it2)->type = _id;
            (*it2)->speed = speed* CKBonusMgr::Instance().getInfoById(_id)->speed_mult;
            (*it2)->distanse = 1.0;
            (*it2)->m_func = getFuncById(_id);
            
            ccBezierConfig bezier;
            bezier.controlPoint_1 = ccp(_p.x,MAX(_p.y + BOX_SIZE/2,_end.y + BOX_SIZE/2)); // control point 1
            bezier.controlPoint_2 = ccp(_end.x,MAX(_p.y + BOX_SIZE/2,_end.y + BOX_SIZE/2)); // control point 2
            bezier.endPosition = ccp(_end.x,_end.y);
            CCFiniteTimeAction *action = CCBezierTo::create(CK::getPointLength(_p, _end)/250, bezier);
            
            action->setTag(2);
            sprite->runAction(action);
            count_free_bomb--;
            CCLOG("complite");
            return;
        };
    };
    
};

typedef bool (CKBombs::*func)(Sbomb *);
func CKBombs::getFuncById(const unsigned char &_id)
{
    switch (_id) {
        case 1:
            if(CKFileOptions::Instance().isKidsModeEnabled())
               return &CKBombs::collision141; //
            else
                return &CKBombs::collision131;
            break;
        case 141:
        case 142:
        case 143:
            return &CKBombs::collision141;
            break;
        case 21:
        case 22:
        case 23:
            return &CKBombs::collision21;
            break;
        case 31:
        case 32:
        case 33:
            return &CKBombs::collision31;
            break;
        case 41:
        case 42:
        case 43:
            return &CKBombs::collision41;
            break;
        case 51:
        case 52:
        case 53:
            return &CKBombs::collision51;
            break;
        case 61:
        case 62:
        case 63:
            return &CKBombs::collision61;
            break;
        case 67:
            return &CKBombs::collision131;
            break;
        case 68://kamikadze
            return &CKBombs::collisionKamikadze;
            break;
        case 71:
        case 72:
        case 73:
            return &CKBombs::collision11;
            break;
        case 81:
        case 82:
        case 83:
            return &CKBombs::collision81;
            break;
        case 85:
        case 86:
        case 87:
            return &CKBombs::collision85;
            break;
        case 111:
        case 112:
        case 113:
            return &CKBombs::collision111;
            break;
        case 121:
        case 122:
        case 123:
            return &CKBombs::collision121;
            break;
        case 131:
        case 132:
        case 133:
            return &CKBombs::collision131;
            break;
        case 161:
        case 162:
        case 163:
            return &CKBombs::collision161;
            break;
        default:
            return &CKBombs::collision11;
            break;
    }
};

void CKBombs::setIsAirplaneBomb(bool _airplane)
{
    is_airplane_bomb = _airplane;
};

void CKBombs::createDestroyEffect(CCNode * _batch)
{
    m_effect->create(m_layer,10);
    m_effect->setGroundHeight(ground_height);
};

void CKBombs::hideNode(cocos2d::CCNode *_sender)
{
    _sender->setVisible(false);
};

void CKBombs::hideAllDestroyEffect()
{
    m_effect->hideAll();
};

void CKBombs::restart()
{
    stop_update = false;
    is_airplane_bomb = true;
    count_free_bomb = 0;
    curent_kaskad_type = 1;
    count_kaskad_bomb = 0;
    need_bombs = 0;
    
    create_new_bomb = true;
    firs_bomb = true;
    
    count_free_bomb = BOMB_POOL_SIZE;
    
    if(!list_bomb.empty())
    {
        for (it = list_bomb.begin(); it != list_bomb.end(); it++)
        {
            if(!(*it)->is_active)
                continue;
            if(!(*it)->sprite)
                continue;
            // CCLOG("find bonus bonus type %d",(*it)->type);
            
            if(!(*it))
                continue;
            (*it)->is_active = false;
            (*it)->type = 1;
            m_layer->removeChild((*it)->sprite, true);
            (*it)->sprite = NULL;
        }
    }
};

void CKBombs::deleteBomb(int _t,Sbomb * _bomb)
{
    if(_bomb->type == 68)// kamikadze
    {
        CKAudioMng::Instance().playEffect("kamikaze_destroy");
    }
    else if(int(_bomb->type/10) == 5)
    {
        CKAudioMng::Instance().playEffect("laser");
    }
    else if(int(_bomb->type/10) == 8)
    {
        CKAudioMng::Instance().playEffect("beans_detonation");
    }
    else if(int(_bomb->type/10) == 12)
    {
        CKAudioMng::Instance().playEffect("cross_detonation");
    }
    else if(int(_bomb->type/10) == 2)
    {
        CKAudioMng::Instance().playEffect("pirate_detonation");
    }
    else if(int(_bomb->type/10) == 4)
    {
        CKAudioMng::Instance().playEffect("deep_detonation");
    }
    else if(int(_bomb->type/10) == 3)
    {
        CKAudioMng::Instance().playEffect("cross_detonation");
    }
    else
    {
        if(int(_bomb->type/10) == 15)
        {
            CKAudioMng::Instance().stopEffect("accelerator_moving");
        }
        CKAudioMng::Instance().stopEffect("tap_bomb_flight");
        CKAudioMng::Instance().playEffect("bomb_detonation");
    }
    
    if(_t == 1)
    {
        CKAudioMng::Instance().playEffect("hit_ground");
    }
    else
    {
        if(int(_bomb->type/10) != 16)//kylimet
        {

            CKAudioMng::Instance().playEffect("hit_block");
        }
    };
    if(streak)
    {
        streak->setVisible(false);
    };
    
   last_pos_bomb = _bomb->sprite->getPosition();
//    if(bomb_blink) //deep
//    {
//        CCSprite * left = CCSprite::createWithSpriteFrameName("bomb41_part.png");// create(loadImage("bomb41_part.png").c_str());
//        m_layer->addChild(left,zOrder_Cube);
//        left->setPosition(ccp(_bomb->sprite->getPositionX() - BOX_SIZE/4,MAX(_bomb->sprite->getPositionY(), ground_height + BOX_SIZE/4)));
//        CCFiniteTimeAction * move = CCMoveBy::create(0.5, ccp(-(_bomb->type-40)*BOX_SIZE - BOX_SIZE/4,0));
//        CCFiniteTimeAction * fade = CCFadeOut::create(0.3);
//        CCAction* spawn = CCSequence::create(move,fade,NULL);
//        left->runAction(spawn);
//        
//        CCSprite * right = CCSprite::createWithSpriteFrameName("bomb41_part.png");//CCSprite::create(loadImage("bomb41_part.png").c_str());
//        m_layer->addChild(right,zOrder_Cube);
//        right->setPosition(ccp(_bomb->sprite->getPositionX() + BOX_SIZE/4,MAX(_bomb->sprite->getPositionY(), ground_height + BOX_SIZE/4)));
//        CCFiniteTimeAction * move2 = CCMoveBy::create(0.5, ccp((_bomb->type-40)*BOX_SIZE + BOX_SIZE/4,0));
//        CCFiniteTimeAction * fade2 = CCFadeOut::create(0.3);
//        CCAction* spawn2 = CCSequence::create(move2,fade2,NULL);
//        right->runAction(spawn2);
//    };
    
    int tmp_type = int(_bomb->type/10);
    if(tmp_type != 16)//kylimet
    {
        
        if(tmp_type == 7 && _t != 2)//touches
        {
            m_effect->show(ccp(_bomb->sprite->getPositionX(),_bomb->sprite->getPositionY()-_bomb->sprite->getContentSize().height/2), 140 + (_bomb->type)%10);
        }
        else
        {
            m_effect->show(ccp(_bomb->sprite->getPositionX(),_bomb->sprite->getPositionY()-_bomb->sprite->getContentSize().height/2), _bomb->type);
        }
        
        if(tmp_type != 15 && tmp_type != 12 && tmp_type != 4 && tmp_type != 13 && tmp_type != 8)
        {
            char x = (left_border)/BOX_SIZE;
            char y = (_bomb->sprite->getPositionY() - ground_height)/BOX_SIZE;
            
            m_array->addJumpCube(x, MAX(0,y));
            m_array->addJumpCube(x + 1, MAX(0,y));
            m_array->addJumpCube(MAX(0,x - 1), MAX(0,y));
            if(tmp_type == 2)
            {
                m_array->addJumpCube(x + 2, MAX(0,y));
                m_array->addJumpCube(MAX(0,x - 2), MAX(0,y));
            }
        }
    }
    touch_bomb = NULL;

    count_free_bomb++;
    
    if(!m_array->updateAfterDestroy())
    {
        if(!collision_kaskad_bomb && !collision_parabola)
            count_fail_bomb++;
    }
    else
    {
        hit_kaskad_bomb = true;
        if(!collision_kaskad_bomb && !collision_parabola)
            count_hit_bomb++;
    };
    
    m_array->jumpCube();
    
    _bomb->is_active = false;
    _bomb->type = 1;
    _bomb->sprite->stopAllActions();
    _bomb->sprite->setVisible(false);
    collision_parabola = false;
    
    if(collision_kaskad_bomb && delete_kaskad_bomb == (need_bombs + 1))//count kaskad
    {
        if(hit_kaskad_bomb)
            count_hit_bomb++;
        else
            count_fail_bomb++;
        collision_kaskad_bomb = false;
    };
    
    if(_bomb->is_airplane_bomb && !collision_kaskad_bomb)
    {
        create_new_bomb = true;
    };
    
    CCLOG("deleteBomb:: Hit %d Fail %d",count_hit_bomb,count_fail_bomb);
};

list<CCNode*>* CKBombs::getBonusCheck()
{
    return &bonus;
};

Sbomb* CKBombs::getTouchBomb()
{
   return touch_bomb;
};

void CKBombs::deleteAllActiveBombs()
{
    if(!list_bomb.empty())
    {
        for (it = list_bomb.begin(); it != list_bomb.end(); it++)
        {
            if((*it)->is_active)
                deleteBomb(0, (*it));
        }
    };
    need_bombs = 0;
    hideAllDestroyEffect();
};

const int CKBombs::update(const float &_dt)
{
    if(stop_update)
        return 0;
     
    if(count_kaskad_bomb < need_bombs)//kaskad
    {
        if(abs(toPoint.x - m_airplane->getPosition().x) > BOX_SIZE/2)
        {
            CCLOG("update:: m_airplane->getPosition().x %f %f count_kaskad_bomb %d",m_airplane->getPosition().x,toPoint.x,count_kaskad_bomb);
            
            // CCLOG("m_airplane->getPosition().x %f %f",m_airplane->getPosition().x,toPoint.x);
            toPoint.x += BOX_SIZE/2;
            count_kaskad_bomb++;
            
            firs_bomb = false;
            create_new_bomb = true;
            create(toPoint,curent_kaskad_type);
            create_new_bomb = false;
            count_create_bomb--;
            count_spec_bombs--;
            if(count_kaskad_bomb == need_bombs)
            {
                toPoint.x =  -10.0;
            };
        }
    };
    
    if(!list_bomb.empty())
    {
        for (it = list_bomb.begin(); it != list_bomb.end(); it++)
        {
            if(!(*it)->is_active)
                continue;
            if(!(*it)->sprite)
                continue;
            // CCLOG("find bonus bonus type %d",(*it)->type);
            //(*it)->sprite->setVisible(true);
            if(!(*it))
                continue;
//            if((*it)->type != 67) //gun
//            {
//                addBonusToList(*it);
//            }
            (*it)->line_velosity += line_velosity;
            
            //update position
            updatePosition(_dt);
            
            //update collision
            if(updateCollision(_dt))
                return 1;
            
            if(updateDistance(_dt))
                return 1;
            
            if(groundUpdate((*it)) == 1)
                return 1;
        };
    }
    return 0;
};

//void CKBombs::addBonusToList(Sbomb* _t)
//{
//    CCNode* node = NULL;
//    do {
//        node = m_bonus->findActiveBonus(_t->sprite->getPosition(),_t->sprite->getContentSize());
//        if(node)
//        {
//            m_array->setFreezDestroyCount(true);
//            bonus.push_back(node);
//        }
//        
//    }
//    while (node);
//};

int CKBombs::updateCollision(const float &_dt)
{
    switch ((*it)->type) {
        case 32:
            {
              /*  func m_fun =  (*it)->m_func;
                if((this->*m_fun)((*it)))
                {
                    deleteBomb(0,(*it));
                    return 1;
                };*/
            };
            break;
        case 68:
            if((*it)->sprite->getActionByTag(2) == NULL)
            {
                func m_fun =  (*it)->m_func;
                if((this->*m_fun)((*it)))
                {
                    deleteBomb(0,(*it));
                    return 1;
                };
            };
            break;
        case 85:
        case 86:
        case 87:
            if((*it)->sprite->getActionByTag(2) == NULL)
            {
                func m_fun =  (*it)->m_func;
                if((this->*m_fun)((*it)))
                {
                };
                m_effect->show((*it)->sprite->getPosition(), (*it)->type);
                m_array->updateAfterDestroy();
                m_array->jumpCube();
                (*it)->is_active = false;
                (*it)->type = 1;
                (*it)->sprite->setVisible(false);
                count_free_bomb++;
                
                return 1;
            };
            break;
        case 61:
        case 62:
        case 63:
        case 111:
        case 112:
        case 113:
            if((*it)->sprite->getPositionX() < -BOX_SIZE*2 || (*it)->sprite->getPositionX() > (win_size.width + BOX_SIZE*2))
            {
                deleteBomb(0, (*it));
                return 1;
            }
            m_func =  (*it)->m_func;
            if((this->*m_func)((*it)))
            {
                deleteBomb(0,(*it));
                return 1;
            };
            break;
        case 151:
        case 152:
        case 153:
        {
            if(((*it)->sprite->getPositionX() < (*it)->sprite->getContentSize().width/2)|| ((*it)->sprite->getPositionX() > (win_size.width + (*it)->sprite->getContentSize().width/2)))
            {
                deleteBomb(0,(*it));
                return 1;
            };
            if((*it)->sprite->getPositionY() < (ground_height + (*it)->sprite->getContentSize().height/2))
            {
                is_ground = true;
                if(!start_accele)
                {
                    CKAudioMng::Instance().playEffect("hit_ground");
                    CKAudioMng::Instance().playEffect("accelerator_moving");
                }
                start_accele = true;
            }
            int t = checkCollision((*it));
            if( t == 0)
            {
                if(!is_ground)
                {
                    (*it)->sprite->setPosition(ccp((*it)->sprite->getPositionX(),(*it)->sprite->getPositionY() - (*it)->speed*(*it)->line_velosity*_dt));
                }
                else
                {
                    (*it)->sprite->setPosition(ccp((*it)->sprite->getPositionX(),ground_height + (*it)->sprite->getContentSize().height/2));
                }
                bomb_speed *= 0.99;//dump
            }
            else
                if(t == 1)
                {
                    if(!start_accele)
                        CKAudioMng::Instance().playEffect("accelerator_moving");
                    start_accele = true;
                    (*it)->line_velosity = 0.1;
                }
                else
                {
                    deleteBomb(0,(*it));
                    return 1;
                };
        };
            break;
        case 161:
        case 162:
        case 163:
            if((m_airplane->getPosition().x - gun_posX) > BOX_SIZE)
            {
                gun_posX = 0;
                m_func =  getFuncById((*it)->type);
                (this->*m_func)((*it));
                deleteBomb(0,(*it));
                //CKAudioMng::Instance().stopEffect("gun");
                m_airplane->stopMachineGun();
                return 1;
            };
            
            if(m_airplane->getPosition().x < gun_posX)
            {
                gun_posX = 0;
                deleteBomb(0,(*it));
               // CKAudioMng::Instance().stopEffect("gun");
                m_airplane->stopMachineGun();
            };
            return 0;
            break;
        default:
            break;
    }
    return 0;
};

int CKBombs::updateDistance(const float &_dt)
{
    if((*it)->distanse < 0)
    {
        CCLOG("colision TRUE");
        switch ((*it)->type) {
            case 51:
            case 52:
            case 53://lazer
            {

                Sbomb* sbomb = (*it);
                m_func =  sbomb->m_func;
                (this->*m_func)(sbomb);
                
                CCFiniteTimeAction *action = CCScaleTo::create(0.13, 0.0, (*it)->sprite->getScaleY()/2);
                (*it)->sprite->runAction(action);
                last_pos_bomb = ccp((*it)->sprite->getPositionX(),MAX(ground_height,(*it)->sprite->getPositionY()+BOX_SIZE/2));
                if(!m_array->updateAfterDestroy())
                {
                    count_fail_bomb++;
                }
                else
                {
                    count_hit_bomb++;
                };
                CKAudioMng::Instance().playEffect("hit_block");
                m_array->jumpCube();
                (*it)->is_active = false;
                count_free_bomb++;
                
                create_new_bomb = true;
                return 1;
            }
                break;
            case 81:
            case 82:
            case 83://parabola
            {
                CCLOG("Update distanse::parabola");
                (*it)->sprite->setPosition(ccp((*it)->sprite->getPositionX(),(*it)->sprite->getPositionY() - (*it)->distanse));
                
                Sbomb* sbomb = (*it);
                m_func =  sbomb->m_func;
                if(!(this->*m_func)(sbomb))
                {
                    count_fail_bomb++;
                }
                else
                {
                    count_hit_bomb++;
                };
                // (*it)->sprite->setPosition(ccp((*it)->sprite->getPositionX(),BOX_SIZE*2));
                deleteBomb(0,sbomb);
                return 1;
            };
                break;
            case 85:
            case 86:
            case 87:
            {
                (*it)->sprite->setPosition(ccp((*it)->sprite->getPositionX(),(*it)->sprite->getPositionY() - (*it)->distanse));
                Sbomb* sbomb = (*it);
                m_func =  sbomb->m_func;
                (this->*m_func)(sbomb);
                m_array->updateAfterDestroy();
                m_array->jumpCube();
                sbomb->is_active = false;
                sbomb->type = 1;
                sbomb->sprite->setVisible(false);
                //m_layer->removeChild(sbomb->sprite, true);
                sbomb->sprite = NULL;
                count_free_bomb++;
//                showDestroyEffect(ccp(sbomb->sprite->getPositionX(),sbomb->sprite->getPositionY()-sbomb->sprite->getContentSize().height/2));
                m_effect->show(ccp(sbomb->sprite->getPositionX(),sbomb->sprite->getPositionY()-sbomb->sprite->getContentSize().height/2), 8);
                return 1;
            }
                break;
            case 131:
            case 132:
            case 133:
            {
                Sbomb* sbomb = (*it);
                m_func =  sbomb->m_func;
                (this->*m_func)(sbomb);
                delete_kaskad_bomb++;
                deleteBomb(0,sbomb);
                if(delete_kaskad_bomb <= need_bombs)
                {
                    create_new_bomb = false;
                }
                
                return 1;
            }
                break;
            case 161:
            case 162:
            case 163:
                break;
            default:
                (*it)->sprite->setPosition(ccp((*it)->sprite->getPositionX(),(*it)->sprite->getPositionY() - (*it)->distanse));
                Sbomb* sbomb = (*it);
                m_func =  sbomb->m_func;
                if((this->*m_func)(sbomb));
                {
                    deleteBomb(0,sbomb);
                    return 1;
                };
                return 0;
                break;
        };
    };
    return 0;
};

void CKBombs::updatePosition(const float &_dt)
{
   
    // CCLOG("updatePosition:: pos(%f,%f)",(*it)->sprite->getPositionX(),(*it)->sprite->getPositionY());
    switch ((*it)->type) {
        case 51:
        case 52:
        case 53://lazer
        {
          //  CCLOG("(*it)->previor_pos %f",(*it)->previor_pos.y);
            (*it)->sprite->setPosition(ccp((*it)->sprite->getPositionX(),(*it)->sprite->getPositionY() - (*it)->speed*(*it)->line_velosity*_dt));
            (*it)->distanse -= (*it)->speed*(*it)->line_velosity*_dt;
            // (*it)->previor_pos.y -= (*it)->speed*(*it)->line_velosity*_dt*0.2;
            float scale = ((*it)->previor_pos.y - (*it)->sprite->getPositionY() - (*it)->sprite->getContentSize().height)/(*it)->sprite->getContentSize().height;
            (*it)->sprite->setScaleY(scale);
        }
            break;
        case 68: //kamikadze
            if(( (*it)->sprite->getPositionY() > (*it)->previor_pos.y))
            {
                direct.x = (*it)->sprite->getPositionX() - (*it)->previor_pos.x;
                direct.y = (*it)->sprite->getPositionY() - (*it)->previor_pos.y;
                float length = sqrtf( direct.x*direct.x + direct.y*direct.y);
                
                direct.x /= length;
                direct.y /= length;
            }
            (*it)->sprite->setPosition(ccp((*it)->sprite->getPositionX() - direct.x*(*it)->speed*(*it)->line_velosity*_dt*2.3,(*it)->sprite->getPositionY() - direct.y*(*it)->speed*(*it)->line_velosity*_dt));
            (*it)->sprite->setRotation(getAngleBeetwenTwoPoint((*it)->previor_pos,(*it)->sprite->getPosition()));
            break;
        case 71:
        case 72:
        case 73:
            (*it)->sprite->setPosition(ccp((*it)->sprite->getPositionX(),(*it)->sprite->getPositionY() - (*it)->speed*_dt*2.0));
            (*it)->distanse -= (*it)->speed*_dt*2.0;
            break;
            
        case 85:
        case 86:
        case 87:
            break;
        case 61:
        case 62:
        case 63:
        case 111:
        case 112:
        case 113://move to toch
        {
            if(target_point_list.size() > 0)
            {
                target_it = target_point_list.begin();
                if(getPointLength((*target_it),tmp_sprite->getPosition()) < (*it)->speed*_dt*1.2*(*it)->line_velosity)
                {
                    target_it++;
                    //CCLOG("next target point %f %f",(*target_it).x,(*target_it).y);
                    direct.x = tmp_sprite->getPositionX() - (*target_it).x;
                    direct.y = tmp_sprite->getPositionY() - (*target_it).y;
                    // CCLOG("next direct target point %f %f",direct.x,direct.y);
                    float length = sqrtf( direct.x*direct.x + direct.y*direct.y);
                    if(length == 0.0)
                    {
                        length = 1.0;
                        direct.x = 0.0;
                        direct.y = 1.0;
                    }
                    else
                    {
                        direct.x /= length;
                        direct.y /= length;
                    }
                    target_point_list.pop_front();
                };
                target_it = target_point_list.begin();
                tmp_sprite->setPosition(ccp(tmp_sprite->getPositionX() - direct.x*(*it)->speed*_dt*(*it)->line_velosity,tmp_sprite->getPositionY() - direct.y*(*it)->speed*_dt*(*it)->line_velosity));
            }
            
            CCPoint direct_t;
            direct_t.x = (*it)->sprite->getPositionX() - tmp_sprite->getPosition().x;
            direct_t.y = (*it)->sprite->getPositionY() - tmp_sprite->getPosition().y;
            
            float length = sqrtf( direct_t.x*direct_t.x + direct_t.y*direct_t.y);
            
            if(length == 0.0)
            {
                length = 1.0;
                direct_t.x = 0.0;
                direct_t.y = 1.0;
                
            }
            if(length != 0.0)
            {
                direct_t.x /= length;
                direct_t.y /= length;
                {
                    direct2.x = direct_t.x*length*1.8*(*it)->line_velosity*_dt;
                    direct2.y = direct_t.y*length*1.8*(*it)->line_velosity*_dt;
                    (*it)->sprite->setPosition(ccp((*it)->sprite->getPositionX() - direct2.x,(*it)->sprite->getPositionY() - direct2.y));
                    if(is_parabola && int((*it)->type/10) != 6)
                    {
                        (*it)->sprite->setRotation((point_touch.x - (*it)->sprite->getPosition().x)/touch_length*-90);
                    }
                    else
                        (*it)->sprite->setRotation(getAngleBeetwenTwoPoint(CCPointZero,direct_t));
                }
            }
        }
            streak->setPosition((*it)->sprite->getPosition());
            
            break;
        case 131:
        case 132:
        case 133://kaskad
            (*it)->distanse -= (*it)->speed*(*it)->line_velosity*_dt;
            (*it)->sprite->setPosition(ccp((*it)->sprite->getPositionX(),(*it)->sprite->getPositionY() - (*it)->speed*(*it)->line_velosity*_dt));
            break;
        case 151:
        case 152:
        case 153:
        {
            (*it)->sprite->setPosition(ccp((*it)->sprite->getPositionX() + bomb_speed*2.5,(*it)->sprite->getPositionY()));
            (*it)->sprite->setRotation((*it)->sprite->getRotation() + bomb_speed*10.0);
        }
            break;
        case 161:
        case 162:
        case 163:
            break;
        default:
            (*it)->sprite->setPosition(ccp((*it)->sprite->getPositionX(),(*it)->sprite->getPositionY() - (*it)->speed*(*it)->line_velosity*_dt));
            (*it)->distanse -= (*it)->speed*(*it)->line_velosity*_dt;
            break;
    }
};

int CKBombs::groundUpdate(Sbomb* _bomb)
{
    if((_bomb->sprite->getPositionY() < (ground_height)))
    {
      //  CCLOG("groundUpdate::");
        //if(count_free_bomb == !())
        switch (_bomb->type) {
            case 22:
            case 23:
                _bomb->sprite->setPosition(ccp(_bomb->sprite->getPositionX(),_bomb->sprite->getContentSize().height/2 + ground_height - BOX_SIZE));
                deleteBomb(1,_bomb);
                break;
            case 81:
            case 82:
            case 83://parabola
                if(!_bomb->sprite->isVisible())
                    break;
                _bomb->sprite->setPosition(ccp(_bomb->sprite->getPositionX(),_bomb->sprite->getContentSize().height/2 + ground_height));
                m_func =  getFuncById(_bomb->type);
                
                if(!(this->*m_func)(_bomb))
                {
                    count_fail_bomb++;
                }
                else
                {
                    count_hit_bomb++;
                };
                deleteBomb(1,_bomb);
                return 1;
                break;
            case 85:
            case 86:
            case 87:
                _bomb->sprite->setPosition(ccp(_bomb->sprite->getPositionX(),_bomb->sprite->getContentSize().height/2 + ground_height));
                _bomb->is_active = false;
                _bomb->type = 1;
                _bomb->sprite->stopAllActions();
                _bomb->sprite->setVisible(false);
//                showDestroyEffect(ccp(_bomb->sprite->getPositionX(),_bomb->sprite->getPositionY()-_bomb->sprite->getContentSize().height/2));
                m_effect->show(ccp(_bomb->sprite->getPositionX(),_bomb->sprite->getPositionY()-_bomb->sprite->getContentSize().height/2), 8);
                count_free_bomb++;
                break;
            case 41:
            case 42:
            case 43:
            case 121:
            case 122:
            case 123:
                _bomb->sprite->setPosition(ccp(_bomb->sprite->getPositionX(),_bomb->sprite->getContentSize().height/2 + ground_height - BOX_SIZE));
                m_func =  getFuncById(_bomb->type);
                (this->*m_func)(_bomb);
                deleteBomb(1,_bomb);
                break;
            case 51:
            case 52:
            case 53://lazer
            {

               // addBonusToList(_bomb);
                
                Sbomb* sbomb = _bomb;
                m_func =  sbomb->m_func;
                (this->*m_func)(sbomb);
                
                CCFiniteTimeAction *action = CCScaleTo::create(0.13, 0.0, _bomb->sprite->getScaleY()/2);
                _bomb->sprite->runAction(action);
                last_pos_bomb = ccp(_bomb->sprite->getPositionX(),MAX(ground_height,_bomb->sprite->getPositionY()+BOX_SIZE/2));
                if(!m_array->updateAfterDestroy())
                {
                    count_fail_bomb++;
                }
                else
                {
                    count_hit_bomb++;
                }
                m_array->jumpCube();
                _bomb->is_active = false;
                count_free_bomb++;
                create_new_bomb = true;
            }
                break;
            case 131:
            case 132:
            case 133://kaskad
            {
                _bomb->sprite->setPosition(ccp(_bomb->sprite->getPositionX(),_bomb->sprite->getPositionY() - _bomb->distanse));
                Sbomb* sbomb = _bomb;
                m_func =  sbomb->m_func;
                (this->*m_func)(sbomb);
                delete_kaskad_bomb++;
                deleteBomb(1,sbomb);
                if(delete_kaskad_bomb <= need_bombs)
                {
                    create_new_bomb = false;
                }
                
                return 1;
            }
                break;
            case 151:
            case 152:
            case 153:
                is_ground = true;
                start_accele = true;
                bomb_speed = 1.0;
                _bomb->sprite->setPosition(ccp(_bomb->sprite->getPositionX(),_bomb->sprite->getContentSize().height/2 + ground_height));
                break;
            default:
                _bomb->sprite->setPosition(ccp(_bomb->sprite->getPositionX(),_bomb->sprite->getContentSize().height/2 + ground_height));
                deleteBomb(1,_bomb);
                
                break;
        }
        
        return 1;
    };
    return 0;
};

void CKBombs::touchAutoFind(const CCPoint &pos)
{
    if(!touch_bomb)
        return;
    CCPoint posS = touch_bomb->sprite->getPosition();
    
    point_touch = pos;
    int y1 = 0;//(pos.y - ground_height)/(BOX_SIZE);
    int x1 = (pos.x - BOX_SIZE/2)/(BOX_SIZE/2); //+ BOX_SIZE/2;
    CCLOG("touch cube %d",x1);
    //  int height_column = 1;
    for(int i = COUNT_ROWS - 1; i > 0; i--)//column height
    {
        if(m_array->getMiniCube(i,x1) != 0)
        {
            y1 = i;
            break;
        }
    };
    point_touch.y = ground_height + y1*(BOX_SIZE/2);
    point_touch.x = BOX_SIZE/2 + BOX_SIZE/4 + x1*(BOX_SIZE/2);
    createTrackMoved(posS,point_touch);
    touch_length = /*getPointLength(ccp(point_touch.x,point_touch.y + BOX_SIZE*2), posS);//*/ fabs(point_touch.x - posS.x);
};

bool CKBombs::touchTap()
{
    if(!touch_bomb)
        return false;
    
    CKAudioMng::Instance().playEffect("cross_detonation");
        collision71(touch_bomb);
        deleteBomb(2,touch_bomb);
    //else
    //    deleteBomb(1,touch_bomb);
    
    return true;
};

void CKBombs::createTrackMoved(const CCPoint& _start,const CCPoint& _p)
{
    target_point_list.clear();
    is_parabola = false;
    if((_start.y - _p.y) < (BOX_SIZE*3.5))//spiral
    {
        tmp_sprite->setPosition(ccp(_start.x,_start.y - BOX_SIZE*1.5 ));
        target_point_list.push_back(tmp_sprite->getPosition());
        
        if(_p.x < _start.x)
        {
            target_point_list.push_back(ccp(_start.x + BOX_SIZE,_start.y - BOX_SIZE*2));
            target_point_list.push_back(ccp(_start.x + BOX_SIZE*2,_start.y));
            target_point_list.push_back(ccp(_start.x + BOX_SIZE*2,_start.y + BOX_SIZE));
            target_point_list.push_back(ccp(_start.x + BOX_SIZE,_start.y + BOX_SIZE*2));
            target_point_list.push_back(ccp(_p.x + BOX_SIZE, _start.y + BOX_SIZE*2));
        }
        else
        {
            target_point_list.push_back(ccp(_start.x - BOX_SIZE,_start.y - BOX_SIZE*2));
            target_point_list.push_back(ccp(_start.x - BOX_SIZE*2,_start.y));
            target_point_list.push_back(ccp(_start.x - BOX_SIZE*2,_start.y + BOX_SIZE));
            target_point_list.push_back(ccp(_start.x - BOX_SIZE,_start.y + BOX_SIZE*2));
            target_point_list.push_back(ccp(_p.x - BOX_SIZE, _start.y + BOX_SIZE*2));
        };
        
        target_point_list.push_back(ccp(_p.x, _start.y + BOX_SIZE*1.9));
        target_point_list.push_back(ccp(_p.x, _p.y));
        target_point_list.push_back(ccp(_p.x, 0));
        target_point_list.push_back(ccp(_p.x, -BOX_SIZE));
    }
    else//parabola
    {
        is_parabola = true;

        
        target_point_list.push_back(ccp(_start.x, _start.y));
        target_point_list.push_back(ccp(_p.x, _start.y));
        target_point_list.push_back(ccp(_p.x, _p.y));
        target_point_list.push_back(ccp(_p.x, -BOX_SIZE));
        target_point_list.push_back(ccp(_p.x, -BOX_SIZE*3));
    };
    target_it = target_point_list.begin();
};

void CKBombs::setCubeArray(CKCubeArray* _array)
{
    m_array = _array;
};

void CKBombs::setBonus(CKBonus *_bonus)
{
    m_bonus = _bonus;
};

void CKBombs::didAccelerate(CCAcceleration* pAccelerationValue)
{
    if(start_accele)
    {
        bomb_speed += pAccelerationValue->x*2.0;
    };
};

CCPoint CKBombs::findMaxHeight(Sbomb* _bomb)
{
    int x1 = (left_border)/(BOX_SIZE);
    int y1 = (_bomb->sprite->getPositionY() - ground_height - _bomb->sprite->getContentSize().height/2)/(BOX_SIZE);
    
    int depth = 2;
    if(_bomb->type == 62)
        depth = 3;
    else
        if(_bomb->type == 63)
            depth = 4;
    
    for(int j = y1; j >= 0; --j)
        for(int i = 0; i < depth;++i)
        {
            if((x1 - i) >= 0)
                if(m_array->getBigCube(j,x1 - i) != 0)
                {
                  //  CCLOG("findMaxHeight:: left x1 %d y1 %d pos(%f,%f)",x1,y1,(x1 - i)*BOX_SIZE + BOX_SIZE,ground_height + j*(BOX_SIZE) +  _bomb->sprite->getContentSize().height/2 + BOX_SIZE);
                    return ccp((x1 - i)*(BOX_SIZE) + panel_left+ BOX_SIZE,ground_height + j*(BOX_SIZE) +  _bomb->sprite->getContentSize().height/2 + BOX_SIZE);
                }
            if((x1 + i)  < COUNT_COLUMNS)
                if(m_array->getBigCube(j,x1 + i) != 0)
                {
//                    CCLOG("findMaxHeight:: right pos(%f,%f)",(x1+i)*(BOX_SIZE) + BOX_SIZE,j*(BOX_SIZE)+ ground_height + _bomb->sprite->getContentSize().height/2 + BOX_SIZE);
                    return ccp((x1+i)*(BOX_SIZE) + panel_left + BOX_SIZE  ,j*(BOX_SIZE)+ ground_height + _bomb->sprite->getContentSize().height/2 + BOX_SIZE);
                }
        };
    CCLOG("findMaxHeight:: pos.x = %f",_bomb->sprite->getPositionX());
    return ccp(_bomb->sprite->getPositionX(),0);
};


#pragma mark - COLLISION
bool CKBombs::collision11(Sbomb* _bomb)
{
    int x1 = (left_border - 0.45*_bomb->sprite->getContentSize().width)/(BOX_SIZE/2);
    int y1 = (_bomb->sprite->getPositionY() - ground_height - _bomb->sprite->getContentSize().height/2)/(BOX_SIZE/2);
    int x2 = (left_border + 0.45*_bomb->sprite->getContentSize().width)/(BOX_SIZE/2);
    
    int y2 = y1;
    if( x1 < 0 || y1 < 0)
        return false;
    while(m_array->getMiniCube(y1 + 1,x1) != 0 || m_array->getMiniCube(y1 + 1,x2) != 0)
    {
        y1++;
    };
    
    if(m_array->getMiniCube(y1,x1) != 0 || m_array->getMiniCube(y2,x2) != 0)
    {
        int x,y,count = 0;
        if(m_array->destroyMiniCub(x1, y1))
        {
            ++count;
            x = x1;
            y = y1;
        };
        if(m_array->destroyMiniCub(x2, y2))
        {
            ++count;
            x = x2;
            y = y2;
        };
        if(count == 1)
        {
            m_array->destroyMiniCub(x, y - 1);
        };
        return true;
    };
    return false;
};

bool CKBombs::collisionKamikadze(Sbomb* _bomb)
{
    int x1 = (left_border)/(BOX_SIZE/2);
    int y1 = (_bomb->sprite->getPositionY() - ground_height - _bomb->sprite->getContentSize().height/2)/(BOX_SIZE/2);
    
    int x = x1/2;
    int y = y1/2;
    while (m_array->getBigCube(y + 1,x) != 0) {
        y++;
    };
    bool p =false;
    if(m_array->getBigCube(y,x) != 0)
        p = true;
    if(p)
    {
        m_array->destroyCube(x*2,y*2);
        y--;
        if(p == false)
            if(m_array->getBigCube(y,x) != 0)
                p = true;
        
        m_array->destroyCube(x*2,y*2);
    }
   // CCLOG("p= %d",p);
    return p;
};
//2. Бомба підвищеної області ураження вбиває повний квадрат льодяників 2х2 (1,2,4)
bool CKBombs::collision21(Sbomb* _bomb)
{
    int x1 = (left_border - 0.5*_bomb->sprite->getContentSize().width)/(BOX_SIZE/2);
    int y1 = (_bomb->sprite->getPositionY() - ground_height - _bomb->sprite->getContentSize().height/2)/(BOX_SIZE/2);
    int x2 = (left_border + 0.5*_bomb->sprite->getContentSize().width)/(BOX_SIZE/2);
    
    int x3 = (left_border)/(BOX_SIZE/2);

    int x = x1/2;
    int y = y1/2;
    bool coll = false;
    unsigned char effect_id = 2;
    if(_bomb->type == 23)
    {
        if(y == -1 )// collision ground
        {
            y++;
            m_array->destroyCube(int((x1 - 1)/2)*2,y*2,effect_id);
            m_array->destroyCube(int((x2 + 1)/2)*2,y*2,effect_id);
        }
        else
        {
            m_array->destroyCube(x*2,y*2,effect_id);
            m_array->destroyCube(x*2,y*2 - 2,effect_id);
            
            m_array->destroyCube(int(x2/2)*2,y*2,effect_id);
            m_array->destroyCube(int(x2/2)*2,y*2 - 2,effect_id);
        }
    }else
        if(_bomb->type == 22)
        {
            if(y == -1 )
            {
                
                x = x3/2;
                y++;
                if(m_array->getBigCube(y,x - 1) != 0)//left
                {
//                    CCLOG("ff");
                    m_array->destroyCube((x-1)*2,y*2,effect_id);
                }
                else
                    if(m_array->getBigCube(y,x + 1) != 0)//right
                    {
//                        CCLOG("ff");
                        m_array->destroyCube((x+1)*2,y*2,effect_id);
                    }
            }
            x = x3/2;
            if(m_array->getBigCube(y,x) != 0)
            {
                m_array->destroyCube(x*2,y*2,effect_id);
                if((_bomb->sprite->getPositionX()- x*BOX_SIZE/2) < 0)
                {
                    m_array->destroyCube((x-1)*2,y*2,effect_id);
                }
                else
                {
                    m_array->destroyCube((x+1)*2,y*2,effect_id);
                }
            }
            else
            {
                m_array->destroyCube(int(x1/2)*2,y*2,effect_id);
                m_array->destroyCube(int(x2/2)*2,y*2,effect_id);
            }
        }else
            if(_bomb->type == 21)
            {
                if(m_array->getBigCube(y,x) != 0)
                {
                    m_array->destroyCube(x*2,y*2,effect_id);
                    coll = true;
                };
                if(m_array->getBigCube(y,x2/2) != 0 && !coll)
                {
                    m_array->destroyCube(int(x2/2)*2,y*2,effect_id);
                };
            };
    return false;
};

//3. Бомба підвищеної ширини ураження – одинарна глибина + ширина (2,3,4);
bool CKBombs::collision31(Sbomb* _bomb)
{
    int x1 = (left_border - _bomb->sprite->getContentSize().width*0.4)/(BOX_SIZE/2);
    int y1 = (_bomb->sprite->getPositionY()  - ground_height - _bomb->sprite->getContentSize().height/2)/(BOX_SIZE/2);

    if( x1 < -2 || y1 < 0)
        return false;
    
    bool p = false;
    
    int t = _bomb->type%10 + 1;
    if(x1 < 0)
        x1--;
    
    for(int i = y1; i >= 0; --i)
    {
        if(p)
            break;
        for(int j = x1; j < (x1 + t);j++)
        {
            if(j >= 0)
            if(m_array->destroyMiniCub(j, i))
            {
                p = true;
            }
        }
    };
    
    return p;
};

//4. Бомба «глибинна» - яка підрізає низ (великі кубики) у ширину – і всі стовпці зсуваються вниз на один рівень.
bool CKBombs::collision41(Sbomb* _bomb)
{
    int x1 = (left_border)/(BOX_SIZE/2);
    int y1 = (_bomb->sprite->getPositionY() - ground_height - _bomb->sprite->getContentSize().height/2)/(BOX_SIZE/2);
    int x = x1/2;
    int y = (y1+1)/2;
    //y++;
    
    switch (_bomb->type) {
        case 43:
            m_array->destroyCube( x*2 + 6,y*2);
            m_array->destroyCube( x*2 - 6,y*2);
        case 42:
            m_array->destroyCube( x*2 + 4,y*2);
            m_array->destroyCube( x*2 - 4,y*2);
            break;
        default:
            break;
    };
    m_array->destroyCube( x*2,y*2);
    m_array->destroyCube( x*2 + 2,y*2);
    m_array->destroyCube( x*2 - 2,y*2);
    
    return false;
};

//5. Бомба лазерна – яка пропалює весь стовпець донизу.
bool CKBombs::collision51(Sbomb* _bomb)
{
    int x1 = (left_border)/(BOX_SIZE/2);
    int y1 = (_bomb->sprite->getPositionY() - ground_height)/(BOX_SIZE/2);
    if(y1 < 0)
        y1 = 0;
    bool p = false;
    if( x1 < 0)
        return false;
    
    while(m_array->getMiniCube(y1,x1) != 0)
    {
        y1++;
    };
    int d = 3;
    if(_bomb->type == 51)
        d = 5;
    else
        if(_bomb->type == 52)
            d = 9;
        else
            d = 20;
    
    while (d > 0) {
        if(m_array->destroyMiniCub(x1, y1))
            p = true;
        y1--;
        d--;
        if(y1 < 0)
            return p;
    };
    return p;
}
//7. Самонавідна бомба - відхиляється від курсу, якщо поруч зі стовпцем, в який вона летить, існує ряд четвертинок, котрий є вищим і вартує збити його.
bool CKBombs::collision61(Sbomb* _bomb)
{
    int x1 = (left_border)/(BOX_SIZE/2);
    int y1 = (_bomb->sprite->getPositionY() - ground_height - BOX_SIZE/2)/(BOX_SIZE/2);
    
    
    if( x1 < 0)
        return false;
    int x = x1/2;
    bool p = false;
    if(m_array->getMiniCube(y1 + 1,x*2) != 0 ||m_array->getMiniCube(y1 + 1,x*2+1) !=0)
    {
        y1++;
    };
    int y = y1/2;
    
    if( m_array->getMiniCube(y1,x*2) != 0 ||m_array->getMiniCube(y1,x*2+1) != 0)
        switch (_bomb->type) {
            case 63:
                m_array->destroyMiniCub(x*2, y1 - 3);
                m_array->destroyMiniCub(x*2 + 1, y1 - 3);
                m_array->destroyMiniCub(x*2, y1 - 2);
                m_array->destroyMiniCub(x*2 + 1, y1 - 2);
                p = true;
            case 62:
                m_array->destroyCube(x*2, y1 - 1);
                p = true;
                break;
            case 61:
            {
                // x = int(x2/2);
                if(m_array->getMiniCube(y*2 + 1,x*2) != 0 || m_array->getMiniCube(y*2,x*2) != 0)
                {
                    m_array->destroyMiniCub(x*2, y*2);
                    m_array->destroyMiniCub(x*2, y*2 + 1);
                }
                else
                {
                    //x = int(x3/2);
                    m_array->destroyMiniCub(x*2 + 1, y*2);
                    m_array->destroyMiniCub(x*2 + 1, y*2 + 1);
                }
                p = true;
            };
                break;
            default:
                break;
        }
    return p;
};
//7. Розривна бомба - вибухає по кліку гравця в повітрі. Вражає сусідні з нею четвертинки. При попаданні прямим ударом в четвертинку без дії гравця поводиться як звичайна бомба.
bool CKBombs::collision71(Sbomb* _bomb)
{
    int y1 = (_bomb->sprite->getPositionY() - ground_height)/(BOX_SIZE/2);
    int x3 = (left_border)/(BOX_SIZE/2);
    // int x = x3/2;
    int y = y1/2;
    bool p = false;
    //  CCLOG("y1  div 2 %d",y1%2);
    int c = 2;
    if(_bomb->type == 73)
        c = 5;
    else
        if(_bomb->type == 72)
            c = 3;
    
    for(int j = y*2; j <= (y*2 + 1); j++)
        for (int i = 0; i < c; i++)
        {
            // if(m_array->large_map[j][x3 - i] != 0 || m_array->large_map[j][x3 + 1 + i] != 0)
            //     p = true;
            if (m_array->destroyMiniCub(x3 - i - 1, j))//letf
                p = true;
            if(m_array->destroyMiniCub(x3 + i, j))//right
                p = true;
        };
    if(p == false)
    {
        int y2 = (_bomb->sprite->getPositionY() - ground_height - _bomb->sprite->getContentSize().height/2)/(BOX_SIZE/2);
        for (int i = 0; i < c; i++)
        {
            if(m_array->getMiniCube(y2,x3 - i) != 0 || m_array->getMiniCube(y2,x3 + 1 + i) != 0)
                p = true;
            m_array->destroyMiniCub(x3 - i - 1, y2);//letf
            m_array->destroyMiniCub(x3 + i, y2);//right
        };
    }
    return p;
};
//8. Розкидна бомба - вдаряється в четвертинку і розлітається вверх на дві бомби, які летять по параболі на відстань однієї четвертинки (вгору, вниз або вбік по параболі).
bool CKBombs::collision81(Sbomb* _bomb)
{
    int x1 = (left_border - 0.3*_bomb->sprite->getContentSize().width)/(BOX_SIZE/2);
    float fx1 = (left_border - 0.3*_bomb->sprite->getContentSize().width)/(BOX_SIZE/2);
    int y1 = (_bomb->sprite->getPositionY() - ground_height - _bomb->sprite->getContentSize().height/2)/(BOX_SIZE/2);
    
    if( x1 < 0 || y1 < 0)
        return false;
  //  CCLOG("collision81:: x1 %d ",x1);
    bool p = false;
    while(m_array->getMiniCube(y1 + 1,x1) != 0)
    {
        y1++;
    };
    
    if(m_array->getMiniCube(y1,x1) != 0 && fx1 > 0.0)
    {
        int x = 0,y = 0;
        if(m_array->destroyMiniCub(x1, y1))
        {
            p = true;
            x = x1;
            y = y1;
        };
        
        if(_bomb->type == 82)
        {
            if(m_array->destroyMiniCub(x, y - 1))
                p = true;
        }
        else
            if(_bomb->type == 83)
            {
                if(m_array->destroyMiniCub(x, y - 1))
                    p = true;
                if(m_array->destroyMiniCub(x, y - 2))
                    p = true;
            };
        y = -1;
        if(x > 0)
        {
            //create left bomb
            for(int i = (COUNT_ROWS*2-1);i >= 0; --i )
                if(m_array->getMiniCube(i,x - 1))
                {
                    y = i;
                    p = true;
                    break;
                };
            
            CCPoint p1 = ccp(x*BOX_SIZE/2+ panel_left - 0.5*_bomb->sprite->getContentSize().width + BOX_SIZE/2,y*BOX_SIZE/2 + ground_height + _bomb->sprite->getContentSize().height/2);
            createMini(_bomb->sprite->getPosition(), p1, _bomb->type + 4);//left
        }
        //create right bomb
        y = -1;
        for(int i = (COUNT_ROWS*2-1);i >= 0; --i )
            if(m_array->getMiniCube(i,x + 1))
            {
                y = i;
                p = true;
                break;
            };
        CCPoint p2 = ccp(x*BOX_SIZE/2+ panel_left + BOX_SIZE + 0.6*_bomb->sprite->getContentSize().width,y*BOX_SIZE/2 + ground_height + _bomb->sprite->getContentSize().height/2);
        
        createMini(_bomb->sprite->getPosition(), p2, _bomb->type + 4);//right
    }
    else
    {
        int x = x1,y = y1;
        if(fx1 < 0.0)
        {
            x = -1;
        }
        y = -1;
        
        //create left bomb
        if(x > 0)
        {
            for(int i = (COUNT_ROWS*2-1);i >= 0; --i )
                if(m_array->getMiniCube(i,x - 1))
                {
                    y = i;
                    p = true;
                    break;
                };
            
            CCPoint p1 = ccp(x*BOX_SIZE/2 + panel_left - 0.5*_bomb->sprite->getContentSize().width + BOX_SIZE/2,y*BOX_SIZE/2 + ground_height + _bomb->sprite->getContentSize().height/2);
            createMini(_bomb->sprite->getPosition(), p1, _bomb->type + 4);
        }
        //create right bomb
        y = -1;
        for(int i = (COUNT_ROWS*2-1);i >= 0; --i )
            if(m_array->getMiniCube(i,x + 1))
            {
                y = i;
                p = true;
                break;
            };
        CCPoint p2 = ccp(x*BOX_SIZE/2 + panel_left+ BOX_SIZE + 0.5*_bomb->sprite->getContentSize().width,y*BOX_SIZE/2 + ground_height + _bomb->sprite->getContentSize().height/2);
        
        createMini(_bomb->sprite->getPosition(), p2, _bomb->type + 4);
        
    };
    CCLOG("collision81 %d",p);
    return p;
};

bool CKBombs::collision85(Sbomb* _bomb)
{
    
    int x1 = (left_border)/(BOX_SIZE/2);
    int y1 = (_bomb->sprite->getPositionY() - ground_height)/(BOX_SIZE/2);
    
    while (m_array->getMiniCube(y1 + 1,x1) != 0) {
        y1++;
    };
    
    if( x1 < 0 || y1 < 0)
        return false;
    
    if(m_array->getMiniCube(y1,x1) != 0)
    {
        m_array->destroyMiniCub(x1, y1);
        if(_bomb->type == 86)
            m_array->destroyMiniCub(x1, y1 - 1);
        
        if(_bomb->type == 87)
        {
            m_array->destroyMiniCub(x1, y1 - 1);
            m_array->destroyMiniCub(x1, y1 - 2);
        }
        
        return true;
    };
    return false;
};

//11. Самонавідна керована - скидується з літака, та потребує додаткового кліку гравця на потрібне місце ураження. З рівнем росте глибина ураження (1-2-3).
bool CKBombs::collision111(Sbomb* _bomb)
{
    CCPoint t1 = RotatePoint(_bomb->sprite->getPosition(),-_bomb->sprite->getRotation()* PI / 180,ccp(_bomb->sprite->getPositionX() - _bomb->sprite->getContentSize().width*0.4,_bomb->sprite->getPositionY() - _bomb->sprite->getContentSize().height/2));
    int x1 = (t1.x - BOX_SIZE/2 - panel_left)/(BOX_SIZE/2);
    int y1 = (t1.y - ground_height)/(BOX_SIZE/2);
    
    int y = y1/2;
    int x = x1/2;
    
    CCPoint t2 = RotatePoint(_bomb->sprite->getPosition(),-_bomb->sprite->getRotation()* PI / 180,ccp(_bomb->sprite->getPositionX() + _bomb->sprite->getContentSize().width*0.4,_bomb->sprite->getPositionY() - _bomb->sprite->getContentSize().height/2));
    int x2 = (t2.x - BOX_SIZE/2 - panel_left)/(BOX_SIZE/2);
    int y2 = (t2.y - ground_height)/(BOX_SIZE/2);
    
    
    CCPoint t3 = RotatePoint(_bomb->sprite->getPosition(),-_bomb->sprite->getRotation()* PI / 180,ccp(_bomb->sprite->getPositionX() - _bomb->sprite->getContentSize().width*0.4,_bomb->sprite->getPositionY() + _bomb->sprite->getContentSize().height/2));
    int x3 = (t3.x - BOX_SIZE/2 - panel_left)/(BOX_SIZE/2);
    int y3 = (t3.y - ground_height)/(BOX_SIZE/2);
    
    CCPoint t4 = RotatePoint(_bomb->sprite->getPosition(),-_bomb->sprite->getRotation()* PI / 180,ccp(_bomb->sprite->getPositionX() + _bomb->sprite->getContentSize().width*0.4,_bomb->sprite->getPositionY() + _bomb->sprite->getContentSize().height/2));
    int x4 = (t4.x - BOX_SIZE/2 - panel_left)/(BOX_SIZE/2);
    int y4 = (t4.y - ground_height)/(BOX_SIZE/2);
    
    int x5 = x1;
    bool p = false;
    
    if(!p)
        if(m_array->getMiniCube(y3,x3) != 0)
        {
            y = y3/2;
            x = x3/2;
            x5 = x3;
            p = true;
        };
    
    if(!p)
        if(m_array->getMiniCube(y4,x4) != 0)
        {
            y = y4/2;
            x = x4/2;
            x5 = x4;
            p = true;
        };
    
    if(!p)
        if( m_array->getMiniCube(y1,x1) != 0)
        {
            y = y1/2;
            x = x1/2;
            x5 = x1;
            p = true;
        };
    
    if(!p)
        if(m_array->getMiniCube(y2,x2) != 0)
        {
            x = x2/2;
            x5 = x2;
            y = y2/2;
            p = true;
        };
    if(p)
        switch (_bomb->type) {
            case 113:
                m_array->destroyCube(x*2, y*2);
                y--;
                m_array->destroyCube(x*2, y*2);
                break;
            case 112:
                if(m_array->getBigCube(y,x) != 0)
                {
                    m_array->destroyCube(x*2, y*2);
                }
                break;
            case 111:
                m_array->destroyMiniCub(x5, y*2);
                m_array->destroyMiniCub(x5, y*2 + 1);
                p = true;
                break;
            default:
                break;
        }
    
    
    return p;
};


//12. Потрійна розсувна - знищує кубики вниз-вліво-вправо від місця ураження. З рівнем збільшується зона ураження.
bool CKBombs::collision121(Sbomb* _bomb)
{
    int x1 = (left_border)/(BOX_SIZE/2);
    int y1 = (_bomb->sprite->getPositionY() - ground_height - _bomb->sprite->getContentSize().height/2)/(BOX_SIZE/2);
    
    
    int x = x1/2;
    int y = y1/2;
    CCLOG("curent_bomb->type %d",_bomb->type);
    
    while(m_array->getBigCube(y + 1,x) != 0)
    {
        y++;
    };
    
    if(_bomb->type == 123 || _bomb->type == 122)
    {
        if(m_array->getBigCube(y,x) != 0)//norm collision
        {
            m_array->destroyCube(x*2, y*2);
            if(_bomb->type == 123)
            {
                //m_array->destroyMiniCub(x*2 + 1, y*2 - 1);
                //m_array->destroyMiniCub(x*2, y*2 - 1);
                m_array->destroyCube(x*2, y*2 - 2);
            }
            //destroy left and right
            if(m_array->getBigCube(y + 1,x-1) != 0)
            {
                m_array->destroyCube(x*2 - 2, y*2 + 2);
            }
            if(m_array->getBigCube(y + 1,x+1) != 0)
            {
                m_array->destroyCube(x*2 + 2, y*2 + 2);
            }
        }
        else
        {
            if(y == -1)
            {
                y++;
                if(m_array->getBigCube(y,x) != 0)
                {
                    m_array->destroyCube(x*2, y*2);
                    return true;
                };
            };
            if(m_array->getBigCube(y,x-1) != 0)
            {
                m_array->destroyCube(x*2 - 2, y*2);
            }
            if(m_array->getBigCube(y,x+1) != 0)
            {
                m_array->destroyCube(x*2 + 2, y*2);
            }
        }
        return true;
    }
    
    if(_bomb->type == 121)
    {
        if(m_array->getBigCube(y,x) != 0)//norm collision
        {
            bool f = false;
            if(m_array->getMiniCube(y*2 + 1,x*2 + 1) != 0 ||m_array->getMiniCube(y*2 + 1,x*2) != 0)
            {
                m_array->destroyCube(x*2, y*2);
            }
            else
            {
                if(m_array->getMiniCube(y*2,x*2 + 1) != 0 ||m_array->getMiniCube(y*2,x*2) != 0)
                {
                    m_array->destroyCube(x*2, y*2 - 1);
                    f = true;
                }
            }
            if(!f)
                y++;
            m_array->destroyMiniCub(x*2 - 1, y*2);
            m_array->destroyMiniCub(x*2 - 1, y*2 + 1);
            m_array->destroyMiniCub(x*2 + 2, y*2);
            m_array->destroyMiniCub(x*2 + 2, y*2 + 1);
        }
        else//groound
        {
            if(y == -1)
            {
                y++;
                if(m_array->getBigCube(y,x) != 0)
                {
                    m_array->destroyCube(x*2, y*2);
                    return true;
                };
            };
            if(m_array->getBigCube(y,x-1) != 0)
            {
                bool p = false;
                if(m_array->destroyMiniCub(x*2 - 1, y*2))
                {
                    p = true;
                };
                if(m_array->destroyMiniCub(x*2 - 1, y*2+1))
                {
                    p = true;
                };
                if(p == false)
                {
                    m_array->destroyMiniCub(x*2 - 2, y*2);
                    m_array->destroyMiniCub(x*2 - 2, y*2 + 1);
                }
            }
            if(m_array->getBigCube(y,x+1) != 0)
            {
                m_array->destroyMiniCub(x*2 + 2, y*2);
                m_array->destroyMiniCub(x*2 + 2, y*2 + 1);
            }
        }
    }
    
    return false;
};
//kaskad
bool CKBombs::collision131(Sbomb* _bomb)
{
    float fx2 = (left_border + _bomb->sprite->getContentSize().width*0.45 )/(BOX_SIZE/2);
    
    int x1 = (left_border - _bomb->sprite->getContentSize().width*0.45 )/(BOX_SIZE/2);
    int y1 = (_bomb->sprite->getPositionY() - ground_height - _bomb->sprite->getContentSize().height/2)/(BOX_SIZE/2);
    int x2 = fx2;
    
    if(fx2 < 0)
        return false;
    
    while (m_array->getMiniCube(y1 + 1,x1) != 0 || m_array->getMiniCube(y1 + 1,x2) != 0) {
        y1++;
    };
    
    if(m_array->destroyMiniCub(x1, y1,1) || m_array->destroyMiniCub(x2, y1,1))
        return true;
    return false;
};
//faster
bool CKBombs::collision141(Sbomb* _bomb)
{
    int x1 = (left_border - 0.3*_bomb->sprite->getContentSize().width)/(BOX_SIZE/2);
    
    int y1 = (_bomb->sprite->getPositionY() - ground_height - _bomb->sprite->getContentSize().height*0.7)/(BOX_SIZE/2);
    
    int x2 = (left_border + 0.3*_bomb->sprite->getContentSize().width)/(BOX_SIZE/2);
    int x3 = (left_border)/(BOX_SIZE/2);
    
    
    if( x1 < 0 || y1 < 0)
        return false;
    
    while(m_array->getMiniCube(y1+1,x1) != 0)
    {
        y1++;
    };
    
    int y2 = y1;
    if(m_array->getMiniCube(y1,x1) != 0 || m_array->getMiniCube(y2,x2) != 0)
    {
        bool count = false;
        if(x3 != x2 || m_array->getMiniCube(y2,x2) == 0)
            if(m_array->getMiniCube(y1,x1)!= 0)
            {
                m_array->destroyMiniCub(x1, y1);
                m_array->destroyMiniCub(x1, y2 - 1);
                count = true;
            };
        if(!count)
            if(m_array->getMiniCube(y2,x2) != 0)
            {
                m_array->destroyMiniCub(x2, y2);
                m_array->destroyMiniCub(x2, y2 - 1);
            };
        return true;
    };
    return false;
};

//15.accelator bomb
int CKBombs::checkCollision(Sbomb* _bomb)
{
    int x1 = (left_border - 0.4*_bomb->sprite->getContentSize().width)/(BOX_SIZE/2);
    int y1 = (_bomb->sprite->getPositionY() - ground_height - _bomb->sprite->getContentSize().height*0.5 - BOX_SIZE/2)/(BOX_SIZE/2);
    int x2 = (left_border + 0.4*_bomb->sprite->getContentSize().width)/(BOX_SIZE/2);
    
    if( x1 < 0)
        return 0;
    if(y1 < 0)
    {
        y1 = 0;
    };
    int dd = y1%2;
    
    if(m_array->getMiniCube(y1,x1) != 0 ||  m_array->getMiniCube(y1,x2) != 0)
    {
        if(dd != 0 || y1 != 0)//it's not ground
            _bomb->sprite->setPosition(ccp(_bomb->sprite->getPositionX(),y1*(BOX_SIZE/2) + ground_height + _bomb->sprite->getContentSize().height*0.5 + BOX_SIZE/2));
        int x = 0,y = (y1)/2;
        y += ((y1) - y*2);
        
        if(m_array->getMiniCube(y1+1,x2) != 0)
        {
            x = x2/2;
            if(_bomb->type == 153)
            {
                // m_array->destroyMiniCub(x2 + 2, y*2 + 1);
                // m_array->destroyMiniCub(x2 + 2, y*2);
                m_array->destroyCube(x2 + 2, y*2);
            }
            if(_bomb->type != 151)
            {
                m_array->destroyMiniCub(x2 + 1, y*2 + 1);
                m_array->destroyMiniCub(x2 + 1, y*2);
            }
            m_array->destroyMiniCub(x2, y*2 + 1);
            m_array->destroyMiniCub(x2, y*2);
            return 2;
        };
        
        if(m_array->getMiniCube(y1+1,x1) != 0)
        {
            x = x1/2;
            CCLOG("collise accelator bomb left %d %d  %d",x,y,x1);
            if(_bomb->type == 153)
            {
                //m_array->destroyMiniCub(x1 - 2, y*2 + 1);
                //m_array->destroyMiniCub(x1 - 2, y*2);
                m_array->destroyCube(x2 - 4, y*2);
            }
            if(_bomb->type != 151)
            {
                m_array->destroyMiniCub(x1 - 1, y*2 + 1);
                m_array->destroyMiniCub(x1 - 1, y*2);
            }
            m_array->destroyMiniCub(x1, y*2 + 1);
            m_array->destroyMiniCub(x1, y*2 );
            
            return 2;
        };
        if(dd != 0 || y1 != 0)
            return 1;
    };
    if(dd == 0 && y1 == 0) //destroy if bomb on ground and detect mini cube
    {
        if(m_array->getMiniCube(y1,x1) != 0 ||  m_array->getMiniCube(y1,x2) != 0)
        {
            if(m_array->getMiniCube(y1,x2) != 0)
            {
                if(_bomb->type == 153)
                {
                    m_array->destroyCube(x2 + 2, y1);
                    //m_array->destroyMiniCub(x2 + 2, y1);
                }
                if(_bomb->type != 151)
                {
                    m_array->destroyMiniCub(x2 + 1, y1);
                    m_array->destroyMiniCub(x2 + 1, y1 + 1);
                }
                m_array->destroyMiniCub(x2, y1);
                m_array->destroyMiniCub(x2, y1 + 1);
                return 2;
            };
            
            if(m_array->getMiniCube(y1,x1) != 0)
            {
                if(_bomb->type == 153)
                {
                    m_array->destroyCube(x1 - 3, y1*2);
                    //m_array->destroyMiniCub(x1 - 2, y1);
                }
                if(_bomb->type != 151)
                {
                    m_array->destroyMiniCub(x1 - 1, y1);
                    m_array->destroyMiniCub(x1 - 1, y1 + 1);
                }
                m_array->destroyMiniCub(x1, y1 );
                m_array->destroyMiniCub(x1, y1 + 1 );
                return 2;
            };
            
            CCLOG("collise accelator bomb %d %d %d %d",x1,y1,x2,dd);
        }
    };
    
    return 0;
}
//16. Кулимет
bool CKBombs::collision161(Sbomb* _bomb)
{
    int x1 = (gun_posX)/(BOX_SIZE/2);
    int y1 = (_bomb->sprite->getPositionY() - ground_height - BOX_SIZE/3)/(BOX_SIZE/2);
    
    if( x1 < 0 || y1 < 0)
        return false;
    int k = 0,n = 6;
    
    if(_bomb->type  == 161)
        n = 3;
   // CCLOG("collision161 x %d y %d",x1,y1);

    bool p = false;
    y1++;
    int x = x1;
    while (k < n && x < COUNT_COLUMNS*2)
    {
        if(m_array->getMiniCube(y1,x) != 0)
        {
            p = true;
            break;
            k++;
        };
        x++;
    };
    
    if(!p)
        y1--;
    k = 0;
    while (k < n && x1 < COUNT_COLUMNS*2)
    {
        if(m_array->getMiniCube(y1,x1) != 0)
        {
            m_array->destroyMiniCub(x1, y1);
            k++;
            if(_bomb->type == 163)
            {
                m_array->destroyMiniCub(x1, y1 - 1);
            }
        };
        x1++;
    };

    return true;
};

