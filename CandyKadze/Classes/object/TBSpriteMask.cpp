//
//  TBSpriteMask.cpp
//  fgf
//
//  Created by PR1.Stigol on 07.06.13.
//
//
#include "TBSpriteMask.h"
const float limit_UV = 10.0f;
TBSpriteMask::~TBSpriteMask()
{
    CC_SAFE_RELEASE_NULL(maskTexture);
    CC_SAFE_RELEASE_NULL(m_pobTexture);
    CC_SAFE_DELETE(pBWShaderProgram);
};
void TBSpriteMask::move(float _x, float _y)
{
    // printf(" u %f %f %f %f ",m_sQuad.bl.texCoords.u,m_sQuad.br.texCoords.u,m_sQuad.tl.texCoords.u,m_sQuad.tr.texCoords.u);
    // printf(" v %f %f %f %f ",m_sQuad.bl.texCoords.v,m_sQuad.br.texCoords.v,m_sQuad.tl.texCoords.v,m_sQuad.tr.texCoords.v);
    m_sQuad.bl.texCoords.u += _x;
    
    m_sQuad.bl.texCoords.v += _y;
    
    m_sQuad.br.texCoords.u += _x;
    m_sQuad.br.texCoords.v += _y;
    
    m_sQuad.tl.texCoords.u += _x;
    m_sQuad.tl.texCoords.v += _y;
    
    m_sQuad.tr.texCoords.u += _x;
    m_sQuad.tr.texCoords.v += _y;
    
    if(m_sQuad.bl.texCoords.u > limit_UV)
    {
        m_sQuad.bl.texCoords.u -= limit_UV;
        m_sQuad.br.texCoords.u -= limit_UV;
        m_sQuad.tl.texCoords.u -= limit_UV;
        m_sQuad.tr.texCoords.u -= limit_UV;
    } else if(m_sQuad.bl.texCoords.u < -limit_UV)
    {
        m_sQuad.bl.texCoords.u += limit_UV;
        m_sQuad.br.texCoords.u += limit_UV;
        m_sQuad.tl.texCoords.u += limit_UV;
        m_sQuad.tr.texCoords.u += limit_UV;
    };
    if(m_sQuad.bl.texCoords.v > limit_UV)
    {
        m_sQuad.bl.texCoords.v -= limit_UV;
        m_sQuad.br.texCoords.v -= limit_UV;
        m_sQuad.tl.texCoords.v -= limit_UV;
        m_sQuad.tr.texCoords.v -= limit_UV;
    } else if(m_sQuad.bl.texCoords.v < -limit_UV)
    {
        m_sQuad.bl.texCoords.v += limit_UV;
        m_sQuad.br.texCoords.v += limit_UV;
        m_sQuad.tl.texCoords.v += limit_UV;
        m_sQuad.tr.texCoords.v += limit_UV;
    };
};
TBSpriteMask* TBSpriteMask::createWithSpriteFrameName(const char *pszSpriteFrameName)
{
    CCSpriteFrame *pFrame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(pszSpriteFrameName);
    
    char msg[256] = {0};
    sprintf(msg, "Invalid spriteFrameName: %s", pszSpriteFrameName);
    CCAssert(pFrame != NULL, msg);
    
    TBSpriteMask *pobSprite = new TBSpriteMask();
    if (pobSprite && pobSprite->initWithSpriteFrame(pFrame))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    CC_SAFE_DELETE(pobSprite);
    return NULL;

}

void TBSpriteMask::moveMask(const float &_x,const float &_y)
{
    m_ccQuad2.bl.x += _x;
    m_ccQuad2.bl.y += _y;
    
    m_ccQuad2.br.x += _x;
    m_ccQuad2.br.y += _y;
    
    m_ccQuad2.tl.x += _x;
    m_ccQuad2.tl.y += _y;
    
    m_ccQuad2.tr.x += _x;
    m_ccQuad2.tr.y += _y;
    
    if(m_ccQuad2.bl.x > limit_UV)
    {
        m_ccQuad2.bl.x -= limit_UV;
        m_ccQuad2.br.x -= limit_UV;
        m_ccQuad2.tl.x -= limit_UV;
        m_ccQuad2.tr.x -= limit_UV;
    } else if(m_ccQuad2.bl.x < -limit_UV)
    {
        m_ccQuad2.bl.x += limit_UV;
        m_ccQuad2.br.x += limit_UV;
        m_ccQuad2.tl.x += limit_UV;
        m_ccQuad2.tr.x += limit_UV;
    };
    if(m_ccQuad2.bl.y > limit_UV)
    {
        m_ccQuad2.bl.y -= limit_UV;
        m_ccQuad2.br.y -= limit_UV;
        m_ccQuad2.tl.y -= limit_UV;
        m_ccQuad2.tr.y -= limit_UV;
    } else if(m_ccQuad2.bl.y < -limit_UV)
    {
        m_ccQuad2.bl.y += limit_UV;
        m_ccQuad2.br.y += limit_UV;
        m_ccQuad2.tl.y += limit_UV;
        m_ccQuad2.tr.y += limit_UV;
    };
};

TBSpriteMask* TBSpriteMask::create(const char *_filename)
{
    TBSpriteMask *pobSprite = new TBSpriteMask();
    if (pobSprite && pobSprite->initWithFile(_filename))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    return NULL;
};

TBSpriteMask* TBSpriteMask::createWithTexture(cocos2d::CCTexture2D *pTexture,const cocos2d::CCRect &rect,bool rotated)
{
    TBSpriteMask *pobSprite = new TBSpriteMask();
    if (pobSprite && pobSprite->initWithTexture(pTexture, rect, rotated))
    {
        pobSprite->autorelease();
        return pobSprite;
    }
    return NULL;
};

void TBSpriteMask::setMaskRect(float _left,float _top,float _right,float _bottom)
{
    float x1 = _left,y1 = _bottom,x2 = _right,y2 = _top;
    m_ccQuad2.bl = vertex2(x1,y1);
    m_ccQuad2.tl = vertex2(x1,y2);
    m_ccQuad2.tr = vertex2(x2,y2);
    m_ccQuad2.br = vertex2(x2,y1);
};

void TBSpriteMask::buildMaskWithTexture(CCTexture2D *_texture,const CCRect &textureRect,bool _rotate,int _type_shader)
{
    //std::string shaderName =  type ? "MaskPositive.fsh" : "MaskNegative.fsh";
    
    const GLchar * fragmentSource1 = "\n\
    #ifdef GL_ES \n\
    precision lowp float; \n\
    #endif \n\
    \n\
    varying vec2 v_texCoord;\n\
    varying vec2 v_maskCoord;\n\
    uniform sampler2D u_texture;\n\
    uniform sampler2D u_mask;\n\
    \n\
    void main()\n\
    {\n\
    vec4 normalColor = texture2D(u_texture, v_texCoord).rgba;\n\
    vec4 maskColor = texture2D(u_mask, v_maskCoord).rgba;\n\
    float alpha = 1.0 - normalColor.r;\n\
    gl_FragColor = vec4(maskColor.r*normalColor.r,maskColor.g*normalColor.g, maskColor.b*normalColor.b, normalColor.a);\n\
    \n\
    }";

    const GLchar * fragmentSource2 = "\n\
    #ifdef GL_ES \n\
    precision lowp float; \n\
    #endif \n\
    \n\
    varying vec2 v_texCoord;\n\
    varying vec2 v_maskCoord;\n\
    uniform sampler2D u_texture;\n\
    uniform sampler2D u_mask;\n\
    \n\
    void main()\n\
    {\n\
    vec4 normalColor = texture2D(u_texture, v_texCoord).rgba;\n\
    vec4 maskColor = texture2D(u_mask, v_maskCoord).rgba*normalColor.a;\n\
    gl_FragColor = vec4(normalColor.r +  maskColor.a,maskColor.a +normalColor.g,maskColor.a + normalColor.b, normalColor.a);\n\
    \n\
    }";
    
    const GLchar * fragmentSource3 = "\n\
    #ifdef GL_ES \n\
    precision lowp float; \n\
    #endif \n\
    \n\
    varying vec2 v_texCoord;\n\
    varying vec2 v_maskCoord;\n\
    uniform sampler2D u_texture;\n\
    uniform sampler2D u_mask;\n\
    \n\
    void main()\n\
    {\n\
    vec4 normalColor = texture2D(u_texture, v_texCoord).rgba;\n\
    vec4 maskColor = texture2D(u_mask, v_maskCoord).rgba*normalColor.a;\n\
    gl_FragColor = vec4(normalColor.r +  maskColor.a*0.5,maskColor.a*0.5 +normalColor.g,maskColor.a*0.5 + normalColor.b, normalColor.a);\n\
    \n\
    }";
    

    const GLchar * fragmentSource4 = "\n\
    #ifdef GL_ES \n\
    precision lowp float; \n\
    #endif \n\
    \n\
    varying vec2 v_texCoord;\n\
    varying vec2 v_maskCoord;\n\
    uniform sampler2D u_texture;\n\
    uniform sampler2D u_mask;\n\
    \n\
    void main()\n\
    {\n\
    vec4 normalColor = texture2D(u_texture, v_texCoord).rgba;\n\
    vec4 maskColor = texture2D(u_mask, v_maskCoord).rgba*normalColor.a;\n\
    gl_FragColor = vec4(normalColor.r +  maskColor.a*0.5,normalColor.g + maskColor.a*0.5,normalColor.b + maskColor.a*0.5, normalColor.a);\n\
    \n\
    }";
    
    const GLchar * fragmentSource5 = "\n\
    #ifdef GL_ES \n\
    precision lowp float; \n\
    #endif \n\
    \n\
    varying vec2 v_texCoord;\n\
    varying vec2 v_maskCoord;\n\
    uniform sampler2D u_texture;\n\
    uniform sampler2D u_mask;\n\
    \n\
    void main()\n\
    {\n\
    vec4 normalColor = texture2D(u_texture, v_texCoord).rgba;\n\
    vec4 maskColor = texture2D(u_mask, v_maskCoord).rgba;\n\
    gl_FragColor = normalColor + maskColor;\n\
    \n\
    }";
    
    const GLchar * ccPositionTextureColor_vert2 =
    #include "ccShader_2PositionTexture_vert.h"
    
    pBWShaderProgram = new CCGLProgram();

    if(_type_shader == 1)
    {
        pBWShaderProgram->initWithVertexShaderByteArray(ccPositionTextureColor_vert2,fragmentSource1);
    }else if(_type_shader == 2)
    {
        pBWShaderProgram->initWithVertexShaderByteArray(ccPositionTextureColor_vert2,fragmentSource2);
    }else if(_type_shader == 3)
    {
        pBWShaderProgram->initWithVertexShaderByteArray(ccPositionTextureColor_vert2,fragmentSource3);
    }else if(_type_shader == 4)
    {
        pBWShaderProgram->initWithVertexShaderByteArray(ccPositionTextureColor_vert2,fragmentSource4);
    }else if(_type_shader == 5)
    {
        pBWShaderProgram->initWithVertexShaderByteArray(ccPositionTextureColor_vert2,fragmentSource5);
    }else
    {
        pBWShaderProgram->initWithVertexShaderByteArray(ccPositionTextureColor_vert2,fragmentSource1);
    }
    
    this->setShaderProgram(pBWShaderProgram);
    pBWShaderProgram->addAttribute(kCCAttributeNamePosition, kCCVertexAttrib_Position);
    pBWShaderProgram->addAttribute(kCCAttributeNameTexCoord, kCCVertexAttrib_TexCoords);
   // pBWShaderProgram->addAttribute(kCCAttributeNameColor, kCCVertexAttrib_Color);
    
    pBWShaderProgram->addAttribute("a_maskCoord", kCCVertexAttrib_Color);
    
    pBWShaderProgram->link();
    pBWShaderProgram->updateUniforms();
    
    maskLocation = glGetUniformLocation(getShaderProgram()->getProgram(), "u_mask");
    m_uTextureLocation = glGetUniformLocation( getShaderProgram()->getProgram(), "u_texture");

    maskTexture = _texture;
    maskTexture->retain();
    
    float x1 = 0.0,y1 = 0.0,x2 = 1.0,y2 = 1.0;
    float scale = CCDirector::sharedDirector()->getContentScaleFactor();
    x1 = textureRect.origin.x*scale/_texture->getPixelsWide();
    y1 = textureRect.origin.y*scale/_texture->getPixelsHigh();
    
    if(_rotate)
    {
        x2 = (textureRect.origin.x + textureRect.size.height)*scale/_texture->getPixelsWide();
        y2 = (textureRect.origin.y + textureRect.size.width)*scale/_texture->getPixelsHigh();
        
        m_ccQuad2.bl = vertex2(x1,y1);
        m_ccQuad2.tl = vertex2(x1,y2);
        m_ccQuad2.tr = vertex2(x2,y2);
        m_ccQuad2.br = vertex2(x2,y1);
    }
    else
    {
        x2 = (textureRect.origin.x + textureRect.size.width)*scale/_texture->getPixelsWide();
        y2 = (textureRect.origin.y + textureRect.size.height)*scale/_texture->getPixelsHigh();
        
        m_ccQuad2.bl = vertex2(x2,y1);
        m_ccQuad2.tl = vertex2(x1,y1);
        m_ccQuad2.tr = vertex2(x1,y2);
        m_ccQuad2.br = vertex2(x2,y2);
    }
    

    
    //setTextureRect(const cocos2d::CCRect &rect)
//    CCLog("bl(%f %f)",m_ccQuad2.bl.x,m_ccQuad2.bl.y);
//    CCLog("tl(%f %f)",m_ccQuad2.tl.x,m_ccQuad2.tl.y);
//    CCLog("tr(%f %f)",m_ccQuad2.tr.x,m_ccQuad2.tr.y);
//    CCLog("br(%f %f)",m_ccQuad2.br.x,m_ccQuad2.br.y);
//    CCLog("textureRect %f %f %f %f",textureRect.origin.x,textureRect.origin.y, textureRect.size.width,textureRect.size.height);
    //m_pShaderProgram->setUniformsForBuiltins();
}

void TBSpriteMask::draw()
{
//    if(!isVisible())
//        return;
    //CCNode::visit()
    CC_NODE_DRAW_SETUP();
    ccGLEnableVertexAttribs(kCCVertexAttribFlag_PosColorTex);
    ccGLBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    getShaderProgram()->setUniformForModelViewProjectionMatrix();
    
    ccGLBlendFunc( m_sBlendFunc.src, m_sBlendFunc.dst );
    
    if (m_pobTexture != NULL)
    {
        ccGLBindTexture2D( m_pobTexture->getName() );
    }
    else
    {
        ccGLBindTexture2D(0);
    }
    
    glUniform1i(m_uTextureLocation, 0);
    
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, maskTexture->getName());
    glUniform1i(maskLocation, 1);

    
#define kQuadSize sizeof(m_sQuad.bl)
    long offset = (long)&m_sQuad;
    
    // vertex
    int diff = offsetof( ccV3F_C4B_T2F, vertices);
    glVertexAttribPointer(kCCVertexAttrib_Position, 3, GL_FLOAT, GL_FALSE, kQuadSize, (void*) (offset + diff));
    
    // texCoods
    diff = offsetof( ccV3F_C4B_T2F, texCoords);
    glVertexAttribPointer(kCCVertexAttrib_TexCoords, 2, GL_FLOAT, GL_FALSE, kQuadSize, (void*)(offset + diff));
    

    glVertexAttribPointer(kCCVertexAttrib_Color, 2, GL_FLOAT, GL_FALSE, sizeof(m_ccQuad2.bl), (void*)(&m_ccQuad2));
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glActiveTexture(GL_TEXTURE0);
}