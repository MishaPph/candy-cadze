//
//  CKAirplane.h
//  CandyKadze
//
//  Created by PR1.Stigol on 20.09.12.
//
//

#ifndef __CandyKadze__CKAirplane__
#define __CandyKadze__CKAirplane__

#include <iostream>
#include "cocos2d.h"
#include "CKCubeArray.h"
#include "CKBomb.h"
#include "CKAirplaneBase.h"
#include "TBSpriteMask.h"

using namespace cocos2d;

class CKBaseNode;
class CKAirplane:public CKAirplaneBase
{
private:
    struct run_bonus
    {
        unsigned char type;
        float progress;
        bool active;
        int b_shield;
        float start,end;
        void calcProgress()
        {
            progress = (end - start)/end;
        };
    };
    static const int COUNT_SHIELD_DESTROY_CUBE = 2;
    static const int COUNT_SHIELD_LOOP = 4;
    static const int COUNT_BOMBX2_LOOP = 2;
    static const int COUNT_GUN_LOOP = 2;
    static const int COUNT_PHANTOM_LOOP = 2;
    static const int COUNT_BONUS_SLOT = 2;
    static const int COUNT_SLOW_LOOP = 2;
    static const int COUNT_ANIMATION_FRAME = 15;
    
    const float PERCENT_PLAY_END_BONUS_ATTENTION = 0.28;
    const float SPEED_SLOW_BONUS = 0.5f;
    
    virtual int winUpdate(const float &dt);
    
    virtual int gameOverUpdete();
    virtual void checkCrash();


    float bomber_limit_x;
    CKBombs *m_bomb,*m_bomb_ghost;
    run_bonus bonus_type[COUNT_BONUS_SLOT];

    CCSprite* animation_sprite,*shield;
    CCRenderTexture *ghost_rtt;
    CCSprite* bomber;
    CCSprite* machine_gun_blink;
    float bomber_posX;
    unsigned char bomber_fire; //2-4
    CCActionInterval* action_fly;
    CCAnimation * win_animation;
    CCAnimation * fly_anim;
    CCParticleSystem* ps_smoke;
    CCParticleSystem* ps_machine_gun;
    CCAction *machine_gun_action;
    float last_dt;
    bool draw_smoke;
    bool show_particle_smoke;
    bool show_particle_machine_gun;
    bool enable_particle;
    bool active_bonus;
    float step_up_bonus;
    void updateBonusAirplane(const float &dt);
    void updatePositionParticle(void);
   // void updatePosition(const float &dt);
    void updateAnimationMask(void);
    void animateScrew();
    void animatePilot();
    void checkCollision();
    unsigned char last_bomb_id;
    
   // CCRenderTexture *rtt_noise;
  //  CCNode* m_node_noise;
    
    //CCRenderTexture *rtt_scarf_top;
    TBSpriteMask *scarf,*m_nose;
    //CCNode* m_node_scarf_top;
    CKBaseNode *m_anim;
    void createNose();
    void createScarf();
    void createScarfTop();
    void callFuncCrashMovePosition();
    void createSmoke();
    void createMachineGunBlink();
    void showWheel();
    CCRenderTexture *rtt_crash_pilot;
    CCNode* pilot_body,*pilot_node;
    void animationSplashEffect(CCNode *_sender,void *_data);
    void animateMoveToFinishPilot();
    void createAnimationLeg();
    int anim_finish_move_pilot;
    //target move
    std::list<CCPoint> target_point_list;
    std::list<CCPoint>::iterator target_it;
    CCNode *target_sprite;
    CCPoint direct,direct2;
    CCPoint kamikadze_start_position;
    virtual void updateDoubleSpeed();
public:
    virtual bool createCrashAnimation();
    virtual bool createWinAnimation();
    
    CKAirplane();
    virtual ~CKAirplane();
    
    bool haveActiveBonus()const;
    bool canAutoShot();
    bool isBonusRun(int _type);
    void setKamikadzeStartPosition(const CCPoint &_pos);
    unsigned char getLastBombID()const;
    
    void startMachineGun();
    void stopMachineGun();
    void setEnableMachineGun(bool _enable);
    
    void setEnableParticle(bool _en);
    void stopAndHideAllBonus();
    void setStepUpBonus(float _step);
    void stopAllBonus();
    void stopBonus(unsigned char _id);
    int runBonus(unsigned char _id);
    
    CCParticleSystem* getParticleSmoke(int _num = 0);
    
    virtual bool touch(CCPoint _pos,unsigned char _type = 1);
    virtual bool createBomb(unsigned char _type = 1);
    
    float getBonusProgress(int _num = 0)const ;
    void setLimitBomberX(float _limit);
    unsigned char getBonusType(int _num = 0) const;
    
    virtual void setBombs(CKBombBase *_bomb);
    void setGhostBomb(CKBombs *_bomb);
    
    virtual CCSprite* create(const char* _name);
    CCSprite* createAnim(const char* _name);
    
    virtual const unsigned char update(const float &dt);
    virtual void reset();
    virtual void pause();
    virtual void resume();
};

class CKAirplane90: public CKAirplaneBase
{
    CCParticleSystem* m_emitter90[3];
    CCParticleSystem* m_emitter90e[3];

    bool play_firework,is_new_score;
    int time_start_new_particle,lvl_complite;
    CKBomb90 *m_bomb;
    virtual int winUpdate(const float &dt);
    virtual int gameOverUpdete();
    virtual bool createWinAnimation();
    virtual void checkCrash();
    virtual bool createCrashAnimation();
    virtual void updateDoubleSpeed();
public:
    virtual bool createBomb(unsigned char _type = 1);
    virtual bool touch(CCPoint _pos,unsigned char _type = 1);
    virtual void setBombs(CKBombBase *_bomb);
    virtual ~CKAirplane90(){};
    CKAirplane90();
    virtual CCSprite* create(const char* _name);
    virtual const unsigned char update(const float &dt);
    virtual void reset();
    virtual void pause();
    virtual void resume();
};

#endif /* defined(__CandyKadze__CKAirplane__) */
