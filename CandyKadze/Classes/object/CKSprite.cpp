//
//  CKSprite.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 02.10.12.
//
//

#include "CKSprite.h"
const float limit_UV = 10.0f;

void CKSprite::move(float _x, float _y)
{
   // printf(" u %f %f %f %f ",m_sQuad.bl.texCoords.u,m_sQuad.br.texCoords.u,m_sQuad.tl.texCoords.u,m_sQuad.tr.texCoords.u);
   // printf(" v %f %f %f %f ",m_sQuad.bl.texCoords.v,m_sQuad.br.texCoords.v,m_sQuad.tl.texCoords.v,m_sQuad.tr.texCoords.v);
    m_sQuad.bl.texCoords.u += _x;
    
    m_sQuad.bl.texCoords.v += _y;
    
    m_sQuad.br.texCoords.u += _x;
    m_sQuad.br.texCoords.v += _y;
    
    m_sQuad.tl.texCoords.u += _x;
    m_sQuad.tl.texCoords.v += _y;
    
    m_sQuad.tr.texCoords.u += _x;
    m_sQuad.tr.texCoords.v += _y;
    
    if(m_sQuad.bl.texCoords.u > limit_UV)
    {
        m_sQuad.bl.texCoords.u -= limit_UV;
        m_sQuad.br.texCoords.u -= limit_UV;
        m_sQuad.tl.texCoords.u -= limit_UV;
        m_sQuad.tr.texCoords.u -= limit_UV;
    } else if(m_sQuad.bl.texCoords.u < -limit_UV)
    {
        m_sQuad.bl.texCoords.u += limit_UV;
        m_sQuad.br.texCoords.u += limit_UV;
        m_sQuad.tl.texCoords.u += limit_UV;
        m_sQuad.tr.texCoords.u += limit_UV;
    };
    if(m_sQuad.bl.texCoords.v > limit_UV)
    {
        m_sQuad.bl.texCoords.v -= limit_UV;
        m_sQuad.br.texCoords.v -= limit_UV;
        m_sQuad.tl.texCoords.v -= limit_UV;
        m_sQuad.tr.texCoords.v -= limit_UV;
    } else if(m_sQuad.bl.texCoords.v < -limit_UV)
    {
        m_sQuad.bl.texCoords.v += limit_UV;
        m_sQuad.br.texCoords.v += limit_UV;
        m_sQuad.tl.texCoords.v += limit_UV;
        m_sQuad.tr.texCoords.v += limit_UV;
    };
}

const ccTex2F& CKSprite::getMove()
{
    return m_sQuad.bl.texCoords;
};



void CKSprite::ondraw(void)
{
    // quick return if not visible. children won't be drawn.
    kmGLPushMatrix();
    
    
    this->transform();

    
    CC_NODE_DRAW_SETUP();

    ccGLEnableVertexAttribs( kCCVertexAttribFlag_PosColorTex);
    
#define kQuadSize sizeof(m_sQuad.bl)
    long offset = (long)&m_sQuad;
    
    // vertex
    int diff = offsetof( ccV3F_C4B_T2F, vertices);
    glVertexAttribPointer(kCCVertexAttrib_Position, 3, GL_FLOAT, GL_FALSE, kQuadSize, (void*) (offset + diff));
    
    // texCoods
    diff = offsetof( ccV3F_C4B_T2F, texCoords);
    glVertexAttribPointer(kCCVertexAttrib_TexCoords, 2, GL_FLOAT, GL_FALSE, kQuadSize, (void*)(offset + diff));
    
    // color
    diff = offsetof( ccV3F_C4B_T2F, colors);
    glVertexAttribPointer(kCCVertexAttrib_Color, 4, GL_UNSIGNED_BYTE, GL_TRUE, kQuadSize, (void*)(offset + diff));
    
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    
    CHECK_GL_ERROR_DEBUG();
    
    
    CC_INCREMENT_GL_DRAWS(1);
    
    CC_PROFILER_STOP_CATEGORY(kCCProfilerCategorySprite, "CCSprite - draw");
    
    
    
    // reset for next frame
    m_nOrderOfArrival = 0;

    
    kmGLPopMatrix();
};

//void CKMaskSprite::draw(void)
//{
//
//    
//  //  getSprite()->setPosition(m_tPosition);
//   // getSprite()->draw();
//};
void CKMaskSprite::visit(void)
{
    m_rtt->clear(0, 0, 0, 0);
    m_rtt->begin();
    if(m_sprite)
        m_sprite->visit();
    if(m_mask)
        m_mask->visit();
    m_rtt->end();
    //getSprite()->visit();
    if (!m_bIsVisible)
    {
        return;
    }
    kmGLPushMatrix();
    
    if (m_pGrid && m_pGrid->isActive())
    {
        m_pGrid->beforeDraw();
    }
    
    this->transform();
    
    CCNode* pNode = NULL;
    unsigned int i = 0;
    
    if(m_pChildren && m_pChildren->count() > 0)
    {
        sortAllChildren();
        // draw children zOrder < 0
        ccArray *arrayData = m_pChildren->data;
        for( ; i < arrayData->num; i++ )
        {
            pNode = (CCNode*) arrayData->arr[i];
            
            if ( pNode && pNode->getZOrder() < 0 )
            {
                pNode->visit();
            }
            else
            {
                break;
            }
        }
        // self draw
        
        this->draw();
        
        for( ; i < arrayData->num; i++ )
        {
            pNode = (CCNode*) arrayData->arr[i];
            if (pNode)
            {
                pNode->visit();
            }
        }
    }
    else
    {
        this->draw();
    }
    
    // reset for next frame
    m_nOrderOfArrival = 0;
    
    if (m_pGrid && m_pGrid->isActive())
    {
        m_pGrid->afterDraw(this);
    }
    
    kmGLPopMatrix();
};

CKMaskSprite::~CKMaskSprite()
{
    //m_rtt->release();
    removeAllChildrenWithCleanup(true);
};

CKMaskSprite::CKMaskSprite()
{
    m_mask = NULL;
    m_sprite = NULL;
};

bool CKMaskSprite::initWithWidthAndHeight(int w ,int h, CCTexture2DPixelFormat eFormat, GLuint uDepthStencilFormat)
{
   // CCRenderTexture::initWithWidthAndHeight(w, h, eFormat,uDepthStencilFormat);
  //  getSprite()->setFlipY(true);
    m_rtt = CCRenderTexture::create(w, h, eFormat, uDepthStencilFormat);
    m_rtt->retain();
    m_rtt->getSprite()->setFlipY(true);
    addChild(m_rtt);
    return true;
};

CKMaskSprite * CKMaskSprite::create(int w, int h, CCTexture2DPixelFormat eFormat, GLuint uDepthStencilFormat)
{
    CKMaskSprite *pRet = new CKMaskSprite();
    
    if(pRet)
    {
        pRet->initWithWidthAndHeight(w, h, eFormat, uDepthStencilFormat);
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
};
