//
//  CKBonus.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 25.09.12.
//
//

#include "CKBonus.h"
#include "CKFileInfo.h"
#include "CKFileOptions.h"
#include "TBSpriteMask.h"

CKBonus::~CKBonus()
{
    CC_SAFE_RELEASE(anim);
    bonus_it =  bonus_list.begin();
    while (bonus_it != bonus_list.end()) {
        m_layer->removeChild((*bonus_it), true);
        bonus_it++;
    };
    bonus_list.clear();
    
    std::list<CKBonusInfo *>::iterator iit = info_list.begin();
    while (iit != info_list.end()) {
        CC_SAFE_DELETE(*iit);
        iit++;
    }
}
CKBonus::CKBonus()
{
    panel_left = 0;
    if(CCDirector::sharedDirector()->getWinSize().width == 568)
    {
        panel_left = 44;
    };
    anim = NULL;
//    bonus_it = bonus_list.begin();
//    
//    while(bonus_it != bonus_list.end())
//    {
//     //   CCNode * _sprite = *bonus_it;
//     //   bonus_it++;
//    };
//    bonus_list.clear();
};

void CKBonus::setLayer(CCNode *_layer)
{
    m_layer = _layer;
};

void CKBonus::restart()
{
    bonus_it =  bonus_list.begin();
    while (bonus_it != bonus_list.end()) {
        m_layer->removeChild((*bonus_it), true);
        bonus_it++;
    };
    bonus_list.clear();
    
    CCLOG("list_bomb_bonus %d list_airplane_bonus %d",list_bomb_bonus.size(),list_airplane_bonus.size());
    //create bomb bonus after load plist
    it = list_bomb_bonus.begin();
    while(it != list_bomb_bonus.end())
    {
        int t = it->second;
        while (t > 0) {
            if(!genBonusArray(it->first,true))//if this position free
            {
                t--;
            }
        };
        it++;
    };
    
    //create bomb bonus after load plist
    it = list_airplane_bonus.begin();
    while(it != list_airplane_bonus.end())
    {
         int t = it->second;
        while (t > 0) {
            if(!genBonusArray(it->first,false))//if this position free
            {
                t--;
            }
        };
        it++;
    };
};

unsigned int CKBonus::isNewBonus()
{    
    it = list_bomb_bonus.begin();
  //  bool p = false;
    CCLOG("CKFileInfo::Instance().open_bonus.size() %d",CKFileInfo::Instance().open_bonus.size());
  //  if(CKFileInfo::Instance().open_bonus.size() <= 0)
  //      return 0;
    
    if(!list_bomb_bonus.empty())
        while( it != list_bomb_bonus.end())
        {
           if(!CKFileInfo::Instance().isOpenBonus(it->first))
           {
               return it->first;
           }
            it++;
        };
    
    it = list_airplane_bonus.begin();
    if(!list_airplane_bonus.empty())
        while( it != list_airplane_bonus.end())
        {
            CCLOG("it->first %d",it->first);
            if(it->first == 0)
            {
                it++;
                continue;
            }
            if(!CKFileInfo::Instance().isOpenBonus(1000 + it->first))
            {
                return (1000 + it->first);
            }
            it++;
        };
    
    return 0;
};
std::string CKBonus::getPlaneName(int _type)
{
    switch (_type) {
        case Bonus_Airplane_Up:
            return "plane_up.png";
            break;
        case Bonus_Airplane_Slow:
            return "plane_slow.png";
            break;
        case Bonus_Airplane_Armor:
            return "plane_armor.png";
            break;
        case Bonus_Airplane_Speed:
            return "plane_speed.png";
            break;
        case Bonus_Airplane_Kamikadze:
            return "plane_kamikadze.png";
            break;
        case Bonus_Airplane_Gun:
            return "plane_gun.png";
            break;
        case Bonus_Airplane_Bomber:
            return "plane_bomber.png";
            break;
        case Bonus_Airplane_Phantom:
            return "plane_phantom.png";
            break;
        default:
            return "bonus_plane1.png";
            break;
    };
};

void CKBonus::update(const float &_dt)
{
    bonus_it = bonus_list.begin();
    while(bonus_it != bonus_list.end())
    {
        CCNode * _sprite = (CCNode *)*bonus_it;
        if(_sprite)
        {
            float step = 0.025*60.0*_dt;
            CKBonusInfo *m_info = static_cast<CKBonusInfo *>(_sprite->getUserData());
            m_info->offset_mask += step;
            if(m_info->offset_mask > 5.5)
            {
                m_info->mask->moveMask(0.0, -5.5);
                m_info->offset_mask -= 5.5;
            }else
            {
                m_info->mask->moveMask(0.0, step);
            };
        };
        bonus_it++;
    };
};

void CKBonus::createBonus(unsigned char _x,unsigned char _y,unsigned char type,bool _bonus_bomb)
{
    if(type == -1 || type == 0)
    {
        return;
    }
    TBSpriteMask* sprite = TBSpriteMask::createWithSpriteFrameName("bonus_stroke.png");
    
    CCTexture2D *texture = CCTextureCache::sharedTextureCache()->addImage(CK::loadImage("bonus_blik.png").c_str());
    sprite->buildMaskWithTexture(texture,CCRect(0, 0, texture->getContentSize().width, texture->getContentSize().height),false,5);
    sprite->moveMask(0.0, -0.2 -0.2*(rand()%8));
    
    CCSprite* body = NULL,*sprite2;
    // if(CKHelper::Instance().getIsIpad())
    {
        sprite2 = CCSprite::createWithSpriteFrameName("bonus_plane_back.png");
        sprite2->setPosition(ccp(sprite->getContentSize().width/2,sprite->getContentSize().height/2));
        CCRotateBy* animate = CCRotateBy::create(1.0, 60);
        CCActionInterval* repeat = CCRepeatForever::create( animate );
        sprite2->runAction(repeat);
    }
    
    CKBonusInfo *m_info = new CKBonusInfo;
    m_info->is_bomb = _bonus_bomb;
    m_info->type = type;
    m_info->count = 1;
    m_info->mask = sprite;
    m_info->offset_mask = 0.0;
    
    if(_bonus_bomb)
    {
        if(CKFileOptions::Instance().isDebugMode())
        {
            type = char(type/10)*10 + CKFileOptions::Instance().getBombLvl();
            CCLOG("createBonus:: debug type %d",type);
        };
        
        body = CCSprite::createWithSpriteFrameName(CKBonusMgr::Instance().getBonusNameById(type).c_str());
        //sprite1->setScale(0.5);
        int d = rand()%MAX(0,(CKBonusMgr::Instance().getInfoById(type)->max - CKBonusMgr::Instance().getInfoById(type)->min)) + CKBonusMgr::Instance().getInfoById(type)->min;
        if(d > 1)
        {
            char str2[NAME_MAX];
            sprintf(str2, "%d",d);
            CCLabelTTF *label = CCLabelTTF::create(str2, "Marker Felt", BOX_SIZE/3);
            label->setColor(ccc3(255,255,255));
            label->setPosition(ccp( body->getContentSize().width*0.2, body->getContentSize().height*0.7));
//            label->setTag(1);
            body->addChild(label);
            m_info->count = d;
        }
      //  sprite1->setRotation(45);
        body->setTag(type);
    }
    else
    {
        CCLOG("airplane type %d",type);//create sprite from bonus type
        body = CCSprite::createWithSpriteFrameName(getPlaneName(type).c_str());
        body->setTag(type);//airplane bonus
    }
    body->setUserData(m_info);
    info_list.push_back(m_info);
    

    
    //body->setPosition(ccp(sprite->getContentSize().width/2,sprite->getContentSize().height/2));
   // node->addChild(body);
   // body->setTag(1);

   // CCSprite* sprite3 = CCSprite::createWithSpriteFrameName("bonus_stroke.png");
  //  sprite3->setPosition(ccp(sprite->getContentSize().width/2,sprite->getContentSize().height*1.5));
    

    sprite->setPosition(ccp(sprite->getContentSize().width/2,sprite->getContentSize().height/2));
    body->addChild(sprite);
    
//    CCFiniteTimeAction*  action = CCSequence::create( CCPlace::create(sprite3->getPosition()),CCMoveBy::create(1, CCPointMake(0,-sprite->getContentSize().height*2)), CCDelayTime::create(2 + CCRANDOM_MINUS1_1()), NULL);
//    sprite3->setBlendFunc((ccBlendFunc){GL_DST_ALPHA,GL_SRC_COLOR});
//    sprite3->runAction(CCRepeatForever::create((CCActionInterval*)action));    
    
    bonus_list.push_back(body);

    int add_x = BOX_SIZE;
    int add_y = CKHelper::Instance().ground_height + BOX_SIZE/2;
    
    body->setPosition(ccp(_y*BOX_SIZE + add_x + panel_left,_x*BOX_SIZE + add_y));
    m_layer->addChild(body,zOrder_Bonuses);
};

bool CKBonus::genBonusArray(unsigned char _type,bool _bonus_bomb)
{
   // CCLOG("generate new bonus type %d",_type);
    unsigned char x = 0,y = 0;
    x = rand()%(COUNT_ROWS);
    y = rand()%(COUNT_COLUMNS);
    if(m_array->getBigCube(x,y) != 0)
    {
        int add_x = BOX_SIZE;
        int add_y = CKHelper::Instance().ground_height +  BOX_SIZE/2;
        CCPoint _p = ccp(y*BOX_SIZE + add_x + panel_left, x*BOX_SIZE + add_y);
        
        //find bonus in this position
        bonus_it = bonus_list.begin();
        while(bonus_it != bonus_list.end())
        {
            CCNode * _sprite = *bonus_it;
            if(abs(_p.x - _sprite->getPosition().x) < BOX_SIZE/2 && abs(_p.y - _sprite->getPosition().y) < BOX_SIZE/2)
            {
                return true;
            }
            bonus_it++;
        };
        createBonus(x,y,_type,_bonus_bomb);
        return false;
    };
   return true; 
};

void CKBonus::genBonus(int _count)
{
    BOX_SIZE = CKHelper::Instance().BOX_SIZE;
    CCLOG("CKHelper::Instance().lvl_number %d",CKHelper::Instance().lvl_number);
    loadBonusWithPlist();

    //create bomb bonus after load plist
    it = list_bomb_bonus.begin();
    while(it != list_bomb_bonus.end())
    {
         int t = it->second;
        while (t > 0) {
            if(!genBonusArray(it->first,true))//if this position free
            {
                t--;
            }
        };
        it++;
    };
    //create bomb bonus after load plist
    it = list_airplane_bonus.begin();
    while(it != list_airplane_bonus.end())
    {
         int t = it->second;
        while (t > 0) {
            
            if(!genBonusArray(it->first,false))//if this position free
            {
                t--;
            }
        };
        it++;
    };
};

unsigned char CKBonus::loadBonusWithPlist()
{
    CCDictionary *dict = CCDictionary::createWithContentsOfFile("levels.plist");
    first_bonus =0;
    if(dict->objectForKey("difficult"))
    {
        CCDictionary *diff  = (CCDictionary *)dict->objectForKey("difficult");
        CCArray* ar_level = (CCArray*)diff->objectForKey(CK::c_difficult[CKHelper::Instance().lvl_speed]);
        CCLOG("DIFFICULT:: %s",CK::c_difficult[CKHelper::Instance().lvl_speed]);
        CCObject* pObj_l = NULL;
        int lvl = 0;
        std::vector<int> tmp_bonus_list;
        CCARRAY_FOREACH(ar_level, pObj_l)
        {
            if(lvl == CKHelper::Instance().lvl_number)//load first list
            {
                CCDictionary *dict_lvl = (CCDictionary*)pObj_l;
                if(dict_lvl->objectForKey("bomb"))
                {
                    CCArray* array_bomb = (CCArray*)dict_lvl->objectForKey("bomb");
                    CCObject* Obj_bomb = NULL;
                    CCARRAY_FOREACH(array_bomb, Obj_bomb)
                    {
                        CCDictionary *dict_bomb = (CCDictionary*)Obj_bomb;
                        if(static_cast<CCString *>(dict_bomb->objectForKey("count"))->intValue() > 0)
                        {
                            unsigned char _type =  static_cast<CCString *>(dict_bomb->objectForKey("type"))->intValue();
                            
                          /*  while(CKFileInfo::Instance().isNeedUpgradeBomb(_type))
                            {
                                _type++;
                            }*/
                            if(first_bonus == 0)
                            {
                                first_bonus = _type;
                            }
                            _type += CKFileInfo::Instance().isNeedUpgradeBomb(_type);
                            
                        list_bomb_bonus.push_back(pair<unsigned char,unsigned char>(_type,static_cast<CCString *>(dict_bomb->objectForKey("count"))->intValue()));
                      //  CCLOG("loadBonus:: load type %d count %d",_type,static_cast<CCString *>(dict_bomb->objectForKey("count"))->intValue());
                        tmp_bonus_list.push_back(_type);
                        }
                    }
                };
                //add random bonus bomb
                CCDictionary *dict_cube = (CCDictionary *)dict_lvl->objectForKey("cube");
                int count_bonus = 0;
                
                if(dictInt(dict_lvl, "count_bomb") == -1)
                  count_bonus = (dictInt(dict_cube, "count_cube") + 5)/10;
                else
                  count_bonus = dictInt(dict_lvl, "count_bomb");
                
               // CCLOG("open bonus bomb(%d) bonus airplane(%d)",CKFileInfo::Instance().open_bonus.size(),CKFileInfo::Instance().open_bonus_airplane.size());
                
                // gen list all open bomb with max lvl
                std::vector<int>::iterator itt = CKFileInfo::Instance().open_bonus.begin();
                while(itt != CKFileInfo::Instance().open_bonus.end())
                {
                    int d = *itt;
                    it = list_bomb_bonus.begin();
                    while (it != list_bomb_bonus.end()) {
                        if(int((*it).first/10) == int(d/10))
                        {
                            if(d < (*it).first)
                                d = (*it).first;
                            break;
                        }
                        it++;
                    }
                    tmp_bonus_list.push_back(d);
                    itt++;
                };
                //add random bonus bomb into list
                if(tmp_bonus_list.size() > 0)
                    while (count_bonus > list_bomb_bonus.size())
                    {
                        unsigned char r = tmp_bonus_list[rand()%tmp_bonus_list.size()];
                        list_bomb_bonus.push_back(pair<unsigned char,unsigned char>(r,1));
                        CCLOG("Gen bonus bomb to map %d",int(r));
                    };
                tmp_bonus_list.clear();
                
                if(dict_lvl->objectForKey("airplane"))
                {
                    CCArray* array_bomb = (CCArray*)dict_lvl->objectForKey("airplane");
                    CCObject* Obj_bomb = NULL;
                    CCARRAY_FOREACH(array_bomb, Obj_bomb)
                    {
                        CCDictionary *dict_bomb = (CCDictionary*)Obj_bomb;
                        if(static_cast<CCString *>(dict_bomb->objectForKey("count"))->intValue() > 0)
                        list_airplane_bonus.push_back(pair<unsigned char,unsigned char>(static_cast<CCString *>(dict_bomb->objectForKey("type"))->intValue(),static_cast<CCString *>(dict_bomb->objectForKey("count"))->intValue()));
                        
                        if(first_bonus == 0)
                        {
                            first_bonus = static_cast<CCString *>(dict_bomb->objectForKey("type"))->intValue() + 1000;
                        };
                        //CCLOG("type %d count %d",static_cast<CCString *>(dict_bomb->objectForKey("type"))->intValue(),static_cast<CCString *>(dict_bomb->objectForKey("count"))->intValue());
                     //   CKFileInfo::Instance().addOpenBonus(static_cast<CCString *>(dict_bomb->objectForKey("type"))->intValue() + 1000);
                       
                        tmp_bonus_list.push_back(static_cast<CCString *>(dict_bomb->objectForKey("type"))->intValue() + 1000);
                    }
                };
                if(dictInt(dict_lvl, "count_airplane") == -1)
                    count_bonus = (dictInt(dict_cube, "count_cube") + 5)/10;
                else
                    count_bonus = dictInt(dict_lvl, "count_airplane");
                
               itt = CKFileInfo::Instance().open_bonus_airplane.begin();
               // if(CKFileInfo::Instance().open_bonus_airplane.size() > 0)
                    while(itt != CKFileInfo::Instance().open_bonus_airplane.end())
                    {
                        tmp_bonus_list.push_back(*itt);
                         itt++;
                    };
                
                //list_airplane_bonus.clear();
                if(tmp_bonus_list.size() > 0)
                    while (count_bonus > list_airplane_bonus.size())
                    {
                        int d = tmp_bonus_list[rand()%tmp_bonus_list.size()];
                        CCLOG("gen airplane bonus  %d",d);
                        list_airplane_bonus.push_back(pair<unsigned int,unsigned char>((d - 1000),1));
                    };
                tmp_bonus_list.clear();
                break;
            };
            lvl++;
        };
    };
    it = list_bomb_bonus.begin();
    unsigned char count = 0;
    while (it != list_bomb_bonus.end()) {
        count += it->second;
        it++;
    };
    CCLOG("count bomb bonus load %d",count);
    dict->release();
    return count;
};

list<CCNode*>* CKBonus::getOpenBonus()
{
    return &open_bonus;
};

void CKBonus::findOpenBonus()
{
    CCLOG(" CKBonus::findOpenBonus");
    bonus_it = bonus_list.begin();
    while(bonus_it != bonus_list.end())
    {
        CCNode * _sprite = (CCNode *)*bonus_it;
        int x = (_sprite->getPositionX() - BOX_SIZE/2 - panel_left)/BOX_SIZE;
        int y = (_sprite->getPositionY() - m_array->getGroundHeight())/BOX_SIZE;
        if(m_array->getBigCube(y,x) == 0)
        {
            CCLOG("CKBonus::_node->getTag() %d",_sprite->getTag());
            open_bonus.push_back(_sprite);
            bonus_it = bonus_list.erase(bonus_it);
        }
        else
        {
            bonus_it++;
        }
       
    };
};

/*CCNode* CKBonus::findActiveBonus(const CCPoint &_p,const CCSize &_size)
{
    //fixet version 11.7 new version bonus
    return NULL;
    bonus_it = bonus_list.begin();
    while(bonus_it != bonus_list.end())
    {
        CCNode * _sprite = *bonus_it;
        int x = (_sprite->getPositionX() - BOX_SIZE/2)/BOX_SIZE;
        int y = (_sprite->getPositionY() - m_array->getGroundHeight())/BOX_SIZE;
        if(m_array->getBigCube(y,x) == 1)
            if(abs(_p.x - _sprite->getPosition().x) < (_size.width/2 + _sprite->getContentSize().width/2) && abs(_p.y - _sprite->getPosition().y) < (_size.height/2 + _sprite->getContentSize().height/2))
            {
                bonus_list.erase(bonus_it);
                CCLOG("bonus find TRUE %d",bonus_list.size());
                return _sprite;
            }
        bonus_it++;
    };
    return NULL;
};*/