//
//  CKAirplane.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 20.09.12.
//
//

#include "CKAirplane.h"
#include "CK.h"
#include "CKFileInfo.h"
#include "CKBackNode.h"
#include "CKSprite.h"
#include "CKGameScene90.h"


#pragma mark - CKAirplane
CKAirplane::CKAirplane()
{
    CCLOG("CKAirplane::CKAirplane()");
    rtt_crash_pilot = NULL;
    animation_sprite = NULL;
    enable_particle = true;
    bomber_limit_x = FLT_MAX;
    m_emitter_splash = NULL;
    ps_smoke = NULL;
    ps_machine_gun = NULL;
    active_bonus = false;
    kamikadze_start_position = CCPointZero;
    shield = NULL;
    bomber = NULL;
    machine_gun_action = NULL;
    show_particle_machine_gun = false;
    ghost_rtt = NULL;
    scarf = NULL;
};

CKAirplane::~CKAirplane()
{
    CCLOG("~CKAirplane");
    CC_SAFE_DELETE(m_anim);
};

void CKAirplane::setLimitBomberX(float _limit)
{
    bomber_limit_x = _limit;
};

unsigned char CKAirplane::getLastBombID()const
{
    return last_bomb_id;
};

CCParticleSystem* CKAirplane::getParticleSmoke(int _num)
{
    if(_num == 0)
    {
        return ps_smoke;
    };
    return ps_smoke;
};

void CKAirplane::setStepUpBonus(float _step)
{
    step_up_bonus = _step;
};


void CKAirplane::setBombs(CKBombBase *_bomb)
{
    m_bomb = static_cast<CKBombs *>(_bomb);
};

void CKAirplane::setGhostBomb(CKBombs *_bomb)
{
    m_bomb_ghost = _bomb;
};


unsigned char CKAirplane::getBonusType(int _num) const
{
    CCLOG("bonus_type %d",bonus_type[_num].type);
    return bonus_type[_num].type;
};

float CKAirplane::getBonusProgress(int _num) const
{
    return bonus_type[_num].progress;
};

bool CKAirplane::haveActiveBonus() const
{
    return  active_bonus;
};

bool CKAirplane::canAutoShot()
{
    if(!haveActiveBonus())
    {
        return true;
    }

    return !isBonusRun(Bonus_Airplane_Gun);
};

bool CKAirplane::isBonusRun(int _type)
{
    for(int i = 0;i < COUNT_BONUS_SLOT;++i)
    {
        if(bonus_type[i].active)
        {
            if (bonus_type[i].type == _type) {
                return true;
            }
        }
    };
     return false;
};

void CKAirplane::setEnableParticle(bool _ne)
{
    enable_particle =_ne;
};

int CKAirplane::gameOverUpdete()
{
    m_emitter->setPosition(ccp(m_sprite->getPosition().x,m_sprite->getPosition().y));
    m_emitter->setAngle(m_sprite->getRotation());
    if (!m_sprite->getActionByTag(2)) {
      //  return rAirplane_Over;//Game over
        int value = rAirplane_Over;
        callFuncGame(m_sprite, &value);
    }
    return 0;
};
void CKAirplane::callFuncCrashMovePosition()
{
    m_sprite->setPosition(ccp(0,ground_air));
};

bool CKAirplane::createCrashAnimation()
{
    CKAudioMng::Instance().stopEffect("plane_fly");
    end_game = rAirplane_Over;
    
    if(machine_gun_blink)
        machine_gun_blink->stopAllActions();
    
    if(CKFileOptions::Instance().isPlayMusic())
    {
        CKAudioMng::Instance().fadeBgToVolume(0.0,0.5);
    };
    
    m_sprite->stopAllActions();
    if(ps_smoke)
    {
        ps_smoke->stopSystem();
        ps_smoke->setVisible(false);
    };
    
    ccBezierConfig bezier0;
    bezier0.controlPoint_1 = ccp(m_sprite->getPositionX() - BOX_SIZE*2,m_sprite->getPositionY() + BOX_SIZE*2); // control point 1
    bezier0.controlPoint_2 =ccp(win_size.width,m_sprite->getPositionY()); // control point 2
    bezier0.endPosition = ccp(win_size.width*1.05,m_sprite->getPositionY()*0.5 + ground_height);
    CCFiniteTimeAction *action0 = CCBezierTo::create(1.0, bezier0);
    CCFiniteTimeAction *rotate0 = CCRotateBy::create(1.0, 760);
    CCFiniteTimeAction *spaw0 = CCSpawn::create(action0,rotate0,NULL);
    
    //CCFiniteTimeAction *move0 = CCMoveTo::create(0.001, ccp(0,ground_air));
    

    CCAction *move = CCMoveTo::create(1.0, ccp(win_size.width*1.05,ground_air));;
    CCActionInterval *move_end = CCEaseSineOut::create((CCActionInterval *)move);

    float time_jump = 0.8;
    CCAction *seq_jump = CCSequence::create(CCJumpBy::create(time_jump, ccp(win_size.width*0.5,0), BOX_SIZE*0.6, 1),CCJumpBy::create(time_jump*0.8, ccp(win_size.width*0.3,0), BOX_SIZE*0.5, 1),CCJumpBy::create(time_jump*0.4, ccp(win_size.width*0.1,0), BOX_SIZE*0.4, 1),move_end, NULL);
    
    CCFiniteTimeAction *rotate2 = CCRotateBy::create(1.5, 830);
    
  //  CCPoint *p = new ccp(win_size.width*0.5,ground_air);
    //CCFiniteTimeAction *splash_call = CCSequence::create(CCDelayTime::create(0.6),CCCallFuncND::create(this, callfuncND_selector(CKAirplane::animationSplashEffect), p),NULL);
    CCFiniteTimeAction *spaw2 = CCSpawn::create(rotate2,seq_jump,NULL);
    
    CCFiniteTimeAction *seq = CCSequence::create(spaw0,CCCallFunc::create(this, callfunc_selector(CKAirplane::callFuncCrashMovePosition)) /*move0*/,spaw2,CCCallFuncND::create(this, callfuncND_selector(CKAirplane::callFuncGame), &end_game),NULL);
    seq->setTag(2);
    m_sprite->runAction(seq);
    
    animateScrew();
    animatePilot();
    
    CKAudioMng::Instance().playEffect("level_failed");
    m_sprite->stopActionByTag(9);
    m_anim->getChildByTag(10)->setVisible(true);
    
    
    CCAnimation* animat = CCAnimation::create();
    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(CK::loadImage("a_Boom.plist").c_str());
    for (int i = 0; i < 15; i++) {
        char str[255];
        sprintf(str, "boom%d.png",i+1);
        animat->addSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(str));
    };

    animat->setDelayPerUnit(0.04);
    CCSprite* sprite = CCSprite::createWithSpriteFrameName("boom1.png");
    m_layer->addChild(sprite,zOrder_Airplane);
    sprite->setPosition(ccp(m_sprite->getPositionX() + m_sprite->getContentSize().width/2,m_sprite->getPositionY()));
    CCFiniteTimeAction* action = CCAnimate::create((CCAnimation*)animat);
    sprite->runAction(action);
    crash_anim = true;
    pos_y = step*START_AIRPLANE_HEIGHT/2;
    
    return true;
};

void CKAirplane::animationSplashEffect(CCNode *_sender,void *_data)
{
    CCPoint *p = (CCPoint *)_data;
    if(p->x< 0)
    {
        m_emitter_splash->stopSystem();
    }
    else
    {
        m_emitter_splash->resumeSchedulerAndActions();
        m_emitter_splash->setVisible(true);
        m_emitter_splash->resetSystem();
        m_emitter_splash->setPosition(ccp(m_sprite->getPosition().x,m_sprite->getPosition().y - m_sprite->getContentSize().height));
    };
};

void CKAirplane::animatePilot()
{

    rtt_crash_pilot = CCRenderTexture::create(BOX_SIZE*2, BOX_SIZE*3,kCCTexture2DPixelFormat_RGBA8888, GL_DEPTH24_STENCIL8_OES);
    
    m_layer->addChild(rtt_crash_pilot,zOrder_Airplane);
    
    pilot_body  = m_anim->getChildByTag(6)->getNode();
    pilot_body->retain();
    pilot_body->removeFromParentAndCleanup(false);
    pilot_body->setPosition(m_sprite->convertToWorldSpace(pilot_body->getPosition()));
    
    CCSprite *pilot_hand_r = CCSprite::createWithSpriteFrameName("plane_pilot_hand.png");
    pilot_hand_r->setPosition(ccp(pilot_hand_r->getContentSize().width/2,pilot_hand_r->getContentSize().height*1.5));
    pilot_hand_r->setAnchorPoint(ccp(1.0,1.0));
    pilot_hand_r->setFlipX(true);
    pilot_body->addChild(pilot_hand_r,-1);
    
    CCSprite *pilot_leg_r = CCSprite::createWithSpriteFrameName("plane_pilot_leg.png");
    pilot_leg_r->setPosition(ccp(2 + pilot_leg_r->getContentSize().width*0.5,pilot_leg_r->getContentSize().height*0.5));
    pilot_leg_r->setAnchorPoint(ccp(1.0,1.0));
    pilot_leg_r->setFlipX(true);
    pilot_leg_r->setTag(34);
    CCAction * seq_r = CCSequence::create(CCRotateTo::create(0.5, -30),CCRotateTo::create(0.6, 0),NULL);
    pilot_leg_r->runAction( CCRepeat::create((CCActionInterval *)seq_r, 10.0));
    pilot_body->addChild(pilot_leg_r,-1);

    
    CCSprite *pilot_hand_l = CCSprite::createWithSpriteFrameName("plane_pilot_hand.png");
    pilot_hand_l->setPosition(ccp(pilot_body->getContentSize().width + pilot_hand_l->getContentSize().width/2,pilot_hand_l->getContentSize().height*1.5));
    pilot_hand_l->setAnchorPoint(ccp(1.0,1.0));
    pilot_body->addChild(pilot_hand_l,-1);
    
    CCSprite *pilot_leg_l = CCSprite::createWithSpriteFrameName("plane_pilot_leg.png");
    pilot_leg_l->setPosition(ccp(pilot_body->getContentSize().width - 2 - pilot_leg_l->getContentSize().width*0.5,pilot_leg_l->getContentSize().height*0.5));
    pilot_leg_l->setAnchorPoint(ccp(0.0,1.0));
    pilot_leg_l->setTag(31);
    
    CCAction * seq_l = CCSequence::create(CCRotateTo::create(0.6, 30),CCRotateTo::create(0.5, 0),NULL);
    pilot_leg_l->runAction( CCRepeat::create((CCActionInterval *)seq_l, 10.0));
    pilot_body->addChild(pilot_leg_l,-1);
    
    //reorder scarf
    scarf->retain();
    scarf->removeFromParentAndCleanup(false);
    scarf->setPosition(ccp(-pilot_body->getContentSize().width*0.1,pilot_body->getContentSize().height*0.5));
    pilot_body->addChild(scarf,-2);
    scarf->release();
    
    CCSprite* parachute = CCSprite::createWithSpriteFrameName("plane_slow.png");
    parachute->setRotation(90);
    parachute->setAnchorPoint(ccp(1.0,0.5));
    parachute->setTag(100);
    parachute->setPosition(ccp(pilot_body->getContentSize().width/2,pilot_body->getContentSize().height*0.75));
    pilot_body->addChild(parachute,-1);
    parachute->setScaleX(0.0);
    parachute->setScaleY(0.1);
    //parachute->retain();
    
    float time_scale = 1.0;
    float time_free_fly = 0.7;
    CCAction * easy_scale = CCEaseBounceOut::create(CCScaleTo::create(0.2*time_scale*time_free_fly, 1.0,1.0));
    CCAction *action_parachute = CCSequence::create(CCDelayTime::create(0.55*time_scale*time_free_fly),CCScaleTo::create(0.25*time_scale*time_free_fly, 1.0,0.2),easy_scale,NULL);
    parachute->runAction(action_parachute);
    
    CCPoint pos_sp = m_sprite->getPosition();
    if(pos_sp.y < BOX_SIZE*4)
        pos_sp.y = BOX_SIZE*4;
    
    ccBezierConfig bezier;
    bezier.controlPoint_1 = ccp(pos_sp.x + BOX_SIZE ,pos_sp.y + BOX_SIZE*3); // control point 1
    bezier.controlPoint_2 = ccp(pos_sp.x + BOX_SIZE*3,pos_sp.y + BOX_SIZE*1); // control point 2
    bezier.endPosition = ccp(pos_sp.x + BOX_SIZE*4,pos_sp.y - BOX_SIZE);
    
    float angle = 9;
    float time_rotate = 0.4;
    
    CCAction *rotate_first = CCEaseSineOut::create(CCRotateBy::create(time_rotate*time_scale, -angle));
    CCAction *rotate_2 = CCEaseSineInOut::create(CCRotateBy::create(time_rotate*2*time_scale, angle*2));
    CCAction *rotate_3 = CCEaseSineInOut::create(CCRotateBy::create(time_rotate*2*time_scale, -angle*2));
    CCAction *rotate_down = CCSequence::create((CCActionInterval*)rotate_first,rotate_2,rotate_3,rotate_2,rotate_3,rotate_2,rotate_3,NULL);
    CCActionInterval *move_down = CCMoveBy::create(4.0*time_scale, ccp(BOX_SIZE*3,- win_size.height*0.9));
    pilot_body->setRotation(45);
    CCAction *start_move = CCSpawn::create(CCBezierTo::create(time_free_fly*time_scale, bezier),CCRotateBy::create(time_free_fly*time_scale*0.8, 315),NULL);
    CCAction *move_complite = CCSequence::create((CCActionInterval *)start_move,CCSpawn::create(move_down,rotate_down,NULL),NULL);
    pilot_body->runAction(move_complite);
    pilot_body->setTag(2);
    anim_finish_move_pilot = 0;
    //insert body to node need for rtt
    pilot_node = CCNode::create();
    m_layer->addChild(pilot_node,zOrder_Airplane);
    pilot_node->addChild(pilot_body);
    pilot_body->release();
};



void CKAirplane::animateMoveToFinishPilot()
{
   
    CCNode* body  = pilot_body;
    body->stopAllActions();
    body->setRotation(0);
    body->setPositionY(body->getPositionY()+1);
    float curent_pos = body->getPositionX();
    anim_finish_move_pilot = 1;
    if(curent_pos > win_size.width)
    {
        curent_pos -= win_size.width;
        anim_finish_move_pilot = 2;
    }

    if(curent_pos > win_size.width*0.5)
    {
        body->runAction(CCSequence::create(CCDelayTime::create(0.6),CCScaleTo::create(0.01, -1.0,1.0),CCDelayTime::create(0.3),CCScaleTo::create(0.01, 1.0,1.0),CCCallFunc::create(this, callfunc_selector(CKAirplane::createAnimationLeg)), CCMoveBy::create(4.0, ccp(win_size.width,0)),NULL));
    }
    else
    {
        body->runAction(CCSequence::create(CCDelayTime::create(0.3),CCScaleTo::create(0.01, -1.0,1.0),CCDelayTime::create(0.3),CCScaleTo::create(0.01, 1.0,1.0),CCDelayTime::create(0.3),CCScaleTo::create(0.01, -1.0,1.0),CCCallFunc::create(this, callfunc_selector(CKAirplane::createAnimationLeg)), CCMoveBy::create(4.0, ccp(-win_size.width,0)),NULL));
    };
    
    CCNode *parachute = pilot_body->getChildByTag(100);
    if(parachute)
    {
        parachute->retain();
        parachute->removeFromParentAndCleanup(false);
        parachute->setPosition(pilot_body->convertToWorldSpace(parachute->getPosition()));
        m_layer->addChild(parachute,zOrder_Cube);
        if(parachute->getPositionX() > win_size.width)
        {
            parachute->setPositionX(parachute->getPositionX() - win_size.width);
        };
        parachute->release();
        float time_rotate = 0.8;
        float angle = 80;
        CCAction *rotate_first = CCEaseSineOut::create(CCRotateBy::create(time_rotate, -angle));
        CCAction *rotate_2 = CCEaseSineInOut::create(CCRotateBy::create(time_rotate*2, angle*2));
        CCAction *rotate_3 = CCEaseSineInOut::create(CCRotateBy::create(time_rotate*2, -angle*2));
        CCAction *rotate_down = CCSequence::create((CCActionInterval*)rotate_first,rotate_2,rotate_3,rotate_2,rotate_3,rotate_2,rotate_3,NULL);
        
        parachute->setPositionY(parachute->getPositionY() + parachute->getContentSize().height);
        parachute->setAnchorPoint(ccp(0.0,0.5));
        
        CCAction* action = CCSpawn::create(CCMoveBy::create(3.0, ccp(-BOX_SIZE*5,-BOX_SIZE*3)),rotate_down,CCScaleTo::create(1.0, 1.0,0.2),NULL);
        parachute->runAction(action);
    }
};

void CKAirplane::createAnimationLeg()
{
    CKAudioMng::Instance().playEffect("body_tipe_on_land");
    CCAction * rotate_leg_left = CCRepeatForever::create(CCRotateBy::create(0.1, 360));
    pilot_body->getChildByTag(31)->runAction(rotate_leg_left);
    
    CCAction * rotate_leg_right = CCRepeatForever::create(CCRotateBy::create(0.1, 360));
    pilot_body->getChildByTag(34)->runAction(rotate_leg_right);
}

void CKAirplane::animateScrew()
{
    //anim screw
    CCPoint pos_sp = m_sprite->getPosition();
    ccBezierConfig bezier2;
    bezier2.controlPoint_1 = ccp(pos_sp.x + BOX_SIZE ,pos_sp.y + BOX_SIZE*3); // control point 1
    bezier2.controlPoint_2 = ccp(pos_sp.x + BOX_SIZE*5,pos_sp.y + BOX_SIZE*3); // control point 2
    bezier2.endPosition = ccp(pos_sp.x + BOX_SIZE*10,-ground_air);
    float time_anim = 1.4;
    CCNode* screw  = m_anim->getChildByTag(11)->getNode();
    screw->retain();
    screw->removeFromParentAndCleanup(false);
    screw->setPosition(m_sprite->convertToWorldSpace(screw->getPosition()));
    m_layer->addChild(screw,zOrder_Airplane);
    screw->release();
    screw->runAction(CCSpawn::create(CCBezierTo::create(time_anim, bezier2),CCRotateBy::create(time_anim, 960),NULL));
    
    ccBezierConfig bezier3;
    bezier3.controlPoint_1 = ccp(pos_sp.x - BOX_SIZE ,pos_sp.y + BOX_SIZE); // control point 1
    bezier3.controlPoint_2 =ccp(pos_sp.x - BOX_SIZE*5,pos_sp.y + BOX_SIZE*2); // control point 2
    bezier3.endPosition = ccp(pos_sp.x - BOX_SIZE*6,-ground_air);
    m_nose->retain();

    m_nose->removeFromParentAndCleanup(false);
    m_nose->setPosition(m_sprite->convertToWorldSpace(m_nose->getPosition()));
    m_layer->addChild(m_nose,zOrder_Airplane);
    m_nose->release();
    m_nose->runAction(CCSpawn::create(CCBezierTo::create(time_anim, bezier3),CCRotateBy::create(time_anim, 960),NULL));
    
};

bool CKAirplane::createWinAnimation()
{
    CCLOG("CKAirplane::createWinAnimation");
    
    if(machine_gun_blink)
        machine_gun_blink->stopAllActions();
    
    
    if(m_sprite->getPositionX() < (win_size.width + m_sprite->getContentSize().width/2) && end_game == 0)
   {
       
       CKAudioMng::Instance().playEffect("level_complited");
       
       speed = 12.6*BOX_SIZE;//set maximum speed move to finish
       end_game = rAirplane_Win; //start win animation
       CCAction *sequ = CCSequence::create(CCMoveTo::create(1.0*((win_size.width*1.1 - m_sprite->getPositionX())/win_size.width), ccp(win_size.width + m_sprite->getContentSize().width,m_sprite->getPositionY())),CCCallFunc::create(this, callfunc_selector(CKAirplane::createWinAnimation)), NULL);
       m_sprite->runAction(sequ);
       return false;
   };
    

    if(ps_smoke)
    {
        ps_smoke->stopSystem();
        ps_smoke->setVisible(false);
    }
    
    {
        
        if(CKFileOptions::Instance().isPlayMusic())
        {
            CKAudioMng::Instance().fadeBgToVolume(0.0,0.5);
        };
        
        win_anim = true;
        //m_sprite->stopAllActions();
        
        target_sprite = CCNode::create();
        //target_sprite->setVisible(false);
        m_layer->addChild(target_sprite,zOrder_Airplane);
        
        target_sprite->setPosition(m_sprite->getPosition());
        m_sprite->retain();
        m_sprite->removeFromParentAndCleanup(false);
        float size_circle = BOX_SIZE*3;
        CCNode * node = m_sprite;
        node->setTag(2);
        node->setPosition(ccp(0,-size_circle));
        target_sprite->addChild(node);
        CC_SAFE_RELEASE_NULL(m_sprite);
        m_sprite = (CCSprite*)target_sprite;
        m_sprite->setPositionY(ground_air + size_circle+BOX_SIZE);
        m_sprite->setPositionX(0);
        CCAction *call_func_finish = CCCallFuncND::create(this, callfuncND_selector(CKAirplane::callFuncGame), &end_game);
        CCAction *call_func_wheel = CCCallFunc::create(this, callfunc_selector(CKAirplane::showWheel));
        
        float time_scale = 0.7;
        float time_start_move = 0.8*time_scale;
        float time_circle = time_start_move*3.0;
        float time_end_move = time_start_move*2.0;
        
        CCAction * move = CCMoveTo::create(time_start_move, ccp(win_size.width*0.4,ground_air + size_circle + BOX_SIZE));
        CCAction *move_down = CCMoveBy::create(time_circle, ccp(0,-BOX_SIZE));
        
        CCAction *move_end = CCMoveTo::create(time_end_move*1.1, ccp(win_size.width*0.9,ground_air + size_circle + BOX_SIZE*0.07));

        CCAction * easy = CCEaseSineOut::create((cocos2d::CCActionInterval *)move_end);
        CCAction *move_seq = CCSequence::create((cocos2d::CCActionInterval *)move,move_down,easy ,call_func_finish,NULL);
        CCAction *rotate_seq = CCSequence::create(CCRotateBy::create(time_circle*0.9, -360*0.9),call_func_wheel,CCRotateBy::create(time_circle*0.1, -360*0.1),NULL);
        
        CCAction *action = CCSpawn::create((cocos2d::CCActionInterval *)move_seq, CCSequence::create(CCDelayTime::create(time_start_move),rotate_seq,NULL),NULL);
        m_sprite->runAction(action);
    };
    return true;
};

void CKAirplane::showWheel()
{
    CKAudioMng::Instance().stopEffect("plane_fly");
    CKAudioMng::Instance().playEffect("end_level_plain_landing");
    float time_scale = 0.7;
    float time_jump = 0.5f*time_scale;
    float time_show_wheel = 0.5f*time_scale;
    CCNode * node = m_sprite->getChildByTag(2);
    CCAction *jump = CCSequence::create(CCJumpBy::create(time_jump, ccp(0,0), BOX_SIZE*0.15, 1),CCJumpBy::create(time_jump*0.8, ccp(0,0), BOX_SIZE*0.125, 1),CCJumpBy::create(time_jump*0.4, ccp(0,0), BOX_SIZE*0.1, 1),NULL);
    CCAction * easy = CCEaseSineOut::create((cocos2d::CCActionInterval *)jump);
    CCAction * seq = CCSequence::create(CCRotateTo::create(time_show_wheel*0.5, -15),CCDelayTime::create(time_jump*0.1),easy,NULL);
    node->runAction(seq);
    
    m_anim->getChildByTag(7)->setVisible(true);
    m_anim->getChildByTag(5)->setVisible(true);
    CCSprite * wheel_l = static_cast<CCSprite *>(m_anim->getChildByTag(7)->getNode());
    CCPoint pos = wheel_l->getPosition();
    wheel_l->stopAllActions();
    wheel_l->setPosition(ccp(wheel_l->getPositionX() + wheel_l->getContentSize().width,wheel_l->getPositionY() + wheel_l->getContentSize().height));
    wheel_l->runAction(CCMoveTo::create(time_show_wheel*0.8, pos));

    CCSprite * wheel_b = static_cast<CCSprite *>(m_anim->getChildByTag(5)->getNode());
    pos = wheel_b->getPosition();
    wheel_b->stopAllActions();
    wheel_b->setPosition(ccp(wheel_b->getPositionX() - wheel_b->getContentSize().width,wheel_b->getPositionY() + wheel_b->getContentSize().height));
    CCAction *act_mov_w = CCMoveTo::create(time_show_wheel*1.3, pos);
    CCAction* act_move_eb = CCEaseBounceOut::create((CCActionInterval *)act_mov_w->copy());
    wheel_b->runAction(act_move_eb);
};

int CKAirplane::winUpdate(const float &dt)
{

    return 0;
};

void CKAirplane::checkCrash()
{
    
};


void CKAirplane::pause()
{
    m_sprite->pauseSchedulerAndActions();
    if(m_emitter)
        m_emitter->pauseSchedulerAndActions();
    
    if(m_emitter_splash)
        m_emitter_splash->pauseSchedulerAndActions();
    if(ps_smoke)
        ps_smoke->pauseSchedulerAndActions();
    if(ps_machine_gun)
        ps_machine_gun->pauseSchedulerAndActions();
};

void CKAirplane::resume()
{
    m_sprite->resumeSchedulerAndActions();
    if(m_emitter)
        m_emitter->resumeSchedulerAndActions();
    if(m_emitter_splash)
        m_emitter_splash->resumeSchedulerAndActions();
    if(ps_smoke)
        ps_smoke->resumeSchedulerAndActions();
    if(ps_machine_gun)
        ps_machine_gun->resumeSchedulerAndActions();
};

void CKAirplane::reset()
{
    end_game = 0;
    speed = 100;
    count_diff = 8;
    
    if(ps_smoke)
        ps_smoke->resetSystem();
    m_sprite->removeFromParentAndCleanup(true);
    CC_SAFE_DELETE(m_anim);

    createAnim("");

    if(!is_ipad)
    {
        curent_height = step*START_AIRPLANE_HEIGHT - 16;
    }
    else
    {
        curent_height = step*START_AIRPLANE_HEIGHT + 50;
    };

    int n = 6 - CKHelper::Instance().lvl_speed;
    while (n > 0) {
        if(count_diff > 0)
        {
            curent_height += diff;
            curent_height -= step;
            count_diff--;
        }
        n--;
    };

    win_anim = false;
    crash_anim = false;
    float k = 1.00;//для підбору швидкості
    switch (CKHelper::Instance().lvl_speed ) {
        case 0:
            speed = (5.1 + CKHelper::Instance().lvl_number*0.1)*BOX_SIZE*k;
            break;
        case 1:
            speed = (6.7 + CKHelper::Instance().lvl_number*0.1)*BOX_SIZE*k;
            break;
        case 2:
            speed = (8.3 + CKHelper::Instance().lvl_number*0.1)*BOX_SIZE*k;
            break;
        case 3:
            speed = (5.1 + CKHelper::Instance().lvl_number*0.1)*BOX_SIZE*k;
            break;
        default:
            break;
    };
    
    m_sprite->stopAllActions();
    m_sprite->setRotation(0);

    pos_x = start_pos_x;// -m_sprite->getContentSize().width;
    m_sprite->setPosition(ccp(pos_x,curent_height));
};
#pragma mark UPDATE

void CKAirplane::updateBonusAirplane(const float &dt)
{
    for(int i = 0;i < COUNT_BONUS_SLOT;++i)
    {
        if(bonus_type[i].type != 0)
            switch (bonus_type[i].type) {
                case Bonus_Airplane_Up://up
                    bonus_type[i].start += step_up_bonus;//BOX_SIZE/80.0;
                    curent_height += step_up_bonus;//BOX_SIZE/80.0;
                    bonus_type[i].calcProgress();
                    if(bonus_type[i].start >= bonus_type[i].end)
                    {
                        stopBonus(bonus_type[i].type);
                    }
                    
                    //   CCLOG("curent_height %f",curent_height);
                    break;
                case Bonus_Airplane_Slow://slow
                    bonus_type[i].start += speed_step;
                    bonus_type[i].calcProgress();
                    
                    if(bonus_type[i].progress <= PERCENT_PLAY_END_BONUS_ATTENTION)
                    {
                        if(!CKAudioMng::Instance().isPlayEffect("bonus_end_attention"))
                            CKAudioMng::Instance().playEffect("bonus_end_attention");
                    }
                    
                    if(bonus_type[i].start >= bonus_type[i].end)
                        stopBonus(bonus_type[i].type);
                    
                    
                    if(end_pos_x < animation_sprite->getPositionX())
                    {
                        animation_sprite->setPosition(ccp(start_pos_x,m_sprite->getPositionY()));
                    };
                    animation_sprite->setPosition(ccp(animation_sprite->getPositionX() + speed_step,m_sprite->getPositionY()));

                    break;
                case Bonus_Airplane_Armor://shield
                    bonus_type[i].start += speed_step;
                    bonus_type[i].calcProgress();
                    if(bonus_type[i].progress <= PERCENT_PLAY_END_BONUS_ATTENTION)
                    {
                        if(!CKAudioMng::Instance().isPlayEffect("bonus_end_attention"))
                            CKAudioMng::Instance().playEffect("bonus_end_attention");
                    }
                    
                    if(bonus_type[i].start >= bonus_type[i].end)
                        stopBonus(bonus_type[i].type);
                    
                    shield->setPosition(ccp(m_sprite->getPositionX() + m_sprite->getContentSize().width/4,m_sprite->getPositionY()));
                    break;
                case Bonus_Airplane_Bomber://bomber
                    // CCLOG("type_7 %d %d",int((bomber->getPositionX() - bomber_posX)/BOX_SIZE),bomber_fire);
                {
                    float koef = (is_ipad)?1.4:1.4;
                    bomber->setPositionX(bomber->getPositionX() + speed_step*koef);
                    bonus_type[i].progress = (bonus_type[i].end - bomber->getPositionX())/bonus_type[i].end;
                }
                    if(bomber->getPositionX() >= bonus_type[i].end)
                    {
                        stopBonus(bonus_type[i].type);
                    }else if(int((bomber->getPositionX() - bomber_posX)/BOX_SIZE) >= bomber_fire && bomber->getPositionX() < bomber_limit_x)//create bomber bomb
                    {
                        bomber_posX = bomber->getPositionX();
                        bomber_fire = rand()%2 + 3;
                        m_bomb->setIsAirplaneBomb(false);
                        m_bomb->create(ccp(bomber->getPosition().x,bomber->getPosition().y - bomber->getContentSize().height/3),22);//pirate bomb
                    };
                    break;
                case Bonus_Airplane_Phantom://ghost
                    bonus_type[i].calcProgress();
                    
                    bonus_type[i].start += speed_step;
                    
                    if(bonus_type[i].progress <= PERCENT_PLAY_END_BONUS_ATTENTION)
                    {
                        if(!CKAudioMng::Instance().isPlayEffect("bonus_end_attention"))
                            CKAudioMng::Instance().playEffect("bonus_end_attention");
                    }
                    
                    if(bonus_type[i].start >= bonus_type[i].end)
                        stopBonus(bonus_type[i].type);
                    else
                        if(bonus_type[i].progress < 0.1)
                        {
                            ghost_rtt->getSprite()->setOpacity(bonus_type[i].progress*1800);
                        };
                {
                    CCPoint prev_pos = m_sprite->getPosition();
                    m_sprite->setPosition(ccp(m_sprite->getContentSize().width*0.5,m_sprite->getContentSize().height*0.5));
                    ghost_rtt->clear(0, 0.0, 0, 0.0);
                    ghost_rtt->begin();
                    m_sprite->visit();
                    ghost_rtt->end();
                    m_sprite->setPosition(prev_pos);
                    
                    ghost_rtt->setPosition(ccp(ghost_rtt->getPositionX() + speed_step,ghost_rtt->getPositionY()));
                    if(ghost_rtt->getPositionX() >= end_pos_x)
                    {
                        ghost_rtt->setPosition(ccp(start_pos_x,prev_pos.y));
                    };
                }

                    break;
                default:
                    bonus_type[i].start += speed_step;
                    bonus_type[i].calcProgress();
                    
                    if(bonus_type[i].type == Bonus_Airplane_Speed || bonus_type[i].type == Bonus_Airplane_Gun)
                    {
                        if(bonus_type[i].progress <= PERCENT_PLAY_END_BONUS_ATTENTION)
                        {
                            if(!CKAudioMng::Instance().isPlayEffect("bonus_end_attention"))
                                CKAudioMng::Instance().playEffect("bonus_end_attention");
                        }
                    }
                    
                    if(bonus_type[i].start >= bonus_type[i].end)
                        stopBonus(bonus_type[i].type);
                    break;
            };
    }
};
void CKAirplane::updatePositionParticle(void)
{
   // if(show_particle_smoke && ps_smoke)
   //     ps_smoke->setPosition(m_sprite->getPosition());
    
    if(show_particle_machine_gun && ps_machine_gun)
    {
        CCPoint pos_gun = ccp(m_sprite->getPositionX(),m_sprite->getPositionY()-BOX_SIZE*0.4);// m_sprite->convertToWorldSpace(machine_gun_blink->getPosition());
        ps_machine_gun->setPosition(pos_gun);
    }
    if(m_emitter_splash)
        if(m_emitter_splash->isActive())
        {
            m_emitter_splash->setPosition(ccp(m_sprite->getPosition().x + m_sprite->getContentSize().width/3,m_sprite->getPosition().y - m_sprite->getContentSize().height/4));
        }
};

void CKAirplane::updateAnimationMask()
{
    if(scarf)
    {
        scarf->move(0.05, 0.0);
        //m_nose->moveMask(0.05, 0.0);
        
    };
    if(rtt_crash_pilot)
    {

        pilot_node->setPosition(ccpSub(ccp(BOX_SIZE,BOX_SIZE*1.5), pilot_body->getPosition()));
        if(anim_finish_move_pilot == 2)
        {
            pilot_node->setVisible(false);
        };
        
        if(anim_finish_move_pilot != 1 && pilot_body->getPositionX() > win_size.width)
        {
            bool b = pilot_node->isVisible();
            rtt_crash_pilot->clear(0, 0.0, 0, 0.0);
            rtt_crash_pilot->begin();
            pilot_node->setVisible(true);
            pilot_node->visit();
            pilot_node->setVisible(b);
            rtt_crash_pilot->end();
            
            
            rtt_crash_pilot->setPosition(pilot_body->getPosition());
            rtt_crash_pilot->setVisible(true);
            rtt_crash_pilot->setPositionX(rtt_crash_pilot->getPositionX() - win_size.width);
        }
        else
        {
            rtt_crash_pilot->setVisible(false);
        };

        pilot_node->setPosition(ccp(0,0));
        if(pilot_body->getPositionY() < (ground_height + pilot_body->getContentSize().height*0.6))
        {
            animateMoveToFinishPilot();
        };
    }
};

void CKAirplane::updateDoubleSpeed()
{
    if(!is_double_speed)
        return;
    
    if(!isBonusRun(Bonus_Airplane_Slow))
    {
        calcDoubleSpeed();
    };
}

const unsigned char CKAirplane::update(const float &dt)
{
    calcSpeedStep(dt);
    
    updateAnimationMask();
    updatePositionParticle();
    
    if(end_game != 0)
    {
        if(crash_anim && m_emitter_splash)
        {
            if(m_sprite->getPositionY() <= (ground_air+BOX_SIZE*0.15))
            {
                if(m_sprite->getPositionX() > (win_size.width + m_sprite->getContentSize().width/2))
                {
                    m_emitter_splash->stopSystem();
                    return 0;
                }
                if(!m_emitter_splash->isActive())
                    m_emitter_splash->resetSystem();
                
                m_emitter_splash->setPosition(ccp(m_sprite->getPositionX() - m_sprite->getContentSize().width*0.45,ground_height));
            };
        }
        return 0;
    };
    
    updateWaitBomb();
    updatePosition(dt);
    updateDoubleSpeed();

    updateBonusAirplane(dt);
    
    checkCollision();

    return 0;
};

void CKAirplane::checkCollision()
{
    if(!m_array)
        return;
    
    int collision = m_array->checkAirplane(ccp(m_sprite->getPositionX() + m_sprite->getContentSize().width/2,m_sprite->getPositionY() -  m_sprite->getContentSize().height/4),ccp(m_sprite->getPositionX(),m_sprite->getPositionY() -  m_sprite->getContentSize().height/2));
    
    if(collision == rAirplane_Over)  //that collision with cube, game over
    {
        for (int i = 0;  i < COUNT_BONUS_SLOT; ++i) {
            if(bonus_type[i].active)
                if(bonus_type[i].b_shield > 0)
                {
                    CKAudioMng::Instance().playEffect("shield_strike");
                    int x1 = (shield->getPositionX() - panel_left)/(BOX_SIZE/2);
                    int y1 = (shield->getPositionY() - ground_height - shield->getContentSize().height/4)/(BOX_SIZE/2);
                    if(x1 < COUNT_COLUMNS*2 && y1 < COUNT_ROWS*2)
                    {
                        // if(!m_array->destroyMiniCub(x1, y1) && !m_array->destroyMiniCub(x1, y1 - 1))
                        {
                            m_array->destroyMiniCub(x1, y1);
                            m_array->destroyMiniCub(x1, y1 - 1);
                        }
                    }
                    
                    bonus_type[i].b_shield--;
                    if(bonus_type[i].b_shield == 0)
                    {
                        stopBonus(bonus_type[i].type);
                    };
                    return;
                }
        };
        stopAllBonus();
        
        createCrashAnimation();
    }
    else
        if(collision == rAirplane_Win && !is_emulation_fly)  //not cube on level, you win
        {
            end_game = 0;
            stopAllBonus();
            createWinAnimation();
            m_array = NULL;
        }
        else
            if (collision == rAirplane_Splash && m_emitter_splash) {
                CKAudioMng::Instance().playEffect("plane_slice");
                m_emitter_splash->resetSystem();
                m_emitter_splash->setPosition(ccp(m_sprite->getPosition().x + m_sprite->getContentSize().width/3,m_sprite->getPosition().y - m_sprite->getContentSize().height/4));
            }
};

void CKAirplane::stopAllBonus()
{
    for(int i = 0; i < COUNT_BONUS_SLOT;i++)
    {
        if(bonus_type[i].active == true)
        {
            stopBonus(bonus_type[i].type);
        };
    };
};

void CKAirplane::stopBonus(unsigned char _id)
{
    for(int i = 0; i < COUNT_BONUS_SLOT;i++)
    {
        if(bonus_type[i].type == _id)
        {
            CKAudioMng::Instance().stopEffect("bonus_end_attention");
            if(bonus_type[i].type == Bonus_Airplane_Slow || bonus_type[i].type == Bonus_Airplane_Armor)
            {
                CKAudioMng::Instance().playEffect("throw_off");
            }
            else
            {
                if(bonus_type[i].type == Bonus_Airplane_Gun)
                {
                    CKAudioMng::Instance().playEffect("machine_gun_deactivation");
                }
                else
                {
                    if(bonus_type[i].type == Bonus_Airplane_Phantom)
                    {
                        CKAudioMng::Instance().playEffect("phantom");
                    }
                    else if(bonus_type[i].type != Bonus_Airplane_Bomber)
                    {
                        CKAudioMng::Instance().playEffect("bonus_deactivation");
                    }
                    else
                    {
                        CKAudioMng::Instance().stopEffect("bomber_flight");
                    }
                }
            }
            
            bonus_type[i].progress = -1.0;
            bonus_type[i].active = false;
            bonus_type[i].b_shield = 0;
            bonus_type[i].type = 0;
 
        };
    };
    active_bonus = (bonus_type[0].type + bonus_type[1].type);
    switch (_id) {
        case Bonus_Airplane_Up:
            break;
        case Bonus_Airplane_Slow://parashut
        {
            if(!animation_sprite)
                break;
            speed *= 2.0;
            CCFiniteTimeAction* move = CCMoveTo::create(5.0, ccp(animation_sprite->getPositionX() + win_size.width*1.2,MAX(0,animation_sprite->getPositionY()-win_size.height/2)));
            
            CCFiniteTimeAction* rotate = CCRotateTo::create(2.4, 110);
            
            CCActionInterval* rotate_ease = CCEaseSineInOut::create((CCActionInterval*)(rotate->copy()->autorelease()) );
            
            CCActionInterval* rotate2 = CCRotateTo::create(1.2, 70);
            CCActionInterval* rotate_ease2 = CCEaseSineInOut::create((CCActionInterval*)(rotate2->copy()->autorelease()) );
            
            CCActionInterval* rotate3 = CCRotateTo::create(1.2, 110);
            CCActionInterval* rotate_ease3 = CCEaseSineInOut::create((CCActionInterval*)(rotate3->copy()->autorelease()) );
            
            //CCActionInterval* rotate_ease2_back = rotate_ease2->reverse();
            CCFiniteTimeAction* seq = CCSequence::create(rotate_ease2,rotate_ease3,rotate_ease2,rotate_ease3, NULL);
            
            CCFiniteTimeAction* seq2 = CCSequence::create(rotate_ease,seq,NULL);
            CCFiniteTimeAction* sp = CCSpawn::create(seq2,move,NULL);
            
            animation_sprite->runAction(sp);
        }
            break;
        case Bonus_Airplane_Armor://shield
        {
            CCFiniteTimeAction* move = CCMoveTo::create(speed/(BOX_SIZE*3), ccp(shield->getPositionX() + win_size.width*0.2,-win_size.height*0.2));
            CCFiniteTimeAction* rotate = CCRotateTo::create(0.6, 90);
            CCFiniteTimeAction* sp = CCSpawn::create(rotate,move,NULL);
            shield->runAction(sp);
        }
            break;
        case Bonus_Airplane_Speed://speed x2
            m_bomb->setSpeed(m_bomb->getSpeed()*0.333333);
            break;
        case 6:
            break;
        case Bonus_Airplane_Phantom://ghost
        {
            if(ghost_rtt)
            {
                ghost_rtt->getSprite()->setOpacity(0);
                ghost_rtt->setVisible(false);
            }
        }
            break;
        default:
            break;
    }
    
    //    bonus_type = 0;
}
void CKAirplane::stopAndHideAllBonus()
{
    for(int i = 0; i < COUNT_BONUS_SLOT;i++)
    {
        bonus_type[i].progress = -1.0;
        bonus_type[i].active = false;
        bonus_type[i].b_shield = 0;
        bonus_type[i].type = 0;
    };
    if(ghost_rtt)
    {
        ghost_rtt->getSprite()->setOpacity(0);
        ghost_rtt->setVisible(false);
        m_bomb_ghost->deleteAllActiveBombs();
    };
    
    if(animation_sprite)
        animation_sprite->setVisible(false);
    if(shield)
        shield->setVisible(false);
    if(bomber)
    {
        bomber->stopAllActions();
        bomber->setVisible(false);
    }
};

int CKAirplane::runBonus(unsigned char _id)
{
    CCLOG("CKAirplane::runBonus %d",_id);
    int bonus_run_number = -1;
    run_bonus * run_bonus = NULL;
    for(int i = 0;i < COUNT_BONUS_SLOT;++i)
    {
        if(!bonus_type[i].active)
        {
            bonus_type[i].active = true;
            bonus_type[i].type = _id;
            bonus_type[i].progress = -1.0;
            bonus_run_number = i;
            run_bonus = &bonus_type[i];
            break;
        }
    };
    if(!run_bonus)
    {
        CCLog("Not free bonus type");
        return bonus_run_number;
    };
    
    active_bonus = true;
    switch (_id) {
        case Bonus_Airplane_Up://up
        {
            CKAudioMng::Instance().playEffect("airplane_up");
            run_bonus->start = 0;
            run_bonus->end = 0;
            setStepUpBonus(BOX_SIZE/80.0);
            if (int((curent_height + BOX_SIZE)/step) <= SHOW_BANNER_AFTER_HEIGHT)
                run_bonus->end = BOX_SIZE;
        }
            break;
        case Bonus_Airplane_Slow:
        {
            CKAudioMng::Instance().playEffect("parachute");
            speed *= 0.5;
            run_bonus->start = 0;
            run_bonus->end = win_size.width*COUNT_SLOW_LOOP + m_sprite->getContentSize().width*2;
            if(animation_sprite == NULL || animation_sprite->getTag() != 2)
            {
                animation_sprite = CCSprite::createWithSpriteFrameName("plane_slow.png");
                m_layer->addChild(animation_sprite,CK::zOrder_Airplane);
            };
            animation_sprite->setVisible(true);
            animation_sprite->stopAllActions();
            animation_sprite->setAnchorPoint(ccp(1.0,0.5));
            animation_sprite->setPosition(ccp(m_sprite->getPositionX() /*- animation_sprite->getContentSize().width*/ - m_sprite->getContentSize().width/2,m_sprite->getPositionY()));
            animation_sprite->setScale(0.0);
            animation_sprite->setRotation(0);
            animation_sprite->setTag(2);
            CCFiniteTimeAction * scale_y = CCScaleTo::create(0.3, 1.0,1.0);
            animation_sprite->runAction(scale_y);
        }
            break;
        case Bonus_Airplane_Armor:
        {
            CKAudioMng::Instance().playEffect("bonus_activation");
            run_bonus->b_shield = COUNT_SHIELD_DESTROY_CUBE;
            run_bonus->start = 0;
            run_bonus->end = win_size.width*COUNT_SHIELD_LOOP + m_sprite->getContentSize().width;
            if(shield == NULL || shield->getTag() != 3)
            {
                shield = CCSprite::createWithSpriteFrameName("plane_shield.png");//CCSprite::create(loadImage("plane_shield.png").c_str());
                m_layer->addChild(shield,CK::zOrder_Airplane);
            };
            shield->stopAllActions();
            shield->setScale(0.0);
            CCAction *scale = CCScaleTo::create(0.3, 1.0);
            shield->setAnchorPoint(ccp(0.0,0.5));
            shield->setVisible(true);
            shield->runAction(scale);
            shield->setRotation(0);
            shield->setTag(3);
        }
            break;
        case Bonus_Airplane_Speed://bomb speedx2
            CKAudioMng::Instance().playEffect("bonus_activation");
            run_bonus->start = 0;
            run_bonus->end = win_size.width*COUNT_BOMBX2_LOOP + m_sprite->getContentSize().width;
            m_bomb->setSpeed(m_bomb->getSpeed()*3.0);
            break;
        case Bonus_Airplane_Kamikadze://kamikadze
        {
            CKAudioMng::Instance().playEffect("kamikaze_flight");
            m_bomb->setIsAirplaneBomb(false);
            
            m_bomb->create((kamikadze_start_position.x == 0)?(ccp(win_size.width,step*(START_AIRPLANE_HEIGHT - 4) + ground_height)):kamikadze_start_position,68);
            run_bonus->active = false;
            run_bonus->type = 0;
        }
            break;
        case Bonus_Airplane_Gun://kylimet
            CKAudioMng::Instance().playEffect("machine_gun_activation");
            run_bonus->start = 0;
            run_bonus->end = win_size.width*COUNT_GUN_LOOP + m_sprite->getContentSize().width;
            break;
        case Bonus_Airplane_Bomber://bomber
        {
            CKAudioMng::Instance().playEffect("bomber_flight");
            if(!bomber)
            {
                bomber = CCSprite::createWithSpriteFrameName("plane_bomber_sprite.png");//CCSprite::create(loadImage("plane_bomber_sprite.png").c_str());
                m_layer->addChild(bomber,CK::zOrder_Airplane);
            };
            bomber->setVisible(true);
            
            float posX  = curent_height + BOX_SIZE;
            
            if(curent_height > step*(START_AIRPLANE_HEIGHT - 12))
                posX += BOX_SIZE;
            
            bomber->setPosition(ccp(-bomber->getContentSize().width/2,posX));
            bomber_posX = bomber->getPositionX();
            //CCAction* move = CCMoveTo::create(speed/((is_ipad)?60:25), ccp(win_size.width + bomber->getContentSize().width/2,posX));
            run_bonus->start = bomber->getPositionX();
            run_bonus->end = win_size.width + bomber->getContentSize().width/2;
            //bomber->runAction(move);
            bomber_fire = rand()%2 + 3;
        };
            break;
        case Bonus_Airplane_Phantom://ghost
        {
            CKAudioMng::Instance().playEffect("phantom");
            run_bonus->start = 0;
            run_bonus->end = win_size.width*COUNT_PHANTOM_LOOP + m_sprite->getContentSize().width;
            if(ghost_rtt == NULL || ghost_rtt->getTag() != 8)
            {

                ghost_rtt = CCRenderTexture::create(m_sprite->getContentSize().width*1.2, m_sprite->getContentSize().height*1.2, kCCTexture2DPixelFormat_RGBA8888, GL_DEPTH24_STENCIL8_OES);
                
                m_bomb_ghost->setAirplane(this);
                m_layer->addChild(ghost_rtt,CK::zOrder_Airplane - 1);
                ghost_rtt->getSprite()->getTexture()->setAntiAliasTexParameters();
            };

            ghost_rtt->setVisible(true);
            ghost_rtt->getSprite()->setOpacity(180);
            ghost_rtt->getSprite()->setColor((ccColor3B){128,128,240});
            
            ghost_rtt->setPosition(ccp(m_sprite->getPositionX(),m_sprite->getPositionY()));
            CCAction * move = CCMoveBy::create(0.5, ccp(-ghost_rtt->getContentSize().width/2,0));
            move->setTag(2);
            ghost_rtt->runAction(move);
            
            ghost_rtt->setTag(8);
        }
            break;
        default:
            break;
    }
    return bonus_run_number;
};

void CKAirplane::setKamikadzeStartPosition(const CCPoint &_pos)
{
    kamikadze_start_position = _pos;
};

bool CKAirplane::createBomb(unsigned char _type)
{
    float offst = 0;
    CCLOG("touch::_type %d",int(_type/10));
    switch (int(_type/10)) {
        case 0:
        case 5:
        case 8:
        case 11:
        case 13:
        case 14:
            offst = BOX_SIZE/4;
            break;
        default:
            break;
    }
    float newX =  int(m_sprite->getPosition().x/(BOX_SIZE*0.5)+0.5)*BOX_SIZE/2 + offst;
    return m_bomb->create(ccp(newX,m_sprite->getPosition().y - m_sprite->getContentSize().height/3),_type);
};

bool CKAirplane::touch(CCPoint _pos,unsigned char _type)
{
    bool active_bonus = false;
    last_bomb_id = _type;
    for(int i = 0;i < COUNT_BONUS_SLOT;i++)
    {
        if(bonus_type[i].type == Bonus_Airplane_Gun)
        {
            m_bomb->setIsAirplaneBomb(false);
            m_bomb->create(ccp(m_sprite->getPosition().x,m_sprite->getPosition().y - m_sprite->getContentSize().height/4),67);
            last_bomb_id = 67;
            active_bonus = true;
            
        };
        if(bonus_type[i].type == Bonus_Airplane_Phantom)
        {
            active_bonus = true;
            bool p = m_bomb->isNewBomb();
            if(p)
            {
                m_bomb->create(ccp(m_sprite->getPosition().x,m_sprite->getPosition().y - m_sprite->getContentSize().height/4),_type);
                if(ghost_rtt->getActionByTag(2) == NULL)
                {
                    m_bomb_ghost->setAirplane(this);
                    m_bomb_ghost->create(ccp(ghost_rtt->getPosition().x,ghost_rtt->getPosition().y - ghost_rtt->getContentSize().height/3),_type);
                }
                return true;
            }
        };
    };
    
    if(!active_bonus)
    {
        if(m_bomb->isNewBomb())//not active bomb
        {
            if(!end_game && (m_sprite->getPosition().x > 0) && (m_sprite->getPosition().x < (win_size.width - m_sprite->getContentSize().width/4)))
            {
                wait_new_loop = false;
                //CCLOG("m_array->isTopCube() %d",m_array->isTopCube(_pos));
                if(CKFileOptions::Instance().isKidsModeEnabled()  && m_array->isTopCube(_pos) && (int(_type/10) != 11))
                {
                    wait_create_bomb = true;
                    wait_position_x = _pos.x;
                    wait_type = _type;
                    CKAudioMng::Instance().playEffect("goal_detect");
                    if((m_sprite->getPosition().x - m_sprite->getContentSize().width/4) > wait_position_x)
                        wait_new_loop = true;
                    setDoubleSpeed(true);
                }
                else
                {
                    if(!wait_create_bomb)
                        createBomb(_type);
                }
                return true;
            };
        };
    };
    
    return false;
};

void CKAirplane::startMachineGun()
{
    if(ps_machine_gun)
    {
        if(!machine_gun_action)
        {
            CCActionInterval * action = CCBlink::create(0.4, 5);
            CCActionInterval *rotate1 = CCRotateTo::create(0.2, 5);
            CCActionInterval *rotate2 = CCRotateTo::create(0.2, -5);
            CCAction *seque = CCSequence::create(rotate1,rotate2,NULL);
            CCFiniteTimeAction *spaw = CCSpawn::create(action,seque,NULL);
            CCAction *machine_gun_action = CCRepeatForever::create((CCActionInterval *)spaw);
            machine_gun_blink->runAction(machine_gun_action);
        }
        
        machine_gun_blink->setVisible(true);
        show_particle_machine_gun = true;
        ps_machine_gun->resetSystem();
        machine_gun_blink->resumeSchedulerAndActions();
        
        m_anim->getChildByTag(14)->getNode()->setScale(1.0);
        m_anim->getChildByTag(15)->getNode()->setScale(1.0);
        
        m_anim->getChildByTag(14)->getNode()->runAction(CCSequence::create(CCScaleTo::create(0.02, 1.2),CCScaleTo::create(0.02, 0.8),CCScaleTo::create(0.02, 1.0),NULL));
        m_anim->getChildByTag(15)->getNode()->runAction(CCSequence::create(CCScaleTo::create(0.02, 1.2),CCScaleTo::create(0.02, 0.8),CCScaleTo::create(0.02, 1.0),NULL));
        
    };
};

void CKAirplane::stopMachineGun()
{
    show_particle_machine_gun = false;
    if(ps_machine_gun)
    {
        ps_machine_gun->stopSystem();
        machine_gun_blink->pauseSchedulerAndActions();
        machine_gun_blink->setVisible(false);
    };
};

void CKAirplane::setEnableMachineGun(bool _enable)
{
    m_anim->getChildByTag(14)->setVisible(_enable);
    m_anim->getChildByTag(15)->setVisible(_enable);
};

void CKAirplane::createNose()
{
    CKObject *m_node_noise = m_anim->getChildByTag(12);
    
    CCSpriteFrame * frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(m_node_noise->getName().c_str());
    m_nose = TBSpriteMask::createWithTexture(frame->getTexture(),frame->getRect(),frame->isRotated());
    m_nose->setPosition(m_node_noise->getPos());
    m_anim->get()->addChild(m_nose,13);
    
    CCTexture2D *texture = CCTextureCache::sharedTextureCache()->addImage(CK::loadImage("a_Plane2_nose_mask.png").c_str());
    m_nose->buildMaskWithTexture(texture,CCRect(0, 0, texture->getContentSize().width, texture->getContentSize().height),false,2);
    m_node_noise->getNode()->removeFromParentAndCleanup(false);
    
};

void CKAirplane::createScarf()
{
    CCNode * m_node_scarf = m_anim->getChildByTag(4)->getNode();

    scarf = TBSpriteMask::create(CK::loadImage("a_Plane2_scarf.png").c_str());
    ccTexParams params = {GL_LINEAR,GL_LINEAR,GL_REPEAT,GL_REPEAT};
    scarf->getTexture()->setTexParameters(&params);
    
    scarf->setPosition(m_node_scarf->getPosition());
    m_anim->get()->addChild(scarf,4);
    scarf->setRotation(m_node_scarf->getRotation());
    
    CCSpriteFrame *frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("plane_scarf_mask.png");
    scarf->setAnchorPoint(m_node_scarf->getAnchorPoint());
    scarf->buildMaskWithTexture(frame->getTexture(),frame->getRect(),frame->isRotated());
    
    m_node_scarf->removeFromParentAndCleanup(false);

};

void CKAirplane::createScarfTop()
{
    CKObject * m_obj = m_anim->getChildByTag(6)->getChildByTag(8);
    
    CCSpriteFrame * frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(m_obj->getName().c_str());
    TBSpriteMask * m_top_sc = TBSpriteMask::createWithTexture(frame->getTexture(),frame->getRect(),frame->isRotated());
    m_top_sc->setPosition(m_obj->getPos());
    m_top_sc->setAnchorPoint(m_obj->getNode()->getAnchorPoint());
    m_anim->getChildByTag(6)->getNode()->addChild(m_top_sc,4);
    
    CCSpriteFrame * frame2 = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(m_anim->getChildByTag(6)->getChildByTag(2)->getName().c_str());
        
    m_top_sc->buildMaskWithTexture(frame2->getTexture(),frame2->getRect(),frame2->isRotated());
    
    m_anim->getChildByTag(6)->getChildByTag(8)->getNode()->removeFromParentAndCleanup(true);
    m_anim->getChildByTag(6)->getChildByTag(2)->getNode()->removeFromParentAndCleanup(true);
};

CCSprite* CKAirplane::createAnim(const char* _name)
{
    
    for(int i = 0;i < COUNT_BONUS_SLOT;++i)
    {
        bonus_type[i].type = 0;
        bonus_type[i].active = false;
        bonus_type[i].progress = -1.0;
        bonus_type[i].b_shield = 0;
    };
    
    m_sprite = (CCSprite *)CCNode::create();//CCSprite::createWithSpriteFrameName(_name);
    
    m_anim = new CKBaseNode;
    m_anim->create(m_sprite);
    m_anim->load(CK::loadFile("c_PlaneFly.plist").c_str());
    m_anim->getChildByTag(7)->setVisible(false);//wheel
    m_anim->getChildByTag(10)->setVisible(false);//destroy mask
    m_anim->getChildByTag(5)->setVisible(false);//wheel big
    m_anim->getChildByTag(11)->getNode()->runAction(CCRepeatForever::create(CCRotateBy::create(1.2, 360)));
    setEnableMachineGun(false);
    
    createNose();
    createScarf();
    createScarfTop();
    
    m_layer->addChild(m_sprite,CK::zOrder_Airplane);
    
    m_sprite->setContentSize(m_anim->getChildByTag(9)->getNode()->getContentSize());
    setStartPositionX(-m_sprite->getContentSize().width/2);
    setEndPositionX(win_size.width + m_sprite->getContentSize().width*0.8);
    pos_x = start_pos_x;
    m_sprite->setPosition(ccp(pos_x,curent_height));
    createSmoke();
    createMachineGunBlink();
    return m_sprite;
};

void CKAirplane::createSmoke()
{
    if(enable_particle)
    {
        if(!m_emitter_splash)
        {
            m_emitter_splash = CCParticleSystemQuad::create(CK::loadFile("splash.plist").c_str());
           
           // m_emitter_splash->setPosition(ccp(BOX_SIZE*8,BOX_SIZE*8));
           // m_emitter_splash->retain();
            m_emitter_splash->setTexture(CCTextureCache::sharedTextureCache()->addImage(CK::loadFile("splash_plane.png").c_str()));
            m_layer->addChild(m_emitter_splash ,zOrderParticle);
             m_emitter_splash->stopSystem();
           // m_emitter_splash->release();
            m_emitter_splash->setPositionType(kCCPositionTypeFree);
        };
        
       // show_particle_smoke = true;
        if(!ps_smoke && !ObjCCalls::isLowCPU())
        {
            ps_smoke = CCParticleSystemQuad::create(CK::loadFile("airplane_smoke.plist").c_str());
             ps_smoke->setTexture(CCTextureCache::sharedTextureCache()->addImage(CK::loadFile("splash_plane.png").c_str()));
            ps_smoke->setPosition(ccp(- m_sprite->getContentSize().width*0.5,0));
          //  ps_smoke->retain();
            ps_smoke->setPositionType(kCCPositionTypeRelative);
            if(SHOW_PARTICLE)
            {
                m_sprite->addChild(ps_smoke ,zOrderParticle);
            }
          //  ps_smoke->release();
        }
    };
};

void CKAirplane::createMachineGunBlink()
{
    if(!ps_machine_gun)
    {
        machine_gun_blink = CCSprite::create(CK::loadImage("machine_gun_blink.png").c_str());
        m_sprite->addChild(machine_gun_blink,20);
        CKObject* tnode = m_anim->getChildByTag(15);
        machine_gun_blink->setPosition(ccp(tnode->getPos().x + BOX_SIZE/2,tnode->getPos().y));
        
        machine_gun_blink->setVisible(false);
        
        ps_machine_gun = CCParticleSystemQuad::create(CK::loadFile("airplane_machine_gun.plist").c_str());
        ps_machine_gun->setTexture(CCTextureCache::sharedTextureCache()->addImage(CK::loadFile("machine_gun.png").c_str()));
        ps_machine_gun->setPosition(ccp(BOX_SIZE*2,-BOX_SIZE/2));
  
        ps_machine_gun->setBlendFunc((ccBlendFunc){GL_ONE,GL_ONE_MINUS_SRC_ALPHA});
        
        if(SHOW_PARTICLE)
        {
            m_layer->addChild(ps_machine_gun ,zOrder_Airplane);
        }
        ps_machine_gun->stopSystem();
       // ps_machine_gun->release();
       // ps_machine_gun->setPositionType(kCCPositionTypeFree);
    }
};

CCSprite* CKAirplane::create(const char* _name)
{

    createAnim(_name);
    return m_sprite;
};

#pragma mark  - CKAirplane90
void CKAirplane90::reset()
{
    
};

void CKAirplane90::pause()
{
    
}
void CKAirplane90::resume()
{
    
}

void CKAirplane90::setBombs(CKBombBase *_bomb)
{
    m_bomb = static_cast<CKBomb90*>(_bomb);
};

int CKAirplane90::gameOverUpdete()
{
    if(!crash_anim)
    {
        CCFiniteTimeAction *seq = CCBlink::create(3.0, 20);
        seq->setTag(2);
        crash_anim = true;
        m_sprite->runAction(seq);
    }
    else
    {
        if (!m_sprite->getActionByTag(2)) {
            // CKAudioMng::Instance().playEffect("level_failed");
            return rAirplane_Over;//Game over
        }
    }
    return 0;
};

bool CKAirplane90::createWinAnimation()
{
    if(m_sprite->getPositionX() >= end_pos_x)
    {
        CKAudioMng::Instance().stopEffect("plane_fly");
        if(CKFileOptions::Instance().isPlayMusic())
        {
           CKAudioMng::Instance().fadeBgToVolume(0.0,0.5);
        };
        
        win_anim = true;
        m_sprite->setPosition(ccp(-m_sprite->getContentSize().width/2,ground_air));
        CCFiniteTimeAction *action = CCMoveTo::create(3.5, ccp(win_size.width*1.2,ground_air));
        action->setTag(2);
        // curent_action = action;
        m_sprite->runAction(action);
        
        for(int i = 0; i < 3; ++i )
        {
            m_emitter90[i] = CCParticleSystemQuad::create("fire90_start.plist");
            m_emitter90[i]->setPosition(ccp(win_size.width*0.5,ground_air));
            m_layer->addChild(m_emitter90[i] ,zOrderParticle);
            m_emitter90e[i] = NULL;
            if(is_new_score)
            {
                
                m_emitter90[i]->setStartColor((ccColor4F){CCRANDOM_0_1(),CCRANDOM_0_1(),CCRANDOM_0_1(),1.0});
                if(i > 0)
                    m_emitter90[i]->stopSystem();
            }
        };
        
        CCLOG("Duration %f",m_emitter90[0]->getDuration());
        int index = 0;
        
        CCFiniteTimeAction *action0 = CCMoveTo::create(1.1, ccp(win_size.width*0.3,win_size.height*0.7));
        action0->setTag(2);
        m_emitter90[index]->runAction(action0);
        index++;
        time_start_new_particle = 0;
        if(!is_new_score)
        {
            CCFiniteTimeAction *action1 = CCMoveTo::create(1.0, ccp(win_size.width*0.5,win_size.height*0.8));
            action1->setTag(2);
            m_emitter90[index]->runAction(action1);
            
            index++;
            CCFiniteTimeAction *action2 = CCMoveTo::create(1.1, ccp(win_size.width*0.7,win_size.height*0.7));
            action2->setTag(2);
            m_emitter90[index]->runAction(action2);
        }
        else
        {
            CKAudioMng::Instance().playEffect("salute_new_score");
        }
    };
    return true;
};

bool CKAirplane90::createCrashAnimation()
{
    return true;
};

int CKAirplane90::winUpdate(const float &dt)
{
    if(is_new_score)
    {
        
        if(time_start_new_particle == 15)
        {
            CCFiniteTimeAction *action1 = CCMoveTo::create(1.0, ccp(win_size.width*0.5,win_size.height*0.8));
            action1->setTag(2);
            m_emitter90[1]->runAction(action1);
            m_emitter90[1]->resetSystem();
            
        };
        
        if(time_start_new_particle == 30)
        {
            CCFiniteTimeAction *action2 = CCMoveTo::create(1.1, ccp(win_size.width*0.7,win_size.height*0.7));
            action2->setTag(2);
            m_emitter90[2]->runAction(action2);
            m_emitter90[2]->resetSystem();
            
        };
    };
    for(int i = 0; i < 3; ++i)
    {
        if(!m_emitter90[i]->getActionByTag(2) && m_emitter90e[i] == NULL && m_emitter90[i]->isActive())
        {
            CKAudioMng::Instance().playEffect("salute");
            m_emitter90[i]->stopSystem();
            CCParticleSmoke::create();
            m_emitter90e[i] = CCParticleSystemQuad::create("fire90_end.plist");
            m_emitter90e[i]->setPosition(m_emitter90[i]->getPosition());
            m_emitter90e[i]->setStartColor(m_emitter90[i]->getStartColor());
            
            m_emitter90e[i]->setEndColor(m_emitter90[i]->getStartColor());
            m_layer->addChild(m_emitter90e[i] ,zOrderParticle);
            CCLOG("Duration %f",m_emitter90e[i]->getDuration());
            
            // CKAudioMng::Instance().playEffect("salute");
        }
    }
    if(!m_sprite->getActionByTag(2))
    {
        return rAirplane_Win;//Game win
    };
    
    time_start_new_particle++;
    return 0;
};

void CKAirplane90::checkCrash()
{
    end_game = rAirplane_Over;
    
    if(CKFileOptions::Instance().isPlayMusic())
    {
        CKAudioMng::Instance().fadeBgToVolume(0.0,0.5);;
    };
    
    CKAudioMng::Instance().playEffect("level_failed");
    // CCTexture2D *tex = CCTextureCache::sharedTextureCache()->addImage(CK::loadImage("plane_900002.png").c_str());
    m_sprite->stopActionByTag(9);
    // m_sprite->setTexture(tex);
    
    CCSpriteFrame* s_frame = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("plane_900002.png");
    m_sprite->setTexture(s_frame->getTexture());
    m_sprite->setTextureRect(s_frame->getRect(), s_frame->isRotated(), m_sprite->getContentSize());
    
};

CCSprite* CKAirplane90::create(const char *_name)
{
    time_start_new_particle = 0;
    play_firework = false;
    m_sprite = CCSprite::createWithSpriteFrameName("plane_900000.png");// (CK::loadImage(_name).c_str());
    
    CCAnimation * anim = CCAnimation::create();
    CCSpriteFrame* m_sf = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("plane_900000.png");
    anim->addSpriteFrame(m_sf);
    anim->addSpriteFrame(m_sf);
    anim->addSpriteFrame(m_sf);
    anim->addSpriteFrame(m_sf);
    anim->addSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName("plane_900001.png"));
    
    anim->setDelayPerUnit(0.2);
    
    CCAnimate* animate = CCAnimate::create( anim );
    CCActionInterval* action_fly = CCRepeatForever::create( animate );
    action_fly->setTag(9);
    m_sprite->runAction(action_fly);
    
    m_layer->addChild(m_sprite,CK::zOrder_Airplane);
    
    setStartPositionX(-m_sprite->getContentSize().width/2);
    setEndPositionX(win_size.width + m_sprite->getContentSize().width/2);
    

    pos_x = -m_sprite->getContentSize().width;
    m_sprite->setPosition(ccp(pos_x,curent_height));
    
    return m_sprite;
};

CKAirplane90::CKAirplane90()
{
    
};

bool CKAirplane90::createBomb(unsigned char _type)
{
    float newX =  int(m_sprite->getPosition().x/(BOX_SIZE*0.5)+0.5)*BOX_SIZE/2 + BOX_SIZE/4;
    return m_bomb->create(ccp(newX,m_sprite->getPosition().y - m_sprite->getContentSize().height/3));
};

bool CKAirplane90::touch(CCPoint _pos,unsigned char _type)
{
    if(!end_game && (m_sprite->getPosition().x > 0) && (m_sprite->getPosition().x < (win_size.width - m_sprite->getContentSize().width/4)))
    {
        //CCLOG("CKAirplane90 m_array->isTopCube() %d",m_array->isTopCube(_pos));
        wait_new_loop = false;
        if(CKFileOptions::Instance().isKidsModeEnabled() && m_array->isTopCube(_pos))
        {
            wait_create_bomb = true;
            wait_position_x = _pos.x;
            if(wait_position_x < m_sprite->getPosition().x - m_sprite->getContentSize().width/4)
            {
                wait_new_loop = true;
            };
            CKAudioMng::Instance().playEffect("goal_detect");
            setDoubleSpeed(true);
            return true;
        }
        else
        {
            if(!wait_create_bomb)
                createBomb();
            return true;
        }
    };
   // CKAudioMng::Instance().playEffect("cant_create_bomb");
    return false;
};
void CKAirplane90::updateDoubleSpeed()
{
    if(!is_double_speed)
        return;
    calcDoubleSpeed();
}

const unsigned char CKAirplane90::update(const float &dt)
{
    
    if(end_game != 0)
    {
        if(end_game == rAirplane_Over)
        {
            return gameOverUpdete();
        }
        else
        {
            if(!win_anim)
            {
                createWinAnimation();
            }
            else
                return winUpdate(dt);
        };
        //return 0;
    };
    
    pos_x += speed*dt;
    m_sprite->setPosition(ccp(pos_x,curent_height));
    
    if(end_game != 0)
        return 0;
    updateDoubleSpeed();
    updateWaitBomb();

    
    if(m_sprite->getPositionX() >= end_pos_x /*(win_size.width + m_sprite->getContentSize().width/2)*/)
    {
        pos_x =  start_pos_x;//-m_sprite->getContentSize().width/2;
        m_sprite->setPosition(ccp(pos_x,curent_height));
        wait_new_loop = false;
        
        if(slow_fall)
        {
            if(m_array->getCountDestroyCube() >= count_destroy)
            {
                CKHelper::Instance().count_hit_for_loop++;
                if(CKHelper::Instance().count_hit_for_loop > max_value)
                    CKHelper::Instance().count_hit_for_loop = max_value;
            }
            else
            {
                CKHelper::Instance().count_hit_for_loop--;
                if(CKHelper::Instance().count_hit_for_loop < -2)
                    CKHelper::Instance().count_hit_for_loop = max_value;
            };
            m_array->setCountDestroyCube(0);
            
            if(CKHelper::Instance().count_hit_for_loop > 0)
            {
                moveToNewLoop();
            };
        }
        else
        {
            moveToNewLoop();
        }

        if(m_emitter_splash)
            m_emitter_splash->stopSystem();
        
        if (int(curent_height/step) == SHOW_BANNER_AFTER_HEIGHT)
            return rAirplane_Banner;
    };
    
    updateDoubleSpeed();
    
    int collision = m_array->checkAirplane(ccp(m_sprite->getPositionX() + m_sprite->getContentSize().width/2,m_sprite->getPositionY() -  m_sprite->getContentSize().height/4),ccp(m_sprite->getPositionX(),m_sprite->getPositionY() -  m_sprite->getContentSize().height/2));
    if(collision == rAirplane_Over)  //that collision with cube, game over
    {
        CKAudioMng::Instance().stopEffect("plane_fly");
        CKAudioMng::Instance().playEffect("plane_crash90");
        if(CKFileOptions::Instance().isPlayMusic())
            CKAudioMng::Instance().fadeBgToVolume(0.0,0.5);;
        checkCrash();
    }
    else
        if(collision == rAirplane_Win)  //not cube on level, you win
        {
            end_game = rAirplane_Win; //start win animation
            is_new_score = false;
            if(CKFileInfo::Instance().getLevel()->score != 0)
            {
                int t_score = 0;
                if(CKHelper::Instance().lvl_speed == 4)
                    t_score = getBonusFinish()*(CKHelper::Instance().lvl_speed + 7)*4;
                else
                    t_score = getBonusFinish()*(CKHelper::Instance().lvl_speed + 6)*4;
                
                CCLOG("is win %d current score %d and %d",CKFileInfo::Instance().getLevel()->score,static_cast<CKGameScene90 *>(m_sprite->getParent())->getCurentScore(),t_score);
                is_new_score = (CKFileInfo::Instance().getLevel()->score < (static_cast<CKGameScene90 *>(m_sprite->getParent())->getCurentScore() + t_score));
            }
            if(CKFileOptions::Instance().isKidsModeEnabled())
                is_new_score = true;
            
            CKAudioMng::Instance().playEffect("level_complited");
            if(CKFileOptions::Instance().isPlayMusic() )
                CKAudioMng::Instance().fadeBgToVolume(0.0,0.5);;
            speed = 12.6*BOX_SIZE;//set maximum speed move to finish
            return 0; // get Hi-score
        }
        else
            if(collision == rAirplane_Splash)
            {
                return rAirplane_Splash;
                //   return 2 + collision;
            };
    
    return 0;
    //  CCLOG("step %d curent_height %d",step,curent_height);
};