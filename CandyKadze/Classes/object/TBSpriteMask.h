//
//  TBSpriteMask.h
//  fgf
//
//  Created by PR1.Stigol on 07.06.13.
//
//

#ifndef __fgf__TBSpriteMask__
#define __fgf__TBSpriteMask__

#include <iostream>
#include "cocos2d.h"
using namespace cocos2d;

class TBSpriteMask: public CCSprite
{
private:
    CCSprite*  mask;
    CCTexture2D* maskTexture;
    CCGLProgram *pBWShaderProgram;
    GLuint textureLocation,offsetLocation;
    GLuint maskLocation,m_uTextureLocation;
    bool type;
    float offset_x;
    ccTex2F offset;
    ccQuad2 m_ccQuad2;
    GLuint kCCVertexAttrib_MaskCoords;
    //void builtWithTexture(CCTexture2D *_texture,const CCRect &textureRect);
    
public:
    virtual ~TBSpriteMask();
    void move(float _x, float _y);
    void moveMask(const float &_x, const float &_y);
    virtual void draw();
    void initWithSprite(CCSprite *_mask,bool _type = false);
    void setMaskRect(float _left,float _top,float _right,float _bottom);
    static TBSpriteMask* createWithSpriteFrameName(const char *pszSpriteFrameName);
    static TBSpriteMask* create(const char *_filename);
    static TBSpriteMask* createWithTexture(cocos2d::CCTexture2D *pTexture,const cocos2d::CCRect &rect,bool rotated = false);
    void buildMaskWithTexture(CCTexture2D *_texture,const CCRect &textureRect,bool _rotate,int _type_shader = 1);
  //  -(void)updateWithSprite:(CCSprite*)sprite;
  //  -(void)updateWithFile:(NSString *)file;
};




#endif /* defined(__fgf__TBSpriteMask__) */
