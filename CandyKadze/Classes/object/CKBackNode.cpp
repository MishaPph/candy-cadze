//
//  CKBackNode.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 28.09.12.
//
//

#include "CKBackNode.h"
#include "CKLanguage.h"

int CKObject::count = 0;
bool CKObject::isTouch()
{
  //  CCLOG("isTouch %d %d %d",(operation_state & OpTouch ),OpTouch,operation_state);
    return (operation_state & OpTouch) && is_enabled;
};

bool CKObject::isAccelerator()
{
    return operation_state & OpAccelerator;
};

bool CKObject::isAction()
{
    return operation_state & OpAction;
};

CKObject::CKObject()
{
    count++;
    curent_state_image = 0;
    z_order = 0;
    is_render_label = false;
    m_parent = NULL;
    count_child = 0;
    anim = NULL;
    label_object = NULL;
    slider_object = NULL;
    m_scroll_object = NULL;
    win_size = CCDirector::sharedDirector()->getWinSize();
};

CKObject* CKObject::clone()
{
    CKObject* tmp = new CKObject(*this);
    switch (type) {
        case CK::GuiButton:
            tmp->m_node = (CCNode *)CCSprite::createWithTexture(static_cast<CCSprite*>(this->m_node)->getTexture());
            static_cast<CCSprite*>(tmp->m_node)->setTextureRect(static_cast<CCSprite*>(this->m_node)->getTextureRect());
            tmp->start_pos = this->start_pos;
            this->m_node->getParent()->addChild(tmp->m_node,this->getzOrder());
            break;
            
        default:
            break;
    }
    
    return tmp;
};

CKObject::~CKObject()
{
    count--;
    it = list_asp.begin();
    while(it != list_asp.end())
    {
        CC_SAFE_DELETE(*it);
        (*it) = NULL;
        it++;
    };
    
    CC_SAFE_DELETE(anim);
    CC_SAFE_DELETE(label_object);
    CC_SAFE_DELETE(slider_object);
    CC_SAFE_DELETE(m_scroll_object);
    
    m_parent = NULL;
    m_user_data = NULL;
//    if(type == CK::GuiLabel && label_object && label_object->bmfont && m_node)
//        {
//            m_node->removeFromParentAndCleanup(false);
//            CC_SAFE_DELETE(m_node);
//        }
    list_asp.clear();
};

void CKObject::setSize(const CCSize &_size)
{
    size = _size;
};

const CCSize& CKObject::getSize()
{
    return size;
};

void CKObject::setPosition(const float &_x,const float &_y)
{
    m_node->setPositionX(_x);
    m_node->setPositionY(_y);
    start_pos = m_node->getPosition();
};

CCNode* CKObject::getNode()
{
    return m_node;
}

void CKObject::setNode(CCNode* _node)
{
    m_node = _node;
}

CCPoint CKObject::getPos()
{
    return m_node->getPosition();
};

const std::string& CKObject::getFunctionName()
{
    return func_name;
};
void CKObject::setFunctionName(const std::string& _str)
{
    func_name = _str;
};

int CKObject::getTag()
{
    return tag;
};
void CKObject::setVisible(bool _v)
{
    m_node->setVisible(_v);
};

bool CKObject::isVisible()
{
    return m_node->isVisible();
};

void CKObject::setValue(int value)
{
    func_value = value;
};

int* CKObject::getValue()
{
    return &func_value;
};

//void CKObject::setVisibleAll(bool _visible)
//{
//    this->setVisible(_visible);
//    it = list_asp.begin();
//    while(it != list_asp.end())
//    {
//        (*it)->setVisibleAll(_visible);
//        it++;
//    };
//};

int CKObject::getStateImage() const
{
    return curent_state_image;
};
const std::string &CKObject::getName()
{
    return map_file[curent_state_image];
};

void CKObject::setStateImage(int _state)
{
    if(map_file.size() < 2)
        return;
    
    if(map_file[_state].empty())
        return;
    
    if(map_file[_state].size() < 2)
        return;
    CCSpriteFrame* spframe = CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(map_file[_state].c_str());
    if(spframe)
    {
       // CCLOG("setStateImage::map_file[%d] ,%s rect(%f %f %f)",_state,map_file[_state].c_str(),spframe->getRect().size.height,spframe->getRect().size.width,spframe->getRect().origin.x);
        static_cast<CCSprite*>(m_node)->setTextureRect(spframe->getRect(),spframe->isRotated(), m_node->getContentSize());
        curent_state_image = _state;
    }
    else
    {
       // CCLOG("str map_file[%d] = %s",0,map_file[0].c_str());
         CCLOG("setStateImage::Error: incorect object state %d %s",_state,map_file[_state].c_str());
        if(map_file[0].length() > 3 && CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(map_file[0].c_str()))
        {
            CCLOG("setStateImage:: map_file[0]  not load state(%d) ,%s",_state,map_file[0].c_str());
            static_cast<CCSprite*>(m_node)->setTextureRect(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(map_file[0].c_str())->getRect(), CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(map_file[0].c_str())->isRotated(), m_node->getContentSize());
            curent_state_image = 0;
        }
        else
        {
            CCLOG("setStateImage::Error: incorect object state %d %s",0,map_file[0].c_str());
        }
    };
};

void CKObject::reloadText()
{
    if(type == CK::GuiLabel)
    {
        if(label_object->bmfont)
        {
            char str[NAME_MAX];
            if(strcmp(map_file[0].c_str(), "Digital") == 0 )
            {
                sprintf(str,"%s%d.fnt",map_file[0].c_str(),int(label_object->label_size + 0.5f));
            }
            else
            {
                sprintf(str,"%s_%s%d.fnt",map_file[0].c_str(),CKLanguage::Instance().getCurrent(),int(label_object->label_size + 0.5f));
            }
            static_cast<CCLabelBMFont *>(m_node)->setString("");
           // static_cast<CCLabelBMFont *>(m_node)->
            
            static_cast<CCLabelBMFont *>(m_node)->setFntFile(str);
            static_cast<CCLabelBMFont *>(m_node)->setString(LCZ(label_object->base_string));
        }
        else
        {
            static_cast<CCLabelTTF *>(m_node)->setString(LCZ(label_object->base_string));
        }
    }
    it = list_asp.begin();
    while(it != list_asp.end())
    {
        (*it)->reloadText();
        it++;
    };
};

CCNode* CKObject::createLabel(LabelObject *label_object,const std::string &_font,const CCSize &_size,CCDictionary *_param)
{
    if(_font.length() > 3)
    {
        //label_object = new LabelObject;
        label_object->bmfont = false;
      //  CCPoint sizes = ccp(size.width,size.height);
//        if(_param->objectForKey("Sizes"))//scale object
//        {
//        //    sizes = CCPointFromString(dictStr(_param, "Sizes"));
//        };
        const char * alig = dictStr(_param, "TextAlignment");
        //CCTextAlignment m_alig;
        if(strcmp(alig, "Center") == 0)
            label_object->alig_label = kCCTextAlignmentCenter;
        else
            if(strcmp(alig, "Left") == 0)
                label_object->alig_label = kCCTextAlignmentLeft;
            else
                if(strcmp(alig, "Right") == 0)
                   label_object->alig_label = kCCTextAlignmentRight;
            //alig_label = m_alig;
            label_object->label_size = dictFloat(_param, "TextSize");

            if(_param->objectForKey("Dynamic") && dictInt(_param, "Dynamic") == 1)
            {
                const char* wstr = LCZ(dictStr(_param, "Text"));
                label_object->base_string = dictStr(_param, "Text");
                CCLabelTTF *_tmp = CCLabelTTF::create(wstr, _font.c_str(),dictFloat(_param, "TextSize"),_size,label_object->alig_label,kCCVerticalTextAlignmentCenter);
                
                //_tmp->setColor(CK::strHextoColor(dictStr(_param, "TextColor")));
                return _tmp;
            }
            else
            {
                
               // CCLabelBMFont* _tmp = new CCLabelBMFont();
               // _tmp->init();

                char str[NAME_MAX];
                if(strcmp(_font.c_str(), "Digital") == 0 )
                {
                    sprintf(str,"%s%d.fnt",_font.c_str(),int(label_object->label_size+0.5f));
                }
                else
                {
                    sprintf(str,"%s_%s%d.fnt",_font.c_str(),CKLanguage::Instance().getCurrent(),int(label_object->label_size+0.5f));
                }
                //CCLOG("createLabelTTF %s string = %s",str,dictStr(_param, "Text"));
                
                 //const char* wstr = dictStr(_param, "Text");
                // CCLOG("createLabelTTF:: wstr %s size %d ",wstr,int(label_size+0.5f));
                CCLabelBMFont* _tmp = CCLabelBMFont::create(LCZ(dictStr(_param, "Text")), str);
                //_tmp->setFntFile(str);
                //_tmp->setString(LCZ(dictStr(_param, "Text")));
                label_object->base_string = dictStr(_param, "Text");
                label_object->bmfont = true;
                return _tmp;
            }

    }
    return NULL;
};

CCNode* CKObject::createPicture(const std::string &_file,CCDictionary *_param)
{
    if(_file.length() > 3)
    {
        if(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(_file.c_str()))
        {
            return CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(_file.c_str()));
        }
        else
        {
            return CCSprite::create(CK::loadImage(_file.c_str()).c_str());
        }
    };
    return NULL;
};

//CCNode* CKObject::createSelector(CCDictionary *_param)
//{
//    if(map_file[0].length() > 3)
//    {
//        if(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(map_file[0].c_str()))
//        {
//            
//            return CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(map_file[0].c_str()));
//            
//        }
//    }
//    return NULL;
//};

//CCNode* CKObject::createButton(const std::string &_file,CCDictionary *_param)
//{
//    if(_file.length() > 3)
//    {
//
//        if(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(_file.c_str()))
//        {
//            return CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(_file.c_str()));
//        }
//    }
//    return NULL;
//};

//CCNode* CKObject::createRadioButton(CCDictionary *_param)
//{
//    if(map_file[0].length() > 3)
//    {
//        if(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(map_file[0].c_str()))
//        {
//            return CCSprite::createWithSpriteFrameName(map_file[0].c_str());
//        }
//    }
//    return NULL;
//};

CCNode*  CKObject::createScrollView(ScrollObject* so,const std::string &_file,CCDictionary *_param)
{
    if(_file.length() > 3)
    {
        
        if(_param->objectForKey("SlideSensitivity"))//scale object
        {
            so->slide_sensivity = CCPointFromString(dictStr(_param, "SlideSensitivity"));
            so->scroll_num_window = dictInt(_param, "NumWindows");
            so->slide_percent = dictFloat(_param, "SlidePercent");
        };
        if(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(_file.c_str()))
        {
            //map_file.clear();
            
            //CCNode* _node = CCNode::create();
            return  CCNode::create();
            //return CCSprite::createWithSpriteFrameName("underlayInventoryBot.png");
        }
    }
    return NULL;
};
//
//CCNode*  CKObject::createSlot(const std::string &_file,CCDictionary *_param)
//{
//    type = CK::GuiSlot;
//
//    operation_state = operation_state | OpTouch;
//    CCNode* _node = CCNode::create();
//    _node->setContentSize(size);
//    return _node;
//};
//
//CCNode*  CKObject::createProgressBar(CCDictionary *_param)
//{
//    type = CK::GuiProgressBar;
//    operation_state = operation_state | OpTouch;
//    
//    CCNode* _node = CCNode::create();
//    _node->setContentSize(size);
//    return _node;
//};

CCNode*  CKObject::createSlider(SlideObject *sl,const std::string &_file,CCDictionary *_param)
{
    sl->vertical = true;
    if(_param->objectForKey("Direction"))
        
    {
        CCPoint point_a = CCPointFromString(dictStr(_param, "MinPos"));
        CCPoint point_b = CCPointFromString(dictStr(_param, "MaxPos"));
        
        if(strcmp(dictStr(_param, "Direction"), "Horizontal") == 0 )
        {
            sl->slide_limit.x = point_a.y;
            sl->slide_limit.y = point_b.y;
            sl->vertical = false;
        }
        else
        {
            sl->slide_limit.x = point_a.x;
            sl->slide_limit.y = point_b.x;
        }
    };
    if(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(_file.c_str()))
    {
        return CCSprite::createWithSpriteFrame(CCSpriteFrameCache::sharedSpriteFrameCache()->spriteFrameByName(_file.c_str()));
    }
    return NULL;
};

CKObject* CKObject::createWithDict(CCDictionary *_dict,const CCSize &_size,CCNode *_parent)
{
    
    CKObject* pObject = new CKObject;
    CCAssert(pObject != NULL, "Not free memory");
    pObject->setSize(_size);
    pObject->loadWithDict(_dict);
    if(pObject->getNode() != NULL)
    {
        _parent->addChild(pObject->getNode(),pObject->getzOrder());
        return pObject;
    }
    else
    {
        CC_SAFE_DELETE(pObject);
    };
    return NULL;
};

//load parallax object with  CCDictionary
void CKObject::loadWithDict(CCDictionary *_dict)
{
    m_dict = _dict;
    m_node = NULL;
    m_state = 0;
    operation_state = 0;
    
    std::string file_name = "";
    std::string type_str = "Picture";
    if(_dict->objectForKey("Type"))
    {
        type_str = dictStr(_dict, "Type");
    };
    type = CK::GuiNode;
    is_enabled = true;
    //load files
    if(_dict->objectForKey("ClipNames"))
    {
        CCArray* frameArray = (CCArray*)_dict->objectForKey("ClipNames");
        CCObject* pObj = NULL;
        int index = 0;
        CCARRAY_FOREACH(frameArray, pObj)
        {
            map_file[index] = static_cast<CCString*>(pObj)->getCString();
           // CCLOG("ClipNames %s",map_file[index].c_str());
            index++;
        };
    };
    
    tag = 0;
    if(_dict->objectForKey("ID"))
    {
        tag = dictInt(_dict, "ID");
    };
    
    
    if(type_str == "Picture")
    {
        type = CK::GuiPicture;
        m_node = createPicture(map_file[0]);
    }
    else
    {
        if(type_str == "Button")
        {
            type = CK::GuiButton;
            operation_state = operation_state | OpTouch;
            m_node = createPicture(map_file[0]);
        }
        else
            if(type_str == "RadioButton")
            {
                type = CK::GuiRadioBtn;
                operation_state = operation_state | OpTouch;
                m_node = createPicture(map_file[0]);
                //m_node = createRadioButton();
            }
            else
                if(type_str == "Label")
                {
                    type = CK::GuiLabel;
                    CCPoint sizes = CCPointFromString(dictStr(_dict, "Sizes"));
                    size.width = sizes.x;
                    size.height = sizes.y;
                    label_object = new LabelObject;
                    m_node = createLabel(label_object,map_file[0],size,(CCDictionary *)_dict->objectForKey("Special"));
                }
                else
                    if(type_str == "Selector")
                    {
                        type = CK::GuiSelector;
                        operation_state = operation_state | OpTouch;
                        m_node = createPicture(map_file[0]);
                    }
                    else
                        if(type_str == "ScrollView")
                        {
                            type = CK::GuiScrollView;
                            operation_state = operation_state | OpTouch;
                            m_scroll_object = new ScrollObject;
                            m_node = createScrollView(m_scroll_object,map_file[0],(CCDictionary *)_dict->objectForKey("Special"));
                        }
                        else
                            if(type_str == "Slot")
                            {
                                CCPoint sizes = CCPointFromString(dictStr(_dict, "Sizes"));
                                size.width = sizes.x;
                                size.height = sizes.y;
                                type = CK::GuiSlot;
                                
                                operation_state = operation_state | OpTouch;
                                m_node = CCNode::create();
                                m_node->setContentSize(size);
                            }
                            else
                            if(type_str == "ProgressBar"){
                                CCPoint sizes = CCPointFromString(dictStr(_dict, "Sizes"));
                                size.width = sizes.x;
                                size.height = sizes.y;
                                
                                type = CK::GuiProgressBar;
                                operation_state = operation_state | OpTouch;
                                m_node = CCNode::create();
                                m_node->setContentSize(size);
                                
                            }
                            else
                                if(type_str == "Slider")
                                {
                                    type = CK::GuiSlider;
                                    slider_object = new SlideObject;
                                    m_node = createSlider(slider_object,map_file[0],(CCDictionary *)_dict->objectForKey("Special"));
                                }
                                else
                                    if(type_str == "Node" || type_str == "Group")
                                    {
                                        type = CK::GuiNode;
                                        m_node = CCNode::create();
                                    };
    };
    
    if(!m_node)
    {
        CC_SAFE_DELETE(label_object);
        CCLOG("Error load Object ClipNames(\"%s\") type %s",map_file[0].c_str(),type_str.c_str());
        return;
    };
    
    loadParametrs(this, _dict);
    loadAnimation((CCDictionary *)_dict->objectForKey("Animation"));
    loadChild((CCDictionary*)_dict->objectForKey("Childs"));
};

void CKObject::loadParametrs(CKObject *_obj,CCDictionary *_dict)
{
    if(_dict->objectForKey("Sizes"))//scale object
    {
        CCPoint sizes = CCPointFromString(dictStr(_dict, "Sizes"));
        _obj->start_scale.x = sizes.x/_obj->m_node->getContentSize().width;
        _obj->start_scale.y = sizes.y/_obj->m_node->getContentSize().height;
        // CCLOG(" Sizes start_scale %f %f",start_scale.x,start_scale.y);
        if(_obj->type == CK::GuiScrollView || _obj->type == CK::GuiNode)
        {
            _obj->start_scale.x = 1.0;
            _obj->start_scale.y = 1.0;
            _obj->m_node->setContentSize(CCSize(sizes.x, sizes.y));
        };
        if(_obj->type == CK::GuiLabel)
        {
            _obj->start_scale.x = 1.0;
            _obj->start_scale.y = 1.0;
        }
    }
    
    _obj->start_pos =  CCPointFromString(dictStr(_dict, "Position"));
    _obj->m_node->setPosition(_obj->start_pos);
    _obj->scale = ccp(1.0,1.0);
    if(_obj->type != CK::GuiLabel)
    {
        if(_dict->objectForKey("Scale"))
        {
            _obj->scale =  CCPointFromString(dictStr(_dict, "Scale"));
        };
        
        _obj->m_node->setScaleX(_obj->start_scale.x*_obj->scale.x);
        _obj->m_node->setScaleY(_obj->start_scale.y*_obj->scale.y);
    }
    
    _obj->z_order = (_dict->objectForKey("zOrder"))?dictInt(_dict, "zOrder"):0;
    
    
    _obj->start_scale.x = _obj->m_node->getScaleX();
    _obj->start_scale.y = _obj->m_node->getScaleY();
    
    
    if(_dict->objectForKey("TransformCenter"))
    {
        _obj->m_node->setAnchorPoint(CCPointFromString(dictStr(_dict, "TransformCenter")));
    }else
    {
        _obj->m_node->setAnchorPoint(ccp(0.5,0.5));
    }
    
    _obj->start_rotation = 0;
    if(_dict->objectForKey("Rotation"))
    {
        _obj->start_rotation = dictInt(_dict, "Rotation");
    };
    
    _obj->m_node->setRotation(_obj->start_rotation);
    
    if(_dict->objectForKey("Touch"))
    {
        _obj->setTouch(dictInt(_dict, "Touch") == 1);
    }
    
    if(_dict->objectForKey("Value"))
    {
        _obj->func_value = dictInt(_dict, "Value");
    };
    
    if(_dict->objectForKey("Event"))
    {
        _obj->func_name = dictStr(_dict, "Event");
    };
};

void CKObject::loadChild(cocos2d::CCDictionary *frameDic)
{
    if(frameDic)
    {
        
        CCDictElement* pElement = NULL;
        
        CCDICT_FOREACH(frameDic, pElement)
        {
            //   CCLOG("load dict Childs key %s",pElement->getStrKey());
            
            CKObject* asp = new CKObject;
            asp->setSize(m_node->getContentSize());
            CCDictionary *tmp_dict = (CCDictionary*)pElement->getObject();
            asp->loadWithDict(tmp_dict);
            
            
            if(asp->getNode() != NULL)
            {                
                if(asp->getFunctionName() == "levelCall")
                {
                    createCopy(tmp_dict,asp->tag,16);
                    m_node->removeChild(asp->getNode(), true);
                    delete asp;
                    asp = NULL;
                }else if(asp->getFunctionName() == "selectCall")
                {
                    CCLOG("create Copy %d",*asp->getValue());
                    createCopy(tmp_dict,asp->tag,*asp->getValue());
                    m_node->removeChild(asp->getNode(), true);
                    delete asp;
                    asp = NULL;
                }else
                {
                    attachChild(asp);
                }
            }
            else
            {
                delete asp;
                asp = NULL;
            }
        }
    };
}
void CKObject::loadAnimation(CCDictionary *animDic)
{
    if(!animDic)
        return;
    
    anim = new AnimObject;
    
    if(animDic->objectForKey("Moving"))
    {
        anim->move =  CCPointFromString(dictStr(animDic, "Moving"));
        if((anim->move.x != 0.0)||(anim->move.y != 0.0))
        {
            operation_state = operation_state | OpMove;
        }
    };
    
    if(animDic->objectForKey("TextureMove"))
    {
        anim->texture_move =  CCPointFromString(dictStr(animDic, "TextureMove"));
        if((anim->texture_move.x != 0.0)||(anim->texture_move.y != 0.0))
        {
            anim->texture_move.x /= size.width;
            anim->texture_move.y /= size.height;
            
            operation_state = operation_state | OpTexMove;
            anim->b_texture_move = true;
            
            CCTexture2D *tex = CCTextureCache::sharedTextureCache()->addImage(CK::loadImage(map_file[0].c_str()).c_str());
            if(tex)
            {
                static_cast<CCSprite *>(m_node)->setTexture(tex);
                static_cast<CCSprite *>(m_node)->setTextureRect(CCRect(0, 0, tex->getContentSize().width, tex->getContentSize().height));
                ccTexParams params = {GL_LINEAR,GL_LINEAR,GL_REPEAT,GL_REPEAT};
                static_cast<CCSprite *>(m_node)->getTexture()->setTexParameters(&params);
            }
        }
    };
    
    if(animDic->objectForKey("Jumping"))
    {
        //  jumping =  CCPointFromString(dictStr(animDic, "Jumping"));
        anim->jumping = dictFloat(animDic, "Jumping");
        if(anim->jumping != 0.0)
        {
            anim->b_jump = true;
            operation_state = operation_state | OpJump | OpAction;
        }
    };
    
    if(animDic->objectForKey("Rotate"))
    {
        anim->amplitude_rotate = dictInt(animDic, "Rotate");
        if(anim->amplitude_rotate)
        {
            anim->b_rotate = true;
            operation_state |=  OpAction;
        }
    };
}
void CKObject::createCopy(CCDictionary *_dict,int _tag,int count)
{
    for(int k = 0; k < count;k++)
    {
        CKObject* tasp = new CKObject;
        tasp->setSize(m_node->getContentSize());
        tasp->loadWithDict(_dict);
        if(tasp->isAction())
            operation_state = operation_state| OpAction;
        tasp->func_value = k;
        tasp->tag = _tag*1000 + k + 1;
        attachChild(tasp);

    };
};

void CKObject::copyObject(CKObject *_object,int _tag)
{
    CKObject* asp = new CKObject;
    asp->setSize(m_node->getContentSize());
    asp->loadWithDict(_object->m_dict);
    if(asp->getNode() != NULL)
    {
        if(asp->isAction())
            operation_state = operation_state| OpAction;
        asp->tag = _tag;
        attachChild(asp);
        
    }
    else
    {
        CC_SAFE_DELETE(asp);
    }
};

void CKObject::attachChild(CKObject*  _obj)
{
    m_node->addChild(_obj->getNode(),_obj->getzOrder());
    _obj->m_parent = this;
    _obj->count_child++;
    list_asp.push_back(_obj);
};

void CKObject::savePosition(const CCPoint &_pos)
{
    start_pos = _pos;
};

void CKObject::moveAllChildren(const float &_x,const float &_y)
{
    type_collect::iterator m_it = list_asp.begin();
    while(m_it != list_asp.end())
    {
        CKObject *m_obj = (*m_it);
        m_obj->setPosition(m_obj->getPos().x + _x, m_obj->getPos().y + _y);
        m_it++;
    };
};

CCRect CKObject::getChildZoneRect()
{
    type_collect::iterator m_it = list_asp.begin();
    CCRect rect;
    while(m_it != list_asp.end())
    {
        if(!(*m_it)->isVisible())
        {
            m_it++;
            continue;
        }
        CCNode *tmp = (*m_it)->getNode();
        
        float min_x = tmp->getPositionX() - tmp->getContentSize().width*tmp->getAnchorPoint().x;
        float max_x = tmp->getPositionX() + tmp->getContentSize().width*(1.0 - tmp->getAnchorPoint().x);
        
        float min_y = tmp->getPositionY() - tmp->getContentSize().height*tmp->getAnchorPoint().y;
        float max_y = tmp->getPositionY() + tmp->getContentSize().height*(1.0 - tmp->getAnchorPoint().y);
        if(min_x < rect.origin.x)
            rect.origin.x = min_x;
        
        if(max_x > rect.size.width)
            rect.size.width = max_x;
        
        if(min_y < rect.origin.y)
            rect.origin.y = min_y;
        
        if(max_y > rect.size.height)
            rect.size.height = max_y;
        
        m_it++;
    };
    return rect;
};

const char* CKObject::getText()
{
    if(is_render_label)
    {
       // return static_cast<CCLabelTTF*>(label_dunamic)->getString();
    }
    else if (label_object->bmfont)
    {
        return static_cast<CCLabelBMFont*>(m_node)->getString();
    }
    else
    {
        return static_cast<CCLabelTTF*>(m_node)->getString();
    }
    return NULL;
};

void CKObject::setText(const std::string &_text)
{
    if(type == CK::GuiLabel)
    {
        if(is_render_label)
        {

        }
        else
        {
            if(label_object->bmfont)
            {
                CCLabelBMFont* b_font = static_cast<CCLabelBMFont*>(m_node);
                if(b_font)
                {
                    if( _text != static_cast<CCLabelBMFont*>(m_node)->getString())
                    {
                        static_cast<CCLabelBMFont*>(m_node)->setVisible(false);
                        static_cast<CCLabelBMFont*>(m_node)->setString("");
                        static_cast<CCLabelBMFont*>(m_node)->setString(_text.c_str());
                        static_cast<CCLabelBMFont*>(m_node)->setVisible(true);
                    }
                }
                
            }
            else
            {
                if( _text != static_cast<CCLabelTTF*>(m_node)->getString())
                {
                    static_cast<CCLabelTTF*>(m_node)->setVisible(false);
                    static_cast<CCLabelTTF*>(m_node)->setString("");
                    static_cast<CCLabelTTF*>(m_node)->setString(_text.c_str());
                    static_cast<CCLabelTTF*>(m_node)->setVisible(true);
                }
            }
            
        }
    };
}
float CKObject::getLong()
{
    return (slider_object->slide_limit.y - slider_object->slide_limit.x);
}
void CKObject::setProgress(const float &_pr)
{
    if(type == CK::GuiProgressBar)
    {
       // static_cast<CCSprite *>(this->getChildByTag(1)->getNode())->setBlendFunc((ccBlendFunc){GL_ONE_MINUS_DST_ALPHA,GL_SRC_COLOR});
        //static_cast<CCSprite *>(this->getChildByTag(2)->getNode())->setBlendFunc((ccBlendFunc){GL_ONE_MINUS_SRC_ALPHA,GL_NONE});
        this->getChildByTag(1)->getNode()->setScaleX(_pr);
    }
    else
        if(type == CK::GuiSlider)
        {
            if(slider_object->vertical)
            {
                
                this->getChildByTag(2)->setVisible(false);
                
                //CKObject* bar = this->getChildByTag(3);
                this->getChildByTag(3)->setPosition(this->getChildByTag(1)->getPos().x,_pr*this->getChildByTag(1)->getNode()->getContentSize().height + this->getChildByTag(1)->getPos().y);
            }
            else
            {
                
                //this->getChildByTag(2)->getNode()->setScaleX(1.0 - _pr);
                CKObject* bar = this->getChildByTag(3);
                float value = (slider_object->slide_limit.y - slider_object->slide_limit.x);
              
                float width_ = this->getChildByTag(1)->getNode()->getContentSize().width;
                this->getChildByTag(1)->getNode()->setScaleX(_pr*value/width_);
              //  CCLOG("setProgress:: %f %f %f %f",bar->getPos().x,percent,_pr,value);
                 bar->setPosition(slider_object->slide_limit.x + _pr*value,this->getChildByTag(1)->getPos().y);
             //   CCLOG("setProgress:: %f %f %f",bar->getPos().x,percent,_pr);
            }
        };
};

float CKObject::getProgress()
{
    if(type == CK::GuiProgressBar)
    {
        return this->getChildByTag(1)->getNode()->getScaleX();
    }
    else
        if(type == CK::GuiSlider)
        {
            if(slider_object->vertical)
            {
                return this->getChildByTag(1)->getNode()->getScaleY();
            }
            else
            {
                float value = (slider_object->slide_limit.y - slider_object->slide_limit.x);
                float percent = MAX(0,(this->getChildByTag(3)->getPos().x - slider_object->slide_limit.x))/value;
                return percent;//this->getChildByTag(1)->getNode()->getScaleX();
            }
        };
    CCAssert(true, "this CKObject not method getProgress()");
    return 0.0;
};

const float& CKObject::getSlidePersent()
{
    return m_scroll_object->slide_percent;
};

CKObject* CKObject::getParent()
{
    return m_parent;
};

bool CKObject::isEnabled()
{
    return is_enabled;
}

void CKObject::setUserData(void *_data)
{
    m_user_data = _data;
};

void* CKObject::getUserData()
{
    return m_user_data;
};

void CKObject::setEnabled(bool _b)
{
    is_enabled = _b;
    if(_b)
    {
       setStateImage(0);
    }
    else
    {
       setStateImage(2);
    };
};
void CKObject::setTouch(bool _b)
{
  //  CCLOG("setTouch::is Touch %d %d %d ",isTouch(),_b,operation_state);
    if(_b)
    {
        operation_state |= OpTouch; //add touch into state
    }
    else
        if(operation_state&OpTouch)
            operation_state ^= OpTouch; //remove touch from state
  //  CCLOG("setTouch::is Touch %d %d",isTouch(),operation_state);
};
CCRect CKObject::rect()
{
    return CCRectMake( getPos().x - getSize().width * m_node->getAnchorPoint().x,
                      getPos().y - getSize().height * m_node->getAnchorPoint().y,
                      getSize().width, getSize().height);
};

CKObject* CKObject::objectForTouch(const CCPoint &_p)
{
    CCLOG("objectForTouch:: %f %f",_p.x,_p.y);
    type_collect::iterator m_it = list_asp.begin();
    while(m_it != list_asp.end())
    {
        if((*m_it)->isTouch() && (*m_it)->isVisible())
        {
            CCPoint local =  m_node->convertToNodeSpace(_p);
            return objectForTouch(local);
        };
        m_it++;
    };
    
    CCPoint local =  m_node->convertToNodeSpace(_p);
     CCLOG("objectForTouch:: convertToNodeSpace%f %f",local.x,local.y);
    CCRect r = rect();
    r.origin = CCPointZero;
    
    if (r.containsPoint(local))
    {
        return this;
    };
    
    return NULL;
};

bool CKObject::isPointInObject(const CCPoint &_p)
{
    CCPoint pLB = ccp(getPos().x - m_node->getContentSize().width*m_node->getAnchorPoint().x*start_scale.x,getPos().y - m_node->getContentSize().height*m_node->getAnchorPoint().y*start_scale.y);
    CCPoint p = ccp(_p.x - pLB.x,_p.y - pLB.y);
    if(p.x > 0 && p.y >0 )
    {
       // CCLOG("isPointInObject %f %f",p.x,p.y);
        if((p.x < m_node->getContentSize().width*start_scale.x) && (p.y < m_node->getContentSize().height*start_scale.y))
        {
            return true;
        }
    };
    return false;
};


CKObject* CKObject::getTouchObject(const CCPoint &_pos)
{
    
    CCPoint pLB = ccp(getPos().x - m_node->getContentSize().width*m_node->getAnchorPoint().x*start_scale.x,getPos().y - m_node->getContentSize().height*m_node->getAnchorPoint().y*start_scale.y);
    
    //CCPoint p = m_node->convertToNodeSpaceAR(_pos); //tutorial select bug
    
    CCPoint p = ccp(_pos.x - pLB.x,_pos.y - pLB.y);
    
    if(isPointInObject(_pos))
    {
            if(type == CK::GuiScrollView)
            {
                CCLOG("touch in Scrool view");
            }else if(type == CK::GuiButton)
            {
                CCLOG("getTouchObject:: tag %d",getTag());
                CCLOG("touch in GuiButton");
            }else if(type == CK::GuiSlot)
            {
                CCLOG("touch in GuiSlot");
            } else if(type == CK::GuiNode)
            {
                CCLOG("touch in GuiNode");
            }
            else{
                CCLOG("touch in Other tag %d",getTag());
                
            }
           // CCLOG("width = %f height %f %f %f %f",p.x,p.y,pLB.x,pLB.y,m_node->getContentSize().width);
            type_collect touch_list;
            type_collect::iterator m_it = list_asp.begin();
            int max_zOrder = -10;
            while(m_it != list_asp.end())
            {
                CKObject *m_obj = (*m_it);
                if(m_obj->isTouch() && m_obj->isVisible())
                {
                    CKObject* tmp = m_obj->getTouchObject(ccp(p.x,p.y));
                    if(tmp != NULL)
                    {
                        touch_list.push_back(tmp);
                        if(max_zOrder < tmp->getzOrder())
                        {
                            max_zOrder = tmp->getzOrder();
                        };
                        //return tmp;
                    };
                };
                m_it++;
            };
            if(max_zOrder != -10)
            {
                m_it = touch_list.begin();
                while(m_it != touch_list.end())
                {
                    if(max_zOrder == (*m_it)->getzOrder())
                        return (*m_it);
                    m_it++;
                }
            }
            return this;
    };
    return NULL;
};
CKObject* CKObject::getTouchObject2(const CCPoint &_pos)
{
    CCPoint pLB;

        pLB = ccp(getPos().x - m_node->getContentSize().width*m_node->getAnchorPoint().x*start_scale.x,getPos().y - m_node->
             getContentSize().height*m_node->getAnchorPoint().y*start_scale.y);

    CCPoint p = ccp(_pos.x - pLB.x,_pos.y - pLB.y);
    
    if(isPointInObject(_pos))
    {
        if(type == CK::GuiScrollView)
        {
            CCLOG("touch in Scrool view");
        }else if(type == CK::GuiButton)
        {
            CCLOG("getTouchObject:: tag %d",getTag());
            CCLOG("touch in GuiButton");
        }else if(type == CK::GuiSlot)
        {
            CCLOG("touch in GuiSlot");
        } else if(type == CK::GuiNode)
        {
            CCLOG("touch in GuiNode");
        }
        else{
            CCLOG("touch in Other tag %d",getTag());
            
        }
        // CCLOG("width = %f height %f %f %f %f",p.x,p.y,pLB.x,pLB.y,m_node->getContentSize().width);
        type_collect touch_list;
        type_collect::iterator m_it = list_asp.begin();
        int max_zOrder = -10;
        while(m_it != list_asp.end())
        {
            CKObject *m_obj = (*m_it);
            if(m_obj->isTouch() && m_obj->isVisible())
            {
                CKObject* tmp = m_obj->getTouchObject2(ccp(p.x,p.y));
                if(tmp != NULL)
                {
                    touch_list.push_back(tmp);
                    if(max_zOrder < tmp->getzOrder())
                    {
                        max_zOrder = tmp->getzOrder();
                    };
                    //return tmp;
                };
            };
            m_it++;
        };
        if(max_zOrder != -10)
        {
            m_it = touch_list.begin();
            while(m_it != touch_list.end())
            {
                if(max_zOrder == (*m_it)->getzOrder())
                    return (*m_it);
                m_it++;
            }
        }
        return this;
    };
    return NULL;
};

CKObject* CKObject::getChildByTag(const int &_tag)
{
    it = list_asp.begin();
    while(it != list_asp.end())
    {
        if((*it)->getTag() == _tag)
        {
                return (*it);
        };
        it++;
    };
    return NULL;
};

CKObject* CKObject::getChildByType(int _type)
{
    it = list_asp.begin();
    while(it != list_asp.end())
    {
        if((*it)->type == _type)
        {
            return (*it);
        };
        it++;
    };
    return NULL;
};

int CKObject::getzOrder()
{
    return z_order;
};

void CKObject::setSlideSensivity(const CCPoint &_point)
{
    if(!m_scroll_object)
    {
        m_scroll_object = new ScrollObject;
    };
    m_scroll_object->slide_sensivity = _point;
};

void CKObject::moveX(const float &_x)
{
    m_node->setPositionX(start_pos.x + _x*m_scroll_object->slide_sensivity.x);
};
void CKObject::moveY(const float &_y)
{
    m_node->setPositionY(start_pos.y + _y*m_scroll_object->slide_sensivity.y);
};

int CKObject::getType()
{
    return type;
};

void CKObject::setStateChild(int _state)
{
    CCLOG("setStateChild %d",_state);
    it = list_asp.begin();
    while(it != list_asp.end())
    {
        if(map_file.size() > 0)
        {
            (*it)->setStateImage(_state);
        };
        it++;
    };
};

void CKObject::onClick()
{
    if(type == CK::GuiRadioBtn)
    {
        
    };
};

void CKObject::update(const CCPoint &_point)
{
    if(anim)
    {
        if(operation_state & OpMove)
        {
            m_node->setPositionX(start_pos.x + _point.x*anim->move.x*size.width);
            //   if(m_node->getActionByTag(2) == NULL)
            m_node->setPositionY(start_pos.y + _point.y*anim->move.y*size.height);
        };
        
        if(anim->b_texture_move)
        {
            CKSprite *_sp = (CKSprite *)m_node;
            _sp->move(anim->texture_move.x,anim->texture_move.y);
        };
    }
    it = list_asp.begin();
    while(it != list_asp.end())
    {
        (*it)->update(_point);
        it++;
    };
};

#pragma mark - BACK NODE
CKBaseNode::~CKBaseNode()
{
    CCLOG("~CKBaseNode destroy %s size %d",name.c_str(),list_asp.size());
    it = list_asp.begin();
    while(it != list_asp.end())
    {
        CC_SAFE_DELETE(*it);
        (*it) = NULL;
        it++;
    };
    list_asp.clear();
    CC_SAFE_RELEASE_NULL(m_dict);
};
CKBaseNode::CKBaseNode()
{
    m_dict = NULL;
}
void CKBaseNode::reloadText()
{
    it = list_asp.begin();
    while(it != list_asp.end())
    {
        (*it)->reloadText();
        it++;
    };
};

void CKBaseNode::reloadAtlas()
{
    return;
    std::list<std::string>::iterator _it = list_atlas.begin();
    while (_it != list_atlas.end()) {
        CCLOG("reloadAtlas:: %s",(*_it).c_str());
        CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile((*_it).c_str());
        _it++;
    };
/*    _it = list_texture_name.begin();
    while (_it != list_texture_name.end()) {
        CCLOG("removeAtlasPlist::remove texture %s",(*_it).c_str());
        CCTextureCache::sharedTextureCache()->removeTextureForKey((*_it).c_str());
        _it++;
    };*/
};

void CKBaseNode::removeAtlasPlist()
{
    std::list<std::string>::iterator _it = list_atlas.begin();
    while (_it != list_atlas.end()) {
        CCSpriteFrameCache::sharedSpriteFrameCache()->removeSpriteFramesFromFile((*_it).c_str());
        _it++;
    };

  
   _it = list_texture_name.begin();
    while (_it != list_texture_name.end()) {
        CCLOG("removeAtlasPlist::remove texture %s",(*_it).c_str());
        CCTextureCache::sharedTextureCache()->removeTextureForKey((*_it).c_str());
        _it++;
    };
    //static_cast<CCSprite*>(getChildByType(CK::GuiPicture)->getNode());
   // glDeleteTextures(1, &textureId );
};
std::string CKBaseNode::getAtlasTextureName(const char * _name)
{
    CCDictionary *dict = CCDictionary::createWithContentsOfFile(_name);
    std::string texturePath("");
    
    if(!dict)
    {
        CCLog("load::Error:: incorect file name");
        return texturePath;
    }
    CCDictionary* frameDic = (CCDictionary*)dict->objectForKey("frames");
    if(!frameDic)
    {
        return texturePath;
    }
    CCDictionary* dict_meta = (CCDictionary*)dict->objectForKey("metadata");
    if(dict_meta)
    {
        CCArray* frames = (CCArray*)dict_meta->objectForKey("atlasFileName");
        CCObject* pObj = NULL;
        CCARRAY_FOREACH(frames, pObj)
        {
            const char* frameName = ((CCString*)pObj)->getCString();
            //save texture file name for delete
            const char *pszPath = CCFileUtils::sharedFileUtils()->fullPathFromRelativePath(CK::loadImage(frameName).c_str());
            CCDictionary *tdict = CCDictionary::createWithContentsOfFileThreadSafe(pszPath);
            
            CCDictionary* metadataDict = (CCDictionary*)tdict->objectForKey("metadata");
            if (metadataDict)
            {
                // try to read  texture file name from meta data
                texturePath = metadataDict->valueForKey("textureFileName")->getCString();
            }
            CCLOG("getAtlasTextureName:: texturePath %s",texturePath.c_str());
            tdict->release();
        }
    };

    dict->release();
    return texturePath;
};

void CKBaseNode::load(const char * _name)
{
    m_dict = CCDictionary::createWithContentsOfFile(_name);
    name = _name;
    CCLog("load:: parallax %s",_name);
    if(!m_dict)
    {
        CCLog("load::Error:: incorect file name");
        return;
    }
    m_dict->retain();
    CCDictionary* frameDic = (CCDictionary*)m_dict->objectForKey("frames");
    if(!frameDic)
    {
        return;
    }
    CCDictionary* dict_meta = (CCDictionary*)m_dict->objectForKey("metadata");
    if(dict_meta)
    {
        CCArray* frames = (CCArray*)dict_meta->objectForKey("atlasFileName");
        CCObject* pObj = NULL;
        CCARRAY_FOREACH(frames, pObj)
        {
            const char* frameName = ((CCString*)pObj)->getCString();
            
            list_atlas.push_back(CK::loadImage(frameName));
            
//save texture file name for delete
                const char *pszPath = CCFileUtils::sharedFileUtils()->fullPathFromRelativePath(CK::loadImage(frameName).c_str());
                CCDictionary *tdict = CCDictionary::createWithContentsOfFile(pszPath);
                
                std::string texturePath("");
                
                CCDictionary* metadataDict = (CCDictionary*)tdict->objectForKey("metadata");
                if (metadataDict)
                {
                    // try to read  texture file name from meta data
                    texturePath = metadataDict->valueForKey("textureFileName")->getCString();
                }
                
                if (! texturePath.empty())
                {
                    // build texture path relative to plist file
                    texturePath = CCFileUtils::sharedFileUtils()->fullPathFromRelativeFile(texturePath.c_str(), pszPath);
                }
                if (! texturePath.empty())
                {
                    list_texture_name.push_back(CK::loadImage(texturePath.c_str()));
                  //  CCTexture2D *tex = CCTextureCache::sharedTextureCache()->textureForKey(texturePath.c_str());
                    CCTexture2D *tex = CCTextureCache::sharedTextureCache()->addImage(texturePath.c_str());
                    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(CK::loadImage(frameName).c_str(),tex);
                }
                else
                {
                    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(CK::loadImage(frameName).c_str());
                }
                tdict->release();
            }
    };
    
   // reloadAtlas();
    
    CCDictElement* pElement = NULL;
    list_asp.clear();
    CCDICT_FOREACH(frameDic, pElement)
    {
        //CCLOG("CKBaseNode::load:: dict key %s",pElement->getStrKey());
        CKObject* asp = CKObject::createWithDict((CCDictionary*)pElement->getObject(), CKHelper::Instance().getWinSize(), m_node);
        if(asp)
        {
            list_asp.push_back(asp);
        }
    }

    is_create = !(list_asp.size() == 0);
    CCLog("load:: end load %s %d",_name,list_asp.size());
};

void CKBaseNode::create(CCNode *_n)
{
    m_node = (CCSpriteBatchNode*)_n;
};

CCNode *CKBaseNode::get()
{
    return m_node;
};

void CKBaseNode::update(const float &_dt)
{

};

//bool CKBaseNode::isVisible()
//{
//    return is_visible;
//};

//void CKBaseNode::setVisible(bool _v)
//{
//    is_visible = _v;
//    m_node->setVisible(_v);
//    it = list_asp.begin();
//    while(it != list_asp.end())
//    {
//        (*it)->setVisible(_v);
//        it++;
//    };
//}

CKObject* CKBaseNode::getChildByTag(const int &_tag)
{
    it = list_asp.begin();
    while(it != list_asp.end())
    {
        if((*it)->getTag() == _tag)
        {
            return (*it);
        };
        it++;
    };
    
    return NULL;
};

CKObject* CKBaseNode::getChildByType(int _type)
{
    it = list_asp.begin();
    while(it != list_asp.end())
    {
        if((*it)->type == _type)
        {
            return (*it);
        }
        it++;
    };
    return NULL;
};



CKObject* CKBaseNode::getTouchObject(const CCPoint &_pos)
{
    std::list<CKObject* > touch_list;
    int max_zOrder = -10;
    it = list_asp.begin();
    while(it != list_asp.end())
    {
        if((*it)->isTouch() && (*it)->isVisible())
        {
            CCPoint pLB = ccp(m_node->getPositionX() - m_node->getContentSize().width*m_node->getAnchorPoint().x,m_node->getPositionY() - m_node->getContentSize().height*m_node->getAnchorPoint().y);
            CCPoint p = ccp(_pos.x - pLB.x,_pos.y - pLB.y);
            //CCPoint p = m_node->convertToNodeSpace(_pos);
           // CCLOG("CKBaseNode::getTouchObject %f %f",p.x,p.y);
            
            CKObject* tmp =(*it)->getTouchObject(p);
            if( tmp != NULL)
            {
                if(tmp->getzOrder() > max_zOrder)
                {
                    max_zOrder = tmp->getzOrder();
                    touch_list.push_back(tmp);
                };
            }
        };
        it++;
    };
    
    if(max_zOrder != -10)
    {
        it = touch_list.begin();
        while(it != touch_list.end())
        {
            if(max_zOrder == (*it)->getzOrder())
                return (*it);
            it++;
        }
    }
    return NULL;
};

CKObject* CKBaseNode::getTouchObject2(const CCPoint &_pos)
{
    std::list<CKObject* > touch_list;
    int max_zOrder = -10;
    it = list_asp.begin();
    while(it != list_asp.end())
    {
        if((*it)->isTouch() && (*it)->isVisible())
        {

            CCPoint pLB = ccp(m_node->getPositionX() - m_node->getContentSize().width*m_node->getAnchorPoint().x,m_node->getPositionY() - m_node->getContentSize().height*m_node->getAnchorPoint().y);
            CCPoint p = ccp(_pos.x - pLB.x,_pos.y - pLB.y);
            CKObject* tmp =(*it)->getTouchObject2(p);
            if( tmp != NULL)
            {
                if(tmp->getzOrder() > max_zOrder)
                {
                    max_zOrder = tmp->getzOrder();
                    touch_list.push_back(tmp);
                };
            }
        };
        it++;
    };
    
    if(max_zOrder != -10)
    {
        it = touch_list.begin();
        while(it != touch_list.end())
        {
            if(max_zOrder == (*it)->getzOrder())
                return (*it);
            it++;
        }
    }
    return NULL;
};