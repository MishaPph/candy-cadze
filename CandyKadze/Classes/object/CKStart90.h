//
//  CKStart90.h
//  CandyKadze
//
//  Created by PR1.Stigol on 04.10.12.
//
//

#ifndef __CandyKadze__CKStart90__
#define __CandyKadze__CKStart90__

#include <iostream>
#include <cocos2d.h>
#include <list.h>

using namespace cocos2d;

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//create background object (starts and cloud) in world 90-x
//stars blink and cloud - move (static speed)
//parametrs - min height position, max count object on display,and speed if is cloud
//random generated visual object with list name.
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class CKStart90
{
    struct CKElement
    {
        CCSprite * sprite;
        float speed;
    };
    int count_starts;
    int max_count;
    void addStars(const float &_x,const float &_y);
    void addClouds(const float &_x,const float &_y);
    CCLayer* m_layer;
    float speed;
    float speed_var;
    float min_height;
    CCSize win_size;
    std::list< CKElement* > star_list;
    std::list< CKElement* >::iterator it;
    std::list< CKElement* > cloud_list;
   // std::vector<char[32]> start_name;
    std::string star_name1,star_name2;
    std::vector<const char *> cloud_name;
public:
    ~CKStart90();
    CKStart90();
    void addStartName(const std::string &_str);
    void addCloudName(const std::string &_str);
    void createStarts(int _max_count,float _speed);
    void createCloud(int _max_count,float _speed,float _speed_var);
    void update(const float &_dt);
    void setLayer(CCLayer* _m_layer);
    void setMinHeight(const float &_height);
};
#endif /* defined(__CandyKadze__CKStart90__) */
