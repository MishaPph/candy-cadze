//
//  CKBomb.h
//  CandyKadze
//
//  Created by PR1.Stigol on 20.09.12.
//
//

#ifndef __CandyKadze__CKBomb__
#define __CandyKadze__CKBomb__

#include "CKBaseBomb.h"
#include "CKBonusMgr.h"
#include <iostream>
#include <list.h>
#include "CKCubeArray.h"

using namespace CK;
using namespace cocos2d;


class CKBonus;
class CKCubeArray;
class CKBombs;
struct Sbomb
{
    typedef bool (CKBombs::*func)(Sbomb*);
    unsigned char type;
    unsigned char previor_type;
    float speed;
    float line_velosity;
    bool is_active;
    CCSprite* sprite;
    CCPoint previor_pos;
    float distanse;
    func m_func;
    bool is_airplane_bomb;
};


struct ExploseEff
{
    CCSprite* sprite;
    CCAction *anim;
    CCRenderTexture* rtt;
    bool is_active;
};

class BombEffect
{
    list< ExploseEff* > list_explose;
    list< ExploseEff* >::iterator it_exp;
    int BOX_SIZE;
    int ground_height;
    CCNode *current_node;
    void run(const cocos2d::CCPoint &_p,const float &_size);
public:
    BombEffect();
    ~BombEffect();
    void setGroundHeight(int _ground_height);
    void create(CCNode *_layer,int _count);
    void hideAll();
    void show(const cocos2d::CCPoint &_p,const unsigned char &_id);
    void callEnd(CCNode *_node,void *data);
};

class CKAirplane;
class CKBombs:public CKBombBase
{
private:
    typedef bool (CKBombs::*func)(Sbomb*);
    
    CKCubeArray* m_array;
    CKAirplane* m_airplane;
   // CKAirplane *airplane;
    list< Sbomb* > list_bomb;
    list< Sbomb* >::iterator it;
    
    int count_free_bomb;
    
    Sbomb* touch_bomb;
    Sbomb* accele_bomb;
    func m_func;
    CKBonus *m_bonus;
    BombEffect *m_effect;
    std::list<CCNode *> bonus;
    CCPoint toPoint;
    CCPoint point_touch;
    std::list<CCPoint> target_point_list;
    std::list<CCPoint>::iterator target_it;
    CCPoint target_position;
    bool is_parabola;
    CCSprite * tmp_sprite;
    bool create_new_bomb,firs_bomb,is_touches_bomb;
    CCPoint direct,direct2;
    unsigned char count_kaskad_bomb,need_bombs,delete_kaskad_bomb;//bomb 130
    bool collision_kaskad_bomb,hit_kaskad_bomb,collision_parabola;
    float touch_length;//touch bomb
    unsigned char count_destroy_cube;//lazer 50
    unsigned char curent_kaskad_type;
    float bomb_speed;//150 accellator
    bool is_ground;
    bool start_accele;
    //CCSprite* m_explos;
   // CCAnimation * anim;
    float gun_posX;
    bool stop_update;
    bool is_airplane_bomb;
    bool bomb_blink;
    int count_spec_bombs;
    void createTrackMoved(const CCPoint& _p,const CCPoint& _end);
   // void addBonusToList(Sbomb* _t);
    Sbomb* last_create_bomb;
    CCMotionStreak *streak;
public:
    Sbomb* getTouchBomb();
    CKBombs();
    virtual ~CKBombs();
    virtual bool create(const CCPoint& _p,const unsigned char &_id = 0);
    void createMini(const CCPoint& _p,const CCPoint& _end,const unsigned char &_id);
    virtual const int update(const float &_dt);
    virtual void deleteBomb(int _t,Sbomb* _bomb);
    virtual void deleteBomb(int _t){};
    void hideNode(CCNode* _sender);
    void deleteAllActiveBombs();
    bool init();
    void restart();
    int getCountCreateSpecBomb();
    void createDestroyEffect(CCNode * _batch = NULL);
    void hideAllDestroyEffect();
    //void showDestroyEffect(const CCPoint &_p);
    //void updateDestroyEffect();
    //void endDestroyEffect(CCNode *, void *_data);
    
    bool isActiveBomb();
    void copy(const CKBombs*_bomb);
    void createNewBomb(bool _t);
    void setIsAirplaneBomb(bool _airplane = true);
    bool isNewBomb();
    void touchAutoFind(const CCPoint &pos);
    bool touchTap();
    void setCubeArray(CKCubeArray* _array);
    void setBonus(CKBonus *_bonus);
    void setAirplane(CKAirplane *_airplane);
    int groundUpdate(Sbomb* _bomb);
    int updateDistance(const float &_dt);
    int updateCollision(const float &_dt);
    void updatePosition(const float &_dt);
    void didAccelerate(CCAcceleration* pAccelerationValue);
    list<CCNode*>* getBonusCheck();
    int checkCollision(Sbomb* _bomb);
    func getFuncById(const unsigned char &_id = 0);
    CCPoint findMaxHeight(Sbomb* _curent_bomb);
    
    bool collision11(Sbomb* _bomb);
    bool collision21(Sbomb* _bomb);
    bool collision31(Sbomb* _bomb);
    bool collision41(Sbomb* _bomb);
    bool collision51(Sbomb* _bomb);
    bool collision61(Sbomb* _bomb);
    bool collision71(Sbomb* _bomb);
    bool collision81(Sbomb* _bomb);
    bool collision85(Sbomb* _bomb);
    bool collision111(Sbomb* _bomb);
    bool collision121(Sbomb* _bomb);
    bool collision131(Sbomb* _bomb);
    bool collision141(Sbomb* _bomb);
    bool collision161(Sbomb* _bomb);
    bool collisionKamikadze(Sbomb* _bomb);
};

#endif /* defined(__CandyKadze__CKBomb__) */
