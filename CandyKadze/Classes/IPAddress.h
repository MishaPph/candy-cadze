//
//  IPAddress.h
//  g15
//
//  Created by PR1.Stigol on 26.04.13.
//
//

#ifndef __g15__IPAddress__
#define __g15__IPAddress__

#include <iostream>

#define MAXADDRS	32

class IPAddresses
{
    
    char *if_names[MAXADDRS];
    char *ip_names[MAXADDRS];
    char *hw_addrs[MAXADDRS];
    unsigned long ip_addrs[MAXADDRS];

    void InitAddresses();
    void FreeAddresses();
    void GetIPAddresses();
    void GetHWAddresses();
    
    public:
    static std::string getWH();
};
// Function prototypes



#endif /* defined(__g15__IPAddress__) */
