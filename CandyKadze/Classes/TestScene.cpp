//
//  TestScene.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 20.05.13.
//
//

#include "TestScene.h"
#include "CKAirplane.h"
#include "TBSpriteMask.h"

#define ground_height 32
#define BOX_SIZE BOX_SIZE_IPHONE
CCScene* TestScene::scene()
{
    // 'scene' is an autorelease object
    CCScene *scene = CCScene::create();
    //  CKHelper::Instance().m_loading->setEnableActive(true);
    // add layer as a child to scene
    CCLayer* layer = new TestScene();
    CCLOG("scene:: scene %d layer %d ",scene,layer);
    if(layer->init())
    {
        scene->addChild(layer);
        
        layer->setTouchEnabled(true);
        layer->release();
    };
    return scene;
};

TestScene::~TestScene()
{
    CCLOG("~TestScene:: destroy");

    
    
};

#pragma mark - INIT

TestScene::TestScene()
{
    CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic("menu.mp3", true);
//    b_win = false;
//    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(CK::loadImage("a_InventoryShop.plist").c_str());
//    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(CK::loadImage("a_TutorialPause.plist").c_str());
//    CCSpriteFrameCache::sharedSpriteFrameCache()->addSpriteFramesWithFile(CK::loadImage("a_Bomb.plist").c_str());
    //CCDirector::sharedDirector()->setContentScaleFactor(1.0);
    

    
    //CCDirector::sharedDirector()->setContentScaleFactor(2.0);
    //m_gui->get()->setPosition(m_sprite->getPosition());
    
//    CCSize size = CCDirector::sharedDirector()->getWinSize();
//    CCSprite *sprite = CCSprite::create("gray_mask.png");
//    sprite->setScaleX(size.width/sprite->getContentSize().width);
//    sprite->setScaleY(size.height/sprite->getContentSize().height);
//    sprite->setPosition( ccp(size.width/2, size.height/2) );
//    this->addChild(sprite, 0);
//    TBSpriteMask *tb = TBSpriteMask::create("a_Bomb.png");
//    tb->setPosition( ccp(size.width/2, size.height/2) );
//    tb->buildMaskWithTexture(CCTextureCache::sharedTextureCache()->addImage("cloud.png"));
//    // add the sprite as a child to this layer
//    this->addChild(tb, 0);
};

void TestScene::ccTouchesBegan(cocos2d::CCSet* touches, cocos2d::CCEvent* event)
{
    

};

void TestScene::draw()
{

};

void TestScene::update(float _dt)
{

       // pos_x  = 0.0;
};