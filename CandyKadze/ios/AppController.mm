//
//  CandyKadzeAppController.mm
//  CandyKadze
//
//  Created by PR1.Stigol on 18.09.12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import "AppController.h"
#import "cocos2d.h"
#import "EAGLView.h"
#include "CKGameScene.h"
#include "CKGameScene90.h"
#import "AppDelegate.h"
#import "GAITracker.h"
#import <Crashlytics/Crashlytics.h>
#import <iAd/iAd.h>

#import <Parse/Parse.h>

#define kFILENAME @"data.plist"

//using namespace CocosDenshion;


@implementation AppController

@synthesize window = _window;;
@synthesize rootViewController = viewController;


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    
    [FBAppCall handleOpenURL:url sourceApplication:sourceApplication fallbackHandler:^(FBAppCall *call) {
        
        if (call.appLinkData && call.appLinkData.targetURL) {
            [[NSNotificationCenter defaultCenter] postNotificationName:APP_HANDLED_URL object:call.appLinkData.targetURL];
        }
        
    }];
    //return [PFFacebookUtils handleOpenURL:url];
    //return [PFFacebookUtils handleOpenURL:url];
    return YES;
}

//- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
//    return [PFFacebookUtils handleOpenURL:url];
//}
#pragma mark -
#pragma mark Application lifecycle

// cocos2d application instance
static AppDelegate s_sharedApplication;


void uncaughtExceptionHandle(NSException *exception)
{
        [Flurry logError:@"Uncaught Exception"
                 message:[exception name]
                   exception:exception];
};


- (void)didClickInterstitial:(NSString *)location
{
     CK::StatisticLog::Instance().setLink(Link_ChartboostIntercitial);
}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {


   self.window = [[[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]]autorelease];
  //  [self.window setBackgroundColor:[UIColor whiteColor]];
    EAGLView *__glView = [EAGLView viewWithFrame: [self.window bounds]
                                     pixelFormat: kEAGLColorFormatRGBA8
                                     depthFormat: GL_DEPTH_COMPONENT24_OES
                              preserveBackbuffer: NO
                                      sharegroup: nil
                                   multiSampling: NO
                                 numberOfSamples:0 ];



    //[__glView setBackgroundColor:[UIColor whiteColor]];
    self.rootViewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    
    [__glView setMultipleTouchEnabled:true];
    
    [self.rootViewController setView:__glView];

    if( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        [self.window addSubview: self.rootViewController.view];
    }
    else//ios 6.0
    {
        [self.window setRootViewController:self.rootViewController];
    };
    
    self.rootViewController.view.hidden = NO;
    
    [self.window makeKeyAndVisible];
    
    [Crashlytics startWithAPIKey:[NSString stringWithFormat:@"%s",crashlytics_API_key]];
    
    [[UIApplication sharedApplication] setStatusBarHidden: YES];
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

#if ENABLE_EVERYPLAY
    [Everyplay setClientId:[NSString stringWithFormat:@"%s",EVERYPLAY_CLIENTID] clientSecret:[NSString stringWithFormat:@"%s",EVERYPLAY_CLIENTSECRET] redirectURI:@"https://m.everyplay.com/auth"];
    [Everyplay initWithDelegate:self andParentViewController:self.rootViewController];
#endif
    
    
    [Parse setApplicationId:[NSString stringWithUTF8String:PARSE_APP_ID]
                  clientKey:[NSString stringWithUTF8String:PARSE_CLIENT_KEY]];
    
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    [PFTwitterUtils initializeWithConsumerKey:[NSString stringWithUTF8String:TWEET_CONSUMER_KEY]
                               consumerSecret:[NSString stringWithUTF8String:TWEET_CONSUMER_SECRET]];
    
#if ENABLE_CHARTBOOST
    [Chartboost sharedChartboost].appId = [NSString stringWithFormat:@"%s",CHARTBOOST_APPID];
    [Chartboost sharedChartboost].appSignature = [NSString stringWithFormat:@"%s",CHARTBOOST_SIGNATURE];
    
    [[Chartboost sharedChartboost] startSession];
   // [[Chartboost sharedChartboost] cacheInterstitial:@"Pause"];
    [[Chartboost sharedChartboost] cacheInterstitial];
    [[Chartboost sharedChartboost] cacheMoreApps];
    [Chartboost sharedChartboost].delegate = self;
#endif
    
    cocos2d::CCApplication::sharedApplication()->run();
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillTerminate:)
                                                 name:UIApplicationWillTerminateNotification
                                               object:self];
    
//    [[NSNotificationCenter defaultCenter]
//     addObserver:self
//     selector:@selector(applicationWillResign)
//     name:UIApplicationWillResignActiveNotification
//     object:NULL];
    
    return YES;
};

- (void)everyplayShown {
    NSLog(@"everyplayShown");
    cocos2d::CCDirector::sharedDirector()->pause();
}

- (void)everyplayHidden {
    NSLog(@"everyplayHidden");
    cocos2d::CCDirector::sharedDirector()->resume();
}
- (void) applicationWillResign{
    
    NSLog(@"About to lose focus");
}
- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    NSLog(@"applicationWillResignActive");
    cocos2d::CCDirector::sharedDirector()->pause();

    cocos2d::CCNode* pScene = cocos2d::CCDirector::sharedDirector()->getRunningScene()->getChildByTag(CK::LAYERID_GAMESCENE);

    if(pScene)
    {
        if(CKHelper::Instance().getWorldNumber() == 0)
            dynamic_cast<CKGameScene90 *>(pScene)->setPause();
        else
            dynamic_cast<CKGameSceneNew *>(pScene)->setPause();
    };
    
    ObjCCalls::sendEventGA("exit_lvl","lvl_exit","exit_lvl",1);//GA Фіксуємо тоді, коли в ігровій сцені користувач переводить аплікацію в бекграунд режим (наприклад натискає механічну кнопку на девайсі і виходить в операційну систему)
};

-(void)pauseDirector
{
    cocos2d::CCNode* pScene = cocos2d::CCDirector::sharedDirector()->getRunningScene()->getChildByTag(CK::LAYERID_GAMESCENE);
    if(pScene)
    {
        if(CKHelper::Instance().getWorldNumber() == 0)
            dynamic_cast<CKGameScene90 *>(pScene)->setPause();
        else
            dynamic_cast<CKGameSceneNew *>(pScene)->setPause();
    };
    
    ObjCCalls::sendEventGA("exit_lvl","lvl_exit","exit_lvl",1);//GA Фіксуємо тоді, коли в ігровій сцені користувач переводить аплікацію в бекграунд режим (наприклад натискає механічну кнопку на девайсі і виходить в операційну систему)
    
    cocos2d::CCDirector::sharedDirector()->pause();
};

- (void)mailComplite
{
    cocos2d::CCNode* pLayer = cocos2d::CCDirector::sharedDirector()->getRunningScene()->getChildByTag(CK::LAYERID_MAIN);
    if(pLayer)
    {
        dynamic_cast<CKMainMenuScene *>(pLayer)->mailCallBack(NULL,NULL);
    };
};

-(void)resumDirector
{
    cocos2d::CCDirector::sharedDirector()->resume();
};

- (void) pause
{
    cocos2d::CCNode* pScene = cocos2d::CCDirector::sharedDirector()->getRunningScene()->getChildByTag(CK::LAYERID_GAMESCENE);
    if(pScene)
    {
        if(CKHelper::Instance().getWorldNumber() == 0)
            dynamic_cast<CKGameScene90 *>(pScene)->setPause();
        else
            dynamic_cast<CKGameSceneNew *>(pScene)->setPause();
    };

}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
   // [self.session handleDidBecomeActive];
    NSLog(@"applicationDidBecomeActive");

    [FBAppCall handleDidBecomeActive];
#if ENABLE_EVERYPLAY    
    [[[Everyplay  sharedInstance] capture] pauseRecording];
#endif

    cocos2d::CCDirector::sharedDirector()->resume();
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    NSLog(@"applicationDidEnterBackground");
    
    cocos2d::CCApplication::sharedApplication()->applicationDidEnterBackground();
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    NSLog(@"applicationWillEnterForeground");
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    cocos2d::CCApplication::sharedApplication()->applicationWillEnterForeground();
}

- (void)applicationWillTerminate:(UIApplication *)application {
    NSLog(@"applicationWillTerminate");
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
    
//    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//    NSString *documentsDirectory = [paths objectAtIndex:0];
//    NSString *filePathAndDirectory = [documentsDirectory stringByAppendingPathComponent:@"terrminate.txt"];
//    
//    NSString *sring = [NSString stringWithFormat:@"applicationWillTerminate"];
//    [sring writeToFile:filePathAndDirectory atomically:true encoding:NSUTF8StringEncoding error:nil];
    
#if ENABLE_EVERYPLAY
    [[[Everyplay  sharedInstance] capture] stopRecording];
#endif
    CKFileOptions::Instance().save();
    [[FBSession activeSession] close];
};

- (NSUInteger) supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
};

- (BOOL) shouldAutorotate {
    return YES;
};

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape( interfaceOrientation );
};

#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
    if(!cocos2d::CCDirector::sharedDirector()->getRunningScene())
    {
        return;
    }
    cocos2d::CCDirector::sharedDirector()->purgeCachedData();
}

- (BOOL)shouldRequestInterstitialsInFirstSession
{
    return NO;
}

- (void)dealloc {
    [_window release];
    [super dealloc];
};

@end

