//
//  CandyKadzeAppController.h
//  CandyKadze
//
//  Created by PR1.Stigol on 18.09.12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <iAd/iAd.h>
#import <FacebookSDK/FacebookSDK.h>
#import <Accounts/Accounts.h>
#import <Twitter/Twitter.h>
#import <AWSRuntime/AWSRuntime.h>
#import <AWSS3/AWSS3.h>
//#import <AWSPersistence/AWSPersistence.h>

#import "GAITrackedViewController.h"
#import "GADBannerView.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>
#import <GameKit/GameKit.h>
#import <Social/SLComposeViewController.h>


@class PingObject;

@class Reachability;

@interface RootViewController : GAITrackedViewController <ADBannerViewDelegate,GADBannerViewDelegate,MFMailComposeViewControllerDelegate,MFMessageComposeViewControllerDelegate,UINavigationControllerDelegate,GKLeaderboardViewControllerDelegate,AmazonServiceRequestDelegate>
{
    BOOL loadBannerGAD;
    BOOL loadBanneriAd;
    BOOL show_banner;
    BOOL playerIsAuthenticated;
    Reachability *internetReach;
    
    Reachability* internetReachable;
    Reachability* hostReachable;
    BOOL active_ping;
    
    MFMessageComposeViewController *picker;
    
    BOOL connection_opened;
    BOOL internetActive;
    BOOL hostActive;
}
- (void) showLeaderboard;
-(void) checkNetworkStatus:(NSNotification *)notice;

//-(bool) initAmazonS3Client;
//- (BOOL)processDowloadBanner;
- (bool) isActiveInternet;
-(bool) hostIsReachable;
- (bool) updateInternetStatus:(Reachability*) curReach;
- (bool) isActiveHostName:(NSString *) hostName;
- (void) createBanner;
- (void) deleteBanner;
- (void) showBanner;
- (void) hideBanner;

- (bool)showiAd;
- (void)hideiAd;

- (bool)showGAD;
- (void)hideGAD;

- (void)createiAd;
- (void) createGAD;

- (void)sendMail:(NSString *) _sybject message:(NSString *)_message image:(NSString *)_image;
- (void) sendMessage:(NSString *) _message;
//- (void) sendTweet:(NSString *) _message image:(NSString *) _image url:(NSString *) _url;
//- (void) followTweet;
//- (void) processUploadStaticJson;
//- (void) processUploadCKJson;
//- (bool) downloadTestFlightAppVersion;

//@property (nonatomic, retain) AmazonS3Client *s3;
//@property (nonatomic,assign) BOOL iAdVisible;

@property (nonatomic, retain) ADBannerView *iadView;
@property (nonatomic, retain) GADBannerView *adMobAd;
//@property (nonatomic,assign) BOOL GADVisible;
//@property (nonatomic, retain) AmazonS3Client *s3_feed;
@end
