//
//  ObjCCalls.m
//  CandyKadze
//
//  Created by PR1.Stigol on 06.11.12.
//
//

#include "CKFacebookHelper.h"

#include "CKLanguage.h"
#include "CKTableScore.h"
#import <FacebookSDK/FacebookSDK.h>
#include "CK.h"
#include "CKHelper.h"
#import "AppController.h"

#pragma mark - CKFACEBOOK_HELPER
static CKFacebookHelper *m_FacebookHelper = NULL;
CKFacebookHelper::CKFacebookHelper()
{
    
};

CKFacebookHelper& CKFacebookHelper::Instance()
{
    if(!m_FacebookHelper)
    {
        m_FacebookHelper = new CKFacebookHelper;
    }
    return *m_FacebookHelper;
}
void CKFacebookHelper::setNextCallFuncName(const char *_func)
{
    sprintf(next_func_name, "%s",_func);
}

void CKFacebookHelper::FB_CreateNewSession()
{
    NSLog(@"FB_CreateNewSession");
    
    FBSession* session = [[FBSession alloc] init];
    [FBSession setActiveSession: session];
    
}

void CKFacebookHelper::FB_TryLogin()
{
    NSArray *permissions = [[NSArray alloc] initWithObjects:@"email",nil];
    // Attempt to open the session. If the session is not open, show the user the Facebook login UX
    [FBSession openActiveSessionWithPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceEveryone allowLoginUI:false completionHandler:^(FBSession *session,
                                                                                                                                                           FBSessionState status,
                                                                                                                                                           NSError *error)
     {
         // Did something go wrong during login? I.e. did the user cancel?
         if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateCreatedOpening) {
             // If so, just send them round the loop again
             [[FBSession activeSession] closeAndClearTokenInformation];
             [FBSession setActiveSession:session];
         }
         else
         {
             if ([FBSession activeSession].isOpen) {
                 FB_Customize();
             }
         }
     }];
    [permissions release];
};

void CKFacebookHelper::FB_Login()
{
    NSArray *permissions = [[NSArray alloc] initWithObjects:
                            @"publish_actions",@"user_groups",
                            nil];
    
    // Attempt to open the session. If the session is not open, show the user the Facebook login UX
    [FBSession openActiveSessionWithPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceEveryone allowLoginUI:true completionHandler:^(FBSession *session,
                                                                                                                                                          FBSessionState status,
                                                                                                                                                          NSError *error)
     {
         // Did something go wrong during login? I.e. did the user cancel?
         if (status == FBSessionStateClosedLoginFailed || status == FBSessionStateCreatedOpening) {
             
             // If so, just send them round the loop again
             [[FBSession activeSession] closeAndClearTokenInformation];
             
             [FBSession setActiveSession:nil];
             FB_CreateNewSession();
         }
         else
         {
             if ([FBSession activeSession].isOpen) {
                 FB_Customize();
             }
         }
     }];
};


void CKFacebookHelper::FB_Customize()
{
    //[FBSession setActiveSession:[FBSession activeSession]];
    NSLog(@"FB_Customize");
    FB_getUserInfo();
    // Start the facebook request
    [[FBRequest requestForMe] startWithCompletionHandler:
     ^(FBRequestConnection *connection, NSDictionary<FBGraphUser> *result, NSError *error)
     {
         // Did everything come back okay with no errors?
         if (!error && result)
         {
             // If so we can extract out the player's Facebook ID and first name
             NSLog(@"ObjCCalls::FB_Customize");
             //sprintf(m_nsstrUserName,"%s",[result.first_name UTF8String]);
             if(strcmp(next_func_name, "FB_SendBrag") == 0)
             {
                 FB_SendBrag(0,0,0,true);
             }
             else if(strcmp(next_func_name, "FB_Challenge") == 0)
             {
                 FB_Challenge(0,0,0,true);
             }
             else if(strcmp(next_func_name, "FB_goToPageLikeOurPage") == 0)
             {
                 FB_goToPageLikeOurPage();
             }
             else if(strcmp(next_func_name, "FB_goToPageLikeAnyPage") == 0)
             {
                 FB_goToPageLikeAnyPage();
             }
             else if(strcmp(next_func_name, "FB_PostImage") == 0)
             {
                 FB_PostImage(next_func_parametr,next_func_filename);
             }
             else if(strcmp(next_func_name, "FB_LoadFriendsList") == 0)
             {
                 FB_LoadFriendsList();
             }
         }
     }];
};

void CKFacebookHelper::FB_LoadUserAvatar(const char *_id)
{
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    
    NSString *dataFile = [docsDir stringByAppendingPathComponent: [NSString stringWithFormat:@"%s.jpg",_id]];
    
    int resolution = 32;
    if(CCDirector::sharedDirector()->getWinSize().width == 1024)
        resolution = 64;
    
    NSString *resourceAddress = [[NSString alloc] initWithFormat:@"https://graph.facebook.com/%s/picture?width=%d&height=%d", _id,resolution,resolution];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:resourceAddress]];
    
    [resourceAddress release];
    NSOperationQueue *queue = [[[NSOperationQueue alloc] init]autorelease];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if (error ||  !data || data.length == 0){
             
         }else{
             
             UIImage *image = [[UIImage alloc]initWithData:data];
             [UIImageJPEGRepresentation(image, 1.0) writeToFile:dataFile atomically:YES];
             [image release];
             
         };
     }];
    
    
};

void CKFacebookHelper::FB_getUserInfoSL()
{
    if( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
        return;
    
    ACAccountStore *accountStore = [[[ACAccountStore alloc] init]autorelease];
    ACAccountType *facebookAccountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    NSArray * m_array = [accountStore accountsWithAccountType:facebookAccountType];
    if(m_array != NULL)
    {
        NSDictionary *tempDict2 = [[[NSMutableDictionary alloc] initWithDictionary: [m_array dictionaryWithValuesForKeys:[NSArray arrayWithObject:@"properties"]]]autorelease];
        NSDictionary *temp3 = [tempDict2 objectForKey:@"properties"];
        
        // NSLog(@"ObjCCalls::FB_LoadFriendsListSL tempDict3 %@ %@",temp3,[temp3 valueForKey:@"uid"]);
        
        NSArray *tempUserName = [temp3 valueForKey:@"fullname"];
        NSArray *tempUserID = [temp3 valueForKey:@"uid"];
        
        for (NSNumber *str in tempUserID) {
            NSLog(@"FB_getUserInfoSL::tempUserID %@",str);
            CKHelper::Instance().FacebookID = [[str stringValue] UTF8String];
        };
        for (NSString *str in tempUserName) {
            NSLog(@"FB_getUserInfoSL::tempUserName %@",str);
            CKHelper::Instance().FACEBOOK_USERNAME = [str UTF8String];
        };
        CKFileInfo::Instance().save();
    };
};

void CKFacebookHelper::FB_getUserInfo()
{
    NSLog(@"FB_getUserInfo");
    [[FBRequest requestForGraphPath:@"me?fields=id,name"]
     startWithCompletionHandler:^(FBRequestConnection *connection,NSDictionary *result,NSError *error)
     {
         // If we received a result with no errors...
         if (!error && result)
         {
             // Get the result
             NSString *resultData = [result objectForKey:@"id"];
             NSString *user_name = [result objectForKey:@"name"];
             CKHelper::Instance().FACEBOOK_USERNAME = [user_name UTF8String];
             CKHelper::Instance().FacebookID = [resultData UTF8String];
             
             CKFileInfo::Instance().save();
             
             NSLog(@"fields=id %@ FACEBOOK_USERID %s name %s",resultData,CKHelper::Instance().FacebookID.c_str(),CKHelper::Instance().FACEBOOK_USERNAME.c_str());
         }
     }];
};

void CKFacebookHelper::FB_ProcessIncomingURL(const char * _targetURL)
{
    // Process the incoming url and see if it's of value...
    NSURL *targetURL = [NSString stringWithUTF8String:_targetURL];
    NSRange range = [targetURL.query rangeOfString:@"notif" options:NSCaseInsensitiveSearch];
    
    // If the url's query contains 'notif', we know it's coming from a notification - let's process it
    if(targetURL.query && range.location != NSNotFound)
    {
        // Yes the incoming URL was a notification
        FB_ProcessIncomingRequest(_targetURL);
    }
    
    range = [targetURL.path rangeOfString:@"challenge_brag" options:NSCaseInsensitiveSearch];
    
    // If the url's path contains 'challenge_brag', we know it comes from a feed post
    if(targetURL.path && range.location != NSNotFound)
    {
        // Yes the incoming URL was a notification
        FB_ProcessIncomingFeed(_targetURL);
    }
    
}

void CKFacebookHelper::FB_ProcessIncomingRequest(const char* _targetURL)
{
    // Extract the notification id
    NSURL *targetURL = [NSString stringWithUTF8String:_targetURL];
    NSArray *pairs = [targetURL.query componentsSeparatedByString:@"&"];
    NSMutableDictionary *queryParams = [[NSMutableDictionary alloc] init];
    for (NSString *pair in pairs)
    {
        NSArray *kv = [pair componentsSeparatedByString:@"="];
        NSString *val = [[kv objectAtIndex:1]
                         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [queryParams setObject:val forKey:[kv objectAtIndex:0]];
        
    }
    
    NSString *requestIDsString = [queryParams objectForKey:@"request_ids"];
    NSArray *requestIDs = [requestIDsString componentsSeparatedByString:@","];
    
    FBRequest *req = [[FBRequest alloc] initWithSession:[FBSession activeSession] graphPath:[requestIDs objectAtIndex:0]];
    
    [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         if (!error)
         {
             
             if ([result objectForKey:@"from"])
             {
                 // NSString *from = [[result objectForKey:@"from"] objectForKey:@"name"];
                 // NSString *id = [[result objectForKey:@"from"] objectForKey:@"id"];
                 
                 //  StartGame(true, true, from, id);
             }
         }
         
     }];
    
}
void CKFacebookHelper::FB_ProcessIncomingFeed(const char* _targetURL)
{
    NSURL *targetURL = [NSString stringWithUTF8String:_targetURL];
    // Here we process an incoming link that has launched the app via a feed post
    
    // Here is extract out the FBID component at the end of the brag, so 'challenge_brag_123456' becomes just 123456
    NSString* val = [[targetURL.path componentsSeparatedByString:@"challenge_brag_"] lastObject];
    
    FBRequest *req = [[FBRequest alloc] initWithSession:[FBSession activeSession] graphPath:val];
    
    // With the FBID extracted, we have enough information to go ahead and request the user's profile picture
    // But we also need their name, so here we make a request to http://graph.facebook.com/USER_ID to get their basic information
    [req startWithCompletionHandler:^(FBRequestConnection *connection, id result, NSError *error)
     {
         // If the result came back okay with no errors...
         if (result && !error)
         {
             
             //  NSString *from = [result objectForKey:@"first_name"];
             
             // We can start the game,
             //  StartGame(true, true, from, val);
             
         }
         
     }];
};

void CKFacebookHelper::FB_RequestWritePermissions()
{
    // We need to request write permissions from Facebook
    static bool bHaveRequestedPublishPermissions = false;
    
    if (!bHaveRequestedPublishPermissions)
    {
        NSArray *permissions = [[NSArray alloc] initWithObjects:
                                @"publish_actions",@"user_groups", nil];
        
        [[FBSession activeSession] requestNewPublishPermissions:permissions defaultAudience:FBSessionDefaultAudienceFriends completionHandler:^(FBSession *session, NSError *error) {
            NSLog(@"Reauthorized with publish permissions.");
        }];
        
        bHaveRequestedPublishPermissions = true;
    }
};

void CKFacebookHelper::FB_PostImage(const char *_msg, const char *_image_name)
{
    
    sprintf(next_func_name,"FB_PostImage");
    
    char img[255];
    char msg[255];
    
    if(_msg)
    {
        sprintf(next_func_filename,"%s",_image_name);
        sprintf(next_func_parametr,"%s",_msg);
        
        if(![FBSession activeSession].isOpen)
        {
            FB_Login();
            return;
        };
        
        sprintf(msg,"%s",_msg);
        sprintf(img,"%s",_image_name);
    }
    else
    {
        sprintf(msg,"%s",next_func_parametr);
        sprintf(img,"%s",next_func_filename);
    }
    // FB_RequestWritePermissions();
    CCLOG("FB_PostImage %s %s",msg,img);
    sprintf(next_func_name,"");
    
    UIImage *m_image = [UIImage imageWithContentsOfFile:[NSString stringWithUTF8String:img]];
    NSData * image = UIImageJPEGRepresentation(m_image,1.0);
    if(!image)
    {
        CCLOG("non image");
        return;
    };
    //FB_RequestWritePermissions();
    
    if(ObjCCalls::getDeviceVersion() >= 6.0)
    {
        AppController *appDelegate = [[UIApplication sharedApplication]delegate];
        [FBDialogs presentOSIntegratedShareDialogModallyFrom:[appDelegate rootViewController] initialText:[NSString stringWithUTF8String:msg] image:m_image url:nil handler:^(FBOSIntegratedShareDialogResult result, NSError *error) {
            if (error && [error code] == 7)
            {
                return;
            }
            
            if (error)
            {
                // [[appDelegate rootViewController] showAlert:[[appDelegate rootViewController] checkErrorMessage:error]];
            }
            else if (result == FBNativeDialogResultSucceeded)
            {
                // [[appDelegate rootViewController] showAlert:@"Posted successfully."];
            }
        }];
        //return;
    }
    else
    {
        NSMutableDictionary * params = [[NSMutableDictionary alloc] initWithObjectsAndKeys:
                                        //@"post", @"type",
                                        image ,@"picture",
                                        [NSString stringWithUTF8String:msg], @"message",
                                        nil];
        
        [FBRequestConnection startWithGraphPath:@"me/photos"
                                     parameters:params
                                     HTTPMethod:@"POST"
                              completionHandler:^(FBRequestConnection *connection,
                                                  id result,
                                                  NSError *error)
         {
             NSString *alertText;
             if (error) {
                 alertText = [NSString stringWithFormat:
                              @"error: domain = %@, code = %d",
                              error.domain, error.code];
             } else {
                 alertText = [NSString stringWithFormat:
                              @"Posted action, id: %@",
                              [result objectForKey:@"id"]];
             }
             //         // Show the result in an alert
             [[[UIAlertView alloc] initWithTitle:@"Result"
                                         message:@"Achivement posted successfully"
                                        delegate:nil
                               cancelButtonTitle:@"OK!"
                               otherButtonTitles:nil] show];
         }];
    }
    
}
void CKFacebookHelper::FB_SendScore(const int nScore)
{
    // Make sure we have write permissions
    FB_RequestWritePermissions();
    
    NSMutableDictionary* params =   [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     [NSString stringWithFormat:@"%d", nScore], @"score",
                                     nil];
    
    NSLog(@"Fetching current score");
    
    // Get the score, and only send the updated score if it's highter
    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"%s/scores", CKHelper::Instance().FACEBOOK_USERNAME.c_str()] parameters:params HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        
        if (result && !error) {
            
            int nCurrentScore = [[[[result objectForKey:@"data"] objectAtIndex:0] objectForKey:@"score"] intValue];
            
            NSLog(@"Current score is %d", nCurrentScore);
            
            if (nScore > nCurrentScore) {
                
                NSLog(@"Posting new score of %d", nScore);
                
                [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"%s/scores", CKHelper::Instance().FACEBOOK_USERNAME.c_str()] parameters:params HTTPMethod:@"POST" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
                    
                    NSLog(@"Score posted");
                }];
            }
            else {
                NSLog(@"Existing score is higher - not posting new score");
            }
        }
        
    }];
    
    // Send our custom OG
    // FB_SendOG();
};


void CKFacebookHelper::FB_loadFriendsWithArray(void *_arr)
{
    CCLOG("FB_loadFriendsWithArray");
    NSArray* resultData = static_cast<NSArray *>(_arr);
    NSMutableArray *deviceFilteredFriends = [[NSMutableArray alloc] init];
    // Check we have some friends. If the player doesn't have any friends, they probably need to put down the demo app anyway, and go outside...
    if ([resultData count] > 0)
    {
        // Loop through the friends returned
        for (NSDictionary *friendObject in resultData)
        {
            // Check if devices info available
            if ([friendObject objectForKey:@"devices"])
            {
                // Yep, we know what devices this friend has.. let's extract them
                NSArray *deviceData = [friendObject objectForKey:@"devices"];
                
                // Loop through the list of devices this friend has...
                for (NSDictionary *deviceObject in deviceData)
                {
                    // Check if there is a device match, in this case we're looking for iOS
                    if ([@"iOS" isEqualToString: [deviceObject objectForKey:@"os"]])
                    {
                        // If there is a match, add it to the list - this friend has an iPhone or iPad. Hurrah!
                        [deviceFilteredFriends addObject: [friendObject objectForKey:@"id"]];
                        break;
                    }
                }
            }
        }
    }
    
    //NSMutableArray *tempDict = [[NSMutableArray alloc] init];
    CKTableFacebook::Instance().clearFriends();
    // If at least one of our suggested friends had an iOS device...
    if ([deviceFilteredFriends count] > 0)
    {
        //NSMutableArray * m_arr = [[NSMutableArray alloc]init];
        for (NSString *friendObject in deviceFilteredFriends)
        {
            NSLog(@"friends ID%@",friendObject);
            //[tempDict addObject: friendObject];
            CKTableFacebook::Instance().addFriend([friendObject UTF8String]);
        };
    };
    
    [deviceFilteredFriends release];
};

void CKFacebookHelper::FB_LoadFriendsListOld()
{
    CCLOG("FB_LoadFriendsListOld");
    // Okay, we're going to filter our friends by their device, we're looking for friends with an iPhone or iPad
    sprintf(next_func_name,"");
    
    // We request a list of our friends' names and devices
    [[FBRequest requestForGraphPath:@"me/friends?fields=name,devices"]
     startWithCompletionHandler:^(FBRequestConnection *connection,NSDictionary *result,NSError *error)
     {
         //NSLog(@"FB_LoadFriendsListOld:: result %@",result);
         // If we received a result with no errors...
         if (!error && result)
         {
             //sprintf(next_func_name," ");
             // Get the result
             NSArray *resultData = [result objectForKey:@"data"];
             FB_loadFriendsWithArray(resultData);
             runCallBack(CK::Callback_State_FB_Complite);
         }
         else
         {
             runCallBack(CK::Callback_State_FB_Error);
         }
     }];
};

void CKFacebookHelper::FB_LoadFriendsList()
{
    CCLOG("FB_LoadFriendsList");
    
    if(ObjCCalls::getDeviceVersion() < 6.0)
    {
        if(FB_isOpenSession())
        {
            FB_LoadFriendsListOld();
        }
        else
        {
            runCallBack(CK::Callback_State_Error);
        }
        return;
    }
    
    //    FB_RequestWritePermissions();
    
    ACAccountStore *accountStore = [[[ACAccountStore alloc] init] autorelease];
    
    ACAccountType *facebookAccountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierFacebook];
    if(!facebookAccountType)
    {
        CCLOG("ObjCCalls::FB_LoadFriendsListSL bad facebookAccountType ");
        return;
    }
    
    NSArray * m_array = [accountStore accountsWithAccountType:facebookAccountType];
    if(m_array == NULL)
    {
        if(FB_isOpenSession())
        {
            FB_LoadFriendsListOld();
        }
        else
        {
            runCallBack(CK::Callback_State_Error);
        }
        return;
    }
    else
    {
        NSDictionary *tempDict2 = [[[NSMutableDictionary alloc] initWithDictionary: [m_array dictionaryWithValuesForKeys:[NSArray arrayWithObject:@"properties"]]] autorelease];
        NSDictionary *temp3 = [tempDict2 objectForKey:@"properties"];
        
        NSArray *tempUserName = [temp3 valueForKey:@"fullname"];
        NSArray *tempUserID = [temp3 valueForKey:@"uid"];
        
        for (NSNumber *str in tempUserID) {
            NSLog(@"FB_LoadFriendsList::tempUserID %@",str);
            CKHelper::Instance().FacebookID = [[str stringValue] UTF8String];
        };
        for (NSString *str in tempUserName) {
            NSLog(@"FB_LoadFriendsList::tempUserName %@",str);
            CKHelper::Instance().FACEBOOK_USERNAME = [str UTF8String];
        };
        CKFileInfo::Instance().save();
    }
    
    
    NSDictionary *options = @{@"ACFacebookAppIdKey" : [NSString stringWithFormat:@"%lld",kuFBAppID ],
                              @"ACFacebookPermissionsKey" : @[@"email"],
                              @"ACFacebookAudienceKey" : ACFacebookAudienceEveryone}; // Needed only when write permissions are requested
    
    [accountStore requestAccessToAccountsWithType:facebookAccountType options:options
                                       completion:^(BOOL granted, NSError *error) {
                                           CCLOG("ObjCCalls::FB_LoadFriendsListSL granted %d",granted);
                                           if (granted)
                                           {
                                               NSArray * m_array = [accountStore accountsWithAccountType:facebookAccountType];
                                               
                                               
                                               NSLog(@"granted %@",error);
                                               // Grab the initial Twitter account to tweet from.
                                               NSArray *accounts = [accountStore accountsWithAccountType:facebookAccountType];
                                               ACAccount *facebookAccount = [accounts lastObject];
                                               
                                               SLRequest *fb_request = [SLRequest requestForServiceType:SLServiceTypeFacebook
                                                                                          requestMethod:SLRequestMethodGET
                                                                                                    URL:[NSURL URLWithString:@"https://graph.facebook.com/me/friends"]
                                                                                             parameters:@{@"fields":@"name,devices"}];
                                               [fb_request setAccount:facebookAccount];
                                               [fb_request performRequestWithHandler:^(NSData* responseData, NSHTTPURLResponse* urlResponse, NSError* error)
                                                {
                                                    //NSLog(@"urlResponse %d",[urlResponse statusCode]);
                                                    if ([urlResponse statusCode] == 200) {
                                                        NSError *jsonParsingError = nil;
                                                        NSArray *publicTimeline = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonParsingError];
                                                        
                                                        if (!publicTimeline) {
                                                            NSLog(@"Error parsing JSON: %@", jsonParsingError);
                                                        } else {
                                                            
                                                            NSArray *resultData = [publicTimeline objectForKey:@"data"];
                                                            FB_loadFriendsWithArray(resultData);
                                                            //[publicTimeline release];
                                                            runCallBack(CK::Callback_State_FB_Complite);
                                                        };
                                                    };
                                                    
                                                }];
                                           }
                                           else
                                           {
                                               if(FB_isOpenSession())
                                               {
                                                   FB_LoadFriendsListOld();
                                               }
                                               else
                                               {
                                                    runCallBack(CK::Callback_State_FB_Error);
                                               }
                    
                                           }
                                           
                                       }];
};

bool CKFacebookHelper::FB_isOpenSession()
{
    CCLOG("ObjCCalls::FB_isOpenSession %d",[FBSession activeSession].isOpen);
    return [FBSession activeSession].isOpen;
}
void CKFacebookHelper::FB_SendBrag(const int score,const int lvl_num,const int world_num,bool _call_func)
{
    
    sprintf(next_func_name,"FB_SendBrag");
    
    if(!_call_func)
        sprintf(next_func_parametr,"%d;%d;%d",score,lvl_num,world_num);
    
    if(![FBSession activeSession].isOpen)
    {
        FB_Login();
        return;
    };
    FB_RequestWritePermissions();
    
    sprintf(next_func_name,"");
    int _score,_lvl_num,_world_num;
    sscanf(next_func_parametr, "%d;%d;%d", &_score,&_lvl_num,&_world_num);
    
    
    NSString *linkURL = [NSString stringWithFormat:@"%s",STIGOL_SUPPORT_URL];
    
    NSString *name = [NSString stringWithUTF8String:LCZ("Checkout my score at CandyKaze")];
    
    NSString * str1 =[NSString stringWithFormat:[NSString stringWithUTF8String:LCZ("I just completed Level # %d in %s")], _lvl_num, WORLD_NAMES[_world_num]];
    NSString * str2 =[NSString stringWithFormat:[NSString stringWithUTF8String:LCZ("My score at this level is %d")], _score];
    NSString *description = [NSString stringWithFormat:@"%@. %@",str1,str2];
    NSString *picture = @"http://i2.minus.com/iGUJwjwdHs2XI.png";
    NSString *pictureURL = picture;//@"http://i2.minus.com/iGUJwjwdHs2XI.png";
    
    // Prepare the native share dialog parameters
    FBShareDialogParams *shareParams = [[FBShareDialogParams alloc] init];
    
    shareParams.link = [NSURL URLWithString:linkURL];
    shareParams.name = name;
    shareParams.caption= [NSString stringWithFormat:@""];
    shareParams.picture= [NSURL URLWithString:pictureURL];
    shareParams.description = description;
    
    if ([FBDialogs canPresentShareDialogWithParams:shareParams]){
        
        [FBDialogs presentShareDialogWithParams:shareParams
                                    clientState:nil
                                        handler:^(FBAppCall *call, NSDictionary *results, NSError *error) {
                                            if(error) {
                                                NSLog(@"Error publishing story.");
                                            } else if (results[@"completionGesture"] && [results[@"completionGesture"] isEqualToString:@"cancel"]) {
                                                NSLog(@"User canceled story publishing.");
                                            } else {
                                                NSLog(@"Story published.");
                                            }
                                        }];
        
    }else {
        
        // Prepare the web dialog parameters
        NSDictionary *params = @{
                                 @"name" : shareParams.name,
                                 @"caption" : shareParams.caption,
                                 @"description" : shareParams.description,
                                 @"picture" : pictureURL,
                                 @"link" : linkURL
                                 };
        
        // Invoke the dialog
        [FBWebDialogs presentFeedDialogModallyWithSession:nil
                                               parameters:params
                                                  handler:
         ^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
             if (error) {
                 NSLog(@"Error publishing story.");
             } else {
                 if (result == FBWebDialogResultDialogNotCompleted) {
                     NSLog(@"User canceled story publishing.");
                 } else {
                     NSLog(@"Story published.");
                 }
             }}];
    }
}

void CKFacebookHelper::FB_Logout()
{
    // Log out of Facebook and reset our session
    [[FBSession activeSession] closeAndClearTokenInformation];
    [FBSession setActiveSession:nil];
};


//++++++++++++++++++++++++++++++++++++
//FB_
//++++++++++++++++++++++++++++++++
void CKFacebookHelper::FB_Challenge(const int score,const int lvl,const int world_num,bool _call_func)
{
    sprintf(next_func_name,"FB_Challenge");
    
    if(!_call_func)
        sprintf(next_func_parametr,"%d;%d;%d",score,lvl,world_num);
    
    if(![FBSession activeSession].isOpen)
    {
        FB_Login();
        return;
    };
    FB_RequestWritePermissions();
    
    sprintf(next_func_name,"");
    int _score,_lvl_num,_world_num;
    sscanf(next_func_parametr, "%d;%d;%d", &_score,&_lvl_num,&_world_num);
    
    
    
    NSString * msg = [NSString stringWithFormat: [NSString stringWithUTF8String:LCZ("I scored %d points. Level # %d in %s . Can you bit it?")],_score,_lvl_num,WORLD_NAMES[_world_num]];
    // 1. No additional parameters provided - enables generic Multi-friend selector
    NSMutableDictionary* params =   [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                     @"{\"challenge_score\":\"0\"}", @"data",
                                     nil];
    
    FBFrictionlessRecipientCache *friendCache = [[FBFrictionlessRecipientCache alloc] init];
    [friendCache prefetchAndCacheForSession:nil];
    
    [FBWebDialogs presentRequestsDialogModallyWithSession:nil
                                                  message:msg
                                                    title:nil
                                               parameters:params
                                                  handler:^(FBWebDialogResult result, NSURL *resultURL, NSError *error) {
                                                      if (error) {
                                                          // Case A: Error launching the dialog or sending request.
                                                          NSLog(@"Error sending request.");
                                                      } else {
                                                          if (result == FBWebDialogResultDialogNotCompleted) {
                                                              // Case B: User clicked the "x" icon
                                                              NSLog(@"User canceled request.");
                                                          } else {
                                                              NSLog(@"Request Sent.");
                                                          }
                                                      }}
                                              friendCache:friendCache];
    
};



void CKFacebookHelper::FB_goToPageLikeOurPage()
{
    
    sprintf(next_func_name,"FB_goToPageLikeOurPage");
    if(![FBSession activeSession].isOpen)
    {
        FB_Login();
        return;
    };
    sprintf(next_func_name,"");
    
    NSString *msg = [NSString stringWithFormat:@"http://www.facebook.com/%s",FB_LIKE_OUR_ID];
    NSURL *nsUrl = [NSURL URLWithString:msg];
    [[UIApplication sharedApplication] openURL:nsUrl];
};
void CKFacebookHelper::FB_goToPageLikeAnyPage()
{
    
    sprintf(next_func_name,"FB_goToPageLikeAnyPage");
    if(![FBSession activeSession].isOpen)
    {
        FB_Login();
        return;
    };
    sprintf(next_func_name,"");
    
    NSString *msg = [NSString stringWithFormat:@"http://www.facebook.com/%s",FB_LIKE_ANY_ID];
    NSURL *nsUrl = [NSURL URLWithString:msg];
    [[UIApplication sharedApplication] openURL:nsUrl];
};

void CKFacebookHelper::FB_GetLike(cocos2d::CCObject *_selector,cocos2d::SEL_CallFunc _call_func,const char *_id)
{
    [FBRequestConnection startWithGraphPath:[NSString stringWithFormat:@"me/likes?fields=id,likes&target_id=%s",_id] parameters:nil HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error) {
        
        if (result && !error)
        {
            for (NSDictionary *dict in [result objectForKey:@"data"])
            {
                NSString *strId = [dict objectForKey:@"id"];
                if([strId isEqualToString:[NSString stringWithUTF8String:_id]])
                {
                    if(_selector)
                    {
                        (_selector->*_call_func)();
                    }
                }
            }
        };
    }];
};
void CKFacebookHelper::setCallBackFunc(cocos2d::CCNode* _node, cocos2d::SEL_CallFuncND _func_sel)
{
    m_target = _node;
    call_back_func = _func_sel;
}

void CKFacebookHelper::runCallBack(int _d)
{
    if(m_target)
    {
        (m_target->*call_back_func)(NULL,&_d);
    }
}





