//
//  Note.m
//  CandyKadze
//
//  Created by PR1.Stigol on 23.04.13.
//
//

#import "Note.h"

@implementation Note

@synthesize noteContent;
@synthesize data = m_data;
// Called whenever the application reads data from the file system
-(BOOL) loadFromContents:(id)contents
                  ofType:(NSString *)typeName
                   error:(NSError *__autoreleasing *)outError
{
    if ([contents length] > 0) {
        m_data = contents;
    } else {
        // When the note is first created, assign some default content
        self.noteContent = @"Empty";
        m_data = nil;
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);//
    NSString *documentsDirectory = [[paths objectAtIndex:0]stringByAppendingPathComponent:@"/"];
    NSString *myDocsPath =  [documentsDirectory stringByAppendingPathComponent:@"~data.plist"];
    [m_data writeToFile:myDocsPath atomically:YES];
    
    NSLog(@"noteContent::loadFromContents %d",m_data.length);
    return YES;
}

- (id)contentsForType:(NSString *)typeName error:(NSError **)outError 
{
    if ([self.data length] == 0) {
        self.noteContent = @"Empty";
        return [NSData dataWithBytes:[self.noteContent UTF8String]
                              length:[self.noteContent length]];
    }
    
    NSLog(@"contentsForType %d",m_data.length);
    return m_data;
}

-(void) setData:(NSData *) _data
{
    m_data = _data;
    NSLog(@"setData %d",m_data.length);
};

-(NSData *) getData
{
    NSLog(@"getData %d",m_data.length);
    return m_data;
    
};
@end

@implementation NoteSave

@synthesize data = m_data;
// Called whenever the application reads data from the file system
-(BOOL) loadFromContents:(id)contents
                  ofType:(NSString *)typeName
                   error:(NSError *__autoreleasing *)outError
{
    if ([contents length] > 0) {
        m_data = contents;
    } else {
        m_data = nil;
    }
    
    NSLog(@"NoteSave::loadFromContents %d",m_data.length);
    return YES;
}

- (id)contentsForType:(NSString *)typeName error:(NSError **)outError
{
    if ([self.data length] == 0) {
        return [NSData dataWithBytes:[@"Empty" UTF8String]
                              length:[@"Empty" length]];
    }
    
    NSLog(@"contentsForType %d",m_data.length);
    return m_data;
}

-(void) setData:(NSData *) _data
{
    m_data = _data;
    NSLog(@"setData %d",m_data.length);
};

-(NSData *) getData
{
    NSLog(@"getData %d",m_data.length);
    return m_data;
    
};
@end
