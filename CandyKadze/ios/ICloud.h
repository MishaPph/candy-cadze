//
//  ICloud.h
//  CandyKadze
//
//  Created by PR1.Stigol on 25.04.13.
//
//

#import <UIKit/UIKit.h>
#import "Note.h"
@interface ICloud
+ (void)loadDocument;
+ (void)deleteDocument;
@end
