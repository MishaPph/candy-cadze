//
//  ObjCCalls.m
//  CandyKadze
//
//  Created by PR1.Stigol on 06.11.12.
//
//


#import "ICloud.h"
#include "ICloudHelper.h"
#include "CK.h"

#pragma mark - ICLOUD_HELPER
static ICloudHelper *m_ICloudHelper = NULL;
ICloudHelper::ICloudHelper()
{
    
};

ICloudHelper &ICloudHelper::Instance()
{
    if(!m_ICloudHelper)
    {
        m_ICloudHelper = new ICloudHelper;
    }
    return *m_ICloudHelper;
};
void ICloudHelper::deleteIcloudDocument()
{
    //  [ICloud deleteDocument];
};

void ICloudHelper::saveFileToIcloud()
{
    NSURL *ubiq = [[NSFileManager defaultManager] URLForUbiquityContainerIdentifier:nil];
    if(!ubiq)
    {
        return;
    };
    NSURL *ubiquitousPackage = [[ubiq URLByAppendingPathComponent:
                                 @"Documents"] URLByAppendingPathComponent:[NSString stringWithUTF8String:CK::file_data_name]];
    
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *dataFile = [docsDir stringByAppendingPathComponent: [NSString stringWithUTF8String:CK::file_data_name]];
    if (![[NSFileManager defaultManager] fileExistsAtPath: dataFile])
    {
        NSLog(@"file not exist");
        return;
    }
    else
    {
        NSLog(@"file exist");
        NoteSave* doc = [[NoteSave alloc] initWithFileURL:ubiquitousPackage];
        [doc setData:[[[NSData alloc] initWithContentsOfFile:dataFile]retain]];
        [doc saveToURL:ubiquitousPackage
      forSaveOperation:UIDocumentSaveForOverwriting
     completionHandler:^(BOOL success) {
         if (success){
             NSLog(@"Saved for overwriting");
         } else {
             NSLog(@"Not saved for overwriting");
         }
     }];
    }
};

void ICloudHelper::loadWithIcloud()
{
    
    [ICloud loadDocument];
};

void ICloudHelper::replaceDataFileWithIcloud()
{
    NSArray *dirPath_tmp = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachDir = [dirPath_tmp objectAtIndex:0];
    NSString *tmpFile = [cachDir stringByAppendingPathComponent: @"~data.plist"];
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *dataFile = [docsDir stringByAppendingPathComponent: [NSString stringWithUTF8String:CK::file_data_name]];
    NSData* data =[[[NSData alloc] initWithContentsOfFile:tmpFile]autorelease];
    [data writeToFile:dataFile atomically:true];
};

bool ICloudHelper::iCloudBetter()
{
    NSArray *dirPath_tmp = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachDir = [dirPath_tmp objectAtIndex:0];
    NSString *tmpFile = [cachDir stringByAppendingPathComponent: @"~data.plist"];
    NSDictionary *dict_tmp = [[[NSDictionary alloc] initWithContentsOfFile:tmpFile] autorelease];
    if(!dict_tmp)
    {
        NSLog(@"iCloudBetter:: not iCloud tmp data");
        return false;
    };
    
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = [dirPaths objectAtIndex:0];
    NSString *dataFile = [docsDir stringByAppendingPathComponent: [NSString stringWithUTF8String:CK::file_data_name]];
    NSDictionary *dict_data = [[[NSDictionary alloc] initWithContentsOfFile:dataFile] autorelease];
    
    NSDictionary *current_score = [dict_data objectForKey:@"score_record"];
    NSDictionary *icloud_score = [dict_tmp objectForKey:@"score_record"];
    
    NSString * str_data = [current_score valueForKey:@"icloud_score"];
    NSString * str_tmp = [icloud_score valueForKey:@"icloud_score"];
    NSString * version_data = [dict_data valueForKey:@"version"];
    NSString * version_tmp = [dict_data valueForKey:@"version"];
    NSLog(@"iCloudBetter data(%d) tmp(%d)",[str_data intValue],[str_tmp intValue]);
    if([str_data intValue] < [str_tmp intValue] && ([version_data floatValue] == [version_tmp floatValue]))
    {
        return true;
    };
    return false;
};

