//
//  CKParse.h
//  CandyKadze
//
//  Created by PR1.Stigol on 14.06.13.
//
//

typedef void (^RequestProductsCompletionHandler)(BOOL success, NSArray * products);
typedef void (*func)(const char *);
@interface IAPHelper : NSObject
- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers;
- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler;
- (void)buyProduct:(NSString *)product;
- (BOOL)productPurchased:(NSString *)productIdentifier;
- (void)restoreCompletedTransactions;
-(void)setCallbackFunc:(func )_func;
- (void)setArray:(NSArray *)_array;
@end


@interface RageIAPHelper : IAPHelper
+ (RageIAPHelper *)sharedInstance;
@end

