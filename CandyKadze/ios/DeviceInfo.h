//
//  ObjCCalls.h
//  CandyKadze
//
//  Created by PR1.Stigol on 06.11.12.
//
//

#ifndef CandyKadze_DeviceInfo_h
#define CandyKadze_DeviceInfo_h


#import <mach/mach.h>
#import <mach/mach_host.h>
#include "CK.h"
#include <iostream>
#include <list>

namespace CK {
    class DeviceInfo
    {
        DeviceInfo();
        void genUserID();
        const float VERSION = 1.0f;
        void * collect_data;
        char * MD5String(const char *_str);
        const char * getGameCenterName();
        void collectSocialID();
    public:
        ~DeviceInfo();
        const char *getFileName();
        const char *getInfoJson();
        void setUserID(const char *_user);
        void showFile(const char*_file_name);
        const char *getUserID();
        void collectAllInfo();
        void deleteOldInfo();
        void saveCollectToFile();
        void saveJson();
        //void sendAllInfoFileToAmazon();
        void showLastSaveFile();
        void showInfo();
        bool createDirectory();
        static DeviceInfo &Instance();
    };
}


#endif
