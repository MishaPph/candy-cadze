//
//  CKNTPTime.cpp
//  CandyKadze
//
//  Created by PR1.Stigol on 04.06.13.
//
//

#include "CKNTPTime.h"
static int cash_time = 0;
pthread_t CKNTPTime::m_bg_thread;
void* func_getTimeWithServer(void *_data)
{
    char   hostname[255];
    strcpy(hostname, "66.162.15.65");
    int portno = 123;     //NTP is port 123
    int maxlen = 512;        //check our buffers
    int i;          // misc var i
    unsigned char msg[48]={010,0,0,0,0,0,0,0,0};    // the packet we send
    unsigned long  buf[512]; // the buffer we get back
    //struct in_addr ipaddr;        //
    struct protoent *proto;     //
    struct sockaddr_in server_addr;
    //struct sockaddr_in adr_clnt;
    int m_socket = -1;  // socket
    //int tmit;   // the time -- This is a time_t sort of
    
    //use Socket;
    //
    //#we use the system call to open a UDP socket
    //socket(SOCKET, PF_INET, SOCK_DGRAM, getprotobyname("udp")) or die "socket: $!";
    
    //        memset( &server_addr, 0, sizeof( server_addr ));
    //        server_addr.sin_family = AF_INET;
    //        server_addr.sin_addr.s_addr = inet_addr(hostname);
    //        server_addr.sin_port = htons(portno);
    
    
    proto = getprotobyname("udp");
    //Create a UDP socket to use:
    m_socket = socket(PF_INET, SOCK_DGRAM, proto->p_proto);
    if(m_socket == -1) {
        perror("");
        pthread_exit(0);
    }
    
    //  struct sockaddr_in adr_inet; // AF_INET
    
    memset(&server_addr,0,sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(portno);
    server_addr.sin_addr.s_addr = inet_addr(hostname);
    
    if ( server_addr.sin_addr.s_addr == INADDR_NONE ) {
        printf("bad address.\n");
    }
    //        int len_inet = sizeof(server_addr);
    //        int z = bind(m_socket, (struct sockaddr *)&server_addr, len_inet);
    //        if ( z == -1 ) {
    //            printf("error bind()");
    //            return -1;
    //        }
    //        printf("socket = %d\n",m_socket);
    
    //printf("ipaddr (in hex): %x\n",server_addr.sin_addr);
    
    /*
     * build a message.  Our message is all zeros except for a one in the
     * protocol version field
     * msg[] in binary is 00 001 000 00000000
     * it should be a total of 48 bytes long
     */
    
    // send the data
    printf("sending data..\n");
    i = sendto(m_socket,msg,sizeof(msg),0,(struct sockaddr *)&server_addr,sizeof(server_addr));
    printf("sending data %d\n" , i);
    if(i == -1 )
    {
        printf("Error sendto %d \n",i);
        pthread_exit(0);
    }
    else
    {
        printf("not error %d \n",m_socket);
        // if(m_socket != 28)
        //     return -1;
    }
    // struct sockaddr_in sockaddr4;
    // socklen_t sockaddr4len = sizeof(sockaddr4);
    
    memset( &buf, 0, maxlen);
    int result = -1;
    result = recv(m_socket, buf, 512, 0);
    printf("result %d\n",result);
    i = close(m_socket);
    printf("close sockopt %d \n",i);
    printf("result %d\n",result);

    
    printf("recvfr: %d \n",i);
    unsigned long *buf2 = (unsigned long *)buf;
    cash_time = ntohl((time_t)buf2[10]);    //# get transmit time
    printf("tmit cash_time = %d\n",cash_time);
    pthread_exit(0);
};

int CKNTPTime::timeInd()
{
    pthread_cancel(m_bg_thread);
    pthread_create(&m_bg_thread, NULL, func_getTimeWithServer, NULL);
    return cash_time;
};

std::string CKNTPTime::ntpdate()
{
    signed int tmit = cash_time;
    printf("ntpdate:: cash_time = %d %d\n",cash_time,tmit);
    if (tmit == -1 || tmit == 0) {
        tmit = timeInd();
    }
    if (tmit == -1 || tmit == 0) {
        return "";
    }
    printf("tmit=%d\n",tmit);
    
    /*
     * Convert time to unix standard time NTP is number of seconds since 0000
     * UT on 1 January 1900 unix time is seconds since 0000 UT on 1 January
     * 1970 There has been a trend to add a 2 leap seconds every 3 years.
     * Leap seconds are only an issue the last second of the month in June and
     * December if you don't try to set the clock then it can be ignored but
     * this is importaint to people who coordinate times with GPS clock sources.
     */
    
    tmit-= 2208988800U;
    
    /* use unix library function to show me the local time (it takes care
     * of timezone issues for both north and south of the equator and places
     * that do Summer time/ Daylight savings time.
     */
    
    int i = time(0);
    
    time_t tim = tmit;
    struct tm  tstruct;
    char       m_buf[80];
    tstruct = * std::localtime(&tim);
    
    strftime(m_buf, 255, "%y%m%d", &tstruct); //local time
    printf("System time is %d seconds off ndp time %s\n",(i - tmit),m_buf);
    return m_buf;
};
int CKNTPTime::timeDiff()
{
    
    int tmit = (cash_time == 0)?timeInd():cash_time;
    
    printf("tmit=%d\n",tmit);
    
    tmit -= 2208988800U;
    
    int  i = time(0);
    
    printf("CKNTPTime::timeDiff=%d\n",(i - tmit));
    return (i - tmit);
}