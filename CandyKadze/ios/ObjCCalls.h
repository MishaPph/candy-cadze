//
//  ObjCCalls.h
//  CandyKadze
//
//  Created by PR1.Stigol on 06.11.12.
//
//

#ifndef CandyKadze_ObjCCalls_h
#define CandyKadze_ObjCCalls_h


#import <mach/mach.h>
#import <mach/mach_host.h>
#include "CK.h"
#include <iostream>
#include <list>

class CKAmazonHelper;

typedef void (*funccallback)( void);
class ObjCCalls
{
private:
    
    static bool low_memory;
    static bool low_cpu;
    static cocos2d::CCNode* callback_object;
    static cocos2d::SEL_CallFuncND callback_func;
   // static cocos2d::SEL_CallFuncND complite_load_func;
public:
  //  static int firs_msg;
  //  friend CKFacebookHelper;
    static void closeSession();
    static void postMessage(const char* _message,const char *img_name = NULL);
    static void sendScreen(const char* _message);
    static bool isOpenSession();
    static void checkFB();
    static bool isActiveConnection();
    static void changeSession();
    static void post();
    static void postFile();
    static std::string getDeviceName();
    //static void sendToAmazon();

    //static void sendToGameStatistic();
    
    static std::string getUserName();
    static void initStatic();
    static void load();
    static void optimezeALLFileInDir();
    static void optimazePlist(const char *file_name);
   // static unsigned int getFreeMemmory();
    static float getVersion();
    static float getDeviceVersion();
    static void goToFasebook();
    static void goToURL(const char *_url);
    static void setLikes();
    static bool isExistFile();

    static void sendFeedback(const char* _message);
    static void checkSendStatistic();
    static void logPageView(bool _GA = false,const char* name = "");
    static void sendCustomGA(int _index,const char* _dimension,int _value);
    static void sendEventGA(const char* _category,const char* _action,const char* _label,const int &_value);
    static void sendEventFlurry(const char *_eventname,bool _t,const char* _format,...);
    static std::string getNTP();
    static void recognizingModel();
    static int isLowCPU();
    static int isLittleMemory();
    static bool showiAd(bool _show);
    static std::string getDeviceLang();
    static void showChartboost(const char *_int = NULL);
    static std::string getIPAddress();
    
    static bool showBanner(int _num = 0);
    static void hideBanner(bool _destroy);
    static void createBanner(int _num = 0);
    
    //static bool downloadBanner();
    static void setMultiTouch(bool _enable);


    //social network
    static void leaderboardSendAllScore(int _total,int best_hiscore,int _world,int world_score,int _ordinary_using,int _special_using,int _accuracy_rating,int _blocks_destroyed,int _plane_flights);
    static void leaderboardSendScore(const int&_total,const char *_leaderID);
    static void showLeaderboard();
    
    static void sendMail(const int _score, const int _lvl_num,const int &_world,const char *image);
    static void tellFriend();
   
    static void sendMessage(const int _lvl_num,const int &_world,const int &_score);
    static void goToGift();
    static void everyplayStartCapture(int _lenght_capture = -1,int _delay = 0);
    static void everyplayShow();
    static void everyplayStopRecording();
    static void everyplayPlayLastVideo(const int &score,const int _lvl_num,const int _world_num);
    static void everyplayPauseRecording();
    static void everyplayResumeRecording();
    
    static void facebookLogin();
    
    static void facebookSendBrag(const char *_world_name,const int _lvl_num,const int _score);
    
    static void setCallBack(cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel);
    //static void setCallBackCompliteLoad(cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel);
    //static void setCallBackErrorLoad(cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel);

    //IAPHelper
    static void IAPHelper_buy(const char *_id);
    static bool IAPHelper_isbuy(const char *_id);
    static void IAPHelper_callback(const char *_id);
    static void IAPHelper_restore();
    //static bool IAPHelper_productPurchased(const char *_id);
    
    static const char* needUpdateApp();
    //static void amazonCallBack(void *_data);
    static void showFile(const char*_file_name);
};

class CKAmazonHelper
{
    CKAmazonHelper();
    const char *testflight_file_name = "candykadze_testflight_current_version.plist";
    const char *stigol_banner = "stigol-banners";
    const char *bucket_statistics = "candykaze-statistics";
    const char *bucket_device_info = "players-data";
    static void *client;
public:
    ~CKAmazonHelper();
    static  CKAmazonHelper &Instance();
    bool needUpdateTestFlightAppVersion(cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel);
    bool downloadBanner();
    void uploadPlayerInfo();
    void uploadStatisticsJson();
};

#endif
