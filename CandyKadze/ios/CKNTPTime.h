//
//  CKNTPTime.h
//  CandyKadze
//
//  Created by PR1.Stigol on 04.06.13.
//
//

#ifndef __CandyKadze__CKNTPTime__
#define __CandyKadze__CKNTPTime__


#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <time.h>
#include <string.h>
#include <iostream>
/**
 * A Network Time Protocol Message.
 * From [RFC 5905](http://tools.ietf.org/html/rfc5905).
 */
//http://www.ccplusplus.com/2011/09/recvfrom-example-c-code.html

//static signed long cash_time = 0;
class CKNTPTime
{
public:
    
    static int timeInd();
    static pthread_t m_bg_thread;
public:
    //    static int cash_time;
    static std::string ntpdate();
    static int timeDiff();
};

#endif
