//
//  CKParse.m
//  CandyKadze
//
//  Created by PR1.Stigol on 14.06.13.
//
//

#import "CKIAPHelper.h"
#import "AppController.h"

#import <StoreKit/StoreKit.h>

// Добавьте в начало файла
NSString *const IAPHelperProductPurchasedNotification = @"IAPHelperProductPurchasedNotification";
static RageIAPHelper * sharedInstance = NULL;
//UIKIT_EXTERN NSString *const IAPHelperProductPurchasedNotification;
// 2
@interface IAPHelper () <SKProductsRequestDelegate,SKPaymentTransactionObserver>
@end

@implementation IAPHelper {
    // 3
    SKProductsRequest * _productsRequest;
    NSArray *m_products;
    func m_func;
    // 4
    RequestProductsCompletionHandler _completionHandler;
    NSSet * _productIdentifiers;
    NSMutableSet * _purchasedProductIdentifiers;
    

    // Добавляем декларации двух новых методов
    
}
- (BOOL)productPurchased:(NSString *)productIdentifier {
    return [_purchasedProductIdentifiers containsObject:productIdentifier];
}

-(void)setCallbackFunc:(func )_func
{
    m_func = _func;
};

- (void)buyProduct:(NSString *)product {
    if([SKPaymentQueue canMakePayments])
    {
        
        AppController *appDelegate = [[UIApplication sharedApplication]delegate];
        if(![[appDelegate rootViewController] isActiveInternet])
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection error!" message:@"Purchase failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [alert release];
            m_func("end");
            return;
        }
        
        NSLog(@"Покупаем %@...", product);
         
        bool b_product = false;
        for (SKProduct * prod in m_products) {
            NSLog(@"m_products %@ %d %d",prod.productIdentifier,[prod.productIdentifier isEqualToString:product],strcmp([prod.productIdentifier UTF8String], [product UTF8String]));
            if([product isEqualToString:prod.productIdentifier])
            {
                b_product = true;
                SKPayment *payment = [SKPayment paymentWithProduct:prod];
                [[SKPaymentQueue defaultQueue] addPayment:payment];
                break;
            }
        };
        if(!b_product) // com.cc.coins.buy10000 not in array
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Buy error!" message:@"Unknown purchase id" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [alert release];
            m_func("end");
//            SKPayment * payment = [SKPayment paymentWithProductIdentifier:product];
//            [[SKPaymentQueue defaultQueue] addPayment:payment];
        }
            //m_func("end");

    }else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"In-App Purchases are Disabled"
                                                        message:@"The ads could not be removed because In-App Purchases are disabled on this device. To change this, go to your device Settings, choose General, then Restrictions, and enable In-App Purchases."
                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        alert.delegate = self;
        [alert show];
        [alert release];
        m_func("end");
    }
};

- (void)dealloc
{
    [[SKPaymentQueue defaultQueue] removeTransactionObserver:self];
    
    [super dealloc];
    
};

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{    
    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState)
        {
                 //m_func("end");
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    };
}
- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error
{
    NSLog(@"restoreCompletedTransactionsFailedWithError");
    m_func("Error");
}
- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"completeTransaction...");
    
    [self provideContentForProductIdentifier:transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"restoreTransaction...");
    
    [self provideContentForProductIdentifier:transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
     m_func("end");
    NSLog(@"failedTransaction...");
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection error!" message:@"Purchase failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
        NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
    }
    
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
}

// Добавьте новый метод
- (void)provideContentForProductIdentifier:(NSString *)productIdentifier {
    
    [_purchasedProductIdentifiers addObject:productIdentifier];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification object:productIdentifier userInfo:nil];
    
    
    m_func( [productIdentifier UTF8String]);
    NSLog(@"provideContentForProductIdentifier %@",productIdentifier);
}

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers {
    
    if ((self = [super init])) {
        
        // Сохраняем идентификаторы продуктов
        _productIdentifiers = productIdentifiers;
        
        // Проверяем ранее купленные продукты
        _purchasedProductIdentifiers = [NSMutableSet new];
        for (NSString * productIdentifier in _productIdentifiers) {
            BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
            if (productPurchased) {
                [_purchasedProductIdentifiers addObject:productIdentifier];
                
                NSLog(@"Ранее купленный: %@", productIdentifier);
            } else {
                NSLog(@"Не купленный: %@", productIdentifier);
            }
            [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
        }
    }
    return self;
}

- (void)restoreCompletedTransactions {
    AppController *appDelegate = [[UIApplication sharedApplication]delegate];
    if(![[appDelegate rootViewController] isActiveInternet])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection error!" message:@"Purchase failed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
        m_func("end");
        return;
    }
   // [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler {
    NSLog(@"requestProductsWithCompletionHandler");
    // 1
    _completionHandler = [completionHandler copy];
    
    // 2
    _productsRequest = [[[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers]autorelease];
    _productsRequest.delegate = self;
    [_productsRequest start];
    
}
#pragma mark - SKProductsRequestDelegate
- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    NSLog(@"Загруженый список продуктов...");
    _productsRequest = nil;
   
    NSArray * skProducts = response.products;
     
    for (SKProduct * skProduct in skProducts) {
        NSLog(@"Найден продукт: %@ %@ %0.2f",
              skProduct.productIdentifier,
              skProduct.localizedTitle,
              skProduct.price.floatValue);
    }
    
    _completionHandler(YES, skProducts);
    _completionHandler = nil;
};

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    
    NSLog(@"Не смог загрузить список продуктов.");
    _productsRequest = nil;
    
    _completionHandler(NO, nil);
    _completionHandler = nil;
    
}
- (void)setArray:(NSArray *)_array
{
    m_products = nil;
    m_products = _array;
    [m_products retain];
}
@end
@implementation RageIAPHelper
+ (RageIAPHelper *)sharedInstance {
    if(sharedInstance == NULL)
    {
        static dispatch_once_t once;
        dispatch_once(&once, ^{
            NSSet * productIdentifiers = [NSSet setWithObjects:
                                          @"com.cc.kids_mode",
                                          @"com.cc.ads_removing",
                                          @"com.cc.coins.x2",@"com.cc.coins.x3",@"com.cc.coins.x5",
                                          @"com.cc.coins.buy10000",@"com.cc.coins.buy20000",@"com.cc.coins.buy50000",
                                          @"com.cc.coins.buy80000",@"com.cc.coins.buy100000",@"com.cc.coins.buy200000",
                                          @"com.cc.coins.buy300000",@"com.cc.coins.buy500000",@"com.cc.coins.buy1million",
                                          nil];
            sharedInstance = [[self alloc] initWithProductIdentifiers:productIdentifiers];
            
            [sharedInstance requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
                if (success) {
                    
                    NSLog(@"products %@",products);
                    [sharedInstance setArray:products];
                }
            }];
            
        });
    }
    return sharedInstance;
}
@end

