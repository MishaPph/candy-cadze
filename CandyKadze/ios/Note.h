//
//  Note.h
//  CandyKadze
//
//  Created by PR1.Stigol on 23.04.13.
//
//

#import <UIKit/UIKit.h>

@interface Note : UIDocument

@property (strong) NSString * noteContent;
@property (strong) NSData * data;
-(void) setData:(NSData *) _data;
-(NSData *) getData;
@end

@interface NoteSave : UIDocument

@property (strong) NSData * data;
-(void) setData:(NSData *) _data;
-(NSData *) getData;
@end