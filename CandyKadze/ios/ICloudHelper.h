//
//  ObjCCalls.h
//  CandyKadze
//
//  Created by PR1.Stigol on 31.07.13.
//
//

#ifndef CandyKadze_ICloudHelper_h
#define CandyKadze_ICloudHelper_h

class ICloudHelper
{
    ICloudHelper();
public:
    static ICloudHelper& Instance();
    //Icloud
    void deleteIcloudDocument();
    void saveFileToIcloud();
    void loadWithIcloud();
    bool iCloudBetter();
    void replaceDataFileWithIcloud();
};


#endif
