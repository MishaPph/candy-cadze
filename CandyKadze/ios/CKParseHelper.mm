//
//  ObjCCalls.m
//  CandyKadze
//
//  Created by PR1.Stigol on 06.11.12.
//
//

#include "CKParseHelper.h"
#import <Parse/Parse.h>
#include "CK.h"
#include "CKTableScore.h"
#include "CKHelper.h"
#include "IPAddress.h"
#include "CKFacebookHelper.h"

#pragma mark  - PARSE_OBJECT
static CKParseHelper *m_ParseObject = NULL;
CKParseHelper::CKParseHelper()
{
    target = NULL;
};
CKParseHelper &CKParseHelper::Instance()
{
    if(!m_ParseObject)
    {
        m_ParseObject = new CKParseHelper;
    };
    return *m_ParseObject;
}

void CKParseHelper::parseOpenLvl(void *_array_user)
{
#if DISABLE_PARSE
    return;
#endif
    NSMutableArray *names = (NSMutableArray *)_array_user;
    CKTableFacebook::Instance().clearLvlRecord();
    PFQuery *query = [PFQuery queryWithClassName:[NSString stringWithUTF8String:"Progress"]];

    [query whereKey:@"FaceBookID" containedIn:names];    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if(objects.count != 0)
            {
                NSLog(@"ObjCCalls::parseLoadOpenLvl %d", objects.count);
                for (PFObject *object in objects) {
                    NSString *name = [object objectForKey:@"FaceBookID"];
                    NSNumber *world_pixel = [object objectForKey:@"Pixel"];
                    NSNumber *world_caramel = [object objectForKey:@"Caramel"];
                    NSNumber *world_choko = [object objectForKey:@"Choco"];
                    NSNumber *world_ice = [object objectForKey:@"Ice"];
                    NSNumber *world_waffel = [object objectForKey:@"Waffles"];
                    CKTableFacebook::Instance().addLvlRecorld([name UTF8String], [world_pixel intValue], [world_caramel intValue], [world_choko intValue], [world_ice intValue], [world_waffel intValue]);
                    CKFacebookHelper::Instance().FB_LoadUserAvatar([name UTF8String]);
                };
                
                if(target)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        int d = 3;
                        (target->*call_func)(NULL,&d);
                    });
                    //target = NULL;
                }
            };
        };
        [query cancel];
        // NSLog(@"PFQuery *query %d",[query retainCount]);
        //[query release];
        //dispatch_sync(dispatch_get_main_queue(), ^{ [query release]; });//end block
    }];
}
void CKParseHelper::parseScore(void *_array_user,int _type_filter)
{
#if DISABLE_PARSE
    return
#endif
    NSArray *names = (NSArray *)_array_user;
    //  CKTableScore::Instance().incThread();
    
    PFQuery *query = [PFQuery queryWithClassName:[NSString stringWithUTF8String:PARSE_CLASSES]];
    if(_type_filter == 1)
    {
        CKTableFacebook::Instance().incThread();
        [query whereKey:@"facebook_id" containedIn:names];
    }
    else if(_type_filter == 2)
    {
        CKTableTwitter::Instance().incThread();
        [query whereKey:@"twitter_id" containedIn:names];
    }
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if(objects.count != 0)
            {
                NSLog(@"parseLoadScore::Successfully retrieved %d scores.update", objects.count);
                for (PFObject *object in objects) {
                    NSString *_id;
                    NSString *_name;
                    if(_type_filter == 1)
                    {
                        _id= [object objectForKey:@"facebook_id"];
                        _name = [object objectForKey:@"facebook_name"];
                    }
                    else if(_type_filter == 2)
                    {
                        _id = [object objectForKey:@"twitter_id"];
                        _name = [object objectForKey:@"twitter_name"];
                    }
                    NSNumber *total= [object objectForKey:@"Total_scores"];
                    NSNumber *hiscore= [object objectForKey:@"High_Scores_All"];
                    NSNumber *ordinary_using= [object objectForKey:@"ordinary_using"];
                    NSNumber *special_using= [object objectForKey:@"special_using"];
                    NSNumber *accuracy_rating= [object objectForKey:@"accuracy_rating"];
                    NSNumber *blocks_destroyed= [object objectForKey:@"blocks_destroyed"];
                    NSNumber *plane_flights= [object objectForKey:@"plane_flights"];
                    
                    NSNumber *world_pixel = [object objectForKey:@"High_Scores_Pixel"];
                    NSNumber *world_caramel = [object objectForKey:@"High_Scores_Caramel"];
                    NSNumber *world_choko = [object objectForKey:@"High_Scores_Choco"];
                    NSNumber *world_ice = [object objectForKey:@"High_Scores_Ice"];
                    NSNumber *world_waffel = [object objectForKey:@"High_Scores_Waffles"];
                    if(_type_filter == 1)
                    {
                        CKTableFacebook::Instance().addRecords([_id UTF8String], [_name UTF8String], [total intValue], [hiscore intValue], [ordinary_using intValue], [special_using intValue], [accuracy_rating intValue], [blocks_destroyed intValue], [plane_flights intValue], [world_pixel intValue], [world_caramel intValue], [world_choko intValue], [world_ice intValue], [world_waffel intValue]);
                    }
                    else
                    {
                        CKTableTwitter::Instance().addRecords([_id UTF8String], [_name UTF8String], [total intValue], [hiscore intValue], [ordinary_using intValue], [special_using intValue], [accuracy_rating intValue], [blocks_destroyed intValue], [plane_flights intValue], [world_pixel intValue], [world_caramel intValue], [world_choko intValue], [world_ice intValue], [world_waffel intValue]);
                    };
                    
                };
            };
            
        };
        if(_type_filter == 1)
        {
            CKTableFacebook::Instance().decThread();
        }
        else
        {
            CKTableTwitter::Instance().decThread();
        };
        if(target)
        {
            dispatch_async(dispatch_get_main_queue(), ^{
            int d = _type_filter;
            (target->*call_func)(NULL,&d);
            });
        }
    }];
};


void CKParseHelper::parseSendOpenWorldLvl(int _world_pixel,int _world_caramel,int _world_choco,int _world_ice,int _world_waffel)
{
#if DISABLE_PARSE
    return;
#endif
    if (CKHelper::Instance().FacebookID.length() < 3) {
        return;
    }
    NSString *userID = [NSString  stringWithUTF8String: CKHelper::Instance().FacebookID.c_str()];
    PFQuery *query = [PFQuery queryWithClassName:[NSString stringWithUTF8String:"Progress"]];
    [query whereKey:@"FaceBookID" equalTo:userID];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if(objects.count != 0)
            {
                // NSLog(@"leaderboardSendAllScore::Successfully retrieved %d scores.update", objects.count);
                for (PFObject *object in objects) {
                    // NSLog(@"%@", [object objectForKey:@"score"]);
                    PFObject *gameScore = [PFObject objectWithoutDataWithClassName:[NSString stringWithUTF8String:"Progress"] objectId:object.objectId];
                    [gameScore setObject:[NSString  stringWithString: userID] forKey:@"FaceBookID"];
                    
                    NSNumber *pixel = [object objectForKey:@"Pixel"];
                    if([pixel intValue] < _world_pixel)
                    {
                        [gameScore setObject:[NSNumber numberWithInt:_world_pixel] forKey:@"Pixel"];
                    };
                    
                    pixel = [object objectForKey:@"Caramel"];
                    if([pixel intValue] < _world_caramel)
                    {
                        [gameScore setObject:[NSNumber numberWithInt:_world_caramel] forKey:@"Caramel"];
                    };
                    
                    pixel = [object objectForKey:@"Choco"];
                    if([pixel intValue] < _world_choco)
                    {
                        [gameScore setObject:[NSNumber numberWithInt:_world_choco] forKey:@"Choco"];
                    };
                    pixel = [object objectForKey:@"Ice"];
                    if([pixel intValue] < _world_ice)
                    {
                        [gameScore setObject:[NSNumber numberWithInt:_world_ice] forKey:@"Ice"];
                    };
                    pixel = [object objectForKey:@"Waffles"];
                    if([pixel intValue] < _world_waffel)
                    {
                        [gameScore setObject:[NSNumber numberWithInt:_world_waffel] forKey:@"Waffles"];
                    };
                    
                    [gameScore saveInBackground];
                }
            } else {
                NSLog(@"Successfully retrieved %d scores add.", objects.count);
                PFObject *gameScore = [PFObject objectWithClassName:[NSString stringWithUTF8String:"Progress"]];
                [gameScore setObject:[NSString  stringWithString: userID] forKey:@"FaceBookID"];
                
                [gameScore setObject:[NSNumber numberWithInt:_world_pixel] forKey:@"Pixel"];
                [gameScore setObject:[NSNumber numberWithInt:_world_caramel] forKey:@"Caramel"];
                [gameScore setObject:[NSNumber numberWithInt:_world_choco] forKey:@"Choco"];
                [gameScore setObject:[NSNumber numberWithInt:_world_ice] forKey:@"Ice"];
                [gameScore setObject:[NSNumber numberWithInt:_world_waffel] forKey:@"Waffles"];
                
                [gameScore saveInBackground];
            }
        }
    }];
};
void CKParseHelper::parseSendAllScore(int _total, int best_hiscore,int _world, int world_score,
                                  int _ordinary_using, int _special_using, int _accuracy_rating,int _blocks_destroyed,int _plane_flights)
{
#if DISABLE_PARSE
    return;
#endif
    NSString *userID = [NSString  stringWithUTF8String: IPAddresses::getWH().c_str()];
    PFQuery *query = [PFQuery queryWithClassName:[NSString stringWithUTF8String:PARSE_CLASSES]];
    [query whereKey:@"UserID" equalTo:userID];
    
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            if(objects.count != 0)
            {
                bool save_file = false;
                // NSLog(@"leaderboardSendAllScore::Successfully retrieved %d scores.update", objects.count);
                for (PFObject *object in objects) {
                    // NSLog(@"%@", [object objectForKey:@"score"]);
                    save_file = false;
                    PFObject *gameScore = [PFObject objectWithoutDataWithClassName:[NSString stringWithUTF8String:PARSE_CLASSES] objectId:object.objectId];
                    [gameScore setObject:[NSString  stringWithString: userID] forKey:@"UserID"];
                    
                    if (CKHelper::Instance().TwitterID.length() > 3) {
                        [gameScore setObject:[NSString stringWithUTF8String:CKHelper::Instance().TwitterID.c_str()] forKey:@"twitter_id"];
                        [gameScore setObject:[NSString  stringWithUTF8String: CKHelper::Instance().TWITTER_USERNAME.c_str()] forKey:@"twitter_name"];
                    }else
                    {
                        save_file = true;
                        NSString *twID= [object objectForKey:@"twitter_id"];
                        CKHelper::Instance().TwitterID = [twID UTF8String];
                        NSString *twName= [object objectForKey:@"twitter_name"];
                        CKHelper::Instance().TWITTER_USERNAME = [twName UTF8String];
                        
                    };
                    if (CKHelper::Instance().FacebookID.length() > 3) {
                        [gameScore setObject:[NSString stringWithUTF8String:CKHelper::Instance().FacebookID.c_str()] forKey:@"facebook_id"];
                        [gameScore setObject:[NSString  stringWithUTF8String: CKHelper::Instance().FACEBOOK_USERNAME.c_str()] forKey:@"facebook_name"];
                    }else
                    {
                        save_file = true;
                        NSString *fbID= [object objectForKey:@"facebook_id"];
                        CKHelper::Instance().FacebookID = [fbID UTF8String];
                        NSString *fbName= [object objectForKey:@"facebook_name"];
                        CKHelper::Instance().FACEBOOK_USERNAME = [fbName UTF8String];
                    }
                    if(save_file)
                        CKFileInfo::Instance().save();
                    
                    [gameScore setObject:[NSNumber numberWithInt:_total] forKey:@"Total_scores"];
                    [gameScore setObject:[NSNumber numberWithInt:best_hiscore] forKey:@"High_Scores_All"];
                    
                    switch (_world) {
                        case 0:
                            [gameScore setObject:[NSNumber numberWithInt:world_score] forKey:@"High_Scores_Pixel"];
                            break;
                        case 1:
                            [gameScore setObject:[NSNumber numberWithInt:world_score] forKey:@"High_Scores_Caramel"];
                            break;
                        case 2:
                            [gameScore setObject:[NSNumber numberWithInt:world_score] forKey:@"High_Scores_Choco"];
                            break;
                        case 3:
                            [gameScore setObject:[NSNumber numberWithInt:world_score] forKey:@"High_Scores_Ice"];
                            break;
                        case 4:
                            [gameScore setObject:[NSNumber numberWithInt:world_score] forKey:@"High_Scores_Waffles"];
                            break;
                        default:
                            break;
                    }
                    
                    [gameScore setObject:[NSNumber numberWithInt:_ordinary_using] forKey:@"ordinary_using"];
                    [gameScore setObject:[NSNumber numberWithInt:_special_using] forKey:@"special_using"];
                    [gameScore setObject:[NSNumber numberWithInt:_accuracy_rating] forKey:@"accuracy_rating"];
                    [gameScore setObject:[NSNumber numberWithInt:_blocks_destroyed] forKey:@"blocks_destroyed"];
                    [gameScore setObject:[NSNumber numberWithInt:_plane_flights] forKey:@"plane_flights"];
                    [gameScore saveInBackground];
                }
            } else {
                NSLog(@"Successfully retrieved %d scores add.", objects.count);
                PFObject *gameScore = [PFObject objectWithClassName:[NSString stringWithUTF8String:PARSE_CLASSES]];
                [gameScore setObject:[NSString  stringWithString: userID] forKey:@"UserID"];
                
                [gameScore setObject:[NSString stringWithUTF8String:CKHelper::Instance().FacebookID.c_str()] forKey:@"facebook_id"];
                [gameScore setObject:[NSString  stringWithUTF8String: CKHelper::Instance().FACEBOOK_USERNAME.c_str()] forKey:@"facebook_name"];
                
                [gameScore setObject:[NSString stringWithUTF8String:CKHelper::Instance().TwitterID.c_str()] forKey:@"twitter_id"];
                [gameScore setObject:[NSString  stringWithUTF8String: CKHelper::Instance().TWITTER_USERNAME.c_str()] forKey:@"twitter_name"];
                
                
                [gameScore setObject:[NSNumber numberWithInt:int(_total)] forKey:@"Total_scores"];
                [gameScore setObject:[NSNumber numberWithInt:int(best_hiscore)] forKey:@"High_Scores_All"];
                
                [gameScore setObject:[NSNumber numberWithInt:0] forKey:@"High_Scores_Pixel"];
                [gameScore setObject:[NSNumber numberWithInt:0] forKey:@"High_Scores_Caramel"];
                [gameScore setObject:[NSNumber numberWithInt:0] forKey:@"High_Scores_Choco"];
                [gameScore setObject:[NSNumber numberWithInt:0] forKey:@"High_Scores_Ice"];
                [gameScore setObject:[NSNumber numberWithInt:0] forKey:@"High_Scores_Waffles"];
                
                switch (_world) {
                    case 0:
                        [gameScore setObject:[NSNumber numberWithInt:world_score] forKey:@"High_Scores_Pixel"];
                        break;
                    case 1:
                        [gameScore setObject:[NSNumber numberWithInt:world_score] forKey:@"High_Scores_Caramel"];
                        break;
                    case 2:
                        [gameScore setObject:[NSNumber numberWithInt:world_score] forKey:@"High_Scores_Choco"];
                        break;
                    case 3:
                        [gameScore setObject:[NSNumber numberWithInt:world_score] forKey:@"High_Scores_Ice"];
                        break;
                    case 4:
                        [gameScore setObject:[NSNumber numberWithInt:world_score] forKey:@"High_Scores_Waffles"];
                        break;
                    default:
                        break;
                }
                
                [gameScore setObject:[NSNumber numberWithInt:_ordinary_using] forKey:@"ordinary_using"];
                [gameScore setObject:[NSNumber numberWithInt:_special_using] forKey:@"special_using"];
                [gameScore setObject:[NSNumber numberWithInt:_accuracy_rating] forKey:@"accuracy_rating"];
                [gameScore setObject:[NSNumber numberWithInt:_blocks_destroyed] forKey:@"blocks_destroyed"];
                [gameScore setObject:[NSNumber numberWithInt:_plane_flights] forKey:@"plane_flights"];
                [gameScore saveInBackground];
            }
        }
    }];
};

void CKParseHelper::parseLoadOpenLvl(std::list<std::string> *_list_friend,cocos2d::CCNode* _selector, cocos2d::SEL_CallFuncND _func_sel)
{
    NSMutableArray *tempDict = [[[NSMutableArray alloc] init]autorelease];
    
    std::list<std::string>::iterator it = _list_friend->begin();
    while (it != _list_friend->end()){
        [tempDict addObject: [NSString stringWithUTF8String:(*it).c_str()] ];
        it++;
    }
 
    parseOpenLvl(tempDict);
};


void CKParseHelper::parseLoadScore(std::list<std::string> *_list,int _type_filter,cocos2d::CCNode* _selector, cocos2d::SEL_CallFuncND _func_sel)
{
    CCLOG("ObjCCalls::parseLoadScore");
    NSMutableArray *tempDict = [[NSMutableArray alloc] init];
    
    std::list<std::string>::iterator it = _list->begin();
    while (it != _list->end()){
        [tempDict addObject: [NSString stringWithUTF8String:(*it).c_str()] ];
        it++;
    }
    if(_type_filter == 1)
    {
        [tempDict addObject: [NSString stringWithUTF8String:CKHelper::Instance().FacebookID.c_str()] ];
    }
    else
    {
        [tempDict addObject: [NSString stringWithUTF8String:CKHelper::Instance().TwitterID.c_str()] ];
    }
    target = _selector;
    call_func = _func_sel;
    parseScore(tempDict,_type_filter);
};

void CKParseHelper::setCallbackFunc(cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel)
{
    target = node;
    call_func =_func_sel;
};



