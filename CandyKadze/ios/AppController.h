//
//  CandyKadzeAppController.h
//  CandyKadze
//
//  Created by PR1.Stigol on 18.09.12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//
#import <FacebookSDK/FacebookSDK.h>
#import <UIKit/UIKit.h>
#import "RootViewController.h"
#import <GameKit/GameKit.h>

#import "Flurry.h"
#import "FlurryAds.h"
#import "FlurryAdDelegate.h"
#import "GAI.h"
#import "Note.h"
#import <Everyplay/Everyplay.h>
#include "CKID.h"
#import "Chartboost.h"

static BOOL session_open = NO;
static BOOL twitter_open = NO;
static BOOL internet_open = NO;
static BOOL s3_amazon_created = NO;

#define APP_HANDLED_URL @"APP_HANDLED_URL"
@class RootViewController;

@interface AppController : NSObject <UIAccelerometerDelegate, UIAlertViewDelegate, UITextFieldDelegate,UIApplicationDelegate,EveryplayDelegate,ChartboostDelegate>
{
 RootViewController    *viewController;
}
@property (strong, nonatomic) RootViewController *rootViewController;

//@property (strong, nonatomic) FBSession *session;
- (void)pause;
- (void)resumDirector;
- (void)pauseDirector;
- (void)mailComplite;

@end


