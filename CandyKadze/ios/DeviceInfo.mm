//
//  ObjCCalls.m
//  CandyKadze
//
//  Created by PR1.Stigol on 06.11.12.
//
//

#include "DeviceInfo.h"
#include "CKNTPTime.h"

#import <GameKit/GKLocalPlayer.h>

#import "ios-ntp.h"
#import <sys/utsname.h>

#include <ifaddrs.h>
#include <arpa/inet.h>

#include "CKHelper.h"

#include "CK.h"

#include "AppController.h"

#import <AdSupport/AdSupport.h>
#import <Foundation/Foundation.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/sockio.h>
#include <net/if.h>
#include <errno.h>
#include <net/if_dl.h>
#include <net/ethernet.h>
#import <sys/utsname.h>
#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>

#pragma mark - DEVICE_INFO
using namespace CK;
#define	min(a,b)	((a) < (b) ? (a) : (b))
#define	max(a,b)	((a) > (b) ? (a) : (b))
#define kBuggyASIID             @"00000000-0000-0000-0000-000000000000"



const char *getMACAddress()
{
    int                 mib[6];
    size_t              len;
    char                *buf;
    unsigned char       *ptr;
    struct if_msghdr    *ifm;
    struct sockaddr_dl  *sdl;
    
    mib[0] = CTL_NET;
    mib[1] = AF_ROUTE;
    mib[2] = 0;
    mib[3] = AF_LINK;
    mib[4] = NET_RT_IFLIST;
    
    if ((mib[5] = if_nametoindex("en0")) == 0) {
        printf("Error: if_nametoindex error\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 1\n");
        return NULL;
    }
    
    if ((buf = (char *)malloc(len)) == NULL) {
        printf("Error: Memory allocation error\n");
        return NULL;
    }
    
    if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 2\n");
        free(buf); // Thanks, Remy "Psy" Demerest
        return NULL;
    }
    
    ifm = (struct if_msghdr *)buf;
    sdl = (struct sockaddr_dl *)(ifm + 1);
    ptr = (unsigned char *)LLADDR(sdl);
    NSString *outstring = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X", *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
    
    free(buf);
    return [outstring UTF8String];
}

static DeviceInfo *m_device_info = NULL;
char * DeviceInfo::MD5String(const char *_str)
{
    const char *cstr = _str;
    unsigned char result[16];
    CC_MD5(cstr, strlen(cstr), result);
    char *str = new char[255];
    sprintf(str, "%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",result[0], result[1], result[2], result[3],
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]);
    CCLOG("MD5String %s",str);
    return str;
};
//const char *DeviceInfo::user_id = NULL;
CK::DeviceInfo::DeviceInfo()
{
    //sprintf(user_id, "");
    collect_data = NULL;
};
void DeviceInfo::setUserID(const char *_user)
{
//    sprintf(user_id, "%s",_user);
    CKHelper::Instance().UserID = _user;
}
const char *DeviceInfo::getUserID()
{
    return CKHelper::Instance().UserID.c_str();
};

CK::DeviceInfo::~DeviceInfo()
{
    delete m_device_info;
    m_device_info = NULL;
};
CK::DeviceInfo& DeviceInfo::Instance()
{
    if(!m_device_info)
    {
        m_device_info = new DeviceInfo;
    }
    return *m_device_info;
};

void DeviceInfo::genUserID()
{
    NSLocale *theLocale = [NSLocale currentLocale];
    [NSUserDefaults resetStandardUserDefaults];
    NSArray *languages = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppleLanguages"];
    NSString *currentLanguage = [languages objectAtIndex:0];
    NSString *language_region = [theLocale objectForKey: NSLocaleCountryCode];
    
    NSString *new_hash_str =  [NSString stringWithFormat:@"%@%@%@%@%@%@%s",[[UIDevice currentDevice] model],[[UIDevice currentDevice] localizedModel],[[UIDevice currentDevice] name],[[UIDevice currentDevice] systemVersion],language_region,currentLanguage,getGameCenterName()];
   
   // setUserID(MD5String([new_hash_str UTF8String]));
    char *str = MD5String([new_hash_str UTF8String]);
    NSLog(@"DeviceInfo::genUserID %s",str);
//    NSString* tmp =  [NSString stringWithFormat:@"%s",MD5String([new_hash_str UTF8String])];
    CKHelper::Instance().UserID = str;
    delete str;
};

const char * DeviceInfo::getGameCenterName()
{
    GKLocalPlayer *lp = [GKLocalPlayer localPlayer];
    if (lp.authenticated) {
        NSLog(@"DeviceInfo::getGameCenterName %@",lp.alias);
        return [lp.alias UTF8String];
    }
    return "";
};

void DeviceInfo::collectAllInfo()
{
    
    NSMutableDictionary * collect_dict = (NSMutableDictionary *)collect_data;
    if(!collect_dict)
        collect_dict = [[NSMutableDictionary dictionaryWithCapacity:15] retain];
    if(CKHelper::Instance().UserID.length() == 0)
    {
        genUserID();
        //CCLOG("user_id %s",user_id);
    };
    
    //language info
    
    NSLocale *theLocale = [NSLocale currentLocale];
    [NSUserDefaults resetStandardUserDefaults];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLanguage = [languages objectAtIndex:0];

    NSString *userLocale = [[NSLocale currentLocale] localeIdentifier];
    currentLanguage = [userLocale substringToIndex:2];
    
//    NSString * language = [[NSLocale preferredLanguages] objectAtIndex:0];
    
    NSString *language_region = [theLocale objectForKey: NSLocaleCountryCode];
    //+++++++++++++++++++++++++++++++++++
    //time info
    time_t tim = time(0);
    struct tm  tstruct;
    char       data_str[80],time_str[80];
    tstruct = * std::localtime(&tim);
    strftime(data_str, sizeof(data_str), "%y%m%d", &tstruct);
    strftime(time_str, sizeof(time_str), "%H:%M:%S", &tstruct);
    //+++++++++++++++++++++++++++++++++++
    // device id
    NSString * advStr;
    if (NSClassFromString(@"ASIdentifierManager")) {
        NSString * asiID = [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];
        if ([asiID compare:kBuggyASIID] == NSOrderedSame) {
            NSLog(@"Error: This device return buggy advertisingIdentifier.");
            advStr = kBuggyASIID;
        } else {
            NSString *rStr = [NSString stringWithFormat:@"%@",asiID];
            const char *t_str = [rStr UTF8String];
            advStr = [NSString stringWithUTF8String:t_str];
        }
    }
    
    NSString *mac_str = [NSString stringWithUTF8String:getMACAddress()];
    NSString *deviseId = @"";
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
        deviseId = advStr;
    else
    {
        deviseId = mac_str;
    }
    //+++++++++++++++++++++++++++++
    //++++++++++++++++info carriere
    CTTelephonyNetworkInfo *netinfo = [[[CTTelephonyNetworkInfo alloc] init] autorelease];
    CTCarrier *carrier = [netinfo subscriberCellularProvider];
    NSString *carrier_name = [NSString stringWithFormat:@"%@",carrier.mobileNetworkCode];
    NSString *carrier_country = [NSString stringWithFormat:@"%@",carrier.mobileCountryCode];
    //+++++++++++++++++++
    //++++Game center
    NSString *gamecenter_name = [NSString stringWithFormat:@"%s", getGameCenterName()];

    //+++++++
    [[UIDevice currentDevice] setBatteryMonitoringEnabled:true];
    NSString* time_zone = [NSString stringWithFormat:@"%ld",(long)[[NSTimeZone localTimeZone] secondsFromGMT]/3600];
    NSString* battery_lvl = [NSString stringWithFormat: @"%0.2f%%", [[UIDevice currentDevice] batteryLevel] * 100.0f ];
    
    [collect_dict setObject:[[UIDevice currentDevice] model] forKey:@"device_model"];
    [collect_dict setObject:[[UIDevice currentDevice] localizedModel] forKey:@"localized_model"];
    
    [collect_dict setObject:[[UIDevice currentDevice] name] forKey:@"device_itunes_name"];
    [collect_dict setObject:[[UIDevice currentDevice] systemVersion] forKey:@"OS_version"];
    [collect_dict setObject:language_region forKey:@"current_locale"];
    [collect_dict setObject:currentLanguage forKey:@"device_language"];
    [collect_dict setObject:[NSString stringWithUTF8String:data_str] forKey:@"date"];
    [collect_dict setObject:[NSString stringWithUTF8String:time_str] forKey:@"time"];
    
    [collect_dict setObject:deviseId forKey:@"device_ID"];
    [collect_dict setObject:[NSString stringWithFormat:@"%.1f",VERSION] forKey:@"ver"];
    [collect_dict setObject:[NSString stringWithFormat:@"%s",CKHelper::Instance().UserID.c_str()] forKey:@"hash"];
    
    if([carrier_name length] > 0)
        [collect_dict setObject:carrier_name forKey:@"device_carrier_name"];
    if([carrier_country length] > 0)
        [collect_dict setObject:carrier_country forKey:@"device_carrier_country"];
    if([gamecenter_name length] > 0)
        [collect_dict setObject:[NSString stringWithFormat:@"%@",gamecenter_name] forKey:@"user_gamecenter_name"];
//
    [collect_dict setObject:[[NSTimeZone localTimeZone] name] forKey:@"user_timezone_name"];
    [collect_dict setObject:time_zone forKey:@"user_timezone"];
    [collect_dict setObject:[theLocale objectForKey:NSLocaleCurrencyCode] forKey:@"user_currency"];
    [collect_dict setObject:battery_lvl forKey:@"device_battery_level"];
    
    collect_data = collect_dict;
    collectSocialID();
    
    NSLog(@"collect_dict = %@",collect_dict);
};

void DeviceInfo::collectSocialID()
{
    NSMutableDictionary * collect_dict = (NSMutableDictionary *)collect_data;
    if(!collect_dict)
        return;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithUTF8String:CK::file_data_name] ];
    NSMutableDictionary *m_dict =  [[[NSMutableDictionary alloc] initWithContentsOfFile:filePath] autorelease];
    
    if(m_dict)
    {
        NSMutableDictionary *m_social = [m_dict objectForKey:@"social"];
        if(m_social)
        {
            if(CKHelper::Instance().FacebookID.length() == 0)
            {
                NSString *str = [m_social objectForKey:@"fbID"];
                CKHelper::Instance().FacebookID = [str UTF8String];
            };
            if(CKHelper::Instance().TwitterID.length() == 0)
            {
                NSString *str = [m_social objectForKey:@"twID"];
                CKHelper::Instance().TwitterID = [str UTF8String];
            };
        }
    };
    CCLOG("DeviceInfo::collectSocialID %s %s",CKHelper::Instance().FacebookID.c_str(),CKHelper::Instance().TwitterID.c_str());
    if(CKHelper::Instance().FacebookID.length() > 0)
    [collect_dict setObject:[NSString stringWithUTF8String:CKHelper::Instance().FacebookID.c_str()] forKey:@"user_facebook_id"];
    if(CKHelper::Instance().TwitterID.length() > 0)
    [collect_dict setObject:[NSString stringWithUTF8String:CKHelper::Instance().TwitterID.c_str()] forKey:@"user_twitter_id"];
};

void DeviceInfo::deleteOldInfo()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"info.plist"];
    NSMutableDictionary *m_dict =  [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
    NSMutableDictionary *collect_dict = (NSMutableDictionary *)collect_data;
    
    if(m_dict && collect_dict)
    {
//#define dictNotEqual(__dict__,__key__,__value__) ![__dict__ objectForKey:__key__] || ![__value__ isEqualToString:[__dict__ objectForKey:__key__]]
//#define dictEqual(__dict__,__filt__,__key__) ([__dict__ objectForKey:__key__] && [__filt__ objectForKey:__key__] && [[__dict__ objectForKey:__key__] isEqual:[__filt__ objectForKey:__key__]])
#define RpAndUpdValue(__dict__,__filt__,__key__)  \
        if([__dict__ objectForKey:__key__] && [__filt__ objectForKey:__key__] && [[__dict__ objectForKey:__key__] isEqual:[__filt__ objectForKey:__key__]]) \
            [__filt__ removeObjectForKey:__key__]; \
        else \
            [__dict__ setValue:[__filt__ objectForKey:__key__]forKey:__key__];

        RpAndUpdValue(m_dict,collect_dict,@"device_ID")
        RpAndUpdValue(m_dict,collect_dict,@"device_model")
        RpAndUpdValue(m_dict,collect_dict,@"localized_model")
        RpAndUpdValue(m_dict,collect_dict,@"device_itunes_name")
        RpAndUpdValue(m_dict,collect_dict,@"OS_version")
        RpAndUpdValue(m_dict,collect_dict,@"current_locale")
        RpAndUpdValue(m_dict,collect_dict,@"device_language")
        RpAndUpdValue(m_dict,collect_dict,@"device_carrier_name")
        RpAndUpdValue(m_dict,collect_dict,@"device_carrier_country")
        RpAndUpdValue(m_dict,collect_dict,@"user_gamecenter_name")
        RpAndUpdValue(m_dict,collect_dict,@"user_timezone_name")
        RpAndUpdValue(m_dict,collect_dict,@"user_timezone")
        RpAndUpdValue(m_dict,collect_dict,@"user_currency")
        RpAndUpdValue(m_dict,collect_dict,@"user_facebook_id")
        RpAndUpdValue(m_dict,collect_dict,@"user_twitter_id")
        
        [m_dict writeToFile:filePath atomically:YES];
//        if(dictEqual(m_dict,collect_dict,@"device_ID"))
//            [collect_dict removeObjectForKey:@"device_ID"];
//        else
//            [m_dict setValue:[collect_dict objectForKey:@"device_ID"]forKey:@"device_ID"];
//        
//        if(dictEqual(m_dict,collect_dict,@"device_model"))
//            [collect_dict removeObjectForKey:@"device_model"];
//        
//        if(dictEqual(m_dict,collect_dict,@"localized_model"))
//            [collect_dict removeObjectForKey:@"localized_model"];
//        
//        if(dictEqual(m_dict,collect_dict,@"device_itunes_name"))
//            [collect_dict removeObjectForKey:@"device_itunes_name"];
//        
//        if(dictEqual(m_dict,collect_dict,@"OS_version"))
//                [collect_dict removeObjectForKey:@"OS_version"];
//
//        if(dictEqual(m_dict,collect_dict,@"current_locale"))
//            [collect_dict removeObjectForKey:@"current_locale"];
//        
//        if(dictEqual(m_dict,collect_dict,@"device_language"))
//            [collect_dict removeObjectForKey:@"device_language"];
//        
//        if(dictEqual(m_dict,collect_dict,@"device_carrier_name"))
//            [collect_dict removeObjectForKey:@"device_carrier_name"];
//        
//        if(dictEqual(m_dict,collect_dict,@"device_carrier_name"))
//            [collect_dict removeObjectForKey:@"device_carrier_country"];
//        
//        if(dictEqual(m_dict,collect_dict,@"user_gamecenter_name"))
//            [collect_dict removeObjectForKey:@"user_gamecenter_name"];
//        
//        if(dictEqual(m_dict,collect_dict,@"user_timezone_name"))
//            [collect_dict removeObjectForKey:@"user_timezone_name"];
//        
//        if(dictEqual(m_dict,collect_dict,@"user_timezone"))
//            [collect_dict removeObjectForKey:@"user_timezone"];
//        
//        if(dictEqual(m_dict,collect_dict,@"user_currency"))
//            [collect_dict removeObjectForKey:@"user_currency"];
//        
//        
//        if(dictEqual(m_dict,collect_dict,@"user_facebook_id"))
//            [collect_dict removeObjectForKey:@"user_facebook_id"];
//        
//        if(dictEqual(m_dict,collect_dict,@"user_twitter_id"))
//            [collect_dict removeObjectForKey:@"user_twitter_id"];
    }else
    {
        saveCollectToFile();
    }
    [m_dict release];
};
const char *DeviceInfo::getFileName()
{
    char data_time[80];
    time_t tim = time(0);
    struct tm  tstruct;
    tstruct = * std::localtime(&tim);
    strftime(data_time, sizeof(data_time), "dt%y%m%dtm%H%M%S", &tstruct);
    NSString *str = [NSString stringWithFormat:@"%s_%s.txt",CKHelper::Instance().UserID.c_str(),data_time];
    return [str UTF8String];
};
const char *DeviceInfo::getInfoJson()
{
    NSError *error;
    NSMutableDictionary * collect_dict = (NSMutableDictionary *)collect_data;
    if(!collect_dict)
    {
        return NULL;
    };
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:collect_dict
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (! jsonData) {
        NSLog(@"Got an error: %@", error);
        return NULL;
    } else
    {
        NSString *jsonString = [[[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding] autorelease];
        NSLog(@"jsonString = %@",jsonString);
        return [jsonString UTF8String];
    };
};
bool DeviceInfo::createDirectory()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePathAndDirectory = [documentsDirectory stringByAppendingPathComponent:@"ck.statistic.info"];
    NSError *error;

    return [[NSFileManager defaultManager] createDirectoryAtPath:filePathAndDirectory
                                     withIntermediateDirectories:NO
                                                      attributes:nil
                                                           error:&error];
};

void DeviceInfo::saveJson()
{


    NSString *jsonString = [NSString stringWithUTF8String:getInfoJson()];

    char data_time[80];
    time_t tim = time(0);
    struct tm  tstruct;
    tstruct = * std::localtime(&tim);
    strftime(data_time, sizeof(data_time), "dt%y%m%dtm%H%M%S", &tstruct);
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePathAndDirectory = [documentsDirectory stringByAppendingPathComponent:@"ck.user.info"];
    NSError *error;
    
    [[NSFileManager defaultManager] createDirectoryAtPath:filePathAndDirectory
                                     withIntermediateDirectories:NO
                                                      attributes:nil
                                                           error:&error];
    NSString *filePath = [filePathAndDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%s_%s.txt",CKHelper::Instance().UserID.c_str(),data_time]];
    [jsonString writeToFile:filePath atomically:true encoding:NSUTF8StringEncoding error:nil];

};
void DeviceInfo::saveCollectToFile()
{
    if(!collect_data)
        return;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"info.plist"];
    [(NSMutableDictionary *)collect_data writeToFile:filePath atomically:YES];
};

void DeviceInfo::showLastSaveFile()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *fileAm = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"last.tmp"]];
    //NSString *msg = [[NSString alloc]
    NSData *file1 = [[[NSData alloc] initWithContentsOfFile:fileAm]retain];
    
    NSString *msg = [[NSString alloc] initWithData:file1 encoding:NSUTF8StringEncoding];
    
    UIAlertView *dialog = [[UIAlertView alloc] init];
    [dialog setDelegate: [[[UIApplication sharedApplication]delegate] rootViewController]];
    [dialog setTitle: @"Last Send"];
    [dialog setMessage: msg];
    [dialog addButtonWithTitle: @"Ok"];
    [dialog show];
    [dialog release];
    [file1 release];
}
void DeviceInfo::showInfo()
{
    collectAllInfo();
    NSMutableDictionary * collect_dict = (NSMutableDictionary *)collect_data;
    
    NSString *msg = [NSString stringWithFormat:@"%@", collect_dict];
    UIAlertView *dialog = [[UIAlertView alloc] init];
    [dialog setDelegate: [[[UIApplication sharedApplication]delegate] rootViewController]];
    [dialog setTitle: @"Info"];
    [dialog setMessage: msg];
    [dialog addButtonWithTitle: @"Ok"];
    [dialog show];
    [dialog release];
};

//void DeviceInfo::sendAllInfoFileToAmazon()
//{
//    AppController *appDelegate = [[UIApplication sharedApplication]delegate];
//    [[appDelegate rootViewController]processUploadStaticJson];
//};

//#pragma mark - STATISTIC_LOG
//static StatisticLog *m_StatisticLog = NULL;
//StatisticLog::StatisticLog()
//{
//    data = NULL;
//    time_in_page = -1;
//    current_page_number = 0;
//};
//StatisticLog::~StatisticLog()
//{
//    [static_cast<NSMutableDictionary *>(data) release];
//    data = NULL;
//};
//StatisticLog &StatisticLog::Instance()
//{
//    if(!m_StatisticLog)
//        m_StatisticLog = new StatisticLog;
//    return *m_StatisticLog;
//};
//
//void StatisticLog::createSession()
//{
//    
//}
//void StatisticLog::setNewPage(const int &_id)
//{
//    if(!data)
//        data  = [[NSMutableDictionary dictionaryWithCapacity:15] retain];
//    
//    NSMutableDictionary *m_dict = static_cast<NSMutableDictionary *>(data);
//    
//    m_dict setObject:(id) forKey:(id<NSCopying>)
//}
//void StatisticLog::save()
//{
//    
//}
//void StatisticLog::showLog()
//{
//    
//}





