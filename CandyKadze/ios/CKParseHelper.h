//
//  ObjCCalls.h
//  CandyKadze
//
//  Created by PR1.Stigol on 29.07.13.
//
//

#ifndef CandyKadze_CKParseHelper_h
#define CandyKadze_CKParseHelper_h

#include "CK.h"
#include <iostream>
#include <list>

class CKParseHelper
{
    CKParseHelper();
    void parseScore(void *_array_user,int _type_filter);
    void parseOpenLvl(void *_array_user);
    cocos2d::SEL_CallFuncND call_func;
    cocos2d::CCNode *target;
public:
    void setCallbackFunc(cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel);
    void parseSendAllScore(int _total,int best_hiscore,int _world,int world_score,int _ordinary_using,int _special_using,int _accuracy_rating,int _blocks_destroyed,int _plane_flights);
    void parseSendOpenWorldLvl(int _world_pixel,int _world_caramel,int _world_choco,int _world_ice,int _world_waffel);

    
    void parseLoadScore(std::list<std::string> *_list,int _type_filter,cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel);
    void parseLoadOpenLvl(std::list<std::string> *_list_friend,cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel);
    static CKParseHelper &Instance();
};
#endif
