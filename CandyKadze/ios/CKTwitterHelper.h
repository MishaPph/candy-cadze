//
//  ObjCCalls.h
//  CandyKadze
//
//  Created by PR1.Stigol on 31.07.13.
//
//

#ifndef CandyKadze_CKTwitterHelper_h
#define CandyKadze_CKTwitterHelper_h

#include "CK.h"

class CKTwitterHelper
{
    CKTwitterHelper();
    cocos2d::SEL_CallFuncND call_func;
    cocos2d::CCNode *target;
   // bool need_load_friends;
public:
    void TweetgetInfo(bool _load_frieds,cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel);
    void goToTwitter(const char *_userID);
    void TweetLogin();
    void TweetPost(const char* _message,const char *_image);
    void TweetLoadFriendList(cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel);
    void TweetFollowing(cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel,const char *_name);
    void TweetIsFollowing(cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel,const char *_name);
    bool TweetCanSend();
    static CKTwitterHelper &Instance();
};

#endif
