//
//  ICloud.m
//  CandyKadze
//
//  Created by PR1.Stigol on 25.04.13.
//
//

#import "ICloud.h"
#define kFILENAME @"data.plist"

@interface ICloud ()

@end

@implementation ICloud

static Note * icloud_doc = nil;
static NSMetadataQuery *icloud_query = nil;

- (void)loadDocument {
    
    icloud_query = [[NSMetadataQuery alloc] init];
    
    [icloud_query setSearchScopes:[NSArray arrayWithObject:
                                   NSMetadataQueryUbiquitousDocumentsScope]];
    NSPredicate *pred = [NSPredicate predicateWithFormat:
                         @"%K == %@", NSMetadataItemFSNameKey, kFILENAME];
    [icloud_query setPredicate:pred];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(queryDidFinishGathering:) name:NSMetadataQueryDidFinishGatheringNotification object:icloud_query];
    
    [icloud_query startQuery];
}

- (void)queryDidFinishGathering:(NSNotification *)notification {
    
    NSMetadataQuery *query = [notification object];
    [query disableUpdates];
    [query stopQuery];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:NSMetadataQueryDidFinishGatheringNotification object:query];
    
	[self loadData:query];
    
};


+ (void)loadData:(NSMetadataQuery *)query {
    if ([query resultCount] == 1) {
        
        NSMetadataItem *item = [query resultAtIndex:0];
        NSURL *url = [item valueForAttribute:NSMetadataItemURLKey];
        icloud_doc = [[[Note alloc] initWithFileURL:url] retain];
        
        [icloud_doc openWithCompletionHandler:^(BOOL success) {
            if (success) {
                NSLog(@"iCloud document opened");
            } else {
                NSLog(@"failed opening document from iCloud");
            }
        }];
	};
};


@end
