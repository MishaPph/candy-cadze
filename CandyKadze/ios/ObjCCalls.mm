//
//  ObjCCalls.m
//  CandyKadze
//
//  Created by PR1.Stigol on 06.11.12.
//
//

#include "ObjCCalls.h"
#include "CKNTPTime.h"
#include "AdSupport/ASIdentifierManager.h"
#include "GAI.h"

#import "AppController.h"
#import <GameKit/GKLocalPlayer.h>

#import "ios-ntp.h"
#import <sys/utsname.h>
#import <Chartboost.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include "CKLanguage.h"
#include "IPAddress.h"
#include "CKHelper.h"
#include "CKTableScore.h"
#include "CKParseHelper.h"
#import "CKIAPHelper.h"

#include "CK.h"

static bool allow_send_to_amazon;
static bool allow_send_to_flurry;
static bool allow_send_to_ga;
static bool allow_send_to_testflight;
static bool is_exist_file_static;


bool  ObjCCalls::low_cpu = false;
bool  ObjCCalls::low_memory = false;
cocos2d::CCNode* ObjCCalls::callback_object = NULL;
cocos2d::SEL_CallFuncND ObjCCalls::callback_func = NULL;


void ObjCCalls::setCallBack(cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel)
{
    callback_object = node;
    callback_func = _func_sel;
}

bool ObjCCalls::isExistFile()
{
    return is_exist_file_static;
};

void ObjCCalls::recognizingModel()
{
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *model = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
    low_memory = false;
    low_cpu = false;
    int type = 0;
    if( [model isEqualToString:@"iPod"])
    {
        low_cpu = true;
        low_memory = true;
    }else if( [model isEqualToString:@"iPad1"])
    {
        low_memory = true;
    }
    else if( [model isEqualToString:@"iPhone3"])
    {
        low_cpu = true;
        low_memory = true;
    }
    if ([model isEqualToString:@"i386"]) {
        type = 1;
    } else if ([model isEqualToString:@"iPod1,1"]) {
        type = 2;
    }
    else if ([model isEqualToString:@"iPod1,1"]) {
        type = 3;
    }
    else if ([model isEqualToString:@"iPod2,1"]) {
        type = 4;
    }
    else if ([model isEqualToString:@"iPod3,1"]) {
        type = 5;
    }
    else if ([model isEqualToString:@"iPod4,1"]) {
        type = 6;
    }
    else if ([model isEqualToString:@"iPhone1,1"]) {
        type = 7;
    }
    else if ([model isEqualToString:@"iPhone1,2"]) {
        type = 8;
    }
    else if ([model isEqualToString:@"iPhone2,1"]) {
        type = 9;
    }
    else if ([model isEqualToString:@"iPad1,1"]) {
        type = 10;
    }
    else if ([model isEqualToString:@"iPad2,1"]) {
        type = 11;
    }
    else if ([model isEqualToString:@"iPad3,1"]) {
        type = 12;
    }
    else if ([model isEqualToString:@"iPhone3,1"]) {
        type = 13;
    }
    else if ([model isEqualToString:@"iPhone4,1"]) {
        type = 14;
    }
    else if ([model isEqualToString:@"iPhone5,1"]) {
        type = 15;
    }
    else if ([model isEqualToString:@"iPhone5,2"]) {
        type = 16;
    }
    else if ([model isEqualToString:@"iPad3,4"]) {
        type = 17;
    }
    else if ([model isEqualToString:@"iPad2,5"]) {
        type = 18;
    };
//    @"i386"      on the simulator
//    @"iPod1,1"   on iPod Touch
//    @"iPod2,1"   on iPod Touch Second Generation
//    @"iPod3,1"   on iPod Touch Third Generation
//    @"iPod4,1"   on iPod Touch Fourth Generation
//    @"iPhone1,1" on iPhone
//    @"iPhone1,2" on iPhone 3G
//    @"iPhone2,1" on iPhone 3GS
//    @"iPad1,1"   on iPad
//    @"iPad2,1"   on iPad 2
//    @"iPad3,1"   on 3rd Generation iPad
//    @"iPhone3,1" on iPhone 4
//    @"iPhone4,1" on iPhone 4S
//    @"iPhone5,1" on iPhone 5 (model A1428, AT&T/Canada)
//    @"iPhone5,2" on iPhone 5 (model A1429, everything else)
//    @"iPad3,4" on 4th Generation iPad
//    @"iPad2,5" on iPad Mini
    if(type == 3 || type == 4 || type == 5 || type == 6 ||  type == 10)
    {
        low_cpu = true;
        low_memory = true;
    };
    if(type == 13)
    {
        low_cpu = true;
    }
    NSLog(@"ObjCCalls::recognizingModel  %@ memory %d cpu %d ", model,low_memory,low_cpu);
};

int ObjCCalls::isLowCPU()
{
    return low_cpu;
};

int ObjCCalls::isLittleMemory()
{
    return low_memory;
}
float ObjCCalls::getDeviceVersion()
{
    return [[[UIDevice currentDevice] systemVersion] floatValue];
};

float ObjCCalls::getVersion()
{
    //Battery state
    [[UIDevice currentDevice] setBatteryMonitoringEnabled:true];
    NSArray *batteryStatus = [NSArray arrayWithObjects:
                              @"Battery status is unknown.",
                              @"Battery is in use (discharging).",
                              @"Battery is charging.",
                              @"Battery is fully charged.", nil];
    
    if ([[UIDevice currentDevice] batteryState] == UIDeviceBatteryStateUnknown)
    {
        NSLog(@"ObjCCalls::getVersion %@", [batteryStatus objectAtIndex:0]);
    }
    else
    {
        NSString *msg = [NSString stringWithFormat:
                         @"Battery charge level: %0.2f%%\n%@", [[UIDevice currentDevice] batteryLevel] * 100,
                         [batteryStatus objectAtIndex:[[UIDevice currentDevice] batteryState]] ];
        NSLog(@"ObjCCalls::getVersion %@", msg);
    }
    
      //bundle version
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
    return  [version floatValue];;
};

void ObjCCalls::checkFB()
{
    if ([[FBSession activeSession]isOpen]) {
        /*
         * if the current session has no publish permission we need to reauthorize
         */
        if ([[[FBSession activeSession]permissions]indexOfObject:@"publish_actions"] == NSNotFound) {
            
            [[FBSession activeSession] requestNewPublishPermissions:[NSArray arrayWithObject:@"publish_action"] defaultAudience:FBSessionDefaultAudienceFriends
                                                  completionHandler:^(FBSession *session,NSError *error){
                                                     // [self postPhoto];
                                                  }];
            
        }else{
           // [self publishStory];
        }
    }else{
        /*
         * open a new session with publish permission
         */
        [FBSession openActiveSessionWithPublishPermissions:[NSArray arrayWithObject:@"publish_actions"]
                                           defaultAudience:FBSessionDefaultAudienceOnlyMe
                                              allowLoginUI:YES
                                         completionHandler:^(FBSession *session, FBSessionState status, NSError *error) {
                                             if (!error && status == FBSessionStateOpen) {
                                                 //[self publishStory];
                                             }else{
                                                 NSLog(@"error");
                                             }
                                         }];
    }
};

void optimizeDict(NSMutableDictionary *_dict)
{
    NSMutableDictionary * dict = _dict;
    NSLog(@"optimizeDict %@",dict);
    if([dict objectForKey:@"ID"])
    {
        NSString *str = [dict objectForKey:@"ID"];
        if([str isEqualToString:@"0"])
        {
            [dict removeObjectForKey:@"ID"];
        };
    };
    if([dict objectForKey:@"Rotation"])
    {
        NSString *str = [dict objectForKey:@"Rotation"];
        if([str isEqualToString:@"0"])
        {
            [dict removeObjectForKey:@"Rotation"];
        };
    };
    if([dict objectForKey:@"Event"])
    {
        NSString *str = [dict objectForKey:@"Event"];
        if([str isEqualToString:@"0"]|| [str isEqualToString:@"functionName"])
        {
            [dict removeObjectForKey:@"Event"];
        };
    };
    if([dict objectForKey:@"Value"])
    {
        NSString *str = [dict objectForKey:@"Value"];
        if([str isEqualToString:@"0"] || [str isEqualToString:@"functionValue"])
        {
            [dict removeObjectForKey:@"Value"];
        };
    };
    if([dict objectForKey:@"TransformCenter"])
    {
        NSString *str = [dict objectForKey:@"TransformCenter"];
        if([str isEqualToString:@"{0.5,0.5}"])
        {
            [dict removeObjectForKey:@"TransformCenter"];
        };
    };
    
    if([dict objectForKey:@"Scale"])
    {
        NSString *str = [dict objectForKey:@"Scale"];
        if([str isEqualToString:@"{1.0,1.0}"])
        {
            [dict removeObjectForKey:@"Scale"];
        };
    };
    
    if([dict objectForKey:@"Special"])
    {
        NSMutableDictionary *s_dict  = [dict objectForKey:@"Special"];
        if([s_dict count] == 0)
        {
            [dict removeObjectForKey:@"Special"];
        }
    };
    if([dict objectForKey:@"Childs"])
    {
        NSMutableDictionary *s_dict  = [dict objectForKey:@"Childs"];
        if([s_dict count] == 0)
        {
            [dict removeObjectForKey:@"Childs"];
        }else
        {
            for (NSString *str in s_dict) {
                NSMutableDictionary *t_dict = [s_dict objectForKey:str];
                optimizeDict(t_dict);
            }
        };
    };
    
    if([dict objectForKey:@"Animation"])
    {
        NSMutableDictionary *a_dict = [dict objectForKey:@"Animation"];
        bool p = false;
        if([a_dict objectForKey:@"Jumping"])
        {
            NSString *str = [a_dict objectForKey:@"Jumping"];
            if([str isEqualToString:@"0"])
            {
                [a_dict removeObjectForKey:@"Jumping"];
            }
            else
            {
                p = true;
            }
        }
        if([a_dict objectForKey:@"Moving"])
        {
            NSString *str = [a_dict objectForKey:@"Moving"];
            if([str isEqualToString:@"{0.0,0.0}"])
            {
                [a_dict removeObjectForKey:@"Moving"];
            }else
            {
                p = true;
            }
        }
        
        if([a_dict objectForKey:@"PulseInterval"])
        {
            NSString *str = [a_dict objectForKey:@"PulseInterval"];
            if([str isEqualToString:@"0"])
            {
                [a_dict removeObjectForKey:@"PulseInterval"];
            }else
            {
                p = true;
            }
        }
        if([a_dict objectForKey:@"TextureMove"])
        {
            NSString *str = [a_dict objectForKey:@"TextureMove"];
            if([str isEqualToString:@"{0.0,0.0}"])
            {
                [a_dict removeObjectForKey:@"TextureMove"];
            }else
            {
                p = true;
            }
        }
        if([a_dict objectForKey:@"Rotate"])
        {
            NSString *str = [a_dict objectForKey:@"Rotate"];
            if([str isEqualToString:@"0"])
            {
                [a_dict removeObjectForKey:@"Rotate"];
            }else
            {
                p = true;
            }
        }
        if(!p)
        {
           [dict removeObjectForKey:@"Animation"]; 
        }
    };
    
};
void ObjCCalls::optimezeALLFileInDir()
{
    DIR *dir;
    struct dirent *ent;

    std::string path = CCFileUtils::sharedFileUtils()->getDocumentPath().c_str();
    path.append("/optimize/");
    if ((dir = opendir (path.c_str())) != NULL) {
        /* print all the files and directories within directory */
        while ((ent = readdir (dir)) != NULL) {
            CCLOG("optimezeALLFileInDir load file in directory %s",ent->d_name);
            std::string tmp_str = ent->d_name;
            
            int nPosRight  = tmp_str.find('.');
            
            std::string _sufix = tmp_str.substr(nPosRight + 1,tmp_str.length());
            std::string _name = tmp_str.substr(0,nPosRight);
            //  char tmp[255];
            
            //sprintf(char *, const char *, ...)
            if(strcmp(_sufix.c_str(), "plist") == 0 )
            {
                optimazePlist(ent->d_name);
            };
        }
        closedir (dir);
    } else {
        /* could not open directory */
        perror ("");
    };
};

void ObjCCalls::optimazePlist(const char *file_name)
{
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);

    NSString *pliststr = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"/optimize/"];
    NSString *plistPath = [pliststr stringByAppendingPathComponent:[NSString stringWithUTF8String:file_name]];
    
 //   const char* pszFullPath = CCFileUtils::sharedFileUtils()->fullPathFromRelativePath(file_name);
//    NSString* pPath = [NSString stringWithUTF8String:pszFullPath];
    NSMutableDictionary *myDic  = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
    
   // NSMutableDictionary *myDic = [[NSMutableDictionary alloc] initWithContentsOfFile:[NSString stringWithUTF8String:file_name]];
    
    if([myDic objectForKey:@"frames"])
    {
        NSMutableDictionary *myFrame = [myDic objectForKey:@"frames"];
        
        for (NSString *str in myFrame) {
            NSMutableDictionary *t_dict = [myFrame objectForKey:str];
            optimizeDict(t_dict);
        }
    };
   // NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
   // NSString *plistPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:[NSString stringWithUTF8String:file_name]];
    
    [myDic writeToFile:plistPath atomically:YES];
};

void ObjCCalls::load()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *plistPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"game_settings.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if(![fileManager fileExistsAtPath:plistPath])
    {
#if TARGET_IPHONE_SIMULATOR
        allow_send_to_amazon = false;
        allow_send_to_testflight = true;
        allow_send_to_ga = false;
        allow_send_to_flurry = false;
      //  is_exist_file_static = true;
       is_exist_file_static = false;
#else
        allow_send_to_amazon = true;
        allow_send_to_testflight = true;
        allow_send_to_ga = true;
        allow_send_to_flurry = true;
#endif
    }
    else
    {
        NSMutableDictionary *myDic = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
        allow_send_to_amazon = [[myDic objectForKey:@"Amazon"] isEqualToString:@"YES"];
        allow_send_to_testflight = [[myDic objectForKey:@"TestFlight"] isEqualToString:@"YES"];
        allow_send_to_ga = [[myDic objectForKey:@"GoogleAnalytics"] isEqualToString:@"YES"];
        allow_send_to_flurry = [[myDic objectForKey:@"Flurry"] isEqualToString:@"YES"];
        NSArray *thisArray = [[NSArray alloc] initWithContentsOfFile:plistPath];
        NSLog(@"The array count: %i", [thisArray count]);
        is_exist_file_static = true;
    };
#if TARGET_IPHONE_SIMULATOR
    allow_send_to_amazon = false;
    allow_send_to_testflight = true;
    allow_send_to_ga = false;
    allow_send_to_flurry = false;
  //  is_exist_file_static = true;
    is_exist_file_static = false;
#endif
};

void ObjCCalls::initStatic()
{
    [[RageIAPHelper sharedInstance] setCallbackFunc: ObjCCalls::IAPHelper_callback];
    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
    if(allow_send_to_ga)
    {
        //GA
        [GAI sharedInstance].debug = YES;
        [GAI sharedInstance].dispatchInterval = 300;
        [GAI sharedInstance].trackUncaughtExceptions = YES;
        [[GAI sharedInstance] trackerWithTrackingId:[NSString stringWithFormat:@"%s",kTrackingId]];
        [GAI sharedInstance].defaultTracker.sessionStart = YES;
        [[[GAI sharedInstance] defaultTracker ] setAppVersion:version];
        [[[GAI sharedInstance] defaultTracker ] setAppScreen:@"Home"];
    }
    
    //Flurry
    if(allow_send_to_flurry)
    {
        [Flurry setAppVersion:version];
        [Flurry setShowErrorInLogEnabled:TRUE];
        [Flurry setDebugLogEnabled:TRUE];
        
        [Flurry setSecureTransportEnabled:FALSE];
        [Flurry setSessionContinueSeconds:10];
        [Flurry startSession:[NSString stringWithFormat:@"%s",flurry_sessionID ]];
        [Flurry logAllPageViews:[[[UIApplication sharedApplication]delegate] rootViewController]];
        [Flurry setEventLoggingEnabled:TRUE];
        [Flurry setSessionReportsOnCloseEnabled:false];
        [Flurry setShowErrorInLogEnabled:TRUE];
        [Flurry setSessionReportsOnPauseEnabled:false];
      //  [Flurry logEvent:@"Application Started" timed:false];
    };
    

//    IAPHelper *m_iap = 
  // IAPHelper *m_iap = [IAPHelper initWithProductIdentifiers:productIdentifiers];
    
//    NSSet * productIdentifiers = [NSSet setWithObjects:
//                                  @"com.cc.kids_mode",
//                                  @"com.cc.ads_removing",
//                                  nil];
    
   // RageIAPHelper::sharedInstance().productIdentifiers(@"com.cc.ads_removing");
   // [[RageIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {}];
  //  CCLOG("com.cc.ads_removing buy %d",[[RageIAPHelper sharedInstance] productPurchased:@"com.cc.ads_removing"]);
};

void ObjCCalls::setMultiTouch(bool _enable)
{
    //[[[[[UIApplication sharedApplication]delegate] rootViewController] view] setMultipleTouchEnabled:false];
    //CCDirector::sharedDirector()->getOpenGLView()->set
};


#pragma mark - IAPHELPER
void ObjCCalls::IAPHelper_buy(const char *_id)
{
    CK::StatisticLog::Instance().setTryBuy(_id, 1);
    [[RageIAPHelper sharedInstance] buyProduct:[NSString stringWithUTF8String:_id]];
}
bool ObjCCalls::IAPHelper_isbuy(const char *_id)
{
    return [[RageIAPHelper sharedInstance] productPurchased:[NSString stringWithUTF8String:_id]];
}
void ObjCCalls::IAPHelper_callback(const char *_id)
{
    CCLOG("ObjCCalls::IAPHelper_callback %s",_id);
    if(callback_object)
    {
        char str[255];
        sprintf(str, "%s",_id);
        (callback_object->*callback_func)(NULL,str);
    }
};
//bool ObjCCalls::IAPHelper_productPurchased(const char *_id)
//{
//   return [[RageIAPHelper sharedInstance] productPurchased:[NSString stringWithUTF8String:_id]];
//};
void ObjCCalls::IAPHelper_restore()
{
    [[RageIAPHelper sharedInstance]restoreCompletedTransactions];
};
#pragma mark - EVENTS
void ObjCCalls::checkSendStatistic()
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *plistPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"game_settings.plist"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if(![fileManager fileExistsAtPath:plistPath])
    {        
       
#if TARGET_IPHONE_SIMULATOR
        allow_send_to_amazon = true;
        allow_send_to_testflight = false;
        allow_send_to_ga = false;
        allow_send_to_flurry = false;
#else
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:nil];
        [params setObject:[NSString stringWithFormat:@"%s","YES"] forKey:[NSString stringWithFormat:@"%s","Amazon"]];
        [params setObject:[NSString stringWithFormat:@"%s","YES"] forKey:[NSString stringWithFormat:@"%s","TestFlight"]];
        [params setObject:[NSString stringWithFormat:@"%s","YES"] forKey:[NSString stringWithFormat:@"%s","GoogleAnalytics"]];
        [params setObject:[NSString stringWithFormat:@"%s","YES"] forKey:[NSString stringWithFormat:@"%s","Flurry"]];
        [params writeToFile:plistPath atomically: YES];
        allow_send_to_amazon = true;
        allow_send_to_testflight = true;
        allow_send_to_ga = true;
        allow_send_to_flurry = true;
    }
    else
    {
        NSMutableDictionary *myDic = [[NSMutableDictionary alloc] initWithContentsOfFile:plistPath];
        allow_send_to_amazon = [[myDic objectForKey:@"Amazon"] isEqualToString:@"YES"];
        allow_send_to_testflight = [[myDic objectForKey:@"TestFlight"] isEqualToString:@"YES"];
        allow_send_to_ga = [[myDic objectForKey:@"GoogleAnalytics"] isEqualToString:@"YES"];
        allow_send_to_flurry = [[myDic objectForKey:@"Flurry"] isEqualToString:@"YES"];        
        NSArray *thisArray = [[NSArray alloc] initWithContentsOfFile:plistPath];
        NSLog(@"The array count: %i", [thisArray count]);
#endif
        
    };
};

void ObjCCalls::sendCustomGA(int _index,const char* _dimension,int _value)
{
    if(allow_send_to_ga)
    {
        id tracker1 = [[GAI sharedInstance] defaultTracker];
        if([tracker1 setCustom:_index dimension:[NSString stringWithFormat:@"%s",_dimension]])
        {
            
            [tracker1 setCustom:_index metric:[NSNumber numberWithLongLong:_value]];
            //[tracker1 sendView:@"Home"];
        }
        else
        {
             CCLOG("sendCustomGA:: Error");
        }
        CCLOG("sendCustomGA:: ind %d, _dimension %s, value %d",_index,_dimension,_value);
    }
};

void ObjCCalls::logPageView(bool _ga,const char* _name)
{
    @try {
         if(_ga)
         {
             if(allow_send_to_ga)
             {
                 // CCLOG("ObjCCalls::logPageView %s",_name);
                 id tracker1 = [[GAI sharedInstance] defaultTracker];
                 if(![tracker1 sendView:[NSString stringWithFormat:@"%s",_name]])
                 {
                     CCLOG("ObjCCalls:logPageView: Error send screen %s",_name);
                 };
                 //[tracker1 sendView];
             }
         }
         else
         {
             if(allow_send_to_flurry)
             {
                 [Flurry	logPageView];
             }
         }
    }
    @catch (NSException *exception) {
        NSLog(@"%@", exception.reason);
        return;
    }
    @finally {
        return;
    }
};

void ObjCCalls::sendEventGA(const char* _category,const char* _action,const char* _label,const int &_value)
{
    if(allow_send_to_ga)
    {
       
        id tracker1 = [[GAI sharedInstance] defaultTracker];
        [tracker1 sendEventWithCategory:[NSString stringWithFormat:@"%s",_category] withAction:[NSString stringWithFormat:@"%s",_action] withLabel:nil withValue:[NSNumber numberWithInt:_value]];
        
      //  CCLOG("sendEventGA:: _category %s,_action %s",_category,_action);
    }
};

void ObjCCalls::sendEventFlurry(const char *_eventname,bool _time,const char* _format,...)
{
    if(!allow_send_to_flurry)
        return;
    
    char sObject[NAME_MAX];
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:nil];
    if(strlen(_eventname) < 3)
    {
        CCLOG("ObjCCalls::sendEvent::Error incorect event name");
        return;
    };
    
    if(strlen(_format) > 3)
    {
        va_list ap;
        va_start(ap, _format);
        vsprintf(sObject, _format, ap);
        std::string left,right,tmp;
        tmp = sObject;
        while(CK::strToTwoStr(tmp,left,right,','))
        {
            tmp = right;
            std::string _left,_right;
            CK::strToTwoStr(left,_left,_right,'=');
            [params setObject:[NSString stringWithFormat:@"%s",_right.c_str()] forKey:[NSString stringWithFormat:@"%s",_left.c_str()]];
            CCLOG("sendEvent::add pair %s %s",_left.c_str(),_right.c_str());
        }
        
        std::string _left,_right;
        CK::strToTwoStr(right,_left,_right,'=');
        [params setObject:[NSString stringWithFormat:@"%s",_right.c_str()] forKey:[NSString stringWithFormat:@"%s",_left.c_str()]];
        va_end(ap);
    }

        [Flurry logEvent:[NSString stringWithFormat:@"%s",_eventname] withParameters:params];
};



void ObjCCalls::goToFasebook()
{
    NSString *msg = [NSString stringWithFormat:@"http://www.facebook.com/groups/stigol"];
    NSURL *nsUrl = [NSURL URLWithString:msg];
    [[UIApplication sharedApplication] openURL:nsUrl];
};

void ObjCCalls::goToURL(const char *_url)
{
   // NSString *msg = [NSString stringWithFormat:@"%s",_url];
    NSURL *nsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"%s",_url]];
    [[UIApplication sharedApplication] openURL:nsUrl];
};


std::string ObjCCalls::getUserName()
{
//    GKLocalPlayer *lp = [GKLocalPlayer localPlayer];
//    if (lp.authenticated) {
//        
//       return std::string([[lp alias] UTF8String]);
//        //return lp.alias;
//        //Any other stuff you need to do with this local player's instance goes here.
//    };
    UIDevice *device = [UIDevice currentDevice];
    return std::string([[device name] UTF8String]);
};

//void ObjCCalls::sendToAmazon()
//{
//    if(allow_send_to_amazon)
//    {
//        if (CKNTPTime::timeDiff() > 500) {
//            return;
//        }
//      //  AppController *appDelegate = [[UIApplication sharedApplication]delegate];
//      //  [[appDelegate rootViewController]processUploadStatic];
//      //  [[appDelegate rootViewController]processUploadFeedback];
//    }
//};
//void ObjCCalls::sendToGameStatistic()
//{
//    AppController *appDelegate = [[UIApplication sharedApplication]delegate];
//    [[appDelegate rootViewController]processUploadCKJson];
//};

std::string ObjCCalls::getDeviceName()
{
    UIDevice *device = [UIDevice currentDevice];
    std::string bar  = std::string([[device name] UTF8String]);
    return bar;
};

std::string ObjCCalls::getIPAddress()
{
    NSString *address = @"error";
    struct ifaddrs *interfaces = NULL;
    struct ifaddrs *temp_addr = NULL;
    int success = 0;
  //  NSString *address = NULL;
    // retrieve the current interfaces - returns 0 on success
    success = getifaddrs(&interfaces);
    if (success == 0) {
        // Loop through linked list of interfaces
        temp_addr = interfaces;
        while (temp_addr != NULL) {
            if( temp_addr->ifa_addr->sa_family == AF_INET) {
                // Check if interface is en0 which is the wifi connection on the iPhone
                if ([[NSString stringWithUTF8String:temp_addr->ifa_name] isEqualToString:@"en0"]) {
                    // Get NSString from C String
                    address = [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)temp_addr->ifa_addr)->sin_addr)];
                }
            }
            
            temp_addr = temp_addr->ifa_next;
        }
    }
    
    // Free memory
    freeifaddrs(interfaces);
    return std::string([address UTF8String]);
}

bool ObjCCalls::isActiveConnection()
{
    AppController *appDelegate = [[UIApplication sharedApplication]delegate];
    return [[appDelegate rootViewController] isActiveInternet];
};

std::string ObjCCalls::getNTP()
{
   // NSDate * networkTime = [NSDate networkDate];
    assert(0);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyMMdd"];
    return std::string([ [NSString stringWithFormat:@"%@", [dateFormatter stringFromDate:[NSDate networkDate]]] UTF8String]);
}


std::string ObjCCalls::getDeviceLang()
{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSArray *languages = [defaults objectForKey:@"AppleLanguages"];
    NSString *currentLanguage = [languages objectAtIndex:0];
    
    return std::string([currentLanguage UTF8String]);
    //  return std::string(_key);
};


bool ObjCCalls::showBanner(int _num)
{
    AppController *appDelegate = [[UIApplication sharedApplication]delegate];
    [[appDelegate rootViewController]showBanner];
    return true;
};

void ObjCCalls::hideBanner(bool _destroy)
{
    AppController *appDelegate = [[UIApplication sharedApplication]delegate];
    //if(!_destroy)
    {
        [[appDelegate rootViewController]hideBanner];
    }
    //else
    {
    //    [[appDelegate rootViewController]deleteBanner];
    }

};

void ObjCCalls::createBanner(int _num)
{
    AppController *appDelegate = [[UIApplication sharedApplication]delegate];
    [[appDelegate rootViewController]createBanner];
};

void ObjCCalls::showChartboost(const char *_int)
{
#if ENABLE_CHARTBOOST
    CCLOG("showChartboost %s",_int);
    // Show an interstitial
    if(strlen(_int) > 2)
    {
        [[Chartboost sharedChartboost] showInterstitial];
        
    }
    else if([[Chartboost sharedChartboost] hasCachedInterstitial:[NSString stringWithFormat:@"%s", _int]])
    {
        [[Chartboost sharedChartboost] showInterstitial:[NSString stringWithFormat:@"%s", _int]];
    }
    else
    {
        [[Chartboost sharedChartboost] showMoreApps];
    }
#endif
};


//bool ObjCCalls::downloadBanner()
//{
//    if (!isActiveConnection()) {
//        return false;
//    }
//    int k = CKNTPTime::timeDiff();
//    if (k > 500) {
//        CCLOG("Error our downloadBanner %d",k);
//        return false;
//    }
//    AppController *appDelegate = [[UIApplication sharedApplication]delegate];
//    return [[appDelegate rootViewController]processDowloadBanner];
//};
#pragma mark - SOCILA
void ObjCCalls::leaderboardSendScore(const int &_score,const char *_leaderID)
{
    GKScore *scoreReporter = [[[GKScore alloc] initWithCategory:[NSString stringWithUTF8String:_leaderID]] autorelease];
	scoreReporter.value = _score;
	[scoreReporter reportScoreWithCompletionHandler: ^(NSError *error){}];
}

void ObjCCalls::leaderboardSendAllScore(int _total, int best_hiscore,int _world, int world_score,
                                        int _ordinary_using, int _special_using, int _accuracy_rating,int _blocks_destroyed,int _plane_flights)
{
    leaderboardSendScore(_total,"Total_scores");
    leaderboardSendScore(best_hiscore,"High_Scores_All");
    switch (_world) {
        case 0:
            leaderboardSendScore(world_score,"High_Scores_Pixel");
            break;
        case 1:
            leaderboardSendScore(world_score,"High_Scores_Caramel");
            break;
        case 2:
            leaderboardSendScore(world_score,"High_Scores_Choco");
            break;
        case 3:
            leaderboardSendScore(world_score,"High_Scores_Ice");
            break;
        case 4:
            leaderboardSendScore(world_score,"High_Scores_Waffles");
            break;
        default:
            break;
    }
    leaderboardSendScore(_ordinary_using,"ordinary_using");
    leaderboardSendScore(_special_using,"special_using");
    leaderboardSendScore(_accuracy_rating,"accuracy_rating");
    leaderboardSendScore(_blocks_destroyed,"blocks_destroyed");
    leaderboardSendScore(_plane_flights,"plane_flights");
    
    CKParseHelper::Instance().parseSendAllScore(_total,best_hiscore,_world,world_score,_ordinary_using,_special_using,_accuracy_rating,_blocks_destroyed,_plane_flights);
};

void ObjCCalls::showLeaderboard()
{
    AppController *appDelegate = [[UIApplication sharedApplication]delegate];
    return [[appDelegate rootViewController]showLeaderboard];
};

void ObjCCalls::goToGift()
{
    NSString *msg = [NSString stringWithFormat:@"https://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/giftSongsWizard?gift=1&salableAdamId=%s&productType=C&pricingParameter=STDQ",APPLE_ID];
    //msg = @"https://buy.itunes.apple.com/WebObjects/MZFinance.woa/wa/giftSongsWizard?gift=1salableAdamId=641029291productType=CpricingParameter=STDQ";
    //NSLog(@"msg %@",msg);
    NSURL *nsUrl = [NSURL URLWithString:msg];
    [[UIApplication sharedApplication] openURL:nsUrl];
};

void ObjCCalls::sendMessage(const int _lvl_num,const int &_world,const int &_score)
{
// [NSString stringWithFormat:@"%s game. I just completed Level # %d in %s \n    <A href = '%@'>Get this game here</A>" , <назва гри>, <номер левела>, <назва світу>, <Appstore_link>];
//https://itunes.apple.com/app/g15/id641029291
    NSString *body  = [NSString stringWithFormat:[NSString stringWithUTF8String:LCZ("%s  game. I just completed Level # %d in %s. My score is %d \n Get this game here:  \n")],GAME_NAME,_lvl_num,WORLD_NAMES[_world],_score];
   NSString* message= [NSString stringWithFormat: @"%@%s",body,ITUNES_URL];
    
    AppController *appDelegate = [[UIApplication sharedApplication]delegate];
    [[appDelegate rootViewController]sendMessage:message];
};

void ObjCCalls::tellFriend()
{
    AppController *appDelegate = [[UIApplication sharedApplication]delegate];
    NSString *Appstore_link = [NSString stringWithFormat: @"http://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=%s",APPLE_ID ];
    NSString *body  = [NSString  stringWithFormat: @"%s <p> <a href = '%@'>%s</a>" ,LCZ("Check out this game."),Appstore_link,LCZ("Get it here")];
    NSString *sybject = [NSString  stringWithFormat:  @"%s game", GAME_NAME];
    [[appDelegate rootViewController]sendMail:sybject message:body image:nil];
};

void ObjCCalls::sendMail(const int _score,const int _lvl_num,const int &_world,const char  *_image)
{
    AppController *appDelegate = [[UIApplication sharedApplication]delegate];
    NSString *Appstore_link = [NSString stringWithFormat: @"http://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=%s",APPLE_ID ];
    NSString *mess = [NSString stringWithFormat:[NSString stringWithUTF8String:LCZ("I just completed Level # %d in %s.<p> My score is %d")],_lvl_num,WORLD_NAMES[_world],_score];
    NSString *body  = [NSString  stringWithFormat: @"%@ <p> <a href = '%@'>%s</a>" ,mess,Appstore_link,LCZ("Get this game here")];
    
    NSString *subject = [NSString  stringWithFormat: [NSString stringWithUTF8String:LCZ("Checkout my score in %s game")], GAME_NAME];
    
    [[appDelegate rootViewController]sendMail:subject message:body image:[NSString  stringWithUTF8String:_image]];
    
    //LCZ("Can’t send mail from this device");
};



#pragma mark - EVERYPLAY
void ObjCCalls::everyplayStartCapture(int _lenght_capture,int _delay)
{
    if([[[Everyplay sharedInstance] capture] isRecording])
        return;
    if(![[[Everyplay sharedInstance] capture] isRecordingSupported])
        return;
    // When integrating against your game, call startRecording and stopRecording
    // methods from [[Everyplay sharedInstance] capture] instead
    if(_lenght_capture == -1)
    {
        [[[Everyplay sharedInstance] capture] startRecording];
    }
    else
    {
        [[[Everyplay sharedInstance] capture] autoRecordForSeconds:_lenght_capture withDelay:_delay];
    };
    CCLOG("everyplayStartCapture %d",[Everyplay sharedInstance]);
};


void ObjCCalls::everyplayShow()
{
    [[Everyplay sharedInstance] showEveryplay];
    
};

void ObjCCalls::everyplayPlayLastVideo(const int &score,const int _lvl_num,const int _world_num)
{
    CK::StatisticLog::Instance().setLink(CK::Link_EveryplayVideo);
    CCLOG("everyplayPlayLastVideo %d",[Everyplay sharedInstance]);
    if(score > 0)
    {
        [[Everyplay sharedInstance] mergeSessionDeveloperData:@{@"score" : [NSString stringWithFormat:@"%d",score], @"level #" : [NSString stringWithFormat:@"%d",_lvl_num], @"world" : [NSString stringWithFormat:@"%s",WORLD_NAMES[_world_num]]}];
    }
    else
    {
        [[Everyplay sharedInstance] mergeSessionDeveloperData:@{@"level #" : [NSString stringWithFormat:@"%d",_lvl_num], @"world" : [NSString stringWithFormat:@"%s",WORLD_NAMES[_world_num]]}];
    }
        
    [[Everyplay sharedInstance] playLastRecording];
};

void ObjCCalls::everyplayPauseRecording()
{
    CCLOG("everyplayPauseRecording %d",[Everyplay sharedInstance]);
    if(![[[Everyplay sharedInstance] capture] isPaused])
        [[[Everyplay sharedInstance] capture] pauseRecording];
};

void ObjCCalls::everyplayResumeRecording()
{
    CCLOG("everyplayResumeRecording %d",[Everyplay sharedInstance]);
    if([[[Everyplay sharedInstance] capture] isPaused])
        [[[Everyplay sharedInstance] capture] resumeRecording];
};

void ObjCCalls::everyplayStopRecording()
{
    CCLOG("everyplayStopRecording %d",[Everyplay sharedInstance]);
   // [[Everyplay sharedInstance] mergeSessionDeveloperData:@{@"testString" : @"hello"}];
   // [[Everyplay sharedInstance] mergeSessionDeveloperData:@{@"testInteger" : @42}];
    if ([[[Everyplay sharedInstance] capture] isRecording] == YES)
    {
        [[[Everyplay sharedInstance] capture] stopRecording];
       // [[Everyplay sharedInstance] playLastRecording];
    }
};

const char* ObjCCalls::needUpdateApp()
{
//    NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
//    NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
//    
//    float current = [version floatValue];
//    
//    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:@"http://itunes.apple.com/lookup?id=641029291"]];
//    NSURLResponse* response = nil;
//    NSData* data = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&response error:nil];
//    
//    NSDictionary *publicTimeline = [NSJSONSerialization JSONObjectWithData:data options:0 error:0];
//    NSDictionary *results = [publicTimeline objectForKey:@"results"];
//    //NSString *stirng = [[results valueForKey:@"version"] string];
//    NSArray *array = [results valueForKey:@"version"];
//    NSArray *releaseNotes = [results valueForKey:@"releaseNotes"];
//    
//    //NSLog(@"releaseNotes = %@",releaseNotes);
//    
//    float server_version = 0.0;
//    NSString *noteStr = @"";
//    for (NSString *str in releaseNotes) {
//        noteStr = str;
//        NSLog(@"noteStr %@",str);
//    }
//    
//    for (NSString *str in array) {
//        server_version = [str floatValue];
//        NSLog(@"needUpdateApp %@",str);
//    }
//    
//    if(server_version > current)
//        return [noteStr UTF8String];
//    
//    return NULL;
    //AppController *appDelegate = [[UIApplication sharedApplication]delegate];
    //[[appDelegate rootViewController]downloadTestFlightAppVersion];
    
    return NULL;
};

void ObjCCalls::showFile(const char*_file_name)
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [[[paths objectAtIndex:0]stringByAppendingPathComponent:@"/"]stringByAppendingPathComponent:@"ck.statistic.info/"];
    NSString *fileAm = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%s",_file_name]];
    //NSString *msg = [[NSString alloc]
    NSData *file1 = [[[NSData alloc] initWithContentsOfFile:[NSString stringWithFormat:@"%s",_file_name]]retain];
    
    NSString *msg = [[NSString alloc] initWithData:file1 encoding:NSUTF8StringEncoding];
    
    UIAlertView *dialog = [[UIAlertView alloc] init];
    [dialog setDelegate: [[[UIApplication sharedApplication]delegate] rootViewController]];
    [dialog setTitle: @"Info"];
    [dialog setMessage: msg];
    [dialog addButtonWithTitle: @"Ok"];
    [dialog show];
    [dialog release];
    [msg release];
    [file1 release];
};

//void ObjCCalls::amazonCallBack(void *_data)
//{
//    NSLog(@" ObjCCalls::amazonCallBack");
//    if(callback_object)
//    {
//        
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *documentsDirectory = [paths objectAtIndex:0];
//        NSString *filePath = [documentsDirectory stringByAppendingPathComponent:@"candykadze_testflight_current_version.plist"];
//        NSMutableDictionary *m_dict =  [[NSMutableDictionary alloc] initWithContentsOfFile:filePath];
//        if(m_dict)
//        {
//            NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
//            NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
//            if([version floatValue] < [[m_dict objectForKey:@"CurrentVersionNumber"] floatValue])
//            {
//                const char *url = [[m_dict objectForKey:@"CurrentVersionURL"] UTF8String];
//                NSString *desc = [m_dict objectForKey:@"CurrentVersionDescription"];
//                NSString *str = [NSString stringWithUTF8String:[desc cStringUsingEncoding:NSASCIIStringEncoding]  ];
//                desc = [str stringByReplacingOccurrencesOfString:@"\n" withString:@"\n"];
//                const char *description = [desc UTF8String];
//                if(callback_object)
//                    (callback_object->*callback_func)((CCNode *)url,(void *)description);
//            }
//            [m_dict release];
//        }
//    };
//};

#pragma mark - CKAMAZON_HELPER
static CKAmazonHelper *m_AmazonHelper = NULL;
void *CKAmazonHelper::client = NULL;
CKAmazonHelper::~CKAmazonHelper()
{
    [(AmazonS3Client *)client release];
};
CKAmazonHelper::CKAmazonHelper()
{
    client = NULL;
}
CKAmazonHelper &CKAmazonHelper::Instance()
{
    if(!m_AmazonHelper)
    {
        m_AmazonHelper = new CKAmazonHelper;
    };
    if(client == NULL)
    {
        client = [[[AmazonS3Client alloc] initWithAccessKey:[NSString stringWithFormat:@"%s",ACCESS_KEY_ID ] withSecretKey:[NSString stringWithFormat:@"%s",SECRET_KEY ] ] retain];
    }
    return *m_AmazonHelper;
};

bool CKAmazonHelper::needUpdateTestFlightAppVersion(cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel)
{
  //  if(![self hostIsReachable])
 //       return false;
    if(client == NULL)
        return false;
    //save plist
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        NSString *file_name = [NSString stringWithUTF8String:testflight_file_name];
        S3GetObjectRequest * getObjectRequest = [[S3GetObjectRequest alloc] initWithKey:file_name withBucket:@"app-version-control"];
        S3GetObjectResponse *getObjectResponse = [(AmazonS3Client *)CKAmazonHelper::client getObject:getObjectRequest];

        CFPropertyListRef plist =  CFPropertyListCreateFromXMLData(kCFAllocatorDefault, (CFDataRef)getObjectResponse.body,
                                                                   kCFPropertyListImmutable,
                                                                   NULL);
        
        NSMutableDictionary *m_dict;
        if ([(id)plist isKindOfClass:[NSDictionary class]])
        {
            m_dict =  [(NSMutableDictionary *)plist retain];
        }
        else
        {
            // clean up ref
            CFRelease(plist);
            return;
        }
        if(m_dict)
        {
            NSDictionary* infoDict = [[NSBundle mainBundle] infoDictionary];
            NSString* version = [infoDict objectForKey:@"CFBundleVersion"];
            NSString* current = [m_dict objectForKey:@"CurrentVersionNumber"];
            if(![version isEqualToString:current])
            {
                const char *url = [[m_dict objectForKey:@"CurrentVersionURL"] UTF8String];
                NSString *desc = [m_dict objectForKey:@"CurrentVersionDescription"];
                NSString *str = [NSString stringWithUTF8String:[desc cStringUsingEncoding:NSASCIIStringEncoding]  ];
                desc = [str stringByReplacingOccurrencesOfString:@"\n" withString:@"\n"];
                const char *description = [desc UTF8String];

                dispatch_sync(dispatch_get_main_queue(), ^{
                                (node->*_func_sel)((CCNode *)url,(void *)description);
                });//end block
            }
            [m_dict release];
            CFRelease(plist);
        }
        
        [getObjectRequest release];

        
    });
    return true;
};

bool CKAmazonHelper::downloadBanner()
{
    if(client == NULL || !ObjCCalls::isActiveConnection())
        return false;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);//
    //NSString *documentsDirectory = [[paths objectAtIndex:0]stringByAppendingPathComponent:@"/ck.banner"];
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSLog(@"processDowloadBanner");
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
    dispatch_async(queue, ^{
        @try{
            
            NSString * str_bucket = [NSString stringWithUTF8String:stigol_banner];
            NSArray *listofobjects = [(AmazonS3Client *)client listObjectsInBucket:str_bucket];
            
            for(int j = 0;j < [listofobjects count]; j++)
            {
                S3ObjectSummary *sumarry = [listofobjects objectAtIndex:j];
                
                NSString *string = [NSString  stringWithFormat:@"%@/%@",documentsDirectory,sumarry.key];
                
                NSDictionary* fileAttribs = [[NSFileManager defaultManager] attributesOfItemAtPath:string error:nil];
                NSDate *create_date = [fileAttribs fileCreationDate]; //or fileModificationDate
                
                NSDateFormatter* df_utc = [[[NSDateFormatter alloc] init] autorelease];
                [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
                [df_utc setDateFormat:@"yyyy.MM.dd HH:mm"];
                NSString* curent_file_data = [df_utc stringFromDate:create_date];
                
                NSString *stringWithoutSpaces = [sumarry.lastModified stringByReplacingOccurrencesOfString:@"T" withString:@" "];
                stringWithoutSpaces = [stringWithoutSpaces stringByReplacingOccurrencesOfString:@"Z" withString:@""];
                stringWithoutSpaces = [stringWithoutSpaces stringByReplacingOccurrencesOfString:@"-" withString:@"."];
                [df_utc setDateFormat:@"yyyy.MM.dd HH:mm:ss.000"];
                [df_utc setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
                NSDate *result2 = [df_utc dateFromString:stringWithoutSpaces];
                
                // NSComparisonResult result_ = [result2 compare:create_date];
                
                
                BOOL update_file = NO;
                switch ([result2 compare:create_date])
                {
                    case NSOrderedDescending:
                        NSLog(@"%@ is in past from %@", result2, curent_file_data);
                        update_file = YES;
                        break;
                }
                
                if (![[NSFileManager defaultManager] fileExistsAtPath:string] || update_file){
                    
                    const char * str = [sumarry.key cStringUsingEncoding:NSUTF8StringEncoding];
                    NSString *myDocsPath =  [documentsDirectory stringByAppendingFormat:@"/%@",sumarry.key];
                    //NSLog(@"myDocsPath %@",myDocsPath);
                    if(![[sumarry.key pathExtension] isEqualToString:@"png"])
                    {
                        //save plist
                        S3GetObjectRequest * getObjectRequest = [[S3GetObjectRequest alloc] initWithKey:sumarry.key withBucket:str_bucket];
                        S3GetObjectResponse *getObjectResponse = [(AmazonS3Client *)client getObject:getObjectRequest];
                        
                        
                        NSData *restoredFile = [[[NSData alloc]init] autorelease];
                        restoredFile = getObjectResponse.body;
                        [restoredFile writeToFile:myDocsPath atomically:YES];
                        
                        
                        [getObjectRequest release];
                        
                    }
                    else if(((str[strlen(str) - 6] == '_')&&(CCDirector::sharedDirector()->getWinSize().width == 768))|| ((str[strlen(str) - 6] != '_') &&(CCDirector::sharedDirector()->getWinSize().width != 768)))
                    {
                        //save file
                        S3GetObjectRequest * getObjectRequest = [[S3GetObjectRequest alloc] initWithKey:sumarry.key withBucket:str_bucket];
                        S3GetObjectResponse *getObjectResponse = [(AmazonS3Client *)client getObject:getObjectRequest];
                        NSData *restoredFile = [[[NSData alloc]init] autorelease];
                        restoredFile = getObjectResponse.body;
                        [restoredFile writeToFile:myDocsPath atomically:YES];
                        [getObjectRequest release];
                    }
                };
            };
        }
        
        @catch (NSException *exception) {
            NSLog(@"There was an exception when connecting to s3: %@",exception.description);
        }
        
    });
    return true;
};

void CKAmazonHelper::uploadPlayerInfo()
{
#if TARGET_IPHONE_SIMULATOR
    return;
#endif
    
    if(client == NULL || !ObjCCalls::isActiveConnection())
        return;
    NSLog(@"CKAmazonHelper::uploadPlayerInfo");
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        @try{
            NSData *data;
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [[[paths objectAtIndex:0]stringByAppendingPathComponent:@"/"]stringByAppendingPathComponent:@"ck.user.info/"];
            
            NSLog(@"LISTING ALL FILES FOUND %@",documentsDirectory);
            NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:NULL];
            
            for (int Count = 0; Count < (int)[directoryContent count]; Count++)
            {
                
                if(![[[directoryContent objectAtIndex:Count] pathExtension] isEqualToString:@"txt"])
                    continue;
                
                NSString *path = [documentsDirectory stringByAppendingPathComponent:[directoryContent objectAtIndex:Count] ];
                
                if ([[NSFileManager defaultManager] fileExistsAtPath:path] && (AmazonS3Client *)client != nil)
                {
                    //File exists
                    NSLog(@"File %d: %@ %@", (Count + 1), [directoryContent objectAtIndex:Count],[[directoryContent objectAtIndex:Count] pathExtension]);
                    NSData *file1 = [[[NSData alloc] initWithContentsOfFile:path]retain];
                    data        = file1;
                    
                    // Upload image data.  Remember to set the content type.
                    S3PutObjectRequest *por = [[[S3PutObjectRequest alloc] initWithKey:[directoryContent objectAtIndex:Count] inBucket:[NSString stringWithUTF8String:bucket_device_info]] autorelease];
                    por.contentType = @"text/plain";
                    por.data = data;
                    // Put the image data into the specified s3 bucket and object.
                    S3PutObjectResponse *putObjectResponse = [(AmazonS3Client *)client putObject:por];
                    
                    if(putObjectResponse != nil && putObjectResponse.error != nil)
                    {
                        NSLog(@"Error: putObjectResponse %@", putObjectResponse.error);
                    }
                    else
                    {
                        NSError *error;
                        bool rem = [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
                        
                        NSLog(@"File exists: %d %@", [[NSFileManager defaultManager] fileExistsAtPath:path],path);
                        NSLog(@"Is deletable file at path: %d", [[NSFileManager defaultManager] isDeletableFileAtPath:path]);
                        
                        
                        if (!rem)	//Delete it
                        {
                            NSLog(@"Delete file error: %@", error);
                        };
                        NSLog(@"Complite:");
                    }
                    
                    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                    
                }
                else
                {
                    NSLog(@"File does not exist");
                }
            };
        }
        @catch (NSException *exception) {
            NSLog(@"There was an exception when connecting to s3: %@",exception.description);
        }
    });
};

void CKAmazonHelper::uploadStatisticsJson()
{
#if TARGET_IPHONE_SIMULATOR
    return;
#endif
    
    if(client == NULL || !ObjCCalls::isActiveConnection())
        return;
    NSLog(@"CKAmazonHelper::uploadStatisticsJson");
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_async(queue, ^{
        @try{
            NSData *data;
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [[[paths objectAtIndex:0]stringByAppendingPathComponent:@"/"]stringByAppendingPathComponent:@"ck.statistic.info/"];
            
            NSLog(@"LISTING ALL FILES FOUND %@",documentsDirectory);
            NSArray *directoryContent = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:documentsDirectory error:NULL];
            
            for (int Count = 0; Count < (int)[directoryContent count]; Count++)
            {
                
                if(![[[directoryContent objectAtIndex:Count] pathExtension] isEqualToString:@"txt"])
                    continue;
                
                NSString *path = [documentsDirectory stringByAppendingPathComponent:[directoryContent objectAtIndex:Count] ];
                
                if ([[NSFileManager defaultManager] fileExistsAtPath:path] && (AmazonS3Client *)client != nil)
                {
                    //File exists
                    NSLog(@"File %d: %@ %@", (Count + 1), [directoryContent objectAtIndex:Count],[[directoryContent objectAtIndex:Count] pathExtension]);
                    NSData *file1 = [[[NSData alloc] initWithContentsOfFile:path]retain];
                    data        = file1;
                    
                    // Upload image data.  Remember to set the content type.
                    S3PutObjectRequest *por = [[[S3PutObjectRequest alloc] initWithKey:[directoryContent objectAtIndex:Count] inBucket:[NSString stringWithUTF8String:bucket_statistics]] autorelease];
                    por.contentType = @"text/plain";
                    por.data = data;
                    // Put the image data into the specified s3 bucket and object.
                    S3PutObjectResponse *putObjectResponse = [(AmazonS3Client *)client putObject:por];
                    
                    if(putObjectResponse != nil && putObjectResponse.error != nil)
                    {
                        NSLog(@"Error: putObjectResponse %@", putObjectResponse.error);
                    }
                    else
                    {
                        NSError *error;
                        bool rem = [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
                        
                        NSLog(@"File exists: %d %@", [[NSFileManager defaultManager] fileExistsAtPath:path],path);
                        NSLog(@"Is deletable file at path: %d", [[NSFileManager defaultManager] isDeletableFileAtPath:path]);
                        
                        
                        if (!rem)	//Delete it
                        {
                            NSLog(@"Delete file error: %@", error);
                        };
                        NSLog(@"Complite:");
                    }
                    
                    //[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
                    
                }
                else
                {
                    NSLog(@"File does not exist");
                }
            };
        }
        @catch (NSException *exception) {
            NSLog(@"There was an exception when connecting to s3: %@",exception.description);
        }
    });
};

