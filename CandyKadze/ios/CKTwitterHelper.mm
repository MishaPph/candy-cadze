//
//  ObjCCalls.m
//  CandyKadze
//
//  Created by PR1.Stigol on 06.11.12.
//
//

#include "CKTwitterHelper.h"
#include "AdSupport/ASIdentifierManager.h"
#import <Twitter/Twitter.h>
#import <Accounts/Accounts.h>
#include "CKHelper.h"
#import "AppController.h"
#include "CKTableScore.h"

#pragma mark - TWEET
static CKTwitterHelper *m_CKTwitterHelper = NULL;
CKTwitterHelper::CKTwitterHelper()
{
   // need_load_friends = false;
    target = NULL;
};
CKTwitterHelper &CKTwitterHelper::Instance()
{
    if(!m_CKTwitterHelper)
    {
        m_CKTwitterHelper = new CKTwitterHelper;
    };
    return *m_CKTwitterHelper;
}


bool CKTwitterHelper::TweetCanSend()
{
    return [TWTweetComposeViewController canSendTweet];
}
void CKTwitterHelper::TweetFollowing(cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel,const char *_name)
{
    if(![TWTweetComposeViewController canSendTweet])
        return;
    
	ACAccountStore *accountStore = [[[ACAccountStore alloc] init]autorelease];
	
	// Create an account type that ensures Twitter accounts are retrieved.
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
	
	// Request access from the user to use their Twitter accounts.
    [accountStore requestAccessToAccountsWithType:accountType withCompletionHandler:^(BOOL granted, NSError *error) {
        if(granted) {
            // Get the list of Twitter accounts.
            NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
            
            if ([accountsArray count] > 0) {
                // Grab the initial Twitter account to tweet from.
                ACAccount *twitterAccount = [accountsArray objectAtIndex:0];
                
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                [tempDict setValue:[NSString stringWithUTF8String:_name] forKey:@"screen_name"];
                [tempDict setValue:@"true" forKey:@"follow"];
                
                TWRequest *postRequest = [[TWRequest alloc] initWithURL: [NSURL URLWithString:@"https://api.twitter.com/1.1/friendships/create.json"]
                                                             parameters:tempDict requestMethod:TWRequestMethodPOST];
                [postRequest setAccount:twitterAccount];
                
                // Perform the request created above and create a handler block to handle the response.
                [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                    
                    if ([urlResponse statusCode] == 200) {
                        NSLog(@"TweetIsFollowing");
                        TweetIsFollowing(node,_func_sel,_name);
                    };
                }];
            }
        }
    }];
};
void CKTwitterHelper::TweetgetInfo(bool _load_frieds,cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel)
{
    if(ObjCCalls::getDeviceVersion() < 6.0)
        return;
    //if ([TWTweetComposeViewController canSendTweet])
    {
        // Create account store, followed by a Twitter account identifer
        ACAccountStore * accountStore = [[[ACAccountStore alloc] init]autorelease];
        ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter] ;
        
        // Request access from the user to use their Twitter accounts.
        [accountStore requestAccessToAccountsWithType:accountType withCompletionHandler:^(BOOL granted, NSError *error)
         {
             // Did user allow us access?
             if (granted == YES)
             {
                 NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
                 
                 if ([accountsArray count] > 0) {
                     // Grab the initial Twitter account to tweet from.
                     ACAccount *twitterAccount = [accountsArray objectAtIndex:0];
                     NSDictionary *tempDict2 = [[[NSMutableDictionary alloc] initWithDictionary: [twitterAccount dictionaryWithValuesForKeys:[NSArray arrayWithObject:@"properties"]]] autorelease];
                     
                     NSString *tempUserID = [[tempDict2 objectForKey:@"properties"] objectForKey:@"user_id"];
                     
                     CKHelper::Instance().TwitterID = [tempUserID UTF8String];
                     NSString *username = twitterAccount.username;
                     CKHelper::Instance().TWITTER_USERNAME = [username UTF8String];
                     CKFileInfo::Instance().save();
                     
                     NSLog(@"TweetgetInfo::tempUserID %s %s %@",CKHelper::Instance().TwitterID.c_str(),CKHelper::Instance().TWITTER_USERNAME.c_str(),twitterAccount.identifier);
                     if(_load_frieds)
                     {
                         TweetLoadFriendList(node,_func_sel);
                     }
                 }
             }
             else
             {
                 if(node)
                 {
                     int d = CK::Callback_State_TW_NoGranted;
                     (node->*_func_sel)(NULL,&d);
                 }
             }
             
         }];
    }
};

void CKTwitterHelper::TweetLoadFriendList(cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel)
{
    CCLOG("ObjCCalls::TweetLoadFriendList");
    
    ACAccountStore *accountStore = [[[ACAccountStore alloc] init]autorelease];
	// Create an account type that ensures Twitter accounts are retrieved.
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
    
    NSArray * m_array = [accountStore accountsWithAccountType:accountType];
    // NSLog(@"ObjCCalls::TweetLoadFriendList ACAccountType %d %@",[accountType accessGranted],m_array);
    if(m_array == NULL)
    {
        //sprintf(ObjCCalls_func_name,"TweetLoadFriendList");
        //need_load_friends = true;
        TweetgetInfo(true,node,_func_sel);
        return;
    }
    else
    {
        NSDictionary *tempDict2 = [[[NSMutableDictionary alloc] initWithDictionary: [m_array dictionaryWithValuesForKeys:[NSArray arrayWithObject:@"properties"]]]autorelease];
        NSDictionary *temp3 = [tempDict2 objectForKey:@"properties"];
        NSArray *tempUserName = [m_array valueForKey:@"username"];
        NSArray *tempUserID = [temp3 valueForKey:@"user_id"];
        
        for (NSString *str in tempUserID) {
            NSLog(@"TweetLoadFriendList::tempUserID %@",str);
            CKHelper::Instance().TwitterID = [str UTF8String];
        };
        
        for (NSString *str in tempUserName) {
            NSLog(@"TweetLoadFriendList::tempUserName %@",str);
            CKHelper::Instance().TWITTER_USERNAME = [str UTF8String];
        };
        CKFileInfo::Instance().save();
    };
   // need_load_friends = false;
//    sprintf(ObjCCalls_func_name," ");
	// Request access from the user to use their Twitter accounts.
    [accountStore requestAccessToAccountsWithType:accountType withCompletionHandler:^(BOOL granted, NSError *error) {
        CCLOG("TweetLoadFriendList granted %d",granted);
        if(granted) {
            // Get the list of Twitter accounts.
            NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
            
            if ([accountsArray count] > 0) {
                // Grab the initial Twitter account to tweet from.
                ACAccount *twitterAccount = [accountsArray objectAtIndex:0];
                
                NSMutableDictionary *tempDict = [[[NSMutableDictionary alloc] init] autorelease];
                [tempDict setValue:[NSString stringWithUTF8String:CKHelper::Instance().TwitterID.c_str()] forKey:@"user_id"];
                [tempDict setValue:@"-1" forKey:@"cursor"];
                
                TWRequest *postRequest = [[[TWRequest alloc] initWithURL: [NSURL URLWithString:@"https://api.twitter.com/1.1/followers/ids.json"] parameters:tempDict requestMethod:SLRequestMethodGET] autorelease];
                
                // Set the account used to post the tweet.
                [postRequest setAccount:twitterAccount];
                
                // Perform the request created above and create a handler block to handle the response.
                [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                    if ([urlResponse statusCode] == 200) {
                        NSError *jsonParsingError = nil;
                        NSDictionary *publicTimeline = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonParsingError];
                        
                        
                        if (!publicTimeline) {
                            NSLog(@"Error parsing JSON: %@", jsonParsingError);
                        }
                        else {
                            //NSLog(@"TweetLoadFriendList %@",[publicTimeline objectForKey:@"ids"]);
                            NSArray *arr = [publicTimeline objectForKey:@"ids"];
                            CKTableTwitter::Instance().clearFriends();
                            if(arr)
                            {
                                for(NSNumber *item in arr) {
                                    NSLog(@"TweetLoadFriendList %@",item);
                                    CKTableTwitter::Instance().addFriend([[item stringValue] UTF8String]);
                                };
                                if(node)
                                {
                                    int d = CK::Callback_State_TW_Complite;
                                    (node->*_func_sel)(NULL,&d);
                                }
                            }
                            else
                            {
                                if(node)
                                {
                                    int d = CK::Callback_State_TW_Complite;
                                    (node->*_func_sel)(NULL,&d);
                                }
                                //  CKTableTwitter::Instance().addFriend(CKHelper::Instance().TwitterID);
                            }
                        };
                        
                    };
                }];
            }
        }
        else
        {
            if(node)
            {
                int d = CK::Callback_State_TW_NoGranted;
                (node->*_func_sel)(NULL,&d);
            }
        }
    }];
};

void CKTwitterHelper::TweetIsFollowing(cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel,const char *_name)
{
    TweetgetInfo(false,NULL,NULL);
    
    ACAccountStore *accountStore = [[[ACAccountStore alloc] init]autorelease];
	
	// Create an account type that ensures Twitter accounts are retrieved.
    ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
	
	// Request access from the user to use their Twitter accounts.
    [accountStore requestAccessToAccountsWithType:accountType withCompletionHandler:^(BOOL granted, NSError *error) {
        CCLOG("ObjCCalls::TweetIsFollowing granted %d",granted);
        if(granted) {
            // Get the list of Twitter accounts.
            NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
            
            if ([accountsArray count] > 0) {
                // Grab the initial Twitter account to tweet from.
                ACAccount *twitterAccount = [accountsArray objectAtIndex:0];
                
                NSMutableDictionary *tempDict = [[NSMutableDictionary alloc] init];
                [tempDict setValue:[NSString stringWithUTF8String:_name] forKey:@"screen_name"];
                [tempDict setValue:@"-1" forKey:@"cursor"];
                
                TWRequest *postRequest = [[TWRequest alloc] initWithURL: [NSURL URLWithString:@"https://api.twitter.com/1.1/users/lookup.json"]
                                                             parameters:tempDict requestMethod:SLRequestMethodGET];
                
                // Set the account used to post the tweet.
                [postRequest setAccount:twitterAccount];
                
                // Perform the request created above and create a handler block to handle the response.
                [postRequest performRequestWithHandler:^(NSData *responseData, NSHTTPURLResponse *urlResponse, NSError *error) {
                    if ([urlResponse statusCode] == 200) {
                        NSError *jsonParsingError = nil;
                        NSArray *publicTimeline = [NSJSONSerialization JSONObjectWithData:responseData options:0 error:&jsonParsingError];
                        
                        if (!publicTimeline) {
                            NSLog(@"Error parsing JSON: %@", jsonParsingError);
                        } else {
                            for(NSDictionary *item in publicTimeline) {
                                NSString *num = [item objectForKey:@"following"];
                                if([num intValue] == 1)
                                {
                                    (node->*_func_sel)(NULL,NULL);
                                };
                            };
                        };
                    };
                }];
            }
        }
    }];
};
void CKTwitterHelper::goToTwitter(const char *_userID)
{
    //    NSString *msg = [NSString stringWithFormat:@"http://www.twitter.com/infostigol"];
    //    NSURL *nsUrl = [NSURL URLWithString:msg];
    //    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?id=41553213"]];
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"twitter://user?id=41553213"]];
    
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://"]]) {
        NSURL *nsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"twitter://user?screen_name=%s",_userID ]];
        [[UIApplication sharedApplication] openURL:nsUrl];
    }
    else
    {
        NSURL *nsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.twitter.com/%s",_userID ]];
        [[UIApplication sharedApplication] openURL:nsUrl];
    }
}
void CKTwitterHelper::TweetLogin()
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"twitter://"]]) {
        NSURL *nsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"twitter://"]];
        [[UIApplication sharedApplication] openURL:nsUrl];
    }
    else
    {
        NSURL *nsUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://www.twitter.com/" ]];
        [[UIApplication sharedApplication] openURL:nsUrl];
    }
    return;
};

void CKTwitterHelper::TweetPost(const char* _message,const char *_image)
{
    AppController *appDelegate = [[UIApplication sharedApplication]delegate];
    NSLog(@"TweetPost %s",_image);
    [[appDelegate rootViewController]sendTweet:[NSString stringWithUTF8String:_message] image:[NSString stringWithUTF8String:_image] url:[NSString stringWithUTF8String:POST_APP_URL]];
    TweetgetInfo(false,NULL,NULL);
};

