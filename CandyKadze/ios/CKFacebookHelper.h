//
//  ObjCCalls.h
//  CandyKadze
//
//  Created by PR1.Stigol on 06.11.12.
//
//

#ifndef CandyKadze_CKFacebookHelper_h
#define CandyKadze_CKFacebookHelper_h
#include "cocos2d.h"

class CKFacebookHelper
{
    CKFacebookHelper();
    char next_func_name[255];
    char next_func_parametr[255];
    char next_func_filename[255];
    cocos2d::CCNode * m_target;
    cocos2d::SEL_CallFuncND call_back_func;
    void runCallBack(int _d);
public:
    void setNextCallFuncName(const char *_func);
    void FB_LoadUserAvatar(const char *_id);
    void FB_CreateNewSession();
    void FB_Login();
    void FB_TryLogin();
    void FB_Logout();
    void FB_getUserInfo();
    void FB_getUserInfoSL();
    void FB_loadFriendsWithArray(void *arr);
    void FB_ProcessIncomingURL(const char* targetURL);
    void FB_ProcessIncomingRequest(const char* targetURL);
    void FB_ProcessIncomingFeed(const char* targetURL);
    void FB_Customize();
    void FB_PostImage(const char *_msg,const char *_image_name);
    void FB_SendRequest(const int nScore);
    void FB_LoadFriendsListOld();
    void FB_LoadFriendsList();
    void FB_SendBrag(const int _score,const int _lvl_num,const int world_num,bool _call_func = false);
    void FB_Challenge(const int _score,const int _lvl,const int world_num,bool x = false);
    void FB_SendScore(const int nScore);
    void FB_RequestWritePermissions();
    void FB_GetLike(cocos2d::CCObject *_selector, cocos2d::SEL_CallFunc _call_func,const char *_id);
    void FB_goToPageLikeOurPage();
    void FB_goToPageLikeAnyPage();
    bool FB_isOpenSession();
    void setCallBackFunc(cocos2d::CCNode* node, cocos2d::SEL_CallFuncND _func_sel);
    static CKFacebookHelper &Instance();
};

#endif
