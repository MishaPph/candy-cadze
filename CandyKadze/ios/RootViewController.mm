//
//  CandyKadzeAppController.h
//  CandyKadze
//
//  Created by PR1.Stigol on 18.09.12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import "RootViewController.h"
#import "AppController.h"


#import <netinet/in.h>
#import <netinet6/in6.h>
#import <sys/socket.h>
#import <arpa/inet.h>
#import <SystemConfiguration/SystemConfiguration.h>
#import "Reachability.h"
#include "ObjCCalls.h"
#include "CKID.h"
#import "SimplePingHelper.h"
#import "CKIAPHelper.h"
#include "CKFacebookHelper.h"

@interface RootViewController ()

- (void)updateView;

@end

@implementation RootViewController

//@synthesize s3 = _s3;
@synthesize adMobAd = adMobAd;
@synthesize iadView = adView;
#pragma mark - UIViewController


- (void)handledURL: (NSNotification *)deepLinkNotification{

        NSURL *targetURL = deepLinkNotification.object;
        CKFacebookHelper::Instance().FB_ProcessIncomingURL([[NSString initWithContentsOfURL:targetURL] UTF8String]);
}

- (void)pingResult:(NSNumber*)success {
    CCLOG("pingResult: %d",success.boolValue);
	if (success.boolValue) {
		active_ping = true;
	} else {
		active_ping = false;
	};
}

-(bool) isActiveInternet;
{
    bool last_ping = active_ping;
    [SimplePingHelper ping:[NSString stringWithUTF8String:REACHABILITY_HOST] target:self sel:@selector(pingResult:)];
    bool success = [self hostIsReachable];
    NSLog(@"internetActive %d  ping %d last_ping %d",success,active_ping,last_ping);
    return success&&active_ping;
};

-(bool) hostIsReachable
{
    bool success = false;
    const char *host_name = REACHABILITY_HOST;
    SCNetworkReachabilityRef reachability = SCNetworkReachabilityCreateWithName(NULL,host_name);
    SCNetworkReachabilityFlags flags;
    success = SCNetworkReachabilityGetFlags(reachability, &flags);
    bool isAvailable = success && (flags & kSCNetworkFlagsReachable) && !(flags & kSCNetworkFlagsConnectionRequired);
    if (isAvailable) {
        NSLog(@"Host is reachable: %d", flags);
    }else{
        NSLog(@"Host is unreachable");
    }
    CFRelease(reachability);
    return isAvailable;
};

-(void) checkNetworkStatus:(NSNotification *)notice
{
    // called after network status changes
    NetworkStatus internetStatus = [internetReachable currentReachabilityStatus];
    switch (internetStatus)
    {
        case NotReachable:
        {
            NSLog(@"The internet is down.");
            self->internetActive = NO;
            
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"The internet is working via WIFI.");
            self->internetActive = YES;
            
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"The internet is working via WWAN.");
            self->internetActive = YES;
            
            break;
        }
    }

    
    NetworkStatus hostStatus = [hostReachable currentReachabilityStatus];
    switch (hostStatus)
    {
        case NotReachable:
        {
            NSLog(@"A gateway to the host server is down.");
            self->hostActive = NO;
            
            break;
        }
        case ReachableViaWiFi:
        {
            NSLog(@"A gateway to the host server is working via WIFI.");
            self->hostActive = YES;
            
            break;
        }
        case ReachableViaWWAN:
        {
            NSLog(@"A gateway to the host server is working via WWAN.");
            self->hostActive = YES;
            
            break;
        }
    }
};
#pragma mark - GAME CENTER
- (void) showLeaderboard;
{
	GKLeaderboardViewController *leaderboardController = [[GKLeaderboardViewController alloc] init];
	if (leaderboardController != NULL)
	{
		leaderboardController.category =  @"Total_scores";
		leaderboardController.timeScope = GKLeaderboardTimeScopeAllTime;
		leaderboardController.leaderboardDelegate = self;
		[self presentModalViewController: leaderboardController animated: YES];
	}
};

- (void)leaderboardViewControllerDidFinish:(GKLeaderboardViewController *)viewController
{
	[self dismissModalViewControllerAnimated: YES];
	[viewController release];
}
#pragma mark MAIL
- (void)sendMail:(NSString *) _sybject message:(NSString *)_message image:(NSString *)_image
{

   // NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
   // NSString *documentsDirectory = [paths objectAtIndex:0];
   // NSString *imageFilePath = [documentsDirectory stringByAppendingPathComponent:@"attach.jpg"];
    
  //  NSData *imageData = [[NSData alloc] initWithContentsOfURL:[NSURL fileURLWithPath:@"attach.jpg"]];
    
    NSData * imageData = UIImageJPEGRepresentation([UIImage imageWithContentsOfFile:_image ],1.0);
   // NSData *imageData =  [NSData dataWithContentsOfFile:imageFilePath];

    MFMailComposeViewController* message_view = [[MFMailComposeViewController alloc] init];
    if([MFMailComposeViewController canSendMail] )
    {
        if(_image)
        {
            [message_view addAttachmentData:imageData mimeType:@"image/jpg" fileName:@"game_logo.jpg"];
        }
        else
        {
            UIImage *image = [UIImage imageNamed:@"mail_icon.png"];
            NSData *data = UIImagePNGRepresentation(image);
            [message_view addAttachmentData:data mimeType:@"image/png" fileName:@"game_logo.jpg"];
        }
        
        message_view.mailComposeDelegate = self;
        [message_view setSubject:  _sybject ];
        
        [message_view setMessageBody: _message   isHTML:YES];
        
        if (message_view) [self presentModalViewController:message_view animated:YES];
            [message_view release];
    }
    else
    {
//        [[[UIAlertView alloc] initWithTitle:@"Warning"
//                                    message:_alert
//                                   delegate:self
//                          cancelButtonTitle:@"OK!"
//                          otherButtonTitles:nil] show];
    }
};
- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    else
        NSLog(@"Not send email");
    [self dismissModalViewControllerAnimated:YES];
};

-(void) sendMessage:(NSString *) _message
{

    if ([MFMessageComposeViewController canSendText]) {
            
            AppController *appDelegate = [[UIApplication sharedApplication]delegate];
            [appDelegate pauseDirector];
            
            picker = [[MFMessageComposeViewController alloc] init];
            picker.messageComposeDelegate = self;
            picker.body = _message;
            picker.wantsFullScreenLayout = NO;
            [self presentModalViewController:picker animated:false];
            [[UIApplication sharedApplication] setStatusBarHidden:YES];

            [picker release];
		}
		else {
			NSLog(@"Device not configured to send SMS.");
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"iMessage" message:@"Device not configured to send SMS." delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
            [alert release];
		}
//	}
//	else {
//		NSLog(@"Device not configured to send SMS.");
//	}
}

// Dismisses the message composition interface when users tap Cancel or Send. Proceeds to update the
// feedback message field with the result of the operation.
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result {
	
	//feedbackMsg.hidden = NO;
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MessageComposeResultCancelled:
			NSLog(@"Result: SMS sending canceled");
			break;
		case MessageComposeResultSent:
			NSLog(@"Result: SMS sent");
			break;
		case MessageComposeResultFailed:
			NSLog(@"Result: SMS sending failed");
			break;
		default:
			NSLog(@"Result: SMS not sent");
			break;
	}
    
    AppController *appDelegate = [[UIApplication sharedApplication]delegate];
    [appDelegate resumDirector];
    [self dismissModalViewControllerAnimated:false];
}



- (void)checkConnection {
    struct sockaddr_in callAddress;
    callAddress.sin_len = sizeof(callAddress);
    callAddress.sin_family = AF_INET;
    callAddress.sin_port = htons(24);
    callAddress.sin_addr.s_addr = inet_addr("10.29.6.48");
    Reachability *reachibility = [Reachability reachabilityWithAddress:&callAddress];
    
    NetworkStatus netStatus = [reachibility currentReachabilityStatus];
    NSLog(@"checkConnection %@",netStatus);
}

// вызывается при обнаружении изменения состояния
- (void) reachabilityChanged: (NSNotification* )note
{
//    Reachability* curReach = [note object];
//    NSParameterAssert([curReach isKindOfClass: [Reachability class]]);
//    [self updateInternetStatus: curReach];
}

// login  or  check status of  game center 
-(void) authenticatePlayer
{
    GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
    // if (localPlayer.isAuthenticated == YES) {
    [localPlayer authenticateWithCompletionHandler:^(NSError *error)
     {
         if (localPlayer.isAuthenticated) {
             NSLog(@"auth complete");
             playerIsAuthenticated = true;
         }
         
     }];
    //}
}

-(void) loadView
{
    adView = nil;
    adMobAd = nil;
    connection_opened = NO;
};

-(void) viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) viewDidAppear:(BOOL)animated {

//    NSArray *languageArray = [NSLocale preferredLanguages];
//    NSString *language = [languageArray objectAtIndex:0];
//    UIAlertView *dialog = [[UIAlertView alloc] init];
//    [dialog setDelegate: self];
//    [dialog setTitle: @"Last Send"];
//    [dialog setMessage: language];
//    [dialog addButtonWithTitle: @"Ok"];
//    [dialog show];
    playerIsAuthenticated = false;
    [super viewDidAppear:FALSE];
    [self authenticatePlayer];
    [RageIAPHelper sharedInstance];
    
    connection_opened = NO;
    
    // check for internet connection
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetworkStatus:) name:kReachabilityChangedNotification object:nil];
    
    internetReachable = [[Reachability reachabilityForInternetConnection] retain];
    [internetReachable startNotifier];
    
    [SimplePingHelper ping:[NSString stringWithUTF8String: REACHABILITY_HOST ] target:self sel:@selector(pingResult:)];
    

 
//    //auto login in tweeter
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(canTweetStatus) name:ACAccountStoreDidChangeNotification object:nil];

}


- (void)viewDidLoad {
    [super viewDidLoad];
};

- (void)viewDidUnload
{
    [adMobAd release];
    [super viewDidUnload];
}

// Override to allow orientations other than the default landscape orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
   return UIInterfaceOrientationIsLandscape( interfaceOrientation );
    // Return YES for supported orientations

};
- (NSUInteger) supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}
- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    
}
- (void)didReceiveMemoryWarning {
        [super didReceiveMemoryWarning];
}
#pragma mark - MEMEORY
- (void)dealloc {
     [[NSNotificationCenter defaultCenter] removeObserver:self];
    self->adView.delegate=nil;
    [self->adView release];
    [self->adMobAd release];
    self->adView = nil;
    self->adMobAd = nil;
    [internetReach release];
    [super dealloc];
    //[self.s3 release];
}


#pragma mark - BANNER

-(void) deleteBanner
{
    self->adView.delegate=nil;
    [self->adView release];
    [self->adMobAd release];
    self->adView = nil;
    self->adMobAd = nil;
};

- (void) createBanner
{
    if(adView == nil)
    {
        [self createiAd];
    };

    if(adMobAd == nil)
    {
        [self createGAD];
    };
}

- (void) showBanner
{
    show_banner = true;
    if(loadBanneriAd)
    {
        [self showiAd];
    }
    else
        if(loadBannerGAD)
        {
            [self showGAD];
        }
}

- (void) hideBanner
{
    show_banner = false;
    [self hideiAd];
    [self hideGAD];
};

- (void) createGAD
{
    if([self isActiveInternet] && adMobAd == nil )
    {
        NSLog(@"createGAD");
        loadBannerGAD = false;
        self.adMobAd = [[GADBannerView alloc] initWithAdSize:kGADAdSizeSmartBannerLandscape];
        
        self.adMobAd.adUnitID =  [NSString stringWithFormat:@"%s",ADUNIT_ID];
        self.adMobAd.delegate = self;
        [self.adMobAd setRootViewController:self];
        [self.view addSubview:self.adMobAd];

        CGRect frame = self.adMobAd.frame;
        frame.origin.x = (self.view.frame.size.height - frame.size.width)/2;
        [self.adMobAd setFrame:frame];
        
        self.adMobAd.hidden = true;
        loadBannerGAD = false;

        GADRequest *request = [GADRequest request];
        request.testDevices = [NSArray arrayWithObjects: GAD_SIMULATOR_ID,@"51c1853194a9ca3088b493c2b31b74ea",@"8f7ccc5e0974ac6fe780c83d9c072d91", nil];
        request.testing = true;
        [adMobAd loadRequest:request];
    }
};

- (void)createiAd
{
    //if(ObjCCalls::isLowCPU())
    {
     //   [self createGAD ];
     //   return;
    };
    
    if([self isActiveInternet] && adView == nil)
    {
        NSLog(@"createiAd");
        
        adView = [[ADBannerView alloc] initWithFrame:CGRectZero];
        
        adView.requiredContentSizeIdentifiers = [NSSet setWithObject:ADBannerContentSizeIdentifierLandscape];
        adView.currentContentSizeIdentifier = ADBannerContentSizeIdentifierLandscape;
        
        CGRect frame = adView.frame;
       // NSLog(@"self.view.frame.size.widt %f %f",self.view.frame.size.height,frame.size.width);
        frame.origin.x = -(self.view.frame.size.height - frame.size.width)/2;
        [adView setFrame:frame];
        
        adView.delegate = self;
        adView.hidden = true;
        
        [self.view addSubview:adView];
    }
};

- (bool)showGAD
{
    NSLog(@"showGAD");
    if(adMobAd == nil  || show_banner == false)
    {
        return false;
    };
  //  if (!self.GADVisible)
    {
        
        CGRect frame = self.adMobAd.frame;
        frame.origin.x = (self.view.frame.size.height - frame.size.width)/2;
        [self.adMobAd setFrame:frame];
        
        self.adMobAd.hidden = false;
        return true;
    };
    return false;
};

- (void)hideGAD
{
    NSLog(@"hideGAD");
    self.adMobAd.hidden = true;
}

- (bool)showiAd
{
    if (adView && show_banner)
    {
        adView.hidden = NO;
        NSLog(@"showiAd");
        return true;
    };
    return false;
};

- (void)hideiAd
{
     adView.hidden = true;
};
- (void)bannerViewDidLoadAd:(ADBannerView *)banner
{
    NSLog(@"bannerViewDidLoadAd");
    loadBanneriAd = true;
    [self hideGAD];
    [self showiAd];
}

- (void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error
{
    NSLog(@"didFailToReceiveAdWithError");
    loadBanneriAd = false;
    [self hideiAd];
    if(loadBannerGAD)
    {
        [self showGAD];
    };
    
};


- (BOOL)bannerViewActionShouldBegin:(ADBannerView *)banner willLeaveApplication:(BOOL)willLeave
{
    NSLog(@"Banner view is beginning an ad action");
    BOOL shouldExecuteAction = YES;
    CK::StatisticLog::Instance().setLink(CK::Link_iAdBanner);
    if (!willLeave && shouldExecuteAction)
    {
        AppController *appDelegate = [[UIApplication sharedApplication]delegate];
        [appDelegate pause];
    };

    [self showiAd];
    
    return shouldExecuteAction;
};

- (void)adViewDidReceiveAd:(GADBannerView *)view;
{
    NSLog(@"Ad Received");
    loadBannerGAD = true;
    CGRect frame = self.adMobAd.frame;
    frame.origin.x = (self.view.frame.size.height - frame.size.width)/2;
    [self.adMobAd setFrame:frame];
    [self showGAD];
};

- (void)adViewWillLeaveApplication:(GADBannerView *)adView
{
    CK::StatisticLog::Instance().setLink(CK::Link_adMobBanner);
}
- (void)adView:(GADBannerView *)view didFailToReceiveAdWithError:(GADRequestError *)error {
    NSLog(@"Failed to receive ad with error: %@", [error localizedFailureReason]);
    loadBannerGAD = false;
    if(loadBanneriAd)
    {
        [self showiAd];
    };
}
@end
